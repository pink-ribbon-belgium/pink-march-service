    jand@Desjani:~>aws events list-rules --region eu-west-1 --profile pink-ribbon-belgium-admin
    {
        "Rules": [
            {
                "Name": "pink-march-service-pollActivities-acceptance",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-pollActivities-acceptance",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-pollActivities for mode acceptance",
                "ScheduleExpression": "rate(1 day)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-pollActivities-demo",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-pollActivities-demo",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-pollActivities for mode demo",
                "ScheduleExpression": "rate(1 hour)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-pollActivities-demo-MANUAL_EXPERIMENT",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-pollActivities-demo-MANUAL_EXPERIMENT",
                "State": "ENABLED",
                "Description": "created manually as admin to experiment",
                "ScheduleExpression": "rate(1 minute)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-pollActivities-dev-experiment",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-pollActivities-dev-experiment",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-pollActivities for mode dev-experiment",
                "ScheduleExpression": "rate(1 hour)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-updateRankings-acceptance",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-updateRankings-acceptance",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-updateRankings for mode acceptance",
                "ScheduleExpression": "rate(1 day)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-updateRankings-demo",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-updateRankings-demo",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-updateRankings for mode demo",
                "ScheduleExpression": "rate(1 hour)",
                "EventBusName": "default"
            },
            {
                "Name": "pink-march-service-updateRankings-dev-experiment",
                "Arn": "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-updateRankings-dev-experiment",
                "State": "ENABLED",
                "Description": "Trigger pink-march-service-updateRankings for mode dev-experiment",
                "ScheduleExpression": "rate(1 hour)",
                "EventBusName": "default"
            }
        ]
    }
    jand@Desjani:~>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-demo-MANUAL_EXPERIMENT
    {
        "Targets": [
            {
                "Id": "Id284140936901",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01058",
                "Input": "{\"mode\":\"demo\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }
    jand@Desjani:~>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-demo
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-demo",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"demo\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }
    jand@Desjani:~>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-acceptance
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-acceptance-00057",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00057\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00058",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00058\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00059",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00059\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00060",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00060\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00061",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00061\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }
    jand@Desjani:~>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-dev-experiment
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-dev-experiment",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities",
                "Input": "{\"mode\":\"dev-experiment\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }

ARNs that "work":

- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities`
  (`pink-march-service-pollActivities-dev-experiment`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01058`
  (`pink-march-service-pollActivities-demo-MANUAL_EXPERIMENT`)

ARNs that "don't work"

- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-demo`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-acceptance-00057`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-acceptance-00058`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-acceptance-00059`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-acceptance-00060`)
- `arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055`
  (`pink-march-service-pollActivities-acceptance-00061`)

There seems to be no difference between the ARNs

Terraform experiment plan seems to do what is expected. Before apply deleted offending trigger
`"pink-march-service-pollActivities-demo-pink-march-service-pollActivities-demo` from lambda
`arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities` `$LATEST` manually in console as
admin.

By doing that, we get 'pending deletion'. Refresh shows the trigger again though. Again. And again.

It is still not removed after half an hour.

After apply, we still see the error on `$LATEST`, and no trigger is added to `build-01058`. Apply worked, and a new
apply does not see relevant changes (there are false changes for `aws_lambda_event_source_mapping.stream-production`).

In the console, we do see the rate changed, and the correct target, exactly like the manual thing.

    jand@Desjani:main>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-demo
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-demo",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01058",
                "Input": "{\"mode\":\"demo\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }

In the console, we can also click "Fix". Let's try that next.

After "Fix", the error is gone, but the rule is now coupled to latest, but with "Matched event" as "input".

Delete manually in console (Lambda, \$LATEST, triggers). "Pending deletion".

[AWS docs](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/Delete-or-Disable-Rule.html):

> Amazon EventBridge is the preferred way to manage your events. CloudWatch Events and EventBridge are the same
> underlying service and API, but EventBridge provides more features. Changes you make in either CloudWatch or
> EventBridge will appear in each console. For more information, see Amazon EventBridge.

After ~ 3 minutes, still not deleted.

Let's delete the entire rule, using Terraform.

Note:

      # module.poll_activities-demo.aws_lambda_permission.poll_activities will be destroyed
      - resource "aws_lambda_permission" "poll_activities" {
          - action        = "lambda:InvokeFunction" -> null
          - function_name = "pink-march-service-pollActivities" -> null
          - id            = "terraform-20200402085132436700000002" -> null
          - principal     = "events.amazonaws.com" -> null
          - source_arn    = "arn:aws:events:eu-west-1:254473600415:rule/pink-march-service-pollActivities-demo" -> null
          - statement_id  = "terraform-20200402085132436700000002" -> null
        }

The `function_name` does not include aliases. We need a qualifier!

Deletion takes over 3 minutes already.

> Error: error deleting CloudWatch Event Rule (pink-march-service-pollActivities-demo): ValidationException: Rule can't
> be deleted since it has targets. status code: 400, request id: bb11b637-b968-4848-83cb-dd5354c3758e

Delete by hand in console.

One rule was deleted, the offending rule was not. And we cannot delete it via the console?

Delete of rule via console worked immediately.

The trigger is now also gone from lambda \$LATEST.

New apply to sync. No relevant changes.

Told you so:

      Error: Error adding new Lambda Permission for pink-march-service-pollActivities: InvalidParameterValueException: We currently do not support adding policies for $LATEST.
      {
        RespMetadata: {
          StatusCode: 400,
          RequestID: "20d3336d-1521-417a-bbfb-8e8d1a6d2dcb"
        },
        Message_: "We currently do not support adding policies for $LATEST.",
        Type: "User"
      }

Finally changed the permission, an now

- `$LATEST` has trigger for dev-experiment

```
    jand@Desjani:main>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-dev-experiment
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-dev-experiment",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities",
                "Input": "{\"mode\":\"dev-experiment\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }
```

- `build-01055` has trigger for acceptance

```
    jand@Desjani:main>aws events list-targets-by-rule --region eu-west-1 --profile pink-ribbon-belgium-admin --rule pink-march-service-pollActivities-acceptance
    {
        "Targets": [
            {
                "Id": "pink-march-service-pollActivities-acceptance-00057",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00057\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00058",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00058\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00059",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00059\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00060",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00060\",\"nrOfTrackerConnectionsToPoll\":10}"
            },
            {
                "Id": "pink-march-service-pollActivities-acceptance-00061",
                "Arn": "arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-pollActivities:build-01055",
                "Input": "{\"mode\":\"acceptance-00061\",\"nrOfTrackerConnectionsToPoll\":10}"
            }
        ]
    }
```

Now delete manual experiment by hand. Success.

Re-enable `poll_activities-demo` rule with Terraform on `build-01071`. Success.

While applying the fix to cron_updateRankings.tf:

    Error: Error adding new Lambda Permission for pink-march-service-updateRankings: ResourceNotFoundException: Cannot find alias arn: arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-updateRankings:build-00884
    {
      RespMetadata: {
        StatusCode: 404,
        RequestID: "947088e9-be7a-44d4-8f53-22cba4cbdb4e"
      },
      Message_: "Cannot find alias arn: arn:aws:lambda:eu-west-1:254473600415:function:pink-march-service-updateRankings:build-00884",
      Type: "User"
    }

And indeed, there is no 00884. Switching to `build-01071`. Success.

Verified in logs that lambda's are called every minute. Ok.

Now lower the rate to 1 hour for acceptance. Demo (and production) can stay at 1 minute.
