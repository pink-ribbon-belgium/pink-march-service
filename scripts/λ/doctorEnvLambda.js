#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const envLambda = require('../../claudia/env')
const jsonfile = require('jsonfile')
const path = require('path')

const envLambdaPath = path.join(__dirname, '..', '..', 'claudia', 'env.json')

const envVarNames = [
  // IDEA read config/custom-environment-variables.yaml to get all the env variables automatically
  'KMS_CMK_ARN',
  'OGONE_TEST_PSP_ID',
  'OGONE_TEST_SHA_IN',
  'OGONE_TEST_USERNAME',
  'OGONE_TEST_PASSWORD',
  'OGONE_PRODUCTION_PSP_ID',
  'OGONE_PRODUCTION_SHA_IN',
  'OGONE_PRODUCTION_USERNAME',
  'OGONE_PRODUCTION_PASSWORD',
  'SENDGRID_API_KEY',
  'FITBIT_DEV_CLIENT_ID',
  'FITBIT_DEV_CLIENT_SECRET',
  'FITBIT_PROD_CLIENT_ID',
  'FITBIT_PROD_CLIENT_SECRET',
  'GOOGLEFIT_DEV_CLIENT_ID',
  'GOOGLEFIT_DEV_CLIENT_SECRET',
  'GOOGLEFIT_PROD_CLIENT_ID',
  'GOOGLEFIT_PROD_CLIENT_SECRET',
  'POLAR_DEV_CLIENT_ID',
  'POLAR_DEV_CLIENT_SECRET',
  'POLAR_PRODUCTION_CLIENT_ID',
  'POLAR_PRODUCTION_CLIENT_SECRET',
  'POLAR_JUNE2020_CLIENT_ID',
  'POLAR_JUNE2020_CLIENT_SECRET',
  'POLAR_DEMO_CLIENT_ID',
  'POLAR_DEMO_CLIENT_SECRET',
  'REBRANDLY_DEV_API_KEY',
  'REBRANDLY_PROD_API_KEY'
]

async function doctor () {
  envVarNames.forEach(envVarName => {
    envLambda[envVarName] = process.env[envVarName]
  })

  await jsonfile.writeFile(envLambdaPath, envLambda)
}

envVarNames.forEach(envVarName => {
  if (!process.env[envVarName]) {
    console.error(`${envVarName} is mandatory`)
    process.exit(-1)
  }
})

doctor().catch(err => {
  console.error(err)
  process.exit(-1)
})
