#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const envLambda = require('../../claudia/env')
const config = require('config')
const jsonfile = require('jsonfile')
const path = require('path')

const envLambdaPath = path.join(__dirname, '..', '..', 'claudia', 'env.json')

async function doctor () {
  envLambda.KMS_CMK_ARN = config.kms.cmk
  envLambda.OGONE_TEST_PSP_ID = config.ogone.test.pspId
  envLambda.OGONE_TEST_SHA_IN = config.ogone.test.shaIn
  envLambda.OGONE_TEST_USERNAME = config.ogone.test.username
  envLambda.OGONE_TEST_PASSWORD = config.ogone.test.password
  envLambda.OGONE_PRODUCTION_PSP_ID = config.ogone.production.pspId
  envLambda.OGONE_PRODUCTION_SHA_IN = config.ogone.production.shaIn
  envLambda.OGONE_PRODUCTION_USERNAME = config.ogone.production.username
  envLambda.OGONE_PRODUCTION_PASSWORD = config.ogone.production.password
  envLambda.SENDGRID_API_KEY = config.email.sendgridApiKey
  envLambda.FITBIT_DEV_CLIENT_ID = config.trackers.fitbit.dev.clientId
  envLambda.FITBIT_DEV_CLIENT_SECRET = config.trackers.fitbit.dev.clientSecret
  envLambda.FITBIT_PROD_CLIENT_ID = config.trackers.fitbit.prod.clientId
  envLambda.FITBIT_PROD_CLIENT_SECRET = config.trackers.fitbit.prod.clientSecret
  envLambda.GOOGLEFIT_DEV_CLIENT_ID = config.trackers.googlefit.dev.clientId
  envLambda.GOOGLEFIT_DEV_CLIENT_SECRET = config.trackers.googlefit.dev.clientSecret
  envLambda.GOOGLEFIT_PROD_CLIENT_ID = config.trackers.googlefit.prod.clientId
  envLambda.GOOGLEFIT_PROD_CLIENT_SECRET = config.trackers.googlefit.prod.clientSecret
  envLambda.POLAR_DEV_CLIENT_ID = config.trackers.polar.dev.clientId
  envLambda.POLAR_DEV_CLIENT_SECRET = config.trackers.polar.clientSecret

  await jsonfile.writeFile(envLambdaPath, envLambda)
}

doctor().catch(err => {
  console.error(err)
  process.exit(-1)
})
