# pink-march-service λ and CD

In λ, you change the "latest" executable and configuration at will. When happy, you "publish" a state ("bank" a state).
This creates a version with consecutive numbers, which is immutable. You can create "aliases" that refer to a particular
version. Many aliases can refer to the same version, and the version an alias refers to can change.

## Continuous Delivery

The lambda is deployed on each successful build by CI, using [`deploy.sh`].

The script uses [claudia `update`] to

- build the clean package,
- do some validation,
- upload the package to S3,
- update the executable of the λ
- update the configuration of the λ
- publish the new combination of executable and configuration
- create an alias for the newly published combination with the CI build number

We intend never to change the build aliases. They allow us to cross-reference the git repository with the published
versions.

The execution role to be used by the λ is defined in the
[Terraform configuration](../../../terraform/main/role-lambda_execution.tf).

[`deploy.sh`] sets

- the runtime (`'nodejs12.x'`)
- the timeout (30s)
- the size of the VM (256MB)
- the environment variables of the version to the contents of [`claudia/env.json`](../../../claudia/env.json)

It deploys the executable to the S3 bucket `lambda.pink-ribbon-belgium.org` under the key
`'pink-march-service/${BUILD_NUMBER}.zip'`. This should never be overwritten (versioning is not enabled on the bucket).

The published version is referred to by an alias called `'build-${BUILD_NUMBER}'`

## Initial definition

Since all properties of a λ, except for the name and region, can be changed in the "latest" state, all properties of a
λ, except for the name, can differ between published versions. As a result, in principle, the base definition of a λ is
only a name, and the ARN derived from the name. It is an empty container.

The execution role is a property of the λ as a whole, and can be changed, but then changes for all versions.

Sadly, when you create a λ, you have to supply a first version of the executable and configuration. You cannot create an
"empty" λ. As a result, when defining a λ with [Terraform], you have to supply some executable and configuration, but
these will evolve constantly. Since a [Terraform] configuration declaratively describes a desired state, this goes out
of sync after the first interaction. The initial executable and configuration are not part of the desired state. With
[Terraform], it is the intention to change the configuration to reflect the new publication, define a resource for the
publication and alias, and apply that. This is overly complex, conflicts with CD practices, and brings no benefits.

Therefor, we do not define the λ "shell" with [Terraform]. There is no declarative definition, managed in IaC, for the
λ.

The λ is created "manually" using [claudia `create`] in [`create.sh`]. The script is considered the trace of what was
done. Sadly, [claudia `create`] does not support assuming a role, and we do not want to give devsecops general
principles to manage λs. Therefor, [`create.sh`] has to be executed by an administrator.

[`create.sh`] uses the AWS profile `pink-ribbon-belgium-admin`.

The name of the λ is `pink-march-service`. It creates the λ in region `eu-west-1`, and associates the execution role
`arn:aws:iam::<ACCOUNT_ID>:role/execution/pink-march-service/pink-march-service-lambda`.

It is irrelevant, but it initializes the λ with:

- the runtime (`'nodejs12.x'`)
- the timeout (30s)
- the size of the VM (256MB)
- the environment variables of the version to the contents of [`claudia/env.json`](../../../claudia/env.json)

It deploys the initial executable to the S3 bucket `lambda.pink-ribbon-belgium.org` under the key
`'pink-march-service/00000.zip'`.

[claudia `create`] does not support tagging.

[`deploy.sh`]: deploy.sh
[`create.sh`]: create.sh
[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[claudia `update`]: https://github.com/claudiajs/claudia/blob/master/docs/update.md
[claudia `create`]: https://github.com/claudiajs/claudia/blob/master/docs/create.md
