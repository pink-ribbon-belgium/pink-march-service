#!/usr/bin/env bash

# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/lic

# Note that npm warnings are emitted about indirect dev dependencies. Don't worry: they are not packaged. These warnings
# are irrelevant (warnings about regular dependencies _would_ be relevant!).

# error handling: see https://intoli.com/blog/exit-on-errors-in-bash-scripts/
# exit when any command fails
set -e

if [ -z "$BITBUCKET_BUILD_NUMBER" ]; then
    export BUILD_VERSION=00000
else
    export BUILD_VERSION=$(printf %05d "${BITBUCKET_BUILD_NUMBER}")
fi
echo "build number: ${BUILD_VERSION}"

if [ -z "$CI" ]; then
    echo "Not working in CI. Only CI can deploy."
    exit 1
else
    npm ci
    scripts/λ/doctorEnvLambda.js
    echo "Working in CI. Using AWS credentials in env."
    # Make claudia build and test in production mode
    export NODE_ENV=production
    export NODE_CONFIG_DIR=`node -e "console.log(path.join(process.cwd(), 'config'))"`
    echo "NODE_CONFIG_DIR: ${NODE_CONFIG_DIR}"
    npx claudia update --no-optional-dependencies --runtime nodejs12.x --timeout 45 --memory 256 --use-s3-bucket lambda.pink-ribbon-belgium.org --s3-key "pink-march-service/${BUILD_VERSION}.zip" --handler lib/service.handler --set-env-from-json claudia/env.json --version build-"${BUILD_VERSION}" --skip-iam --config claudia/service.json
fi
