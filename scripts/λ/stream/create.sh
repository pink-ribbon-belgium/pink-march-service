#!/usr/bin/env bash

# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/lic

scripts/λ/doctorEnvLambdaFromConfigLocal.js
npx claudia create --profile pink-ribbon-belgium-admin --no-optional-dependencies --runtime nodejs12.x --timeout 30 --memory 256 --use-s3-bucket lambda.pink-ribbon-belgium.org --s3-key "pink-march-service-stream/00000.zip" --set-env-from-json claudia/env.json --version build-"00000" --skip-iam --keep --handler lib/stream.handler --name "pink-march-service-stream" --role "arn:aws:iam::254473600415:role/execution/lambda-execution-pink-march-service" --region "eu-west-1" --config claudia/stream.json
npx claudia tag --profile pink-ribbon-belgium-admin --tags "name=Pink March Service,env=production,repo=$(git config --get remote.origin.url)" --config claudia/stream.json
