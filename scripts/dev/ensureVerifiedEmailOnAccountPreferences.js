#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const AccountPreferences = require('../../lib/api/account/AccountPreferencesDTO')
  .schema.tailor('dynamodb')
  .required()

const farFuture = '2222-03-17T13:04:57.321Z'
const accountPreferencesKeyPattern = /^\/[^/]+\/account\/[^/]+\/preferences$/

async function getAccount (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}`
  console.log(`        getting most recent version of Account with key ${key} …`)
  const result = await dynamodb
    .query({
      TableName: dynamodbTableName,
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':key': key,
        ':submitted': farFuture
      },
      KeyConditionExpression: '#key = :key and #submitted <= :submitted',
      Limit: 1,
      ScanIndexForward: false // true = ascending, false = descending
    })
    .promise()
  return result && result.Count && result.Items[0].data
}

async function writeAccountPreferences (dynamodb, dynamodbTableName, Item) {
  await dynamodb
    .put({
      TableName: dynamodbTableName,
      Item
    })
    .promise()
  console.log(`  ✅  account preferences {${Item.key}, ${Item.submitted}} updated`)
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'

  const { dynamodb } = await getDynamodb()
  console.info(`👀 scanning account preferences for mode ${mode} …`)
  let foundCount = 0
  let noUpdateNeededCount = 0
  let updateNeededCount = 0
  let canUpdateCount = 0
  let problemCount = 0
  let updatedCount = 0
  const dummyEmails = []
  let done = false
  let ExclusiveStartKey
  while (!done) {
    const result = await dynamodb
      .scan({
        TableName: dynamodbTableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':keyStart': `/${mode}/account/`
        },
        FilterExpression: 'begins_with (#key, :keyStart)',
        Select: 'ALL_ATTRIBUTES',
        ExclusiveStartKey,
        Limit: 1000
      })
      .promise()
    console.log(`👁 found ${result.Count} Account related Items for mode ${mode}`)
    // there are public profiles in the result too
    const accountPreferences = result.Items.filter(item => accountPreferencesKeyPattern.test(item.key))
    foundCount += accountPreferences.length
    console.log(`   of which ${accountPreferences.length} Account Preferences for mode ${mode}`)
    await Promise.all(
      accountPreferences.map(async Item => {
        const data = Item.data
        console.log(`  🦋️found {${Item.key}, ${Item.submitted}}`)
        console.log(`     structureVersion === ${data.structureVersion}`)
        console.log(`     verifiedEmail === ${JSON.stringify(data.verifiedEmail)}`)
        let needsUpdate = false
        let canUpdate = true
        if (data.structureVersion !== 2) {
          data.structureVersion = 2
          needsUpdate = true
          console.log('     🌀️structureVersion set to 2')
        }
        if (!data.verifiedEmail) {
          console.log('     👿️no verifiedEmail')
          needsUpdate = true
          const account = await getAccount(dynamodb, dynamodbTableName, mode, data.accountId)
          if (!account) {
            console.error(
              `  ⁉️could not find an account ${Item.data.accountId} for preferences {${Item.key}, ${Item.submitted}}`
            )
            canUpdate = false
          } else {
            canUpdate = true
            if (!account.email) {
              dummyEmails.push({ key: Item.key, submitted: Item.submitted, accountId: account.id })
              console.error(
                `  ⁉️account ${Item.data.accountId} for preferences {${Item.key}, ${Item.submitted}} has no email, using dummy`
              )
            } else {
              console.log(
                `  🐳 received account ${account.id} with email for preferences {${Item.key}, ${Item.submitted}}`
              )
            }
            data.verifiedEmail = {
              email: account.email || 'rozemars-dev@peopleware.be', // dummy email, for 4 cases in production
              isVerified: true // heuristic
            }
            console.log(`     🌀 email set to ${data.verifiedEmail.email}`)
          }
        }
        if (!needsUpdate) {
          noUpdateNeededCount++
          console.log(`  ✅  account preferences {${Item.key}, ${Item.submitted}} needs no update`)
        } else {
          updateNeededCount++
          const validation = AccountPreferences.validate(data)
          if (validation.error) {
            console.error(`  ⁉️validation failed for account preferences {${Item.key}, ${Item.submitted}}`)
            console.error(`    💀️ ${validation.error.annotate()}`)
            canUpdate = false
          }
          if (!canUpdate) {
            problemCount++
            console.error(
              `  ‼️account preferences {${Item.key}, ${Item.submitted}} needs update, but cannot be updated`
            )
          } else {
            canUpdateCount++
            if (!doWrite) {
              console.log(
                `  🛠 account preferences {${Item.key}, ${Item.submitted}} needs update, and can be updated; to write, run with \`--write\``
              )
            } else {
              console.log(
                `  🛠 account preferences {${Item.key}, ${Item.submitted}} needs update, and can be updated; writing …`
              )
              await writeAccountPreferences(dynamodb, dynamodbTableName, Item)
              updatedCount++
            }
          }
        }
      })
    )
    if (!result.LastEvaluatedKey) {
      console.log('👌 all done')
      done = true
    } else {
      ExclusiveStartKey = result.LastEvaluatedKey
      console.log('👁 there is more')
    }
  }
  console.log(`found ${foundCount} account preferences`)
  console.log(`  ${noUpdateNeededCount} did not need update`)
  console.log(`  ${updateNeededCount} needed update`)
  console.log(`  ${problemCount} could not be updated`)
  console.log(`  ${canUpdateCount} could be updated`)
  console.log(`  used dummy email on ${dummyEmails.length} account preferences`)
  console.log(`  ${updatedCount} successfully updated`)
  console.log('dummies:')
  dummyEmails.forEach(d => {
    console.log(`  ${JSON.stringify(d)}`)
  })
  if (!doWrite) {
    console.log('this was a dry run')
  }
}

modeScript(
  'ensureVerifiedEmailOnAccountPreferences',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

The script scans all Account Preferences Items (all versions, 'actual' if it
exists), and sets \`structureVersion\` to \`2\`. If there is no \`verifiedEmail\` on the
\`item.data\`, the corresponding latest version of \`Account\` is loaded, and the email there is
used if it exists. No new versions are created, and \`submitted\` is not changed.

Since this is a correction script that will be run when there are still few records, we consider
every found email in \`Account\` to be \`verified: true\`.

IF NO EMAIL IS FOUND IN EITHER Account Preferences NOR ACCOUNT, WE LOG A WARNING!. THIS DATA IS
CORRUPT.`
)
