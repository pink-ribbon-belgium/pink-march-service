/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4

async function execute (mode, dynamodbTableName) {
  const createShortLink = require('../../lib/server/business/group/shortLink/createShortLink')
  const getCompany = require('../../lib/server/business/company/get')
  const putCompany = require('../../lib/server/business/company/put')
  const getTeam = require('../../lib/server/business/team/get')
  const putTeam = require('../../lib/server/business/team/put')
  const GroupType = require('../../lib/api/group/GroupType')

  const sot = new Date().toISOString()
  const sub = 'generate-link-script'
  const flowId = uuidv4()

  // GroupType and GroupId have te be provided before executing the script
  const groupId = ''
  const groupType = ''

  let group
  let itemData
  let shortLinkResult
  if (groupId && groupType) {
    switch (groupType.toLowerCase()) {
      case GroupType.teamType.toLowerCase():
        group = await getTeam(sot, sub, mode, flowId, { teamId: groupId })
        try {
          if (group) {
            shortLinkResult = await createShortLink(sot, sub, mode, flowId, {
              joinLink: group.joinLink,
              groupId
            })
          }
          console.log('Generated shortLink ::::::::::', shortLinkResult)
          itemData = {
            createdAt: sot,
            createdBy: sub,
            structureVersion: 1,
            id: group.id,
            code: group.code,
            groupType: group.groupType,
            linkId: shortLinkResult.linkId,
            shortUrl: shortLinkResult.shortLink
          }
        } catch (err) {
          console.log('create shortLink failed', err)
          return
        }
        console.log('putTeam called with ::::', itemData)
        await putTeam(sot, sub, mode, flowId, { teamId: groupId }, itemData)
        break
      case GroupType.companyType.toLowerCase():
        group = await getCompany(sot, sub, mode, flowId, { companyId: groupId })
        try {
          if (group) {
            shortLinkResult = await createShortLink(sot, sub, mode, flowId, {
              joinLink: group.joinLink,
              groupId: groupId
            })
          }
          console.log('Generated shortLink ::::::::::', shortLinkResult)
          itemData = {
            ...group,
            linkId: shortLinkResult.linkId,
            shortUrl: shortLinkResult.shortLink
          }
        } catch (err) {
          console.log('create shortLink failed', err)
          return
        }
        delete itemData.joinLink
        delete itemData.links
        console.log('putCompany called with ::::', itemData)
        await putCompany(sot, sub, mode, flowId, { companyId: groupId }, itemData)
        break
      /* istanbul ignore next */
      default:
        break
    }
  } else {
    console.log('groupId and groupType have to be provided!!')
  }
}

modeScript(
  'generateLinkForGroup',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Generate shortLink for group.
`
)
