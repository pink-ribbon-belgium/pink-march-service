#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Mode = require('../../lib/ppwcode/Mode').schema.required()
const path = require('path')

/**
 * Generalized main function of scripts that require 1 mode parameter.
 * The script calls `execute` with the mode, and shows a usage message when
 * appropriate.
 */
async function modeScript (scriptName, execute, help) {
  /* `process.argv` is an array containing the command line arguments. The first element will be 'node', the second
    element will be the name of the JavaScript file. The next elements will be any additional command line arguments. */
  if (process.argv.includes('-h')) {
    console.log(help)
    process.exit(0)
  }
  const modeArg = process.argv[2]
  const modeValidation = Mode.validate(modeArg)
  if (modeValidation.error) {
    console.error(modeValidation.error.message)
    console.error(help)
    process.exit(-1)
  }
  const mode = modeValidation.value
  console.log(`mode: ${mode}`)

  // Make sure NODE_ENV is set according to the mode _before_ we load `config` ANYWHERE
  process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
  if (['production', 'demo', 'june2020'].includes(mode)) {
    process.env.NODE_ENV = 'production' // use production config
  } else {
    process.env.NODE_ENV = 'mocha' // use mocha config
  }
  const config = require('config')
  const dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')(mode) // requires config

  try {
    await execute(mode, dynamodbTableName, config)
  } catch (err) {
    console.error(`${scriptName} failed:`)
    console.error(err)
    process.exit(-1)
  }
}

module.exports = modeScript
