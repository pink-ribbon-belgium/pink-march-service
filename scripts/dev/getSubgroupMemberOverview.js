#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccountProfile (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/publicProfile`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey, indexName) {
  const index = {
    indexName: `Index_${indexName}`,
    partitionKey: `partition_key_${indexName}`,
    sortKey: `sort_key_${indexName}`
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 3000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getSubgroupsForCompany (dynamodb, dynamodbTableName, mode, companyId) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/subgroup/company/${companyId}`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getSubGroupMemberInfo (dynamodb, dynamodbTableName, mode, subgroup) {
  const indexPartitionKey = `/${mode}/group/${subgroup.data.groupId}/members`
  const companyMembers = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
  const memberSlots = companyMembers.filter(slot => slot.data.subgroupId === subgroup.data.id)

  const accountProfiles = await Promise.all(
    memberSlots.map(s => getAccountProfile(dynamodb, dynamodbTableName, mode, s.data.accountId))
  )

  return {
    name: subgroup.data.name,
    teamId: subgroup.data.id,
    members: accountProfiles.map(a => `${a.key} => ${a.data.firstName} ${a.data.lastName}`)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  // const companyId = '6e53bdea-c8a9-4874-893c-8561147c8c80'
  // const indexPartitionKey = `/${mode}/group/${companyId}/members`
  // const subgroupId = 'c5b45b43-46d5-4e76-a06d-0a44930cd30c'
  //

  const companyId = '6e53bdea-c8a9-4874-893c-8561147c8c80'
  const companySubGroups = await getSubgroupsForCompany(dynamodb, dynamodbTableName, mode, companyId)

  const subgroupInfo = await Promise.all(
    companySubGroups.map(s => getSubGroupMemberInfo(dynamodb, dynamodbTableName, mode, s))
  )

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    subgroups: subgroupInfo
  }

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('Company-Team member overview', statistics)
}

modeScript(
  'getSubgroupMemberOverview',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get the statistics about the slots (claimed vs free).
`
)
