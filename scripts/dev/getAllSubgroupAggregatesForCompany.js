#!/usr/bin/env node

/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const getSubgroupAggregates = require('../../lib/server/business/aggregates/team/getTeamAggregatesForCompany')

  const sot = new Date().toISOString()
  const flowId = uuidv4()
  const sub = 'delete-member-script'
  const companyId = ''

  try {
    const aggregates = await getSubgroupAggregates(sot, sub, mode, flowId, { companyId })
    logDetails('Aggregates', aggregates)
  } catch (e) {
    logDetails('GetAggregates failed', e)
  }
}

modeScript(
  'getSubgroupAggregates',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Manually get teamaggregates for subgroups.
`
)
