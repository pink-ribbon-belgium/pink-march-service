/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4
const util = require('util')

async function execute (mode, dynamodbTableName, config) {
  const getPolarData = require('../../lib/server/business/account/trackerConnection/polar/getPolarData')
  const getActual = require('../../lib/server/aws/dynamodb/getActual')

  // const accountId = '8aGXGTy7sZ'
  const accountId = 'IW8nnLtIPZ'
  const key = `/${mode}/account/${accountId}/trackerConnection`
  const actualTrackerConnection = await getActual(mode, key, true)

  const sot = new Date().toISOString()
  const sub = accountId
  const flowId = uuidv4()

  const startDate = new Date(config.activityPeriod.startDate)
  const endDate = new Date(config.activityPeriod.endDate)
  const response = await getPolarData(sot, sub, mode, flowId, actualTrackerConnection.data, startDate, endDate)

  console.log('Getting Polar activity data')
  console.log(`Tracker Data : ${util.inspect(response, false, null, true /* enable colors */)}`)
}

modeScript(
  'testPolarActivityData',
  execute,
  `
Run this script with argument \`<MODE>\`.
`
)
