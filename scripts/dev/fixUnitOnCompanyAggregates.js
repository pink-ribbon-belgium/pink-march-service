#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const GroupType = require('./../../lib/api/group/GroupType')

async function getExistingAggregates (dynamodb, dynamodbTableName, mode, groupType) {
  const indexPartitionKey = groupType === GroupType.teamType ? `/${mode}/teamAggregate` : `/${mode}/companyAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getCompanyOrTeam (dynamodb, dynamodbTableName, mode, groupId, groupType) {
  const key = `/${mode}/${groupType.toLowerCase()}/${groupId}`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function updateCompanyAggregates (dynamodb, dynamodbTableName, mode, existingAggregates) {
  const companies = await Promise.all(
    existingAggregates.map(a =>
      getCompanyOrTeam(dynamodb, dynamodbTableName, mode, a.data.companyId, GroupType.companyType)
    )
  )

  const companiesToProcess = companies.filter(c => c.data.unit)

  const dataForUpdata = companiesToProcess.map(a => {
    const aggregate = existingAggregates.filter(x => x.data.companyId === a.data.id)
    console.log(aggregate)
    return { key: aggregate[0].key, name: aggregate[0].data.name, unit: a.data.unit }
  })

  dataForUpdata.forEach(d => console.log(`${d.key} => ${d.name} => unit: ${d.unit}`))

  await Promise.all(dataForUpdata.map(c => updateAggregateWithUnit(dynamodb, dynamodbTableName, mode, c.key, c.unit)))
}

async function updateAggregateWithUnit (dynamodb, dynamodbTableName, mode, aggregateKey, newUnit) {
  const newPartitionKey2 = `/${mode}/companyAggregate/unit/${newUnit}`

  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#unit = :unit, #partition_key_2 = :partition_key_2',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#unit': 'unit',
      '#partition_key_2': 'partition_key_2'
    },
    ExpressionAttributeValues: {
      ':unit': newUnit,
      ':partition_key_2': newPartitionKey2
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    console.log(result)
  } catch (e) {
    console.log(e)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const existingCompanyAggregates = await getExistingAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    GroupType.companyType
  )

  await updateCompanyAggregates(dynamodb, dynamodbTableName, mode, existingCompanyAggregates)
}

modeScript(
  'statistics',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Verify steps from aggregates with real activities.
`
)
