/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const getDynamodb = require('./_getDynamodb')
const dynamodbHelper = require('./_dynamodbHelper')
const modeScript = require('./_modeScript')
const TeamAggregate = require('../../lib/server/business/aggregates/team/TeamAggregate')
const CompanyAggregate = require('../../lib/server/business/aggregates/company/CompanyAggregate')
const AggregateCalculations = require('../../lib/api/aggregates/calculation/AggregateCalculations')

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function getAggregates (dynamodb, dynamodbTableName, mode, aggregateType) {
  const indexPartitionKey = `/${mode}/${aggregateType}Aggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  return await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
}

async function getMembers (dynamodb, dynamodbTableName, mode, aggregateId) {
  const indexPartitionKey = `/${mode}/group/${aggregateId}/members`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
  return result.length
}

async function getSubgroupMembers (dynamodb, dynamodbTableName, mode, aggregate) {
  const indexPartitionKey = `/${mode}/group/${aggregate.data.companyId}/members`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const results = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
  console.log(':::::::::::::::::::::::::::')
  console.log(':::::::::::::::::::::::::::')
  console.log('subgroupId', aggregate.data.teamAggregateId)
  console.log(':::::::::::::::::::::::::::')
  console.log(results)
  console.log(':::::::::::::::::::::::::::')
  console.log(':::::::::::::::::::::::::::')

  const subgroupMembers = results.filter(slot => slot.data.subgroupId === aggregate.data.teamAggregateId)
  return subgroupMembers.length
}

async function updateTeamAggregates (dynamodb, dynamodbTableName, mode, aggregates, doWrite) {
  const updatedAggregates = []
  await Promise.all(
    aggregates.map(async aggregate => {
      let memberTotal
      if (!aggregate.data.companyId) {
        memberTotal = await getMembers(dynamodb, dynamodbTableName, mode, aggregate.data.teamAggregateId)
      } else {
        memberTotal = await getSubgroupMembers(dynamodb, dynamodbTableName, mode, aggregate)
      }
      console.log('Total# members', memberTotal)
      console.log('Initial TeamAggregate', aggregate)
      if (memberTotal > 0) {
        const averages = AggregateCalculations.calculateAverage(
          memberTotal,
          aggregate.data.totalSteps,
          aggregate.data.totalDistance
        )

        console.log(`Initial TeamAggregate: ${inspectForLog(aggregate)}`)

        const updatedAggregate = new TeamAggregate({
          mode: aggregate.mode,
          dto: {
            ...aggregate.data,
            averageSteps: averages.averageSteps,
            averageDistance: averages.averageDistance
          },
          sot: aggregate.data.createdAt,
          sub: aggregate.data.createdBy
        })

        const actualItem = {
          ...updatedAggregate.toItem(),
          submitted: 'actual',
          sort_key_1: updatedAggregate.averageSteps,
          sort_key_2: updatedAggregate.averageSteps
        }
        console.log('Updated TeamAggregate', actualItem)
        console.log(':::::::::::::::::::::::::::')

        updatedAggregates.push(actualItem)
      }
    })
  )

  if (doWrite) {
    await Promise.all(
      updatedAggregates.map(agg => {
        dynamodbHelper.writeToDatabase(dynamodb, dynamodbTableName, agg)
        console.log(`TeamAggregate updated => ${agg.key}`)
      })
    )
  } else {
    console.log('this was a dry run')
  }

  return updatedAggregates
}

async function updateCompanyAggregates (dynamodb, dynamodbTableName, mode, aggregates, doWrite) {
  const updatedAggregates = []
  await Promise.all(
    aggregates.map(async aggregate => {
      console.log('Initial CompanyAggregate', aggregate)
      const memberTotal = await getMembers(dynamodb, dynamodbTableName, mode, aggregate.data.companyId)
      if (memberTotal > 0) {
        const averages = AggregateCalculations.calculateAverage(
          memberTotal,
          aggregate.data.totalSteps,
          aggregate.data.totalDistance
        )
        console.log(':::::::::::::::::::::::::::')
        console.log('Totaal# members', memberTotal)
        console.log(':::::::::::::::::::::::::::')
        console.log('Averages', averages)
        console.log(':::::::::::::::::::::::::::')
        const updatedAggregate = new CompanyAggregate({
          mode: aggregate.mode,
          dto: {
            ...aggregate.data,
            averageSteps: averages.averageSteps,
            averageDistance: averages.averageDistance
          },
          sot: aggregate.data.createdAt,
          sub: aggregate.data.createdBy
        })

        const actualItem = {
          ...updatedAggregate.toItem(),
          submitted: 'actual',
          sort_key_1: updatedAggregate.averageSteps,
          sort_key_2: updatedAggregate.averageSteps
        }
        console.log('Updated CompanyAggregate', actualItem)
        console.log(':::::::::::::::::::::::::::')

        updatedAggregates.push(actualItem)
      } else {
        const notUpdatedAggregate = new CompanyAggregate({
          mode: aggregate.mode,
          dto: {
            ...aggregate.data,
            averageSteps: aggregate.data.totalSteps,
            averageDistance: aggregate.data.totalSteps
          },
          sot: aggregate.data.createdAt,
          sub: aggregate.data.createdBy
        })

        const actual = {
          ...notUpdatedAggregate.toItem(),
          submitted: 'actual',
          sort_key_1: notUpdatedAggregate.averageSteps,
          sort_key_2: notUpdatedAggregate.averageSteps
        }
        updatedAggregates.push(actual)
      }
    })
  )

  if (doWrite) {
    await Promise.all(
      updatedAggregates.map(agg => {
        dynamodbHelper.writeToDatabase(dynamodb, dynamodbTableName, agg)
        console.log(`CompanyAggregate updated => ${agg.key}`)
      })
    )
  } else {
    console.log('this was a dry run')
  }

  return updatedAggregates
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'

  const { dynamodb } = await getDynamodb()

  const teamAggregates = await getAggregates(dynamodb, dynamodbTableName, mode, 'team')
  const companyAggregates = await getAggregates(dynamodb, dynamodbTableName, mode, 'company')

  const updatedTeamAggregates = await updateTeamAggregates(dynamodb, dynamodbTableName, mode, teamAggregates, doWrite)
  const updatedCompanyAggregates = await updateCompanyAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    companyAggregates,
    doWrite
  )

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    aggregates_info: {
      totalTeamAggregates: teamAggregates.length,
      totalCompanyAggregates: companyAggregates.length,
      updatedTeamAggregates: updatedTeamAggregates.length,
      updatedCompanyAggregates: updatedCompanyAggregates.length
    }
  }

  console.log('')
  console.log('')
  console.log('RESULTS:')
  console.log('')
  console.log('===================================================================================')
  logDetails('Updated TeamAggregates', updatedTeamAggregates)
  console.log('===================================================================================')
  logDetails('Updated CompanyAggregates', updatedCompanyAggregates)
  console.log('')
  console.log('')
  logDetails('Statistics', statistics)
}

modeScript(
  'UpdateAverageStepsForAggregates',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Update average steps and average distance of existing Team-/CompanyAggregates
`
)
