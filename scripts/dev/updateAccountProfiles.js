#!/usr/bin/env node
/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Mode = require('../../lib/ppwcode/Mode').schema.required()
const getDynamoDB = require('./_getDynamodb')
const path = require('path')
const util = require('util')
const assert = require('assert')

const help = 'Run this script with argument `<MODE>`.'
const indexA = {
  indexName: 'Index_A',
  partitionKey: 'partition_key_A',
  sortKey: 'sort_key_A'
}
const accountKeyPattern = /^\/[^/]+\/account\/[^/]+$/
let dynamodbTableName
let config

if (process.argv.includes('-h')) {
  console.log(help)
  process.exit(0)
}
const modeArg = process.argv[2]
const modeValidation = Mode.validate(modeArg)
if (modeValidation.error) {
  console.error(modeValidation.error.message)
  console.error(help)
  process.exit(-1)
}
const mode = modeValidation.value
console.log(`mode: ${mode}`)

/**
 * Function that will take the entered mode
 * and returns the correct environment
 * @param mode demo or production
 */
function requiredCorrectConfig (mode) {
  if (!config) {
    process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
    if (['production', 'demo'].includes(mode)) {
      process.env.NODE_ENV = 'production'
    } else {
      process.env.NODE_ENV = 'mocha'
    }
    config = require('config')
    dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')
  }
}

/**
 * First get all de account ids from the Account Table
 *
 * @param mode environment demo or production
 * @returns {Set<string>} set of account item keys
 */
async function getAllAccountids (mode) {
  const { dynamodb } = await getDynamoDB()
  requiredCorrectConfig(mode)
  console.info(`getting account ids for ${mode} ...`)
  const data = new Set()
  let done = false
  let lastEvaluatedKey
  const TableName = dynamodbTableName(mode)
  while (!done) {
    const result = await dynamodb
      .scan({
        TableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': `/${mode}/account`
        },
        FilterExpression: 'begins_with(#key, :key)',
        Select: 'ALL_ATTRIBUTES',
        ExclusiveStartKey: lastEvaluatedKey
      })
      .promise()
    result.Items.filter(item => accountKeyPattern.test(item.key)).forEach(item => {
      console.log(`Found account with key ${item.key}. Adding to data.`)
      data.add(item.key)
    })
    if (!result.LastEvaluatedKey) {
      done = true
    } else {
      lastEvaluatedKey = result.LastEvaluatedKey
    }
  }
  console.log(`Found ${data.size} disctinct account keys`)
  return data
}

/**
 * Call `updateAccountProfilesFor` for all account keys in `accountKeys`.
 *
 * @param {string} mode - demo or production
 * @param {Set<string>} accountKeys - keys of the accounts to update the profile for
 * @returns
 */
async function updateAccountProfiles (mode, accountKeys) {
  return Promise.all(Array.from(accountKeys).map(accountKey => updateAccountProfilesFor(mode, accountKey)))
}

async function updateAccountProfilesFor (mode, accountKey) {
  const { dynamodb } = await getDynamoDB()
  requiredCorrectConfig(mode)
  const TableName = dynamodbTableName(mode)
  // MUDO: Refactor => Handle multiple results (see getByIndex)
  const profilesResult = await dynamodb
    .query({
      TableName,
      ExpressionAttributeNames: {
        '#key': 'key'
      },
      ExpressionAttributeValues: {
        ':key': `${accountKey}/publicProfile`
      },
      KeyConditionExpression: '#key = :key',
      ScanIndexForward: false // true = ascending, false = descending
    })
    .promise()
  const profileItems = profilesResult.Items
  console.log(`found ${profileItems.length} profile items for account key ${accountKey}`)
  if (profileItems.length > 0) {
    const hasActual = profileItems.some(profileItem => profileItem.submitted === 'actual')
    console.log(`profile items for account key ${accountKey} has an actual: ${hasActual}`)
    const profilesWithoutIndexA = profileItems.filter(profileItem => !profileItem[indexA.partitionKey])
    console.log(`found ${profilesWithoutIndexA.length} profile items for account key ${accountKey} without Index A`)
    const itemsToPut = profilesWithoutIndexA.map(profileItem => {
      const adaptedItem = { ...profileItem }
      adaptedItem[indexA.partitionKey] = `/${mode}/AccountProfile`
      adaptedItem[indexA.sortKey] = profileItem.submitted
      return { profileItem, adaptedItem }
    })
    if (!hasActual) {
      const mostRecentProfileItem = profileItems[0]
      assert(profileItems.every(profileItem => profileItem.submitted <= mostRecentProfileItem.submitted))
      const actualItem = {
        ...mostRecentProfileItem,
        submitted: 'actual',
        [indexA.partitionKey]: `/${mode}/AccountProfile`,
        [indexA.sortKey]: 'actual'
      }
      itemsToPut.push({ adaptedItem: actualItem })
    }
    return Promise.all(itemsToPut.map(itemToPut => write(dynamodb, TableName, itemToPut)))
  }
}

/**
 * @param dynamodb
 * @param dynamodbTableName
 * @param itemToPut
 * @returns {Promise<void>}
 */
async function write (dynamodb, dynamodbTableName, itemToPut) {
  const doWrite = process.argv[3] === '--write'
  if (doWrite) {
    await dynamodb
      .put({
        TableName: dynamodbTableName,
        Item: itemToPut.adaptedItem
      })
      .promise()
    console.log(
      `  ✅  account profile {${itemToPut.adaptedItem.data.accountId}, ${itemToPut.adaptedItem.submitted}} updated`
    )
  } else {
    console.log(
      `  ✅  account profile {${itemToPut.adaptedItem.data.accountId}, ${itemToPut.adaptedItem.submitted}} would be updated; to write, run with \`--write\``
    )
    console.log('would update:')
    console.log(util.inspect(itemToPut.profileItem, { depth: 999 }))
    console.log('to:')
    console.log(util.inspect(itemToPut.adaptedItem, { depth: 999 }))
  }
}

async function execute (mode) {
  const accountKeys = await getAllAccountids(mode)
  await updateAccountProfiles(mode, accountKeys)
}

execute(mode).catch(err => {
  console.error('update failed:')
  console.error(err)
  process.exit(-1)
})
