#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const moment = require('moment')

/***
 * Returns all slots for the given payment
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @returns {Promise<[]>}
 */
async function getSlotsForPayment (dynamodb, dynamodbTableName, mode, paymentId) {
  const indexPartitionKey = `/${mode}/payment/${paymentId}/slots`

  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @returns {Promise<[]>}
 */
async function getSuccessFullPayments (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/status/9/payments`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()
  const fs = require('fs')

  const doWrite = process.argv.some(x => x === '--write')

  const successFullPayments = await getSuccessFullPayments(dynamodb, dynamodbTableName, mode)

  let nrPaymentsVsSlotsNotOk = 0
  let nrPaymentsVsSlotsOk = 0

  const wrongPayments = []

  const paymentsWithSlots = await Promise.all(
    successFullPayments.map(async payment => {
      const slotsForGroup = await getSlotsForPayment(dynamodb, dynamodbTableName, mode, payment.data.paymentId)
      return {
        payment,
        nrOfSlots: slotsForGroup !== undefined ? slotsForGroup.length : 0,
        slots: slotsForGroup
      }
    })
  )

  paymentsWithSlots.forEach(item => {
    if (item.payment.data.numberOfSlots !== item.nrOfSlots) {
      nrPaymentsVsSlotsNotOk++
      wrongPayments.push(item)
    } else {
      nrPaymentsVsSlotsOk++
    }
  })

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    payments_info: {
      payment_vs_slots_ok: nrPaymentsVsSlotsOk,
      payment_vs_slots_nok: nrPaymentsVsSlotsNotOk,
      wrong_payments: wrongPayments
    }
  }

  if (doWrite) {
    const dateString = moment().format('YYYYMMDD')
    fs.writeFile(`checkPaymentVsSlots_${dateString}.json`, JSON.stringify(statistics, null, 2), err => {
      if (err) throw err
      console.log('Statistics written to file')
    })
  } else {
    console.log('')
    console.log('')
    console.log('===================================================================================')
    console.log('')
    console.log('RESULTS:')
    console.log('')

    logDetails('Slot Statistics', statistics)
  }
}

modeScript(
  'checkPaymentVsSlots',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get the statistics about the slots (claimed vs free).
`
)
