/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const dynamodbHelper = require('./_dynamodbHelper')
const _ = require('lodash')
const fs = require('fs')
const getDynamoDB = require('./_getDynamodb')
const ObjectsToCSV = require('objects-to-csv')
const moment = require('moment')
const path = require('path')
const Mode = require('../../lib/ppwcode/Mode').schema.required()
const help = `
Run this script with the argument \`<MODE>\`.`

if (process.argv.includes('-h')) {
  console.log(help)
  process.exit(0)
}
const modeArgs = process.argv[2]
const modeValidation = Mode.validate(modeArgs)
if (modeValidation.error) {
  console.error(modeValidation.error.message)
  console.error(help)
  process.exit(-1)
}
const mode = modeValidation.value
console.log(`mode: ${mode}`)

let config
let dynamodbTableName

/**
 * Make sure that the script uses the correct environment
 * which is given by the parameter mode at startup.
 * @param mode {string}
 */
function requireCorrectConfig (mode) {
  if (!config) {
    process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
    if (['production', 'demo', 'june2020'].includes(mode)) {
      process.env.NODE_ENV = 'production' // use production config
    } else {
      process.env.NODE_ENV = 'mocha' // use mocha config
    }
    config = require('config')
    dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')
  }
}

async function exportToCSV (result, fileName) {
  const dateString = moment().format('YYYYMMDD')
  const csv = new ObjectsToCSV(result)

  var outputFolder = './output'
  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  await csv.toDisk(`./output/${fileName}_${dateString}.csv`, { append: false })
}

const sot = new Date().toISOString()

async function getVersionAtOrBefore (mode, key) {
  const { dynamodb } = await getDynamoDB()

  // MUDO: Refactor => Handle multiple results (see getByIndex)

  const result = await dynamodb
    .query({
      TableName: dynamodbTableName(mode),
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':key': key,
        ':submitted': sot
      },
      KeyConditionExpression: '#key = :key and #submitted <= :submitted',
      Limit: 1,
      ScanIndexForward: false // true = ascending, false = descending
    })
    .promise()
  if (result.Count > 0) {
    return result.Items[0].data
  } else {
    return []
  }
}

async function getGroupProfile (mode, groupId) {
  const key = `/${mode}/group/${groupId}/publicProfile`
  return getVersionAtOrBefore(mode, key)
}

/**
 * Fetch all the successful payments
 * @param mode {string}
 * @returns {Promise<*[]>}
 */
async function getAllSuccessfulPayments (mode) {
  const { dynamodb } = await getDynamoDB()
  const myDynamoDbTableName = dynamodbTableName(mode)

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const sot = new Date().toISOString()

  const memberIndexPartitionKey = `/${mode}/status/9/payments`

  const params = {
    TableName: myDynamoDbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey,
      '#sortKey': index.sortKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': memberIndexPartitionKey,
      ':sortKey': sot
    },
    KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey <= :sortKey',
    ScanIndexForward: true,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, myDynamoDbTableName, mode, params)

  return result
}

let groupResults = []

async function collectCompanyOrTeamData (mode, successfulPayments) {
  groupResults = await Promise.all(
    successfulPayments.map(async item => {
      const groupProfile = await getGroupProfile(mode, item.data.groupId)
      return {
        payment_Id: item.data.paymentId,
        payment_initiated_at: item.data.paymentInitiatedAt,
        groupId: item.data.groupId,
        companyName: groupProfile.name,
        type: item.data.groupType,
        voucher_code: item.data.voucherCode,
        number_of_slots: item.data.numberOfSlots,
        amount: item.data.amount,
        payment_status: 'success'
      }
    })
  )
  return groupResults
}

async function execute (mode) {
  requireCorrectConfig(mode)
  console.info(`getting the successful payment for ${mode} ....`)
  const successfulPayments = await getAllSuccessfulPayments(mode)

  // Filter on unique paymentId because of duplicate payments (only payment_Initiated was updated)
  const uniquePayments = _.uniqBy(successfulPayments, 'data.paymentId')

  const finalResult = await collectCompanyOrTeamData(mode, uniquePayments)
  await exportToCSV(finalResult, 'Successful_Payments')
}

execute(mode).catch(err => {
  console.error('exportSuccessfulPayments failed: ')
  console.error(err)
  process.exit(-1)
})
