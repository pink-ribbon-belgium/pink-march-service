/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4
const util = require('util')
const moment = require('moment')

async function execute (mode, dynamodbTableName, config) {
  const getGoogleFitData = require('../../lib/server/business/account/trackerConnection/googlefit/getGoogleFitData')
  const getActual = require('../../lib/server/aws/dynamodb/getActual')

  // const accountId = 'ofJeL7Zr1I' // Peter June2020
  const accountId = 'TmqQ0YJJeM' // Peter local

  const key = `/${mode}/account/${accountId}/trackerConnection`
  const actualTrackerConnection = await getActual(mode, key, true)

  const sot = new Date().toISOString()
  const sub = accountId
  const flowId = uuidv4()

  // const startDate = new Date('2020-06-02')
  // const endDate = new Date('2020-06-06')

  let startDate
  let endDate

  /* istanbul ignore next : we can't test different modes */
  switch (mode) {
    case 'dev-experiment':
    case 'demo':
    case 'june2020':
    case 'production':
      startDate = new Date(config.activityPeriod[mode].startdate)
      endDate = new Date(config.activityPeriod[mode].enddate)
      break
    default:
      startDate = new Date(config.activityPeriod.default.startdate)
      endDate = new Date(config.activityPeriod.default.enddate)
      break
  }

  const response = await getGoogleFitData(sot, sub, mode, flowId, actualTrackerConnection.data, startDate, endDate)

  console.log()
  console.log()
  console.log(
    `Getting Google Fit activity data from ${moment(startDate).format('YYYY-MM-DD')} - ${moment(endDate).format(
      'YYYY-MM-DD'
    )}`
  )
  console.log(`Tracker Data : ${util.inspect(response, false, null, true /* enable colors */)}`)
}

modeScript(
  'testGetGoogleFitActivityData',
  execute,
  `
Run this script with argument \`<MODE>\`.
`
)
