#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const should = require('should')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const Slot = require('../../lib/server/business/slot/Slot')

const updatedSlots = []
const noUpdateNeededSlots = []
const failedUpdateSlots = []

const groupSlotOverview = []

/****
 * Scan all the groupAdminstrations in order to find all groupId's
 * @param dynamodb  reference to dynamoDB
 * @param dynamodbTableName name of the table to scan
 * @param mode  mode to use
 * @returns {Promise<*>}  Array of groupId's
 */
async function getAllGroupInfo (dynamodb, dynamodbTableName, mode) {
  console.info(`👀 scanning GroupAdministrations for mode ${mode} …`)

  const finalResult = new Set()
  let done = false

  let exclusiveStartKeyValue

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted',
      '#mode': 'mode'
    },
    ExpressionAttributeValues: {
      ':key': `/${mode}/groupAdministration/account`,
      ':submitted': 'actual',
      ':mode': mode
    },
    FilterExpression: 'begins_with(#key, :key) and #submitted = :submitted and #mode = :mode',
    Select: 'ALL_ATTRIBUTES',
    ExclusiveStartKey: undefined,
    ScanIndexForward: true, // true = ascending, false = descending
    Limit: 1000
  }

  while (!done) {
    params.ExclusiveStartKey = exclusiveStartKeyValue

    const result = await dynamodb.scan(params).promise()
    // console.log(`👁 found ${result.Count} GroupAdministration related Items for mode ${mode}`)

    result.Items.forEach(item => {
      finalResult.add(item)
    })

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      exclusiveStartKeyValue = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }
  // console.log(`Found ${finalResult.length} distinct GroupAdministrations keys`)

  const result = Array.from(finalResult).map(i => {
    return { groupId: i.data.groupId, groupType: i.data.groupType }
  })

  return result
}

async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey) {
  let done = false

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  let finalResult = []

  while (!done) {
    const result = await dynamodb.query(params).promise()

    finalResult = finalResult.concat(result.Items.map(item => item))

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }

  return finalResult
}

async function getAllSlotsForGroup (dynamodb, dynamodbTableName, mode, groupInfo) {
  const memberIndexPartitionKey = `/${mode}/group/${groupInfo.groupId}/members`
  const freeIndexPartitionKey = `/${mode}/group/${groupInfo.groupId}/free`

  const [takenSlots, availableSlots] = await Promise.allSettled([
    getSlots(dynamodb, dynamodbTableName, mode, memberIndexPartitionKey),
    getSlots(dynamodb, dynamodbTableName, mode, freeIndexPartitionKey)
  ])

  if (takenSlots.status !== 'fulfilled' && takenSlots.reason.message !== 'Not Found') {
    console.log(`getByIndex takenslots [${memberIndexPartitionKey}] not fulfilled =>  : ${inspectForLog(takenSlots)}`)
  }

  if (availableSlots.status !== 'fulfilled' && availableSlots.reason.message !== 'Not Found') {
    console.log(
      `getByIndex availableSlots [${freeIndexPartitionKey}] not fulfilled => : ${inspectForLog(availableSlots)}`
    )
  }

  const foundTakenSlots = takenSlots.status === 'fulfilled' ? takenSlots.value : []
  const foundFreeSlots = availableSlots.status === 'fulfilled' ? availableSlots.value : []
  const slotForGroup = foundTakenSlots.concat(foundFreeSlots)

  groupSlotOverview.push({
    groupId: groupInfo.groupId,
    takenSlots: foundTakenSlots.length,
    freeSlots: foundFreeSlots.length
  })

  if (slotForGroup.length > 0) {
    const result = slotForGroup.map(slot => {
      return { groupInfo, slot }
    })
    return result
  }
}

async function updateSlot (dynamodb, dynamodbTableName, mode, slotWithGroupInfo, doWrite) {
  const slot = slotWithGroupInfo.slot
  const originalSlot = { ...slot }

  // Set structureVersion to 2 if not & set grouptype
  if (slot.data.structureVersion < 2) {
    slot.data.groupType = slotWithGroupInfo.groupInfo.groupType
    slot.data.structureVersion = 2
  }

  const tempSlotData = new Slot({
    mode: slot.mode,
    dto: {
      ...slot.data
    },
    sot: slot.data.createdAt,
    sub: slot.data.createdBy
  })

  try {
    const tempSlot = tempSlotData.toItem()

    // clean undefined fields
    Object.keys(tempSlot.data).forEach(key => (tempSlot.data[key] === undefined ? delete tempSlot.data[key] : {}))

    try {
      should(tempSlot.data).be.deepEqual(slot.data)
    } catch (e) {
      console.log(`tempSlotItem.data & slot.data are not equal ==> ${slot.key} => ${e}`)
      console.log('---------------------------------------------------------------------------------')
      console.log(`tempSlotItem Data :${inspectForLog(tempSlot)}`)
      console.log()
      console.log(`slot: ${inspectForLog(slot)}`)
      console.log('---------------------------------------------------------------------------------')
      throw e
    }

    if (slot.submitted === 'actual') {
      slot.data.structureVersion = 2
      slot.data.groupType = slotWithGroupInfo.groupInfo.groupType

      slot.partition_key_A = tempSlot.partition_key_A
      slot.partition_key_B = tempSlot.partition_key_B
      slot.partition_key_C = tempSlot.partition_key_C
      slot.partition_key_D = tempSlot.partition_key_D
      slot.partition_key_E = tempSlot.partition_key_E

      slot.sort_key_A = 'actual'
      slot.sort_key_B = 'actual'
      slot.sort_key_C = slot.partition_key_C ? 'actual' : undefined
      slot.sort_key_D = 'actual'
      slot.sort_key_E = 'actual'
    } else {
      slot.partition_key_A = undefined
      slot.partition_key_B = undefined
      slot.partition_key_C = undefined
      slot.partition_key_D = undefined
      slot.partition_key_E = undefined

      slot.sort_key_A = undefined
      slot.sort_key_B = undefined
      slot.sort_key_C = undefined
      slot.sort_key_D = undefined
      slot.sort_key_E = undefined
    }

    try {
      // cleanup undefined fields
      Object.keys(slot).forEach(key => (slot[key] === undefined ? delete slot[key] : {}))

      // If original & updated are
      should(originalSlot).be.deepEqual(slot)

      console.log(`No changes on slot ==> ${slot.key}`)
      noUpdateNeededSlots.push(slot)
    } catch (e) {
      // do the actual update
      if (doWrite) {
        await writeToDatabase(dynamodb, dynamodbTableName, slot)
        console.log(`slot updated => ${slot.key}`)
      }

      updatedSlots.push(slot)

      console.log(`Slot before update :${inspectForLog(originalSlot)}`)
      console.log()
      console.log(`Updated slot: ${inspectForLog(slot)}`)
      console.log('---------------------------------------------------------------------------------')
    }
  } catch (e) {
    console.log(`Something failed while trying to update the slot ==> ${slot.key} => ${e}`)
    failedUpdateSlots.push(slot)
  }
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

async function writeToDatabase (dynamodb, dynamodbTableName, Item) {
  await dynamodb
    .put({
      TableName: dynamodbTableName,
      Item
    })
    .promise()
  console.log(`  ✅  GroupAdministration {${Item.key}, ${Item.submitted}} updated`)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'
  const { dynamodb } = await getDynamodb()

  // 1) Get all existing groupId's
  const allGroupInfo = await getAllGroupInfo(dynamodb, dynamodbTableName, mode)

  // 2) Get all existing slots for the groups
  const existingSlotsWithGroupInfo = (
    await Promise.all(allGroupInfo.map(groupInfo => getAllSlotsForGroup(dynamodb, dynamodbTableName, mode, groupInfo)))
  )
    .filter(x => x !== undefined)
    .flat()

  // 3) Update the slots
  await Promise.all(
    existingSlotsWithGroupInfo.map(slotWithGroupInfo =>
      updateSlot(dynamodb, dynamodbTableName, mode, slotWithGroupInfo, doWrite)
    )
  )

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  groupSlotOverview.sort((a, b) => (a.groupId > b.groupId ? 1 : -1))
  // logDetails('Overview Group Slots', groupSlotOverview)

  const existingActualSlots = existingSlotsWithGroupInfo.filter(
    slotWithGroupInfo => slotWithGroupInfo.slot.submitted === 'actual'
  )

  // submitted slots will only be returned if there are slots from before this update process
  // new submitted won't have index partition_key's
  const existingSubmittedSlots = existingSlotsWithGroupInfo.filter(
    slotWithGroupInfo => slotWithGroupInfo.slot.submitted !== 'actual'
  )

  // logDetails('Slots No update Needed', noUpdateNeededSlots)
  logDetails('Slots Updated', updatedSlots)

  // logDetails(
  //   'Actual Claimd Slots',
  //   existingActualSlots.filter(slot => slot.slot.data.accountId)
  // )

  const statistics = {
    mode: mode,
    found_groups: allGroupInfo.length,
    found_slots: existingSlotsWithGroupInfo.length,
    actual_slot_info: {
      slot_payed: existingActualSlots.length,
      slot_claimed: existingActualSlots.filter(slot => slot.slot.data.accountId).length,
      slot_free: existingActualSlots.filter(slot => !slot.slot.data.accountId).length
    },
    submitted_slot_info: {
      total: existingSubmittedSlots.length
    },
    update_info: {
      failed_updates_Slots: failedUpdateSlots.length,
      no_update_needed_Slots: noUpdateNeededSlots.length,
      updated_Slots: updatedSlots.length
    }
  }

  logDetails('Statistics', statistics)

  if (!doWrite) {
    console.log('this was a dry run')
  }
}

modeScript(
  'fixIndexesOnSlot',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Fix indexes on Slot if not already exists
The script scans all Slot Items
Set all partitionkeys on \`Slot\` correct and sets \`structureVersion\` to \`2\`
No new versions are created, and \`submitted\` is not changed.
`
)
