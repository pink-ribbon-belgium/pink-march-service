/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

async function deleteItem (dynamodb, dynamodbTableName, item) {
  await dynamodb
    .delete({
      TableName: dynamodbTableName,
      Key: {
        key: item.key,
        submitted: item.submitted
      }
    })
    .promise()
  console.log(`  ✅  Item {${item.key}, ${item.submitted}} deleted`)
}

async function writeToDatabase (dynamodb, dynamodbTableName, item) {
  await dynamodb
    .put({
      TableName: dynamodbTableName,
      Item: item
    })
    .promise()
  console.log(`  ✅  Item {${item.key}, ${item.submitted}} updated`)
}

async function queryAll (dynamodb, dynamodbTableName, mode, params) {
  let done = false

  let finalResult = []

  while (!done) {
    const result = await dynamodb.query(params).promise()

    finalResult = finalResult.concat(result.Items.map(item => item))

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }

  return finalResult
}

async function queryOne (dynamodb, dynamodbTableName, mode, params) {
  let done = false

  const finalResult = []

  while (!done) {
    const result = await dynamodb.query(params).promise()

    if (result.Items.length > 0) {
      return result.Items[0]
    }

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }

  return finalResult
}

async function queryActualByKey (dynamodb, dynamodbTableName, mode, key) {
  let done = false

  const finalResult = []

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': 'actual'
    },
    KeyConditionExpression: '#key = :key and #submitted = :submitted',
    Limit: 1,
    ScanIndexForward: false,
    ConsistentRead: true
  }

  while (!done) {
    const result = await dynamodb.query(params).promise()

    if (result.Items.length > 0) {
      return result.Items[0]
    }

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }

  return finalResult
}

module.exports = { queryAll, queryOne, queryActualByKey, writeToDatabase, deleteItem }
