/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const getDynamodb = require('./_getDynamodb')
const dynamodbHelper = require('./_dynamodbHelper')
const AccountAggregate = require('../../lib/server/business/aggregates/account/AccountAggregate')
const modeScript = require('./_modeScript')
const activityCalculations = require('../../lib/api/activity/ActivityCalculations')

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function getAccountAggregates (dynamodb, dynamodbTableName, mode) {
  const indexPartitionKey = `/${mode}/accountAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  return await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
}

async function updateAccountAggregates (dynamodb, dynamodbTableName, mode, aggregates, doWrite) {
  const updatedAggregates = []

  aggregates.map(aggregate => {
    console.log(`Initial AccountAggregate: ${inspectForLog(aggregate)}`)

    const updatedAggregate = new AccountAggregate({
      mode: aggregate.mode,
      dto: {
        ...aggregate.data,
        previousLevel: activityCalculations.calculateLevelFromSteps(aggregate.data.totalSteps, 1),
        acknowledged: false
      },
      sot: aggregate.data.createdAt,
      sub: aggregate.data.createdBy
    })

    const actualItem = {
      ...updatedAggregate.toItem(),
      submitted: 'actual',
      sort_key_1: aggregate.data.totalSteps,
      sort_key_2: aggregate.data.totalSteps,
      sort_key_3: aggregate.data.totalSteps // Should always be filled in
    }
    console.log('Updated AccountAggregate', actualItem)
    console.log(':::::::::::::::::::::::::::')

    updatedAggregates.push(actualItem)
  })
  if (doWrite) {
    await Promise.all(
      updatedAggregates.map(agg => {
        dynamodbHelper.writeToDatabase(dynamodb, dynamodbTableName, agg)
        console.log(`AccountAggregate updated => ${agg.key}`)
      })
    )
  } else {
    console.log('this was a dry run')
  }

  return updatedAggregates
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'

  const { dynamodb } = await getDynamodb()

  const aggregates = await getAccountAggregates(dynamodb, dynamodbTableName, mode)
  const updateResults = await updateAccountAggregates(dynamodb, dynamodbTableName, mode, aggregates, doWrite)

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    account_aggregate_info: {
      total: aggregates.length,
      updated: updateResults.length
    }
  }

  console.log('')
  console.log('')
  console.log('RESULTS:')
  console.log('')
  console.log('===================================================================================')
  logDetails('Updated', updateResults)
  console.log('')
  console.log('')
  logDetails('Statistics', statistics)
}

modeScript(
  'addNewPropertiesToAccountAggregates',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Update existing AccountAggregates
`
)
