#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

// const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4

async function execute (mode, dynamodbTableName) {
  // const { dynamodb } = await getDynamodb()
  const processPayment = require('./../../lib/server/business/payment/processPayment')

  const sot = new Date().toISOString()
  const sub = 'verifyNonProcessedPayments'
  const flowId = uuidv4()

  await processPayment(sot, sub, mode, flowId, {
    // accountId: '3qqdUYLAjO',
    // paymentId: 'd8dac2fb-d2ef-4bd5-a8ae-ebc78840f771'
  })
}

modeScript(
  'verifySinglePayment',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Verify a single payment.
`
)
