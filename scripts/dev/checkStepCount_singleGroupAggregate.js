#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const GroupType = require('./../../lib/api/group/GroupType')
// const TrackerType = require('./../../lib/api/account/trackerConnection/TrackerType')
// const AggregateCalculations = require('./../../lib/api/aggregates/calculation/AggregateCalculations')

const scriptName = 'checkStepCount_singleGroupAggregate'

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

function logHeader (scriptName, mode, doUpdate) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log(`Execute ${scriptName}`)
  console.log(`MODE: ${mode}`)
  console.log('')
  if (doUpdate) {
    console.log('===> WARNING : All changes will be written to the database !!!!')
  } else {
    console.log('===> This is a dry-run !')
  }
  console.log('')
  console.log('')
}

function writeToFile (mode, fileName, result) {
  var fs = require('fs')

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  const filePath = `output/${fileName}_${mode}.json`
  fs.writeFile(filePath, JSON.stringify(result, null, 2), err => {
    if (err) throw err
    console.log('')
    console.log('Data written to file: ' + filePath)
  })
}

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @param indexName => A/B/C/D/E
 * @returns {Promise<[]>}
 */
async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey, indexName) {
  const index = {
    indexName: `Index_${indexName}`,
    partitionKey: `partition_key_${indexName}`,
    sortKey: `sort_key_${indexName}`
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 3000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getActivitiesForAccount (dynamodb, dynamodbTableName, mode, accountId) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/activity/account/${accountId}`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 2000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function verifyTotalSteps (dynamodb, dynamodbTableName, mode, existingAggregate) {
  const existingAggregateInfo = existingAggregate

  let aggregateType = ''
  if (existingAggregateInfo.key.includes('companyAggregate')) {
    aggregateType = 'CompanyAggregate'
  } else {
    if (existingAggregateInfo.key.includes('teamAggregate')) {
      if (existingAggregateInfo.data.groupType === GroupType.companyType) {
        aggregateType = 'SubGroupAggregate'
      } else {
        aggregateType = 'TeamAggregate'
      }
    }
  }

  let indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
  let memberSlots = []

  switch (aggregateType) {
    case 'CompanyAggregate':
      indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
      memberSlots = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
      break
    case 'TeamAggregate':
      indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.teamAggregateId}/members`
      memberSlots = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
      break
    case 'SubGroupAggregate':
      indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
      // eslint-disable-next-line no-case-declarations
      const companyMembers = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
      memberSlots = companyMembers.filter(s => s.data.subgroupId === existingAggregateInfo.data.teamAggregateId)
      break
  }

  const tempActivitiesMembers = await Promise.all(
    memberSlots.map(slot => getActivitiesForAccount(dynamodb, dynamodbTableName, mode, slot.data.accountId))
  )

  const combinedMemberActivities = tempActivitiesMembers.flat()

  const totalStepsActivities = combinedMemberActivities.reduce((partialSum, a) => partialSum + a.data.numberOfSteps, 0)

  const totalDistanceActivities = combinedMemberActivities.reduce((partialSum, a) => partialSum + a.data.distance, 0)

  const verifiedAggregate = {
    mode: mode,
    date: new Date().toISOString(),
    aggregateInfo: {
      key: existingAggregateInfo.key,
      name:
        existingAggregateInfo.key.includes('teamAggregate') &&
        existingAggregateInfo.data.groupType === GroupType.companyType
          ? existingAggregateInfo.data.extendedName
          : existingAggregateInfo.data.name,
      finalResult: existingAggregateInfo.data.totalSteps === totalStepsActivities ? 'OK' : 'FAILED',
      type: aggregateType,
      memberCount: memberSlots.length,
      deviation: totalStepsActivities - existingAggregateInfo.data.totalSteps
    },
    totalstepsInfo: {
      from_Aggregate: existingAggregateInfo.data.totalSteps,
      calculated_by_activities: totalStepsActivities
    },
    totalDistanceInfo: {
      from_Aggregate: existingAggregateInfo.data.totalDistance,
      calculated_by_activities: totalDistanceActivities
    },
    activityInfo: combinedMemberActivities.map(
      a =>
        `${a.data.accountId} => ${a.data.activityDate} => ${a.key} => ${a.data.trackerType} => ${a.data.numberOfSteps}`
    )
  }

  return verifiedAggregate
}

async function getAggregate (dynamodb, dynamodbTableName, mode, key) {
  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function updateAggregate (dynamodb, dynamodbTableName, mode, aggregateKey, totalSteps, totalDistance) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#totalSteps = :totalSteps, #data.#totalDistance = :totalDistance',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance'
    },
    ExpressionAttributeValues: {
      ':totalSteps': totalSteps,
      ':totalDistance': totalDistance
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    //    console.log(result)
    return result
  } catch (e) {
    console.log(e)
  }
}

async function processAggregates (dynamodb, dynamodbTableName, mode, aggregateKey, doUpdate, doWriteToOutput) {
  const aggregateBefore = await getAggregate(dynamodb, dynamodbTableName, mode, aggregateKey)

  const verifiedAggregate = await verifyTotalSteps(dynamodb, dynamodbTableName, mode, aggregateBefore)

  let updateResults
  let aggregateAfterUpdate
  if (doUpdate) {
    updateResults = await updateAggregate(
      dynamodb,
      dynamodbTableName,
      mode,
      verifiedAggregate.aggregateInfo.key,
      verifiedAggregate.totalstepsInfo.calculated_by_activities,
      verifiedAggregate.totalDistanceInfo.calculated_by_activities
    )

    aggregateAfterUpdate = await getAggregate(dynamodb, dynamodbTableName, mode, aggregateKey)
  }

  const summary = {
    mode: mode,
    date: new Date().toISOString(),
    result: verifiedAggregate,
    aggregate_before: aggregateBefore,
    aggregate_after: aggregateAfterUpdate,
    updateResults: updateResults
  }

  if (doWriteToOutput) {
    writeToFile(mode, `${scriptName}_${aggregateBefore.data.name}`, summary)
  } else {
    logDetails('Aggregate Summary', summary)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWriteToOutput = process.argv.some(x => x === '--output')
  const doUpdate = process.argv.some(x => x === '--write')

  logHeader(scriptName, mode, doUpdate)

  const aggregateKey = '/production/teamAggregate/28b61299-b929-4840-b56f-a24a8134ecd6'

  await processAggregates(dynamodb, dynamodbTableName, mode, aggregateKey, doUpdate, doWriteToOutput)
}

modeScript(
  'checkStepCount_singleGroupAggregate',
  execute,
  `
Run this script with argument \`<MODE>\`.
Purpose: Verify steps of Team & Company Aggregates with real activities.

Parameters:
    \`--output\`    write the output to a file
    \`--write\`     update the aggregates in the database

The script does a dry run, unless \`--write\` is given as an argument.
`
)
