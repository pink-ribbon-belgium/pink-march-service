#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccount (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getGroupProfile (dynamodb, dynamodbTableName, mode, groupId) {
  const key = `/${mode}/group/${groupId}/publicProfile`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getGroupAdministration (dynamodb, dynamodbTableName, mode, accountId, groupId) {
  const key = `/${mode}/groupAdministration/account/${accountId}/group/${groupId}`
  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getAccountPreferences (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/preferences`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getAccountProfile (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/publicProfile`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getCompanyOrTeam (dynamodb, dynamodbTableName, mode, groupId, groupType) {
  const key = `/${mode}/${groupType.toLowerCase()}/${groupId}`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getSlotForAccount (dynamodb, dynamodbTableName, mode, accountId) {
  const index = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }

  const indexPartitionKey = `/${mode}/account/${accountId}/slot`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getSlotsForGroup (dynamodb, dynamodbTableName, mode, groupId) {
  const memberIndexPartitionKey = `/${mode}/group/${groupId}/members`
  const freeIndexPartitionKey = `/${mode}/group/${groupId}/free`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const paramsMembers = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': memberIndexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const paramsFree = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': freeIndexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const memberSlots = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, paramsMembers)
  const freeSlots = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, paramsFree)

  return memberSlots.concat(freeSlots)
}

async function getAdministratorOf (dynamodb, dynamodbTableName, mode, accountId) {
  const indexPartitionKey = `/${mode}/account/${accountId}/administratorOf`

  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getPayments (dynamodb, dynamodbTableName, mode, groupId, groupType) {
  const index = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }

  const indexPartitionKey = `/${mode}/${groupType.toLowerCase()}/${groupId}/payments`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccountAggregate (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/accountAggregate/${accountId}`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getTrackerConnection (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/trackerConnection`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getTeamAggregate (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/teamAggregate/${accountId}`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getCompanyAggregate (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/companyAggregate/${accountId}`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getActivities (dynamodb, dynamodbTableName, mode, accountId) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/activity/account/${accountId}`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getSubgroupsForCompany (dynamodb, dynamodbTableName, mode, companyId) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/subgroup/company/${companyId}`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function collectUserData (dynamodb, dynamodbTableName, mode, accountId) {
  const [
    account,
    accountPreferences,
    accountProfile,
    administratorOf,
    slot,
    accountAggregate,
    trackerConnection,
    activities
  ] = await Promise.all([
    getAccount(dynamodb, dynamodbTableName, mode, accountId),
    getAccountPreferences(dynamodb, dynamodbTableName, mode, accountId),
    getAccountProfile(dynamodb, dynamodbTableName, mode, accountId),
    getAdministratorOf(dynamodb, dynamodbTableName, mode, accountId),
    getSlotForAccount(dynamodb, dynamodbTableName, mode, accountId),
    getAccountAggregate(dynamodb, dynamodbTableName, mode, accountId),
    getTrackerConnection(dynamodb, dynamodbTableName, mode, accountId),
    getActivities(dynamodb, dynamodbTableName, mode, accountId)
  ])

  let groupType
  let groupId
  let subgroupId

  if (slot.length > 0) {
    groupType = slot[0].data.groupType
    groupId = slot[0].data.groupId
    subgroupId = slot[0].data.subgroupId
  } else if (administratorOf.length > 0) {
    groupType = administratorOf[0].data.groupType
    groupId = administratorOf[0].data.groupId
  }

  const subGroupAggregate = await getTeamAggregate(dynamodb, dynamodbTableName, mode, subgroupId)

  const userInfo = {
    mode: mode,
    date: new Date().toISOString(),
    accountInfo: {
      account: account,
      accountPreferences,
      accountProfile
    },
    groupInfo: { group: undefined, groupProfile: undefined },
    isAdminstrator: administratorOf !== undefined && administratorOf.length > 0,
    groupAdministration: undefined,
    payments: undefined,
    trackerConnection: trackerConnection,
    activities: activities,
    aggregates: {
      accountAggregate: accountAggregate,
      teamAggreagate: undefined,
      companyAggreagate: undefined,
      subGroupAggregate: subGroupAggregate
    },
    slot_user: slot
  }

  if (groupId !== undefined) {
    const [
      groupProfile,
      groupAdministration,
      payments,
      teamOrCompany,
      subGroups,
      slotForGroup,
      teamAggregate,
      companyAggregate
    ] = await Promise.all([
      getGroupProfile(dynamodb, dynamodbTableName, mode, groupId),
      getGroupAdministration(dynamodb, dynamodbTableName, mode, accountId, groupId),
      getPayments(dynamodb, dynamodbTableName, mode, groupId, groupType),
      getCompanyOrTeam(dynamodb, dynamodbTableName, mode, groupId, groupType),
      getSubgroupsForCompany(dynamodb, dynamodbTableName, mode, groupId),
      getSlotsForGroup(dynamodb, dynamodbTableName, mode, groupId),
      getTeamAggregate(dynamodb, dynamodbTableName, mode, groupId),
      getCompanyAggregate(dynamodb, dynamodbTableName, mode, groupId)
    ])

    if (groupProfile && groupProfile.data.logo) {
      groupProfile.data.logo = '[HAS LOGO]'
    }

    if (subGroups && subGroups.length > 0) {
      subGroups.forEach(g => {
        if (g.data.logo) {
          g.data.logo = '[HAS LOGO]'
        }
      })
    }

    const slotPreferences = await Promise.all(
      slotForGroup
        .filter(x => x.data.accountId !== undefined)
        .map(async s => {
          const slotpreferences = await getAccountPreferences(dynamodb, dynamodbTableName, mode, s.data.accountId)
          const account = await getAccount(dynamodb, dynamodbTableName, mode, s.data.accountId)

          return `${s.data.accountId} => ${
            slotpreferences !== undefined ? slotpreferences.data.verifiedEmail.email : '??'
          } => ${account.data.sub}`
        })
    )

    const successfullPayments = payments.filter(p => p.data.paymentStatus === '9')

    userInfo.groupInfo.group = teamOrCompany
    userInfo.groupInfo.groupProfile = groupProfile
    userInfo.groupInfo.subGroup_for_user =
      subGroups.length > 0 ? subGroups.filter(x => x.data.id === subgroupId) : undefined
    userInfo.groupInfo.subGroups = subGroups
    userInfo.groupAdministration = groupAdministration
    userInfo.payments = {
      successfull_payments: successfullPayments.map(
        p => `${p.data.paymentInitiatedAt} => ${p.data.paymentId} => ${p.data.numberOfSlots} => ${p.data.paymentStatus}`
      ),
      payments
    }
    userInfo.slots_for_group = {
      total_slots: slotForGroup.length,
      member_slots: slotForGroup.filter(s => s.partition_key_E === '/production/slots/members').length,
      free_slots: slotForGroup.filter(s => s.partition_key_E === '/production/slots/free').length,
      slots_with_email: slotPreferences,
      slots: slotForGroup
    }

    userInfo.trackerConnection = trackerConnection
    userInfo.aggregates.accountAggregate = accountAggregate
    userInfo.aggregates.teamAggreagete = teamAggregate
    userInfo.aggregates.companyAggreagete = companyAggregate
  }

  return userInfo
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()
  const fs = require('fs')

  const doWrite = process.argv.some(x => x === '--write')

  const accountId = 'UEAzSIQ2Wk'
  // const accountId = 'fvogBa1LEm'

  const userInfo = await collectUserData(dynamodb, dynamodbTableName, mode, accountId)
  if (doWrite) {
    var outputFolder = './output'

    if (!fs.existsSync(outputFolder)) {
      fs.mkdirSync(outputFolder)
    }

    var userDataFolder = `${outputFolder}/userinfo`

    if (!fs.existsSync(userDataFolder)) {
      fs.mkdirSync(userDataFolder)
    }

    let firstName
    let lastName
    if (userInfo.accountInfo.accountProfile !== undefined && userInfo.accountInfo.accountProfile.data !== undefined) {
      firstName = userInfo.accountInfo.accountProfile.data.firstName
      lastName = userInfo.accountInfo.accountProfile.data.lastName
    } else {
      if (userInfo.isAdminstrator === true && userInfo.groupInfo.group.data.groupType === 'Company') {
        firstName = userInfo.groupInfo.group.contactFirstName
        lastName = userInfo.groupInfo.group.contactLastName
      }
    }

    const filePath = `${userDataFolder}/userInfo_${accountId}_${firstName}_${lastName}.json`

    fs.writeFile(filePath, JSON.stringify(userInfo, null, 2), err => {
      if (err) throw err
      console.log('UserInfo written to file')
    })
  }

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('Statistics', userInfo)
}

modeScript(
  'userInfo',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get the statistics about the slots (claimed vs free).

The script writes the output to a file, if \`--write\` is given as an argument.
`
)
