#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')
const path = require('path')
process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
process.env.NODE_ENV = 'mocha' // use mocha config
const config = require('config')

const region = 'eu-west-1'
const profile = 'pink-ribbon-belgium-dev'
const rolePathAndName = 'automated-test/automated-test-pink-march-service'

AWS.config.apiVersions = {
  dynamodb: '2012-08-10'
}
AWS.config.update({ region })
AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile })
console.log(`using profile '${profile}' (see \`~/.aws/config\`) ; working in region '${region}'`)

async function clean (inclDevExperiment, dryRun) {
  console.log('retrieving AWS user data …')
  const iam = new AWS.IAM()
  const userData = await iam.getUser({}).promise()
  const accountId = userData.User.Arn.split(':')[4]
  console.info('AWS user data retrieved, account id determined') // do not log accountId
  console.info(`getting credentials for role '${rolePathAndName}' …`)
  const sts = new AWS.STS()
  const stsId = uuidv4()
  const roleSessionName = `pink-march-service-${stsId}`
  const stsData = await sts
    .assumeRole({
      RoleArn: `arn:aws:iam::${accountId}:role/${rolePathAndName}`,
      RoleSessionName: roleSessionName
    })
    .promise()
  console.info(`AWS credentials for role '${rolePathAndName}' received (session name: '${roleSessionName}'). Assuming.`)
  AWS.config.update({
    accessKeyId: stsData.Credentials.AccessKeyId,
    secretAccessKey: stsData.Credentials.SecretAccessKey,
    sessionToken: stsData.Credentials.SessionToken,
    region
  })
  const dynamodb = new AWS.DynamoDB.DocumentClient()
  console.info('👀 starting scan …')
  let counter = 0
  let done = false

  let params
  if (inclDevExperiment === true) {
    params = {
      TableName: config.dynamodb.test.tableName,
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':automatedTest': '/automated-test-',
        ':qa': '/qa-',
        ':acceptance': '/acceptance-',
        ':devExperiment': '/dev-experiment'
      },
      // do not delete production or demo
      // AttributeValueList: ['/automated-test-' /*, '/qa-', '/acceptance-', '/dev-experiment'*/]
      FilterExpression:
        'begins_with (#key, :automatedTest) or begins_with (#key, :qa) or begins_with (#key, :acceptance) or begins_with (#key, :devExperiment)',
      Select: 'SPECIFIC_ATTRIBUTES',
      ProjectionExpression: '#key, #submitted',
      Limit: 1000,
      ExclusiveStartKey: undefined
    }
  } else {
    params = {
      TableName: config.dynamodb.test.tableName,
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':automatedTest': '/automated-test-',
        ':qa': '/qa-',
        ':acceptance': '/acceptance-'
      },
      // do not delete production or demo
      // AttributeValueList: ['/automated-test-' /*, '/qa-', '/acceptance-', '/dev-experiment'*/]
      FilterExpression:
        'begins_with (#key, :automatedTest) or begins_with (#key, :qa) or begins_with (#key, :acceptance)',
      Select: 'SPECIFIC_ATTRIBUTES',
      ProjectionExpression: '#key, #submitted',
      Limit: 1000,
      ExclusiveStartKey: undefined
    }
  }

  while (!done) {
    const result = await dynamodb.scan(params).promise()
    counter += result.Count
    console.log(`👁 found ${result.Count} matches (${counter})`)

    if (dryRun === false) {
      await Promise.all(
        result.Items.map(async Key => {
          console.log(`  💀️ deleting {${Key.key}, ${Key.submitted}} …`)
          await dynamodb
            .delete({
              TableName: config.dynamodb.test.tableName,
              Key
            })
            .promise()
          console.log(`  ⚰️ {${Key.key}, ${Key.submitted}} deleted`)
        })
      )
    }

    if (!result.LastEvaluatedKey) {
      console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      console.log('👁 there was more')
    }
  }
  console.log(`deleted ${counter} items`)
}

async function execute () {
  /* `process.argv` is an array containing the command line arguments. The first element will be 'node', the second
   element will be the name of the JavaScript file. The next elements will be any additional command line arguments. */
  if (process.argv.includes('-h')) {
    console.log(help)
    process.exit(0)
  }

  const inclDevExperiment = process.argv.includes('--include-dev-experiment')
  const dryrun = process.argv.includes('--dryrun')

  if (dryrun === true) {
    console.log('WARNING ITEMS WILL BE DELETED')
  }

  clean(inclDevExperiment, dryrun).catch(err => {
    console.error('clean failed:')
    console.error(err)
    process.exit(-1)
  })
}

const help = `Run this script with these arguments:
\`--include-dev-experiment\` => also clean dev-experiment
\`--dryrun\` => The script does a dry run, no items will be deleted.
`

execute()
