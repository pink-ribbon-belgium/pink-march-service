#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const should = require('should')
const getDynamodb = require('./_getDynamodb')
const dynamodbHelper = require('./_dynamodbHelper')
const modeScript = require('./_modeScript')
const AccountAggregate = require('../../lib/server/business/aggregates/account/AccountAggregate')

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function getAccountAggregates (dynamodb, dynamodbTableName, mode) {
  const indexPartitionKey = `/${mode}/accountAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const results = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return results
}

async function updateAccountAggregates (dynamodb, dynamodbTableName, mode, aggregates, doWrite) {
  const updatedAggregates = []
  const notUpdatedAggregates = []
  const failedUpdates = []

  // Remove partition- & sortkey 3 if subgroupId = undefined
  aggregates.forEach(aggregate => {
    const originalAggregate = { ...aggregate }

    const tempAggregateData = new AccountAggregate({
      mode: aggregate.mode,
      dto: {
        ...aggregate.data
      },
      sot: aggregate.data.createdAt,
      sub: aggregate.data.createdBy
    })

    try {
      const tempAggregate = tempAggregateData.toItem()
      // clean undefined fields
      Object.keys(tempAggregate.data).forEach(key =>
        tempAggregate.data[key] === undefined ? delete tempAggregate.data[key] : {}
      )

      aggregate.partition_key_1 = tempAggregate.partition_key_1
      aggregate.partition_key_2 = tempAggregate.partition_key_2
      aggregate.partition_key_3 = tempAggregate.partition_key_3
      aggregate.sort_key_1 = tempAggregate.partition_key_1 ? tempAggregate.data.totalSteps : undefined
      aggregate.sort_key_2 = tempAggregate.partition_key_2 ? tempAggregate.data.totalSteps : undefined
      aggregate.sort_key_3 = tempAggregate.partition_key_3 ? tempAggregate.data.totalSteps : undefined

      try {
        // cleanup undefined fields
        Object.keys(aggregate).forEach(key => (aggregate[key] === undefined ? delete aggregate[key] : {}))

        // Check if original & updated are equal
        should(originalAggregate).be.deepEqual(aggregate)

        console.log(`No changes on aggregate ==> ${aggregate.key}`)
        notUpdatedAggregates.push(aggregate)
      } catch (e) {
        updatedAggregates.push(aggregate)

        console.log(`Before Update: ${inspectForLog(originalAggregate)}`)
        console.log()
        console.log(`AFTER Updated: ${inspectForLog(aggregate)}`)
        console.log('---------------------------------------------------------------------------------')

        console.log(`Aggregate has changes ==> ${aggregate.key}`)
      }
    } catch (error) {
      console.log(`Something failed while trying to update the AccountAggregate ==> ${aggregate.key}`)
      failedUpdates.push({ aggregate, error })
    }
  })

  if (doWrite) {
    await Promise.all(
      updatedAggregates.map(agg => {
        dynamodbHelper.writeToDatabase(dynamodb, dynamodbTableName, agg)
        console.log(`AccountAggregate updated => ${agg.key}`)
      })
    )
  } else {
    console.log('this was a dry run')
  }

  return { updatedAggregates, notUpdatedAggregates, failedUpdates }
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'

  const { dynamodb } = await getDynamodb()

  const aggregates = await getAccountAggregates(dynamodb, dynamodbTableName, mode)

  const updateResults = await updateAccountAggregates(dynamodb, dynamodbTableName, mode, aggregates, doWrite)

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    account_aggregate_info: {
      total: aggregates.length,
      updated: updateResults.updatedAggregates.length,
      no_update_needed: updateResults.notUpdatedAggregates.length,
      failedUpdates: updateResults.failedUpdates.length
    }
  }

  console.log('')
  console.log('')
  console.log('RESULTS:')
  console.log('')
  console.log('===================================================================================')
  //  logDetails('Updated', updateResults.updatedAggregates)
  //  logDetails('NOT Updated', updateResults.notUpdatedAggregates)
  logDetails('FAILED updates', updateResults.failedUpdates)
  console.log('')
  console.log('')
  logDetails('Statistics', statistics)
}

modeScript(
  'fixIndexesOnAccountAggregates',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Update indexes of existing AccountAggregates
`
)
