/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const AWS = require('aws-sdk')
const { v4: uuidv4 } = require('uuid')

const region = 'eu-west-1'
const profile = 'pink-ribbon-belgium-dev'
const rolePathAndName = 'automated-test/automated-test-pink-march-service'

let dynamodb

async function getDynamodb () {
  if (!dynamodb) {
    AWS.config.apiVersions = {
      dynamodb: '2012-08-10'
    }
    AWS.config.update({ region })
    AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile })
    console.log(`using profile '${profile}' (see \`~/.aws/config\`) ; working in region '${region}'`)
    console.log('retrieving AWS user data …')
    const iam = new AWS.IAM()
    const userData = await iam.getUser({}).promise()
    const accountId = userData.User.Arn.split(':')[4]
    const userIdentification = `iam!${userData.User.Path}${userData.User.UserName}!${userData.User.UserId}`.replace(
      /\//g,
      '~'
    )
    console.info(`AWS user data retrieved, account id determined; user: ${userIdentification}`) // do not log accountId
    console.info(`getting credentials for role '${rolePathAndName}' …`)
    const sts = new AWS.STS()
    const stsId = uuidv4()
    const roleSessionName = `pink-march-service-${stsId}`
    const stsData = await sts
      .assumeRole({
        RoleArn: `arn:aws:iam::${accountId}:role/${rolePathAndName}`,
        RoleSessionName: roleSessionName
      })
      .promise()
    console.info(
      `AWS credentials for role '${rolePathAndName}' received (session name: '${roleSessionName}'). Assuming.`
    )
    AWS.config.update({
      accessKeyId: stsData.Credentials.AccessKeyId,
      secretAccessKey: stsData.Credentials.SecretAccessKey,
      sessionToken: stsData.Credentials.SessionToken,
      region
    })
    dynamodb = { dynamodb: new AWS.DynamoDB.DocumentClient(), userIdentification }
  }
  return dynamodb
}

module.exports = getDynamodb
