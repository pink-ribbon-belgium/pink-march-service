#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const fs = require('fs')
const ObjectsToCSV = require('objects-to-csv')
const moment = require('moment')

const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

async function getAllActivities (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const indexPartitionKey = `/${mode}/activity`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunkSize {Integer} Size of every group
 */
function chunkArray (myArray, chunkSize) {
  let index = 0
  const arrayLength = myArray.length
  const tempArray = []

  let myChunk
  for (index = 0; index < arrayLength; index += chunkSize) {
    myChunk = myArray.slice(index, index + chunkSize)
    // Do something if you want with the group
    tempArray.push(myChunk)
  }

  return tempArray
}

/**
 * Append the result to a Csv file
 * @param result {Array}
 * @param fileName {string}
 * @returns {Promise<void>}
 */
function appendToCsv (result, fileName) {
  const csv = new ObjectsToCSV(result)

  var outputFolder = './output'
  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  csv.toDisk(`./output/${fileName}.csv`, { append: true })
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  console.info(`getting Activity data for ${mode} ...`)

  const activityData = await getAllActivities(dynamodb, dynamodbTableName, mode)

  console.log(`Number of activities => ${activityData.length}`)

  const finalResult = activityData.map(a => {
    return {
      accountId: a.data.accountId,
      date: a.data.activityDate,
      steps: a.data.numberOfSteps,
      distance: a.data.distance,
      type: a.data.trackerType
    }
  })

  const chunkedArray = chunkArray(finalResult, 30000)

  const dateString = moment().format('YYYYMMDDhhmm')

  const fileName = `ActivityData_${mode}_${dateString}`

  chunkedArray.map(arr => appendToCsv(arr, fileName))

  console.log('Export Activity Data finished')
}

modeScript(
  'exportActivityData',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get the statistics about the slots (claimed vs free).
`
)
