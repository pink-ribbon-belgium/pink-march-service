#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @returns {Promise<[]>}
 */
async function getLeastRecentUpdateConnections (dynamodb, dynamodbTableName, mode, sot, nrOfTrackerConnectionsToPoll) {
  const key = `/${mode}/trackerConnection`

  const indexA = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: indexA.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': indexA.partitionKey,
      '#sortKey': indexA.sortKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': key,
      ':sortKey': sot
    },
    KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey <= :sortKey',
    ScanIndexForward: true,
    Limit: nrOfTrackerConnectionsToPoll
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const numberOfConnectionsToPoll = 100
  const sot = new Date().toISOString()

  const result = await getLeastRecentUpdateConnections(
    dynamodb,
    dynamodbTableName,
    mode,
    sot,
    numberOfConnectionsToPoll
  )

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    least_recent_trackerConnections: result.map(
      r => `${r.key} => ${r.data.trackerType} => ${r.data.hasError} => ${r.sort_key_A}`
    )
  }

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('Least Recent TrackerConnections', statistics)
}

modeScript(
  'statistics',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get Least Recent TrackerConnections.
`
)
