#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const sendGridMail = require('@sendgrid/mail')
const path = require('path')
process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
const config = require('config')

const getEmailTemplate = require('../../lib/server/business/payment/getEmailTemplate')

async function sendMail (mailTemplate) {
  const msg = {
    to: 'peter_marien@peopleware.be',
    from: {
      name: mailTemplate.fromname,
      email: 'peter_marien@peopleware.be'
    },
    subject: mailTemplate.subject,
    html: mailTemplate.body
  }

  sendGridMail.setApiKey(config.email.sendgridApiKey)

  try {
    await sendGridMail.send(msg)
  } catch (err) {
    /* istanbul ignore next */
    console.error(`An error occured while trying to send a confirmation email : ${err.toString()}`)
  }
}

async function testMailTemplates () {
  const individualNl = await getEmailTemplate('confirmation_individual', 'NL')
  await sendMail(individualNl)

  const individualFr = await getEmailTemplate('confirmation_individual', 'FR')
  await sendMail(individualFr)

  const teamNl = await getEmailTemplate('confirmation_team', 'NL')
  await sendMail(teamNl)

  const teamFr = await getEmailTemplate('confirmation_team', 'FR')
  await sendMail(teamFr)

  const companyNl = await getEmailTemplate('confirmation_company', 'NL')
  await sendMail(companyNl)

  const companyFr = await getEmailTemplate('confirmation_company', 'FR')
  await sendMail(companyFr)
}

testMailTemplates().catch(err => {
  console.error('test mailtemplates failed:')
  console.error(err)
  process.exit(-1)
})
