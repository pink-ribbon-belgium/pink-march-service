#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4
const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const TrackerType = require('./../../lib/api/account/trackerConnection/TrackerType')

async function getTrackerConnectionsByType (dynamodbTableName, mode, trackerType) {
  const { dynamodb } = await getDynamodb()

  const indexPartitionKey = `/${mode}/trackerConnection/${trackerType}`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

async function deleteTrackerConnections (mode, trackerConnections, trackerType) {
  const deleteTracker = require('./../../lib/server/business/account/trackerConnection/delete')
  const sot = new Date().toISOString()
  const flowId = uuidv4()
  const connectionsToDelete = trackerConnections.slice(0, 200)

  connectionsToDelete.map(async connection => {
    await deleteTracker(sot, connection.data.accountId, mode, flowId, { accountId: connection.data.accountId })
  })
}

async function execute (mode, dynamodbTableName) {
  const trackerType = TrackerType.googleType

  const trackerConnections = await getTrackerConnectionsByType(dynamodbTableName, mode, trackerType)
  logDetails('Connections', trackerConnections)
  console.log('#Connections to delete ::::: ', trackerConnections.length)
  console.log()
  console.log()
  if (trackerConnections.length) {
    await deleteTrackerConnections(mode, trackerConnections, trackerType)
  }
}

modeScript(
  'testGetFitbitTrackerData',
  execute,
  `
Run this script with argument \`<MODE>\`.
`
)
