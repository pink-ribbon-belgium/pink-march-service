#!/usr/bin/env node

/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const getDynamodb = require('./_getDynamodb')
const util = require('util')
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4
const Slot = require('../../lib/server/business/slot/Slot')
const AccountProfile = require('../../lib/server/business/account/profile/AccountProfile')
const AccountAggregate = require('../../lib/server/business/aggregates/account/AccountAggregate')
const TeamAggregate = require('../../lib/server/business/aggregates/team/TeamAggregate')
const Activity = require('../../lib/server/business/activity/Activity')
const dynamoDbHelper = require('./_dynamodbHelper')
//
// const slotKeys = {
//   indexName: 'Index_B',
//   partitionKey: 'partition_key_B',
//   sortKey: 'sort_key_B'
// }

const slotItems = []
const profileItems = []
const accountAggregateItems = []
const activityItems = []
let groupAggregateItem
const accountIds = ['eric-id', 'fons-id', 'carlo-id']

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

async function createSlots (mode, flowId, dynamodb, dynamodbTableName, slotData) {
  console.log('::::::::: Create Slots ::::::::::')
  await Promise.all(
    slotData.map(async i => {
      const slot = new Slot({
        mode: mode,
        dto: {
          ...i
        },
        sot: i.createdAt,
        sub: i.createdBy
      })

      const databaseItem = slot.toItem()
      databaseItem.flowId = flowId
      databaseItem.submitted = 'actual'
      databaseItem.sort_key_A = 'actual'
      databaseItem.sort_key_B = 'actual'
      databaseItem.sort_key_C = 'actual'
      databaseItem.sort_key_D = 'actual'
      databaseItem.sort_key_E = 'actual'

      await dynamoDbHelper.writeToDatabase(dynamodb, dynamodbTableName, databaseItem)
      slotItems.push(databaseItem)
    })
  )
}

async function createAccountProfiles (mode, flowId, dynamodb, dynamodbTableName, profileData) {
  console.log('::::::::: Create AccountProfiles ::::::::::')
  await Promise.all(
    profileData.map(async i => {
      const profile = new AccountProfile({
        mode: mode,
        dto: {
          ...i
        },
        sot: i.createdAt,
        sub: i.createdBy
      })

      const databaseItem = profile.toItem()
      databaseItem.flowId = flowId
      databaseItem.submitted = 'actual'
      databaseItem.sort_key_A = 'actual'

      await dynamoDbHelper.writeToDatabase(dynamodb, dynamodbTableName, databaseItem)
      profileItems.push(databaseItem)
    })
  )
}

async function createAggregates (mode, flowId, dynamodb, dynamodbTableName, aggregateData) {
  console.log('::::::::: Create AccountAggregates ::::::::::')
  await Promise.all(
    aggregateData.map(async i => {
      const accountAggregate = new AccountAggregate({
        mode: mode,
        dto: {
          ...i
        },
        sot: i.createdAt,
        sub: i.createdBy
      })

      const databaseItem = accountAggregate.toItem()
      databaseItem.flowId = flowId
      databaseItem.submitted = 'actual'
      databaseItem.sort_key_1 = accountAggregate.totalSteps
      databaseItem.sort_key_2 = accountAggregate.totalSteps
      databaseItem.sort_key_3 = accountAggregate.totalSteps

      await dynamoDbHelper.writeToDatabase(dynamodb, dynamodbTableName, databaseItem)
      accountAggregateItems.push(databaseItem)
    })
  )
}

async function createActivities (mode, flowId, dynamodb, dynamodbTableName, activityData) {
  console.log('::::::::: Create Activities ::::::::::')
  await Promise.all(
    activityData.map(async i => {
      const datePart = i.activityDate.slice(0, 10)
      const activity = new Activity({
        mode: mode,
        dto: {
          ...i
        },
        sot: i.createdAt,
        sub: i.createdBy
      })

      const databaseItem = activity.toItem()
      databaseItem.flowId = flowId
      databaseItem.submitted = 'actual'
      databaseItem.sort_key_A = datePart
      databaseItem.sort_key_B = datePart

      await dynamoDbHelper.writeToDatabase(dynamodb, dynamodbTableName, databaseItem)
      activityItems.push(databaseItem)
    })
  )
}

async function createGroupAggregate (mode, flowId, dynamodb, dynamodbTableName, groupAggregateData) {
  console.log('::::::::: Create GroupAggregate ::::::::::')

  const teamAggregate = new TeamAggregate({
    mode: mode,
    dto: {
      ...groupAggregateData
    },
    sot: groupAggregateData.createdAt,
    sub: groupAggregateData.createdBy
  })
  const databaseItem = teamAggregate.toItem()
  databaseItem.flowId = flowId
  databaseItem.submitted = 'actual'
  databaseItem.sort_key_1 = teamAggregate.averageSteps
  databaseItem.sort_key_2 = teamAggregate.averageSteps

  await dynamoDbHelper.writeToDatabase(dynamodb, dynamodbTableName, databaseItem)
  groupAggregateItem = databaseItem
}

async function createAccounts (sot, sub, mode, flowId, dynamodb, dynamodbTableName, groupId) {
  const accountId1 = accountIds[0]
  const accountId2 = accountIds[1]
  const accountId3 = accountIds[2]
  const teamName = 'De stappertjes'

  const paymentId = uuidv4()

  const baseProfile = {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 1,
    gender: 'M',
    dateOfBirth: '1984-01-23T15:22:39.212Z',
    zip: '4500'
  }

  const profilesToCreate = [
    { ...baseProfile, accountId: accountId1, firstName: 'Eric', lastName: 'id' },
    { ...baseProfile, accountId: accountId2, firstName: 'Fons', lastName: 'id' },
    { ...baseProfile, accountId: accountId3, firstName: 'Carlo', lastName: 'id' }
  ]

  await createAccountProfiles(mode, flowId, dynamodb, dynamodbTableName, profilesToCreate)

  const baseSlot = {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 2,
    groupId,
    groupType: 'Team',
    paymentId
  }

  const slotsToCreate = [
    { ...baseSlot, id: uuidv4() },
    { ...baseSlot, id: uuidv4(), accountId: accountId1 },
    { ...baseSlot, id: uuidv4(), accountId: accountId2 },
    { ...baseSlot, id: uuidv4(), accountId: accountId3 }
  ]

  await createSlots(mode, flowId, dynamodb, dynamodbTableName, slotsToCreate)
  console.log('SlotItems :::::::::::::::::::::', slotItems.length)

  const baseAccountAggregate = {
    acknowledged: false,
    createdAt: '2021-03-01T14:24:36.296Z',
    createdBy: sub,
    groupId,
    groupType: 'Team',
    name: teamName,
    previousLevel: 1,
    rank: 9999999,
    structureVersion: 1,
    totalParticipants: 9999999
  }

  const accountAggregatesToCreate = [
    { ...baseAccountAggregate, accountId: accountId1, totalDistance: 5200, totalSteps: 8000 },
    { ...baseAccountAggregate, accountId: accountId2, totalDistance: 5200, totalSteps: 8000 },
    { ...baseAccountAggregate, accountId: accountId3, totalDistance: 5200, totalSteps: 8000 }
  ]

  await createAggregates(mode, flowId, dynamodb, dynamodbTableName, accountAggregatesToCreate)
  console.log('AccountAggregates :::::::::::::::::::::', accountAggregateItems.length)

  const teamAggregate = {
    averageDistance: 0,
    averageSteps: 0,
    teamAggregateId: groupId,
    createdAt: sot,
    createdBy: sub,
    hasLogo: false,
    name: teamName,
    groupType: 'Team',
    rank: 1,
    structureVersion: 1,
    totalTeams: 9999999,
    totalDistance: 15600,
    totalSteps: 24000
  }
  await createGroupAggregate(mode, flowId, dynamodb, dynamodbTableName, teamAggregate)

  const baseActivity = {
    createdAt: '2021-03-16T14:47:02.569Z',
    deleted: false,
    createdBy: sub,
    trackerType: 'manual',
    structureVersion: 1
  }

  const activitiesToCreate = [
    { ...baseActivity, accountId: accountId1, activityDate: '2021-04-02', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId1, activityDate: '2021-04-03', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId1, activityDate: '2021-04-04', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId1, activityDate: '2021-04-05', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId2, activityDate: '2021-04-03', numberOfSteps: 4000, distance: 2600 },
    { ...baseActivity, accountId: accountId2, activityDate: '2021-04-04', numberOfSteps: 4000, distance: 2600 },
    { ...baseActivity, accountId: accountId3, activityDate: '2021-04-01', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId3, activityDate: '2021-04-02', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId3, activityDate: '2021-04-03', numberOfSteps: 2000, distance: 1300 },
    { ...baseActivity, accountId: accountId3, activityDate: '2021-04-04', numberOfSteps: 2000, distance: 1300 }
  ]

  await createActivities(mode, flowId, dynamodb, dynamodbTableName, activitiesToCreate)
}

async function checkUpdatedItems (dynamodb, dynamodbTableName, mode, data, title) {
  return await Promise.all(
    data.map(async item => await dynamoDbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, item.key))
  )
}

async function cleanDb (dynamodb, dynamodbTableName) {
  console.log('::::::::: Deleting AccountProfiles ::::::::::')
  await Promise.allSettled(profileItems.map(profile => dynamoDbHelper.deleteItem(dynamodb, dynamodbTableName, profile)))
  console.log('::::::::: Deleting Slots ::::::::::')
  await Promise.allSettled(slotItems.map(slot => dynamoDbHelper.deleteItem(dynamodb, dynamodbTableName, slot)))
  console.log('::::::::: Deleting AccountAggregates ::::::::::')
  await Promise.allSettled(
    accountAggregateItems.map(accAgg => dynamoDbHelper.deleteItem(dynamodb, dynamodbTableName, accAgg))
  )
  console.log('::::::::: Deleting Activities ::::::::::')
  await Promise.allSettled(activityItems.map(act => dynamoDbHelper.deleteItem(dynamodb, dynamodbTableName, act)))
  console.log('::::::::: Deleting GroupAggregate ::::::::::')
  await dynamoDbHelper.deleteItem(dynamodb, dynamodbTableName, groupAggregateItem)
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()
  const deleteMember = require('../../lib/server/business/team/deleteMember')

  const sot = new Date().toISOString()
  const flowId = uuidv4()
  const sub = 'test-delete-member-script'
  const groupId = uuidv4()

  // Set mode to automated test so lingering data can be removed by cleanDynamoDb script
  // Use testMode when modifying script
  // const testMode = `automated-test-${uuidv4()}`

  // Create accountAggregates, accountProfiles, activities, groupAggregate and slots
  await createAccounts(sot, sub, mode, flowId, dynamodb, dynamodbTableName, groupId)

  let success
  try {
    await Promise.all(
      accountIds.map(async id => {
        await deleteMember(sot, sub, mode, flowId, { teamId: groupId, accountId: id })
      })
    )
    success = true
  } catch (e) {
    logDetails('DeleteMember failed', e)
    success = false
  }

  // only wait and get updatedCompanyAggregate if delete is successful
  if (success) {
    await sleep(60000)

    // Check if soft delete of activities is successful
    const updatedActivities = await checkUpdatedItems(
      dynamodb,
      dynamodbTableName,
      mode,
      activityItems,
      'updatedActivityItems'
    )
    if (updatedActivities.every(activity => activity.data.deleted === true)) {
      console.log()
      console.log('  ✅  Activities updated successfully')
    } else {
      console.log()
      console.log('Activities failed to updated properly')
      logDetails('Activities failed to update', updatedActivities)
    }

    // Check if accountAggregates steps/distance are set to 0
    const updatedAccountAggregates = await checkUpdatedItems(
      dynamodb,
      dynamodbTableName,
      mode,
      accountAggregateItems,
      'updatedAccountAggregates'
    )
    if (updatedAccountAggregates.every(agg => agg.data.totalSteps === 0 && agg.data.totalDistance === 0)) {
      console.log()
      console.log('  ✅  AccountAggregates updated successfully')
    } else {
      console.log()
      console.log('AccountAggregates failed to updated properly')
      logDetails('Recalculation failed', updatedAccountAggregates)
    }

    // Check if companyAggregate is updated accordingly
    const updatedTeamAggregate = await dynamoDbHelper.queryActualByKey(
      dynamodb,
      dynamodbTableName,
      mode,
      groupAggregateItem.key
    )
    if (updatedTeamAggregate.data.totalSteps === 0 && updatedTeamAggregate.data.totalDistance === 0) {
      console.log()
      console.log(
        `  ✅  GroupAggregate {${updatedTeamAggregate.key}, ${updatedTeamAggregate.submitted}} updated successfully`
      )
      console.log()
    } else {
      console.log()
      console.log(
        `GroupAggregate {${updatedTeamAggregate.key}, ${updatedTeamAggregate.submitted}} failed to updated properly`
      )
      logDetails('Recalculation failed', updatedTeamAggregate)
      console.log()
    }
  }

  // After checks clean dynamodb
  await cleanDb(dynamodb, dynamodbTableName)
}

modeScript(
  'testDeleteMember',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Verify calculation of steps when member is deleted.
`
)
