#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @returns {Promise<[]>}
 */
async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey) {
  const index = {
    indexName: 'Index_E',
    partitionKey: 'partition_key_E',
    sortKey: 'sort_key_E'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const memberIndexPartitionKey = `/${mode}/slots/members`
  const freeIndexPartitionKey = `/${mode}/slots/free`

  const [takenSlots, availableSlots] = await Promise.all([
    getSlots(dynamodb, dynamodbTableName, mode, memberIndexPartitionKey),
    getSlots(dynamodb, dynamodbTableName, mode, freeIndexPartitionKey)
  ])

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    actual_slot_info: {
      slot_payed: takenSlots.length + availableSlots.length,
      slot_claimed: takenSlots.length,
      slot_free: availableSlots.length
    }
  }

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('Slot Statistics', statistics)
}

modeScript(
  'statistics',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get the statistics about the slots (claimed vs free).
`
)
