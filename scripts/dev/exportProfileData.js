#!/usr/bin/env node
/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const ObjectsToCSV = require('objects-to-csv')
const fs = require('fs')
const moment = require('moment')
const path = require('path')
const GroupType = require('../../lib/api/group/GroupType')
const TrackerType = require('../../lib/api/account/trackerConnection/TrackerType')
const Mode = require('../../lib/ppwcode/Mode').schema.required()
const help = `
Run this script with argument \`<MODE>\`.`

if (process.argv.includes('-h')) {
  console.log(help)
  process.exit(0)
}
const modeArg = process.argv[2]
const modeValidation = Mode.validate(modeArg)
if (modeValidation.error) {
  console.error(modeValidation.error.message)
  console.error(help)
  process.exit(-1)
}
const mode = modeValidation.value
console.log(`mode: ${mode}`)

let companyResults = []
let teamResults = []
let userResults = []
let config
let dynamodbTableName

/**
 * Make sure that the script uses the correct environment
 * which is given by the parameter mode at startup
 * @param mode
 */
function requireCorrectConfig (mode) {
  if (!config) {
    process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
    if (['production', 'demo', 'june2020'].includes(mode)) {
      process.env.NODE_ENV = 'production' // use production config
    } else {
      process.env.NODE_ENV = 'mocha' // use mocha config
    }
    config = require('config')
    dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')
  }
}

const sot = new Date().toISOString()

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunkSize {Integer} Size of every group
 */
function chunkArray (myArray, chunkSize) {
  let index = 0
  const arrayLength = myArray.length
  const tempArray = []

  let myChunk
  for (index = 0; index < arrayLength; index += chunkSize) {
    myChunk = myArray.slice(index, index + chunkSize)
    // Do something if you want with the group
    tempArray.push(myChunk)
  }

  return tempArray
}

async function getVersionAtOrBefore (mode, key) {
  const { dynamodb } = await getDynamodb()
  const tableName = dynamodbTableName(mode)

  const params = {
    TableName: tableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': sot
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName(mode), mode, params)

  if (result !== undefined) {
    return result.data
  }

  return result
}

async function getActualData (mode, key) {
  const { dynamodb } = await getDynamodb()
  const tableName = dynamodbTableName(mode)

  try {
    const result = await dynamodbHelper.queryActualByKey(dynamodb, tableName, mode, key)

    if (result !== undefined) {
      return result.data
    }
  } catch (e) {
    return undefined
  }
}

async function getAccount (mode, accountId) {
  const key = `/${mode}/account/${accountId}` // TODO move to key calculation to Account static?
  return getVersionAtOrBefore(mode, key)
}

async function getGroupProfile (mode, groupId) {
  const key = `/${mode}/group/${groupId}/publicProfile`
  return getVersionAtOrBefore(mode, key)
}

async function getAccountPreferences (mode, accountId) {
  const key = `/${mode}/account/${accountId}/preferences`
  return getVersionAtOrBefore(mode, key)
}

async function getAccountProfile (mode, accountId) {
  const key = `/${mode}/account/${accountId}/publicProfile`
  return getVersionAtOrBefore(mode, key)
}

async function getCompany (mode, companyId) {
  const key = `/${mode}/company/${companyId}`
  return getVersionAtOrBefore(mode, key)
}

async function getTeam (mode, teamId) {
  const key = `/${mode}/team/${teamId}`
  return getVersionAtOrBefore(mode, key)
}

async function getAccountAggregate (mode, accountId) {
  const key = `/${mode}/accountAggregate/${accountId}`
  return getActualData(mode, key)
}

async function getCompanyAggregate (mode, companyId) {
  const key = `/${mode}/companyAggregate/${companyId}`
  return getActualData(mode, key)
}

async function getTeamAggregate (mode, companyId) {
  const key = `/${mode}/teamAggregate/${companyId}`
  return getActualData(mode, key)
}

async function getTrackerConnection (mode, accountId) {
  const key = `/${mode}/account/${accountId}/trackerConnection`
  return getActualData(mode, key)
}

async function getSlotsForGroup (mode, groupId) {
  const { dynamodb } = await getDynamodb()
  const tableName = dynamodbTableName(mode)

  const memberIndexPartitionKey = `/${mode}/group/${groupId}/members`
  const freeIndexPartitionKey = `/${mode}/group/${groupId}/free`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const paramsMembers = {
    TableName: tableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': memberIndexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const paramsFree = {
    TableName: tableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': freeIndexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const memberSlots = await dynamodbHelper.queryAll(dynamodb, tableName, mode, paramsMembers)
  const freeSlots = await dynamodbHelper.queryAll(dynamodb, tableName, mode, paramsFree)

  return memberSlots.concat(freeSlots)
}

async function getSubgroups (dynamodb, dynamodbTableName, mode, groupId, groupType) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const indexPartitionKey = `/${mode}/subgroup/${groupType.toLowerCase()}/${groupId}`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/**
 * Fetch the group administration data
 * @param mode mode
 * @returns {Promise<[]>}
 */
async function getAllGroupAdministrations (mode) {
  const { dynamodb } = await getDynamodb()
  const tableName = dynamodbTableName(mode)

  const index = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }

  const indexPartitionKey = `/${mode}/groupAdministration`

  const params = {
    TableName: tableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  const finalResult = result.filter(s => s.submitted === 'actual')

  return finalResult
}

async function deleteExistingDataFile (exportName) {
  const dateString = moment().format('YYYYMMDD')

  const filePath = `./output/${exportName}_${dateString}.csv`
  if (fs.existsSync(filePath)) {
    try {
      fs.unlinkSync(filePath)
    } catch (err) {
      console.error(err)
    }
  }
}

/**
 * Create or append a CSV file for the given data
 * @param data      data to export
 * @param exportName  name to use for the export file
 * @returns {Promise<void>}
 */
async function appendToCsv (data, exportName) {
  const dateString = moment().format('YYYYMMDD')

  const csv = new ObjectsToCSV(data)

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  // Save to file:
  await csv.toDisk(`./output/${exportName}_${dateString}.csv`, { append: true })
}

/**
 * Create a CSV file for the given data
 * @param data      data to export
 * @param exportName  name to use for the export file
 * @returns {Promise<void>}
 */
async function exportToCsv (data, exportName) {
  const dateString = moment().format('YYYYMMDD')

  const csv = new ObjectsToCSV(data)

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  // Save to file:
  await csv.toDisk(`./output/${exportName}_${dateString}.csv`, { append: false })
}

/**
 * Exports the data for company
 * returns the data into a csv file
 * @param mode
 * @returns {Promise<void>}
 */
async function exportCompanyData (mode, companyAdministrationData) {
  console.log('=> start processing CompanyData')

  const { dynamodb } = await getDynamodb()
  const tableName = dynamodbTableName(mode)

  companyResults = await Promise.all(
    companyAdministrationData.map(async itemData => {
      const [
        account,
        groupProfile,
        company,
        preferences,
        slotsForGroup,
        subgroups,
        companyAggregate
      ] = await Promise.all([
        getAccount(mode, itemData.accountId),
        getGroupProfile(mode, itemData.groupId),
        getCompany(mode, itemData.groupId),
        getAccountPreferences(mode, itemData.accountId),
        getSlotsForGroup(mode, itemData.groupId),
        getSubgroups(dynamodb, tableName, mode, itemData.groupId, GroupType.companyType),
        getCompanyAggregate(mode, itemData.groupId)
      ])

      const claimedSlots = slotsForGroup.filter(s => s.data.accountId !== undefined)

      const companyMemberAccountAggregates = await Promise.all(
        claimedSlots.map(s => {
          return getAccountAggregate(mode, s.data.accountId)
        })
      )

      if (company !== undefined) {
        const companyData = {
          companyName: groupProfile.name,
          companyId: itemData.groupId,
          type: company.groupType,
          street: company.address,
          zip: company.zip,
          city: company.city,
          vat: company.vat,
          reference: company.reference,
          administratorId: account.id,
          joinLink: company.shortUrl,
          language: preferences !== undefined ? preferences.language : '',
          newsletter: preferences !== undefined ? preferences.newsletter : undefined,
          knownFromType: preferences !== undefined ? preferences.knownFromType : '',
          otherText: preferences !== undefined ? preferences.otherText : '',
          contactFirstName: company.contactFirstName,
          contactLastName: company.contactLastName,
          // email: preferences.verifiedEmail.email,
          contactEmail: company.contactEmail,
          contactPhone: company.contactTelephone,
          payedSlots: slotsForGroup.length,
          takenSlots: claimedSlots.length,
          numberOfCompanyTeams: subgroups.length,
          unit: company.unit !== undefined ? company.unit : '',
          totalSteps: companyAggregate !== undefined ? companyAggregate.totalSteps : '',
          totalDistance: companyAggregate !== undefined ? companyAggregate.totalDistance : '',
          activeUsers: companyMemberAccountAggregates.filter(a => a.totalSteps > 0).length,
          averageSteps:
            companyAggregate !== undefined && companyAggregate.totalSteps > 0 ? companyAggregate.averageSteps : '',
          averageDistance:
            companyAggregate !== undefined && companyAggregate.totalSteps > 0 ? companyAggregate.averageDistance : '',
          rank: companyAggregate !== undefined && companyAggregate.totalSteps > 0 ? companyAggregate.rank : ''
        }

        return companyData
      }
    })
  )

  await exportToCsv(
    companyResults.filter(x => x !== undefined),
    'CompanyData'
  )
}

/**
 * Exports the data for teams
 * returns the data into a csv file
 * @param mode
 * @returns {Promise<void>}
 */
async function exportTeamData (mode, teamAdministrationData) {
  console.log('=> start processing TeamData')

  teamResults = await Promise.all(
    teamAdministrationData.map(async itemData => {
      const slotsForGroup = await getSlotsForGroup(mode, itemData.groupId)

      if (slotsForGroup.length > 1) {
        const [account, accountProfile, groupProfile, team, preferences, teamAggregate] = await Promise.all([
          getAccount(mode, itemData.accountId),
          getAccountProfile(mode, itemData.accountId),
          getGroupProfile(mode, itemData.groupId),
          getTeam(mode, itemData.groupId),
          getAccountPreferences(mode, itemData.accountId),
          getTeamAggregate(mode, itemData.groupId)
        ])

        const claimedSlots = slotsForGroup.filter(s => s.data.accountId !== undefined)

        const companyMemberAccountAggregates = await Promise.all(
          claimedSlots.map(s => {
            return getAccountAggregate(mode, s.data.accountId)
          })
        )

        const teamData = {
          teamName: groupProfile.name,
          teamId: itemData.groupId,
          type: team.groupType,
          administratorId: account.id,
          language: preferences.language,
          newsletter: preferences.newsletter,
          knownFromType: preferences.knownFromType,
          otherText: preferences.otherText,
          contactFirstName: accountProfile.firstName,
          contactLastName: accountProfile.lastName,
          contactEmail: preferences.verifiedEmail.email,
          joinLink: team.shortUrl,
          payedSlots: slotsForGroup.length,
          takenSlots: claimedSlots.length,
          isCompanyTeam: 'false',
          totalSteps: teamAggregate !== undefined ? teamAggregate.totalSteps : '',
          totalDistance: teamAggregate !== undefined ? teamAggregate.totalDistance : '',
          activeUsers: companyMemberAccountAggregates.filter(a => a.totalSteps > 0).length,
          averageSteps: teamAggregate !== undefined && teamAggregate.totalSteps > 0 ? teamAggregate.averageSteps : '',
          averageDistance:
            teamAggregate !== undefined && teamAggregate.totalSteps > 0 ? teamAggregate.averageDistance : '',
          rank: teamAggregate !== undefined && teamAggregate.totalSteps > 0 ? teamAggregate.rank : ''
        }

        return teamData
      }
    })
  )

  // ToDo: add subgroups
  await exportToCsv(
    teamResults.filter(r => r !== undefined),
    'TeamData'
  )
}

async function getClaimedSlots (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_E',
    partitionKey: 'partition_key_E',
    sortKey: 'sort_key_E'
  }

  const memberIndexPartitionKey = `/${mode}/slots/members`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': memberIndexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/**
 * Exports the data of individual users/ participants
 * of the pink march.
 * Returns the data into a csv file
 * @param mode
 * @returns {Promise<void>}
 */
async function exportUserData (mode) {
  console.log('=> start processing UserData')

  await deleteExistingDataFile('UserData')

  const { dynamodb } = await getDynamodb()

  requireCorrectConfig(mode)
  let slotData = []
  const TableName = dynamodbTableName(mode)

  const claimedSlots = await getClaimedSlots(dynamodb, TableName, mode)

  slotData = claimedSlots.map(item => item.data)

  const chunkedArray = chunkArray(slotData, 15000)

  for (const part of chunkedArray) {
    await collectAndWriteUserData(mode, part)
  }
}

async function collectAndWriteUserData (mode, slotData) {
  const activityCalculations = require('../../lib/api/activity/ActivityCalculations')

  userResults = await Promise.all(
    slotData.map(async itemData => {
      const [
        account,
        preferences,
        profile,
        groupProfile,
        accountAggregate,
        trackerConnection,
        slotsForGroup
      ] = await Promise.all([
        getAccount(mode, itemData.accountId),
        getAccountPreferences(mode, itemData.accountId),
        getAccountProfile(mode, itemData.accountId),
        getGroupProfile(mode, itemData.groupId),
        getAccountAggregate(mode, itemData.accountId),
        getTrackerConnection(mode, itemData.accountId),
        getSlotsForGroup(mode, itemData.groupId)
      ])

      const userGroupType = slotsForGroup.length > 1 ? itemData.groupType : 'Individual'

      if (profile) {
        return {
          firstName: profile.firstName,
          lastName: profile.lastName,
          accountId: account.id,
          gender: profile.gender,
          dateOfBirth: profile.dateOfBirth.slice(0, 10),
          age: calculateAge(new Date(profile.dateOfBirth.slice(0, 10))),
          email:
            preferences !== undefined && preferences.verifiedEmail !== undefined
              ? preferences.verifiedEmail.email
              : account.email,
          auth: account.sub,
          language: preferences.language,
          newsletter: preferences.newsletter,
          knownFromType: preferences.knownFromType,
          otherText: preferences.otherText,
          zip: profile.zip,
          province: getProvince(profile.zip),
          groupType: userGroupType, // ToDo: Individual/team/company
          teamName: userGroupType === GroupType.teamType ? groupProfile.name : '',
          companyName: userGroupType === GroupType.companyType ? groupProfile.name : '',
          trackerType: trackerConnection !== undefined ? trackerConnection.trackerType : TrackerType.manualType,
          totalSteps: accountAggregate !== undefined ? accountAggregate.totalSteps : '',
          totalDistance: accountAggregate !== undefined ? accountAggregate.totalDistance : '',
          ranking: accountAggregate !== undefined && accountAggregate.rank !== 9999999 ? accountAggregate.rank : '',
          level:
            accountAggregate !== undefined
              ? activityCalculations.calculateLevelFromSteps(accountAggregate.totalSteps, 1)
              : ''
        }
      } else {
        return undefined
      }
    })
  )

  await appendToCsv(
    userResults.filter(r => r !== undefined),
    'UserData'
  )
}

function calculateAge (dateOfBirth) {
  const dateToCalculate = new Date()

  const calculateYear = dateToCalculate.getFullYear()
  const calculateMonth = dateToCalculate.getMonth()
  const calculateDay = dateToCalculate.getDate()

  const birthYear = dateOfBirth.getFullYear()
  const birthMonth = dateOfBirth.getMonth()
  const birthDay = dateOfBirth.getDate()

  let age = calculateYear - birthYear
  const ageMonth = calculateMonth - birthMonth
  const ageDay = calculateDay - birthDay

  if (ageMonth < 0 || (ageMonth === 0 && ageDay < 0)) {
    age = parseInt(age) - 1
  }
  return age
}

function getProvince (zip) {
  if (zip >= 1000 && zip <= 1299) {
    return 'Brussels Hoofdstedelijk Gewest'
  }
  if (zip >= 1300 && zip <= 1499) {
    return 'Waals-Brabant'
  }
  if ((zip >= 1500 && zip <= 1999) || (zip >= 3000 && zip <= 3499)) {
    return 'Vlaams Brabant'
  }

  if (zip >= 2000 && zip <= 2999) {
    return 'Antwerpen'
  }

  if (zip >= 3500 && zip <= 3999) {
    return 'Limburg'
  }

  if (zip >= 4000 && zip <= 4999) {
    return 'Luik'
  }

  if (zip >= 5000 && zip <= 5999) {
    return 'Namen'
  }

  if ((zip >= 6000 && zip <= 6599) || (zip >= 7000 && zip <= 7999)) {
    return 'Henegouwen'
  }

  if (zip >= 6600 && zip <= 6999) {
    return 'Luxemburg'
  }

  if (zip >= 8000 && zip <= 8999) {
    return 'West-Vlaanderen'
  }

  if (zip >= 9000 && zip <= 9999) {
    return 'Oost-Vlaanderen'
  }

  return ''
}

async function execute (mode) {
  requireCorrectConfig(mode)
  console.info(`getting profile data for ${mode} ...`)

  const groupAdministrations = await getAllGroupAdministrations(mode)

  // Filter only company data
  const companyAdministrationData = groupAdministrations
    .filter(i => i.data.groupType.toLowerCase() === 'company')
    .map(item => item.data)

  await exportCompanyData(mode, companyAdministrationData)

  const teamAdministrationData = groupAdministrations
    .filter(i => i.data.groupType === GroupType.teamType)
    .map(item => item.data)

  await exportTeamData(mode, teamAdministrationData)

  await exportUserData(mode)

  console.log('Export Profile Data finished')
}

execute(mode).catch(err => {
  console.error('exportProfileData failed:')
  console.error(err)
  process.exit(-1)
})
