/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const uuidv4 = require('uuid').v4
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const dynamodbHelper = require('./_dynamodbHelper')

function clean (obj) {
  for (var propName in obj) {
    if (obj[propName] === null || obj[propName] === undefined) {
      delete obj[propName]
    }
  }
}

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @returns {Promise<[]>}
 */
async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey) {
  const index = {
    indexName: 'Index_E',
    partitionKey: 'partition_key_E',
    sortKey: 'sort_key_E'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/***
 * Returns all existing accountAggregates
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @returns {Promise<[]>}
 */
async function getExistingAccountAggregates (dynamodb, dynamodbTableName, mode) {
  const indexPartitionKey = `/${mode}/accountAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function checkIfAggregateExists (mode, accountsToCheck) {
  console.log(`For mode:  ${mode} we start checking if we have to create aggregates`)
  const getAccountAggregate = require('../../lib/server/business/aggregates/account/get')

  const aggregatesToCreate = []
  clean(accountsToCheck)
  for (let i = 0; i < accountsToCheck.length; i++) {
    if (accountsToCheck[i] !== undefined && accountsToCheck[i].submitted === 'actual') {
      const accountId = accountsToCheck[i].data.accountId
      let existingAccountAggregate

      try {
        existingAccountAggregate = await getAccountAggregate(
          accountsToCheck[i].submitted,
          accountsToCheck[i].submittedBy,
          mode,
          accountsToCheck[i].flowId,
          { accountId }
        )
      } catch (e) {
        existingAccountAggregate = undefined
      }
      if (!existingAccountAggregate) {
        aggregatesToCreate.push({
          accountId,
          groupId: accountsToCheck[i].data.groupId,
          groupType: accountsToCheck[i].data.groupType
        })
      }
    }
  }
  console.log(`for mode: ${mode} we have to create ${aggregatesToCreate.length} aggregates`)
  return aggregatesToCreate
}

async function createAccountAggregates (mode, aggregatesToCreate, doWrite) {
  const getAccountProfile = require('../../lib/server/business/account/profile/get')
  const getActivitiesByAccountId = require('../../lib/server/business/account/activity/getActivitiesByAccountId')
  const writeAccountAggregate = require('../../lib/server/business/aggregates/account/write')

  const submitted = new Date().toISOString()
  const submittedBy = 'createAccountAggregateScript'
  const flowId = uuidv4()

  const listOfAggregatesToCreate = []

  for (let i = 0; i < aggregatesToCreate.length; i++) {
    const accountInfo = aggregatesToCreate[i]
    console.log(`Creating aggregate for ${i} in mode: ${mode}`)

    try {
      const accountProfile = await getAccountProfile(submitted, submittedBy, mode, flowId, {
        accountId: accountInfo.accountId
      })
      let activities = []
      try {
        activities = await getActivitiesByAccountId(submitted, submittedBy, mode, flowId, {
          accountId: accountInfo.accountId
        })
      } catch (err) {
        if (err.isBoom && err.output.statusCode === 404) {
          console.log(` ====> No activities found for ${accountInfo.accountId}`)
        } else {
          throw err
        }
      }

      let totalSteps = 0
      let totalDistance = 0
      if (activities.length > 0) {
        totalSteps = activities.reduce((partialSum, a) => partialSum + a.numberOfSteps, 0)
        totalDistance = activities.reduce((partialSum, a) => partialSum + a.distance, 0)
      }

      const accountAggregateData = {
        createdAt: submitted,
        createdBy: submittedBy,
        structureVersion: 1,
        accountId: accountInfo.accountId,
        totalSteps: totalSteps,
        totalDistance: totalDistance,
        groupId: accountInfo.groupId,
        groupType: accountInfo.groupType,
        name: `${accountProfile.firstName} ${accountProfile.lastName}`,
        rank: 9999999,
        totalParticipants: 9999999,
        acknowledged: false,
        previousLevel: 1
      }

      listOfAggregatesToCreate.push(accountAggregateData)
    } catch (e) {
      console.log(`Failed To create AccountAggreagate for ${accountInfo} => ${e}`)
    }
  }

  let successFulCreations = 0

  if (listOfAggregatesToCreate.length > 0 && doWrite) {
    const outcomes = await Promise.allSettled(
      listOfAggregatesToCreate.map(item => writeAccountAggregate(submitted, submittedBy, mode, flowId, item))
    )

    successFulCreations = outcomes.filter(o => o.status === 'fulfilled').length
  } else {
    console.log('Dry Run => to create these aggregates you must run this script with --write')
    console.log()
    console.log(util.inspect(listOfAggregatesToCreate, false, null, true /* enable colors */))

    successFulCreations = listOfAggregatesToCreate.length
  }

  console.log('All done 👍')

  return successFulCreations
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWrite = process.argv[3] === '--write'

  const memberIndexPartitionKey = `/${mode}/slots/members`

  const takenSlots = await getSlots(dynamodb, dynamodbTableName, mode, memberIndexPartitionKey)
  const existingAccountAggregatesBefore = await getExistingAccountAggregates(dynamodb, dynamodbTableName, mode)

  const existingAccountIds = existingAccountAggregatesBefore.map(a => a.data.accountId)

  const slotsToProcess = takenSlots.filter(s => !existingAccountIds.includes(s.data.accountId))

  const aggregatesToCreate = await checkIfAggregateExists(mode, slotsToProcess)

  console.log(`For ${mode} there are ${aggregatesToCreate.length} account aggregates that need to be created`)
  console.log(util.inspect(aggregatesToCreate, false, null, true /* enable colors */))

  console.log('Accounts aggregates are being created ....')
  const successfullCreations = await createAccountAggregates(mode, aggregatesToCreate, doWrite)

  const existingAccountAggregatesAfter = await getExistingAccountAggregates(dynamodb, dynamodbTableName, mode)

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    isDryRun: !doWrite,
    accountAggregate_info: {
      takenSlots: takenSlots.length,
      before: existingAccountAggregatesBefore.length,
      to_be_created: aggregatesToCreate.length,
      created: successfullCreations,
      after: existingAccountAggregatesAfter.length
    }
  }

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('AccountAggregate Statistics', statistics)
}
modeScript(
  'createAccAggregatesForExistingAccounts',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: create an accountaggregate for an existing profile if it not already exists
this script does scan all the accounts, checks if they have 1 or more slots. Then checks
if the account has an aggregate, if not create one for that account
`
)
