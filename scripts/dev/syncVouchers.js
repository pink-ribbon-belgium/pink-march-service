#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const VoucherDynamodb = require('../../lib/api/voucher/VoucherDTO')
  .schema.tailor('dynamodb')
  .required()
const Voucher = require('../../lib/server/business/voucher/Voucher')
const getDynamodb = require('./_getDynamodb')
const path = require('path')
const fs = require('fs').promises
const yaml = require('js-yaml')
const assert = require('assert')
const Joi = require('@hapi/joi')
const modeScript = require('./_modeScript')

const sot = new Date().toISOString()
const indexA = {
  indexName: 'Index_A',
  partitionKey: 'partition_key_A',
  sortKey: 'sort_key_A'
}

const VoucherYAML = VoucherDynamodb.keys({
  createdAt: Joi.string()
    .optional()
    .strip(),
  createdBy: Joi.string()
    .optional()
    .strip()
})

async function loadYaml (absPath) {
  console.log(`👁 trying to load ${absPath} …`)
  let contents
  try {
    contents = await fs.readFile(absPath) // fails if it does not exist
    console.log(`${absPath} found`)
  } catch (err) {
    return undefined
  }
  const vouchers = yaml.safeLoad(contents, { filename: absPath })
  Object.keys(vouchers).forEach(voucherCode => {
    vouchers[voucherCode].voucherCode = voucherCode
  })
  validateVouchers(vouchers, VoucherYAML)
  console.log(`${absPath} loaded and validated; found ${Object.keys(vouchers).length} vouchers`)
  return vouchers
}

function validateVouchers (vouchers, schema) {
  Object.keys(vouchers).forEach(voucherCode => {
    const validation = schema.validate(vouchers[voucherCode])
    if (validation.error) {
      throw validation.error
    }
    vouchers[voucherCode] = validation.value
  })
}

async function getExistingVouchers (mode, dynamodbTableName, partitionKeyValue) {
  const { dynamodb } = await getDynamodb()
  console.info(`👀 getting existing vouchers for mode ${mode} …`)
  let done = false
  const mostRecentVoucherItems = {}
  while (!done) {
    const result = await dynamodb
      .query({
        TableName: dynamodbTableName,
        IndexName: indexA.indexName,
        ExpressionAttributeNames: {
          '#partitionKey': indexA.partitionKey
        },
        ExpressionAttributeValues: {
          ':partitionKey': partitionKeyValue
        },
        KeyConditionExpression: '#partitionKey = :partitionKey',
        ScanIndexForward: false
      })
      .promise()

    result.Items.forEach(item => {
      if (!mostRecentVoucherItems[item.key] || mostRecentVoucherItems[item.key].submitted < item.submitted) {
        mostRecentVoucherItems[item.key] = item
      }
      // else we already had a more recent item
    })
    if (!result.LastEvaluatedKey) {
      console.log('👌 all done')
      done = true
    } else {
      console.log('👁 there is more …')
    }
  }
  const vouchers = Object.keys(mostRecentVoucherItems).reduce((acc, k) => {
    acc[mostRecentVoucherItems[k].data.voucherCode] = mostRecentVoucherItems[k].data
    return acc
  }, {})
  validateVouchers(vouchers, VoucherDynamodb)
  console.info(
    `🤠 got and validated existing vouchers for mode ${mode}; found ${Object.keys(vouchers).length} vouchers`
  )
  return vouchers
}

async function writeVoucher (mode, dynamodbTableName, partitionKeyValue, voucher) {
  const validation = VoucherYAML.validate(voucher)
  if (validation.error) {
    throw validation.error
  }

  const { dynamodb, userIdentification: sub } = await getDynamodb()

  const voucherPersistentObject = new Voucher({
    mode,
    sot,
    sub,
    dto: { createdAt: sot, createdBy: sub, ...voucher }
  })
  const voucherItem = {
    mode: mode,
    key: voucherPersistentObject.key,
    submitted: sot,
    submittedBy: sub,
    data: voucherPersistentObject.toJSON(),
    [indexA.partitionKey]: partitionKeyValue,
    sort_key_A: sot
  }

  console.info(`  ✍️ writing voucher ${voucher.voucherCode} for mode ${mode} …`)
  await dynamodb
    .put({
      TableName: dynamodbTableName,
      Item: voucherItem
    })
    .promise()
  console.info(`  👍 voucher ${voucher.voucherCode} for mode ${mode} written`)
}

const voucherPropertiesToCompare = ['reusable', 'amount', 'percentage', 'expiryDate']

function isDifferent (voucher1, voucher2) {
  assert.ok(voucher1)
  assert.strictEqual(voucher1.voucherCode, voucher1.voucherCode)
  return !voucher2 || voucherPropertiesToCompare.some(p => voucher1[p] !== voucher2[p])
}

async function writeFile (srcFilePath, vouchers) {
  Object.keys(vouchers).forEach(voucherCode => {
    delete vouchers[voucherCode].voucherCode
  })
  const result = Object.keys(vouchers).length <= 0 ? '' : yaml.safeDump(vouchers, { noRefs: false })
  await fs.writeFile(srcFilePath, result)
}

async function execute (mode, dynamodbTableName) {
  const partitionKeyValue = `/${mode}/voucher`
  const srcFilePath = path.format({ dir: __dirname, name: `vouchers-${mode}`, ext: '.yaml' })
  console.log(`using \`${srcFilePath}\``)
  const [desiredState, existingVouchers] = await Promise.all([
    loadYaml(srcFilePath),
    getExistingVouchers(mode, dynamodbTableName, partitionKeyValue)
  ])
  if (!desiredState) {
    console.log(`🤖 ${srcFilePath} not found. Will create it from existing vouchers …`)
    await writeFile(srcFilePath, existingVouchers)
    console.log(`👌 created ${srcFilePath} from persistent storage`)
    return
  }
  const desiredVoucherCodes = Object.keys(desiredState)
  const vouchersToExpire = Object.keys(existingVouchers)
    .filter(voucherCode => !desiredVoucherCodes.includes(voucherCode))
    .map(voucherCode => {
      const voucherToExpire = existingVouchers[voucherCode]
      voucherToExpire.expiryDate = sot
      return voucherToExpire
    })
  console.log(`found ${vouchersToExpire.length} vouchers to expire`)
  const vouchersToUpdate = Object.keys(desiredState)
    .filter(voucherCode => isDifferent(desiredState[voucherCode], existingVouchers[voucherCode]))
    .map(voucherCode => desiredState[voucherCode])
  console.log(`found ${vouchersToUpdate.length} vouchers to update or create`)
  const vouchersToWrite = vouchersToUpdate.concat(vouchersToExpire)
  console.log(`✍ writing ${vouchersToWrite.length} vouchers …`)
  await Promise.all(vouchersToWrite.map(voucher => writeVoucher(mode, dynamodbTableName, partitionKeyValue, voucher)))
  console.log(`👍 done writing vouchers, updating ${srcFilePath} …`)
  const newState = await getExistingVouchers(mode, dynamodbTableName, partitionKeyValue)
  await writeFile(srcFilePath, newState)
  console.log(`👌 updated ${srcFilePath}; done`)
}

modeScript(
  'syncVouchers',
  execute,
  `
Run this script with argument \`<MODE>\`.

When there is no file \`vouchers-<MODE>.yaml\` next to the script, it
will be created with the vouchers found in persistent storage. Those files are
\`.gitignore\`d.

Edit the file and commit.

When \`vouchers-<MODE>.yaml\` is found next to the script, persistent
storage is brought in sync with it:

  - vouchers not found in the YAML, that exist in persistent storage, get an
    \`expiryDate\` of **now** (they are "deleted")
  - vouchers found in the YAML file are written to persistent storage, either
    creating or updating the voucher in persistent storage.

In the \`vouchers-<MODE>.yaml\` file, \`createdAt\` and \`createdBy\` are
ignored. They are written for your information.`
)
