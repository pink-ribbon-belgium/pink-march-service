#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const TrackerType = require('./../../lib/api/account/trackerConnection/TrackerType')

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccountPreferences (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/preferences`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param trackerType
 * @returns {Promise<[]>}
 */
async function getTrackerConnectionsByType (dynamodb, dynamodbTableName, mode, trackerType) {
  const indexPartitionKey = `/${mode}/trackerConnection/${trackerType}`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()
  var fs = require('fs')

  const trackerType = TrackerType.polarType

  const trackerConnections = await getTrackerConnectionsByType(dynamodb, dynamodbTableName, mode, trackerType)

  const preferences = await Promise.all(
    trackerConnections.map(t => {
      return getAccountPreferences(dynamodb, dynamodbTableName, mode, t.data.accountId)
    })
  )

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    trackerType: trackerType,
    mailAddresses: preferences.map(p => (p.data.verifiedEmail !== undefined ? p.data.verifiedEmail.email : '')),
    result: {
      number_of_trackerConnections: trackerConnections.length,
      connections: trackerConnections
    }
  }

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  fs.writeFile(`./output/trackerConnections_${trackerType}.json`, JSON.stringify(statistics, null, 2), err => {
    if (err) throw err
    console.log('COMPANY Data written to file')
  })

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  logDetails('Slot Statistics', statistics)
}

modeScript(
  'getTrackerConnectionByType',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Get trackerconnections by type.
`
)
