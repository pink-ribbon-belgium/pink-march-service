#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const GroupAdministration = require('../../lib/api/group/GroupAdministrationDTO')
  .schema.tailor('dynamodb')
  .required()

const farFuture = '2222-03-17T13:04:57.321Z'

let problemCount = 0
let successCount = 0

async function getGroupAdminstrations (dynamodb, dynamodbTableName, mode) {
  console.info(`👀 scanning GroupAdministrations for mode ${mode} …`)

  const data = new Set()
  let done = false
  let lastEvaluatedKey
  while (!done) {
    const result = await dynamodb
      .scan({
        TableName: dynamodbTableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': `/${mode}/groupAdministration/account`
        },
        FilterExpression: 'begins_with(#key, :key)',
        Select: 'ALL_ATTRIBUTES',
        ExclusiveStartKey: lastEvaluatedKey
      })
      .promise()
    console.log(`👁 found ${result.Count} GroupAdministration related Items for mode ${mode}`)

    result.Items.forEach(item => {
      console.log(`Found account with key ${item.key}. Adding to data.`)
      data.add(item)
    })
    if (!result.LastEvaluatedKey) {
      done = true
    } else {
      lastEvaluatedKey = result.LastEvaluatedKey
    }
  }
  console.log(`Found ${data.size} disctinct account keys`)
  return data
}

async function writeToDatabase (dynamodb, dynamodbTableName, Item) {
  await dynamodb
    .put({
      TableName: dynamodbTableName,
      Item
    })
    .promise()
  console.log(`  ✅  GroupAdminstration {${Item.key}, ${Item.submitted}} updated`)
}

async function getCompany (dynamodb, dynamodbTableName, mode, companyId) {
  const key = `/${mode}/company/${companyId}`
  console.log(`        getting most recent version of Company with key ${key} …`)
  const result = await dynamodb
    .query({
      TableName: dynamodbTableName,
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':key': key,
        ':submitted': farFuture
      },
      KeyConditionExpression: '#key = :key and #submitted <= :submitted',
      Limit: 1,
      ScanIndexForward: false // true = ascending, false = descending
    })
    .promise()
  return result && result.Count && result.Items[0].data
}

async function updateItem (dynamodb, dynamodbTableName, mode, item, doWrite) {
  const company = await getCompany(dynamodb, dynamodbTableName, mode, item.data.groupId)

  const updatedItem = {
    ...item
  }

  updatedItem.data.groupType = company !== 0 ? company.groupType : 'Team'
  updatedItem.data.structureVersion = 2

  const validation = GroupAdministration.validate(updatedItem.data)

  if (validation.error) {
    console.error(`  ⁉️validation failed for groupAdministration {${item.key}, ${item.submitted}}`)
    console.error(`    💀️ ${validation.error.annotate()}`)
    console.error(`  ‼️groupAdministration {${item.key}, ${item.submitted}} needs update, but cannot be updated`)

    problemCount++
  } else {
    console.log(
      `Updated item => key: ${item.key} => structureVersion: ${item.data.structureVersion} => groupType: ${item.data.groupType}`
    )
    successCount++

    if (doWrite) {
      await writeToDatabase(dynamodb, dynamodbTableName, updatedItem)
    }
  }
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'
  const { dynamodb } = await getDynamodb()

  const allGroupAdministrationData = await getGroupAdminstrations(dynamodb, dynamodbTableName, mode)

  // const filteredItems = Array.from(allGroupAdministrationData).filter(item => item.data.structureVersion === 1)
  const filteredItems = Array.from(allGroupAdministrationData).filter(item => item.data.groupType === undefined)

  filteredItems.forEach(item => {
    console.log(`Filtered item => structureVersion: ${item.data.structureVersion} => key ${item.key}`)
  })

  console.log(`Filtering on structureVersion 1 returned  ${filteredItems.length} items`)

  await Promise.all(filteredItems.map(item => updateItem(dynamodb, dynamodbTableName, mode, item, doWrite)))

  console.log(`found ${allGroupAdministrationData.size} groupAdministrations`)
  console.log(`found ${filteredItems.length} groupAdministrations to update`)
  console.log(`  ${problemCount} could not be updated`)
  console.log(`  ${successCount} are updated`)

  if (!doWrite) {
    console.log('this was a dry run')
  }
}

modeScript(
  'ensureGroupTypeOnGroupAdministration',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Add groupType to GroupAdministration if not already exists
The script scans all GroupAdminstration Items
Adds the \`Grouptype\` and sets \`structureVersion\` to \`2\` to all items where
grouptype is undefined.
No new versions are created, and \`submitted\` is not changed.
`
)
