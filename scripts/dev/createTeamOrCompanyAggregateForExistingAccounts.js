/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const uuidv4 = require('uuid').v4
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')
const dynamodbHelper = require('./_dynamodbHelper')
const GroupType = require('./../../lib/api/group/GroupType')

const AggregateCalculations = require('./../../lib/api/aggregates/calculation/AggregateCalculations')

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @param indexName => A/B/C/D/E
 * @returns {Promise<[]>}
 */
async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey, indexName) {
  const index = {
    indexName: `Index_${indexName}`,
    partitionKey: `partition_key_${indexName}`,
    sortKey: `sort_key_${indexName}`
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/***
 * Returns all existing accountAggregates
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @returns {Promise<[]>}
 */
async function getExistingAggregates (dynamodb, dynamodbTableName, mode, groupType) {
  const indexPartitionKey = groupType === GroupType.teamType ? `/${mode}/teamAggregate` : `/${mode}/companyAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccountAggregate (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/accountAggregate/${accountId}`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function getSubGroup (dynamodb, dynamodbTableName, mode, subgroupId) {
  const key = `/${mode}/subgroup/${subgroupId}`

  return dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)
}

async function filterOnlyNonExistingAggregates (dynamodb, dynamodbTableName, mode, groupType, aggregateInfoList) {
  const aggregateInfoToCreate = []

  for (const aggregateInfo of aggregateInfoList) {
    let existingAggregate
    try {
      const key = `/${mode}/${groupType.toLowerCase()}Aggregate/${aggregateInfo.groupId}`
      existingAggregate = await dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key, true)
    } catch (e) {
      existingAggregate = []
    }

    if (existingAggregate.length === 0) {
      aggregateInfoToCreate.push(aggregateInfo)
    }
  }

  console.log(`for mode: ${mode} we have to create ${aggregateInfoToCreate.length} ${groupType} aggregates`)

  return aggregateInfoToCreate
}

/***
 * Create subgroup teamAggregates based on the given list of aggregate information
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param aggregatesToCreate    array with info to create the TeamAggregate for a subGroup
 * @param doWrite
 * @returns {Promise<*>}
 */
async function createSubgroupTeamAggregates (dynamodb, dynamodbTableName, mode, aggregatesToCreate, doWrite) {
  const getGroupProfile = require('../../lib/server/business/group/profile/get')
  const writeTeamAggregate = require('../../lib/server/business/aggregates/team/write')

  const submitted = new Date().toISOString()
  const submittedBy = 'createTeamAggregateScript'
  const flowId = uuidv4()

  const listOfAggregatesToCreate = []

  for (let i = 0; i < aggregatesToCreate.length; i++) {
    const aggregateInfo = aggregatesToCreate[i]

    try {
      const [groupProfile, subGroup] = await Promise.allSettled([
        getGroupProfile(submitted, submittedBy, mode, flowId, {
          companyId: aggregateInfo.companyId
        }),
        getSubGroup(dynamodb, dynamodbTableName, mode, aggregateInfo.groupId)
      ])

      const takenSlots = aggregateInfo.slots.filter(s => s.accountId !== undefined)
      const accountAggregates = await Promise.all(
        takenSlots.map(slot => getAccountAggregate(dynamodb, dynamodbTableName, mode, slot.accountId))
      )

      let totalSteps = 0
      let totalDistance = 0
      if (accountAggregates.length > 0) {
        totalSteps = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalSteps, 0)
        totalDistance = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalDistance, 0)
      }

      const averages = AggregateCalculations.calculateAverage(takenSlots.length, totalSteps, totalDistance)

      const teamAggregateData = {
        createdAt: submitted,
        createdBy: submittedBy,
        structureVersion: 1,
        teamAggregateId: aggregateInfo.groupId,
        totalSteps: totalSteps,
        totalDistance: totalDistance,
        averageSteps: takenSlots.length === 0 ? 0 : averages.averageSteps,
        averageDistance: takenSlots.length === 0 ? 0 : averages.averageDistance,
        totalTeams: 9999999,
        companyId: aggregateInfo.companyId,
        name: subGroup.value.data.name,
        rank: 9999999,
        groupType: aggregateInfo.groupType,
        extendedName: `${subGroup.value.data.name} (${groupProfile.value.name})`,
        hasLogo: !!groupProfile.value.logo
      }

      listOfAggregatesToCreate.push(teamAggregateData)

      console.log(`Added Subgroup TeamAggregate ${i + 1} of ${aggregatesToCreate.length}`)
    } catch (e) {
      console.log(`Failed To create Subgroup TeamAggregate for ${aggregateInfo} => ${e}`)
    }
  }

  let successFulCreations = 0

  if (listOfAggregatesToCreate.length > 0 && doWrite) {
    const outcomes = await Promise.allSettled(
      listOfAggregatesToCreate.map(item => writeTeamAggregate(submitted, submittedBy, mode, flowId, item))
    )

    successFulCreations = outcomes.filter(o => o.status === 'fulfilled').length
  } else {
    console.log('Dry Run => to create these aggregates you must run this script with --write')
    console.log()
    console.log(util.inspect(listOfAggregatesToCreate, false, null, true /* enable colors */))

    successFulCreations = listOfAggregatesToCreate.length
  }

  console.log('All done 👍')

  return successFulCreations
}

/***
 * Create teamAggregates based on the given list of aggregate information
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param aggregatesToCreate    array with info to create the TeamAggregate
 * @param doWrite
 * @returns {Promise<*>}
 */
async function createTeamAggregates (dynamodb, dynamodbTableName, mode, aggregatesToCreate, doWrite) {
  const getGroupProfile = require('../../lib/server/business/group/profile/get')
  const writeTeamAggregate = require('../../lib/server/business/aggregates/team/write')

  const submitted = new Date().toISOString()
  const submittedBy = 'createTeamAggregateScript'
  const flowId = uuidv4()

  const listOfAggregatesToCreate = []

  for (let i = 0; i < aggregatesToCreate.length; i++) {
    const aggregateInfo = aggregatesToCreate[i]

    try {
      const groupProfile = await getGroupProfile(submitted, submittedBy, mode, flowId, {
        teamId: aggregateInfo.groupId
      })

      const takenSlots = aggregateInfo.slots.filter(s => s.accountId !== undefined)
      const accountAggregates = await Promise.all(
        takenSlots.map(slot => getAccountAggregate(dynamodb, dynamodbTableName, mode, slot.accountId))
      )

      let totalSteps = 0
      let totalDistance = 0
      if (accountAggregates.length > 0) {
        totalSteps = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalSteps, 0)
        totalDistance = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalDistance, 0)
      }

      const averages = AggregateCalculations.calculateAverage(takenSlots.length, totalSteps, totalDistance)

      const teamAggregateData = {
        createdAt: submitted,
        createdBy: submittedBy,
        structureVersion: 1,
        teamAggregateId: aggregateInfo.groupId,
        totalSteps: totalSteps,
        totalDistance: totalDistance,
        averageSteps: takenSlots.length === 0 ? 0 : averages.averageSteps,
        averageDistance: takenSlots.length === 0 ? 0 : averages.averageDistance,
        totalTeams: 9999999,
        companyId: undefined,
        name: groupProfile.name,
        rank: 9999999,
        groupType: aggregateInfo.groupType,
        extendedName: undefined,
        hasLogo: !!groupProfile.logo
      }

      listOfAggregatesToCreate.push(teamAggregateData)

      console.log(`Added TeamAggregate ${i + 1} of ${aggregatesToCreate.length}`)
    } catch (e) {
      console.log(`Failed To create TeamAggregate for ${aggregateInfo} => ${e}`)
    }
  }

  let successFulCreations = 0

  if (listOfAggregatesToCreate.length > 0 && doWrite) {
    const outcomes = await Promise.allSettled(
      listOfAggregatesToCreate.map(item => writeTeamAggregate(submitted, submittedBy, mode, flowId, item))
    )

    successFulCreations = outcomes.filter(o => o.status === 'fulfilled').length
  } else {
    console.log('Dry Run => to create these aggregates you must run this script with --write')
    console.log()
    console.log(util.inspect(listOfAggregatesToCreate, false, null, true /* enable colors */))

    successFulCreations = listOfAggregatesToCreate.length
  }

  console.log('All done 👍')

  return successFulCreations
}

/***
 * Create CompanyAggregates based on the given list of aggregate information
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param aggregatesToCreate    array with info to create the TeamAggregate
 * @param doWrite
 * @returns {Promise<*>}
 */
async function createCompanyAggregates (dynamodb, dynamodbTableName, mode, aggregatesToCreate, doWrite) {
  const getGroupProfile = require('../../lib/server/business/group/profile/get')
  const getCompany = require('../../lib/server/business/company/get')
  const writeCompanyAggregate = require('../../lib/server/business/aggregates/company/write')

  const submitted = new Date().toISOString()
  const submittedBy = 'createCompanyAggregateScript'
  const flowId = uuidv4()

  const listOfAggregatesToCreate = []

  for (let i = 0; i < aggregatesToCreate.length; i++) {
    const aggregateInfo = aggregatesToCreate[i]

    try {
      const groupProfile = await getGroupProfile(submitted, submittedBy, mode, flowId, {
        teamId: aggregateInfo.groupId
      })

      const company = await getCompany(submitted, submittedBy, mode, flowId, {
        companyId: aggregateInfo.groupId
      })

      const takenSlots = aggregateInfo.slots.filter(s => s.accountId !== undefined)
      const accountAggregates = await Promise.all(
        takenSlots.map(slot => getAccountAggregate(dynamodb, dynamodbTableName, mode, slot.accountId))
      )

      let totalSteps = 0
      let totalDistance = 0
      if (accountAggregates.length > 0) {
        totalSteps = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalSteps, 0)
        totalDistance = accountAggregates.reduce((partialSum, a) => partialSum + a.data.totalDistance, 0)
      }

      const averages = AggregateCalculations.calculateAverage(takenSlots.length, totalSteps, totalDistance)

      const companyAggregateData = {
        createdAt: submitted,
        createdBy: submittedBy,
        structureVersion: 1,
        companyId: aggregateInfo.groupId,
        totalSteps: totalSteps,
        totalDistance: totalDistance,
        averageSteps: takenSlots.length === 0 ? 0 : averages.averageSteps,
        averageDistance: takenSlots.length === 0 ? 0 : averages.averageDistance,
        totalCompanies: 9999999,
        name: groupProfile.name,
        rank: 9999999,
        unit: company.unit,
        hasLogo: !!groupProfile.logo
      }

      listOfAggregatesToCreate.push(companyAggregateData)

      console.log(`Added CompanyAggregate ${i + 1} of ${aggregatesToCreate.length}`)
    } catch (e) {
      console.log(`Failed To create CompanyAggregate for ${aggregateInfo} => ${e}`)
    }
  }

  let successFulCreations = 0

  if (listOfAggregatesToCreate.length > 0 && doWrite) {
    const outcomes = await Promise.allSettled(
      listOfAggregatesToCreate.map(item => writeCompanyAggregate(submitted, submittedBy, mode, flowId, item))
    )

    successFulCreations = outcomes.filter(o => o.status === 'fulfilled').length
  } else {
    console.log('Dry Run => to create these aggregates you must run this script with --write')
    console.log()
    console.log(util.inspect(listOfAggregatesToCreate, false, null, true /* enable colors */))

    successFulCreations = listOfAggregatesToCreate.length
  }

  console.log('All done 👍')

  return successFulCreations
}

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

/***
 * Collect the data for creating company/team aggregates for the existing data
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @returns {Promise<{subgroupAggregateInfo: [], companyAggregateInfo: [], teamAggregateInfo: []}>}
 */
async function getInfoForAggregates (dynamodb, dynamodbTableName, mode) {
  const indexPartitionKey = `/${mode}/slots`

  const allSlots = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'D')

  const allSlotsData = allSlots.map(s => s.data)

  const companyAggregateInfo = []
  const teamAggregateInfo = []
  const subgroupAggregateInfo = []

  let existingCompanyInfo
  let existingTeamInfo

  allSlots.forEach(s => {
    switch (s.data.groupType) {
      case GroupType.companyType:
        existingCompanyInfo = companyAggregateInfo.filter(i => i.groupId === s.data.groupId)

        if (existingCompanyInfo.length === 0) {
          const slotsForCompany = allSlotsData.filter(i => i.groupId === s.data.groupId)

          companyAggregateInfo.push({
            type: 'companyAggregateInfo',
            groupId: s.data.groupId,
            groupType: s.data.groupType,
            slots: slotsForCompany
          })
        }

        if (s.data.subgroupId !== undefined) {
          existingTeamInfo = teamAggregateInfo.filter(i => i.groupId === s.data.subgroupId)

          if (existingCompanyInfo.length === 0) {
            const slotsForSubgroup = allSlotsData.filter(i => i.subgroupId === s.data.subgroupId)

            subgroupAggregateInfo.push({
              type: 'subgroupAggregateInfo',
              groupId: s.data.subgroupId,
              groupType: s.data.groupType,
              companyId: s.data.groupId,
              slots: slotsForSubgroup
            })
          }
        }

        break
      case GroupType.teamType:
        existingTeamInfo = teamAggregateInfo.filter(i => i.groupId === s.data.groupId)

        if (existingTeamInfo.length === 0) {
          const slotsForGroup = allSlotsData.filter(i => i.groupId === s.data.groupId)

          if (slotsForGroup.length > 1) {
            teamAggregateInfo.push({
              type: 'teamAggregateInfo',
              groupId: s.data.groupId,
              groupType: s.data.groupType,
              slots: slotsForGroup
            })
          }
        }
        break
      default:
        console.log('Do NOTHING => code should never be reached')
    }
  })

  return { companyAggregateInfo, teamAggregateInfo, subgroupAggregateInfo }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWrite = process.argv[3] === '--write'

  const companyAggregatesBefore = await getExistingAggregates(dynamodb, dynamodbTableName, mode, GroupType.companyType)
  const teamAggregatesBefore = await getExistingAggregates(dynamodb, dynamodbTableName, mode, GroupType.teamType)

  const aggregateInfo = await getInfoForAggregates(dynamodb, dynamodbTableName, mode)

  const finalCompanyAggregateInfoToCreate = await filterOnlyNonExistingAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    GroupType.companyType,
    aggregateInfo.companyAggregateInfo
  )

  const finalTeamAggregateInfoToCreate = await filterOnlyNonExistingAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    GroupType.teamType,
    aggregateInfo.teamAggregateInfo
  )

  const finalSubgroupTeamAggregateInfoToCreate = await filterOnlyNonExistingAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    GroupType.teamType,
    aggregateInfo.subgroupAggregateInfo
  )

  logDetails('CompanyAggregate Info', finalCompanyAggregateInfoToCreate)
  logDetails('TeamAggregate Info', finalTeamAggregateInfoToCreate)
  logDetails('SubgroupTeamAggregate Info', finalSubgroupTeamAggregateInfoToCreate)

  const successfulTeamCreations = await createTeamAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    finalTeamAggregateInfoToCreate,
    doWrite
  )

  const successfulSubgroupTeamCreations = await createSubgroupTeamAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    finalSubgroupTeamAggregateInfoToCreate,
    doWrite
  )

  const successfulCompanyCreations = await createCompanyAggregates(
    dynamodb,
    dynamodbTableName,
    mode,
    finalCompanyAggregateInfoToCreate,
    doWrite
  )

  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log('RESULTS:')
  console.log('')

  const companyAggregatesAfter = await getExistingAggregates(dynamodb, dynamodbTableName, mode, GroupType.companyType)
  const teamAggregatesAfter = await getExistingAggregates(dynamodb, dynamodbTableName, mode, GroupType.teamType)

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    isDryRun: !doWrite,
    team_aggregate_info: {
      team_before: teamAggregatesBefore.filter(t => t.data.groupType === GroupType.teamType).length,
      team_to_be_created: finalTeamAggregateInfoToCreate.length,
      team_created: successfulTeamCreations,
      team_after: teamAggregatesAfter.filter(t => t.data.groupType === GroupType.teamType).length
    },
    subgroup_team_aggregate_info: {
      subgroup_team_before: teamAggregatesBefore.filter(t => t.data.groupType === GroupType.companyType).length,
      subgroup_team_to_be_created: finalSubgroupTeamAggregateInfoToCreate.length,
      subgroup_team_created: successfulSubgroupTeamCreations,
      subgroup_team_after: teamAggregatesAfter.filter(t => t.data.groupType === GroupType.companyType).length
    },
    company_aggregate_info: {
      company_before: companyAggregatesBefore.length,
      company_to_be_created: finalCompanyAggregateInfoToCreate.length,
      company_created: successfulCompanyCreations,
      company_after: companyAggregatesAfter.length
    }
  }

  logDetails('Team/Company Aggregates', statistics)
}
modeScript(
  'createTeamOrCompanyAggregateForExistingAccounts',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: create an accountaggregate for an existing profile if it not already exists
this script does scan all the accounts, checks if they have 1 or more slots. Then checks
if the account has an aggregate, if not create one for that account
`
)
