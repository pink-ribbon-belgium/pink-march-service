#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

// const getDynamodb = require('./_getDynamodb')
// const path = require('path')
// process.env.NODE_CONFIG_DIR = path.join(__dirname, '..', '..', 'config')
// const config = require('config')
const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4
const util = require('util')

async function logExistingActivity (mode, accountId, phase) {
  const getByIndex = require('../../lib/server/aws/dynamodb/getByIndex')

  const indexPartitionKey = `/${mode}/activity/account/${accountId}`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  try {
    const activitiesBefore = await getByIndex(mode, indexPartitionKey, index)
    console.log(`ACTIVITIES ${phase} => ${activitiesBefore.length}`)
    const userFriendlyOutput = activitiesBefore.map(item => ({
      date: item.data.activityDate,
      steps: item.data.numberOfSteps
    }))

    console.log(`${util.inspect(userFriendlyOutput, false, null, true /* enable colors */)}`)
  } catch (e) {
    console.log('No activities found')
  }
}

async function execute (mode, dynamodbTableName) {
  const updateActivities = require('../../lib/server/business/account/activity/updateActivities')
  const getActual = require('../../lib/server/aws/dynamodb/getActual')

  /* Note: replace with accountId from database */
  const accountId = 'fMInaPd9La'

  await logExistingActivity(mode, accountId, 'BEFORE')

  const key = `/${mode}/account/${accountId}/trackerConnection`
  const actualTrackerConnection = await getActual(mode, key, true)

  const sot = new Date().toISOString()
  const flowId = uuidv4()

  await updateActivities(flowId, sot, mode, actualTrackerConnection.data)

  await logExistingActivity(mode, accountId, 'AFTER')
}

modeScript(
  'testGetFitbitTrackerData',
  execute,
  `
Run this script with argument \`<MODE>\`.
`
)
