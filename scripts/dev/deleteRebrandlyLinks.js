/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const modeScript = require('./_modeScript')
const uuidv4 = require('uuid').v4

const sleep = milliseconds => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

async function execute (mode, dynamodbTableName) {
  const deleteShortLink = require('../../lib/server/business/group/shortLink/deleteShortLink')
  const getShortLinks = require('../../lib/server/business/group/shortLink/getAll')

  const sot = new Date().toISOString()
  const sub = 'delete-links-script'
  const flowId = uuidv4()

  let shortLinks

  shortLinks = await getShortLinks(sot, sub, mode, flowId)
  console.log('#shortLinks ::::::::', shortLinks.length)
  // timeouts are set to stay within rate limit
  // up to 10 API calls per second
  // up to 20,000 API calls per hour*
  while (shortLinks.length > 0) {
    for (const item of shortLinks) {
      const deleteResult = await deleteShortLink(sot, sub, mode, flowId, { linkId: item.linkId })
      console.log(deleteResult)
      await sleep(200)
    }
    shortLinks = await getShortLinks(sot, sub, mode, flowId)
    console.log('#shortLinks ::::::::', shortLinks.length)
    console.log()
    console.log()
  }
  console.log('deleted links')
}

modeScript(
  'deleteRebrandlyLinks',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Delete all shortLinks.
`
)
