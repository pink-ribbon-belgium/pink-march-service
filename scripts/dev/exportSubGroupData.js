#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const fs = require('fs')
const ObjectsToCSV = require('objects-to-csv')
const moment = require('moment')

const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

/***
 * Get the Group profile data
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param groupId
 * @returns {Promise<GroupProfileDTO>}
 */
async function getGroupProfile (dynamodb, dynamodbTableName, mode, groupId) {
  const key = `/${mode}/group/${groupId}/publicProfile`
  const result = await getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
  return result.data
}

async function getTeamAggregate (dynamodb, dynamodbTableName, mode, subGroupId) {
  const key = `/${mode}/teamAggregate/${subGroupId}`
  const result = await dynamodbHelper.queryActualByKey(dynamodb, dynamodbTableName, mode, key)

  return result.data
}

/**
 * Returns all Subgroups
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @returns {Promise<SubGroupDTO[]>}
 */
async function getSubGroups (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const indexPartitionKey = `/${mode}/subgroup`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result.map(x => x.data)
}

async function getNumberOfSubgroupMembers (dynamodb, dynamodbTableName, mode, subgroupData) {
  const indexPartitionKey = `/${mode}/group/${subgroupData.groupId}/members`

  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const results = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  const subgroupMembers = results.filter(slot => slot.data.subgroupId === subgroupData.id)
  return subgroupMembers.length
}

/***
 * Enrich the given SubgroupData with data from the groupProfile & teamAggregate
 * @param dynamodb
 * @param dynamodbTableName {dynamodbTableName} name of table in DynmoDb
 * @param mode {mode}
 * @param subGroupData {SubGroupDTO[]} subgroup data
 * @returns {Promise<unknown[]>}
 */
async function enrichSubgroupData (dynamodb, dynamodbTableName, mode, subGroupData) {
  const enrichedSubgroups = await Promise.all(
    subGroupData.map(async itemData => {
      const [company, teamAggregate, numberOfMembers] = await Promise.all([
        getGroupProfile(dynamodb, dynamodbTableName, mode, itemData.groupId),
        getTeamAggregate(dynamodb, dynamodbTableName, mode, itemData.id),
        getNumberOfSubgroupMembers(dynamodb, dynamodbTableName, mode, itemData)
      ])

      const enrichedData = {
        subgroupId: itemData.id,
        name: itemData.name,
        companyName: company !== undefined ? company.name : 'UNKNOWN',
        totalSteps: teamAggregate !== undefined ? teamAggregate.totalSteps : 0,
        totalDistance: teamAggregate !== undefined ? teamAggregate.totalDistance : 0,
        averageSteps: teamAggregate !== undefined ? teamAggregate.averageSteps : 0,
        averageDistance: teamAggregate !== undefined ? teamAggregate.averageDistance : 0,
        rank: teamAggregate !== undefined ? teamAggregate.rank : 0,
        members: numberOfMembers
      }

      return enrichedData
    })
  )

  return enrichedSubgroups
}

/**
 * Export the result to a Csv file
 * @param result {Array}
 * @param fileName {string}
 * @returns {Promise<void>}
 */
async function exportToCsv (result, fileName) {
  const csv = new ObjectsToCSV(result)

  const dateString = moment().format('YYYYMMDD')

  var outputFolder = './output'
  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  await csv.toDisk(`./output/${fileName}_${dateString}.csv`, { append: false })
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  console.info(`==> Getting all SubGroup data for ${mode} ...`)
  const subGroupData = await getSubGroups(dynamodb, dynamodbTableName, mode)

  console.info('==> Enrich SubGroup data ...')
  const finalResult = await enrichSubgroupData(dynamodb, dynamodbTableName, mode, subGroupData)

  console.info('==> Write SubGroup data to csv ...')
  const fileName = `SubGroupData_${mode}`
  await exportToCsv(finalResult, fileName)

  console.log('===> Export SubGroup Data finished')
}

modeScript(
  'exportSubGroupData',
  execute,
  `
Run this script with argument \`<MODE>\`.

Purpose: Export of subgroups enriched with steps, rank & nubmer of members.
`
)
