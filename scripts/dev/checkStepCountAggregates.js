#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const GroupType = require('./../../lib/api/group/GroupType')
// const TrackerType = require('./../../lib/api/account/trackerConnection/TrackerType')
// const AggregateCalculations = require('./../../lib/api/aggregates/calculation/AggregateCalculations')

const scriptName = 'checkStepCountAggregates'

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

function logHeader (scriptName, mode, doUpdate) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log(`Execute ${scriptName}`)
  console.log(`MODE: ${mode}`)
  console.log('')
  if (doUpdate) {
    console.log('===> WARNING : All changes will be written to the database !!!!')
  } else {
    console.log('===> This is a dry-run !')
  }
  console.log('')
  console.log('')
}

function writeToFile (mode, fileName, result) {
  var fs = require('fs')

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  const filePath = `output/${fileName}_${mode}.json`
  fs.writeFile(filePath, JSON.stringify(result, null, 2), err => {
    if (err) throw err
    console.log('')
    console.log('Data written to file: ' + filePath)
  })
}

/***
 * Returns all existing accountAggregates
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param groupType
 * @returns {Promise<[]>}
 */
async function getExistingAggregates (dynamodb, dynamodbTableName, mode, groupType) {
  const indexPartitionKey = groupType === GroupType.teamType ? `/${mode}/teamAggregate` : `/${mode}/companyAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

/***
 * Returns all free/claimed slots
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @param indexPartitionKey
 * @param indexName => A/B/C/D/E
 * @returns {Promise<[]>}
 */
async function getSlots (dynamodb, dynamodbTableName, mode, indexPartitionKey, indexName) {
  const index = {
    indexName: `Index_${indexName}`,
    partitionKey: `partition_key_${indexName}`,
    sortKey: `sort_key_${indexName}`
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 3000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

// async function getActivitiesForAccount (dynamodb, dynamodbTableName, mode, accountId) {
//   const index = {
//     indexName: 'Index_B',
//     partitionKey: 'partition_key_B',
//     sortKey: 'sort_key_B'
//   }
//
//   const indexPartitionKey = `/${mode}/activity/account/${accountId}`
//
//   const params = {
//     TableName: dynamodbTableName,
//     IndexName: index.indexName,
//     ExpressionAttributeNames: {
//       '#partitionKey': index.partitionKey
//     },
//     ExpressionAttributeValues: {
//       ':partitionKey': indexPartitionKey
//     },
//     KeyConditionExpression: '#partitionKey = :partitionKey',
//     ScanIndexForward: false,
//     Limit: 2000,
//     ExclusiveStartKey: undefined
//   }
//
//   const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
//
//   return result
// }

async function getActivities (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const indexPartitionKey = `/${mode}/activity`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function verifyTotalSteps (dynamodb, dynamodbTableName, mode, existingAggregates) {
  const verifiedResults = []

  const allActivities = await getActivities(dynamodb, dynamodbTableName, mode)

  for (let i = 0; i < existingAggregates.length; i++) {
    const existingAggregateInfo = existingAggregates[i]

    let aggregateType = ''
    if (existingAggregateInfo.key.includes('companyAggregate')) {
      aggregateType = 'CompanyAggregate'
    } else {
      if (existingAggregateInfo.key.includes('teamAggregate')) {
        if (existingAggregateInfo.data.groupType === GroupType.companyType) {
          aggregateType = 'SubGroupAggregate'
        } else {
          aggregateType = 'TeamAggregate'
        }
      }
    }

    let indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
    let memberSlots = []

    switch (aggregateType) {
      case 'CompanyAggregate':
        indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
        memberSlots = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
        break
      case 'TeamAggregate':
        indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.teamAggregateId}/members`
        memberSlots = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
        break
      case 'SubGroupAggregate':
        indexPartitionKey = `/${mode}/group/${existingAggregateInfo.data.companyId}/members`
        // eslint-disable-next-line no-case-declarations
        const companyMembers = await getSlots(dynamodb, dynamodbTableName, mode, indexPartitionKey, 'B')
        memberSlots = companyMembers.filter(s => s.data.subgroupId === existingAggregateInfo.data.teamAggregateId)
        break
    }

    // const tempActivitiesMembers = await Promise.all(
    //   memberSlots.map(slot => getActivitiesForAccount(dynamodb, dynamodbTableName, mode, slot.data.accountId))
    // )

    const tempActivitiesMembers = memberSlots.map(slot =>
      allActivities.filter(a => a.data.accountId === slot.data.accountId)
    )

    const combinedMemberActivities = tempActivitiesMembers.flat()
    // if (tempActivitiesMembers.length !== 0) {
    //   combinedMemberActivities = tempActivitiesMembers.reduce(function (prev, curr) {
    //     return prev.concat(curr)
    //   })
    // }

    const totalStepsActivities = combinedMemberActivities.reduce(
      (partialSum, a) => partialSum + a.data.numberOfSteps,
      0
    )

    const totalDistanceActivities = combinedMemberActivities.reduce((partialSum, a) => partialSum + a.data.distance, 0)

    //
    // const averagesActivities = AggregateCalculations.calculateAverage(
    //   memberSlots.length,
    //   totalStepsActivities,
    //   totalDistanceActivities
    // )
    //

    // const averagesAggregates = AggregateCalculations.calculateAverage(
    //   memberSlots.length,
    //   totalStepsAccountAggregates,
    //   totalDistanceAccountAggregates
    // )

    const verifiedAggregate = {
      mode: mode,
      date: new Date().toISOString(),
      aggregateInfo: {
        key: existingAggregateInfo.key,
        name:
          existingAggregateInfo.key.includes('teamAggregate') &&
          existingAggregateInfo.data.groupType === GroupType.companyType
            ? existingAggregateInfo.data.extendedName
            : existingAggregateInfo.data.name,
        finalResult: existingAggregateInfo.data.totalSteps === totalStepsActivities ? 'OK' : 'FAILED',
        type: aggregateType,
        memberCount: memberSlots.length,
        deviation: totalStepsActivities - existingAggregateInfo.data.totalSteps
      },
      totalstepsInfo: {
        from_Aggregate: existingAggregateInfo.data.totalSteps,
        calculated_by_activities: totalStepsActivities
      },
      totalDistanceInfo: {
        from_Aggregate: existingAggregateInfo.data.totalDistance,
        calculated_by_activities: totalDistanceActivities
      }
      // averageStepsInfo: {
      //   from_Aggregate: existingAggregateInfo.data.averageSteps,
      //   calculated_by_activities: averagesActivities.averageSteps
      // },
      // averageDistanceInfo: {
      //   from_Aggregate: existingAggregateInfo.data.averageDistance,
      //   calculated_by_activities: averagesActivities.averageDistance
      // }
    }

    verifiedResults.push(verifiedAggregate)
  }

  return verifiedResults
}

async function updateAggregate (dynamodb, dynamodbTableName, mode, aggregateKey, totalSteps, totalDistance) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#totalSteps = :totalSteps, #data.#totalDistance = :totalDistance',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance'
    },
    ExpressionAttributeValues: {
      ':totalSteps': totalSteps,
      ':totalDistance': totalDistance
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    //    console.log(result)
    return result
  } catch (e) {
    console.log(e)
  }
}

async function processAggregates (dynamodb, dynamodbTableName, mode, groupType, doUpdate, doWriteToOutput, showAll) {
  const aggregatesBefore = await getExistingAggregates(dynamodb, dynamodbTableName, mode, groupType)
  const verifiedAggregates = await verifyTotalSteps(dynamodb, dynamodbTableName, mode, aggregatesBefore)

  let updateResults
  if (doUpdate) {
    updateResults = await Promise.all(
      verifiedAggregates.map(agg =>
        updateAggregate(
          dynamodb,
          dynamodbTableName,
          mode,
          agg.aggregateInfo.key,
          agg.totalstepsInfo.calculated_by_activities,
          agg.totalDistanceInfo.calculated_by_activities
        )
      )
    )
  }

  const summary = {
    mode: mode,
    date: new Date().toISOString(),
    failed: verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'FAILED').length,
    ok: verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'OK').length,
    aggregates: showAll ? verifiedAggregates : verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'FAILED'),
    companyUpdateResults: updateResults
    // aggregates_no_members: verifiedAggregates.filter(a => a.aggregateInfo.memberCount === 0)
  }

  if (doWriteToOutput) {
    writeToFile(mode, `${scriptName}_${groupType}`, summary)
  } else {
    logDetails(`Summary ${groupType}Aggregates`, summary)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWriteToOutput = process.argv.some(x => x === '--output')
  const doUpdate = process.argv.some(x => x === '--write')
  const showAll = process.argv.some(x => x === '--all')
  const teamsonly = process.argv.some(x => x === '--teamsonly')

  logHeader(scriptName, mode, doUpdate)

  if (!teamsonly) {
    await processAggregates(
      dynamodb,
      dynamodbTableName,
      mode,
      GroupType.companyType,
      doUpdate,
      doWriteToOutput,
      showAll
    )
  }

  await processAggregates(dynamodb, dynamodbTableName, mode, GroupType.teamType, doUpdate, doWriteToOutput, showAll)
}

modeScript(
  'statistics',
  execute,
  `
Run this script with argument \`<MODE>\`.
Purpose: Verify steps of Team & Company Aggregates with real activities.

Parameters:
    \`--all\`       show all the accountAggregates otherwise only those with deviating number of steps (optional)
    \`--output\`    write the output to a file
    \`--write\`     update the aggregates in the database
    \`--teamsonly\`  verify only the team aggregates in the database

The script does a dry run, unless \`--write\` is given as an argument.
`
)
