#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const GroupType = require('./../../lib/api/group/GroupType')

const scriptName = 'checkStepCountAccountAggregates'

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

function logHeader (scriptName, mode, doUpdate) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log(`Execute ${scriptName}`)
  console.log(`MODE: ${mode}`)
  console.log('')
  if (doUpdate) {
    console.log('===> WARNING : All changes will be written to the database !!!!')
  } else {
    console.log('===> This is a dry-run !')
  }
  console.log('')
  console.log('')
}

function writeToFile (mode, fileName, result) {
  var fs = require('fs')

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  const filePath = `output/${fileName}_${mode}.json`
  fs.writeFile(filePath, JSON.stringify(result, null, 2), err => {
    if (err) throw err
    console.log('')
    console.log('Data written to file: ' + filePath)
  })
}

/***
 * Returns all existing accountAggregates
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @returns {Promise<[]>}
 */
async function getExistingAggregates (dynamodb, dynamodbTableName, mode) {
  console.log('===> Get existing ACCOUNT aggregates...')

  const indexPartitionKey = `/${mode}/accountAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

// async function getActivitiesForAccount (dynamodb, dynamodbTableName, mode, accountId) {
//   const index = {
//     indexName: 'Index_B',
//     partitionKey: 'partition_key_B',
//     sortKey: 'sort_key_B'
//   }
//
//   const indexPartitionKey = `/${mode}/activity/account/${accountId}`
//
//   const params = {
//     TableName: dynamodbTableName,
//     IndexName: index.indexName,
//     ExpressionAttributeNames: {
//       '#partitionKey': index.partitionKey
//     },
//     ExpressionAttributeValues: {
//       ':partitionKey': indexPartitionKey
//     },
//     KeyConditionExpression: '#partitionKey = :partitionKey',
//     ScanIndexForward: false,
//     Limit: 2000,
//     ExclusiveStartKey: undefined
//   }
//
//   const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)
//
//   return result
// }

async function getActivities (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const indexPartitionKey = `/${mode}/activity`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function verifyTotalSteps (dynamodb, dynamodbTableName, mode, existingAggregates) {
  console.log('===> Verifying existing ACCOUNT aggregates...')

  const verifiedResults = []

  const allActivities = await getActivities(dynamodb, dynamodbTableName, mode)

  for (let i = 0; i < existingAggregates.length; i++) {
    const existingAggregateInfo = existingAggregates[i]

    // const tempActivitiesMembers = await Promise.all(
    //   memberSlots.map(slot => getActivitiesForAccount(dynamodb, dynamodbTableName, mode, slot.data.accountId))
    // )

    const memberActivities = allActivities.filter(a => a.data.accountId === existingAggregateInfo.data.accountId)

    const totalStepsActivities = memberActivities.reduce((partialSum, a) => partialSum + a.data.numberOfSteps, 0)
    const totalDistanceActivities = memberActivities.reduce((partialSum, a) => partialSum + a.data.distance, 0)

    const verifiedAggregate = {
      mode: mode,
      date: new Date().toISOString(),
      aggregateInfo: {
        key: existingAggregateInfo.key,
        name:
          existingAggregateInfo.key.includes('teamAggregate') &&
          existingAggregateInfo.data.groupType === GroupType.companyType
            ? existingAggregateInfo.data.extendedName
            : existingAggregateInfo.data.name,
        finalResult: existingAggregateInfo.data.totalSteps === totalStepsActivities ? 'OK' : 'FAILED',
        type: 'accountAggregate'
      },
      totalstepsInfo: {
        from_Aggregate: existingAggregateInfo.data.totalSteps,
        calculated_by_activities: totalStepsActivities,
        deviation: existingAggregateInfo.data.totalSteps - totalStepsActivities
      },
      totalDistanceInfo: {
        from_Aggregate: existingAggregateInfo.data.totalDistance,
        calculated_by_activities: totalDistanceActivities
      },
      activityInfo: memberActivities.map(
        a => `${a.data.activityDate} => ${a.key} => ${a.data.trackerType} => ${a.data.numberOfSteps}`
      )
    }

    verifiedResults.push(verifiedAggregate)
  }

  return verifiedResults
}

async function updateAggregate (dynamodb, dynamodbTableName, mode, aggregateKey, totalSteps, totalDistance) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = :totalSteps, #data.#totalDistance = :totalDistance, #sort_key_1 = :totalSteps, #sort_key_2 = :totalSteps, #sort_key_3 = :totalSteps',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance',
      '#sort_key_1': 'sort_key_1',
      '#sort_key_2': 'sort_key_2',
      '#sort_key_3': 'sort_key_3'
    },
    ExpressionAttributeValues: {
      ':totalSteps': totalSteps,
      ':totalDistance': totalDistance
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    //    console.log(result)
    return result
  } catch (e) {
    console.log(e)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWriteToOutput = process.argv.some(x => x === '--output')
  const updateAll = process.argv.some(x => x === '--update')
  const updateFailed = process.argv.some(x => x === '--updatefailed')
  const showAll = process.argv.some(x => x === '--showall')

  logHeader(scriptName, mode, updateAll)

  const existingAggregates = await getExistingAggregates(dynamodb, dynamodbTableName, mode)
  const verifiedAggregates = await verifyTotalSteps(dynamodb, dynamodbTableName, mode, existingAggregates)

  let updateResults
  if (updateAll || updateFailed) {
    let aggregatesToUpdate
    if (updateFailed) {
      aggregatesToUpdate = verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'FAILED')
    } else {
      aggregatesToUpdate = verifiedAggregates
    }

    updateResults = await Promise.all(
      aggregatesToUpdate.map(agg =>
        updateAggregate(
          dynamodb,
          dynamodbTableName,
          mode,
          agg.aggregateInfo.key,
          agg.totalstepsInfo.calculated_by_activities,
          agg.totalDistanceInfo.calculated_by_activities
        )
      )
    )
  }

  const summary = {
    mode: mode,
    date: new Date().toISOString(),
    failed: verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'FAILED').length,
    ok: verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'OK').length,
    aggregates: showAll ? verifiedAggregates : verifiedAggregates.filter(a => a.aggregateInfo.finalResult === 'FAILED'),
    updateResults
  }

  if (doWriteToOutput) {
    writeToFile(mode, scriptName, summary)
  } else {
    logDetails('Summary AccountAggregates', summary)
  }
}

modeScript(
  scriptName,
  execute,
  `
Run this script with argument \`<MODE>\`.
Purpose: Verify steps from Account Aggregates with real activities.

Parameters:
    \`--showall\`       show all the accountAggregates otherwise only those with deviating number of steps (optional)
    \`--output\`        write the output to a file
    \`--update\`        update all the accountAggregates in the database
    \`--updatefailed\`  update only the failed accountAggregates in the database

The script does a dry run, unless \`--write\` is given as an argument.
`
)
