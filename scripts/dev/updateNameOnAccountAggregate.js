#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')

const getDynamodb = require('./_getDynamodb')
const dynamodbHelper = require('./_dynamodbHelper')
const modeScript = require('./_modeScript')
// const AccountAggregate = require('../../lib/server/business/aggregates/account/AccountAggregate')

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

async function getVersionAtOrBefore (dynamodb, dynamodbTableName, mode, key) {
  const submittedBefore = new Date().toISOString()

  const params = {
    TableName: dynamodbTableName,
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submittedBefore
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryOne(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAccountProfile (dynamodb, dynamodbTableName, mode, accountId) {
  const key = `/${mode}/account/${accountId}/publicProfile`
  return getVersionAtOrBefore(dynamodb, dynamodbTableName, mode, key)
}

async function getAccountAggregates (dynamodb, dynamodbTableName, mode) {
  const indexPartitionKey = `/${mode}/accountAggregate`

  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const results = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return results
}

async function updateAccountAggregates (dynamodb, dynamodbTableName, mode, aggregates, doWrite) {
  const updatedAggregates = []
  const savedAggregates = []
  const failedUpdates = []

  await Promise.all(
    aggregates.map(async aggregate => {
      const accountId = (aggregate.data.name = aggregate.data.accountId)
      const accountProfile = await getAccountProfile(dynamodb, dynamodbTableName, mode, accountId)

      aggregate.data.name = `${accountProfile.data.firstName} ${accountProfile.data.lastName}`
      updatedAggregates.push(aggregate)
    })
  )

  if (doWrite) {
    await Promise.all(
      updatedAggregates.map(agg => {
        try {
          dynamodbHelper.writeToDatabase(dynamodb, dynamodbTableName, agg)
          savedAggregates.push(agg)
          console.log(`AccountAggregate updated => ${agg.key}`)
        } catch (e) {
          failedUpdates.push(agg)
          console.log(`AccountAggregate updated FAILED => ${agg.key} => ${e}`)
        }
      })
    )
  } else {
    console.log('this was a dry run')
  }

  return { updatedAggregates, savedAggregates, failedUpdates }
}

async function execute (mode, dynamodbTableName) {
  const doWrite = process.argv[3] === '--write'

  const { dynamodb } = await getDynamodb()

  const aggregates = await getAccountAggregates(dynamodb, dynamodbTableName, mode)

  const updateResults = await updateAccountAggregates(dynamodb, dynamodbTableName, mode, aggregates, doWrite)

  const statistics = {
    mode: mode,
    date: new Date().toISOString(),
    account_aggregate_info: {
      total: aggregates.length,
      updated: updateResults.updatedAggregates.length,
      saved: updateResults.savedAggregates.length,
      failedUpdates: updateResults.failedUpdates.length
    }
  }

  console.log('')
  console.log('')
  console.log('RESULTS:')
  console.log('')
  console.log('===================================================================================')
  //  logDetails('Updated', updateResults.updatedAggregates)
  //  logDetails('NOT Updated', updateResults.notUpdatedAggregates)
  logDetails('READY updates', updateResults.updatedAggregates)
  logDetails('FAILED updates', updateResults.failedUpdates)
  console.log('')
  console.log('')
  logDetails('Statistics', statistics)
}

modeScript(
  'updateNameOnAccountAggregate',
  execute,
  `
Run this script with argument \`<MODE>\`.

The script does a dry run, unless \`--write\` is given as last argument.

Purpose: Update name field of existing AccountAggregates (set '[firstName] [lastName]' instead of '[lastName] [firstName]'
`
)
