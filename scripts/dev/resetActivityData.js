#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const dynamodbHelper = require('./_dynamodbHelper')
const getDynamodb = require('./_getDynamodb')
const modeScript = require('./_modeScript')

const scriptName = 'resetActivityData'

function inspectForLog (input) {
  const parameters = { compact: false, depth: null, colors: true, showHidden: false }
  return util.inspect(input, parameters)
}

function logDetails (title, subject) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log('')
  console.log(`${title}: ${inspectForLog(subject)}`)
  console.log('')
  console.log('===================================================================================')
}

function logHeader (scriptName, mode, doUpdate) {
  console.log('')
  console.log('')
  console.log('===================================================================================')
  console.log(`Execute ${scriptName}`)
  console.log(`MODE: ${mode}`)
  console.log('')
  if (doUpdate) {
    console.log('===> WARNING : All changes will be written to the database !!!!')
  } else {
    console.log('===> This is a dry-run !')
  }
  console.log('')
  console.log('')
}

function writeToFile (mode, fileName, result) {
  var fs = require('fs')

  var outputFolder = './output'

  if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder)
  }

  const filePath = `output/${fileName}_${mode}.json`
  fs.writeFile(filePath, JSON.stringify(result, null, 2), err => {
    if (err) throw err
    console.log('')
    console.log('Data written to file: ' + filePath)
  })
}

async function getExistingAccountAggregates (dynamodb, dynamodbTableName, mode) {
  console.log('===> Get existing ACCOUNT aggregates...')

  const indexPartitionKey = `/${mode}/accountAggregate`

  return await queryAllAggregates(dynamodb, dynamodbTableName, mode, indexPartitionKey)
}

async function getExistingCompanyAggregates (dynamodb, dynamodbTableName, mode) {
  console.log('===> Get existing COMPANY aggregates...')

  const indexPartitionKey = `/${mode}/companyAggregate`

  return await queryAllAggregates(dynamodb, dynamodbTableName, mode, indexPartitionKey)
}

async function getExistingTeamAggregates (dynamodb, dynamodbTableName, mode) {
  console.log('===> Get existing TEAM aggregates...')

  const indexPartitionKey = `/${mode}/teamAggregate`

  return await queryAllAggregates(dynamodb, dynamodbTableName, mode, indexPartitionKey)
}

/***
 * Returns all existing aggregates
 * @param dynamodb
 * @param dynamodbTableName
 * @param mode
 * @indexPartitionKey unique key for the index
 * @returns {Promise<[]>}
 */
async function queryAllAggregates (dynamodb, dynamodbTableName, mode, indexPartitionKey) {
  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function getAllActivities (dynamodb, dynamodbTableName, mode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const indexPartitionKey = `/${mode}/activity`

  const params = {
    TableName: dynamodbTableName,
    IndexName: index.indexName,
    ExpressionAttributeNames: {
      '#partitionKey': index.partitionKey
    },
    ExpressionAttributeValues: {
      ':partitionKey': indexPartitionKey
    },
    KeyConditionExpression: '#partitionKey = :partitionKey',
    ScanIndexForward: false,
    Limit: 1000,
    ExclusiveStartKey: undefined
  }

  const result = await dynamodbHelper.queryAll(dynamodb, dynamodbTableName, mode, params)

  return result
}

async function resetAccountAggregate (dynamodb, dynamodbTableName, mode, aggregateKey) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = :totalSteps, ' +
      '#data.#totalDistance = :totalDistance, ' +
      '#data.#rank = :rank, ' +
      '#data.#previousLevel = :previousLevel, ' +
      '#data.#acknowledged = :acknowledged, ' +
      '#sort_key_1 = :totalSteps, ' +
      '#sort_key_2 = :totalSteps, ' +
      '#sort_key_3 = :totalSteps',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance',
      '#previousLevel': 'previousLevel',
      '#acknowledged': 'acknowledged',
      '#rank': 'rank',
      '#sort_key_1': 'sort_key_1',
      '#sort_key_2': 'sort_key_2',
      '#sort_key_3': 'sort_key_3'
    },
    ExpressionAttributeValues: {
      ':totalSteps': 0,
      ':totalDistance': 0,
      ':rank': 1,
      ':previousLevel': 1,
      ':acknowledged': false
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    //    console.log(result)
    return result
  } catch (e) {
    console.log(e)
  }
}

async function resetTeamOrCompanyAggregate (dynamodb, dynamodbTableName, mode, aggregateKey) {
  const params = {
    TableName: dynamodbTableName,
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = :totalSteps, ' +
      '#data.#totalDistance = :totalDistance, ' +
      '#data.#averageSteps = :averageSteps, ' +
      '#data.#averageDistance = :averageDistance, ' +
      '#data.#rank = :rank, ' +
      '#sort_key_1 = :averageSteps, ' +
      '#sort_key_2 = :averageSteps',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance',
      '#averageSteps': 'averageSteps',
      '#averageDistance': 'averageDistance',
      '#rank': 'rank',
      '#sort_key_1': 'sort_key_1',
      '#sort_key_2': 'sort_key_2'
    },
    ExpressionAttributeValues: {
      ':totalSteps': 0,
      ':totalDistance': 0,
      ':averageSteps': 0,
      ':averageDistance': 0,
      ':rank': 1
    },
    ReturnValues: 'ALL_NEW'
  }

  try {
    const result = await dynamodb.update(params).promise()
    //    console.log(result)
    return result
  } catch (e) {
    console.log(e)
  }
}

async function execute (mode, dynamodbTableName) {
  const { dynamodb } = await getDynamodb()

  const doWriteToOutput = process.argv.some(x => x === '--output')
  const doUpdate = process.argv.some(x => x === '--write')
  const doShowDetails = process.argv.some(x => x === '--details')

  logHeader(scriptName, mode, doUpdate)

  const [activitiesBefore, accountAggregatesBefore, teamAggregatesBefore, companyAggregatesBefore] = await Promise.all([
    getAllActivities(dynamodb, dynamodbTableName, mode),
    getExistingAccountAggregates(dynamodb, dynamodbTableName, mode),
    getExistingTeamAggregates(dynamodb, dynamodbTableName, mode),
    getExistingCompanyAggregates(dynamodb, dynamodbTableName, mode)
  ])

  let accountAggregateUpdateResults
  let teamAggregateUpdateResults
  let companyAggregateUpdateResults
  if (doUpdate) {
    await Promise.all(
      activitiesBefore.map(activity => dynamodbHelper.deleteItem(dynamodb, dynamodbTableName, activity))
    )

    accountAggregateUpdateResults = await Promise.all(
      accountAggregatesBefore.map(agg => resetAccountAggregate(dynamodb, dynamodbTableName, mode, agg.key))
    )

    teamAggregateUpdateResults = await Promise.all(
      teamAggregatesBefore.map(agg => resetTeamOrCompanyAggregate(dynamodb, dynamodbTableName, mode, agg.key))
    )

    companyAggregateUpdateResults = await Promise.all(
      companyAggregatesBefore.map(agg => resetTeamOrCompanyAggregate(dynamodb, dynamodbTableName, mode, agg.key))
    )
  }

  const [activitiesAfter, accountAggregatesAfter, teamAggregatesAfter, companyAggregatesAfter] = await Promise.all([
    getAllActivities(dynamodb, dynamodbTableName, mode),
    getExistingAccountAggregates(dynamodb, dynamodbTableName, mode),
    getExistingTeamAggregates(dynamodb, dynamodbTableName, mode),
    getExistingCompanyAggregates(dynamodb, dynamodbTableName, mode)
  ])

  const summary = {
    mode: mode,
    date: new Date().toISOString(),
    activity_info: {
      activities_before: activitiesBefore.length,
      activities_after: activitiesAfter.length
    },
    account_aggregate_info: {
      accountAggregates_before: accountAggregatesBefore.length,
      accountAggregates_after: accountAggregatesAfter.length,
      number_of_resets: accountAggregatesAfter.filter(
        agg =>
          agg.data.totalDistance === 0 &&
          agg.data.totalSteps === 0 &&
          agg.data.rank === 1 &&
          agg.data.previousLevel === 1 &&
          agg.data.acknowledged === false &&
          agg.sort_key_1 === 0 &&
          agg.sort_key_2 === 0 &&
          agg.sort_key_3 === 0
      ).length,
      updates: doShowDetails ? accountAggregateUpdateResults : undefined
    },
    team_aggregate_info: {
      teamAggregates_before: teamAggregatesBefore.length,
      teamAggregates_after: teamAggregatesAfter.length,
      number_of_resets: teamAggregatesAfter.filter(
        agg =>
          agg.data.totalDistance === 0 &&
          agg.data.totalSteps === 0 &&
          agg.data.rank === 1 &&
          agg.data.averageSteps === 0 &&
          agg.data.averageDistance === 0 &&
          agg.sort_key_1 === 0 &&
          agg.sort_key_2 === 0
      ).length,
      updates: doShowDetails ? teamAggregateUpdateResults : undefined
    },
    company_aggregate_info: {
      companyAggregates_before: companyAggregatesBefore.length,
      companyAggregates_after: companyAggregatesAfter.length,
      number_of_resets: companyAggregatesAfter.filter(
        agg =>
          agg.data.totalDistance === 0 &&
          agg.data.totalSteps === 0 &&
          agg.data.rank === 1 &&
          agg.data.averageSteps === 0 &&
          agg.data.averageDistance === 0 &&
          agg.sort_key_1 === 0 &&
          agg.sort_key_2 === 0
      ).length,
      updates: doShowDetails ? companyAggregateUpdateResults : undefined
    }
  }

  if (doWriteToOutput) {
    writeToFile(mode, scriptName, summary)
  } else {
    logDetails('Reset Summary', summary)
  }
}

modeScript(
  scriptName,
  execute,
  `
Run this script with argument \`<MODE>\`.
Purpose: Delete all activity data & reset aggregates.

Parameters:
    \`--details\`   show update details (optional)
    \`--output\`    write the output to a file (optional)
    \`--write\`     update the accountAggregates in the database (optional)

The script does a dry run, unless \`--write\` is given as an argument.
`
)
