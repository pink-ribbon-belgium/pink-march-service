#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const yaml = require('js-yaml')
const fsBare = require('fs')
const path = require('path')
const _ = require('lodash')
const fs = fsBare.promises
const openApi = require('../../common/openApi')

const cache = {}

async function loadYaml (relPath) {
  if (!cache[relPath]) {
    const absPath = path.format({ dir: openApi.dir, name: relPath, ext: '.yaml' })
    const contents = await fs.readFile(absPath)
    const loaded = yaml.safeLoad(contents, { filename: absPath })
    cache[relPath] = await dereferenced(loaded, absPath)
  }
  return cache[relPath]
}

const splicePattern = /^\$splice:(.*)$/
const concatPattern = /^\$concat:(.*)$/

function openApiDirRelPath (mentionedIn, reference) {
  return path.relative(openApi.dir, path.join(path.dirname(mentionedIn), reference))
}

async function dereferenced (contents, absPath) {
  if (!contents) {
    return contents
  }
  if (Array.isArray(contents)) {
    return concatenated(contents, absPath)
  }
  if (typeof contents === 'object') {
    const ext = await extended(contents, absPath)
    const keys = Object.keys(ext)
    const values = await Promise.all(keys.map(key => dereferenced(ext[key], absPath)))

    return keys.reduce((acc, key, i) => {
      acc[key] = values[i]
      return acc
    }, {})
  }
  if (typeof contents === 'string') {
    const splice = splicePattern.exec(contents)
    if (!splice) {
      return contents
    } else {
      return loadYaml(openApiDirRelPath(absPath, splice[1]))
    }
  }
  return contents
}

async function concatenated (items, absPath) {
  const concatenations = await Promise.all(
    items.map(async value => {
      const concat = concatPattern.exec(value)
      if (!concat) {
        return dereferenced(value, absPath)
      } else {
        const toConcat = await loadYaml(openApiDirRelPath(absPath, concat[1]))
        return { toConcat }
      }
    })
  )
  return concatenations.reduce((acc, v) => {
    if (v && Array.isArray(v.toConcat)) {
      return acc.concat(v.toConcat)
    }
    acc.push(v)
    return acc
  }, [])
}

/**
 * `obj` properties overwrite and appends referenced object properties if `$extend` is not the last property.
 * Otherwise, referenced object properties overwrite and append to `obj`.
 *
 * This doesn't merge arrays.
 */
async function extended (obj, absPath) {
  if (Object.prototype.hasOwnProperty.call(obj, '$extend')) {
    const base = await Promise.all(obj.$extend.reverse().map(e => loadYaml(openApiDirRelPath(absPath, e))))
    delete obj.$extend
    return _.defaultsDeep({}, obj, ...base)
  }
  return obj
}

async function consolidateOpenAPI () {
  const index = await loadYaml('index')
  const result = yaml.safeDump(index, { noRefs: true })
  await fs.writeFile(openApi.consolidated, result)
}

consolidateOpenAPI()
