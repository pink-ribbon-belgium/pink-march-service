#!/usr/bin/env node

/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const authenticatedAWS = require('../../common/authenticatedAWS')
const fs = require('fs').promises
const openApi = require('../../common/openApi')
const APIGateway = require('../../common/APIGateway')

const claudia = require('../../claudia/service')
const apigatewayData = require('../../apigateway')

const APIGatewaySchema = APIGateway.schema

const lambdaName = claudia.lambda.name
const region = claudia.lambda.region
// when used as devsecops human
const profile = 'pink-ribbon-belgium-dev'
const rolePathAndName = 'devsecops/pink-march-service-infrastructure'

const latestStage = 'api-LATEST'

async function deploy (buildNumber) {
  const apigatewayDataValidation = APIGatewaySchema.validate(apigatewayData)
  if (apigatewayDataValidation.error) {
    throw apigatewayDataValidation.error
  }
  const apiGatewayId = apigatewayDataValidation.value.id
  const reservedStages = apigatewayDataValidation.value.reservedStages.map(build => `api-${build}`).concat(latestStage)
  console.log(`API Gateway ID: ${apigatewayDataValidation.value.id}`)
  console.log('Reserved stages: ')
  reservedStages.forEach(rs => {
    console.log(`  - ${rs}`)
  })

  const { AWS, accountId } = await authenticatedAWS(region, profile, rolePathAndName)
  const apiGateway = new AWS.APIGateway({ apiVersion: '2015-07-09' })

  console.log(`Getting existing states of API Gateway ${apiGatewayId} …`)
  const stagesResponse = await apiGateway.getStages({ restApiId: apiGatewayId }).promise()
  const stages = stagesResponse.item
  const existingStages = stages.map(s => s.stageName)
  console.log('Existing stages:')
  existingStages.forEach(es => {
    console.log(`  - ${es}`)
  })

  const reservedStagesThatDoNotExist = reservedStages.filter(rs => existingStages.every(es => es !== rs))
  if (reservedStagesThatDoNotExist.length > 0) {
    console.warn('⚠️ There are reserved stages that do not exist:')
    reservedStagesThatDoNotExist.forEach(doesNotExist => {
      console.warn(`  - ${doesNotExist}`)
    })
    console.warn('⚠️ Please remove these stages from `apigateway.json`.')
  }

  const lambdaBuild = `build-${buildNumber}`

  console.log(`Loading OpenAPI spec file ${openApi.consolidated} …`)
  const spec = await fs.readFile(openApi.consolidated, 'utf8')
  console.log("OpenAPI spec file loaded. Expanding macro's …")
  const lambdaInvocationArn = `arn:aws:apigateway:${region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${region}:${accountId}:function:${lambdaName}:${lambdaBuild}/invocations`
  const expandedSpec = spec.replace(/\$\$lambdaInvocationArn\$\$/g, lambdaInvocationArn)
  console.log('OpenAPI spec file loaded. Importing OpenAPI spec …')

  // noinspection SpellCheckingInspection
  await apiGateway
    .putRestApi({
      restApiId: apiGatewayId,
      body: expandedSpec,
      mode: 'overwrite',
      failOnWarnings: true,
      parameters: {
        endpointConfigurationTypes: 'REGIONAL',
        basepath: 'prepend'
      }
    })
    .promise()
  console.log('OpenAPI spec imported.')

  /* Allow execution of Lambda by API Gateway. This must be done per stage, because we need to address the qualified
     Lambda function.

     The Lambda version or alias is mentioned in the integration, via the stage variable `lambdaBuild`. The stage
     variable `lambdaBuild is filled out in the stage definition. It is thus the stage that selects which Lambda version
     is called. That means the permission is also defined at the level of the stage.

     - See https://docs.aws.amazon.com/lambda/latest/dg/with-on-demand-https-example-configure-event-source.html, section
       3.6.
     - See http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html

     Do

         > aws lambda get-policy --profile pink-ribbon-belgium-dev --region eu-west-1 --function-name pink-march-service --qualifier ${lambdaBuild}

     to see the current permissions. */
  console.log(`Giving all stages of API Gateway ${apiGatewayId} permission to call λ alias '${lambdaBuild}' …`)
  const executionArn = `arn:aws:execute-api:${region}:${accountId}:${apiGatewayId}/*/*/*`
  const lambda = new AWS.Lambda({ apiVersion: '2015-03-31' })
  // noinspection SpellCheckingInspection
  await lambda
    .addPermission({
      Action: 'lambda:InvokeFunction',
      FunctionName: lambdaName,
      Qualifier: lambdaBuild,
      Principal: 'apigateway.amazonaws.com',
      SourceArn: executionArn,
      StatementId: `apicallslambda-${buildNumber}`
    })
    .promise()
  console.log(`All stages of API Gateway ${apiGatewayId} are now permitted to call λ alias '${lambdaBuild}'.`)

  const tags = {
    name: 'Pink March Service',
    env: 'production',
    // IDEA get git info
    repo: `pink-march-service/${__dirname}`,
    build: buildNumber,
    lambdaBuild
  }

  const description = `Continuous deployment of build ${buildNumber}`
  const stageParameters = {
    restApiId: apiGatewayId,
    cacheClusterEnabled: false,
    description,
    stageDescription: description,
    tracingEnabled: false, // TODO enable?
    variables: tags // unused, but interesting documentation, like tags
  }

  if (!existingStages.includes(latestStage)) {
    console.warn('There is no LATEST stage. It will be created.')
  }
  stageParameters.stageName = latestStage
  console.log(`Deploying to stage ${latestStage} …`)
  const latestResponse = await apiGateway.createDeployment(stageParameters).promise()
  // IDEA add tags
  const deploymentId = latestResponse.id
  console.log(`Deployed to ${latestStage}. Deployment id: ${deploymentId}`)
  if (!existingStages.includes(latestStage)) {
    existingStages.push(latestStage)
  }
  console.log(`URI to call the API Gateway: ${APIGateway.uri(region, apiGatewayId, latestStage)}`)

  console.log(`There are ${existingStages.length} existing stages. The maximum number is 10.`)
  if (existingStages.length >= 10) {
    console.warn('To be able to deploy this build, the oldest non-reserved stage will be deleted.')
    const candidatesForDeletion = existingStages.filter(es => !reservedStages.includes(es)).sort()
    console.warn('Candidates for deletion:')
    candidatesForDeletion.forEach(cfd => {
      console.warn(`  - ${cfd}`)
    })
    const selectedForDeletion = candidatesForDeletion[0]
    console.warn(`Deleting stage ${selectedForDeletion} …`)
    await apiGateway
      .deleteStage({
        restApiId: apiGatewayId,
        stageName: selectedForDeletion
      })
      .promise()
    console.warn(`Stage ${selectedForDeletion} deleted.`)
  }

  const stageName = `api-${buildNumber}`
  stageParameters.deploymentId = deploymentId
  stageParameters.stageName = stageName
  stageParameters.tags = tags
  // IDEA stageParameters.documentationVersion = `0.0.0-build.${buildNumber}`
  delete stageParameters.stageDescription
  console.log(`Deploying to stage ${stageName} …`)
  await apiGateway.createStage(stageParameters).promise()
  console.log(`Deployed to ${stageName}. Deployment id: ${deploymentId}`)
  console.log(`URI to call the API Gateway: ${APIGateway.uri(region, apiGatewayId, stageName)}`)
}

/* `process.argv` is an array containing the command line arguments. The first element will be 'node', the second
    element will be the name of the JavaScript file. The next elements will be any additional command line arguments. */
const buildNumber = process.argv[2]
if (!(buildNumber && /^\d{5}$/.test(buildNumber))) {
  console.error("Requires the build number, padded to 5 digits, as argument (e.g., '00123')")
  process.exit(-1)
}
console.log(`Deploying API ${buildNumber}`)

deploy(buildNumber).catch(err => {
  console.error('deploy failed:')
  console.error(err)
  process.exit(-1)
})
