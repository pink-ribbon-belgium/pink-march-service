/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const AWS = require('../lib/server/aws/configuredAWS')
const { v4: uuidv4 } = require('uuid')

let authenticatedInstance

async function getAccountId (AWS) {
  const iam = new AWS.IAM()
  const userData = await iam.getUser({}).promise()
  const accountId = userData.User.Arn.split(':')[4]
  console.info('  AWS user data retrieved') // do not log accountId
  return accountId
}

async function authenticatedAWS (region, profile, rolePathAndName) {
  /* Either branch cannot be covered by local tests or CI. This is not production code, but code used in tests and
     devsecops scripts. */
  /* istanbul ignore next */
  if (!authenticatedInstance) {
    AWS.config.update({ region })
    if (!process.env.CI) {
      console.info('Not working on CI. Using AWS credentials from `~/.aws/credentials`')
      // get credentials from ~/.aws/credentials for `profile`, get role `rolePathAndName` with those
      // credentials, and continue with the role credentials
      AWS.config.credentials = new AWS.SharedIniFileCredentials({ profile })
      const accountId = await getAccountId(AWS)
      const sts = new AWS.STS()
      const stsId = uuidv4()
      const roleSessionName = `pink-march-service-${stsId}`
      const stsData = await sts
        .assumeRole({
          RoleArn: `arn:aws:iam::${accountId}:role/${rolePathAndName}`,
          RoleSessionName: roleSessionName
        })
        .promise()
      console.info(
        `  AWS credentials for role ${rolePathAndName} received (session name: ${roleSessionName}). Assuming.`
      )
      AWS.config.update({
        accessKeyId: stsData.Credentials.AccessKeyId,
        secretAccessKey: stsData.Credentials.SecretAccessKey,
        sessionToken: stsData.Credentials.SessionToken,
        region
      })
      authenticatedInstance = { AWS, accountId }
    } else {
      // else, working on a CI system: get the credentials from env variables, and continue with those
      console.info('Working on CI. Using AWS credentials from env $AWS_ACCESS_KEY_ID and $AWS_SECRET_ACCESS_KEY')
      const accountId = await getAccountId(AWS)
      authenticatedInstance = { AWS, accountId }
    }
  }
  return authenticatedInstance
}

module.exports = authenticatedAWS
