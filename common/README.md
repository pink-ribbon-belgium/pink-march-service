# Common

This directory contains supporting code that is shared between devsecops [`scripts`](./scripts) and [`test`](./test).

This is not prodution code. It is only tested when it seems necessary.
