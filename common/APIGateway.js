/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../lib/_util/validateSchema')

const example = {
  id: 'dj7jd39jd0',
  reservedStages: ['04050', '00234', '00000']
}

const Region = Joi.string().required()

const GatewayId = Joi.string()
  .alphanum()
  .pattern(/^[a-z\d]{10}$/)
  .length(10)
  .required()

const BuildNumber = Joi.string()
  .pattern(/^\d{5}$/)
  .required()

const StageName = Joi.string()
  .pattern(/^api-\d{5}$/)
  .allow('api-LATEST')
  .required()

const APIGateway = Joi.object()
  .keys({
    id: GatewayId,
    reservedStages: Joi.array()
      .items(BuildNumber)
      .unique()
      .sort({ order: 'descending' })
      .max(8)
      .required()
  })
  .example(example)

const uri = new Contract({
  pre: [
    region => validateSchema(Region, region),
    (region, apiGatewayId) => validateSchema(GatewayId, apiGatewayId),
    (region, apiGatewayId, stageName) => validateSchema(StageName, stageName)
  ],
  post: [
    (region, apiGatewayId, stageName, result) =>
      result === `https://${apiGatewayId}.execute-api.${region}.amazonaws.com/${stageName}`
  ]
}).implementation(function uri (region, apiGatewayId, stageName) {
  return `https://${apiGatewayId}.execute-api.${region}.amazonaws.com/${stageName}`
})

module.exports = { example, schema: APIGateway, uri }
