# Deploy the Lambda function

[node-lambda] is a way to test λs locally.

Looking for a way to have an 'ignore' in packaging the λ, [Package Lambda Functions The Easy Way With NPM] shows a very
interesting option.

[claudia] packages and uploads λs. It does much more, we do not use.

[Installing and Configuring Claudia.js]:

> - `AWSLambdaFullAccess` is required for all Claudia deployments
> - `IAMFullAccess` is required if you want Claudia to automatically create execution roles for your Lambda function
>   (recommended for beginners). If you cannot use this role due to corporate policies, or do not want to grant Claudia
>   access to IAM, then create a Lambda execution role manually, and pass the name using --role <ROLE NAME> when
>   executing claudia create.
> - `AmazonAPIGatewayAdministrator` is required if you want to use Claudia API Builder, Claudia Bot Builder or deploy
>   API Gateway Proxy APIs.

Since we will not let Claudia create the execution role, we only need `AWSLambdaFullAccess`

Note:

> Since Claudia version 1.7.0, you can also use the `--profile <PROFILE_NAME>` command line argument

[Including and Excluding Files from Deployment]:

> Claudia uses the standard Node.js packaging model (by executing npm pack), so if you’re used to NPM, things will just
> work out of the box:
>
> - all the standard patterns ignored by NPM are ignored by Claudia as well (including `.git`, `.hg` and so on)
> - if `.gitignore` or `.npmignore` exist, patterns listed in those files will be excluded (check out the NPM packaging
>   documentation for details on how to keep files out of your package).
> - if the `package.json` file includes the `files` property, only those patterns will be included. See the files
>   section of the NPM configuration guide for more information.
> - all NPM workflow steps (such as `post-install`) will be executed as during a normal installation, so you can script
>   additional tasks such as transpilation easily.

Also:

> The Node.js Lambda runtime already includes some standard libraries, such as `aws-sdk` […]. You can reduce the size of
> your deployment package slightly by excluding those libraries, even if your code uses them. To do that, install them
> as **optional dependencies** (`-O`, `--save-optional`), and then provide the `--no-optional-dependencies` flag when
> running `claudia create` or `claudia update`.

[claudia]: https://www.npmjs.com/package/claudia
[installing and configuring claudia.js]: https://claudiajs.com/tutorials/installing.html
[including and excluding files from deployment]: https://claudiajs.com/tutorials/packaging.html
[node-lambda]: https://github.com/motdotla/node-lambda
[package lambda functions the easy way with npm]:
  https://hackernoon.com/package-lambda-functions-the-easy-way-with-npm-e38fc14613ba
