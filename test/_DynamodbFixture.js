/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const dynamodb = require('../lib/server/aws/dynamodb/dynamodb')
const util = require('util')
const dynamodbTableName = require('../lib/server/aws/dynamodb/dynamodbTableName')
const Item = require('../lib/server/aws/dynamodb/Item').schema
const Joi = require('@hapi/joi')
const assert = require('assert')

const ItemWrite = Item.tailor('write').label('item')
const requiredItemWrite = ItemWrite.required().label('item')
const requiredItemRead = Item.tailor('read')
  .required()
  .label('item')

class DynamodbFixture {
  constructor () {
    this.keyCouples = []
    this.dynamodbInstance = undefined // cannot get it async in constructor
    this.dynamodbPromise = undefined
  }

  async ensureDynamodbInstance () {
    if (!this.dynamodbPromise) {
      this.dynamodbPromise = dynamodb().then(dynamodbInstance => {
        this.dynamodbInstance = dynamodbInstance
      })
    }
    return this.dynamodbPromise
  }

  remember (key, submitted, tableName, intentionalInvalidItem) {
    this.keyCouples.push({ key, submitted, tableName, intentionalInvalidItem })
  }

  async hasArrivedSingle (key, submitted, tableName, intentionalInvalidItem) {
    await this.ensureDynamodbInstance()
    const params = {
      TableName: tableName,
      ExpressionAttributeNames: {
        '#key': 'key',
        '#submitted': 'submitted'
      },
      ExpressionAttributeValues: {
        ':key': key,
        ':submitted': submitted
      },
      KeyConditionExpression: '#key = :key and #submitted = :submitted',
      Limit: 1,
      ConsistentRead: true
    }

    const result = await this.dynamodbInstance.query(params).promise()

    const item = result.Items[0]
    if (!intentionalInvalidItem) {
      Joi.attempt(item, requiredItemRead)
    } else {
      assert.ok(item)
      const validation = ItemWrite.validate(item)
      assert.ok(validation.error)
    }

    console.info(`item ${util.inspect(result)} has arrived`)
    return item
  }

  async hasArrived () {
    await this.ensureDynamodbInstance()
    const results = await Promise.all(
      this.keyCouples.map(async keyCouple =>
        this.hasArrivedSingle(keyCouple.key, keyCouple.submitted, keyCouple.tableName, keyCouple.intentionalInvalidItem)
      )
    )
    console.info('all items have arrived')
    return results
  }

  /**
   * @param {Item} item - the item to store
   * @param {boolean} [allowInvalidItem] - Makes it possible to store invalid data, which is necessary for some tests.
   *                                       In this case, `item` MUST be invalid.
   */
  async putItem (item, allowInvalidItem) {
    if (!allowInvalidItem) {
      Joi.attempt(item, requiredItemWrite)
    } else {
      assert.ok(item)
      const validation = ItemWrite.validate(item)
      assert.ok(validation.error)
    }
    await this.ensureDynamodbInstance()
    const tableName = dynamodbTableName(item.mode)
    const params = {
      TableName: tableName,
      Item: item
    }
    await this.dynamodbInstance.put(params).promise()
    console.log(`put item ${util.inspect(item)}, waiting for arrival …`)
    this.remember(item.key, item.submitted, tableName, allowInvalidItem)
    await this.hasArrivedSingle(item.key, item.submitted, tableName, allowInvalidItem)
  }

  async clean () {
    await this.ensureDynamodbInstance()
    const toDelete = this.keyCouples.slice()
    await Promise.all(
      toDelete.map(async keyCouple => {
        await Promise.all([
          this.dynamodbInstance
            .delete({
              TableName: keyCouple.tableName,
              Key: { key: keyCouple.key, submitted: keyCouple.submitted }
            })
            .promise(),
          this.dynamodbInstance
            .delete({
              TableName: keyCouple.tableName,
              Key: { key: keyCouple.key, submitted: 'actual' } // if it exists
            })
            .promise()
        ])
        this.keyCouples = this.keyCouples.filter(kc => kc !== keyCouple)
        console.info(`deleted item ${util.inspect(keyCouple)}`)
      })
    )
    assert(this.keyCouples.length <= 0)
    console.info('deleted all items')
  }
}

module.exports = DynamodbFixture
