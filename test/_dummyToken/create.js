/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const JWT = require('jsonwebtoken')
const config = require('config')
const getPrivateKey = require('./_getPrivateKey')
const customClaims = require('../../lib/server/authorization/customClaims')

const defaultsPayload = {
  iss: config.sts.test.issuer,
  aud: config.authorization.audience,
  sub: 'aDummySub',
  nbf: new Date(2020, 1, 26, 11, 30, 19, 345).getTime() / 1000,
  exp: new Date(2120, 1, 26, 11, 30, 19, 345).getTime() / 1000,
  [customClaims.raasClaim]: []
}

async function createDummyToken (overwrites, propertiesToDelete) {
  const payload = {
    ...defaultsPayload,
    ...overwrites
  }
  if (propertiesToDelete) {
    propertiesToDelete.forEach(ptd => {
      delete payload[ptd]
    })
  }
  return JWT.sign(payload, await getPrivateKey(), {
    algorithm: (overwrites && overwrites.algorithm) || 'RS256'
  })
}

module.exports = createDummyToken
