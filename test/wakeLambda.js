/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const createLambdaCall = require('./integration/_createLambdaInvokeCall')
const { v4: uuidv4 } = require('uuid')

const id = uuidv4()
const event = {
  httpMethod: 'OPTIONS',
  path: '/wake',
  resource: '/wake',
  headers: {
    'x-mode': `automated-test-${id}`,
    'x-flow-id': id
  },
  pathParameters: {},
  body: null
}

describe('λ', function () {
  it('can be woken up if needed in tests', async function () {
    // ensure the λ to be a awake before tests really start
    // this test has a long time-out for that
    // we only try once
    // IDEA: if this is not good enough (but it should be), we can introduce retries, and a shorter timeout for one try
    this.timeout(20000)

    const lambdaBuild = process.env.LAMBDA_BUILD || process.env.API_GATEWAY || process.env.CLOUDFRONT
    if (!lambdaBuild) {
      console.log('Running integration tests locally via `service#handler`. No need to wake a λ build.')
      return
    }
    console.log(`waking λ build ${lambdaBuild} …`)
    const actualCall = await createLambdaCall(lambdaBuild)
    const response = await actualCall(event)
    console.log(response)
    console.log(`λ build ${lambdaBuild} is awake`)
  })
})
