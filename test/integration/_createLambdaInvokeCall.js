/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const authenticatedTestAWS = require('../_authenticatedTestAWS')
const util = require('util')
const { name } = require('../../claudia/service').lambda

const lambdaInvocationParamsBase = {
  FunctionName: name,
  InvocationType: 'RequestResponse',
  LogType: 'None'
}

async function createLambdaInvokeCall (buildNumber) {
  const AWS = await authenticatedTestAWS()
  const lambda = new AWS.Lambda({ apiVersion: '2015-03-31' })
  return async function (event) {
    const params = {
      ...lambdaInvocationParamsBase,
      Qualifier: `build-${buildNumber}`,
      Payload: JSON.stringify(event)
    }
    const responsePromise = lambda.invoke(params).promise()
    console.log('lambda called. awaiting response …')
    const response = await responsePromise
    // eslint-disable-next-line no-undef
    console.log('lambda response', util.inspect(response))
    return JSON.parse(response.Payload)
  }
}

module.exports = createLambdaInvokeCall
