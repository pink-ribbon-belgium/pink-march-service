/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const callLambda = require('../_callLambda')
const generateBaseEvent = require('../_generateBaseEvent')
const getIntegrationTestRunMode = require('../_getIntegrationTestRunMode')
const getToken = require('../../_getTrueToken')
const DynamodbFixture = require('../../_DynamodbFixture')
const getVersionAtOrBefore = require('../../../lib/server/aws/dynamodb/getVersionAtOrBefore')
const dynamodbTableName = require('../../../lib/server/aws/dynamodb/dynamodbTableName')
const TrackerConnection = require('../../../lib/server/business/account/trackerConnection/TrackerConnection')

const mode = getIntegrationTestRunMode()

const itemFlowId = '98521c71-9a25-40da-bd2c-9e26e1c8111e'
const deleteFlowId = 'b90448c8-871b-48a8-9032-ebae8bf463c5'

describe(testName, () => {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.auth = await getToken(mode, this.dynamodbFixture)
    this.accountId = this.auth.accountId
    const baseEvent = generateBaseEvent(deleteFlowId, mode, this.auth.token)
    this.event = {
      httpMethod: 'DELETE',
      path: `/I/account/${this.accountId}/trackerConnection`,
      resource: '/I/account/{accountId}/trackerConnection',
      ...baseEvent
    }
    this.callLambda = async function (pathParameters) {
      this.event.pathParameters = pathParameters
      return callLambda(this.event)
    }
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    const connection = new TrackerConnection({
      mode,
      dto: {
        accountId: this.accountId,
        trackerType: 'fitbit',
        tokenType: 'refresh',
        encryptedToken: 'WA==',
        exp: '2020-03-31T08:30:25.125Z',
        hasError: false,
        createdAt: '2020-03-31T08:30:25.125Z',
        createdBy: this.accountId,
        structureVersion: 1
      }
    })
    await this.dynamodbFixture.putItem({ ...connection.toItem(), submitted: 'actual', flowId: itemFlowId })
    const res = await this.callLambda({ accountId: this.accountId })
    console.log(res)
    res.statusCode.should.equal(204)
    const key = `/${mode}/account/${this.accountId}/trackerConnection`
    const written = await getVersionAtOrBefore(mode, key, new Date().toISOString(), true)
    this.dynamodbFixture.remember(key, 'actual', dynamodbTableName(mode))
    this.dynamodbFixture.remember(key, written.submitted, dynamodbTableName(mode))
    written.should.be.ok()
  })

  it('should throw a 404', async function () {
    const res = await this.callLambda({ accountId: this.accountId })
    res.statusCode.should.equal(404)
  })

  it('should throw a 400 for pathParameters', async function () {
    const res = await this.callLambda({})
    res.statusCode.should.equal(400)
  })
})
