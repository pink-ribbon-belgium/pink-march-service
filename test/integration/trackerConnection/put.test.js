/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const callLambda = require('../_callLambda')
const generateBaseEvent = require('../_generateBaseEvent')
const getIntegrationTestRunMode = require('../_getIntegrationTestRunMode')
const getToken = require('../../_getTrueToken')
const DynamodbFixture = require('../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const getVersionAtOrBefore = require('../../../lib/server/aws/dynamodb/getVersionAtOrBefore')
const TrackerType = require('../../../lib/api/account/trackerConnection/TrackerType')
const dynamodbTableName = require('../../../lib/server/aws/dynamodb/dynamodbTableName')

const mode = getIntegrationTestRunMode()

describe(testName, () => {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.flowId = uuidv4()
    this.auth = await getToken(mode, this.dynamodbFixture)
    this.accountId = this.auth.accountId
    this.payload = {
      trackerType: TrackerType.automatedTestType,
      code: 'this-is-a-code',
      redirectUrl: 'https://domain.net/redirect',
      hasError: false
    }

    const baseEvent = generateBaseEvent(this.flowId, mode, this.auth.token)
    this.event = {
      httpMethod: 'PUT',
      path: `/I/account/${this.accountId}/trackerConnection`,
      resource: '/I/account/{accountId}/trackerConnection',
      ...baseEvent
    }
    this.callLambda = async function (pathParameters, body) {
      this.event.pathParameters = pathParameters
      this.event.body = body ? JSON.stringify(body) : null
      return callLambda(this.event)
    }
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    const res = await this.callLambda({ accountId: this.accountId }, this.payload)
    console.log(res)
    res.statusCode.should.equal(204)
    const key = `/${mode}/account/${this.accountId}/trackerConnection`
    const written = await getVersionAtOrBefore(mode, key, new Date().toISOString(), true)
    this.dynamodbFixture.remember(key, 'actual', dynamodbTableName(mode))
    this.dynamodbFixture.remember(key, written.submitted, dynamodbTableName(mode))
    written.should.be.ok()
  })

  it('should throw a 400 for pathParameters', async function () {
    const res = await this.callLambda({}, this.payload)
    res.statusCode.should.equal(400)
  })

  it('should throw a 400 for body', async function () {
    const res = await this.callLambda({ accountId: this.accountId })
    res.statusCode.should.equal(400)
  })
})
