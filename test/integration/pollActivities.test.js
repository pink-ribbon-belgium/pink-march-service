/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const authenticatedTestAWS = require('../_authenticatedTestAWS')
const util = require('util')
const { v4: uuidv4 } = require('uuid')
const { name } = require('../../claudia/pollActivities').lambda
const should = require('should')

const lambdaInvocationParamsBase = {
  FunctionName: name,
  InvocationType: 'RequestResponse',
  LogType: 'None'
}

const nrOfTrackerConnectionsToPoll = 3

if (process.env.LAMBDA_BUILD) {
  /* It makes no sense to have a local integration test for pollActivities: there is no integration.
     The unit test tests all there is to test.
     This test only runs for lambda. */
  describe(testName, function () {
    this.timeout(5000) // wait for wake

    beforeEach(function () {
      this.mode = `automated-test-${uuidv4()}`

      this.event = { mode: this.mode, nrOfTrackerConnectionsToPoll }
    })

    it('works', async function () {
      const AWS = await authenticatedTestAWS()
      const lambda = new AWS.Lambda({ apiVersion: '2015-03-31' })
      // MUDO test skeleton for now: setup some things to poll and to update
      const params = {
        ...lambdaInvocationParamsBase,
        Qualifier: `build-${process.env.LAMBDA_BUILD}`,
        Payload: JSON.stringify(this.event)
      }
      const responsePromise = lambda.invoke(params).promise()
      console.log('lambda called. awaiting response …')
      const response = await responsePromise
      // eslint-disable-next-line no-undef
      console.log('lambda response', util.inspect(response))
      response.StatusCode.should.equal(200)
      const payload = JSON.parse(response.Payload)
      should(payload).equal(null)
      // MUDO test skeleton for now: check some things to polled and updated
    })
  })
} else {
  console.log('pollActivities integration test only runs against lambda (set `process.env.LAMBDA_BUILD`)')
}
