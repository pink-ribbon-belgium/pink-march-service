/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const DynamodbFixture = require('../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const getIntegrationTestRunMode = require('./_getIntegrationTestRunMode')
const callLambda = require('./_callLambda')
const util = require('util')
const generateBaseEvent = require('./_generateBaseEvent')
const getToken = require('../_getTrueToken')
const config = require('config')
const getVersionAtOrBefore = require('../../lib/server/aws/dynamodb/getVersionAtOrBefore')
const dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')

const mode = getIntegrationTestRunMode()

const itemFlowId = 'f8277c1e-b36a-4003-bdea-0e65ef1afa3b'

const unautorizedResponseBody = {
  statusCode: 401,
  error: 'Unauthorized',
  message: 'invalid_token',
  attributes: {
    realm: config.authorization.audience,
    charset: 'utf-8',
    error_description: 'invalid signature',
    verifyFailed: true,
    error: 'invalid_token'
  }
}

function generateGetTest (
  resource,
  createPathParams,
  createUnauthorizedPathParams,
  createDynamodbItemData,
  Constructor,
  bodyChecks200,
  tweakDynamodbItem,
  isPublic,
  noPut,
  joinLink
) {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.auth = await getToken(mode, this.dynamodbFixture)
    this.flowId = uuidv4()

    this.setup = function (cpp, killTheSignature) {
      const baseEvent = generateBaseEvent(
        this.flowId,
        mode,
        killTheSignature ? this.auth.token + 'killTheSignature' : this.auth.token
      )
      const pathParameters = cpp ? cpp(this.auth) : {}
      /* prettier-ignore */
      this.path = pathParameters
        ? Object.keys(pathParameters).reduce(
          (acc, param) => acc.replace(`{${param}}`, pathParameters[param]),
          resource
        )
        : resource
      this.event = {
        path: this.path,
        resource,
        ...baseEvent,
        pathParameters
      }
      this.completeEvent()
    }

    this.shouldBeGoodResponse = function (response, statusCode) {
      console.log('response:', util.inspect(response))
      response.should.be.an.Object()
      response.statusCode.should.equal(statusCode)
      response.headers.should.be.an.Object()
      response.headers['x-flow-id'].should.equal(this.flowId)
    }

    this.forbiddenResponseBody = function () {
      return {
        statusCode: 403,
        error: 'Forbidden',
        message: 'insufficient_scope',
        attributes: {
          realm: config.authorization.audience,
          charset: 'utf-8',
          error: 'insufficient_scope',
          error_description:
            "no matching Resource Action Authorization in 'https://pink-ribbon-belgium.org/raas' for requested Resource Action",
          ra: `${this.event.httpMethod}:${this.path}`
        }
      }
    }
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
  })

  describe('get', function () {
    beforeEach(async function () {
      this.completeEvent = function () {
        this.event.httpMethod = 'GET'
      }
    })

    it('should work', async function () {
      let expectedResponseBody
      if (createDynamodbItemData && Constructor) {
        const itemDataToGet = createDynamodbItemData(this.auth)
        const model = new Constructor({
          sot: itemDataToGet.createdAt,
          sub: itemDataToGet.createdBy,
          mode,
          dto: itemDataToGet
        })
        const item = {
          mode,
          key: model.key,
          submitted: itemDataToGet.createdAt,
          submittedBy: itemDataToGet.createdBy,
          flowId: itemFlowId,
          data: model.toJSON()
        }
        if (tweakDynamodbItem) {
          tweakDynamodbItem(item)
        }
        await this.dynamodbFixture.putItem(item)
        if (joinLink) {
          expectedResponseBody = {
            ...model.toJSON(),
            links: model.getLinks(),
            joinLink: model.getJoinLink()
          }
        } else {
          expectedResponseBody = {
            ...model.toJSON(),
            links: model.getLinks()
          }
        }
      }

      this.setup(createPathParams)

      const response = await callLambda(this.event)

      this.shouldBeGoodResponse(response, 200)
      if (bodyChecks200) {
        bodyChecks200(response.body, this.auth)
      } else {
        response.body.should.deepEqual(expectedResponseBody)
      }
    })
    it('should be unauthorized', async function () {
      this.setup(createPathParams, true)
      const response = await callLambda(this.event)

      this.shouldBeGoodResponse(response, 401)
      response.headers.should.have.ownProperty('x-www-authenticate')
      response.body.should.deepEqual(unautorizedResponseBody)
    })
    if (
      /{.*}/.test(resource) &&
      createDynamodbItemData /* otherwise, we cannot make a difference between existing and not existing */
    ) {
      // otherwise, not found or forbidden is not possible
      it('should be not found', async function () {
        this.setup(createPathParams)
        const response = await callLambda(this.event)

        this.shouldBeGoodResponse(response, 404)
        response.body.should.deepEqual({ statusCode: 404, error: 'Not Found', message: 'Not Found' })
      })
      if (createUnauthorizedPathParams && !isPublic) {
        it('should be forbidden', async function () {
          this.setup(createUnauthorizedPathParams)
          const response = await callLambda(this.event)

          this.shouldBeGoodResponse(response, 403)
          response.headers.should.have.ownProperty('x-www-authenticate')
          response.body.should.deepEqual(this.forbiddenResponseBody())
        })
      }
    }
    // IDEA can we test a get that returns a 400 (Bad path params)?
  })
  if (!noPut) {
    describe('put', function () {
      beforeEach(async function () {
        this.completeEvent = function () {
          this.event.httpMethod = 'PUT'
          this.itemData = createDynamodbItemData(this.auth)
          /* itemData contains too much information, but that is allowed */
          this.event.body = JSON.stringify(this.itemData)
        }
      })

      if (createDynamodbItemData && Constructor) {
        it('should work', async function () {
          this.setup(createPathParams)
          const model = new Constructor({
            sot: this.itemData.createdAt,
            sub: this.itemData.createdBy,
            mode,
            dto: this.itemData
          })

          const response = await callLambda(this.event)

          this.shouldBeGoodResponse(response, 204)
          // cannot use dynamodbFixture.hasArrived, because we do not know submitted
          const item = await getVersionAtOrBefore(mode, model.key, new Date().toISOString(), true)
          this.dynamodbFixture.remember(item.key, item.submitted, dynamodbTableName(mode))
        })
      }
      it('should be unauthorized', async function () {
        this.setup(createPathParams, true)
        const response = await callLambda(this.event)

        this.shouldBeGoodResponse(response, 401)
        response.headers.should.have.ownProperty('x-www-authenticate')
        response.body.should.deepEqual(unautorizedResponseBody)
      })
      if (/{.*}/.test(resource)) {
        // otherwise, forbidden is not possible
        if (createUnauthorizedPathParams) {
          it('should be forbidden', async function () {
            this.setup(createUnauthorizedPathParams)
            const response = await callLambda(this.event)

            this.shouldBeGoodResponse(response, 403)
            response.headers.should.have.ownProperty('x-www-authenticate')
            response.body.should.deepEqual(this.forbiddenResponseBody())
          })
        }
      }
      // IDEA can we do a put test that returns a 400 because of bad pathParams?
      it('should be a bad request with a bad body', async function () {
        this.setup(createPathParams)
        delete this.itemData.structureVersion
        /* itemData contains too much information, but that is allowed */
        this.event.body = JSON.stringify(this.itemData)
        const response = await callLambda(this.event)

        this.shouldBeGoodResponse(response, 400)
      })
    })
  }
}

module.exports = generateGetTest
