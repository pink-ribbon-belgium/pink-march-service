/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const ServiceHealthStatus = require('../../lib/api/health/ServiceHealthStatus')
const generateGetPutTest = require('./_generateGetPutTest')

describe(testName, function () {
  this.timeout(5000)

  generateGetPutTest(
    '/I/health',
    undefined,
    undefined,
    undefined,
    undefined,
    body => {
      body.should.containDeep({
        structureVersion: 1,
        dynamodb: ServiceHealthStatus.OK,
        status: ServiceHealthStatus.OK
      })
    },
    undefined,
    false,
    true
  )
  // cannot test unhealthy state
})
