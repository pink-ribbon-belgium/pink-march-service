/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const generateGetPutTest = require('./_generateGetPutTest')
const Account = require('../../lib/server/business/account/Account')

describe(testName, function () {
  this.timeout(5000)

  generateGetPutTest(
    '/I/account/{accountId}',
    auth => ({
      accountId: auth.accountId
    }),
    auth => ({
      accountId: 'notMyAccountId'
    }),
    undefined,
    Account,
    (body, auth) => {
      body.should.be.an.Object()
      body.createdBy.should.equal('pink-march-auth0-ensureAccount')
      body.structureVersion.should.equal(1)
      body.id.should.equal(auth.accountId)
      const expectedLinks = {
        preferences: `/I/account/${auth.accountId}/preferences`,
        publicProfile: `/I/account/${auth.accountId}/publicProfile`,
        slot: `/I/account/${auth.accountId}/slot`,
        administratorOf: `/I/account/${auth.accountId}/administratorOf`
      }
      body.links.should.deepEqual(expectedLinks)
    },
    undefined,
    false,
    true
  )
  // cannot test unhealthy state
})
