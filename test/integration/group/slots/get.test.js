/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const DynamodbFixture = require('../../../_DynamodbFixture')
const getToken = require('../../../_getTrueToken')
const testName = require('../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const getIntegrationTestRunMode = require('../../_getIntegrationTestRunMode')
const callLambda = require('../../_callLambda')
const util = require('util')
const generateBaseEvent = require('../../_generateBaseEvent')
const Slot = require('../../../../lib/server/business/slot/Slot')
const { startCase } = require('lodash/string')
const groupTypes = require('../../../../lib/api/group/GroupTypePathParameter').values

const mode = getIntegrationTestRunMode()

const itemFlowId = '1c396a09-ee99-42a3-873a-ea6080778329'
const paymentIds = ['9e3c87c1-3529-49b3-a6f3-591beb6f7219', 'e6014e41-a968-41d1-8fdc-5e3922734b4a']
const slotIds = [
  '2a1c641f-8207-4958-b33d-c19a79442972',
  '4f233c0a-e3bc-4aff-abc6-a353a96c78f2',
  '2a63a14c-aad0-4739-bfcd-9c315a3fdcc3',
  '180b0f0b-1983-44c2-8f6f-9b8439045184',
  '9766dfaa-a3f9-4c63-bb38-374a5e540663'
]
const nrOfSlots = slotIds.length
const accountIds = [
  'fd4ca430-08a4-4f84-add3-1eb572723b99',
  '94e395bc-e73a-4af3-b569-fb2930342ae4',
  '95bc0d73-249c-41b5-a22c-2389c4c0381c',
  '8d254078-aa7a-49bd-b9c5-4ce091c13bea',
  '037c0023-e222-45ec-884c-d8689751b58b'
]
const nrOfTakenSlots = [0, 3, slotIds.length]

describe(testName, function () {
  this.timeout(5000)

  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.auth = await getToken(mode, this.dynamodbFixture)
    this.flowId = uuidv4()

    this.callLambda = async function () {
      return callLambda(this.event)
    }
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
  })

  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      beforeEach(async function () {
        this.groupId = this.auth[`${groupType}Id`]

        const baseEvent = generateBaseEvent(this.flowId, mode, this.auth.token)
        this.event = {
          path: `/I/${groupType}/${this.groupId}/slots`,
          resource: '/I/{groupType}/{groupId}/slots',
          ...baseEvent,
          httpMethod: 'GET',
          pathParameters: {
            groupType,
            groupId: this.groupId
          }
        }
      })

      nrOfTakenSlots.forEach(nr => {
        it(`works with ${nr}/${slotIds.length} taken`, async function () {
          await Promise.all(
            new Array(nrOfSlots).fill(undefined).map((_ignore, index) => {
              const slot = new Slot({
                mode: mode,
                dto: {
                  id: slotIds[index],
                  groupType: startCase(groupType),
                  groupId: this.groupId,
                  accountId: index < nr ? accountIds[index] : undefined,
                  paymentId: paymentIds[index % paymentIds.length],
                  createdAt: '2020-03-25T15:04:14.491Z',
                  createdBy: `sub-group-slots-integration-test-${index}`,
                  structureVersion: 2
                }
              })
              return this.dynamodbFixture.putItem({
                flowId: itemFlowId,
                submitted: 'actual',
                ...slot.toItem(),
                sort_key_A: 'actual',
                sort_key_B: 'actual',
                sort_key_C: 'actual'
              })
            })
          )

          const response = await this.callLambda()

          console.log('response:', util.inspect(response))
          response.should.be.an.Object()
          response.statusCode.should.equal(200)
          response.headers.should.be.an.Object()
          response.headers['x-flow-id'].should.equal(this.flowId)
          response.body.should.be.an.Object()
          const expectedResult = {
            totalSlots: slotIds.length,
            takenSlots: nr,
            availableSlots: slotIds.length - nr
          }
          response.body.should.deepEqual(expectedResult)
        })
      })
      it('it should throw a 404 when there are no slots', async function () {
        const response = await this.callLambda()

        console.log('response:', util.inspect(response))
        response.should.be.an.Object()
        response.statusCode.should.equal(404)
      })
    })
  })
})
