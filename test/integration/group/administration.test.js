/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const generateGetPutTest = require('../_generateGetPutTest')
const dynamodbExample = require('../../../lib/api/group/GroupAdministrationDTO').exampleDynamodb
const GroupAdministration = require('../../../lib/server/business/group/administration/GroupAdministration')
const { startCase } = require('lodash/string')
const { values: groupTypes } = require('../../../lib/api/group/GroupTypePathParameter')

describe(testName, function () {
  this.timeout(5000)

  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      const groupIdPropertyName = `${groupType}Id`
      generateGetPutTest(
        '/I/{groupType}/{groupId}/administrators/{accountId}',
        auth => ({
          groupType,
          groupId: auth[groupIdPropertyName],
          accountId: auth.accountId
        }),
        auth => ({
          groupType,
          groupId: '47120006-c476-4dd1-9dfe-e7ca872c2e09s', // will not be authorized
          accountId: auth.accountId
        }),
        auth => ({
          ...dynamodbExample,
          accountId: auth.accountId,
          groupType: startCase(groupType),
          groupId: auth[groupIdPropertyName],
          deleted: false
        }),
        GroupAdministration,
        undefined,
        item => {
          item.submitted = 'actual'
          item.partition_key_A = `/${item.mode}/account/${item.data.accountId}/administratorOf`
          item.sort_key_A = 'actual'
          item.partition_key_B = `/${item.mode}/group/${item.data[groupIdPropertyName]}/administrators`
          item.sort_key_B = 'actual'
        }
      )
    })
  })
})
