/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const getAuthorizedSub = require('../../lib/server/authorization/getAuthorizedSub')
const getTrueToken = require('../_getTrueToken')
const getIntegrationTestRunMode = require('./_getIntegrationTestRunMode')
const eventExample = require('../../lib/ppwcode/lambda/apigateway/Event').example
const Hoek = require('@hapi/hoek')

const mode = getIntegrationTestRunMode()

describe(testName, function () {
  beforeEach(function () {
    getAuthorizedSub.contract.verifyPostconditions = true
  })

  afterEach(function () {
    getAuthorizedSub.contract.verifyPostconditions = false
  })

  it('works with a token from the true STS', async function () {
    const { token } = await getTrueToken(mode)
    const event = Hoek.clone(eventExample)
    event.headers['x-mode'] = mode
    event.headers.authorization = `Bearer ${token}`
    event.resource = '/I/health'
    event.path = '/I/health'

    const sub = await getAuthorizedSub(event)
    console.log(sub)
  })
})
