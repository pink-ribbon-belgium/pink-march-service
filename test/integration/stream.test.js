/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const authenticatedTestAWS = require('../_authenticatedTestAWS')
const util = require('util')
const { v4: uuidv4 } = require('uuid')
const { name } = require('../../claudia/stream').lambda
const should = require('should')
const { exampleActual: actualItemExample } = require('../../lib/server/aws/dynamodb/Item')
const streamEventExample = require('../_streamEventExample')
const { marshall } = require('aws-sdk/lib/dynamodb/converter')

const itemFlowId = '21175c30-f779-49b0-91be-7be193ffe856'
const itemCreatedAtOld = '2020-04-02T11:12:46.383Z'
const itemCreatedAtNew = '2020-04-03T11:12:46.383Z'
const itemSub = 'sub-stream-test'
const itemSubmitted = 'actual'

const callFlowId = 'd6385763-6dff-4ce5-bd91-bf0b6e33d64d'
const context = {
  awsRequestId: callFlowId
}

const lambdaInvocationParamsBase = {
  FunctionName: name,
  InvocationType: 'RequestResponse',
  LogType: 'None'
}

async function createLambdaInvokeCall (buildNumber) {
  const AWS = await authenticatedTestAWS()
  const lambda = new AWS.Lambda({ apiVersion: '2015-03-31' })
  return async function lambdaInvoker (event) {
    const params = {
      ...lambdaInvocationParamsBase,
      Qualifier: `build-${process.env.LAMBDA_BUILD}`,
      Payload: JSON.stringify(event)
    }
    const responsePromise = lambda.invoke(params).promise()
    console.log('lambda called. awaiting response …')
    const response = await responsePromise
    // eslint-disable-next-line no-undef
    console.log('lambda response', util.inspect(response))
    response.StatusCode.should.equal(200)
    return JSON.parse(response.Payload)
  }
}

function createLocalCall () {
  const localHandler = require('../../lib/stream').handler
  return async function (event) {
    const result = await localHandler(event, context)
    // when we return `undefined`, lambda returns `null`
    return result === undefined ? null : result
  }
}

let lambdaCaller

async function callLambda (event) {
  if (!lambdaCaller) {
    lambdaCaller = process.env.LAMBDA_BUILD
      ? await createLambdaInvokeCall(process.env.LAMBDA_BUILD)
      : await createLocalCall()
  }
  return lambdaCaller(event)
}

describe(testName, function () {
  this.timeout(5000) // wait for wake

  beforeEach(function () {
    this.mode = `automated-test-${uuidv4()}`

    this.createItem = function (changedValue, old) {
      return {
        ...actualItemExample,
        mode: this.mode,
        key: `/${this.mode}/something/that/is/a/key`,
        submitted: itemSubmitted,
        submittedBy: itemSub,
        flowId: itemFlowId,
        data: {
          createdAt: old ? itemCreatedAtOld : itemCreatedAtNew,
          createdBy: itemSub,
          structureVersion: 24,
          changed: changedValue
        }
      }
    }

    this.keys = { key: this.key, submitted: itemSubmitted }
    this.oldItem = this.createItem(3, true)
    this.newItem = this.createItem(5, false)
    this.wrongOldItem = this.createItem(666, true)
    delete this.wrongOldItem.key

    this.event = {
      Records: [
        {
          eventName: 'INSERT',
          dynamodb: { Keys: this.keys, NewImage: marshall(this.newItem) }
        },
        {
          eventName: 'MODIFY',
          dynamodb: { Keys: this.keys, OldImage: marshall(this.oldItem), NewImage: marshall(this.newItem) }
        },
        {
          eventName: 'REMOVE',
          dynamodb: { Keys: this.keys, OldImage: marshall(this.oldItem) }
        },
        {
          eventName: 'MODIFY',
          dynamodb: { Keys: this.keys, OldImage: marshall(this.wrongOldItem), NewImage: marshall(this.newItem) }
        }
      ]
    }
  })

  it('works with an example event', async function () {
    const response = await callLambda(streamEventExample)
    should(response).equal(null)
  })
  it('works', async function () {
    // MUDO test skeleton for now: setup some things to process
    const response = await callLambda(this.event)
    should(response).equal(null)
    // MUDO test skeleton for now: check some things that are processed
  })
})
