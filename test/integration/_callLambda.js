/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')
const fetch = require('node-fetch')
const createLambdaInvokeCall = require('./_createLambdaInvokeCall')
const APIGatewayURI = require('../../common/APIGateway').uri
const APIGateway = require('../../apigateway')
const region = require('../../claudia/service').lambda.region
const Hoek = require('@hapi/hoek')

function createLocalCall () {
  const localHandler = require('../../lib/service').handler
  const generateContext = require('./_generateContext')
  return async function (event) {
    return localHandler(event, generateContext())
  }
}

async function createHttpCall (name, buildUri) {
  return async function (event) {
    const uri = buildUri(event)
    const options = {
      method: event.httpMethod,
      headers: { ...event.headers, accept: 'application/json' },
      body: event.body ? event.body : undefined
    }
    console.log(`calling ${name} at ${options.method}:${uri}`)
    const maskedHeaders = Hoek.clone(options.headers)
    maskedHeaders.authorization = maskedHeaders.authorization ? 'Bearer XXXXXX' : maskedHeaders.authorization
    console.log('headers:', maskedHeaders)
    console.log('body:', options.body)
    console.log('awaiting response …')
    const responsePromise = await fetch(uri, options)
    const response = await responsePromise
    const headers = response.headers.raw()
    const interpretedResponse = {
      statusCode: response.status,
      headers: Object.keys(headers).reduce((acc, h) => {
        // first element of array
        acc[h] = headers[h][0]
        return acc
      }, {}),
      body: await response.text()
    }
    console.log(`${name} response`, util.inspect(interpretedResponse))
    return interpretedResponse
  }
}

async function createAPIGatewayCall (buildNumber) {
  return createHttpCall(
    'api gateway',
    event => `${APIGatewayURI(region, APIGateway.id, `api-${buildNumber}`)}${event.path}`
  )
}

async function createCloudfrontCall (stageName) {
  return createHttpCall(
    'cloudfront',
    event => `https://app.pink-march.pink-ribbon-belgium.org/${stageName}${event.path}`
  )
}

let actualCall

/**
 * Either calls the handler, locally, or the deployed lambda remotely,
 * based on build number.
 *
 * If the environment variable LAMBDA_BUILD is not set, the tests are run
 * against the local version. If it is set, the tests are run against the
 * remote Lambda alias with that build number.
 */
async function callLambda (event, context) {
  const maskedEvent = Hoek.clone(event)
  if (maskedEvent && maskedEvent.headers && maskedEvent.headers.authorization) {
    maskedEvent.headers.authorization = 'Bearer XXXXXXXXXXX'
  }
  console.log('event:', util.inspect(maskedEvent))
  if (!actualCall) {
    /* prettier-ignore */
    actualCall = process.env.CLOUDFRONT
      ? await createCloudfrontCall(process.env.CLOUDFRONT)
      : process.env.API_GATEWAY
        ? await createAPIGatewayCall(process.env.API_GATEWAY)
        : process.env.LAMBDA_BUILD
          ? await createLambdaInvokeCall(process.env.LAMBDA_BUILD)
          : createLocalCall()
  }
  const response = await actualCall(event, context)
  response.body = response.body ? JSON.parse(response.body) : response.body
  return response
}

module.exports = callLambda
