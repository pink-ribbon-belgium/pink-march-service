/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const generateBaseEvent = require('./_generateBaseEvent')
const { v4: uuidv4 } = require('uuid')
const callLambda = require('./_callLambda')
const getToken = require('../_dummyToken/create')

// TODO: Remove this if after we have resolved the FORBIDDEN issue
if (!process.env.API_GATEWAY && !process.CLOUDFRONT) {
  describe(testName, function () {
    this.timeout(5000)

    beforeEach(async function () {
      this.mode = `automated-test-${uuidv4()}`
      this.callFlowId = uuidv4()
      this.token = await getToken()
      this.event = {
        httpMethod: 'GET',
        path: '/I/health',
        resource: '/I/health',
        ...(await generateBaseEvent(this.callFlowId, this.mode, this.token))
      }

      this.shouldReport = async function (statusCode, messages) {
        const response = await callLambda(this.event)
        console.log(response)
        response.should.be.an.Object()
        response.statusCode.should.equal(statusCode)
        response.headers.should.be.an.Object()
        response.headers['x-flow-id'].should.be.a.String()
        response.body.should.be.ok()
        messages &&
          messages.forEach(m => {
            response.body.message.should.containEql(m)
          })
      }
    })

    describe('400', function () {
      it('reports 400 with an event without method', async function () {
        delete this.event.httpMethod
        return this.shouldReport(400, ['"httpMethod" is required'])
      })
      it('reports 400 with an event with an unsupported method', async function () {
        /* TODO must be 405 Method Not Allowed
            The method specified in the Request-Line is not allowed for the resource identified by the Request-URI. The
            response MUST include an Allow header containing a list of valid methods for the requested resource.
            Or, we if API Gateway can handle this, make it a pre -> 500. */
        this.event.httpMethod = 'UNSUPPORTED'
        return this.shouldReport(400, ['"httpMethod" must be one of'])
      })
      it('reports 400 with an event without a path', async function () {
        delete this.event.path
        return this.shouldReport(400, ['"path" is required'])
      })
      it('reports 400 with an event with a path value that is not a real path', async function () {
        this.event.path = 'http://example.org/not/relative'
        return this.shouldReport(400, ['"path"'])
      })
      it('reports 400 with an event without headers', async function () {
        delete this.event.headers
        return this.shouldReport(400, ['"headers" is required'])
      })
      it('reports 400 with an event with headers without a flowId', async function () {
        delete this.event.headers['x-flow-id']
        return this.shouldReport(400, ['"headers.x-flow-id" is required'])
      })
      it("reports 400 with an event with headers with a flowId that isn't a uuid", async function () {
        this.event.headers['x-flow-id'] = 'not a uuid'
        return this.shouldReport(400, ['"headers.x-flow-id" must be a valid'])
      })
      it('reports 400 with an event with headers without a mode', async function () {
        delete this.event.headers['x-mode']
        return this.shouldReport(400, ['"headers.x-mode" is required'])
      })
      it('reports 400 with an event with headers with an unsupported mode', async function () {
        this.event.headers['x-mode'] = 'unsupported'
        return this.shouldReport(400, ['"headers.x-mode"', 'fails to match'])
      })
      it('reports 401 with an event with headers without authorization', async function () {
        delete this.event.headers.authorization
        return this.shouldReport(401, ['invalid_token'])
      })
      it('reports 400 with an event with headers with an authorization that is not a JWT', async function () {
        this.event.headers.authorization = 'Bearer not a JWT'
        return this.shouldReport(400, ['"headers.authorization"', 'fails to match'])
      })
      it('reports 400 with an event with headers with an authorization with a JWT, without the Bearer prefix', async function () {
        this.event.headers.authorization = await getToken()
        return this.shouldReport(400, ['"headers.authorization"', 'fails to match'])
      })
      it('reports 400 with an event with headers with an authorization that not recognizable', async function () {
        this.event.headers.authorization = 'not recognizable'
        return this.shouldReport(400, ['"headers.authorization"', 'fails to match'])
      })
      it('reports 400 with an event without headers', async function () {
        delete this.event.pathParameters
        return this.shouldReport(400, ['"pathParameters" is required'])
      })
      it('reports 400 with an event with multiple omissions', async function () {
        delete this.event.httpMethod
        delete this.event.path
        delete this.event.headers['x-mode']
        delete this.event.headers['x-flow-id']
        delete this.event.pathParameters
        return this.shouldReport(400, [
          '"httpMethod"',
          '"path"',
          '"headers.x-mode"',
          '"headers.x-flow-id"',
          'is required'
        ])
      })
      it('400 takes precendence over 501', async function () {
        delete this.event.path
        delete this.event.resource
        return this.shouldReport(400)
      })
    })
    describe('5xx', function () {
      /* TODO could not test this, because authorization kicks in first; that has changed, and now gives a 404 or 405;
              But maybe we can test it now. On the other hand, maybe a 501 is better: it is a precondition then that we
              are not called with unsupported RAs - API Gateway blocks that - we then DO need integration test for that,
              but only when going through API Gateway.
      it('reports 501 with an unsupported resource', async function () {
        this.event.resource = '/notanendpoint'
        return this.shouldReport(501, [
          `Operation "${this.event.httpMethod}:${this.event.resource}" is not implemented`
        ])
      })
      */
      it('forces an error when requested', async function () {
        this.event.headers['x-force-error'] = 555
        return this.shouldReport(555)
      })
    })
  })
}
