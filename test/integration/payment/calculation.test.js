/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const DynamodbFixture = require('../../_DynamodbFixture')
const getToken = require('../../_getTrueToken')
const testName = require('../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const getIntegrationTestRunMode = require('../_getIntegrationTestRunMode')
const callLambda = require('../_callLambda')
const util = require('util')
const generateBaseEvent = require('../_generateBaseEvent')

const mode = getIntegrationTestRunMode()

describe(testName, function () {
  describe('post', function () {
    beforeEach(async function () {
      this.dynamodbFixture = new DynamodbFixture()
      this.auth = await getToken(mode, this.dynamodbFixture)
      this.flowId = uuidv4()
      const baseEvent = generateBaseEvent(this.flowId, mode, this.auth.token)
      this.event = {
        httpMethod: 'POST',
        path: '/I/payment/calculation',
        resource: '/I/payment/calculation',
        ...baseEvent
      }

      this.callLambda = async function (params) {
        this.event.body = JSON.stringify(params)
        return callLambda(this.event)
      }
    })

    it('should work', async function () {
      const params = {
        numberOfParticipants: 4,
        groupId: uuidv4()
      }
      const expectedResult = {
        unitPrice: 10,
        slotsToAdd: 4,
        numberOfParticipants: 4,
        amountPaid: 0,
        subTotal: 40,
        discount: 0,
        total: 40,
        voucherIsValid: true
      }
      const response = await this.callLambda(params)

      console.log('response:', util.inspect(response))
      response.should.be.an.Object()
      response.statusCode.should.equal(200)
      response.headers.should.be.an.Object()
      response.headers['x-flow-id'].should.equal(this.flowId)
      response.body.should.be.an.Object()
      response.body.should.deepEqual(expectedResult)
    })
  })
})
