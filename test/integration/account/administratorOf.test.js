/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const generateGetPutTest = require('../_generateGetPutTest')
const dynamodbExample = require('../../../lib/api/group/GroupAdministrationDTO').exampleDynamodb
const GroupAdministration = require('../../../lib/server/business/group/administration/GroupAdministration')
const GroupType = require('../../../lib/api/group/GroupType')

describe(testName, function () {
  this.timeout(5000)

  GroupType.values.forEach(groupType => {
    const groupIdName = `${groupType.toLowerCase()}Id`
    describe(groupType, function () {
      generateGetPutTest(
        '/I/account/{accountId}/administratorOf',
        auth => ({
          accountId: auth.accountId
        }),
        auth => ({
          accountId: 'notMyAccountId'
        }),
        auth => ({
          ...dynamodbExample,
          accountId: auth.accountId,
          groupId: auth[groupIdName],
          groupType,
          deleted: false
        }),
        GroupAdministration,
        (body, auth) => {
          body.should.be.an.Array()
          body.length.should.be.above(0)
          body.forEach(ga => {
            ga.should.be.an.Object()
            ga.createdAt.should.be.a.String()
            ga.createdBy.should.be.a.String()
            ga.structureVersion.should.be.a.Number()
            ga.accountId.should.equal(auth.accountId)
            ga.groupType.should.equalOneOf(GroupType.values)
            if (ga.groupType === GroupType.teamType) {
              ga.groupId.should.equal(auth.teamId)
            } else {
              ga.groupId.should.equal(auth.companyId)
            }
            ga.deleted.should.be.false()
          })
        },
        item => {
          item.submitted = 'actual'
          item.partition_key_A = `/${item.mode}/account/${item.data.accountId}/administratorOf`
          item.sort_key_A = 'actual'
          item.partition_key_B = `/${item.mode}/group/${item.data.groupId}/administrators`
          item.sort_key_B = 'actual'
        },
        false,
        true
      )
    })
  })
})
