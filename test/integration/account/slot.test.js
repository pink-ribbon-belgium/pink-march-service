/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const generateGetPutTest = require('../_generateGetPutTest')
const slotDynamodbExample = require('../../../lib/api/slot/SlotDTO').exampleDynamodb
const Slot = require('../../../lib/server/business/slot/Slot')
const { v4: uuidv4 } = require('uuid')
const should = require('should')

const slotId = uuidv4()
const paymentId = uuidv4()

describe(testName, function () {
  this.timeout(5000)

  generateGetPutTest(
    '/I/account/{accountId}/slot',
    auth => ({
      accountId: auth.accountId
    }),
    auth => ({
      accountId: 'notMyAccountId'
    }),
    auth => ({
      ...slotDynamodbExample,
      id: slotId,
      accountId: auth.accountId,
      groupId: auth.companyId,
      groupType: 'Company',
      paymentId
    }),
    Slot,
    (body, auth) => {
      body.should.be.an.Object()
      body.createdAt.should.be.a.String()
      body.createdBy.should.be.a.String()
      body.structureVersion.should.be.a.Number()
      body.accountId.should.equal(auth.accountId)
      body.groupId.should.equal(auth.companyId)
      should(body.deleted).be.not.ok()
    },
    item => {
      item.submitted = 'actual'
      item.partition_key_A = `/${item.mode}/payment/${paymentId}/slots`
      item.sort_key_A = 'actual'
      item.partition_key_B = `/${item.mode}/group/${item.data.groupId}/members`
      item.sort_key_B = 'actual'
      item.partition_key_C = `/${item.mode}/account/${item.data.accountId}/slot`
      item.sort_key_C = 'actual'
    },
    undefined,
    true
  )
})
