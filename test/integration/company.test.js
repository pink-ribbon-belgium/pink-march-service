/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const generateGetPutTest = require('./_generateGetPutTest')
const companyDynamodbExample = require('../../lib/api/company/CompanyDTO').exampleDynamodb
const Company = require('../../lib/server/business/company/Company')

describe(testName, function () {
  this.timeout(5000)

  generateGetPutTest(
    '/I/company/{companyId}',
    auth => ({
      companyId: auth.companyId
    }),
    auth => ({
      companyId: '47120006-c476-4dd1-9dfe-e7ca872c2e09s' // will not be authorized
    }),
    auth => ({
      ...companyDynamodbExample,
      id: auth.companyId
    }),
    Company,
    undefined,
    undefined,
    false,
    undefined,
    true
  )
})
