/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const Joi = require('@hapi/joi')
const should = require('should')
const { ValidationError } = require('@hapi/joi/lib/errors')

/* eslint-env mocha */

/**
 * Create tests that make sure `schema` is a serious schema.
 *
 * Use inside a `describe`.
 */
module.exports = function expectSeriousSchema (schema, failures, unknownAllowed) {
  it('is a Joi schema', function () {
    should(schema).be.ok()
    Joi.isSchema(schema).should.be.true()
  })
  if (schema.type === 'object') {
    if (unknownAllowed) {
      it('allows unknown keys (allow for server evolution)', function () {
        schema._flags.should.be.an.Object()
        schema._flags.unknown.should.be.true()
      })
    } else {
      it('does not allow unknown keys', function () {
        schema._flags.should.be.an.Object()
        should(schema._flags.unknown).not.be.ok()
      })
    }
  }
  it('has at least one example', function () {
    schema.$_terms.should.be.an.Object()
    schema.$_terms.examples.should.be.an.Array()
    schema.$_terms.examples.length.should.be.greaterThan(0)
  })
  describe('examples', function () {
    if (
      schema.$_terms !== undefined &&
      schema.$_terms.examples !== undefined &&
      schema.$_terms.examples.forEach !== undefined
    ) {
      schema.$_terms.examples.forEach(function (ex, index) {
        it(`${index} : example ${JSON.stringify(ex)} passes the schema`, function () {
          should(schema.validate(ex).error).not.be.ok()
        })
      })
    }
  })
  describe('failures', function () {
    failures.forEach(function (f, i) {
      it(i + ': fails for ' + JSON.stringify(f), function () {
        const expectedError = schema.validate(f).error
        should(expectedError).be.ok()
        expectedError.should.be.an.Error()
        expectedError.should.be.instanceof(ValidationError)
        console.log(expectedError)
      })
    })
  })
}
