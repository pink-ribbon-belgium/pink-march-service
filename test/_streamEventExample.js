/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

module.exports = {
  Records: [
    {
      eventID: '3606a1b3b265631c0a1c1e3e2a0d6570',
      eventName: 'REMOVE',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929118,
        Keys: {
          submitted: {
            S: 'actual'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        OldImage: {
          mode: {
            S: 'automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be'
          },
          submittedBy: {
            S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: 'actual'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-01-23T15:22:39.212Z'
              },
              accountId: {
                S: 'V4KuwuerXZ'
              },
              groupType: {
                S: 'Team'
              },
              deleted: {
                BOOL: false
              },
              createdBy: {
                S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/administrators'
          },
          partition_key_A: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/account/V4KuwuerXZ/administratorOf'
          },
          flowId: {
            S: 'f8277c1e-b36a-4003-bdea-0e65ef1afa3b'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        SequenceNumber: '145041200000000051109177791',
        SizeBytes: 956,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '9c86f57ac1ca3fd4b7d35f4e49cca29f',
      eventName: 'REMOVE',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929120,
        Keys: {
          submitted: {
            S: '2020-03-25T15:04:14.491Z'
          },
          key: {
            S:
              '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/slot/180b0f0b-1983-44c2-8f6f-9b8439045184'
          }
        },
        OldImage: {
          mode: {
            S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880'
          },
          sort_key_C: {
            S: 'actual'
          },
          submittedBy: {
            S: 'sub-group-slots-integration-test-3'
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: '2020-03-25T15:04:14.491Z'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-03-25T15:04:14.491Z'
              },
              groupType: {
                S: 'Team'
              },
              createdBy: {
                S: 'sub-group-slots-integration-test-3'
              },
              paymentId: {
                S: 'e6014e41-a968-41d1-8fdc-5e3922734b4a'
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              id: {
                S: '180b0f0b-1983-44c2-8f6f-9b8439045184'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/free'
          },
          partition_key_A: {
            S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/payment/e6014e41-a968-41d1-8fdc-5e3922734b4a/slots'
          },
          flowId: {
            S: '1c396a09-ee99-42a3-873a-ea6080778329'
          },
          key: {
            S:
              '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/slot/180b0f0b-1983-44c2-8f6f-9b8439045184'
          }
        },
        SequenceNumber: '145041300000000051109178737',
        SizeBytes: 1016,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: 'e0b51feb0f3d8b10f813bee83c3082b0',
      eventName: 'REMOVE',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929120,
        Keys: {
          submitted: {
            S: 'actual'
          },
          key: {
            S:
              '/automated-test-d8eaac4f-7573-4c1e-8e3a-cb1a427023c7/group/2c823261-0363-4749-961f-08b5c5cca713/slot/2a1c641f-8207-4958-b33d-c19a79442972'
          }
        },
        OldImage: {
          mode: {
            S: 'automated-test-d8eaac4f-7573-4c1e-8e3a-cb1a427023c7'
          },
          submittedBy: {
            S: 'sub-group-slots-get-test-slot-0'
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: 'actual'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-03-30T14:52:10.373Z'
              },
              groupType: {
                S: 'Company'
              },
              createdBy: {
                S: 'sub-group-slots-get-test-slot-0'
              },
              paymentId: {
                S: '9e3c87c1-3529-49b3-a6f3-591beb6f7219'
              },
              groupId: {
                S: '2c823261-0363-4749-961f-08b5c5cca713'
              },
              id: {
                S: '2a1c641f-8207-4958-b33d-c19a79442972'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-d8eaac4f-7573-4c1e-8e3a-cb1a427023c7/group/2c823261-0363-4749-961f-08b5c5cca713/free'
          },
          partition_key_A: {
            S: '/automated-test-d8eaac4f-7573-4c1e-8e3a-cb1a427023c7/payment/9e3c87c1-3529-49b3-a6f3-591beb6f7219/slots'
          },
          flowId: {
            S: '8e8e5e8d-feec-4019-9493-90e186a1ae90'
          },
          key: {
            S:
              '/automated-test-d8eaac4f-7573-4c1e-8e3a-cb1a427023c7/group/2c823261-0363-4749-961f-08b5c5cca713/slot/2a1c641f-8207-4958-b33d-c19a79442972'
          }
        },
        SequenceNumber: '145041400000000051109178763',
        SizeBytes: 961,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '881bccde6fb4870f81253461ab2cd54a',
      eventName: 'INSERT',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929122,
        Keys: {
          submitted: {
            S: '2020-03-25T15:04:14.491Z'
          },
          key: {
            S:
              '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/slot/180b0f0b-1983-44c2-8f6f-9b8439045184'
          }
        },
        NewImage: {
          mode: {
            S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880'
          },
          sort_key_C: {
            S: 'actual'
          },
          submittedBy: {
            S: 'sub-group-slots-integration-test-3'
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: '2020-03-25T15:04:14.491Z'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-03-25T15:04:14.491Z'
              },
              groupType: {
                S: 'Team'
              },
              createdBy: {
                S: 'sub-group-slots-integration-test-3'
              },
              paymentId: {
                S: 'e6014e41-a968-41d1-8fdc-5e3922734b4a'
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              id: {
                S: '180b0f0b-1983-44c2-8f6f-9b8439045184'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/free'
          },
          partition_key_A: {
            S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/payment/e6014e41-a968-41d1-8fdc-5e3922734b4a/slots'
          },
          flowId: {
            S: '1c396a09-ee99-42a3-873a-ea6080778329'
          },
          key: {
            S:
              '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f/slot/180b0f0b-1983-44c2-8f6f-9b8439045184'
          }
        },
        SequenceNumber: '145041500000000051109179320',
        SizeBytes: 1016,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: 'fdcf8f5dfc33fcbf80995724c2b943b7',
      eventName: 'INSERT',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929123,
        Keys: {
          submitted: {
            S: 'actual'
          },
          key: {
            S:
              '/automated-test-0acaa4d2-a5de-4364-a8d3-d523090d52ff/group/2c823261-0363-4749-961f-08b5c5cca713/slot/9766dfaa-a3f9-4c63-bb38-374a5e540663'
          }
        },
        NewImage: {
          mode: {
            S: 'automated-test-0acaa4d2-a5de-4364-a8d3-d523090d52ff'
          },
          submittedBy: {
            S: 'sub-group-slots-get-test-slot-4'
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: 'actual'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-03-30T14:52:10.373Z'
              },
              groupType: {
                S: 'Company'
              },
              createdBy: {
                S: 'sub-group-slots-get-test-slot-4'
              },
              paymentId: {
                S: '9e3c87c1-3529-49b3-a6f3-591beb6f7219'
              },
              groupId: {
                S: '2c823261-0363-4749-961f-08b5c5cca713'
              },
              id: {
                S: '9766dfaa-a3f9-4c63-bb38-374a5e540663'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-0acaa4d2-a5de-4364-a8d3-d523090d52ff/group/2c823261-0363-4749-961f-08b5c5cca713/free'
          },
          partition_key_A: {
            S: '/automated-test-0acaa4d2-a5de-4364-a8d3-d523090d52ff/payment/9e3c87c1-3529-49b3-a6f3-591beb6f7219/slots'
          },
          flowId: {
            S: '8e8e5e8d-feec-4019-9493-90e186a1ae90'
          },
          key: {
            S:
              '/automated-test-0acaa4d2-a5de-4364-a8d3-d523090d52ff/group/2c823261-0363-4749-961f-08b5c5cca713/slot/9766dfaa-a3f9-4c63-bb38-374a5e540663'
          }
        },
        SequenceNumber: '145041600000000051109179894',
        SizeBytes: 961,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '72eca9f192d463d6dd096ed2cad51894',
      eventName: 'INSERT',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929124,
        Keys: {
          submitted: {
            S: 'actual'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        NewImage: {
          mode: {
            S: 'automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be'
          },
          submittedBy: {
            S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: 'actual'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-01-23T15:22:39.212Z'
              },
              accountId: {
                S: 'V4KuwuerXZ'
              },
              groupType: {
                S: 'Team'
              },
              deleted: {
                BOOL: false
              },
              createdBy: {
                S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/group/undefined/administrators'
          },
          partition_key_A: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/account/V4KuwuerXZ/administratorOf'
          },
          flowId: {
            S: 'f8277c1e-b36a-4003-bdea-0e65ef1afa3b'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        SequenceNumber: '145041700000000051109180666',
        SizeBytes: 929,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '6823485f3679e36f33aca36960e46335',
      eventName: 'REMOVE',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929125,
        Keys: {
          submitted: {
            S: 'actual'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        OldImage: {
          mode: {
            S: 'automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be'
          },
          submittedBy: {
            S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
          },
          sort_key_A: {
            S: 'actual'
          },
          sort_key_B: {
            S: 'actual'
          },
          submitted: {
            S: 'actual'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-01-23T15:22:39.212Z'
              },
              accountId: {
                S: 'V4KuwuerXZ'
              },
              groupType: {
                S: 'Team'
              },
              deleted: {
                BOOL: false
              },
              createdBy: {
                S: "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ-.~%!$&'()*+,"
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/group/undefined/administrators'
          },
          partition_key_A: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/account/V4KuwuerXZ/administratorOf'
          },
          flowId: {
            S: 'f8277c1e-b36a-4003-bdea-0e65ef1afa3b'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        SequenceNumber: '145041800000000051109180870',
        SizeBytes: 929,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '35eac22855ea6f922ab92bc7fed639f2',
      eventName: 'INSERT',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929125,
        Keys: {
          submitted: {
            S: '2020-04-03T15:52:05.240Z'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        NewImage: {
          mode: {
            S: 'automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be'
          },
          submittedBy: {
            S: 'auth0%7C5e53ec67461a990ce3c5f0ff'
          },
          sort_key_A: {
            S: '2020-04-03T15:52:05.240Z'
          },
          sort_key_B: {
            S: '2020-04-03T15:52:05.240Z'
          },
          submitted: {
            S: '2020-04-03T15:52:05.240Z'
          },
          data: {
            M: {
              createdAt: {
                S: '2020-04-03T15:52:05.240Z'
              },
              accountId: {
                S: 'V4KuwuerXZ'
              },
              groupType: {
                S: 'Team'
              },
              deleted: {
                BOOL: false
              },
              createdBy: {
                S: 'auth0%7C5e53ec67461a990ce3c5f0ff'
              },
              groupId: {
                S: 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
              },
              structureVersion: {
                N: '2'
              }
            }
          },
          partition_key_B: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/group/undefined/administrators'
          },
          partition_key_A: {
            S: '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/account/V4KuwuerXZ/administratorOf'
          },
          flowId: {
            S: 'f66159ff-39cf-453b-bf47-71d56417409f'
          },
          key: {
            S:
              '/automated-test-fd20632f-6dc3-4493-b79a-dccb894d46be/groupAdministration/account/V4KuwuerXZ/group/ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
          }
        },
        SequenceNumber: '145041900000000051109181010',
        SizeBytes: 937,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    {
      eventID: '26f2dcde0967b1e4cc945b6eeb45c5cf',
      eventName: 'MODIFY',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929139,
        Keys: {
          submitted: 'actual',
          key: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection'
        },
        NewImage: {
          mode: { S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880' },
          submittedBy: { S: 'auth0%7C5e53ec67461a990ce3c5f0ff' },
          sort_key_A: { S: 'actual' },
          sort_key_B: { S: 'actual' },
          submitted: { S: 'actual' },
          data: {
            M: {
              createdAt: { S: '2020-04-03T15:52:19.138Z' },
              accountId: { S: 'V4KuwuerXZ' },
              deleted: { BOOL: true },
              createdBy: { S: 'auth0%7C5e53ec67461a990ce3c5f0ff' },
              structureVersion: { N: '1' }
            }
          },
          flowId: { S: 'b90448c8-871b-48a8-9032-ebae8bf463c5' },
          key: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection' }
        },
        OldImage: {
          mode: { S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880' },
          submittedBy: { S: 'V4KuwuerXZ' },
          sort_key_A: { S: '2020-03-31T08:30:25.125Z' },
          sort_key_B: { S: '2020-03-31T08:30:25.125Z' },
          submitted: { S: 'actual' },
          data: {
            M: {
              encryptedToken: { S: 'WA==' },
              createdAt: { S: '2020-03-31T08:30:25.125Z' },
              accountId: { S: 'V4KuwuerXZ' },
              createdBy: { S: 'V4KuwuerXZ' },
              trackerType: { S: 'fitbit' },
              tokenType: { S: 'refresh' },
              exp: { S: '2020-03-31T08:30:25.125Z' },
              structureVersion: { N: '1' }
            }
          },
          partition_key_B: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/trackerConnection/fitbit' },
          partition_key_A: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/trackerConnection' },
          flowId: { S: '98521c71-9a25-40da-bd2c-9e26e1c8111e' },
          key: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection' }
        },
        SequenceNumber: '144922400000000004873190779',
        SizeBytes: 1169,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    },
    // the NewImage in this record is not a good Item
    {
      eventID: '26f2dcde0967b1e4cc945b6eeb45c5cf',
      eventName: 'MODIFY',
      eventVersion: '1.1',
      eventSource: 'aws:dynamodb',
      awsRegion: 'eu-west-1',
      dynamodb: {
        ApproximateCreationDateTime: 1585929139,
        Keys: {
          submitted: 'actual',
          key: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection'
        },
        NewImage: {
          mode: { S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880' },
          submittedBy: { S: 'auth0%7C5e53ec67461a990ce3c5f0ff' },
          sort_key_A: { S: 'actual' },
          sort_key_B: { S: 'actual' },
          submitted: { S: 'actual' },
          data: {
            M: {
              createdAt: { S: '2020-04-03T15:52:19.138Z' },
              accountId: { S: 'V4KuwuerXZ' },
              deleted: { BOOL: true },
              createdBy: { S: 'auth0%7C5e53ec67461a990ce3c5f0ff' },
              structureVersion: { N: '1' }
            }
          },
          flowId: { S: 'NOT A GOOD FLOW ID' },
          key: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection' }
        },
        OldImage: {
          mode: { S: 'automated-test-f5699942-cc8b-4ce3-9564-361d750eb880' },
          submittedBy: { S: 'V4KuwuerXZ' },
          sort_key_A: { S: '2020-03-31T08:30:25.125Z' },
          sort_key_B: { S: '2020-03-31T08:30:25.125Z' },
          submitted: { S: 'actual' },
          data: {
            M: {
              encryptedToken: { S: 'WA==' },
              createdAt: { S: '2020-03-31T08:30:25.125Z' },
              accountId: { S: 'V4KuwuerXZ' },
              createdBy: { S: 'V4KuwuerXZ' },
              trackerType: { S: 'fitbit' },
              tokenType: { S: 'refresh' },
              exp: { S: '2020-03-31T08:30:25.125Z' },
              structureVersion: { N: '1' }
            }
          },
          partition_key_B: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/trackerConnection/fitbit' },
          partition_key_A: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/trackerConnection' },
          flowId: { S: '98521c71-9a25-40da-bd2c-9e26e1c8111e' },
          key: { S: '/automated-test-f5699942-cc8b-4ce3-9564-361d750eb880/account/V4KuwuerXZ/trackerConnection' }
        },
        SequenceNumber: '144922400000000004873190779',
        SizeBytes: 1169,
        StreamViewType: 'NEW_AND_OLD_IMAGES'
      },
      eventSourceARN:
        'arn:aws:dynamodb:eu-west-1:254473600415:table/pink-march-service-test/stream/2020-04-03T14:54:22.284'
    }
  ]
}
