/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const fetch = require('node-fetch')
const config = require('config')
const JWT = require('jsonwebtoken')
const { promisify } = require('util')
const publicSigningKeyGetter = require('../lib/server/authorization/publicSigningKeyGetter').sts
const customClaims = require('../lib/server/authorization/customClaims')
const assert = require('assert')
const groupAdministrationDynamodbExample = require('../lib/api/group/GroupAdministrationDTO').exampleDynamodb
const GroupAdministration = require('../lib/server/business/group/administration/GroupAdministration')

const verify = promisify(JWT.verify)

const tokenEndpoint = 'https://auth-dev.pink-march.pink-ribbon-belgium.org/oauth/token'
// noinspection SpellCheckingInspection
const clientId = 'Nqh2z2JEwdmspCW03zys5xeQWi4K97xS'

const tokens = {}

const teamId = 'ff5f77ff-9475-4eaa-ae5c-6c3de6f13e8f'
const companyId = '4b90243d-d19a-49c7-a8ca-85c24b2546b0'

function groupAdministrationItem (mode, groupType) {
  const accountId = config.authorization.automatedTestUserCredentials.accountId
  const actual = 'actual'
  const data = {
    ...groupAdministrationDynamodbExample,
    accountId,
    groupId: groupType === 'Team' ? teamId : companyId,
    deleted: false,
    groupType
  }
  const model = new GroupAdministration({
    sot: groupAdministrationDynamodbExample.createdAt,
    sub: groupAdministrationDynamodbExample.createdBy,
    mode,
    dto: data
  })
  return {
    mode,
    key: model.key,
    submitted: actual,
    submittedBy: data.createdBy,
    flowId: '4dd438fc-02d6-4d03-baa5-ac58e9e39aaa',
    partition_key_A: `/${mode}/account/${accountId}/administratorOf`,
    sort_key_A: actual,
    partition_key_B: `/${mode}/group/${groupType === 'Team' ? teamId : companyId}/administrators`,
    sort_key_B: actual,
    data
  }
}

/**
 * Get a token using username and password for integration tests
 */
async function getTrueToken (mode, dynamodbFixture) {
  if (!tokens[mode]) {
    if (dynamodbFixture) {
      console.log('creating administrations, so user will have necessary raas')
      const teamItem = groupAdministrationItem(mode, 'Team')
      const groupItem = groupAdministrationItem(mode, 'Company')
      await Promise.all([dynamodbFixture.putItem(teamItem), dynamodbFixture.putItem(groupItem)]) // NOTE: will be removed by clean up after first test
    }
    console.log(`getting token for ${mode}`)
    const response = await fetch(tokenEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify({
        client_id: clientId,
        ...config.authorization.automatedTestUserCredentials,
        audience: config.authorization.audience,
        scopes: [config.authorization.scope],
        grant_type: 'password',
        mode
      })
    })
    const body = await response.json()
    if (body.error) {
      console.error(body.error_description)
      console.error(`Could not retrieve token from ${tokenEndpoint}`)
      console.error(`
This test code requires

    authorization:
      automatedTestUserCredentials:
        username: <AUTOMATED TEST USER NAME>
        password: <AUTOMATED TEST USER PASSWORD>

in \`config/local.yaml\``)
      const err = new Error(body.error)
      err.description = body.error_description
      throw err
    }
    console.log('token received')
    const token = body.access_token
    const decoded = await verify(token, publicSigningKeyGetter, {
      algorithms: ['RS256'],
      clockTolerance: 1000
    })
    console.log('token decoded', decoded)
    assert.strictEqual(decoded[customClaims.modeClaim], mode)
    tokens[mode] = {
      token,
      sub: decoded.sub,
      accountId: decoded[customClaims.accountIdClaim],
      mode: decoded[customClaims.modeClaim],
      teamId,
      companyId
    }
  }
  return tokens[mode]
}

module.exports = getTrueToken
