/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const { v4: uuidv4 } = require('uuid')
const StreamRecordHandlerContract = require('../../lib/stream/StreamRecordHandlerContract')
const should = require('should')
const { exampleActual: actualItemExample } = require('../../lib/server/aws/dynamodb/Item')
const log = require('../../lib/_util/simpleLog')

const callFlowId = '4205e454-b5f1-402b-9ae8-2899f4f63c8f'
const context = {
  awsRequestId: callFlowId
}
const itemFlowId = 'e447d9df-0f69-4bff-9cad-ff9323567583'
const itemCreatedAtOld = '2020-04-02T11:12:46.383Z'
const itemCreatedAtNew = '2020-04-03T11:12:46.383Z'
const itemSub = 'sub-stream-test'
const itemSubmitted = 'actual'

/* NOTE: This tests the unmarshalled handler. The marshalled handler is not tested directly in unit tests. */
describe(testName, function () {
  beforeEach(async function () {
    this.sandbox = sinon.createSandbox()
    this.mode = `automated-test-${uuidv4()}`

    this.infoSpy = this.sandbox.spy(log, 'info')
    this.warnSpy = this.sandbox.spy(log, 'warn')
    this.errorSpy = this.sandbox.spy(log, 'error')

    this.streamRecordHandlerStubs = new Array(3).fill(undefined).map(() => this.sandbox.stub())
    this.streamRecordHandlers = this.streamRecordHandlerStubs.map(stub =>
      StreamRecordHandlerContract.implementation(stub)
    )

    this.stream = proxyquire('../../lib/stream', {
      './stream/streamRecordHandlers': this.streamRecordHandlers
    })
    this.handler = this.stream.unmarshalledHandler
    this.handler.contract.verifyPostconditions = true

    this.key = `/${this.mode}/something/that/is/a/key`

    this.createItem = function (changedValue, old) {
      return {
        ...actualItemExample,
        mode: this.mode,
        key: this.key,
        submitted: itemSubmitted,
        submittedBy: itemSub,
        flowId: itemFlowId,
        data: {
          createdAt: old ? itemCreatedAtOld : itemCreatedAtNew,
          createdBy: itemSub,
          structureVersion: 24,
          changed: changedValue
        }
      }
    }

    this.keys = { key: this.key, submitted: itemSubmitted }
    this.combinedKey = `${this.key}#${itemSubmitted}`
    this.oldItem = this.createItem(3, true)
    this.newItem = this.createItem(5, false)
    this.wrongNewItem = this.createItem(666, false)
    delete this.wrongNewItem.submittedBy

    this.createEvent1Record = function (hasOld, hasNew) {
      const dynamodb = { Keys: this.keys }
      if (hasNew) {
        dynamodb.NewImage = this.newItem
      }
      if (hasOld) {
        dynamodb.OldImage = this.oldItem
      }
      return {
        Records: [{ eventName: hasNew ? (hasOld ? 'MODIFY' : 'INSERT') : hasOld ? 'REMOVE' : 'impossible', dynamodb }]
      }
    }

    this.shouldResolve1Record = async function (event, hasOld) {
      const response = await this.handler(event, context)

      should(response).be.undefined()
      const sot = this.streamRecordHandlerStubs[0].getCalls()[0].args[1]
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.should.be.calledOnce()
        streamRecordHandler.should.be.calledWithMatch(callFlowId, sot, {
          mode: this.mode,
          event: hasOld ? 'MODIFY' : 'INSERT',
          keys: this.keys,
          oldItem: hasOld ? this.oldItem : undefined,
          newItem: this.newItem
        })
      })
    }

    this.allShouldBeWell = function (nrOfRecordHandlings, nrOfUnhandled) {
      // .should.be.calledXXX() doesn't work with `module` as an argument (https://github.com/shouldjs/sinon/issues/17)
      this.warnSpy.called.should.not.be.true()
      this.errorSpy.called.should.not.be.true()
      const infoCalls = this.infoSpy.getCalls()
      const nrOfInfoCalls = 2 // was => const nrOfInfoCalls = nrOfRecordHandlings + 2  => changed due to reduce of logging
      infoCalls.length.should.equal(nrOfInfoCalls)
      const finalInfo = this.infoSpy.getCalls()[nrOfInfoCalls - 1].args[4]
      console.log(finalInfo)
      finalInfo.recordHandlings.total.should.equal(nrOfRecordHandlings)
      finalInfo.recordHandlings.errors.should.equal(0)
      finalInfo.recordHandlings.unhandled.should.equal(nrOfUnhandled)
      finalInfo.recordHandlings.handled.should.equal(nrOfRecordHandlings - nrOfUnhandled)
    }
  })

  afterEach(function () {
    this.handler.contract.verifyPostconditions = false
    this.streamRecordHandlers.forEach(contractFunction => {
      contractFunction.contract.verifyPostconditions = false
    })
    this.sandbox.restore()
  })

  describe('expected behavior', function () {
    beforeEach(async function () {
      this.streamRecordHandlers.forEach(contractFunction => {
        contractFunction.contract.verifyPostconditions = true
      })
    })

    it('works with 1 resource record with old and new, handled', async function () {
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.resolves()
      })

      await this.shouldResolve1Record(this.createEvent1Record(true, true), true)

      this.allShouldBeWell(this.streamRecordHandlers.length, 0)
    })

    it('works with 1 resource record with only new, handled', async function () {
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.resolves()
      })

      await this.shouldResolve1Record(this.createEvent1Record(false, true), false)

      this.allShouldBeWell(this.streamRecordHandlers.length, 0)
    })

    it('works with 1 resource record with only old (REMOVE) - does not call handlers', async function () {
      const response = await this.handler(this.createEvent1Record(true, false), context)

      should(response).be.undefined()
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.should.not.be.called()
      })

      this.allShouldBeWell(0, 0)
    })

    it('works with 1 resource record, unhandled', async function () {
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.resolves(false)
      })

      await this.shouldResolve1Record(this.createEvent1Record(true, true), true)

      this.allShouldBeWell(this.streamRecordHandlers.length, this.streamRecordHandlers.length)
    })

    it('works with 1 resource record that has a wrong new item', async function () {
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.resolves(true)
      })

      const response = await this.handler(
        {
          Records: [
            {
              eventName: 'INSERT',
              dynamodb: { Keys: this.keys, NewImage: this.wrongNewItem }
            }
          ]
        },
        context
      )

      should(response).be.undefined()
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.should.not.be.called()
      })

      // .should.be.calledXXX() doesn't work with `module` as an argument (https://github.com/shouldjs/sinon/issues/17)
      this.infoSpy.calledTwice.should.be.true()
      this.warnSpy.called.should.not.be.true()
      this.errorSpy.calledOnce.should.be.true()
    })

    it('works with 1 resource record that has a wrong old item', async function () {
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.resolves(true)
      })

      const response = await this.handler(
        {
          Records: [
            {
              eventName: 'MODIFY',
              dynamodb: { Keys: this.keys, OldImage: this.wrongNewItem, NewImage: this.newItem }
            }
          ]
        },
        context
      )

      should(response).be.undefined()
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.should.not.be.called()
      })

      // .should.be.calledXXX() doesn't work with `module` as an argument (https://github.com/shouldjs/sinon/issues/17)
      this.infoSpy.calledTwice.should.be.true()
      this.warnSpy.called.should.not.be.true()
      this.errorSpy.calledOnce.should.be.true()
    })

    it('works with 3 resource records', async function () {
      const event = {
        Records: [
          {
            eventName: 'INSERT',
            dynamodb: { Keys: this.keys, NewImage: this.newItem },
            resolution: undefined
          },
          {
            eventName: 'MODIFY',
            dynamodb: { Keys: this.keys, OldImage: this.oldItem, NewImage: this.newItem },
            resolution: false
          },
          {
            eventName: 'MODIFY',
            dynamodb: { Keys: this.keys, OldImage: this.oldItem, NewImage: this.newItem },
            resolution: undefined
          }
        ]
      }

      this.streamRecordHandlerStubs.forEach((streamRecordHandler, index) => {
        streamRecordHandler.resolves(event.Records[index].resolution)
      })

      const response = await this.handler(event, context)

      should(response).be.undefined()
      const sot = this.streamRecordHandlerStubs[0].getCalls()[0].args[1]
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.should.be.calledThrice()
        streamRecordHandler.should.be.alwaysCalledWithMatch(callFlowId, sot, {
          mode: this.mode,
          event: sinon.match.string,
          keys: this.keys,
          oldItem: sinon.match.object.or(undefined),
          newItem: sinon.match.object
        })
        const calls = streamRecordHandler.getCalls()
        calls[0].args[2].event.should.equal('INSERT')
        should(calls[0].args[2].oldItem).be.undefined()
        calls[0].args[2].newItem.should.deepEqual(this.newItem)
        calls[1].args[2].event.should.equal('MODIFY')
        calls[1].args[2].oldItem.should.deepEqual(this.oldItem)
        calls[2].args[2].event.should.equal('MODIFY')
        calls[2].args[2].oldItem.should.deepEqual(this.oldItem)
      })

      // .should.be.calledXXX() doesn't work with `module` as an argument (https://github.com/shouldjs/sinon/issues/17)
      this.warnSpy.called.should.not.be.true()
      this.errorSpy.called.should.not.be.true()
      const infoCalls = this.infoSpy.getCalls()
      const nrOfRecordHandlings = this.streamRecordHandlers.length * event.Records.length
      const nrOfUnhandled = event.Records.length
      const nrOfInfoCalls = 2 // was const nrOfInfoCalls = nrOfRecordHandlings + 2 => reduced logging
      infoCalls.length.should.equal(nrOfInfoCalls)
      const finalInfo = infoCalls[nrOfInfoCalls - 1].args[4]
      console.log(finalInfo)
      finalInfo.recordHandlings.total.should.equal(nrOfRecordHandlings)
      finalInfo.recordHandlings.errors.should.equal(0)
      finalInfo.recordHandlings.unhandled.should.equal(nrOfUnhandled)
      finalInfo.recordHandlings.handled.should.equal(nrOfRecordHandlings - nrOfUnhandled)
    })
  })

  describe('unexpected behavior', function () {
    it('reports when 1 stream handler throws', async function () {
      const error = new Error()
      this.streamRecordHandlerStubs.forEach((streamRecordHandler, index) => {
        if (index === 2) {
          streamRecordHandler.rejects(error)
        } else {
          streamRecordHandler.resolves()
        }
      })

      await this.shouldResolve1Record(this.createEvent1Record(true, true), true)

      this.warnSpy.calledOnce.should.be.true()
      this.errorSpy.calledOnce.should.be.true()
      const infoCalls = this.infoSpy.getCalls()
      // infoCalls.length.should.equal(this.streamRecordHandlers.length + 2 - 2) // 1 error, final is warning
      infoCalls.length.should.equal(1) // Reduced the logging
      const finalInfo = this.warnSpy.getCalls()[0].args[4]
      console.log(finalInfo)
      finalInfo.recordHandlings.total.should.equal(this.streamRecordHandlers.length)
      finalInfo.recordHandlings.errors.should.equal(1)
      finalInfo.recordHandlings.unhandled.should.equal(0)
      finalInfo.recordHandlings.handled.should.equal(this.streamRecordHandlers.length - 1)
    })
    it('reports when all stream handlers throw', async function () {
      const error = new Error()
      this.streamRecordHandlerStubs.forEach(streamRecordHandler => {
        streamRecordHandler.rejects(error)
      })

      await this.shouldResolve1Record(this.createEvent1Record(false, true), false)

      this.infoSpy.calledOnce.should.be.true()
      this.warnSpy.calledOnce.should.be.true()
      this.errorSpy.getCalls().length.should.equal(this.streamRecordHandlerStubs.length)
      const finalInfo = this.warnSpy.getCalls()[0].args[4]
      console.log(finalInfo)
      finalInfo.recordHandlings.total.should.equal(this.streamRecordHandlers.length)
      finalInfo.recordHandlings.errors.should.equal(this.streamRecordHandlers.length)
      finalInfo.recordHandlings.unhandled.should.equal(0)
      finalInfo.recordHandlings.handled.should.equal(0)
    })
  })
})
