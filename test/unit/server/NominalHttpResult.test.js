/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const nominalHttpResult = require('../../../lib/server/NominalHttpResult')
const UUIDExample = require('../../../lib/ppwcode/UUID').example
const should = require('should')

describe(testName, function () {
  beforeEach(function () {
    nominalHttpResult.contract.verifyPostconditions = true
  })

  afterEach(function () {
    nominalHttpResult.contract.verifyPostconditions = false
  })

  it('should work', function () {
    const result = nominalHttpResult(UUIDExample, '')
    console.log(result)
    result.should.be.ok()
  })

  it('should work with an object result', function () {
    const result = nominalHttpResult(UUIDExample, { a: 1, b: 2, c: { d: { e: 3 } } })
    console.log(result)
    result.should.be.ok()
  })

  it('should work without content', function () {
    const result = nominalHttpResult(UUIDExample, undefined)
    console.log(result)
    result.should.be.ok()
  })

  it("doesn't stringify undefined", function () {
    should(JSON.stringify(undefined)).equal(undefined)
  })

  it('parses null', function () {
    should(JSON.parse('null')).equal(null)
  })
})
