/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const get = require('../../../../../lib/server/business/statistics/getStatistics')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = '7e416b6f-4256-4548-b2de-c29148106ae3'

    this.sot = '2020-06-28T06:47:24.911Z'
    this.sub = 'get-statistics-test'
    this.key = `/${this.mode}/statistics`

    this.dynamodbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.actualItem = {
      mode: this.mode,
      key: this.key,
      submitted: 'actual',
      submittedBy: this.sub,
      flowId: this.flowId,
      data: {
        createdAt: this.sot,
        createdBy: this.sub,
        structureVersion: 1,
        totalParticipants: 85,
        daysUntil: 10,
        timesRoundTheWorld: 8.1
      }
    }

    await this.dynamodbFixture.putItem(this.actualItem)

    const result = await get(this.sot, this.sub, this.mode, this.flowId)
    /* daysUntil is'nt checked because the activityPeriod set in config cannot be adjusted
    some tests like updateActivities depend on the specific activityPeriod set in the config
     */
    result.totalParticipants.should.equal(this.actualItem.data.totalParticipants)
    result.daysUntil.should.equal(this.actualItem.data.daysUntil)
    result.timesRoundTheWorld.should.equal(this.actualItem.data.timesRoundTheWorld)
  })

  it('should throw not found', async function () {
    const result = await get(this.sot, this.sub, this.mode, this.flowId).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
