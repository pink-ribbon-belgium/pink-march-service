/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../lib/server/business/statistics/writeStatistics')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = '7e416b6f-4256-4548-b2de-c29148106ae3'

    this.sot = '2020-10-19T22:34:24.911Z'
    this.sub = 'write-statistics-test'
    this.key = `/${this.mode}/statistics`

    this.getAggregates = sinon.stub()
    this.getTakenSlots = sinon.stub()

    this.dynamodbFixture.remember(this.key, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

    this.writeStatisticsProxy = proxyquire('../../../../../lib/server/business/statistics/writeStatistics', {
      '../aggregates/account/getAllAccountAggregates': this.getAggregates,
      '../slot/getAllTakenSlots': this.getTakenSlots
    })

    this.writeStatisticsProxy.contract.verifyPostconditions = true
    write.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    this.writeStatisticsProxy.contract.verifyPostconditions = false
    write.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.getTakenSlots.resolves([
      {
        name: 'a-slot'
      },
      {
        name: 'a-slot'
      },
      {
        name: 'a-slot'
      },
      {
        name: 'a-slot'
      },
      {
        name: 'a-slot'
      }
    ])
    this.getAggregates.resolves([
      {
        totalDistance: 750000
      },
      {
        totalDistance: 550000
      },
      {
        totalDistance: 350000
      },
      {
        totalDistance: 200000
      },
      {
        totalDistance: 500000
      }
    ])

    const expectedData = {
      totalParticipants: 5,
      daysUntil: 84,
      timesRoundTheWorld: 0.1,
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 1
    }

    await this.writeStatisticsProxy(this.sot, this.sub, this.mode, this.flowId)

    const storedItems = await this.dynamodbFixture.hasArrived()
    storedItems.length.should.equal(2)
    storedItems.every(item => item.key.should.equal(this.key))
    storedItems.every(item => item.data.totalParticipants.should.equal(expectedData.totalParticipants))
    storedItems.every(item => item.data.timesRoundTheWorld.should.equal(expectedData.timesRoundTheWorld))
    /* daysUntil is'nt checked because the activityPeriod set in config cannot be adjusted
    some tests like updateActivities depend on the specific activityPeriod set in the config
     */
  })
})
