/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/aggregates/team/get')
const generateGetTest = require('../../_generateGetTest')
const { v4: uuidv4 } = require('uuid')
const GroupType = require('../../../../../../lib/api/group/GroupType')

const teamId = '648c69e2-5bca-457f-bc32-8968c9247d4b'
const companyId = uuidv4()
describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      teamAggregateId: teamId,
      totalSteps: 2500,
      totalDistance: 3000,
      averageSteps: 2754,
      averageDistance: 3485,
      totalTeams: 10,
      companyId: companyId,
      groupType: GroupType.companyType,
      name: 'some-kind-of-team-name',
      extendedName: 'some-kind-of-team-name (Peopleware)',
      rank: 9,
      hasLogo: false
    }),
    mode => `/${mode}/teamAggregate/${teamId}`,
    { teamAggregateId: 'teamAggregateId' },
    () => ({ teamAggregateId: teamId }),
    () => ({ teamAggregateId: '88ba99ac-0f74-40d4-bdff-a4edd345289d' }),
    () => ({ teamAggregateId: 5442 }),
    () => ({
      team: `/I/team/${teamId}`
    }),
    true,
    undefined,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      teamAggregateId: teamId,
      totalSteps: 2500,
      totalDistance: 3000,
      averageSteps: 2754,
      averageDistance: 3485,
      totalTeams: 10,
      companyId: companyId,
      groupType: GroupType.companyType,
      name: 'some-kind-of-team-name',
      extendedName: 'some-kind-of-team-name (Peopleware)',
      rank: 9,
      hasLogo: false
    })
  )
})
