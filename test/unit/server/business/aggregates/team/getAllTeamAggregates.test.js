/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const config = require('config')
const getAll = require('../../../../../../lib/server/business/aggregates/team/getAllTeamAggregates')
const GroupType = require('../../../../../../lib/api/group/GroupType')
const write = require('../../../../../../lib/server/business/aggregates/team/write')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = uuidv4()
    this.teamAggregateId1 = uuidv4()
    this.teamAggregateId2 = uuidv4()
    this.teamAggregateId3 = uuidv4()

    this.sot = '2020-05-18T06:47:24.911Z'
    this.sub = 'zeer-rustige-eric'

    this.dynamoDbKey1 = `/${this.mode}/teamAggregate/${this.teamAggregateId1}`
    this.dynamoDbKey2 = `/${this.mode}/teamAggregate/${this.teamAggregateId2}`
    this.dynamoDbKey3 = `/${this.mode}/teamAggregate/${this.teamAggregateId3}`

    console.log(`Key1 => ${this.dynamoDbKey1}`)
    console.log(`Key2 => ${this.dynamoDbKey2}`)
    console.log(`Key3 => ${this.dynamoDbKey3}`)

    getAll.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getAll.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.dynamodbFixture.remember(this.dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, this.sot, config.dynamodb.test.tableName)

    const itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      teamAggregateId: this.teamAggregateId1,
      totalSteps: 4510,
      totalDistance: 4265,
      averageSteps: 4510,
      averageDistance: 4256,
      totalTeams: 15,
      companyId: '80f41029-0576-4479-a6fb-b46631f6a338',
      groupType: GroupType.companyType,
      name: 'Subgroup',
      extendedName: 'Company(Subgroup)',
      rank: 3,
      hasLogo: false
    }
    const itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      teamAggregateId: this.teamAggregateId2,
      totalSteps: 5360,
      totalDistance: 4260,
      averageSteps: 5360,
      averageDistance: 4260,
      totalTeams: 15,
      groupType: GroupType.teamType,
      name: 'team',
      extendedName: 'Team(team)',
      rank: 2,
      hasLogo: false
    }
    const itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      teamAggregateId: this.teamAggregateId3,
      totalSteps: 6523,
      totalDistance: 5604,
      averageSteps: 6523,
      averageDistance: 5604,
      totalTeams: 15,
      groupType: GroupType.teamType,
      name: 'team',
      extendedName: 'Team(team)',
      rank: 1,
      hasLogo: false
    }

    await Promise.all([
      write(this.sot, this.sub, this.mode, this.flowId, itemData1),
      write(this.sot, this.sub, this.mode, this.flowId, itemData2),
      write(this.sot, this.sub, this.mode, this.flowId, itemData3)
    ])

    const result = await getAll(this.sot, this.sub, this.mode, this.flowId)
    result.length.should.equal(3)
  })

  it('should throw not found', async function () {
    const result = await getAll(this.sot, this.sub, this.mode, this.flowId).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
