/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../../lib/server/business/aggregates/team/write')

const flowId = '8ae92bba-d904-49fc-9aba-7a22de215073'
const sot = '2020-05-29T04:51:52.191Z'
const sub = 'sub'

const teamAggregateId = 'e039ce8f-11fb-4080-9cd2-699cd28fb5b0'
const totalSteps = 1200
const totalDistance = 1000
const averageSteps = 1000
const averageDistance = 846
const totalTeams = 50
const companyId = '09a57709-79b3-4555-b562-02e5071c9270'
const name = 'John Doe'
const rank = 5
const groupType = 'Company'
const extendedName = 'Company(John Doe)'
const hasLogo = false

describe(testName, function () {
  beforeEach(async function f () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.dynamoDbKey = `/${this.mode}/teamAggregate/${teamAggregateId}`

    this.dynamodbFixture.remember(this.dynamoDbKey, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey, 'actual', config.dynamodb.test.tableName)

    this.itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 8,
      teamAggregateId,
      totalSteps,
      totalDistance,
      averageSteps,
      averageDistance,
      totalTeams,
      companyId,
      groupType,
      name,
      extendedName,
      rank,
      hasLogo
    }

    this.expectedData = {
      ...this.itemData
    }

    write.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    write.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    await write(sot, sub, this.mode, flowId, this.itemData)
    const storedItems = await this.dynamodbFixture.hasArrived()
    storedItems.length.should.equal(2)
    storedItems.every(item => item.data.should.be.deepEqual(this.expectedData))
  })
})
