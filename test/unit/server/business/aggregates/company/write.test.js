/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../../lib/server/business/aggregates/company/write')

const flowId = 'ec6e0843-a482-4e1b-896f-c82a10d380a8'
const sot = '2020-04-29T14:31:52.191Z'
const sub = 'id-of-the-creator'
const companyId = '8f03fe49-9109-4a15-9dc8-a1f5ca90aaad'
const totalSteps = 1000
const totalDistance = 1200

const name = 'Peopleware'
const extendedName = 'Peopleware (Peopleware & Partners)'
const rank = 3
const totalCompanies = 25
const unit = 'PPW_PARTNERS'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.dynamoDbKey = `/${this.mode}/companyAggregate/${companyId}`

    this.dynamodbFixture.remember(this.dynamoDbKey, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey, 'actual', config.dynamodb.test.tableName)

    this.itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 80,
      companyId: companyId,
      totalSteps: totalSteps,
      totalDistance: totalDistance,
      averageSteps: totalSteps,
      averageDistance: totalDistance,
      name: name,
      extendedName: extendedName,
      rank: rank,
      hasLogo: true,
      totalCompanies: totalCompanies,
      unit: unit
    }

    this.expectedData = {
      ...this.itemData
    }

    write.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    write.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })
  it('works', async function () {
    await write(sot, sub, this.mode, flowId, this.itemData)
    const storedItems = await this.dynamodbFixture.hasArrived()
    storedItems.length.should.equal(2)
    storedItems.some(item => item.submitted === sot).should.be.true()
    storedItems.some(item => item.submitted === 'actual').should.be.true()
    storedItems.forEach(accountAggregateConnection => {
      if (accountAggregateConnection.submitted === 'actual') {
        accountAggregateConnection.should.have.ownProperty('partition_key_1')
        accountAggregateConnection.partition_key_1.should.equal(`/${this.mode}/companyAggregate`)
        accountAggregateConnection.should.have.ownProperty('sort_key_1')
        accountAggregateConnection.sort_key_1.should.equal(totalSteps)

        accountAggregateConnection.should.have.ownProperty('partition_key_2')
        accountAggregateConnection.partition_key_2.should.equal(`/${this.mode}/companyAggregate/unit/${unit}`)
        accountAggregateConnection.should.have.ownProperty('sort_key_2')
        accountAggregateConnection.sort_key_2.should.equal(totalSteps)

        accountAggregateConnection.should.not.have.ownProperty('partition_key_3')
        accountAggregateConnection.should.not.have.ownProperty('sort_key_3')
      } else {
        accountAggregateConnection.should.not.have.ownProperty('partition_key_1')
        accountAggregateConnection.should.not.have.ownProperty('sort_key_1')
        accountAggregateConnection.should.not.have.ownProperty('partition_key_2')
        accountAggregateConnection.should.not.have.ownProperty('sort_key_2')
        accountAggregateConnection.should.not.have.ownProperty('partition_key_3')
        accountAggregateConnection.should.not.have.ownProperty('sort_key_3')
      }
      accountAggregateConnection.data.should.be.deepEqual(this.expectedData)
    })
  })
})
