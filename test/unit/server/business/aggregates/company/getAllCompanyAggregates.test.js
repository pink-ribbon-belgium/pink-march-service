/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const config = require('config')
const getAll = require('../../../../../../lib/server/business/aggregates/company/getAllCompanyAggregates')
const write = require('../../../../../../lib/server/business/aggregates/company/write')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = 'fe4addc5-880c-4905-b4a1-fcc67703f73a'
    this.companyId1 = '93a2bd29-3c46-4fb1-9e2d-99261483dc2b'
    this.companyId2 = 'cde471ba-22a7-47fd-b378-8e86c76807ee'
    this.companyId3 = 'ed0d23dd-a9de-42af-9d6c-5965c372e244'

    this.sot = '2020-05-18T06:47:24.911Z'
    this.sub = 'zeer-rustige-eric'

    this.dynamoDbKey1 = `/${this.mode}/companyAggregate/${this.companyId1}`
    this.dynamoDbKey2 = `/${this.mode}/companyAggregate/${this.companyId2}`
    this.dynamoDbKey3 = `/${this.mode}/companyAggregate/${this.companyId3}`

    console.log(`Key1 => ${this.dynamoDbKey1}`)
    console.log(`Key2 => ${this.dynamoDbKey2}`)
    console.log(`Key3 => ${this.dynamoDbKey3}`)

    getAll.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getAll.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.dynamodbFixture.remember(this.dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, this.sot, config.dynamodb.test.tableName)

    const itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      companyId: this.companyId1,
      totalSteps: 4510,
      totalDistance: 4265,
      averageSteps: 4510,
      averageDistance: 4265,
      totalCompanies: 15,
      name: 'Subgroup',
      rank: 3,
      hasLogo: false
    }
    const itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      companyId: this.companyId2,
      totalSteps: 5360,
      totalDistance: 4260,
      averageSteps: 5360,
      averageDistance: 4260,
      totalCompanies: 15,
      name: 'company-2',
      rank: 2,
      hasLogo: false
    }
    const itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      companyId: this.companyId3,
      totalSteps: 6523,
      totalDistance: 5604,
      averageSteps: 6523,
      averageDistance: 5604,
      totalCompanies: 15,
      name: 'company-3',
      unit: 'TEST',
      extendedName: 'company3 (TEST)',
      rank: 1,
      hasLogo: false
    }

    await Promise.all([
      write(this.sot, this.sub, this.mode, this.flowId, itemData1),
      write(this.sot, this.sub, this.mode, this.flowId, itemData2),
      write(this.sot, this.sub, this.mode, this.flowId, itemData3)
    ])

    const result = await getAll(this.sot, this.sub, this.mode, this.flowId)
    result.length.should.equal(3)
  })

  it('should throw not found', async function () {
    const result = await getAll(this.sot, this.sub, this.mode, this.flowId).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
