/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/aggregates/company/get')
const generateGetTest = require('../../_generateGetTest')

const companyId = '24376254-c793-45ef-bee0-f18ca5a3b039'
describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      companyId,
      totalSteps: 3000,
      totalDistance: 2500,
      averageSteps: 2500,
      averageDistance: 2500,
      totalCompanies: 10,
      unit: 'Unit',
      name: 'some-kind-of-monster',
      extendedName: 'some-kind-of-monster (Unit)',
      rank: 9,
      hasLogo: false
    }),
    mode => `/${mode}/companyAggregate/${companyId}`,
    { companyId: 'companyId' },
    () => ({ companyId: companyId }),
    () => ({ companyId: '88ba99ac-0f74-40d4-bdff-a4edd345289d' }),
    () => ({ companyId: 5442 }),
    () => ({
      company: `/I/company/${companyId}`
    }),
    true,
    undefined,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      companyId,
      totalSteps: 3000,
      totalDistance: 2500,
      averageSteps: 2500,
      averageDistance: 2500,
      totalCompanies: 10,
      unit: 'Unit',
      name: 'some-kind-of-monster',
      extendedName: 'some-kind-of-monster (Unit)',
      rank: 9,
      hasLogo: false
    })
  )
})
