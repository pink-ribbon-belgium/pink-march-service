/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../../lib/server/business/aggregates/account/put')

const AccountAggregate = require('./../../../../../../lib/server/business/aggregates/account/AccountAggregate')

const flowId = '755ede2a-8e9e-4c15-b254-4cdafba28dff'
const sot = '2020-04-15T11:05:52.191Z'
const sub = 'id-of-the-creator'
const accountId = 'account-id-aggregates-put-test'

const totalSteps = 1000
const totalDistance = 1200
const level = 1
const stepsToNextLevel = 2000

const accountAggregateFlowId = '7614e831-9702-4cbc-bb48-54d02f77a19b'
const groupId = '09a57709-79b3-4555-b562-02e5071c9270'
const groupType = 'Company'
const subGroupId = '2afaaa05-3e00-4098-aa65-0b020a3bb019'
const name = 'John Doe'
const rank = 5
const totalParticipants = 100

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.initialAccountAggregateData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 80,
      accountId: accountId,
      totalSteps: totalSteps,
      totalDistance: totalDistance,
      level: level,
      stepsToNextLevel: stepsToNextLevel,
      groupId: groupId,
      groupType: groupType,
      subGroupId: subGroupId,
      name: name,
      rank: rank,
      totalParticipants,
      previousLevel: 0,
      newLevel: false,
      acknowledged: true
    }

    const accountAggregate = new AccountAggregate({
      mode: this.mode,
      dto: {
        ...this.initialAccountAggregateData
      },
      sot: this.initialAccountAggregateData.createdAt,
      sub: this.initialAccountAggregateData.createdBy
    })

    const actualItem = {
      ...accountAggregate.toItem(),
      flowId: accountAggregateFlowId,
      submitted: 'actual',
      sort_key_1: this.initialAccountAggregateData.totalSteps,
      sort_key_2: this.initialAccountAggregateData.totalSteps,
      sort_key_3: this.initialAccountAggregateData.totalSteps
    }

    this.accountAggregateKey = `/${this.mode}/accountAggregate/${accountId}`

    await this.dynamodbFixture.putItem(actualItem)

    // const retrieved = await this.dynamodbFixture.hasArrived()

    put.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    put.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    const updatedAccountAggregateData = {
      ...this.initialAccountAggregateData,
      previousLevel: this.initialAccountAggregateData.level,
      acknowledged: true,
      totalSteps: 10
    }

    const expectedAccountAggregateData = {
      ...this.initialAccountAggregateData,
      previousLevel: this.initialAccountAggregateData.level,
      acknowledged: true
    }

    await put(
      sot,
      sub,
      this.mode,
      flowId,
      {
        accountId: accountId
      },
      updatedAccountAggregateData
    )
    const storedItems = await this.dynamodbFixture.hasArrived()

    storedItems.length.should.equal(1)
    storedItems[0].submitted.should.be.equal('actual')
    storedItems[0].data.should.be.deepEqual(expectedAccountAggregateData)
    storedItems[0].data.should.not.be.deepEqual(updatedAccountAggregateData)
    storedItems[0].data.should.not.be.deepEqual(this.initialAccountAggregateData)
  })

  it('should throw a exception if aggregate not exists', async function () {
    await this.dynamodbFixture.clean()

    const updatedAccountAggregateData = {
      ...this.initialAccountAggregateData,
      previousLevel: this.initialAccountAggregateData.level,
      acknowledged: true,
      totalSteps: 10
    }

    const exc = await put(
      sot,
      sub,
      this.mode,
      flowId,
      {
        accountId: accountId
      },
      updatedAccountAggregateData
    ).should.be.rejected()

    console.log(exc)
    exc.exception.code.should.be.equal('ValidationException')
    exc.exception.message.should.be.equal('The document path provided in the update expression is invalid for update')
    exc.exception.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const exc = await put(sot, sub, this.mode, flowId, { accountId: 'wrong id' }, this.itemData).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
