/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/aggregates/account/get')
const generateGetTest = require('../../_generateGetTest')
const { v4: uuidv4 } = require('uuid')
const GroupType = require('../../../../../../lib/api/group/GroupType')

const accountId = 'and-account-id'
const groupId = uuidv4()
const subGroupId = uuidv4()
describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      accountId: accountId,
      totalSteps: 80361,
      totalDistance: 3000,
      groupId: groupId,
      groupType: GroupType.companyType,
      subGroupId: subGroupId,
      name: 'some-kind-of-a-name',
      rank: 10,
      totalParticipants: 100,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }),
    mode => `/${mode}/accountAggregate/${accountId}`,
    { accountId: 'accountId' },
    () => ({ accountId }),
    () => ({ accountId: '88ba99ac-0f74-40d4-bdff-a4edd345289d' }),
    () => ({ accountId: 5442 }),
    () => ({
      account: `/I/account/${accountId}`,
      accountProfile: `/I/account/${accountId}/publicProfile`
    }),
    true,
    undefined,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      accountId: accountId,
      totalSteps: 80361,
      totalDistance: 3000,
      level: 3,
      previousLevel: 1,
      stepsToNextLevel: 49640,
      groupId: groupId,
      groupType: GroupType.companyType,
      subGroupId: subGroupId,
      name: 'some-kind-of-a-name',
      rank: 10,
      totalParticipants: 100,
      newLevel: true,
      acknowledged: false
    })
  )
})
