/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const config = require('config')
const getAggregates = require('../../../../../../lib/server/business/aggregates/account/getAggregatesForSubgroup')
const GroupType = require('../../../../../../lib/api/group/GroupType')
const writeAccountAggregate = require('../../../../../../lib/server/business/aggregates/account/write')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.groupId = uuidv4()
    this.subgroupId = '6404ba2e-f907-4722-87eb-d2ea9d199209'
    this.otherSubgroupId = 'f1d954b8-4109-45cd-9399-d9ba2408f96d'
    this.accountId1 = 'amazing-accountId'
    this.accountId2 = 'test-accountId'
    this.accountId3 = 'amazing-test-accountId'
    this.flowId = uuidv4()

    this.sot = '2020-05-16T06:07:24.911Z'
    this.sub = 'rustige-eric'

    const dynamoDbKey1 = `/${this.mode}/accountAggregate/${this.accountId1}`
    const dynamoDbKey2 = `/${this.mode}/accountAggregate/${this.accountId2}`
    const dynamoDbKey3 = `/${this.mode}/accountAggregate/${this.accountId3}`

    console.log(`Key1 => ${dynamoDbKey1}`)
    console.log(`Key2 => ${dynamoDbKey2}`)
    console.log(`Key3 => ${dynamoDbKey3}`)

    this.dynamodbFixture.remember(dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, this.sot, config.dynamodb.test.tableName)

    const itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId1,
      totalSteps: 4500,
      totalDistance: 1596,
      groupId: this.groupId,
      groupType: GroupType.companyType,
      subGroupId: this.subgroupId,
      name: 'brie',
      rank: 2,
      totalParticipants: 7,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }
    const itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId2,
      totalSteps: 77534,
      totalDistance: 65843,
      groupId: this.groupId,
      groupType: GroupType.companyType,
      subGroupId: this.subgroupId,
      name: 'bril',
      rank: 4,
      totalParticipants: 7,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }
    const itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId3,
      totalSteps: 3650,
      totalDistance: 2560,
      groupId: this.groupId,
      groupType: GroupType.companyType,
      subGroupId: this.otherSubgroupId,
      name: 'met',
      rank: 1,
      totalParticipants: 7,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }

    await Promise.all([
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData1, accountId: this.accountId1 }),
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData2, accountId: this.accountId2 }),
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData3, accountId: this.accountId3 })
    ])

    getAggregates.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getAggregates.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    const result = await getAggregates(this.sot, this.sub, this.mode, this.flowId, { subgroupId: this.subgroupId })
    result.length.should.equal(2)
    result.every(item => item.subGroupId.should.equal(this.subgroupId))
  })

  it('should throw not found', async function () {
    const result = await getAggregates(this.sot, this.sub, this.mode, this.flowId, {
      subgroupId: 'a46e6733-b121-4e55-a173-44c26dc74758'
    }).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })

  it('should throw a bad request', async function () {
    const result = await getAggregates(this.sot, this.sub, this.mode, this.flowId, {
      subgroupId: 'wrong-id'
    }).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
