/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const config = require('config')
const getAll = require('../../../../../../lib/server/business/aggregates/account/getAllAccountAggregates')
const writeAccountAggregate = require('../../../../../../lib/server/business/aggregates/account/write')
const GroupType = require('../../../../../../lib/api/group/GroupType')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.groupId = '882c5700-b3cb-4183-bbb4-eea80485fc47'
    this.otherGroupId = 'a42cab2a-4595-4c1b-9119-928f1fec3a00'
    this.accountId1 = 'amazing-accountId-getAll'
    this.accountId2 = 'test-accountId-getAll'
    this.accountId3 = 'amazing-test-accountId-getAll'
    this.flowId = 'd0fae4dc-536b-4f9e-8a6b-10b63a722d9a'

    this.sot = '2020-06-23T16:27:24.911Z'
    this.sub = 'get-all-account-aggregates'

    this.dynamoDbKey1 = `/${this.mode}/accountAggregate/${this.accountId1}`
    this.dynamoDbKey2 = `/${this.mode}/accountAggregate/${this.accountId2}`
    this.dynamoDbKey3 = `/${this.mode}/accountAggregate/${this.accountId3}`

    console.log(`Key1 => ${this.dynamoDbKey1}`)
    console.log(`Key2 => ${this.dynamoDbKey2}`)
    console.log(`Key3 => ${this.dynamoDbKey3}`)

    getAll.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getAll.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.dynamodbFixture.remember(this.dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, this.sot, config.dynamodb.test.tableName)

    const itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId1,
      totalSteps: 8560,
      totalDistance: 7562,
      groupId: this.groupId,
      groupType: GroupType.teamType,
      name: 'brie',
      rank: 5,
      totalParticipants: 15,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }
    const itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId2,
      totalSteps: 7592,
      totalDistance: 6584,
      groupId: this.groupId,
      groupType: GroupType.teamType,
      name: 'brilleke',
      rank: 2,
      totalParticipants: 15,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }
    const itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 8,
      accountId: this.accountId3,
      totalSteps: 4850,
      totalDistance: 3500,
      groupId: this.otherGroupId,
      groupType: GroupType.companyType,
      name: 'met',
      rank: 1,
      totalParticipants: 7,
      previousLevel: 1,
      newLevel: false,
      acknowledged: true
    }

    await Promise.all([
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData1, accountId: this.accountId1 }),
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData2, accountId: this.accountId2 }),
      writeAccountAggregate(this.sot, this.sub, this.mode, this.flowId, { ...itemData3, accountId: this.accountId3 })
    ])

    const result = await getAll(this.sot, this.sub, this.mode, this.flowId)
    console.log(result)
    result.length.should.equal(3)
  })

  it('should throw not found', async function () {
    const result = await getAll(this.sot, this.sub, this.mode, this.flowId).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
