/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const Slot = require('../../../../../lib/server/business/slot/Slot')
const SlotDTO = require('../../../../../lib/api/slot/SlotDTO')
const Mode = require('../../../../../lib/ppwcode/Mode')

const cases = ['free', 'taken']

describe(testName, function () {
  beforeEach(function () {
    Slot.contract.verifyPostconditions = true
  })

  afterEach(function () {
    Slot.contract.verifyPostconditions = false
  })

  cases.forEach(c => {
    describe(c, function () {
      beforeEach(function () {
        this.kwargs = {
          mode: Mode.example,
          dto: {
            ...SlotDTO.exampleDynamodb
          }
        }
        if (c === 'free') {
          delete this.kwargs.dto.accountId
        }
      })

      it(`should work for a ${c} slot`, function () {
        const result = new Slot(this.kwargs)
        result.should.upholdInvariants()
      })

      it(`should return links for a ${c} slot`, function () {
        let links
        if (this.kwargs.dto.accountId === undefined) {
          links = {
            payment: `/I/payment/${this.kwargs.dto.paymentId}`,
            group: `/I/${this.kwargs.dto.groupType.toLowerCase()}/${this.kwargs.dto.groupId}`
          }
        } else {
          links = {
            account: `/I/account/${this.kwargs.dto.accountId}`,
            accountProfile: `/I/account/${this.kwargs.dto.accountId}/publicProfile`,
            payment: `/I/payment/${this.kwargs.dto.paymentId}`,
            group: `/I/${this.kwargs.dto.groupType.toLowerCase()}/${this.kwargs.dto.groupId}`
          }
        }
        const result = new Slot(this.kwargs)
        result.getLinks().should.deepEqual(links)
      })
    })
  })
})
