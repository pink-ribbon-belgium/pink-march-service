/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const get = require('../../../../../lib/server/business/slot/getSlotsByPayment')
const { values: groupTypes } = require('../../../../../lib/api/group/GroupType')

const sot = '2020-01-06T11:23:00.007Z'
const sub = 'id-sub'
// required in get, but unused
const getFlowId = 'dbc5df57-1398-4c00-968e-355dcac4c539'

describe(testName, function () {
  this.retries(2)

  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      describe('structureVersion 2', function () {
        beforeEach(async function () {
          this.createItem = (flowId, groupId, slotId, paymentId, accountId, subgroupId) => {
            const slot = {
              createdAt: sot,
              createdBy: sub,
              structureVersion: 2,
              id: slotId,
              groupType,
              groupId: groupId,
              paymentId: paymentId,
              accountId: accountId,
              subgroupId: subgroupId
            }

            return {
              mode: this.mode,
              key: `/${this.mode}/group/${groupId}/slot/${slotId}`,
              submitted: sot,
              submittedBy: sub,
              flowId,
              data: slot,
              partition_key_A: `/${this.mode}/payment/${slot.paymentId}/slots`,
              sort_key_A: 'actual',
              partition_key_B: slot.accountId
                ? `/${this.mode}/group/${slot.groupId}/members`
                : `/${this.mode}/group/${slot.groupId}/free`,
              sort_key_B: 'actual',
              partition_key_C: slot.accountId ? `/${this.mode}/account/${slot.accountId}/slot` : undefined,
              sort_key_C: 'actual'
            }
          }
        })

        it('should work (1 slot)', async function () {
          const itemFlowId = 'e0c42ffa-9e84-4b98-bd74-9049d6c8c106'
          const groupId = '8bacad93-11c5-41b4-9305-9f5443045330'
          const slotId = '1d846ff7-4043-42d7-928f-31adbcfad957'
          const paymentId = '1f869448-ba33-4c99-8834-ce2b06097104'
          const accountId = 'ebec5e3a-2e05-41c6-a324-5913c50d5b2a'
          const subgroupId = 'c07db465-1a8c-4426-b1ed-9ac2e7cb38a3'
          const item = this.createItem(itemFlowId, groupId, slotId, paymentId, accountId, subgroupId)

          await this.dynamodbFixture.putItem(item)

          const result = await get(sot, sub, this.mode, getFlowId, { paymentId })
          result.length.should.equal(1)
          result.every(item => item.paymentId === paymentId)
        })

        it('should work (multiple slots)', async function () {
          const itemFlowId1 = '78c74339-b9ac-4662-9685-481d71002e72'
          const itemFlowId2 = '9bccfe06-76cc-4806-a69d-d4bada05062e'
          const itemFlowId3 = 'dc3020d6-1d1d-4f10-95bc-a834f34d4a64'

          const groupId1 = '5bef7005-d62e-4710-8083-30e09ca69460'
          const groupId2 = 'a9435641-4077-4f5d-9f5d-092b43e8a19d'
          const groupId3 = 'f003e8d0-6f01-49ea-b7d9-d3ac333f46b2'

          const slotId1 = 'd757e75b-28d0-4b58-bdcd-7f9cda06b919'
          const slotId2 = '618e1090-e34d-400e-a6bd-1e06ad304031'
          const slotId3 = '7af3f5c5-e783-4d9b-b2bd-7a6711e34419'

          const paymentId1 = '7b9aa1f8-9641-4f4e-82a4-97a23ca07477'
          const paymentId2 = 'db4c2d6a-9ecb-4e33-92e5-dd65a5555c79'

          const accountId1 = 'f0676518-dfab-45fa-b654-2d91b3b6f504'
          const accountId2 = 'b4d351b9-28c7-4be6-a95a-0a1377cff708'
          const accountId3 = '7e114b25-b885-4c5f-b9bc-d9a56f039ee5'

          const item1 = this.createItem(itemFlowId1, groupId1, slotId1, paymentId1, accountId1)
          const item2 = this.createItem(itemFlowId2, groupId2, slotId2, paymentId1, accountId2)
          const item3 = this.createItem(itemFlowId3, groupId3, slotId3, paymentId2, accountId3)

          await Promise.all([
            this.dynamodbFixture.putItem(item1),
            this.dynamodbFixture.putItem(item2),
            this.dynamodbFixture.putItem(item3)
          ])

          const resultPromise = get(sot, sub, this.mode, getFlowId, { paymentId: paymentId1 })
          const result = await resultPromise

          result.length.should.equal(2)
          result.every(item => item.paymentId === paymentId1)
        })
      })
    })

    describe('structureVersion 1', function () {
      beforeEach(async function () {
        this.createItem = (flowId, groupId, slotId, paymentId, accountId) => {
          const slot = {
            createdAt: sot,
            createdBy: sub,
            structureVersion: 1,
            id: slotId,
            groupId: groupId,
            paymentId: paymentId,
            accountId: accountId
          }

          return {
            mode: this.mode,
            key: `/${this.mode}/group/${groupId}/slot/${slotId}`,
            submitted: sot,
            submittedBy: sub,
            flowId,
            data: slot,
            partition_key_A: `/${this.mode}/payment/${slot.paymentId}/slots`,
            sort_key_A: 'actual',
            partition_key_B: slot.accountId
              ? `/${this.mode}/group/${slot.groupId}/members`
              : `/${this.mode}/group/${slot.groupId}/free`,
            sort_key_B: 'actual',
            partition_key_C: slot.accountId ? `/${this.mode}/account/${slot.accountId}/slot` : undefined,
            sort_key_C: 'actual'
          }
        }

        this.storeGroup = async function (groupId, groupFlowId) {
          await this.dynamodbFixture.putItem({
            mode: this.mode,
            key: `/${this.mode}/${groupType.toLowerCase()}/${groupId}`,
            submitted: sot,
            submittedBy: sub,
            flowId: groupFlowId,
            data: {
              createdAt: sot,
              createdBy: sub,
              structureVersion: 1,
              id: groupId,
              code: 'ABCD-ABCD-ABCD-ABCD',
              groupType,
              // below only for company, but it doesn't hurt for team
              address: 'this is an address 12',
              zip: '2500',
              city: 'Lier',
              vat: 'BE0452123456',
              contactFirstName: 'Jos',
              contactLastName: 'Pros',
              contactEmail: 'email@exampleIn.com',
              contactTelephone: '+32498765432',
              unit: 'Test-Unit'
            }
          })
        }
      })

      it('should work (1 slot)', async function () {
        const itemFlowId = 'e0c42ffa-9e84-4b98-bd74-9049d6c8c106'
        const groupId = '8bacad93-11c5-41b4-9305-9f5443045330'
        const groupFlowId = '4f488ace-57af-4045-8a9a-8328e7ca9258'
        const slotId = '1d846ff7-4043-42d7-928f-31adbcfad957'
        const paymentId = '1f869448-ba33-4c99-8834-ce2b06097104'
        const accountId = 'ebec5e3a-2e05-41c6-a324-5913c50d5b2a'

        const item = this.createItem(itemFlowId, groupId, slotId, paymentId, accountId)

        await Promise.all([this.storeGroup(groupId, groupFlowId), this.dynamodbFixture.putItem(item)])

        const result = await get(sot, sub, this.mode, getFlowId, { paymentId })
        result.length.should.equal(1)
        result.every(item => item.paymentId === paymentId)
      })

      it('should work (multiple slots)', async function () {
        const itemFlowId1 = '78c74339-b9ac-4662-9685-481d71002e72'
        const itemFlowId2 = '9bccfe06-76cc-4806-a69d-d4bada05062e'
        const itemFlowId3 = 'dc3020d6-1d1d-4f10-95bc-a834f34d4a64'

        const groupId1 = '5bef7005-d62e-4710-8083-30e09ca69460'
        const groupId2 = 'a9435641-4077-4f5d-9f5d-092b43e8a19d'
        const groupId3 = 'f003e8d0-6f01-49ea-b7d9-d3ac333f46b2'

        const groupFlowId1 = 'c69981f2-9642-49e2-b254-8861d6c8a686'
        const groupFlowId2 = '0bb14b20-76a3-4f76-ab99-ca6a2bde8063'
        const groupFlowId3 = 'b2c5f91e-74b5-4316-bc55-a8d6b2d8b40a'

        const slotId1 = 'd757e75b-28d0-4b58-bdcd-7f9cda06b919'
        const slotId2 = '618e1090-e34d-400e-a6bd-1e06ad304031'
        const slotId3 = '7af3f5c5-e783-4d9b-b2bd-7a6711e34419'

        const paymentId1 = '7b9aa1f8-9641-4f4e-82a4-97a23ca07477'
        const paymentId2 = 'db4c2d6a-9ecb-4e33-92e5-dd65a5555c79'

        const accountId1 = 'f0676518-dfab-45fa-b654-2d91b3b6f504'
        const accountId2 = 'b4d351b9-28c7-4be6-a95a-0a1377cff708'
        const accountId3 = '7e114b25-b885-4c5f-b9bc-d9a56f039ee5'

        const item1 = this.createItem(itemFlowId1, groupId1, slotId1, paymentId1, accountId1)
        const item2 = this.createItem(itemFlowId2, groupId2, slotId2, paymentId1, accountId2)
        const item3 = this.createItem(itemFlowId3, groupId3, slotId3, paymentId2, accountId3)

        await Promise.all([
          this.storeGroup(groupId1, groupFlowId1),
          this.storeGroup(groupId2, groupFlowId2),
          this.storeGroup(groupId3, groupFlowId3),
          this.dynamodbFixture.putItem(item1),
          this.dynamodbFixture.putItem(item2),
          this.dynamodbFixture.putItem(item3)
        ])

        const resultPromise = get(sot, sub, this.mode, getFlowId, { paymentId: paymentId1 })
        const result = await resultPromise

        result.length.should.equal(2)
        result.every(item => item.paymentId === paymentId1)
      })
    })
  })

  it('should work (return empty list if nothing is found)', async function () {
    const paymentId = uuidv4()

    const result = await get(sot, sub, this.mode, getFlowId, { paymentId: paymentId })

    result.length.should.equal(0)
  })
  it('should throw a bad request', async function () {
    const exc = await get(sot, sub, this.mode, getFlowId, { paymentId: 'wrong id' }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
