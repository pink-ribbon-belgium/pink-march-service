/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const config = require('config')
const get = require('../../../../../lib/server/business/slot/getAllTakenSlots')
const write = require('../../../../../lib/server/business/slot/write')
const GroupType = require('../../../../../lib/api/group/GroupType')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = '0ed61b15-350c-4f8b-af1d-acf993c2868e'
    this.companyId1 = '10767522-42a8-49af-af59-d6f79e4dd85b'
    this.companyId2 = '3a8984e9-bfa7-499f-884d-c89fe2f75b7b'
    this.companyId3 = 'd43494a7-6866-4a20-9523-09474f975106'
    this.slotId1 = '3869fbde-18b8-4af4-b393-72c3c9e0c56c'
    this.slotId2 = '15badf1f-bc6b-42ef-ba15-6c6f7b34fefb'
    this.slotId3 = '5a12d646-e3ba-4053-bce8-f2184754c94b'

    this.sot = '2020-05-18T06:47:24.911Z'
    this.sub = 'get-all-slots-test'

    this.dynamoDbKey1 = `/${this.mode}/group/${this.companyId1}/slot/${this.slotId1}`
    this.dynamoDbKey2 = `/${this.mode}/group/${this.companyId2}/slot/${this.slotId2}`
    this.dynamoDbKey3 = `/${this.mode}/group/${this.companyId3}/slot/${this.slotId3}`

    console.log(`Key1 => ${this.dynamoDbKey1}`)
    console.log(`Key2 => ${this.dynamoDbKey2}`)
    console.log(`Key3 => ${this.dynamoDbKey3}`)

    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.dynamodbFixture.remember(this.dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey3, this.sot, config.dynamodb.test.tableName)

    const itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 2,
      id: this.slotId1,
      groupType: GroupType.teamType,
      groupId: this.companyId1,
      paymentId: '38d81e7f-1756-4d1d-bdfa-88f68133d22b',
      accountId: 'Taken-1'
    }
    const itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 2,
      id: this.slotId2,
      groupType: GroupType.companyType,
      groupId: this.companyId2,
      paymentId: 'ce79138b-7980-4932-a043-a39635519560',
      accountId: 'Taken-2'
    }
    const itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 2,
      id: this.slotId3,
      groupType: GroupType.teamType,
      groupId: this.companyId3,
      paymentId: 'ce79138b-7980-4932-a043-a39635519560'
    }

    await Promise.all([
      write(this.sot, this.sub, this.mode, this.flowId, itemData1),
      write(this.sot, this.sub, this.mode, this.flowId, itemData2),
      write(this.sot, this.sub, this.mode, this.flowId, itemData3)
    ])

    const result = await get(this.sot, this.sub, this.mode, this.flowId)
    result.length.should.equal(2)
  })

  it('should throw not found', async function () {
    const result = await get(this.sot, this.sub, this.mode, this.flowId).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
