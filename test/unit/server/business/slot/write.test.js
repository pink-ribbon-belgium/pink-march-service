/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../lib/server/business/slot/write')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const groupTypes = require('../../../../../lib/api/group/GroupType').values

const itemFlowId = '2988da18-f5e1-4cf4-8b60-6aca2baf2ad9'
const sot = '2020-03-26T15:04:35.047Z'
const sub = 'sub-slot-write-test'
const slotId = 'dcb177b4-f6f7-4d32-84e9-59120ec1ef3a'
const groupId = 'cd68949c-3e5b-4ddc-9ffa-feef96b6846c'
const paymentId = '58136b84-103c-41f3-bed1-798a2b5f4e3b'
const accountId = '4c155595-a7d6-458f-8412-acacd50da10e'
const subgroupId = '6d609939-4529-4b15-8842-5c6a11bef14c'

describe(testName, function () {
  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      this.timeout(5000)

      beforeEach(async function () {
        this.dynamodbFixture = new DynamodbFixture()
        this.mode = `automated-test-${uuidv4()}`
        this.key = `/${this.mode}/group/${groupId}/slot/${slotId}`

        this.dynamodbFixture.remember(this.key, sot, config.dynamodb.test.tableName)
        this.dynamodbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

        this.freeSlotArgs = {
          structureVersion: 2,
          id: slotId,
          groupType,
          groupId,
          paymentId,
          subgroupId,
          unknownProperty: {
            extraValue: 'extra value'
          }
        }

        write.contract.verifyPostconditions = true
      })

      afterEach(async function () {
        write.contract.verifyPostconditions = false
        await this.dynamodbFixture.clean()
      })

      it('should work, with the slot free', async function () {
        const expectedSubmitted = {
          mode: this.mode,
          key: this.key,
          submitted: sot,
          submittedBy: sub,
          flowId: itemFlowId,
          data: {
            createdAt: sot,
            createdBy: sub,
            structureVersion: 2,
            id: slotId,
            groupType,
            groupId,
            paymentId,
            subgroupId
          }
        }

        const expectedActual = {
          ...expectedSubmitted,
          submitted: 'actual',
          partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
          partition_key_B: `/${this.mode}/group/${groupId}/free`,
          partition_key_D: `/${this.mode}/slots`,
          partition_key_E: `/${this.mode}/slots/free`,
          sort_key_A: 'actual',
          sort_key_B: 'actual',
          sort_key_D: 'actual',
          sort_key_E: 'actual'
        }

        await write(sot, sub, this.mode, itemFlowId, this.freeSlotArgs)

        const storedItems = await this.dynamodbFixture.hasArrived()

        storedItems.length.should.equal(2)
        storedItems.some(slotItem => slotItem.submitted === sot).should.be.true()
        storedItems.some(slotItem => slotItem.submitted === 'actual').should.be.true()

        storedItems.forEach(slotItem => {
          if (slotItem.submitted === 'actual') {
            slotItem.should.deepEqual(expectedActual)
          } else {
            slotItem.should.deepEqual(expectedSubmitted)
          }
        })
      })

      it('should work, with the slot occupied', async function () {
        const expectedSubmitted = {
          mode: this.mode,
          key: this.key,
          submitted: sot,
          submittedBy: sub,
          flowId: itemFlowId,
          data: {
            createdAt: sot,
            createdBy: sub,
            structureVersion: 2,
            id: slotId,
            accountId,
            groupType,
            groupId,
            paymentId,
            subgroupId
          }
        }

        const expectedActual = {
          ...expectedSubmitted,
          submitted: 'actual',
          partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
          partition_key_B: `/${this.mode}/group/${groupId}/members`,
          partition_key_C: `/${this.mode}/account/${accountId}/slot`,
          partition_key_D: `/${this.mode}/slots`,
          partition_key_E: `/${this.mode}/slots/members`,
          sort_key_A: 'actual',
          sort_key_B: 'actual',
          sort_key_C: 'actual',
          sort_key_D: 'actual',
          sort_key_E: 'actual'
        }

        await write(sot, sub, this.mode, itemFlowId, { ...this.freeSlotArgs, accountId })

        const storedItems = await this.dynamodbFixture.hasArrived()

        storedItems.length.should.equal(2)
        storedItems.some(slotItem => slotItem.submitted === sot).should.be.true()
        storedItems.some(slotItem => slotItem.submitted === 'actual').should.be.true()

        storedItems.forEach(slotItem => {
          if (slotItem.submitted === 'actual') {
            slotItem.should.deepEqual(expectedActual)
          } else {
            slotItem.should.deepEqual(expectedSubmitted)
          }
        })
      })
    })
  })
})
