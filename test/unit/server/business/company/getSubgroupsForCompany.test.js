/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamoDbFixture = require('../../../../_DynamodbFixture')
const Subgroup = require('../../../../../lib/server/business/subgroup/SubGroup')
const uuidv4 = require('uuid').v4

const groupId = '2c823261-0363-4749-961f-08b5c5cca713'
const getSot = '2020-03-30T14:53:28.122Z'
const getSub = 'sub-group-slots-get-test-get'
const slotSot = '2020-03-30T14:52:10.373Z'
const slotSub = 'slotSub-get-subgroups'
const getFlowId = 'a2b03ba9-b8df-478d-bc31-3f4167709c2c'
const slotFlowId = '1c270cbf-d1ee-4128-afae-fb5672e9c7b4'
const getSubgroups = require('../../../../../lib/server/business/company/getSubgroupsForCompany')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamoDbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.accountId = 'account-id-for-test'

    getSubgroups.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getSubgroups.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  async function createSubgroups (mode, dynamodbFixture, subgroupData) {
    await Promise.all(
      subgroupData.map(i => {
        const subgroup = new Subgroup({
          mode: mode,
          dto: {
            ...i
          },
          sot: i.createdAt,
          sub: i.createdBy
        })

        const databaseItem = subgroup.toItem()
        databaseItem.flowId = slotFlowId
        databaseItem.submitted = 'actual'
        databaseItem.partition_key_A = `/${mode}/subgroup`
        databaseItem.partition_key_B = `/${mode}/subgroup/${subgroup.groupType.toLowerCase()}/${subgroup.groupId}`
        databaseItem.partition_key_C = `/${mode}/subgroup/${subgroup.groupType.toLowerCase()}`
        databaseItem.sort_key_A = subgroup.name
        databaseItem.sort_key_B = subgroup.name
        databaseItem.sort_key_C = subgroup.name

        return dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  it('should work', async function () {
    this.timeout(5000)
    const baseSubgroup = {
      createdAt: slotSot,
      createdBy: slotSub,
      structureVersion: 1,
      groupId,
      groupType: 'Company',
      code: 'TEST-CODE-TEST-CODE',
      logo: 'VE9PTUFOWVNFQ1JFVFM=',
      linkId: uuidv4(),
      shortUrl: `https://rebrand.ly/${uuidv4()}`
    }

    const subgroupData = [
      { ...baseSubgroup, id: uuidv4(), accountId: 'account-id', name: 'subgroup-1' },
      { ...baseSubgroup, id: uuidv4(), accountId: 'amazing-account-id', name: 'subgroup-2' },
      { ...baseSubgroup, id: uuidv4(), groupId: uuidv4(), accountId: 'magnificent-account-id', name: 'subgroup-4' }
    ]

    await createSubgroups(this.mode, this.dynamodbFixture, subgroupData)

    const arrivedSlots = await this.dynamodbFixture.hasArrived()
    arrivedSlots.length.should.be.equal(3)

    const results = await getSubgroups(getSot, getSub, this.mode, getFlowId, {
      companyId: groupId
    })
    results.length.should.equal(2)
    results.every(item => item.groupId === groupId)
  })

  it('should work when company has no subgroups', async function () {
    const results = await getSubgroups(getSot, getSub, this.mode, getFlowId, {
      companyId: uuidv4()
    })
    results.length.should.equal(0)
  })
})
