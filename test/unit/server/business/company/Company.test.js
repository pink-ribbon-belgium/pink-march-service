/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const Company = require('../../../../../lib/server/business/company/Company')
const Group = require('../../../../../lib/server/business/group/Group')
const Mode = require('../../../../../lib/ppwcode/Mode')

describe(testName, function () {
  beforeEach(function () {
    Company.contract.verifyPostconditions = true
  })

  afterEach(function () {
    Company.contract.verifyPostconditions = false
  })

  it('works', function () {
    const result = new Company(Company.contract.KwargsExample)
    console.log(result.getJoinLink())
    result.should.upholdInvariants()
  })

  it('should getJoinLink', function () {
    const linkKwargs = {
      mode: Mode.exampleJune2020,
      groupId: Company.contract.KwargsExample.dto.id,
      groupType: Company.contract.KwargsExample.dto.groupType
    }
    const linkUrl = Group.getJoinLink(linkKwargs)
    const expectedResult = `https://june2020.pink-march.pink-ribbon-belgium.org/index.html#/join/${Company.contract.KwargsExample.dto.groupType.toLowerCase()}/${
      Company.contract.KwargsExample.dto.id
    }`

    linkUrl.should.equal(expectedResult)
  })

  it('should set demo joinLink', function () {
    const linkKwargs = {
      mode: Mode.exampleDemo,
      groupId: Company.contract.KwargsExample.dto.id,
      groupType: Company.contract.KwargsExample.dto.groupType
    }
    const linkUrl = Group.getJoinLink(linkKwargs)
    const expectedResult = `https://demo.pink-march.pink-ribbon-belgium.org/index.html#/join/${Company.contract.KwargsExample.dto.groupType.toLowerCase()}/${
      Company.contract.KwargsExample.dto.id
    }`

    linkUrl.should.equal(expectedResult)
  })

  it('should set june2020 joinLink', function () {
    const linkKwargs = {
      mode: Mode.exampleJune2020,
      groupId: Company.contract.KwargsExample.dto.id,
      groupType: Company.contract.KwargsExample.dto.groupType
    }
    const linkUrl = Group.getJoinLink(linkKwargs)
    const expectedResult = `https://june2020.pink-march.pink-ribbon-belgium.org/index.html#/join/${Company.contract.KwargsExample.dto.groupType.toLowerCase()}/${
      Company.contract.KwargsExample.dto.id
    }`

    linkUrl.should.equal(expectedResult)
  })
})
