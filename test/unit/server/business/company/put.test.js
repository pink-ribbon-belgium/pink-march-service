/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../lib/server/business/company/put')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.testUuid = uuidv4()
    this.flowId = '3addd738-118f-462c-a9d0-06fef2191272'
    this.mode = `automated-test-${this.testUuid}`
    this.createdAt = new Date().toISOString()
    this.createdBy = 'id-of-the-company-creator'
    this.companyId = '5cf0c3b6-9284-460f-a46f-7c92a8e1366b'

    this.dynamoDbKey = `/${this.mode}/company/${this.companyId}`

    this.itemData = {
      createdAt: this.createdAt,
      createdBy: this.createdBy,
      structureVersion: 81,
      id: this.companyId,
      code: 'XXXX-CCCC-BBBB-AAAA',
      address: 'this address 17',
      zip: '2500',
      city: 'Lier',
      vat: 'BE1234567890',
      contactFirstName: 'Eric',
      contactLastName: 'Cross',
      contactEmail: 'email@example.com',
      contactTelephone: '+32498765432',
      groupType: 'Company',
      linkId: null,
      shortUrl: 'https://rebrand.ly/mtt6q8r'
    }

    this.storedTestItemKey = { key: this.dynamoDbKey, submitted: this.createdAt }

    put.contract.verifyPostconditions = true

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key',
          '#submitted': 'submitted'
        },
        ExpressionAttributeValues: {
          ':key': this.dynamoDbKey,
          ':submitted': this.createdAt
        },
        KeyConditionExpression: '#key = :key and #submitted = :submitted',
        Limit: 1,
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    if (this.storedTestItemKey) {
      await this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: {
            key: this.storedTestItemKey.key,
            submitted: this.storedTestItemKey.submitted
          }
        })
        .promise()
      console.log(`deleted item ${util.inspect(this.storedTestItemKey)}`)
      delete this.storedTestItemKey
    }
  })

  it('works', async function () {
    await put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        companyId: this.companyId
      },
      this.itemData
    )
    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(1)
    retrieved.Items[0].key.should.equal(this.dynamoDbKey)
  })

  it('should throw a badRequest', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest4-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()
    const companyId = uuidv4()

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 81,
      id: 'testcompany',
      code: 'XXXX-CCCC-BBBB-AAAA',
      address: 'this address 17',
      zip: '2500',
      city: 'Lier',
      vat: 'BE1234567890',
      contactFirstName: 'Eric',
      contactLastName: 'Cross',
      contactEmail: 'emailexample.com',
      contactTelephone: '+32498765432',
      groupType: 'Company'
    }

    const exception = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        companyId: companyId
      },
      itemData
    ).should.be.rejected()
    console.log(exception)
    exception.isBoom.should.be.true()
    exception.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest5-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 64564,
      id: 'testcompany',
      code: 'XXXX-CCCC-BBBB-AAAA',
      address: 'this address 17',
      zip: '2500',
      city: 'Lier',
      vat: 'BE1234567890',
      contactFirstName: 'Eric',
      contactLastName: 'Cross',
      contactEmail: 'emailexample.com',
      contactTelephone: '+32498765432',
      groupType: 'Company'
    }

    const exc = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        companyId: 45678
      },
      itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
