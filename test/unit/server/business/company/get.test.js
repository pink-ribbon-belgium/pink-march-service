/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/company/get')
const generateGetTest = require('../_generateGetTest')
const { v4: uuidv4 } = require('uuid')

const companyId = 'b8b767f5-d49d-45dd-a3a1-74bf0a9ac6fd'
const linkId = uuidv4()

describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 89,
      id: companyId,
      code: 'AAAA-BBBB-CCCC-DDDD',
      address: 'this is an address 12',
      zip: '2500',
      city: 'Lier',
      vat: 'BE0123456789',
      reference: 'REF2021-1245652',
      contactFirstName: 'Jos',
      contactLastName: 'Cross',
      contactEmail: 'email@example.com',
      contactTelephone: '+32498765432',
      groupType: 'Company',
      unit: 'test-unit',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r'
    }),
    mode => `/${mode}/company/${companyId}`,
    { companyId: 'id' },
    () => ({
      companyId
    }),
    () => ({
      companyId: '8857b5d0-6cc4-4d93-ab22-a8b121c8e5aa'
    }),
    () => ({
      companyId: 4524
    }),
    () => ({
      administrators: `/I/company/${companyId}/administrators`,
      slots: `/I/company/${companyId}/slots`,
      payments: `/I/company/${companyId}/payments`,
      publicProfile: `/I/company/${companyId}/publicProfile`
    }),
    false,
    false,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 89,
      id: companyId,
      code: 'AAAA-BBBB-CCCC-DDDD',
      address: 'this is an address 12',
      zip: '2500',
      city: 'Lier',
      vat: 'BE0123456789',
      reference: 'REF2021-1245652',
      contactFirstName: 'Jos',
      contactLastName: 'Cross',
      contactEmail: 'email@example.com',
      contactTelephone: '+32498765432',
      groupType: 'Company',
      unit: 'test-unit',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r',
      joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/company/${companyId}`
    })
  )
})
