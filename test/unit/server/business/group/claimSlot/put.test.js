/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const uuidv4 = require('uuid').v4
const writeSlot = require('../../../../../../lib/server/business/slot/write')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const boom = require('@hapi/boom')
const sendGrid = require('@sendgrid/mail')
const { values: groupTypes } = require('../../../../../../lib/api/group/GroupType')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { contract: getEmailTemplateContract } = require('../../../../../../lib/server/business/payment/getEmailTemplate')
const { contract: getPreferencesContract } = require('../../../../../../lib/server/business/account/preferences/get')
const { contract: getAccountProfileContract } = require('../../../../../../lib/server/business/account/profile/get')
const { contract: getAccountContract } = require('../../../../../../lib/server/business/account/get')
const {
  contract: writeAccountAggregateContract
} = require('../../../../../../lib/server/business/aggregates/account/write')

const preferencesCreatedAt = '2018-03-30T09:43:50.834Z'

const slotId = '42a1bfd6-6d00-4b8a-b9df-371e5ef3c9a9'
const groupId = '60ed976c-c277-497d-aa9d-328ae4c6952c'
const paymentId = '03785602-fa5f-4657-b983-111364a3e811'
const itemSot = '2020-01-23T15:22:39.212Z'
const itemSub = 'item-sub-claimSlot-test'
const itemFlowId = '3efb2184-beca-4d08-a64f-897b48fefada'
const verifiedEmail = { email: 'someone@somewhere.com', isVerified: true }
const claimSot = '2020-03-30T09:43:50.834Z'
const claimSub = 'claim-sub-claimSlot-test'
const claimFlowId = 'f09e2f31-bbf5-4ea6-bf46-e12f5f1e3d43'
const claimAccountId = 'accountId-claimSlot-test'

const locales = [
  { locale: 'nl', expectedTemplateLanguage: 'NL' },
  { locale: 'nl-BE', expectedTemplateLanguage: 'NL' },
  { locale: 'fr', expectedTemplateLanguage: 'FR' },
  { locale: 'fr-BE', expectedTemplateLanguage: 'FR' },
  { locale: 'fr-FR', expectedTemplateLanguage: 'FR' },
  { locale: 'en-UK', expectedTemplateLanguage: 'NL' }
]

const authMethods = ['auth0', 'windowslive', 'facebook', 'google-oauth2', 'wrong']

describe(testName, function () {
  this.timeout(5000)

  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.mode = `automated-test-${uuidv4()}`

    this.getEmailTemplate = sinon.stub()
    const getEmailTemplateContractFunction = getEmailTemplateContract.implementation(this.getEmailTemplate)
    getEmailTemplateContractFunction.contract.verifyPostconditions = true

    this.getPreferences = sinon.stub()
    const getPreferencesContractFunction = getPreferencesContract.implementation(this.getPreferences)
    getPreferencesContractFunction.contract.verifyPostconditions = true

    this.getAccountProfile = sinon.stub()
    const getAccountProfileContractFunction = getAccountProfileContract.implementation(this.getAccountProfile)
    getAccountProfileContractFunction.contract.verifyPostconditions = true

    this.writeAccountAggregate = sinon.stub()
    const writeAccountAggregateContractFunction = writeAccountAggregateContract.implementation(
      this.writeAccountAggregate
    )
    getAccountProfileContractFunction.contract.verifyPostconditions = true

    this.getAccount = sinon.stub()
    const getAccountContractFunction = getAccountContract.implementation(this.getAccount)

    this.putClaimSlotProxy = proxyquire('../../../../../../lib/server/business/group/claimSlot/put', {
      '../../payment/getEmailTemplate': getEmailTemplateContractFunction,
      '../../account/preferences/get': getPreferencesContractFunction,
      '../../account/profile/get': getAccountProfileContractFunction,
      '../../aggregates/account/write': writeAccountAggregateContractFunction,
      '../../account/get': getAccountContractFunction
    })
    this.sandbox = sinon.createSandbox()
    this.sandbox.stub(sendGrid, 'send')

    this.putClaimSlotProxy.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.sandbox.restore()
    this.putClaimSlotProxy.contract.verifyPostconditions = false

    await this.dynamodbFixture.clean()
  })

  groupTypes.forEach(groupType => {
    const groupIdPropertyName = `${groupType.toLowerCase()}Id`

    describe(groupType, function () {
      locales.forEach(c => {
        it(`should work in locale ${c.locale}`, async function () {
          const body = {
            structureVersion: 2,
            id: slotId,
            groupType,
            groupId,
            paymentId
          }

          const dynamodbKey = `/${this.mode}/group/${groupId}/slot/${slotId}`

          this.dynamodbFixture.remember(dynamodbKey, itemSot, config.dynamodb.test.tableName)
          this.dynamodbFixture.remember(dynamodbKey, 'actual', config.dynamodb.test.tableName)

          const expectedSubmittedItem = {
            mode: this.mode,
            key: dynamodbKey,
            submitted: itemSot,
            submittedBy: itemSub,
            flowId: itemFlowId,
            data: { createdAt: itemSot, createdBy: itemSub, ...body }
          }

          const expectedActualItemBeforeClaim = {
            ...expectedSubmittedItem,
            partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
            partition_key_B: `/${this.mode}/group/${groupId}/free`,
            partition_key_D: `/${this.mode}/slots`,
            partition_key_E: `/${this.mode}/slots/free`,
            submitted: 'actual',
            sort_key_A: 'actual',
            sort_key_B: 'actual',
            sort_key_D: 'actual',
            sort_key_E: 'actual'
          }

          await writeSlot(itemSot, itemSub, this.mode, itemFlowId, body)

          const retrieved = await this.dynamodbFixture.hasArrived()
          retrieved.length.should.equal(2)
          retrieved.forEach(item => {
            if (item.submitted === itemSot) {
              item.should.deepEqual(expectedSubmittedItem)
            } else {
              item.should.deepEqual(expectedActualItemBeforeClaim)
            }
          })

          this.dynamodbFixture.remember(dynamodbKey, claimSot, config.dynamodb.test.tableName)

          const expectedExtraSubmittedItem = {
            mode: this.mode,
            key: dynamodbKey,
            submitted: claimSot,
            submittedBy: claimAccountId,
            flowId: claimFlowId,
            data: { createdAt: claimSot, createdBy: claimAccountId, ...body, accountId: claimAccountId }
          }
          const expectedActualItemAfterClaim = {
            ...expectedExtraSubmittedItem,
            partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
            partition_key_B: `/${this.mode}/group/${groupId}/members`,
            partition_key_C: `/${this.mode}/account/${claimAccountId}/slot`,
            partition_key_D: `/${this.mode}/slots`,
            partition_key_E: `/${this.mode}/slots/members`,
            submitted: 'actual',
            sort_key_A: 'actual',
            sort_key_B: 'actual',
            sort_key_C: 'actual',
            sort_key_D: 'actual',
            sort_key_E: 'actual'
          }

          this.getPreferences.resolves({
            createdAt: preferencesCreatedAt,
            createdBy: claimAccountId,
            structureVersion: 2,
            accountId: claimAccountId,
            language: c.locale,
            verifiedEmail,
            newsletter: true,
            knownFromType: 'WALKING'
          })

          const expectedAccountProfile = {
            firstName: 'Jan',
            lastName: 'Peeters',
            accountId: claimAccountId,
            gender: 'M',
            dateOfBirth: '1984-01-23T15:22:39.212Z',
            zip: '3600',
            createdAt: '2020-04-29T15:22:39.212Z',
            createdBy: 'a-unit-test',
            structureVersion: 1
          }

          this.getAccountProfile.resolves(expectedAccountProfile)

          const expectedAccount = {
            email: 'Jan_Peeters@example.com',
            accountId: claimAccountId,
            sub: 'auth0|601bf7978e96f60068464984'
          }

          this.getAccount.resolves(expectedAccount)

          this.getEmailTemplate.resolves({
            flow: 'individual',
            fromname: 'from name',
            language: c.expectedTemplateLanguage,
            subject: 'some subject',
            body: '<!DOCTYPE html>  <html>  <body>  <p><strong>This is a test</strong></p>  </body>  </html>'
          })

          await this.putClaimSlotProxy(claimSot, claimAccountId, this.mode, claimFlowId, {
            [groupIdPropertyName]: groupId,
            accountId: claimAccountId
          })

          const retrievedAfterClaim = await this.dynamodbFixture.hasArrived()
          retrievedAfterClaim.length.should.equal(3)
          retrievedAfterClaim.forEach(item => {
            if (item.submitted === itemSot) {
              item.should.deepEqual(expectedSubmittedItem)
            } else if (item.submitted === claimSot) {
              item.should.deepEqual(expectedExtraSubmittedItem)
            } else {
              item.should.deepEqual(expectedActualItemAfterClaim)
            }
          })

          const expectedAccountAggregate = {
            createdAt: claimSot,
            createdBy: claimAccountId,
            structureVersion: 1,
            accountId: claimAccountId,
            totalSteps: 0,
            totalDistance: 0,
            previousLevel: 1,
            groupId: groupId,
            groupType: groupType,
            name: `${expectedAccountProfile.firstName} ${expectedAccountProfile.lastName}`,
            rank: 9999999,
            totalParticipants: 9999999,
            acknowledged: false
          }

          this.getEmailTemplate.should.be.calledWith('confirmation_individual', c.expectedTemplateLanguage)
          this.getPreferences.should.be.called()
          this.getAccountProfile.should.be.called()
          this.getAccount.should.be.called()
          this.writeAccountAggregate.should.be.calledWithMatch(
            claimSot,
            claimAccountId,
            this.mode,
            claimFlowId,
            expectedAccountAggregate
          )
          sendGrid.send.should.be.called()

          const actual = retrievedAfterClaim.find(i => i.submitted === 'actual')
          console.log(actual)
          actual.data.accountId.should.equal(claimAccountId)
        })
      })
      authMethods.forEach(auth => {
        it(`should work with ${auth} authentication`, async function () {
          const body = {
            structureVersion: 2,
            id: slotId,
            groupType,
            groupId,
            paymentId
          }

          const dynamodbKey = `/${this.mode}/group/${groupId}/slot/${slotId}`

          this.dynamodbFixture.remember(dynamodbKey, itemSot, config.dynamodb.test.tableName)
          this.dynamodbFixture.remember(dynamodbKey, 'actual', config.dynamodb.test.tableName)

          const expectedSubmittedItem = {
            mode: this.mode,
            key: dynamodbKey,
            submitted: itemSot,
            submittedBy: itemSub,
            flowId: itemFlowId,
            data: { createdAt: itemSot, createdBy: itemSub, ...body }
          }

          const expectedActualItemBeforeClaim = {
            ...expectedSubmittedItem,
            partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
            partition_key_B: `/${this.mode}/group/${groupId}/free`,
            partition_key_D: `/${this.mode}/slots`,
            partition_key_E: `/${this.mode}/slots/free`,
            submitted: 'actual',
            sort_key_A: 'actual',
            sort_key_B: 'actual',
            sort_key_D: 'actual',
            sort_key_E: 'actual'
          }

          await writeSlot(itemSot, itemSub, this.mode, itemFlowId, body)

          const retrieved = await this.dynamodbFixture.hasArrived()
          retrieved.length.should.equal(2)
          retrieved.forEach(item => {
            if (item.submitted === itemSot) {
              item.should.deepEqual(expectedSubmittedItem)
            } else {
              item.should.deepEqual(expectedActualItemBeforeClaim)
            }
          })

          this.dynamodbFixture.remember(dynamodbKey, claimSot, config.dynamodb.test.tableName)

          const expectedExtraSubmittedItem = {
            mode: this.mode,
            key: dynamodbKey,
            submitted: claimSot,
            submittedBy: claimAccountId,
            flowId: claimFlowId,
            data: { createdAt: claimSot, createdBy: claimAccountId, ...body, accountId: claimAccountId }
          }
          const expectedActualItemAfterClaim = {
            ...expectedExtraSubmittedItem,
            partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
            partition_key_B: `/${this.mode}/group/${groupId}/members`,
            partition_key_C: `/${this.mode}/account/${claimAccountId}/slot`,
            partition_key_D: `/${this.mode}/slots`,
            partition_key_E: `/${this.mode}/slots/members`,
            submitted: 'actual',
            sort_key_A: 'actual',
            sort_key_B: 'actual',
            sort_key_C: 'actual',
            sort_key_D: 'actual',
            sort_key_E: 'actual'
          }

          this.getPreferences.resolves({
            createdAt: preferencesCreatedAt,
            createdBy: claimAccountId,
            structureVersion: 2,
            accountId: claimAccountId,
            language: locales[0].locale,
            verifiedEmail,
            newsletter: true,
            knownFromType: 'WALKING'
          })

          const expectedAccountProfile = {
            firstName: 'Jan',
            lastName: 'Peeters',
            accountId: claimAccountId,
            gender: 'M',
            dateOfBirth: '1984-01-23T15:22:39.212Z',
            zip: '3600',
            createdAt: '2020-04-29T15:22:39.212Z',
            createdBy: 'a-unit-test',
            structureVersion: 1
          }

          this.getAccountProfile.resolves(expectedAccountProfile)

          const expectedAccount = {
            email: 'Jan_Peeters@example.com',
            accountId: claimAccountId,
            sub: `${auth}|601bf7978e96f60068464984`
          }

          this.getAccount.resolves(expectedAccount)

          this.getEmailTemplate.resolves({
            flow: 'individual',
            fromname: 'from name',
            language: locales[0].expectedTemplateLanguage,
            subject: 'some subject',
            body: '<!DOCTYPE html>  <html>  <body>  <p><strong>This is a test</strong></p>  </body>  </html>'
          })

          await this.putClaimSlotProxy(claimSot, claimAccountId, this.mode, claimFlowId, {
            [groupIdPropertyName]: groupId,
            accountId: claimAccountId
          })

          const retrievedAfterClaim = await this.dynamodbFixture.hasArrived()
          retrievedAfterClaim.length.should.equal(3)
          retrievedAfterClaim.forEach(item => {
            if (item.submitted === itemSot) {
              item.should.deepEqual(expectedSubmittedItem)
            } else if (item.submitted === claimSot) {
              item.should.deepEqual(expectedExtraSubmittedItem)
            } else {
              item.should.deepEqual(expectedActualItemAfterClaim)
            }
          })

          const expectedAccountAggregate = {
            createdAt: claimSot,
            createdBy: claimAccountId,
            structureVersion: 1,
            accountId: claimAccountId,
            totalSteps: 0,
            totalDistance: 0,
            previousLevel: 1,
            groupId: groupId,
            groupType: groupType,
            name: `${expectedAccountProfile.firstName} ${expectedAccountProfile.lastName}`,
            rank: 9999999,
            totalParticipants: 9999999,
            acknowledged: false
          }

          this.getEmailTemplate.should.be.calledWith('confirmation_individual', locales[0].expectedTemplateLanguage)
          this.getPreferences.should.be.called()
          this.getAccountProfile.should.be.called()
          this.getAccount.should.be.called()
          this.writeAccountAggregate.should.be.calledWithMatch(
            claimSot,
            claimAccountId,
            this.mode,
            claimFlowId,
            expectedAccountAggregate
          )
          sendGrid.send.should.be.called()

          const actual = retrievedAfterClaim.find(i => i.submitted === 'actual')
          console.log(actual)
          actual.data.accountId.should.equal(claimAccountId)
        })
      })
      it('should throw 404 when there are account preferences', async function () {
        this.getPreferences.rejects(boom.notFound())

        this.getAccountProfile.resolves({
          firstName: 'Jan',
          lastName: 'Peeters',
          accountId: claimAccountId,
          gender: 'M',
          dateOfBirth: '1984-01-23T15:22:39.212Z',
          zip: '3600',
          createdAt: '2020-04-29T15:22:39.212Z',
          createdBy: 'a-unit-test',
          structureVersion: 1
        })

        await this.putClaimSlotProxy(claimSot, claimSub, this.mode, claimFlowId, {
          [groupIdPropertyName]: groupId,
          accountId: claimAccountId
        }).should.be.rejectedWith(boom.notFound())
      })
      it('should throw 404 when there is no account profile', async function () {
        this.getPreferences.resolves({
          createdAt: preferencesCreatedAt,
          createdBy: claimAccountId,
          structureVersion: 2,
          accountId: claimAccountId,
          language: 'nl',
          verifiedEmail
        })

        this.getAccountProfile.rejects(boom.notFound())

        await this.putClaimSlotProxy(claimSot, claimSub, this.mode, claimFlowId, {
          [groupIdPropertyName]: groupId,
          accountId: claimAccountId
        }).should.be.rejectedWith(boom.notFound())
      })
      it('should throw 404 when there are no slots', async function () {
        this.getPreferences.resolves({
          createdAt: preferencesCreatedAt,
          createdBy: claimAccountId,
          structureVersion: 2,
          accountId: claimAccountId,
          language: 'nl',
          verifiedEmail,
          newsletter: true,
          knownFromType: 'WALKING'
        })

        this.getAccountProfile.resolves({
          firstName: 'Jan',
          lastName: 'Peeters',
          accountId: claimAccountId,
          gender: 'M',
          dateOfBirth: '1984-01-23T15:22:39.212Z',
          zip: '3600',
          createdAt: '2020-04-29T15:22:39.212Z',
          createdBy: 'a-unit-test',
          structureVersion: 1
        })

        await this.putClaimSlotProxy(claimSot, claimSub, this.mode, claimFlowId, {
          [groupIdPropertyName]: groupId,
          accountId: claimAccountId
        }).should.be.rejectedWith(boom.notFound())
      })
      it('should throw a bad request', async function () {
        const exc = await this.putClaimSlotProxy('2020-01-15T15:22:39.212Z', claimSub, this.mode, claimFlowId, {
          [groupIdPropertyName]: 'wrong id',
          accountId: claimAccountId
        }).should.be.rejected()
        console.log(exc)
        exc.isBoom.should.be.true()
        exc.output.statusCode.should.equal(400)
      })
    })
  })
})
