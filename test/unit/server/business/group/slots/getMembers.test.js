/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const getMembers = require('../../../../../../lib/server/business/group/slots/getMembers')
const DynamoDbFixture = require('../../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const Boom = require('@hapi/boom')
const GroupType = require('../../../../../../lib/api/group/GroupType')

const Slot = require('../../../../../../lib/server/business/slot/Slot')
const AccountProfile = require('../../../../../../lib/server/business/account/profile/AccountProfile')

const groupId = '20f3731f-c059-4997-b627-5eeb30283f3b'
const otherGroupId = 'f9c2b27d-58ec-4fb0-be30-fe13de0c993e'
const paymentId = 'f9c2b27d-58ec-4fb0-be30-fe13de0c993e'
const otherPaymentId = 'f9c2b27d-58ec-4fb0-be30-fe13de0c993e'

const slotFlowId = '1d7d1aff-24bc-40fe-9170-ba12b697a1d2'
const accountProfileFlowId = 'd5e49283-f216-4bbe-8453-e3626ce3eea0'
const slotSot = '2020-06-30T14:52:10.373Z'
const slotSub = 'slotSub-group-getMembers'
const getSot = '2020-06-30T14:53:28.122Z'
const getSub = 'group-slots-get-test-get'
const getFlowId = 'c7ccfcdd-6fe6-4ab7-98ea-75e415f0bf7d'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamoDbFixture()
    this.mode = `automated-test-${uuidv4()}`
    getMembers.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getMembers.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  async function createSlots (mode, dynamodbFixture, slotData) {
    await Promise.all(
      slotData.map(data => {
        const slotId = data.slotId
        const sub = slotSub

        const slotData = {
          ...data,
          createdAt: slotSot,
          createdBy: sub,
          structureVersion: 2,
          id: slotId
        }

        const slot = new Slot({
          mode: mode,
          dto: {
            ...slotData
          },
          sot: slotSot,
          sub
        })

        const databaseItem = slot.toItem()
        databaseItem.flowId = slotFlowId
        databaseItem.submitted = 'actual'
        databaseItem.sort_key_A = 'actual'
        databaseItem.sort_key_B = 'actual'

        return dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  async function createAccountProfiles (mode, dynamodbFixture, accountProfileData) {
    await Promise.all(
      accountProfileData.map(i => {
        const accountId = i.accountId
        const firstName = i.firstName
        const lastName = i.lastName
        const sub = 'sub-of-account-profile'

        const itemData = {
          accountId: accountId,
          firstName: firstName,
          lastName: lastName,
          gender: 'M',
          dateOfBirth: '1987-01-23T15:22:39.212Z',
          zip: '4500',
          createdAt: '2020-04-22T18:21:28.122Z',
          createdBy: sub,
          structureVersion: 1
        }

        const accountProfile = new AccountProfile({
          mode: mode,
          dto: {
            ...itemData
          },
          sot: slotSot,
          sub
        })

        const databaseItem = accountProfile.toItem()
        databaseItem.flowId = accountProfileFlowId
        databaseItem.submitted = 'actual'
        databaseItem.sort_key_A = 'actual'

        dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  it('should work', async function () {
    this.timeout(5000)
    const slots = [
      { groupId, paymentId, slotId: uuidv4(), accountId: 'jan-account', groupType: GroupType.teamType },
      {
        groupId: otherGroupId,
        paymentId: otherPaymentId,
        slotId: uuidv4(),
        accountId: 'piet-account',
        groupType: GroupType.companyType
      },
      {
        groupId,
        paymentId,
        slotId: uuidv4(),
        accountId: 'jef-account',
        groupType: GroupType.companyType,
        subgroupId: uuidv4()
      },
      { groupId, paymentId, slotId: uuidv4(), accountId: 'jos-account', groupType: GroupType.teamType }
    ]

    const accountProfileData = [
      { accountId: 'jan-account', firstName: 'Jan', lastName: 'Janssens' },
      { accountId: 'piet-account', firstName: 'Piet', lastName: 'Peeters' },
      { accountId: 'jef-account', firstName: 'Jef', lastName: 'Jefferson' },
      { accountId: 'jos-account', firstName: 'Jos', lastName: 'Peetermans' }
    ]

    await Promise.all([
      createSlots(this.mode, this.dynamodbFixture, slots),
      createAccountProfiles(this.mode, this.dynamodbFixture, accountProfileData)
    ])

    const arrivedSlots = await this.dynamodbFixture.hasArrived()
    arrivedSlots.length.should.be.equal(8)

    const result = await getMembers(getSot, getSub, this.mode, getFlowId, {
      groupId
    })

    console.log(':::::::::', result)
    result.length.should.equal(3)
  })

  it('should throw not found', async function () {
    await getMembers(getSot, getSub, this.mode, getFlowId, {
      groupId: '6f997fe6-ff1e-4852-b318-13a17b40811f'
    }).should.be.rejectedWith(Boom.notFound())
  })

  it('should throw a bad request', async function () {
    const result = await getMembers(getSot, getSub, this.mode, getFlowId, {
      companyId: '6f997fe6-ff1e-4852-b318-13a17b40811f'
    }).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
