/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const getGroupSlots = require('../../../../../../lib/server/business/group/slots/get')
const DynamoDbFixture = require('../../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const boom = require('@hapi/boom')
const { values: groupTypes } = require('../../../../../../lib/api/group/GroupType')

const groupId = '2c823261-0363-4749-961f-08b5c5cca713'
const slotFlowId = '8e8e5e8d-feec-4019-9493-90e186a1ae90'
const slotSot = '2020-03-30T14:52:10.373Z'
const slotSub = 'sub-group-slots-get-test-slot'
const getSot = '2020-03-30T14:53:28.122Z'
const getSub = 'sub-group-slots-get-test-get'
const getFlowId = '99967ea7-cd32-4a5e-9bbb-09deceb358b0'

const paymentIds = ['9e3c87c1-3529-49b3-a6f3-591beb6f7219', 'e6014e41-a968-41d1-8fdc-5e3922734b4a']
const slotIds = [
  '2a1c641f-8207-4958-b33d-c19a79442972',
  '4f233c0a-e3bc-4aff-abc6-a353a96c78f2',
  '2a63a14c-aad0-4739-bfcd-9c315a3fdcc3',
  '180b0f0b-1983-44c2-8f6f-9b8439045184',
  '9766dfaa-a3f9-4c63-bb38-374a5e540663'
]
const nrOfSlots = slotIds.length
const accountIds = [
  'fd4ca430-08a4-4f84-add3-1eb572723b99',
  '94e395bc-e73a-4af3-b569-fb2930342ae4',
  '95bc0d73-249c-41b5-a22c-2389c4c0381c',
  '8d254078-aa7a-49bd-b9c5-4ce091c13bea',
  '037c0023-e222-45ec-884c-d8689751b58b'
]
const nrOfTakenSlots = [0, 3, slotIds.length]

const structureVersions = [1, 2]

describe(testName, function () {
  beforeEach(async function () {
    this.mode = `automated-test-${uuidv4()}`
    this.dynamodbFixture = new DynamoDbFixture()
    getGroupSlots.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
    getGroupSlots.contract.verifyPostconditions = false
  })

  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      structureVersions.forEach(structureVersion => {
        describe(`structureVersion ${structureVersion}`, function () {
          nrOfTakenSlots.forEach(nr => {
            it(`works with ${nr}/${slotIds.length} taken`, async function () {
              await Promise.all(
                new Array(nrOfSlots).fill(undefined).map((_ignore, index) => {
                  const slotId = slotIds[index]
                  const paymentId = paymentIds[index % paymentIds.length]
                  const accountId = index < nr ? accountIds[index] : undefined
                  const sub = `${slotSub}-${index}`

                  return this.dynamodbFixture.putItem({
                    mode: this.mode,
                    key: `/${this.mode}/group/${groupId}/slot/${slotId}`,
                    submitted: 'actual',
                    submittedBy: sub,
                    flowId: slotFlowId,
                    data: {
                      createdAt: slotSot,
                      createdBy: sub,
                      structureVersion,
                      id: slotId,
                      groupType: structureVersion < 2 ? undefined : groupType,
                      groupId,
                      paymentId,
                      accountId
                    },
                    partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
                    sort_key_A: 'actual',
                    partition_key_B: `/${this.mode}/group/${groupId}/${accountId ? 'members' : 'free'}`,
                    sort_key_B: 'actual',
                    partition_key_C: accountId && `/${this.mode}/account/${accountId}/slot`,
                    sort_key_C: accountId && 'actual'
                  })
                })
              )

              const result = await getGroupSlots(getSot, getSub, this.mode, getFlowId, {
                groupType: groupType.toString().toLowerCase(),
                groupId
              })
              const expectedResult = {
                totalSlots: slotIds.length,
                takenSlots: nr,
                availableSlots: slotIds.length - nr
              }
              console.log(result)
              result.should.deepEqual(expectedResult)
            })
          })
        })
      })

      it('should throw a 404', async function () {
        await getGroupSlots(getSot, getSub, this.mode, getFlowId, {
          groupId: '2be0c001-3964-4c7f-a3ab-30be3e960b80'
        }).should.be.rejectedWith(boom.notFound())
      })
    })
  })

  it('should throw a 400', async function () {
    const exc = await getGroupSlots(getSot, getSub, this.mode, getFlowId, {
      groupId: this.groupId
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
