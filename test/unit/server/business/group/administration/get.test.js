/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/group/administration/get')
const generateGetTest = require('../../_generateGetTest')
const GroupType = require('../../../../../../lib/api/group/GroupType')

const groupId = '8bacad93-11c5-41b4-9305-9f5443045330'
const accountId = 'an-account-id-for-slot'

describe(testName, function () {
  GroupType.values.forEach(groupType => {
    describe(groupType, function () {
      describe('structureVersion 2', function () {
        generateGetTest(
          get,
          (sot, sub) => ({
            createdAt: sot,
            createdBy: sub,
            structureVersion: 2,
            groupId,
            groupType,
            accountId,
            deleted: false
          }),
          mode => `/${mode}/groupAdministration/account/${accountId}/group/${groupId}`,
          { accountId: 'accountId', groupId: 'groupId' },
          () => ({
            accountId,
            groupType: groupType.toLowerCase(),
            groupId
          }),
          () => ({
            accountId,
            groupType: groupType === GroupType.companyType ? 'team' : 'company', // cover the special if in this get
            groupId
          }),
          () => ({
            accountId: 'amazing-accountId',
            groupType: groupType,
            groupId: 5546
          }),
          () => ({
            account: `/I/account/${accountId}`,
            group: `/I/${groupType.toLowerCase()}/${groupId}`
          }),
          true
        )
      })
      describe('structureVersion 1 (legacy)', function () {
        generateGetTest(
          get,
          (sot, sub) => ({
            createdAt: sot,
            createdBy: sub,
            structureVersion: 1,
            groupId,
            accountId,
            deleted: false
          }),
          mode => `/${mode}/groupAdministration/account/${accountId}/group/${groupId}`,
          { accountId: 'accountId', groupId: 'groupId' },
          () => ({
            accountId,
            groupType: groupType.toLowerCase(),
            groupId
          }),
          () => ({
            accountId,
            groupType: groupType.toLowerCase(),
            groupId: '7a94a14f-f9bf-4cba-b00c-98df9c020af4'
          }),
          () => ({
            accountId: 'amazing-accountId',
            groupType: groupType,
            groupId: 5546
          }),
          () => ({
            account: `/I/account/${accountId}`,
            group: `/I/${groupType.toLowerCase()}/${groupId}`
          }),
          true,
          (result, sot, sub) => {
            result.should.deepEqual({
              createdAt: sot,
              createdBy: sub,
              structureVersion: 2,
              groupType,
              groupId,
              accountId,
              deleted: false,
              links: {
                account: `/I/account/${accountId}`,
                group: `/I/${groupType.toLowerCase()}/${groupId}`
              }
            })
          }
        )
      })
    })
  })
})
