/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const get = require('../../../../../../lib/server/business/group/administration/getAdministratorsForGroup')
const Boom = require('@hapi/boom')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  function createItem (sot, sub, mode, flowId, pathParams, partialKey, groupType) {
    const idName = `${groupType.toLowerCase()}Id`
    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78675,
      groupId: pathParams[idName],
      accountId: pathParams.accountId,
      groupType,
      deleted: false
    }

    return {
      mode: mode,
      key: `/${mode}${partialKey}`,
      submitted: itemData.createdAt,
      submittedBy: itemData.createdBy,
      flowId: 'dbd17017-ac6e-497e-bdc1-876fec2375ab',
      data: { ...itemData },
      partition_key_A: `/${mode}/account/${pathParams.accountId}/administratorOf`,
      sort_key_A: `/${mode}/group/${pathParams[idName]}`,
      partition_key_B: `/${mode}/group/${pathParams[idName]}/administrators`,
      sort_key_B: `/${mode}/account/${pathParams.accountId}`
    }
  }

  function createTest (groupType) {
    it(`should work for ${groupType}`, async function () {
      this.timeout(3000)
      const sot = new Date().toISOString()
      const sub = 'id-subTest'
      const mode = `automated-test-${uuidv4()}`
      const flowId1 = uuidv4()
      const flowId2 = uuidv4()
      const idName = `${groupType.toLowerCase()}Id`
      const groupId = uuidv4()

      const pathParameters1 = {
        accountId: 'pretty-accountId',
        [idName]: groupId
      }
      const pathParameters2 = {
        accountId: 'amazing-accountId',
        [idName]: groupId
      }
      const key1 = `/groupAdministration/account/${pathParameters1.accountId}/group/${[idName]}`
      const key2 = `/groupAdministration/account/${pathParameters2.accountId}/group/${[idName]}`

      const item1 = createItem(sot, sub, mode, flowId1, pathParameters1, key1, groupType)
      const item2 = createItem(sot, sub, mode, flowId2, pathParameters2, key2, groupType)
      await this.dynamodbFixture.putItem(item1)
      await this.dynamodbFixture.putItem(item2)

      const resultPromise = get(sot, sub, mode, flowId1, { [idName]: groupId })
      const result = await resultPromise

      result.length.should.equal(2)
      result.every(item => item.groupId === groupId)
    })
  }

  createTest('Team')
  createTest('Company')

  it('reports not found when the company does not exist', async function () {
    const sot = new Date().toISOString()
    const sub = 'id-of-the-subTest2'
    const mode = `automated-test-${uuidv4()}`
    const startFlowId = uuidv4()
    const teamId = uuidv4()
    const pathParameters = {
      accountId: 'pretty-accountId',
      teamId: teamId
    }
    const key = `/groupAdministration/account/${pathParameters.accountId}/group/${teamId}`

    const item = createItem(sot, sub, mode, startFlowId, pathParameters, key, 'team')
    await this.dynamodbFixture.putItem(item)

    const exc = await get(sot, sub, mode, startFlowId, {
      teamId: uuidv4()
    }).should.be.rejectedWith(Boom.notFound())
    console.log(exc)
  })

  it('throws a bad request', async function () {
    const sot = new Date().toISOString()
    const sub = 'id-of-the-subTest3'
    const mode = `automated-test-${uuidv4()}`
    const startFlowId = uuidv4()
    const companyId = uuidv4()
    const pathParameters = {
      accountId: 'pretty-accountId',
      companyId: companyId
    }
    const key = `/groupAdministration/account/${pathParameters.accountId}/group/${companyId}`

    const item = createItem(sot, sub, mode, startFlowId, pathParameters, key, 'company')
    await this.dynamodbFixture.putItem(item)

    const exc = await get(sot, sub, mode, startFlowId, {
      companyId: 453
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
