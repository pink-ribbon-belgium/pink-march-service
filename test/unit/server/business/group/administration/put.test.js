/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../../lib/server/business/group/administration/put')
const DynamodbFixture = require('../../../../../_DynamodbFixture')

const sot = '2020-03-23T13:58:37.238Z'
const sub = 'sub-test-group-administration-put'
const accountId = 'accountId-test-group-administration-put'
const groupId = '33f177c3-acdd-4ec4-8725-b72fc8fdd7cc'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.flowId = uuidv4()
    this.mode = `automated-test-${this.flowId}`
    this.key = `/${this.mode}/groupAdministration/account/${accountId}/group/${groupId}`

    this.dto = {
      structureVersion: 2,
      deleted: false
    }

    this.dynamodbFixture.remember(this.key, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

    put.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    await this.dynamodbFixture.clean()
  })

  function createTest (groupType) {
    it(`should work for ${groupType}`, async function () {
      this.timeout(3000)

      await put(
        sot,
        sub,
        this.mode,
        this.flowId,
        {
          accountId,
          groupId,
          groupType: groupType.toLowerCase()
        },
        this.dto
      )

      const retrieved = await this.dynamodbFixture.hasArrived()
      retrieved.length.should.equal(2)
      retrieved.every(item => item.key.should.equal(this.key))

      const submittedItems = retrieved.filter(i => i.submitted === sot)
      submittedItems.every(item => item.sort_key_A.should.equal(sot))
      submittedItems.every(item => item.sort_key_B.should.equal(sot))
      submittedItems.every(item =>
        item.partition_key_A.should.equal(`/${this.mode}/account/${item.data.accountId}/administratorOf`)
      )
      submittedItems.every(item =>
        item.partition_key_B.should.equal(`/${this.mode}/group/${item.data.groupId}/administrators`)
      )

      const actualItems = retrieved.filter(i => i.submitted === 'actual')
      actualItems.every(item => item.sort_key_A.should.equal('actual'))
      actualItems.every(item => item.sort_key_B.should.equal('actual'))
      actualItems.every(item =>
        item.partition_key_A.should.equal(`/${this.mode}/account/${item.data.accountId}/administratorOf`)
      )
      actualItems.every(item =>
        item.partition_key_B.should.equal(`/${this.mode}/group/${item.data.groupId}/administrators`)
      )
    })
  }

  createTest('Team')
  createTest('Company')

  it('should throw a badRequest with bad path parameters', async function () {
    const exc = await put(
      sot,
      sub,
      this.mode,
      this.flowId,
      {
        accountId: accountId,
        groupId: groupId
        // no type
      },
      this.dto
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest with a bad body', async function () {
    this.dto.deleted = 12345
    const exc = await put(
      sot,
      sub,
      this.mode,
      this.flowId,
      {
        accountId,
        groupId,
        groupType: 'team'
      },
      this.dto
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
