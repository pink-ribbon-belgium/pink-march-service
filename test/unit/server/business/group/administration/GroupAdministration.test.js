/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const GroupAdministration = require('../../../../../../lib/server/business/group/administration/GroupAdministration')
const GroupAdministrationDTO = require('../../../../../../lib/api/group/GroupAdministrationDTO')
const Mode = require('../../../../../../lib/ppwcode/Mode')
const GroupType = require('../../../../../../lib/api/group/GroupType')

const KwargsInputTeam = {
  mode: Mode.example,
  dto: {
    ...GroupAdministrationDTO.exampleIn,
    createdAt: '2020-03-23T08:35:18.393Z',
    createdBy: 'a-sub',
    groupType: GroupType.teamType
  }
}
const KwargsInputCompany = {
  mode: Mode.example,
  dto: {
    ...GroupAdministrationDTO.exampleIn,
    createdAt: '2020-03-23T08:35:18.393Z',
    createdBy: 'a-sub',
    groupType: GroupType.companyType
  }
}
const KwargsDynamodbTeam = {
  mode: Mode.example,
  dto: GroupAdministrationDTO.exampleDynamodbTeam
}
const KwargsExample = {
  mode: Mode.example,
  dto: GroupAdministrationDTO.exampleDynamodbCompany
}

describe(testName, function () {
  beforeEach(function () {
    GroupAdministration.contract.verifyPostconditions = true
  })
  afterEach(function () {
    GroupAdministration.contract.verifyPostconditions = false
  })
  it('should work with team input', function () {
    const result = new GroupAdministration(KwargsInputTeam)
    result.should.upholdInvariants()
  })
  it('should work with company input', function () {
    const result = new GroupAdministration(KwargsInputCompany)
    result.should.upholdInvariants()
  })
  it('should work with a team from dynamodb', function () {
    const result = new GroupAdministration(KwargsDynamodbTeam)
    result.should.upholdInvariants()
  })
  it('should work with a company from dynamodb', function () {
    const result = new GroupAdministration(KwargsExample)
    result.should.upholdInvariants()
  })
})
