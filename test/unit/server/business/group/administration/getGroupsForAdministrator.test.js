/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const dynamodb = require('../../../../../../lib/server/aws/dynamodb/dynamodb')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const get = require('../../../../../../lib/server/business/group/administration/getGroupsForAdministrator')
const Boom = require('@hapi/boom')
const groupTypes = require('../../../../../../lib/api/group/GroupType').values

const sub = 'sub-getGroupsForAdministrator-test'
const itemSot = '2018-03-24T08:34:18.024Z'
const accountId = 'accountId-getGroupsForAdministrator-test'
const itemFlowId = '7a18e581-4b4f-40f5-8d66-265171aabc13'
const getSot = '2020-03-24T08:34:18.024Z'
const getFlowId = '9515b784-aa1a-489a-b868-8c61d0e7506a'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()
    this.dynamodbFixture = new DynamodbFixture()

    this.mode = `automated-test-${uuidv4()}`

    get.contract.verifyPostconditions = true

    this.createItem = function (groupType, groupId, submittedValue, structureVersion1) {
      const itemData = {
        createdAt: itemSot,
        createdBy: sub,
        structureVersion: structureVersion1 ? 1 : 2,
        groupId,
        accountId,
        groupType: structureVersion1 ? undefined : 'Team',
        deleted: false
      }

      return {
        mode: this.mode,
        key: `/${this.mode}/groupAdministration/account/${accountId}/group/${groupId}`,
        submitted: submittedValue,
        submittedBy: sub,
        flowId: itemFlowId,
        data: itemData,
        partition_key_A: `/${this.mode}/account/${accountId}/administratorOf`,
        sort_key_A: submittedValue,
        partition_key_B: `/${this.mode}/group/${groupId}/administrators`,
        sort_key_B: submittedValue
      }
    }
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    const item1 = this.createItem('Team', '8380731d-6980-4d1e-825e-de84baf278cf', 'actual')
    const item2 = this.createItem('Company', '1778651b-fbec-48f4-a5bd-0c3f6e8e8a2e', 'actual')
    const item3 = this.createItem('Team', '00a566b8-d5db-4e9b-aff7-c38aa7c70ec2', itemSot)

    await Promise.all([
      this.dynamodbFixture.putItem(item1),
      this.dynamodbFixture.putItem(item2),
      this.dynamodbFixture.putItem(item3)
    ])

    const result = await get(getSot, sub, this.mode, getFlowId, { accountId })

    result.length.should.equal(2)
    result.every(item => item.accountId.should.equal(accountId))
  })
  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      it('reports not found when the company does not exist', async function () {
        const item = this.createItem(groupType, '60878fec-59e7-41a2-827b-560d8fc298ba', itemSot)
        await this.dynamodbFixture.putItem(item)

        const exc = await get(getSot, sub, this.mode, getFlowId, {
          accountId: 'nice-accountId'
        }).should.be.rejectedWith(Boom.notFound())
        console.log(exc)
      })
      it('should throw a bad request', async function () {
        const item = this.createItem(groupType, '577f0734-f739-4dcf-83e7-32443321f0f9', itemSot)
        await this.dynamodbFixture.putItem(item)

        const exc = await get(getSot, sub, this.mode, getFlowId, {
          accountId: 524
        }).should.be.rejected()
        console.log(exc)
        exc.isBoom.should.be.true()
        exc.output.statusCode.should.equal(400)
      })
    })
  })

  it('should work with structure version 1, when there are no groups', async function () {
    const item1 = this.createItem('Team', '8380731d-6980-4d1e-825e-de84baf278cf', 'actual', true)
    const item2 = this.createItem('Company', '1778651b-fbec-48f4-a5bd-0c3f6e8e8a2e', 'actual', true)
    const item3 = this.createItem('Team', '00a566b8-d5db-4e9b-aff7-c38aa7c70ec2', itemSot, true)

    await Promise.all([
      this.dynamodbFixture.putItem(item1),
      this.dynamodbFixture.putItem(item2),
      this.dynamodbFixture.putItem(item3)
    ])

    const result = await get(getSot, sub, this.mode, getFlowId, { accountId })

    result.length.should.equal(0) // no associated group
  })
  it('should work with structure version 1, when there are groups', async function () {
    const groupId1 = '6e93860f-c793-4a3f-8f9c-b1a79227b3f3'
    const groupId2 = 'f606e4c9-2f3d-432b-a0f9-63294f983c47'
    const groupId3 = '751bc92c-5cba-4ee7-bfbf-a85fa06ccdf2'
    this.dynamodbFixture.putItem({
      mode: this.mode,
      key: `/${this.mode}/team/${groupId1}`,
      submitted: itemSot,
      submittedBy: sub,
      itemFlowId,
      data: {
        createdAt: itemSot,
        createdBy: sub,
        structureVersion: 1,
        id: groupId1,
        code: 'TEST-TEST-TEST-TEST',
        groupType: 'Team'
      }
    })
    const item1 = this.createItem('Team', groupId1, 'actual', true)
    const item2 = this.createItem('Company', groupId2, 'actual', true)
    const item3 = this.createItem('Team', groupId3, itemSot, true)

    await Promise.all([
      this.dynamodbFixture.putItem({
        mode: this.mode,
        key: `/${this.mode}/team/${groupId1}`,
        submitted: itemSot,
        submittedBy: sub,
        flowId: itemFlowId,
        data: {
          createdAt: itemSot,
          createdBy: sub,
          structureVersion: 1,
          id: groupId1,
          code: 'TEST-TEST-TEST-TEST',
          groupType: 'Team'
        }
      }),
      this.dynamodbFixture.putItem({
        mode: this.mode,
        key: `/${this.mode}/team/${groupId2}`,
        submitted: itemSot,
        submittedBy: sub,
        flowId: itemFlowId,
        data: {
          createdAt: itemSot,
          createdBy: sub,
          structureVersion: 1,
          id: groupId2,
          code: 'TEST-TEST-TEST-TEST',
          groupType: 'Company'
        }
      }),
      this.dynamodbFixture.putItem(item1),
      this.dynamodbFixture.putItem(item2),
      this.dynamodbFixture.putItem(item3)
    ])

    const result = await get(getSot, sub, this.mode, getFlowId, { accountId })

    result.length.should.equal(2) // no associated group
    result.every(item => item.accountId.should.equal(accountId))
    result.every(item => item.structureVersion.should.equal(2))
  })
})
