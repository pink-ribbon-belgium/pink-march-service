/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../../lib/server/business/group/profile/put')
const DynmamodbFixture = require('../../../../../_DynamodbFixture')

const groupId = '0990a5d2-548a-4b8e-9480-8870e17707c0'
const createdAt = '2020-01-11T11:20:01.007Z'
const createdBy = 'id-groupProfile-test'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynmamodbFixture()

    this.flowId = uuidv4()
    this.mode = `automated-test-${this.flowId}`

    this.dto = {
      structureVersion: 1,
      unknownProperty: {
        extraValue: 'extra value'
      },
      name: 'test-name-groupProfile',
      logo: 'VE9PTUFOWVNFQ1JFVFM='
    }
    this.key = `/${this.mode}/group/${groupId}/publicProfile`

    this.dynamodbFixture.remember(this.key, createdAt, config.dynamodb.test.tableName)

    put.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    await this.dynamodbFixture.clean()
  })
  it('works with a teamId', async function () {
    await put(
      createdAt,
      createdBy,
      this.mode,
      this.flowId,
      {
        teamId: groupId
      },
      this.dto
    )
    const retrieved = await this.dynamodbFixture.hasArrived()
    retrieved.length.should.equal(1)
    retrieved[0].key.should.equal(this.key)
  })

  it('works with companyId', async function () {
    await put(
      createdAt,
      createdBy,
      this.mode,
      this.flowId,
      {
        companyId: groupId
      },
      this.dto
    )
    const retrieved = await this.dynamodbFixture.hasArrived()
    retrieved.length.should.equal(1)
    retrieved[0].key.should.equal(this.key)
  })

  it('should throw a badRequest when body is invalid', async function () {
    this.dto.name = 12345

    const exc = await put(
      createdAt,
      createdBy,
      this.mode,
      this.flowId,
      { teamId: groupId },
      this.dto
    ).should.be.rejected()

    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const exc = await put(createdAt, createdBy, this.mode, this.flowId, { groupId }, this.dto).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
