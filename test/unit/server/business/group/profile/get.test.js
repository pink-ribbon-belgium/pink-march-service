/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const get = require('../../../../../../lib/server/business/group/profile/get')
const Boom = require('@hapi/boom')
const { v4: uuidv4 } = require('uuid')

const itemFlowId = '09509069-a11b-43d4-a27d-3ef50d4c5fe2'

describe(testName, function () {
  // TODO needs cleaning
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  function createItem (sot, sub, mode, flowId, pathParams, partialKey, groupType) {
    const idName = `${groupType}Id`
    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 76,
      id: pathParams[idName],
      name: 'test-groupName',
      logo: 'VE9PTUFOWVNFQ1JFVFM='
    }

    return {
      mode: mode,
      key: `/${mode}${partialKey}`,
      submitted: itemData.createdAt,
      submittedBy: itemData.createdBy,
      flowId: itemFlowId,
      data: { ...itemData }
    }
  }

  function createTest (groupType) {
    it(`should work for ${groupType}`, async function () {
      this.timeout(3000)
      const idName = `${groupType}Id`
      const testUuid = uuidv4()
      const sot = '2020-02-04T11:00:00.007Z'
      const sub = 'id-subTest3'
      const mode = `automated-test-${testUuid}`
      const startFlowId = 'e443139b-2683-4e6e-b0aa-315d34798bd8'
      const pathParams = {
        [idName]: '5f787064-2580-43c1-b53d-b5a041e287e1'
      }
      const key = `/group/${pathParams[idName]}/publicProfile`

      const item = createItem(sot, sub, mode, startFlowId, pathParams, key, groupType)
      await this.dynamodbFixture.putItem(item)

      const getItem = {
        mode: mode,
        key: `/${mode}${key}`,
        submitted: sot,
        submittedBy: sub,
        flowId: itemFlowId,
        data: {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 76,
          id: pathParams[idName],
          name: 'test-groupName',
          logo: 'VE9PTUFOWVNFQ1JFVFM=',
          links: {
            group: `/I/group/${pathParams[idName]}`
          }
        }
      }

      const resultPromise = get(sot, sub, mode, startFlowId, pathParams)
      const result = await resultPromise
      console.log(`expected result => ${JSON.stringify(result)}`)
      delete item.data.unknownProperty // Note: Extra property was put in DynamoDB, we don't need to check it with deepEqual
      result.should.deepEqual(getItem.data)
    })
  }

  createTest('team')
  createTest('company')

  it('should work with companyId', async function () {
    this.testUuid = uuidv4()
    this.sot = '2020-02-04T11:00:00.007Z'
    this.sub = 'id-subTest1'
    this.mode = `automated-test-${this.testUuid}`
    this.startFlowId = 'e443139b-2683-4e6e-b0aa-315d34798bd8'
    this.pathParams = {
      companyId: '5f787064-2580-43c1-b53d-b5a041e287e1'
    }
    this.key = `/group/${this.pathParams.companyId}/publicProfile`

    const item = createItem(this.sot, this.sub, this.mode, this.startFlowId, this.pathParams, this.key, 'company')
    await this.dynamodbFixture.putItem(item)

    const getItem = {
      mode: this.mode,
      key: `/${this.mode}${this.key}`,
      submitted: this.sot,
      submittedBy: this.sub,
      flowId: itemFlowId,
      data: {
        createdAt: this.sot,
        createdBy: this.sub,
        structureVersion: 76,
        id: this.pathParams.companyId,
        name: 'test-groupName',
        logo: 'VE9PTUFOWVNFQ1JFVFM=',
        links: {
          group: `/I/group/${this.pathParams.companyId}`
        }
      }
    }

    const resultPromise = get(this.sot, this.sub, this.mode, this.startFlowId, this.pathParams)
    const result = await resultPromise
    console.log(`expected result => ${JSON.stringify(result)}`)
    delete item.data.unknownProperty // Note: Extra property was put in DynamoDB, we don't need to check it with deepEqual
    result.should.deepEqual(getItem.data)
  })

  it('reports not found when the account does not exist', async function () {
    const testUuid = uuidv4()
    const sot = '2020-01-27T11:00:00.007Z'
    const sub = 'id-subTest4'
    const mode = `automated-test-${testUuid}`
    const startFlowId = '4d1abb85-343f-4c49-90b8-4de1efee0c80'
    const pathParams = {
      teamId: 'cca7caab-f446-47d9-a3a1-36d7560672b2'
    }
    const key = `/group/${pathParams.teamId}/publicProfile`

    const item = createItem(sot, sub, mode, startFlowId, pathParams, key, 'team')
    await this.dynamodbFixture.putItem(item)

    const exc = await get(sot, sub, mode, startFlowId, {
      teamId: 'fd6b3e9a-4611-46e8-84bb-78ec5f5a1069'
    }).should.be.rejectedWith(Boom.notFound())
    console.log(exc)
  })

  it('should throw a bad request', async function () {
    const sot = '2020-02-19T19:44:08.491Z'
    const later = '2020-02-19T19:45:09.981Z'
    const sub = 'id-subTest2'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const groupType = 'team'

    const pathParameters = {
      accountId: 'amazing-accountId',
      teamId: uuidv4()
    }
    const key = `/groupAdministration/account/${pathParameters.accountId}/group/${pathParameters.teamId}`
    const item = createItem(sot, sub, mode, flowId, pathParameters, key, groupType)
    await this.dynamodbFixture.putItem(item)

    const exc = await get(later, sub, mode, flowId, {
      accountId: 'amazing-accountId',
      teamId: 5546
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
