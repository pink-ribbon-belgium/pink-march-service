/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const getGroupPayments = require('../../../../../../lib/server/business/group/payments/get')
const DynamoDbFixture = require('../../../../../_DynamodbFixture')
const PaymentExample = require('../../../../../../lib/api/payment/PaymentDTO').exampleDynamodb
const Payment = require('../../../../../../lib/server/business/payment/Payment')
const uuidv4 = require('uuid').v4
const boom = require('@hapi/boom')

const itemFlowId = '8e5b06a1-3797-4236-86e2-036d14063647'

describe(testName, function () {
  // TODO needs cleaning
  beforeEach(function () {
    this.mode = `automated-test-${uuidv4()}`
    this.sot = new Date().toISOString()
    this.sub = uuidv4()
    this.flowId = uuidv4()
    this.dynamoDbFixture = new DynamoDbFixture()
    getGroupPayments.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    getGroupPayments.contract.verifyPostconditions = false
    await this.dynamoDbFixture.clean()
  })

  it('should work', async function () {
    const paymentModel = new Payment({ mode: this.mode, dto: PaymentExample })
    await this.dynamoDbFixture.putItem({
      mode: this.mode,
      key: paymentModel.key,
      submitted: paymentModel.createdAt,
      submittedBy: paymentModel.createdBy,
      flowId: itemFlowId,
      data: paymentModel.toJSON(),
      partition_key_A: `/${this.mode}/voucher/${paymentModel.voucherCode}/payments`,
      sort_key_A: paymentModel.createdAt,
      partition_key_B: `/${this.mode}/status/${paymentModel.paymentStatus}/payments`,
      sort_key_B: paymentModel.createdAt,
      partition_key_C: `/${this.mode}/team/${paymentModel.groupId}/payments`,
      sort_key_C: paymentModel.createdAt
    })

    const result = await getGroupPayments(this.sot, this.sub, this.mode, this.flowId, { teamId: paymentModel.groupId })

    result.should.be.an.Array()
    result.length.should.equal(1)
    delete result[0].links
    result[0].should.deepEqual(PaymentExample)
  })

  it('should work with companyId', async function () {
    const paymentModel = new Payment({ mode: this.mode, dto: PaymentExample })
    await this.dynamoDbFixture.putItem({
      mode: this.mode,
      key: paymentModel.key,
      submitted: paymentModel.createdAt,
      submittedBy: paymentModel.createdBy,
      flowId: itemFlowId,
      data: paymentModel.toJSON(),
      partition_key_A: `/${this.mode}/voucher/${paymentModel.voucherCode}/payments`,
      sort_key_A: paymentModel.createdAt,
      partition_key_B: `/${this.mode}/status/${paymentModel.paymentStatus}/payments`,
      sort_key_B: paymentModel.createdAt,
      partition_key_C: `/${this.mode}/company/${paymentModel.groupId}/payments`,
      sort_key_C: paymentModel.createdAt
    })

    const result = await getGroupPayments(this.sot, this.sub, this.mode, this.flowId, {
      companyId: paymentModel.groupId
    })

    result.should.be.an.Array()
    result.length.should.equal(1)
    delete result[0].links
    result[0].should.deepEqual(PaymentExample)
  })

  it('should throw 404', async function () {
    await getGroupPayments(this.sot, this.sub, this.mode, this.flowId, {
      teamId: PaymentExample.groupId
    }).should.be.rejectedWith(boom.notFound())
  })

  it('should throw a bad request', async function () {
    const exc = await getGroupPayments(this.sot, this.sub, this.mode, this.flowId, {
      groupId: PaymentExample.groupId
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
