/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

function mockResponse (body, status, statusText) {
  const mockResponse = {
    status,
    statusText,
    json: () => body
  }

  return Promise.resolve(mockResponse)
}

describe(testName, function () {
  beforeEach(async function () {
    this.fetchStub = sinon.stub()

    this.deleteShortLinkProxy = proxyquire('../../../../../../lib/server/business/group/shortLink/deleteShortLink', {
      'node-fetch': this.fetchStub
    })
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2021-02-08T11:29:43.572Z'
    this.sub = 'delete-shortlink-test'
    this.joinLink = 'https://www.derozemars.be/'

    this.deleteShortLinkProxy.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.deleteShortLinkProxy.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const linkId = uuidv4()
    const groupId = uuidv4()

    const responseBody = {
      id: linkId,
      title: groupId,
      slashtag: 'xe5d20z',
      destination: this.joinLink,
      createdAt: '2021-02-18T12:46:27.000Z',
      updatedAt: '2021-02-18T12:46:27.000Z',
      expiredAt: null,
      status: 'deleted',
      tags: [],
      clicks: 0,
      isPublic: false,
      shortUrl: 'rebrand.ly/xe5d20z',
      domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
      domainName: 'rebrand.ly',
      domain: {
        id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        fullName: 'rebrand.ly',
        active: true
      },
      https: true,
      favourite: false,
      integrated: false
    }

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.deleteShortLinkProxy(this.sot, this.sub, this.mode, this.flowId, {
      linkId
    })

    console.log(result)
    result.should.be.an.Object()
    result.linkId.should.equal(linkId)
    result.deleted.should.equal(responseBody.status)
  })

  it('should set error code and statustext as result', async function () {
    const groupId = uuidv4()
    const expectedResult = {
      linkId: groupId,
      deleted: '404 - Not Found'
    }
    this.fetchStub.returns(mockResponse({}, 404, 'Not Found'))

    const result = await this.deleteShortLinkProxy(this.sot, this.sub, this.mode, this.flowId, {
      linkId: groupId
    })
    console.log(result)
    result.should.be.an.Object()
    result.linkId.should.equal(expectedResult.linkId)
    result.deleted.should.equal(expectedResult.deleted)
  })

  it('should use correct apikey with production mode', async function () {
    const linkId = uuidv4()
    const groupId = uuidv4()

    const responseBody = {
      id: linkId,
      title: groupId,
      slashtag: 'xe5d20z',
      destination: this.joinLink,
      createdAt: '2021-02-18T12:46:27.000Z',
      updatedAt: '2021-02-18T12:46:27.000Z',
      expiredAt: null,
      status: 'deleted',
      tags: [],
      clicks: 0,
      isPublic: false,
      shortUrl: 'rebrand.ly/xe5d20z',
      domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
      domainName: 'rebrand.ly',
      domain: {
        id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        fullName: 'rebrand.ly',
        active: true
      },
      https: true,
      favourite: false,
      integrated: false
    }

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.deleteShortLinkProxy(this.sot, this.sub, 'production', this.flowId, {
      linkId
    })
    console.log(result)
    result.should.be.an.Object()
    result.linkId.should.equal(linkId)
    result.deleted.should.equal(responseBody.status)
  })
})
