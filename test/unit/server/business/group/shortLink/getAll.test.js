/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

function mockResponse (body, status, statusText) {
  const mockResponse = {
    status,
    statusText,
    json: () => body
  }

  return Promise.resolve(mockResponse)
}

describe(testName, function () {
  beforeEach(async function () {
    this.fetchStub = sinon.stub()

    this.getAllProxy = proxyquire('../../../../../../lib/server/business/group/shortLink/getAll', {
      'node-fetch': this.fetchStub
    })
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2021-02-08T11:29:43.572Z'
    this.sub = 'create-shortlink-test'

    this.linkId = uuidv4()
    this.linkId2 = uuidv4()
    this.linkId3 = uuidv4()
    this.groupId = uuidv4()
    this.groupId2 = uuidv4()
    this.groupId3 = uuidv4()
    this.joinLink = 'https://www.google.com/'
    this.joinLink2 = 'https://www.facebook.com/'
    this.joinLink3 = 'https://www.derozemars.be/'

    this.getAllProxy.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.getAllProxy.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const responseBody = [
      {
        id: this.linkId,
        title: this.groupId,
        slashtag: 'xe5d20z',
        destination: this.joinLink,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/xe5d20z',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      },
      {
        id: this.linkId2,
        title: this.groupId2,
        slashtag: 'oe5d281h',
        destination: this.joinLink2,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/oe5d281h',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      },
      {
        id: this.linkId3,
        title: this.groupId3,
        slashtag: 'rd754a2',
        destination: this.joinLink,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/rd754a2',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      }
    ]

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.getAllProxy(this.sot, this.sub, this.mode, this.flowId)

    result.should.be.an.Array()
    result.length.should.equal(3)
    result[0].linkId.should.equal(responseBody[0].id)
    result[0].shortLink.should.equal(`https://${responseBody[0].shortUrl}`)
    result[0].destination.should.equal(responseBody[0].destination)
    result[1].linkId.should.equal(responseBody[1].id)
    result[1].shortLink.should.equal(`https://${responseBody[1].shortUrl}`)
    result[1].destination.should.equal(responseBody[1].destination)
    result[2].linkId.should.equal(responseBody[2].id)
    result[2].shortLink.should.equal(`https://${responseBody[2].shortUrl}`)
    result[2].destination.should.equal(responseBody[2].destination)
  })

  it('should return empty array', async function () {
    this.fetchStub.returns(mockResponse({}, 404, 'Not Found'))

    const result = await this.getAllProxy(this.sot, this.sub, this.mode, this.flowId)
    console.log(result)

    result.length.should.equal(0)
  })

  it('should use correct apikey with production mode', async function () {
    const responseBody = [
      {
        id: this.linkId,
        title: this.groupId,
        slashtag: 'xe5d20z',
        destination: this.joinLink,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/xe5d20z',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      },
      {
        id: this.linkId2,
        title: this.groupId2,
        slashtag: 'oe5d281h',
        destination: this.joinLink2,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/oe5d281h',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      },
      {
        id: this.linkId3,
        title: this.groupId3,
        slashtag: 'rd754a2',
        destination: this.joinLink3,
        createdAt: '2021-02-18T12:46:27.000Z',
        updatedAt: '2021-02-18T12:46:27.000Z',
        expiredAt: null,
        status: 'active',
        tags: [],
        clicks: 0,
        isPublic: false,
        shortUrl: 'rebrand.ly/rd754a2',
        domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        domainName: 'rebrand.ly',
        domain: {
          id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
          fullName: 'rebrand.ly',
          active: true
        },
        https: true,
        favourite: false,
        integrated: false
      }
    ]

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.getAllProxy(this.sot, this.sub, 'production', this.flowId)

    result.should.be.an.Array()
    result.length.should.equal(3)
    result[0].linkId.should.equal(responseBody[0].id)
    result[0].shortLink.should.equal(`https://${responseBody[0].shortUrl}`)
    result[0].destination.should.equal(responseBody[0].destination)
    result[1].linkId.should.equal(responseBody[1].id)
    result[1].shortLink.should.equal(`https://${responseBody[1].shortUrl}`)
    result[1].destination.should.equal(responseBody[1].destination)
    result[2].linkId.should.equal(responseBody[2].id)
    result[2].shortLink.should.equal(`https://${responseBody[2].shortUrl}`)
    result[2].destination.should.equal(responseBody[2].destination)
  })
})
