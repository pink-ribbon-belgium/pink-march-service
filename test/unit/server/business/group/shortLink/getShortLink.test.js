/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const GroupType = require('../../../../../../lib/api/group/GroupType')

function mockResponse (body, status, statusText) {
  const mockResponse = {
    status,
    statusText,
    json: () => body
  }

  return Promise.resolve(mockResponse)
}

describe(testName, function () {
  beforeEach(async function () {
    this.fetchStub = sinon.stub()

    this.getShortLinkProxy = proxyquire('../../../../../../lib/server/business/group/shortLink/getShortLink', {
      'node-fetch': this.fetchStub
    })
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2021-02-08T11:29:43.572Z'
    this.sub = 'get-shortlink-test'

    this.getShortLinkProxy.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.getShortLinkProxy.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const linkId = uuidv4()
    const groupId = uuidv4()
    const joinLink = `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.companyType.toLowerCase()}/${groupId}`

    const responseBody = {
      id: linkId,
      title: groupId,
      slashtag: 'xe5d20z',
      destination: joinLink,
      createdAt: '2021-02-18T12:46:27.000Z',
      updatedAt: '2021-02-18T12:46:27.000Z',
      expiredAt: null,
      status: 'active',
      tags: [],
      clicks: 0,
      isPublic: false,
      shortUrl: 'rebrand.ly/xe5d20z',
      domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
      domainName: 'rebrand.ly',
      domain: {
        id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        fullName: 'rebrand.ly',
        active: true
      },
      https: true,
      favourite: false,
      integrated: false
    }

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.getShortLinkProxy(this.sot, this.sub, this.mode, this.flowId, {
      linkId
    })

    console.log(result)
    result.should.be.an.Object()
    result.linkId.should.equal(linkId)
    result.shortLink.should.equal(`https://${responseBody.shortUrl}`)
    result.destination.should.equal(responseBody.destination)
  })

  it('should throw error', async function () {
    const groupId = uuidv4()
    this.fetchStub.returns(mockResponse({}, 404, 'Not Found'))

    const exception = await this.getShortLinkProxy(this.sot, this.sub, this.mode, this.flowId, {
      linkId: groupId
    }).should.be.rejectedWith(new Error('404 - Not Found'))
    console.log(exception)
  })

  it('should use correct apikey with production mode', async function () {
    const linkId = uuidv4()
    const groupId = uuidv4()
    const joinLink = `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.companyType.toLowerCase()}/${groupId}`

    const responseBody = {
      id: linkId,
      title: groupId,
      slashtag: 'xe5d20z',
      destination: joinLink,
      createdAt: '2021-02-18T12:46:27.000Z',
      updatedAt: '2021-02-18T12:46:27.000Z',
      expiredAt: null,
      status: 'active',
      tags: [],
      clicks: 0,
      isPublic: false,
      shortUrl: 'rebrand.ly/xe5d20z',
      domainId: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
      domainName: 'rebrand.ly',
      domain: {
        id: '8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        ref: '/domains/8f104cc5b6ee4a4ba7897b06ac2ddcfb',
        fullName: 'rebrand.ly',
        active: true
      },
      https: true,
      favourite: false,
      integrated: false
    }

    this.fetchStub.returns(mockResponse(responseBody, 200))

    const result = await this.getShortLinkProxy(this.sot, this.sub, 'production', this.flowId, {
      linkId
    })
    console.log(result)
    result.should.be.an.Object()
    result.linkId.should.equal(linkId)
    result.shortLink.should.equal(`https://${responseBody.shortUrl}`)
    result.destination.should.equal(responseBody.destination)
  })
})
