/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const getGroupVerification = require('../../../../../../lib/server/business/group/verification/get')
const DynamoDbFixture = require('../../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4

const itemFlowId = 'e338b4bd-fdda-45c6-a94e-b9ad00fc01f7'

describe(testName, function () {
  beforeEach(async function () {
    this.testUuid = uuidv4()
    this.sot = '2020-01-27T11:00:00.007Z'
    this.sub = 'id-subTest4'
    this.mode = `automated-test-${this.testUuid}`
    this.startFlowId = '4d1abb85-343f-4c49-90b8-4de1efee0c80'
    this.dynamodbFixture = new DynamoDbFixture()
    getGroupVerification.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
    getGroupVerification.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const result = await getGroupVerification(this.sot, this.sub, this.mode, this.startFlowId, { teamId: uuidv4() })
    result.should.be.an.Object()
    result.groupExists.should.be.false()
    result.slotAvailable.should.be.false()
  })

  it('should work with companyId', async function () {
    const result = await getGroupVerification(this.sot, this.sub, this.mode, this.startFlowId, { companyId: uuidv4() })
    result.should.be.an.Object()
    result.groupExists.should.be.false()
    result.slotAvailable.should.be.false()
  })

  it('should throw a bad request', async function () {
    const teamId = uuidv4()
    const exc = await getGroupVerification(this.sot, this.sub, this.mode, this.startFlowId, {
      groupId: teamId
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  describe('with team', function () {
    beforeEach(async function () {
      this.teamId = uuidv4()
      await this.dynamodbFixture.putItem({
        mode: this.mode,
        key: `/${this.mode}/team/${this.teamId}`,
        submitted: this.sot,
        submittedBy: this.sub,
        flowId: itemFlowId,
        data: {
          id: this.teamId,
          structureVersion: 1,
          createdAt: this.sot,
          createdBy: this.sub,
          code: 'FKFG-KDKK-KSKK-LWSK',
          groupType: 'Team'
        }
      })
    })

    it('should return groupExists true', async function () {
      const result = await getGroupVerification(this.sot, this.sub, this.mode, this.startFlowId, {
        teamId: this.teamId
      })
      result.should.be.an.Object()
      result.groupExists.should.be.true()
      result.slotAvailable.should.be.false()
    })

    it('should be completely fine', async function () {
      const slotId = uuidv4()
      const paymentId = uuidv4()
      await this.dynamodbFixture.putItem({
        mode: this.mode,
        key: `/${this.mode}/group/${this.teamId}/slot/${slotId}`,
        submitted: 'actual',
        submittedBy: this.sub,
        flowId: itemFlowId,
        data: {
          id: slotId,
          groupId: this.teamId,
          structureVersion: 1,
          createdAt: this.sot,
          createdBy: this.sub,
          paymentId
        },
        partition_key_B: `/${this.mode}/group/${this.teamId}/free`,
        sort_key_B: 'actual'
      })

      const result = await getGroupVerification(this.sot, this.sub, this.mode, this.startFlowId, {
        teamId: this.teamId
      })
      result.should.be.an.Object()
      result.groupExists.should.be.true()
      result.slotAvailable.should.be.true()
    })
  })
})
