/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/voucher/get')
const generateGetTest = require('../_generateGetTest')

const voucherCode = 'PV2020'

function createVoucher (percentage, amount, sot, sub) {
  return {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 1,
    voucherCode,
    reusable: false,
    amount,
    percentage,
    expiryDate: '2020-03-19T13:08:05.387Z'
  }
}

describe(testName, function () {
  describe('percentage', function () {
    generateGetTest(
      get,
      createVoucher.bind(undefined, 0.1, undefined),
      mode => `/${mode}/voucher/${voucherCode}`,
      { voucherCode: 'voucherCode' },
      () => ({
        voucherCode
      }),
      () => ({
        voucherCode: 'AD7153'
      }),
      () => ({
        voucherCode: 'invalid code'
      })
    )
  })
  describe('amount', function () {
    generateGetTest(
      get,
      createVoucher.bind(undefined, undefined, 5.34),
      mode => `/${mode}/voucher/${voucherCode}`,
      { voucherCode: 'voucherCode' },
      () => ({
        voucherCode
      }),
      () => ({
        voucherCode: 'AD7153'
      }),
      () => ({
        voucherCode: 'invalid code'
      })
    )
  })
})
