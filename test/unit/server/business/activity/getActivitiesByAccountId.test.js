/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const { v4: uuidv4 } = require('uuid')

const testName = require('../../../../_testName')(module)
const config = require('config')
const getActivitiesByAccountId = require('../../../../../lib/server/business/account/activity/getActivitiesByAccountId')
const putActivity = require('../../../../../lib/server/business/activity/put')
const DynamodbFixture = require('../../../../_DynamodbFixture')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.accountId = 'account-id-for-test'

    this.flowId = uuidv4()

    this.sot = '2020-04-16T16:07:24.911Z'
    this.sub = 'sub-of-getLastActivity-test'

    const activityDate1 = '2020-06-12T00:00:00.000Z'
    const activityDate2 = '2020-06-13T00:00:00.000Z'
    const activityDate3 = '2020-06-14T00:00:00.000Z'

    const dynamoDbKey1 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate1.slice(0, 10)}`
    const dynamoDbKey2 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate2.slice(0, 10)}`
    const dynamoDbKey3 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate2.slice(0, 10)}`

    console.log(`Key1 => ${dynamoDbKey1}`)
    console.log(`Key2 => ${dynamoDbKey2}`)
    console.log(`Key3 => ${dynamoDbKey3}`)

    this.dynamodbFixture.remember(dynamoDbKey1, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, this.sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, 'actual', config.dynamodb.test.tableName)

    this.itemData1 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate1,
      trackerType: 'fitbit',
      numberOfSteps: 1000,
      distance: 950,
      deleted: false
    }
    this.itemData2 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate2,
      trackerType: 'fitbit',
      numberOfSteps: 2000,
      distance: 950,
      deleted: false
    }
    this.itemData3 = {
      createdAt: this.sot,
      createdBy: this.sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate3,
      trackerType: 'fitbit',
      numberOfSteps: 3000,
      distance: 950,
      deleted: true
    }
    await Promise.all([
      putActivity(
        this.sot,
        this.sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: activityDate1 },
        this.itemData3
      ),
      putActivity(
        this.sot,
        this.sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: activityDate2 },
        this.itemData1
      ),
      putActivity(
        this.sot,
        this.sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: activityDate3 },
        this.itemData2
      )
    ])

    getActivitiesByAccountId.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getActivitiesByAccountId.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    const result = await getActivitiesByAccountId(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    })
    console.log('Result', result)
    result.length.should.equal(2)
    result.every(item => item.accountId.should.equal(this.accountId))
  })

  it('should throw not found', async function () {
    const result = await getActivitiesByAccountId(this.sot, this.sub, this.mode, this.flowId, {
      accountId: 'some-non-existing-account'
    }).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
