/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const { v4: uuidv4 } = require('uuid')

const testName = require('../../../../_testName')(module)
const config = require('config')
const getLastActivity = require('../../../../../lib/server/business/activity/getLastActivity')
const putActivity = require('../../../../../lib/server/business/activity/put')
const TrackerType = require('../../../../../lib/api/account/trackerConnection/TrackerType')
const DynamodbFixture = require('../../../../_DynamodbFixture')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.accountId = 'account-id-for-test'

    this.flowId = uuidv4()

    const sot = '2020-04-16T16:07:24.911Z'
    const sub = 'sub-of-getLastActivity-test'

    const activityDate1 = '2020-06-12'
    const activityDate2 = '2020-06-13'
    const activityDate3 = '2020-06-14'
    const activityDate4 = '2020-06-15'

    const dynamoDbKey1 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate1.slice(0, 10)}`
    const dynamoDbKey2 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate2.slice(0, 10)}`
    const dynamoDbKey3 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate3.slice(0, 10)}`
    const dynamoDbKey4 = `/${this.mode}/activity/account/${this.accountId}/date/${activityDate4.slice(0, 10)}`

    console.log(`Key1 => ${dynamoDbKey1}`)
    console.log(`Key2 => ${dynamoDbKey2}`)
    console.log(`Key3 => ${dynamoDbKey3}`)
    console.log(`Key3 => ${dynamoDbKey4}`)

    this.dynamodbFixture.remember(dynamoDbKey1, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey1, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey2, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey3, 'actual', config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey4, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(dynamoDbKey4, 'actual', config.dynamodb.test.tableName)

    this.itemData1 = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate1,
      trackerType: TrackerType.fitbitType,
      numberOfSteps: 1000,
      distance: 500,
      deleted: false
    }
    this.itemData2 = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate2,
      trackerType: TrackerType.fitbitType,
      numberOfSteps: 2000,
      distance: 1500,
      deleted: false
    }
    this.itemData3 = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate3,
      trackerType: TrackerType.fitbitType,
      numberOfSteps: 3000,
      distance: 2500,
      deleted: false
    }
    this.itemData4 = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      accountId: this.accountId,
      activityDate: activityDate4,
      trackerType: TrackerType.manualType,
      numberOfSteps: 4000,
      distance: 3500,
      deleted: false
    }
    await Promise.all([
      putActivity(
        sot,
        sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: this.itemData4.activityDate },
        this.itemData4
      ),
      putActivity(
        sot,
        sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: this.itemData3.activityDate },
        this.itemData3
      ),
      putActivity(
        sot,
        sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: this.itemData1.activityDate },
        this.itemData1
      ),
      putActivity(
        sot,
        sub,
        this.mode,
        this.flowId,
        { accountId: this.accountId, activityDate: this.itemData2.activityDate },
        this.itemData2
      )
    ])

    getLastActivity.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getLastActivity.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    const result = await getLastActivity(this.mode, this.accountId)
    result.should.be.deepEqual(this.itemData4)
  })

  it('should throw not found', async function () {
    const result = await getLastActivity(this.mode, 'some-non-existing-account').should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })
})
