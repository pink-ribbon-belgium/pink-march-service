/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const calculation = require('../../../../../../lib/server/business/activity/calculation/calculateDistance')

const sot = '2020-03-08T11:29:43.572Z'
const mode = `automated-test-${uuidv4()}`
const sub = 'id-calc-test'
const flowId = uuidv4()

describe(testName, function () {
  beforeEach(function () {
    calculation.contract.verifyPostconditions = true
  })
  afterEach(function () {
    calculation.contract.verifyPostconditions = false
  })

  it('should work for steps', async function () {
    const body = {
      gender: 'F',
      steps: 2225
    }

    const result = await calculation(sot, sub, mode, flowId, undefined, body)
    console.log(result)
    result.should.be.an.Object()
    result.steps.should.equal(body.steps)
  })

  it('should work for distance', async function () {
    const body = {
      gender: 'M',
      distance: 2000
    }

    const result = await calculation(sot, sub, mode, flowId, undefined, body)
    result.should.be.an.Object()
    result.distance.should.equal(body.distance)
  })
})
