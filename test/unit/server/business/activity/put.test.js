/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../lib/server/business/activity/put')
const TrackerType = require('../../../../../lib/api/account/trackerConnection/TrackerType')

const flowId = '755ede2a-8e9e-4c15-b254-4cdafba28dff'
const sot = '2020-04-15T11:05:52.191Z'
const sub = 'id-of-the-creator'
const accountId = 'account-id-activity-put-test'
const activityDate = new Date('2020-06-01').toISOString()
const trackerType = TrackerType.polarType

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.dynamoDbKey = `/${this.mode}/activity/account/${accountId}/date/${activityDate.slice(0, 10)}`

    this.dynamodbFixture.remember(this.dynamoDbKey, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey, 'actual', config.dynamodb.test.tableName)

    this.itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 80,
      accountId: accountId,
      activityDate: activityDate,
      trackerType: trackerType,
      numberOfSteps: 1000,
      distance: 750,
      deleted: false
    }

    put.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000)
    put.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    await put(
      sot,
      sub,
      this.mode,
      flowId,
      {
        accountId: accountId,
        activityDate: activityDate
      },
      this.itemData
    )
    const storedItems = await this.dynamodbFixture.hasArrived()
    storedItems.length.should.equal(2)
    storedItems.some(item => item.submitted === sot).should.be.true()
    storedItems.some(item => item.submitted === 'actual').should.be.true()

    storedItems.forEach(activity => {
      if (activity.submitted === 'actual') {
        activity.should.have.ownProperty('partition_key_A')
        activity.partition_key_A.should.equal(`/${this.mode}/activity`)
        activity.should.have.ownProperty('sort_key_A')
        activity.sort_key_A.should.equal(activityDate.slice(0, 10))

        activity.should.have.ownProperty('partition_key_B')
        activity.partition_key_B.should.equal(`/${this.mode}/activity/account/${accountId}`)
        activity.should.have.ownProperty('sort_key_B')
        activity.sort_key_B.should.equal(activityDate.slice(0, 10))
      } else {
        activity.should.not.have.ownProperty('partition_key_A')
        activity.should.not.have.ownProperty('sort_key_A')
        activity.should.not.have.ownProperty('partition_key_B')
        activity.should.not.have.ownProperty('sort_key_B')
      }

      const expectedResult = {
        ...this.itemData,
        activityDate: this.itemData.activityDate.slice(0, 10)
      }

      activity.data.should.be.deepEqual(expectedResult)
    })
  })

  it('should throw a badRequest', async function () {
    const itemData = {
      ...this.itemData,
      accountId: 'some invalid accountId'
    }
    const exc = await put(
      sot,
      sub,
      this.mode,
      flowId,
      { accountId: accountId, activityDate: activityDate },
      itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const exc = await put(
      sot,
      sub,
      this.mode,
      flowId,
      { accountId: 'wrong id', activityDate: activityDate },
      this.itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw forbidden if activitydate is not within activityperiod', async function () {
    const activityDateOutOfPeriod = new Date('2020-05-31').toISOString()

    this.itemData.activityDate = activityDateOutOfPeriod

    const exc = await put(
      sot,
      sub,
      this.mode,
      flowId,
      {
        accountId: accountId,
        activityDate: activityDateOutOfPeriod
      },
      this.itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(403)
    exc.data.startDate.should.be.equal(config.activityPeriod.default.startdate)
    exc.data.endDate.should.be.equal(config.activityPeriod.default.enddate)
  })
})
