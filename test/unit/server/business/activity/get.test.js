/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/activity/get')
const generateGetTest = require('../_generateGetTest')
const TrackerType = require('../../../../../lib/api/account/trackerConnection/TrackerType')

const accountId = 'another-account-id'
const activityDate = '2020-01-23T15:22:39.212Z'
describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      accountId: accountId,
      activityDate: activityDate,
      trackerType: TrackerType.polarType,
      numberOfSteps: 100,
      distance: 10000,
      deleted: false
    }),
    mode => `/${mode}/activity/account/${accountId}/date/${activityDate.slice(0, 10)}`,
    { accountId: 'accountId', activityDate: 'activityDate' },
    () => ({ accountId, activityDate }),
    () => ({ accountId: 'some-non-existing-account', activityDate }),
    () => ({ accountId: 5442, activityDate }),
    () => ({
      account: `/I/account/${accountId}`,
      accountProfile: `/I/account/${accountId}/publicProfile`
    }),
    true
  )
})
