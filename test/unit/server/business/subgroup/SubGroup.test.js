/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const SubGroup = require('../../../../../lib/server/business/subgroup/SubGroup')
const SubGroupDTO = require('../../../../../lib/api/subgroup/SubGroupDTO')
const Mode = require('../../../../../lib/ppwcode/Mode')

const KwargsExample = {
  mode: Mode.example,
  dto: {
    ...SubGroupDTO.exampleDynamodb
  }
}

describe(testName, function () {
  beforeEach(function () {
    SubGroup.contract.verifyPostconditions = true
  })

  afterEach(function () {
    SubGroup.contract.verifyPostconditions = false
  })
  it('should work', function () {
    const result = new SubGroup(KwargsExample)
    result.should.upholdInvariants()
  })

  it('should getJoinLink', function () {
    const linkKwargs = {
      mode: Mode.exampleDemo,
      groupId: KwargsExample.dto.id
    }
    const linkUrl = SubGroup.getJoinLink(linkKwargs)
    const expectedResult = `https://demo.pink-march.pink-ribbon-belgium.org/index.html#/join/subgroup/${KwargsExample.dto.id}`

    linkUrl.should.equal(expectedResult)
  })

  it('should set june2020 joinLink', function () {
    const linkKwargs = {
      mode: Mode.exampleJune2020,
      groupId: KwargsExample.dto.id
    }
    const linkUrl = SubGroup.getJoinLink(linkKwargs)
    const expectedResult = `https://june2020.pink-march.pink-ribbon-belgium.org/index.html#/join/subgroup/${KwargsExample.dto.id}`

    linkUrl.should.equal(expectedResult)
  })
})
