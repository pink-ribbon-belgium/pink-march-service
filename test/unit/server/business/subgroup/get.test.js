/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/subgroup/get')
const generateGetTest = require('../_generateGetTest')
const { v4: uuidv4 } = require('uuid')

const subgroupId = 'b212156a-a113-4251-aa52-a06842d3fb20'
const groupId = '7ada088a-0230-4a1e-9ded-562801acf2ab'
const linkId = uuidv4()

describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      id: subgroupId,
      groupId: groupId,
      code: 'TEST-CODE-TEST-CODE',
      name: 'subgroup',
      logo: 'VE9PTUFOWVNFQ1JFVFM=',
      groupType: 'Company',
      accountId: 'another-amazing-account-id',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r'
    }),
    mode => `/${mode}/subgroup/${subgroupId}`,
    { subgroupId: 'id' },
    () => ({
      subgroupId,
      companyId: groupId
    }),
    () => ({
      subgroupId: '77ab99ac-0f74-40d4-bdff-a4edd345289d',
      companyId: 'd98ee343-2937-4e1d-85db-66eb8cc840a3'
    }),
    () => ({
      subgroupId: 4524,
      companyId: 47631
    }),
    () => ({
      slots: `/I/company/${subgroupId}/slots`
    }),
    false,
    false,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      id: subgroupId,
      groupId: groupId,
      code: 'TEST-CODE-TEST-CODE',
      name: 'subgroup',
      logo: 'VE9PTUFOWVNFQ1JFVFM=',
      groupType: 'Company',
      accountId: 'another-amazing-account-id',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r',
      joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/subgroup/${subgroupId}`
    })
  )
})
