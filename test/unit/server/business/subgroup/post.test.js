/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const post = require('../../../../../lib/server/business/subgroup/post')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

const flowId = 'dc8eee62-adcd-47d8-ae1f-404b7e2d45ea'
const sot = '2020-04-06T15:00:35.047Z'
const sub = 'id-of-subgroup-creator'
const subgroupId = '0d5ddaff-a382-423f-bebd-a2c020a1887d'
const accountId = 'amazing-accountId'
const name = 'MySubGroup'
const logo = 'VE9PTUFOWVNFQ1JFVFM='
const slotId = '59a7d9ff-074d-4f30-ad35-04a7f80ce371'
const paymentId = '5c71673e-6176-47b8-bf50-5e8e5d87ad84'
const groupType = 'Company'
const groupId = '05c91e16-25b9-4a1d-b2b4-836b97284413'

describe(testName, function () {
  beforeEach(async function () {
    this.mode = `automated-test-${uuidv4()}`
    this.getSlotStub = sinon.stub()
    this.writeSlotSpy = sinon.spy()
    this.writeSubgroupSpy = sinon.spy()
    this.getGroupProfileStub = sinon.stub()
    this.writeTeamAggregateStub = sinon.stub()

    this.postProxy = proxyquire('../../../../../lib/server/business/subgroup/post', {
      '../account/slot/get': this.getSlotStub,
      '../slot/write': this.writeSlotSpy,
      './write': this.writeSubgroupSpy,
      '../group/profile/get': this.getGroupProfileStub,
      '../aggregates/team/write': this.writeTeamAggregateStub
    })

    this.getGroupProfileStub.returns({
      name: 'Peopleware'
    })

    this.dtoData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      id: subgroupId,
      groupId: groupId,
      code: 'TEST-TEST-TEST-TEST',
      name: name,
      logo: logo,
      groupType,
      accountId: accountId
    }
    post.contract.verifyPostconditions = true
    this.postProxy.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    post.contract.verifyPostconditions = false
  })

  it('works', async function () {
    const slot = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 2,
      id: slotId,
      accountId,
      groupId: subgroupId,
      groupType,
      subgroupId: undefined,
      paymentId,
      links: {
        account: `/I/account/${accountId}`,
        accountProfile: `/I/account/${accountId}/publicProfile`,
        payment: `/I/payment/${paymentId}`,
        group: `/I/${groupType.toLowerCase()}/${subgroupId}`
      }
    }
    this.getSlotStub.resolves(slot)

    const result = await this.postProxy(sot, sub, this.mode, flowId, {}, this.dtoData)
    console.log(result)
    this.writeSlotSpy.should.be.called()
    this.writeSubgroupSpy.should.be.called()
    this.getGroupProfileStub.should.be.called()
    this.writeTeamAggregateStub.should.be.called()
  })

  it('should throw not found', async function () {
    const result = await post(sot, sub, this.mode, flowId, {}, this.dtoData).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })

  it('should throw a bad request', async function () {
    this.dtoData.groupType = 'Team'
    const result = await post(sot, sub, this.mode, flowId, {}, this.dtoData).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
