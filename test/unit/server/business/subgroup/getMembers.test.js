/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const getMembers = require('../../../../../lib/server/business/subgroup/getMembers')
const DynamoDbFixture = require('../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const boom = require('@hapi/boom')

const Slot = require('../../../../../lib/server/business/slot/Slot')
const AccountProfile = require('../../../../../lib/server/business/account/profile/AccountProfile')

const theGroupId = '2c823261-0363-4749-961f-08b5c5cca713'
const slotFlowId = '8e8e5e8d-feec-4019-9493-90e186a1ae90'
const accountProfileFlowId = '33eb990a-b014-45ce-bc71-90cbc870de54'
const slotSot = '2020-03-30T14:52:10.373Z'
const slotSub = 'slotSub-getMembers'
const getSot = '2020-03-30T14:53:28.122Z'
const getSub = 'sub-group-slots-get-test-get'
const getFlowId = '99967ea7-cd32-4a5e-9bbb-09deceb358b0'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamoDbFixture()
    this.mode = `automated-test-${uuidv4()}`
    getMembers.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getMembers.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  async function createSlots (mode, dynamodbFixture, slotData) {
    await Promise.all(
      slotData.map(i => {
        const slotId = i.slotId
        const paymentId = i.paymentId
        const accountId = i.accountId
        const subgroupId = i.subgroupId
        const sub = slotSub

        const slotData = {
          createdAt: slotSot,
          createdBy: sub,
          structureVersion: 2,
          id: slotId,
          groupType: 'Company',
          groupId: theGroupId,
          paymentId,
          accountId,
          subgroupId
        }

        const slot = new Slot({
          mode: mode,
          dto: {
            ...slotData
          },
          sot: slotSot,
          sub
        })

        const databaseItem = slot.toItem()
        databaseItem.flowId = slotFlowId
        databaseItem.submitted = 'actual'
        databaseItem.sort_key_A = 'actual'
        databaseItem.sort_key_B = 'actual'

        return dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  async function createAccountProfiles (mode, dynamodbFixture, accountProfileData) {
    await Promise.all(
      accountProfileData.map(i => {
        const accountId = i.accountId
        const firstName = i.firstName
        const lastName = i.lastName
        const sub = 'sub-of-account-profile'

        const itemData = {
          accountId: accountId,
          firstName: firstName,
          lastName: lastName,
          gender: 'M',
          dateOfBirth: '1987-01-23T15:22:39.212Z',
          zip: '4500',
          createdAt: '2020-04-22T18:21:28.122Z',
          createdBy: sub,
          structureVersion: 1
        }

        const accountProfile = new AccountProfile({
          mode: mode,
          dto: {
            ...itemData
          },
          sot: slotSot,
          sub
        })

        const databaseItem = accountProfile.toItem()
        databaseItem.flowId = accountProfileFlowId
        databaseItem.submitted = 'actual'
        databaseItem.sort_key_A = 'actual'

        dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  it('should work', async function () {
    this.timeout(5000)
    var baseSlot = {
      groupId: theGroupId,
      paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678'
    }
    const theSubgroupId = '59ef3d1a-8d99-423a-aac9-65382f44d334'

    var slotData = [
      { ...baseSlot, slotId: uuidv4(), accountId: 'jan-account', subgroupId: theSubgroupId },
      { ...baseSlot, slotId: uuidv4(), accountId: 'piet-account', subgroupId: theSubgroupId },
      { ...baseSlot, slotId: uuidv4(), accountId: 'jef-account' },
      { ...baseSlot, slotId: uuidv4(), accountId: 'jos-account' }
    ]

    var accountProfileData = [
      { accountId: 'jan-account', firstName: 'Jan', lastName: 'Janssens' },
      { accountId: 'piet-account', firstName: 'Piet', lastName: 'Peeters' },
      { accountId: 'jef-account', firstName: 'Jef', lastName: 'Jefferson' },
      { accountId: 'jos-account', firstName: 'Jos', lastName: 'Peetermans' }
    ]

    await Promise.all([
      createSlots(this.mode, this.dynamodbFixture, slotData),
      createAccountProfiles(this.mode, this.dynamodbFixture, accountProfileData)
    ])

    const arrivedSlots = await this.dynamodbFixture.hasArrived()
    arrivedSlots.length.should.be.equal(8)

    const result = await getMembers(getSot, getSub, this.mode, getFlowId, {
      subgroupId: theSubgroupId,
      companyId: theGroupId
    })

    console.log(result)
    result.length.should.equal(2)
    result.some(item => item.name === 'Janssens Jan' && item.accountId === 'jan-account')
    result.some(item => item.name === 'Peeters Piet' && item.accountId === 'piet-account')
  })

  it('should throw not found', async function () {
    const theSubgroupId = '59ef3d1a-8d99-423a-aac9-65382f44d334'

    var baseSlot = {
      groupId: theGroupId,
      paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678'
    }
    var slotData = [
      { ...baseSlot, slotId: uuidv4(), accountId: 'jan-account' },
      { ...baseSlot, slotId: uuidv4(), accountId: 'piet-account' },
      { ...baseSlot, slotId: uuidv4(), accountId: 'jef-account' },
      { ...baseSlot, slotId: uuidv4(), accountId: 'jos-account' }
    ]

    await createSlots(this.mode, this.dynamodbFixture, slotData)

    await getMembers(getSot, getSub, this.mode, getFlowId, {
      subgroupId: theSubgroupId,
      companyId: theGroupId
    }).should.be.rejectedWith(boom.notFound())
  })

  it('should throw a 400', async function () {
    const exc = await getMembers(getSot, getSub, this.mode, getFlowId, {
      companyId: theGroupId
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
