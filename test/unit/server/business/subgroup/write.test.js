/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../lib/server/business/subgroup/write')
const DynamodbFixture = require('../../../../_DynamodbFixture')

const itemFlowId = '3e2a5eee-38b2-477f-907f-731bc76aca76'
const sot = '2020-04-13T15:04:35.047Z'
const sub = 'sub-subgroup-write-test'
const subgroupId = '60583e2b-44fe-4cb1-86b4-3dc25063ff9b'
const groupId = '730c9c08-da98-430d-a832-3f17aa02c1ef'
const accountId = '32ae2230-6b62-4578-8535-0506f51b59f6'
const name = 'subgroup'
const logo = 'VE9PTUFOWVNFQ1JFVFM='

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.key = `/${this.mode}/subgroup/${subgroupId}`

    this.dynamodbFixture.remember(this.key, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

    this.dtoData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      id: subgroupId,
      groupId,
      code: 'TEST-TEST-TEST-TEST',
      name: name,
      logo: logo,
      groupType: 'Company',
      accountId: accountId
    }

    this.storedItemShouldBe = async function () {
      const storedItems = await this.dynamodbFixture.hasArrived()
      console.log(storedItems)
      storedItems.length.should.equal(2)
      storedItems.every(item => item.key.should.equal(this.key))

      const submittedItems = storedItems.filter(i => i.submitted === sot)
      submittedItems.every(item => (item.sort_key_A === undefined).should.be.true())
      submittedItems.every(item => (item.sort_key_B === undefined).should.be.true())
      submittedItems.every(item => (item.sort_key_C === undefined).should.be.true())

      const actualItems = storedItems.filter(i => i.submitted === 'actual')
      actualItems.every(item => item.sort_key_A.should.equal(item.data.name))
      actualItems.every(item => item.sort_key_B.should.equal(item.data.name))
      actualItems.every(item => item.sort_key_C.should.equal(item.data.name))
    }
    write.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000)
    write.contract.verifyPostconditions = false
    this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    await write(sot, sub, this.mode, itemFlowId, this.dtoData)
    await this.storedItemShouldBe()
  })
})
