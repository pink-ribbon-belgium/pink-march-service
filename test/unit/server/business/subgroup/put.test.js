/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../lib/server/business/subgroup/put')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.testUuid = uuidv4()
    this.flowId = '755ede2a-8e9e-4c15-b254-4cdafba28dff'
    this.mode = `automated-test-${this.testUuid}`
    this.name = 'subgroup'
    this.logo = 'VE9PTUFOWVNFQ1JFVFM='
    this.createdAt = new Date().toISOString()
    this.createdBy = 'id-of-the-creator'
    this.subgroupId = 'faff7de7-69a9-4d6f-8e4f-1955612571a2'
    this.groupId = '2e3d710b-a421-4923-9cb4-b1799c911d89'
    this.accountId = 'amazing-test-account-id'
    this.dynamoDbKey = `/${this.mode}/subgroup/${this.subgroupId}`

    this.itemData = {
      createdAt: this.createdAt,
      createdBy: this.createdBy,
      structureVersion: 80,
      id: this.subgroupId,
      groupId: this.groupId,
      code: 'TEST-TEST-TEST-TEST',
      name: this.name,
      logo: this.logo,
      groupType: 'Company',
      accountId: this.accountId
    }
    this.storedTestItemKey = { key: this.dynamoDbKey, submitted: this.createdAt }

    put.contract.verifyPostconditions = true

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key',
          '#submitted': 'submitted'
        },
        ExpressionAttributeValues: {
          ':key': this.dynamoDbKey,
          ':submitted': this.createdAt
        },
        KeyConditionExpression: '#key = :key and #submitted = :submitted',
        Limit: 1,
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })

  afterEach(async function () {
    this.timeout(15000)
    put.contract.verifyPostconditions = false

    if (this.storedTestItemKey) {
      await this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: {
            key: this.dynamoDbKey,
            submitted: this.createdAt
          }
        })
        .promise()
      console.log(`deleted item ${this.dynamoDbKey}`)
      delete this.dynamoDbKey
    }
  })

  it('works', async function () {
    await put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        subgroupId: this.subgroupId,
        companyId: this.groupId
      },
      this.itemData
    )
    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(1)
    retrieved.Items[0].key.should.equal(this.dynamoDbKey)
  })

  it('should throw a badRequest', async function () {
    const itemData = {
      ...this.itemData,
      id: 'wrong id'
    }
    const exc = await put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        subgroupId: this.subgroupId,
        companyId: this.groupId
      },
      itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const exc = await put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        subgroupId: 'wrong id',
        companyId: 'oops'
      },
      this.itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
