/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamoDbFixture = require('../../../../_DynamodbFixture')
const Slot = require('../../../../../lib/server/business/slot/Slot')
const uuidv4 = require('uuid').v4
const boom = require('@hapi/boom')

const groupId = '2c823261-0363-4749-961f-08b5c5cca713'
const subgroupId = '239cf39e-8a86-47b1-b9f9-82219074adf3'
const slotFlowId = '8e8e5e8d-feec-4019-9493-90e186a1ae90'
const slotSot = '2020-03-30T14:52:10.373Z'
const slotSub = 'slotSub-delete-company-member'
const getSot = '2020-03-30T14:53:28.122Z'
const getSub = 'sub-group-slots-get-test-get'
const getFlowId = '99967ea7-cd32-4a5e-9bbb-09deceb358b0'
const getByIndex = require('../../../../../lib/server/aws/dynamodb/getByIndex')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const config = require('config')

const indexB = {
  indexName: 'Index_B',
  partitionKey: 'partition_key_B',
  sortKey: 'sort_key_B'
}

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamoDbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.getByIndexStub = sinon.stub()
    this.configStub = sinon.stub()
    this.deleteSubgroupMemberProxy = proxyquire('../../../../../lib/server/business/subgroup/deleteSubgroupMember', {
      '../../aws/dynamodb/getByIndex': this.getByIndexStub,
      config: {
        ...config,
        activityPeriod: {
          default: {
            enddate: '2021-05-31'
          }
        }
      }
    })

    this.deleteSubgroupMemberProxy.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    this.deleteSubgroupMemberProxy.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  async function createSlots (mode, dynamodbFixture, slotData) {
    await Promise.all(
      slotData.map(i => {
        const slot = new Slot({
          mode: mode,
          dto: {
            ...i
          },
          sot: i.createdAt,
          sub: i.createdBy
        })

        const databaseItem = slot.toItem()
        databaseItem.flowId = slotFlowId
        databaseItem.submitted = 'actual'
        databaseItem.sort_key_A = 'actual'
        databaseItem.sort_key_B = 'actual'

        return dynamodbFixture.putItem(databaseItem)
      })
    )
  }

  it('should work', async function () {
    this.timeout(5000)
    const baseSlot = {
      createdAt: slotSot,
      createdBy: slotSub,
      structureVersion: 2,
      groupId,
      groupType: 'Company',
      paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678'
    }

    const slotData = [
      { ...baseSlot, id: uuidv4() },
      { ...baseSlot, id: uuidv4(), accountId: 'ezekiel-id' },
      { ...baseSlot, id: uuidv4(), accountId: 'tony-id', subgroupId },
      { ...baseSlot, id: uuidv4(), accountId: 'gerard-id', subgroupId }
    ]

    await createSlots(this.mode, this.dynamodbFixture, slotData)

    const arrivedSlots = await this.dynamodbFixture.hasArrived()
    arrivedSlots.length.should.be.equal(4)

    const slotToDelete = new Slot({
      mode: this.mode,
      dto: {
        ...slotData[2]
      },
      sot: slotSot,
      sub: slotSub
    })

    this.getByIndexStub.returns([slotToDelete.toItem()])

    await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId,
      companyId: groupId,
      accountId: 'tony-id'
    })

    const memberSlots = await getByIndex(this.mode, `/${this.mode}/group/${groupId}/members`, indexB, 'actual')
    console.log('::::::::::::::::::::::::::::::::::', memberSlots)
    const slotsForSubgroup = memberSlots.filter(s => s.data.subgroupId === subgroupId)
    slotsForSubgroup.length.should.equal(1)
  })

  it('should throw not found', async function () {
    this.getByIndexStub.throws(boom.notFound())
    await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId,
      companyId: groupId,
      accountId: 'tony-id'
    }).should.be.rejectedWith(boom.notFound())
  })

  it('should throw a 400', async function () {
    const exc = await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId,
      accountId: 'tony-id'
    }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a 400 when member does not belong to subgroup', async function () {
    const slotToDelete = new Slot({
      mode: this.mode,
      dto: {
        createdAt: slotSot,
        createdBy: slotSub,
        structureVersion: 2,
        groupId,
        groupType: 'Company',
        paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678',
        id: uuidv4(),
        accountId: 'tony-id',
        subgroupId
      },
      sot: slotSot,
      sub: slotSub
    })
    this.getByIndexStub.returns([slotToDelete.toItem()])

    await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId: '3fc4ae37-422c-459a-b746-e1be3d6c6989',
      companyId: groupId,
      accountId: 'tony-id'
    }).should.be.rejectedWith(boom.badRequest())
  })

  it('should throw a 400 when member does not belong to company', async function () {
    const slotToDelete = new Slot({
      mode: this.mode,
      dto: {
        createdAt: slotSot,
        createdBy: slotSub,
        structureVersion: 2,
        groupId,
        groupType: 'Company',
        paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678',
        id: uuidv4(),
        accountId: 'tony-id',
        subgroupId
      },
      sot: slotSot,
      sub: slotSub
    })
    this.getByIndexStub.returns([slotToDelete.toItem()])

    await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId,
      companyId: '3fc4ae37-422c-459a-b746-e1be3d6c6989',
      accountId: 'tony-id'
    }).should.be.rejectedWith(boom.badRequest())
  })

  it('should throw a 403 when period to delete members has passed', async function () {
    const accountId = 'tony-id'
    const slotToDelete = new Slot({
      mode: this.mode,
      dto: {
        createdAt: slotSot,
        createdBy: slotSub,
        structureVersion: 2,
        groupId,
        groupType: 'Company',
        paymentId: 'cecf13c3-01d3-4984-a1c7-225af74fa678',
        id: uuidv4(),
        accountId: 'tony-id',
        subgroupId
      },
      sot: slotSot,
      sub: slotSub
    })
    this.getByIndexStub.returns([slotToDelete.toItem()])

    this.deleteSubgroupMemberProxy = proxyquire('../../../../../lib/server/business/subgroup/deleteSubgroupMember', {
      '../../aws/dynamodb/getByIndex': this.getByIndexStub,
      config: {
        ...config,
        activityPeriod: {
          default: {
            enddate: '2021-01-08'
          }
        }
      }
    })

    await this.deleteSubgroupMemberProxy(getSot, getSub, this.mode, getFlowId, {
      subgroupId,
      companyId: groupId,
      accountId
    }).should.be.rejectedWith(
      boom.forbidden('member delete period has passed', {
        mode: this.mode,
        accountId,
        memberDeleteEndDate: new Date('2021-01-01')
      })
    )
  })
})
