/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamodbFixture = require('../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const getActual = require('../../../../../lib/server/aws/dynamodb/getActual')

const boom = require('@hapi/boom')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const TeamAggregate = require('../../../../../lib/server/business/aggregates/team/TeamAggregate')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.getMemberStub = sinon.stub()
    this.getActualStub = sinon.stub()
    this.deleteSubgroupProxy = proxyquire('../../../../../lib/server/business/subgroup/deleteSubgroup', {
      './getMembers': this.getMemberStub,
      '../../aws/dynamodb/getActual': this.getActualStub
    })
    this.deleteSubgroupProxy.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    this.deleteSubgroupProxy.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  async function createSubgroup (mode, flowId, dynamodbFixture, subgroupData) {
    const dbItem = {
      mode,
      key: `/${mode}/subgroup/${subgroupData.id}`,
      submitted: 'actual',
      submittedBy: subgroupData.createdBy,
      flowId,
      data: subgroupData
    }
    return dynamodbFixture.putItem(dbItem)
  }

  async function createTeamAggregate (mode, flowId, dynamodbFixture, teamAggregateData) {
    const teamAggregate = new TeamAggregate({
      mode,
      dto: {
        ...teamAggregateData
      },
      sot: teamAggregateData.createdAt,
      sub: teamAggregateData.createdBy
    })

    const dbItem = {
      ...teamAggregate.toItem(),
      flowId
    }
    return dynamodbFixture.putItem(dbItem)
  }

  it('should work', async function () {
    this.timeout(5000)
    this.getMemberStub.throws(boom.notFound())
    const sot = '2021-03-31T12:25:26.918Z'
    const sub = 'testDeleteSubgroup'
    const linkId = uuidv4()
    const groupId = uuidv4()
    const subgroupId = uuidv4()
    const flowId = uuidv4()

    const subgroupData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      id: subgroupId,
      groupId: groupId,
      code: 'TEST-CODE-TEST-CODE',
      name: 'subgroup',
      logo: 'VE9PTUFOWVNFQ1JFVFM=',
      groupType: 'Company',
      accountId: 'another-amazing-account-id',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r'
    }

    await createSubgroup(this.mode, flowId, this.dynamodbFixture, subgroupData)

    const arrivedSubgroup = await this.dynamodbFixture.hasArrived()
    arrivedSubgroup.length.should.be.equal(1)

    const teamAggregateData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      teamAggregateId: subgroupId,
      totalSteps: 2500,
      totalDistance: 3000,
      averageSteps: 2754,
      averageDistance: 3485,
      totalTeams: 10,
      companyId: groupId,
      groupType: 'Company',
      name: 'some-kind-of-team-name',
      extendedName: 'some-kind-of-team-name (Peopleware)',
      rank: 9,
      hasLogo: false
    }

    await createTeamAggregate(this.mode, flowId, this.dynamodbFixture, teamAggregateData)

    const arrivedTeamAggregate = await this.dynamodbFixture.hasArrived()
    arrivedTeamAggregate.length.should.be.equal(2)

    await this.deleteSubgroupProxy(sot, sub, this.mode, flowId, {
      subgroupId,
      companyId: groupId
    })

    const subgroupKey = `/${this.mode}/subgroup/${subgroupId}`
    const aggregateKey = `/${this.mode}/teamAggregate/${subgroupId}`

    await getActual(this.mode, subgroupKey).should.be.rejectedWith(boom.notFound())
    await getActual(this.mode, aggregateKey).should.be.rejectedWith(boom.notFound())
  })

  it('should throw a 400', async function () {
    const sot = '2021-03-31T12:25:26.918Z'
    const sub = 'testDeleteSubgroup'
    const groupId = uuidv4()
    const subgroupId = uuidv4()
    const flowId = uuidv4()

    this.getMemberStub.resolves([{ accountId: 'test', name: 'eric debusser' }])

    const exc = await this.deleteSubgroupProxy(sot, sub, this.mode, flowId, {
      subgroupId,
      companyId: groupId
    }).should.be.rejectedWith(boom.badRequest())
    console.log(exc)
  })

  it('should throw not found when subgroup is not found', async function () {
    this.getMemberStub.throws(boom.notFound())
    this.getActualStub.onCall(0).throws(boom.notFound())

    const sot = '2021-03-31T12:25:26.918Z'
    const sub = 'testDeleteSubgroup'
    const groupId = uuidv4()
    const subgroupId = uuidv4()
    const flowId = uuidv4()

    await this.deleteSubgroupProxy(sot, sub, this.mode, flowId, {
      subgroupId,
      companyId: groupId
    }).should.be.rejectedWith(boom.notFound())
  })

  it('should throw not found when teamAggregate is not found', async function () {
    this.getMemberStub.throws(boom.notFound())
    this.getActualStub.onCall(1).throws(boom.notFound())

    const sot = '2021-03-31T12:25:26.918Z'
    const sub = 'testDeleteSubgroup'
    const groupId = uuidv4()
    const subgroupId = uuidv4()
    const flowId = uuidv4()

    await this.deleteSubgroupProxy(sot, sub, this.mode, flowId, {
      subgroupId,
      companyId: groupId
    }).should.be.rejectedWith(boom.notFound())
  })
})
