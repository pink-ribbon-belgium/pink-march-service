/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const initializePayment = require('../../../../../lib/server/business/payment/initializePayment')
const paymentExample = require('./minimal_payment_example.json')
const uuidv4 = require('uuid').v4
const sinon = require('sinon')
const proxyquire = require('proxyquire')

describe(testName, function () {
  beforeEach(async function () {
    initializePayment.contract.verifyPostconditions = true
    this.putPayment = sinon.stub().resolves()
    this.getVoucher = sinon.spy()
    this.subject = proxyquire('../../../../../lib/server/business/payment/initializePayment', {
      './put': this.putPayment,
      '../voucher/get': this.getVoucher
    })
    this.subject.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.subject.contract.verifyPostconditions = false
    initializePayment.contract.verifyPostconditions = false
  })

  it('returns a valid postUrl, when not in error', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()

    const result = await this.subject(
      sot,
      sub,
      mode,
      flowId,
      {
        teamId: '64713b2f-c8d9-430d-be7a-7324c3930c87'
      },
      paymentExample
    )

    this.putPayment.should.be.called()
    console.log(`initializePayment.test 1 ::: expected result => ${result}`)
    result.postUrl.should.be.ok()
  })

  /* TODO do not test the production URL like this - it will leave ugliness in the production DynamoDB table.
          Can we stub this?
  it('returns a production postUrl, when not in error', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = 'production'
    const flowId = uuidv4()

    // Amount is hardcoded in paymentExample if numberofslots changes amount has to be changed manually
    const result = await initializePayment(
      sot,
      sub,
      mode,
      flowId,
      {
        companyId: '64713b2f-c8d9-430d-be7a-7324c3930c87'
      },
      paymentExample
    )
    console.log(`initializePayment.test 1 ::: expected result => ${result.postUrl}`)
    result.postUrl.should.be.ok()
  })
  */

  it('should get voucher', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()

    paymentExample.voucherCode = 'ERICDOERUSTIG'

    const result = await this.subject(
      sot,
      sub,
      mode,
      flowId,
      {
        teamId: uuidv4()
      },
      paymentExample
    )
    console.log(result)
    this.getVoucher.should.be.called()
    console.log(`initializePayment.test 1 ::: expected result => ${result}`)
  })

  it('should throw a bad request', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    paymentExample.voucherCode = 'ERICDOERUSTIG'

    const result = await initializePayment(
      sot,
      sub,
      mode,
      flowId,
      {
        groupId: '64713b2f-c8d9-430d-be7a-7324c3930c87'
      },
      paymentExample
    ).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
