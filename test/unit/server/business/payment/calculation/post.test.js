/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const post = require('../../../../../../lib/server/business/payment/calculation/post')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

describe(testName, function () {
  beforeEach(function () {
    post.contract.verifyPostconditions = true
  })

  afterEach(function () {
    post.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-subTest1'
    const flowId = uuidv4()

    const body = {
      numberOfParticipants: 3,
      groupId: uuidv4()
    }

    const result = await post(sot, sub, mode, flowId, undefined, body)

    result.should.be.an.Object()
    result.numberOfParticipants.should.equal(body.numberOfParticipants)
  })

  it('should call getVoucher when voucherCode is provided', async function () {
    const getVoucherSpy = sinon.spy()
    const postProxy = proxyquire('../../../../../../lib/server/business/payment/calculation/post', {
      '../../voucher/get': getVoucherSpy
    })

    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-subTest1'
    const flowId = uuidv4()

    const body = {
      numberOfParticipants: 3,
      groupId: uuidv4(),
      voucherCode: 'SOMECODE14'
    }

    await postProxy(sot, sub, mode, flowId, undefined, body)

    getVoucherSpy.should.be.called()
  })
})
