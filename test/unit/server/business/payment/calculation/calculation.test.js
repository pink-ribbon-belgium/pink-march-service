/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const GroupType = require('../../../../../../lib/api/group/GroupType')
const PaymentStatus = require('../../../../../../lib/api/payment/PaymentStatus')
const boom = require('@hapi/boom')

const sot = '2020-03-08T11:29:43.572Z'
const sub = 'calculation-test'
const beforeSot = '2018-03-08T11:29:43.572Z'
const afterSot = '2022-03-08T11:29:43.572Z'

const groupId = 'c4ba7719-2232-45b3-8919-08e00dd69648'
const flowId = uuidv4()

const voucherCode = 'PPW20'
const voucherBase = {
  voucherCode,
  expiryDate: afterSot,
  createdAt: '2016-03-08T11:29:43.572Z',
  createdBy: 'someone',
  structureVersion: 22,
  reusable: true
}

describe(testName, function () {
  beforeEach(async function () {
    this.getSlotsForGroupStub = sinon.stub()
    this.calculationProxy = proxyquire('../../../../../../lib/server/business/payment/calculation/calculation', {
      '../../group/slots/get': this.getSlotsForGroupStub
    })
    this.calculationProxy.contract.verifyPostconditions = true
    this.mode = `automated-test-${uuidv4()}`
  })

  afterEach(async function () {
    this.calculationProxy.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 2,
      groupId
    }
    this.getSlotsForGroupStub.resolves({ totalSlots: 3 })

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    console.log(result)
    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(8)
    result.voucherIsValid.should.be.true()
  })

  it('should work with 4 existing slots', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 2,
      groupId
    }
    this.getSlotsForGroupStub.resolves({ totalSlots: 4 })

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    console.log(result)
    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(8)
    result.voucherIsValid.should.be.true()
  })

  it('should work with 5 existing slots', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 2,
      groupId
    }
    this.getSlotsForGroupStub.resolves({ totalSlots: 5 })

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    console.log(result)
    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(8)
    result.voucherIsValid.should.be.true()
  })

  it('should work with 10 existing slots', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 2,
      groupId
    }
    this.getSlotsForGroupStub.resolves({ totalSlots: 10 })

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    console.log(result)
    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(8)
    result.voucherIsValid.should.be.true()
  })

  it('should have 8 as unitPrice with teams of at least 5 participants', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 5,
      groupId
    }
    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(8)
    result.voucherIsValid.should.be.true()
  })

  it('should set discount to voucherAmount', async function () {
    const voucher = { ...voucherBase, amount: 20 }
    const params = {
      structureVersion: 1,
      numberOfParticipants: 4,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.discount.should.equal(voucher.amount)
    result.voucherIsValid.should.be.true()
  })

  it('should calculate discount by percentage', async function () {
    const voucher = { ...voucherBase, percentage: 0.2 }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.discount.should.equal(result.numberOfParticipants * result.unitPrice * voucher.percentage)
    result.voucherIsValid.should.be.true()
  })

  it('should return exactly 0 with 100% discount', async function () {
    const voucher = { ...voucherBase, percentage: 1 }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.total.should.equal(0)
    result.voucherIsValid.should.be.true()
  })

  it('should return exactly 0 with a very large discount amount', async function () {
    const voucher = { ...voucherBase, amount: 1e12 }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }
    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.total.should.equal(0)
    result.voucherIsValid.should.be.true()
  })

  it('does not give a discount with an invalid voucher', async function () {
    const voucher = { ...voucherBase, amount: 1, expiryDate: beforeSot }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.discount.should.equal(0)
    result.voucherIsValid.should.be.true()
  })

  it('a voucher is valid on the expiryDate', async function () {
    const voucher = { ...voucherBase, amount: 1, expiryDate: sot }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.discount.should.equal(1)
    result.voucherIsValid.should.be.true()
  })

  it('should not give discount when non-reusablevoucher is already used', async function () {
    const voucher = {
      ...voucherBase,
      reusable: false,
      percentage: 0.2
    }

    this.getByIndex = sinon.stub()

    this.calculationProxy = proxyquire('../../../../../../lib/server/business/payment/calculation/calculation', {
      './../../../../server/aws/dynamodb/getByIndex': this.getByIndex,
      '../../group/slots/get': this.getSlotsForGroupStub
    })

    this.getByIndex.returns([
      {
        data: {
          amount: 120,
          createdAt: '2020-03-09T14:08:37.926Z',
          createdBy: 'some-account',
          groupId: '49b14d65-5505-4489-8861-58e4ebe58d49',
          groupType: GroupType.teamType,
          numberOfSlots: 20,
          paymentId: 'dff1a58c-b266-4e74-8c50-1a07db332c27',
          paymentInitiatedAt: '2020-03-09T12:58:31.132Z',
          paymentStatus: PaymentStatus.INITIALIZED,
          structureVersion: 1
        },
        key: '/production/payment/dff1a58c-b266-4e74-8c50-1a07db332c27',
        mode: 'production',
        partition_key_A: `/production/voucher/${voucherCode}/payments`,
        partition_key_B: '/production/status/Initialized/payments',
        partition_key_C: '/production/team/49b14d65-5505-4489-8861-58e4ebe58d49/payments',
        sort_key_A: '2020-03-09T14:08:37.926Z',
        sort_key_B: '2020-03-09T14:08:37.926Z',
        sort_key_C: '2020-03-09T14:08:37.926Z',
        submitted: '2020-03-09T14:08:37.926Z',
        submittedBy: 'auth0%7C5e663c74a440d22ed314b022'
      },
      {
        data: {
          amount: 120,
          createdAt: '2020-03-09T14:08:37.926Z',
          createdBy: 'some-account',
          groupId: '49b14d65-5505-4489-8861-58e4ebe58d49',
          groupType: GroupType.teamType,
          numberOfSlots: 20,
          paymentId: 'dff1a58c-b266-4e74-8c50-1a07db332c27',
          paymentInitiatedAt: '2020-03-09T12:58:31.132Z',
          paymentStatus: PaymentStatus.PAYMENT_REQUESTED,
          structureVersion: 1
        },
        key: '/production/payment/dff1a58c-b266-4e74-8c50-1a07db332c27',
        mode: 'production',
        partition_key_A: `/production/voucher/${voucherCode}/payments`,
        partition_key_B: '/production/status/9/payments',
        partition_key_C: '/production/team/49b14d65-5505-4489-8861-58e4ebe58d49/payments',
        sort_key_A: '2020-03-09T14:08:37.926Z',
        sort_key_B: '2020-03-09T14:08:37.926Z',
        sort_key_C: '2020-03-09T14:08:37.926Z',
        submitted: '2020-03-09T14:08:37.926Z',
        submittedBy: 'auth0%7C5e663c74a440d22ed314b022'
      }
    ])

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    result.should.be.an.Object()
    result.discount.should.equal(0)
    result.voucherIsValid.should.be.false()
  })

  it('should give discount when non-reusablevoucher is not used', async function () {
    const voucher = {
      ...voucherBase,
      reusable: false,
      percentage: 0.2
    }

    const params = {
      structureVersion: 1,
      numberOfParticipants: 1,
      groupId,
      voucherCode
    }

    this.getSlotsForGroupStub.throws(boom.notFound)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, voucher)

    console.log(result)
    result.should.be.an.Object()
    result.discount.should.equal(2)
    result.voucherIsValid.should.be.true()
  })

  it('should throw badRequest', async function () {
    const params = {
      structureVersion: 1,
      numberOfParticipants: 2,
      groupId
    }
    this.getSlotsForGroupStub.throws(boom.badRequest)

    const result = await this.calculationProxy(sot, sub, this.mode, flowId, params, undefined)

    console.log(result)
    result.should.be.an.Object()
    result.slotsToAdd.should.equal(params.numberOfParticipants)
    result.unitPrice.should.equal(10)
    result.voucherIsValid.should.be.true()
  })
})
