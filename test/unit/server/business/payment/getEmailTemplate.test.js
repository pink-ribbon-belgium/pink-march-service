/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const GetEmailTemplate = require('../../../../../lib/server/business/payment/getEmailTemplate')

describe(testName, function () {
  beforeEach(function () {
    GetEmailTemplate.contract.verifyPostconditions = true
  })

  afterEach(function () {
    GetEmailTemplate.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const templateName = 'confirmation_individual'
    const result = await GetEmailTemplate(templateName, 'NL')

    result.should.be.an.Object()
    result.flow.should.equal('individual')
    result.language.should.equal('NL')
  })
})
