/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/payment/get')
const generateGetTest = require('../_generateGetTest')
const PaymentStatus = require('../../../../../lib/api/payment/PaymentStatus')
const x = require('cartesian')

const paymentId = 'b084f142-08b1-4b90-839e-7a0380a1ad60'
const groupId = '8591fa23-3460-43b8-8493-e55c3028448b'
const voucherCode = 'VOUCHER0987654321'

const cases = x({
  groupType: ['Company', 'Team'],
  paymentStatus: [
    PaymentStatus.INITIALIZED,
    PaymentStatus.ORDER_STORED,
    PaymentStatus.STANDBY,
    PaymentStatus.PAYMENT_REQUESTED
  ]
})

describe(testName, function () {
  cases.forEach(c => {
    describe(`${c.groupType} - ${c.paymentStatus}`, function () {
      generateGetTest(
        get,
        (sot, sub) => ({
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          paymentInitiatedAt: sot,
          paymentId,
          groupId,
          voucherCode,
          groupType: c.groupType,
          numberOfSlots: 20,
          amount: 46.25,
          paymentStatus: c.paymentStatus
        }),
        mode => `/${mode}/payment/${paymentId}`,
        { paymentId: 'paymentId' },
        () => ({
          paymentId
        }),
        () => ({
          paymentId: 'ab6456e8-dd90-43c0-b4a7-9bec28cc65ed'
        }),
        () => ({
          groupId
        }),
        () => ({
          versions: `/I/payment/${paymentId}/versions`,
          group: `/I/${c.groupType.toLowerCase()}/${groupId}`,
          voucher: `/I/voucher/${voucherCode}`
        })
      )
    })
  })
})
