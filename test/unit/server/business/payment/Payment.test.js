/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const Payment = require('../../../../../lib/server/business/payment/Payment')
const PaymentDTO = require('../../../../../lib/api/payment/PaymentDTO')
const Mode = require('../../../../../lib/ppwcode/Mode')

const KwargsExample = {
  mode: Mode.example,
  dto: {
    ...PaymentDTO.exampleDynamodb
  }
}

describe(testName, function () {
  beforeEach(function () {
    Payment.contract.verifyPostconditions = true
  })

  afterEach(function () {
    Payment.contract.verifyPostconditions = false
  })
  it(' should work', function () {
    const result = new Payment(KwargsExample)
    console.log(result)
    result.should.upholdInvariants()
  })

  it('should return links', function () {
    const links = {
      versions: `/I/payment/${KwargsExample.dto.paymentId}/versions`,
      group: `/I/${KwargsExample.dto.groupType.toLowerCase()}/${KwargsExample.dto.groupId}`,
      voucher: `/I/voucher/${KwargsExample.dto.voucherCode}`
    }
    const result = new Payment(KwargsExample)
    result.getLinks().should.deepEqual(links)
  })
})
