/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const Payment = require('../../../../../lib/server/business/payment/Payment')
const put = require('../../../../../lib/server/business/payment/put')
const PaymentStatus = require('../../../../../lib/api/payment/PaymentStatus')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()
    put.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    if (this.storedTestItemKey) {
      await this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: {
            key: this.storedTestItemKey.key,
            submitted: this.storedTestItemKey.submitted
          }
        })
        .promise()
      console.log(`deleted item ${util.inspect(this.storedTestItemKey)}`)
      delete this.storedTestItemKey
    }
  })

  it('should work', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-subTest1'
    const groupId = uuidv4()
    const groupType = 'Team'
    const voucherCode = 'LIBELLE2020'
    const flowId = uuidv4()

    const pathParameters = {
      paymentId: uuidv4()
    }

    const body = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 7452745,
      unknownProperty: {
        extraValue: 'extra value'
      },
      paymentInitiatedAt: sot,
      paymentId: pathParameters.paymentId,
      groupId: groupId,
      groupType: groupType,
      voucherCode: voucherCode,
      numberOfSlots: 15,
      amount: 26.75,
      paymentStatus: PaymentStatus.PAYMENT_DELETION_PENDING
    }

    const dynamodbKey = `/${mode}/payment/${pathParameters.paymentId}`

    this.storedTestItemKey = { key: dynamodbKey, submitted: sot }

    const payment = new Payment({
      mode: mode,
      dto: { ...body },
      sot: sot,
      sub: sub
    })

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': payment.key
        },
        KeyConditionExpression: '#key = :key',
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }

    await put(
      sot,
      sub,
      mode,
      flowId,
      {
        paymentId: pathParameters.paymentId
      },
      body
    )

    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(1)
    retrieved.Items[0].key.should.equal(dynamodbKey)
  })
  it('should throw a badRequest', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-subTest1'
    const groupType = 'Team'
    const voucherCode = 'LIBELLE2020'
    const flowId = uuidv4()

    const pathParameters = {
      paymentId: uuidv4()
    }
    const body = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 7452745,
      unknownProperty: {
        extraValue: 'extra value'
      },
      paymentInitiatedAt: sot,
      paymentId: pathParameters.paymentId,
      groupId: groupType,
      groupType: groupType,
      voucherCode: voucherCode,
      numberOfSlots: 15,
      amount: 26.75,
      paymentStatus: 'fail'
    }
    const exc = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        paymentId: pathParameters.paymentId
      },
      body
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-subTest3'
    const groupType = 'Team'
    const groupId = uuidv4()
    const voucherCode = 'LIBELLE2020'
    const flowId = uuidv4()

    const pathParameters = {
      paymentId: uuidv4()
    }
    const body = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 7452745,
      unknownProperty: {
        extraValue: 'extra value'
      },
      paymentInitiatedAt: sot,
      paymentId: pathParameters.paymentId,
      groupId: groupId,
      groupType: groupType,
      voucherCode: voucherCode,
      numberOfSlots: 15,
      amount: 26.75,
      paymentStatus: 'fail'
    }
    const exc = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        groupId: pathParameters.paymentId
      },
      body
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
