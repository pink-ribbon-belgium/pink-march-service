/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const processPayment = require('../../../../../lib/server/business/payment/processPayment')
const Boom = require('@hapi/boom')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const uuidv4 = require('uuid').v4
const Ogone = require('../../../../../lib/ppwcode/ogone/Ogone')
const GroupType = require('../../../../../lib/api/group/GroupType')
const sendGrid = require('@sendgrid/mail')
const ExceptionConditionViolation = require('@toryt/contracts-iv/lib/IV/ExceptionConditionViolation')
const should = require('should')

describe(testName, function () {
  beforeEach(async function () {
    this.sandbox = sinon.createSandbox()
    this.getPaymentStub = this.sandbox.stub(Ogone.prototype, 'getPayment')
    this.getPayment = sinon.stub()
    this.putPayment = sinon.spy()
    this.writeSlot = sinon.spy()
    this.getSlots = sinon.stub()
    this.getPreferences = sinon.stub()
    this.getEmailTemplate = sinon.stub()
    this.getSlotsForGroup = sinon.stub()
    this.getAccountProfile = sinon.stub()
    this.writeAccountAggregate = sinon.stub()
    this.getAccountAggregate = sinon.stub()
    this.writeTeamAggregate = sinon.stub()
    this.getTeamAggregate = sinon.stub()
    this.getCompany = sinon.stub()
    this.getGroupProfile = sinon.stub()
    this.writeCompanyAggregate = sinon.stub()
    this.getCompanyAggregate = sinon.stub()
    this.getAccount = sinon.stub()
    this.getTeam = sinon.stub()
    this.putTeam = sinon.stub()
    this.putCompany = sinon.stub()
    this.createShortLink = sinon.stub()

    this.paymentProxy = proxyquire('../../../../../lib/server/business/payment/processPayment', {
      './get': this.getPayment,
      './put': this.putPayment,
      '../slot/write': this.writeSlot,
      '../slot/getSlotsByPayment': this.getSlots,
      './getEmailTemplate': this.getEmailTemplate,
      '../account/preferences/get': this.getPreferences,
      '../group/slots/get': this.getSlotsForGroup,
      '../account/profile/get': this.getAccountProfile,
      '../../../server/business/group/profile/get': this.getGroupProfile,
      '../../../server/business/company/get': this.getCompany,
      '../../../server/business/company/put': this.putCompany,
      '../aggregates/account/get': this.getAccountAggregate,
      '../aggregates/account/write': this.writeAccountAggregate,
      '../aggregates/team/get': this.getTeamAggregate,
      '../aggregates/team/write': this.writeTeamAggregate,
      '../aggregates/company/write': this.writeCompanyAggregate,
      '../aggregates/company/get': this.getCompanyAggregate,
      '../account/get': this.getAccount,
      '../team/get': this.getTeam,
      '../team/put': this.putTeam,
      '../group/shortLink/createShortLink': this.createShortLink
    })
    this.paymentProxy.contract.verifyPostconditions = true
    this.sandbox.stub(sendGrid, 'send')

    this.getPreferences.returns({
      language: 'nl-BE',
      verifiedEmail: { email: 'someone@somewhere.com', verified: false }
    })
    this.getEmailTemplate.returns({
      subject: 'some subject',
      body: '<!DOCTYPE html>  <html>  <body>  <p><strong>This is a test</strong></p>  </body>  </html>'
    })
    this.getSlots.returns([])
    this.getPaymentStub.resolves({ status: '9' })
    this.getSlotsForGroup.returns({
      totalSlots: 0,
      takenSlots: 0,
      availableSlots: 0
    })
    this.getAccountProfile.returns({
      firstName: 'Eric',
      lastName: 'Rustig'
    })

    this.getAccount.returns({
      sub: 'auth0|601bf7978e96f60068464984'
    })

    this.getGroupProfile.returns({
      name: 'De Stappers',
      logo: false
    })

    this.getCompany.returns({
      zip: '2221',
      groupType: 'Company',
      unit: 'vivium',
      contactEmail: 'test@test.be'
    })

    this.getTeam.returns({
      groupType: 'Team'
    })

    this.createShortLink.returns({
      linkId: '12c8b379-f581-4fb5-b2c7-1acc60833bed',
      shortLink: 'https://test.be'
    })

    this.putTeam.resolves()
    this.putCompany.resolves()

    this.getAccountAggregate.throws(Boom.notFound())
    this.writeAccountAggregate.resolves()

    this.getTeamAggregate.throws(Boom.notFound())
    this.writeTeamAggregate.resolves()

    this.getCompanyAggregate.throws(Boom.notFound())
    this.writeCompanyAggregate.resolves()
  })

  afterEach(async function () {
    this.timeout(15000)
    this.sandbox.restore()
    processPayment.contract.verifyPostconditions = false
  })

  it('should call stubs (Team)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeam.returns({
      groupType: 'Team',
      id: payment.groupId
    })

    const expectedTeamData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      id: payment.groupId,
      groupType: GroupType.teamType,
      linkId: '12c8b379-f581-4fb5-b2c7-1acc60833bed',
      shortUrl: 'https://test.be'
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal('https://test.be')

    this.putTeam.should.be.calledWithMatch(sot, sub, mode, flowId, { teamId: payment.groupId }, expectedTeamData)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeamAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs (Team) - return existing team', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeam.returns({
      groupType: 'Team',
      linkId: '12c8b379-f581-4fb5-b2c7-1acc60833bed',
      shortUrl: 'https://existing-team.be'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal('https://existing-team.be')
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeamAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs (Team) - return joinLink when create shortlink fails', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const joinLink = `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.teamType.toLowerCase()}/${uuidv4()}`

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeam.returns({
      id: payment.groupId,
      groupType: 'Team',
      joinLink
    })

    this.createShortLink.throws(Boom.notFound())

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    const expectedTeamData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      id: payment.groupId,
      groupType: GroupType.teamType,
      shortUrl: joinLink
    }

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal(joinLink)

    this.putTeam.should.be.calledWithMatch(sot, sub, mode, flowId, { teamId: payment.groupId }, expectedTeamData)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeamAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs (Team) - return shortUrl when create linkId is present', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const linkId = uuidv4()
    const shortUrl = 'https://test.be'

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeam.returns({
      id: payment.groupId,
      groupType: 'Team',
      linkId,
      shortUrl
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal(shortUrl)

    this.putTeam.should.not.be.called()
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeamAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs (Team) - return no shortUrl or joinLink when team is an individual', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 10,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeam.returns({
      id: payment.groupId,
      groupType: 'Team'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(0)

    this.putTeam.should.not.be.called()
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs for individual', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 10,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(0)
    should(result.joinLink).equal(null)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeam.should.be.called()
  })

  it('should call stubs (Company)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: GroupType.companyType,
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const expectedCompanyData = {
      zip: '2221',
      groupType: 'Company',
      unit: 'vivium',
      contactEmail: 'test@test.be',
      linkId: '12c8b379-f581-4fb5-b2c7-1acc60833bed',
      shortUrl: 'https://test.be'
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(10)
    result.joinLink.should.equal('https://test.be')

    this.putCompany.should.be.calledWithMatch(
      sot,
      sub,
      mode,
      flowId,
      { companyId: payment.groupId },
      expectedCompanyData
    )

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.writeCompanyAggregate.should.be.called()
    this.getCompanyAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.getAccountProfile.should.not.be.called()
    this.getAccountAggregate.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()
    this.getTeamAggregate.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
    this.getCompany.should.be.called()
  })

  it('should call stubs (Company) - return existing company', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: GroupType.companyType,
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getCompany.returns({
      zip: '2221',
      groupType: 'Company',
      unit: 'vivium',
      contactEmail: 'test@test.be',
      linkId: 'c4c1ba02-5e7a-4933-b3b5-351b01d0cda4',
      shortUrl: 'https://existing-link.be'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(10)
    result.joinLink.should.equal('https://existing-link.be')
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.writeCompanyAggregate.should.be.called()
    this.getCompanyAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.getAccountProfile.should.not.be.called()
    this.getAccountAggregate.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()
    this.getTeamAggregate.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
    this.getCompany.should.be.called()
  })

  it('should call stubs (Company) - return joinLink when create shortlink fails', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const joinLink = `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.companyType.toLowerCase()}/${uuidv4()}`

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: GroupType.companyType,
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.createShortLink.throws(Boom.notFound())

    this.getCompany.returns({
      zip: '2221',
      groupType: 'Company',
      unit: 'vivium',
      contactEmail: 'test@test.be',
      joinLink
    })

    const expectedCompanyData = {
      zip: '2221',
      groupType: 'Company',
      unit: 'vivium',
      contactEmail: 'test@test.be',
      shortUrl: joinLink
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(10)
    result.joinLink.should.equal(joinLink)

    this.putCompany.should.be.calledWithMatch(
      sot,
      sub,
      mode,
      flowId,
      { companyId: payment.groupId },
      expectedCompanyData
    )

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.writeCompanyAggregate.should.be.called()
    this.getCompanyAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.getAccountProfile.should.not.be.called()
    this.getAccountAggregate.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()
    this.getTeamAggregate.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
    this.getCompany.should.be.called()
  })

  it('should call stubs (should not create TeamAggregate if already exists)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'facebook|601bf7978e96f60068464984'
    })

    this.getTeamAggregate.returns({ name: 'some-name', rank: 1, totalTeams: 7 })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()
    this.getTeamAggregate.should.be.called()
    this.getTeam.should.be.called()

    this.getGroupProfile.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
  })

  it('should call stubs (should not create TeamAggregate when only 1 slot is payed and no existing slots)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 8,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getTeamAggregate.returns({ name: 'some-name', rank: 1, totalTeams: 7 })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(0)
    should(result.joinLink).equal(null)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.called()

    this.getTeamAggregate.should.not.be.called()
    this.getGroupProfile.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
  })

  it('should call stubs (should create AccountAggregate & TeamAggregate when multiple slot are payed and no existing slots)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 2,
      amount: 8,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getSlotsForGroup.returns({
      totalSlots: 0,
      takenSlots: 0,
      availableSlots: 0
    })

    const expectedTeamAggregateData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      teamAggregateId: payment.groupId,
      totalSteps: 0,
      totalDistance: 0,
      totalTeams: 9999999,
      companyId: undefined,
      name: 'De Stappers',
      rank: 9999999,
      groupType: payment.groupType,
      extendedName: undefined,
      hasLogo: false
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(1)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.be.called()
    this.getAccountAggregate.calledOnce.should.be.true()
    this.writeAccountAggregate.should.be.called()

    this.getTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()

    this.writeTeamAggregate.should.be.calledWithMatch(sot, sub, mode, flowId, expectedTeamAggregateData)
  })

  it('should call stubs (should create TeamAggregate when only 1 slot is payed and 1 existing slots)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const groupId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId,
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 8,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getSlotsForGroup.returns({
      totalSlots: 1,
      takenSlots: 1,
      availableSlots: 0
    })

    this.getAccountAggregate.resolves({
      groupId,
      totalSteps: 1000,
      totalDistance: 820
    })

    const expectedTeamAggregateData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      teamAggregateId: payment.groupId,
      totalSteps: 1000,
      totalDistance: 820,
      totalTeams: 9999999,
      companyId: undefined,
      name: 'De Stappers',
      rank: 9999999,
      groupType: payment.groupType,
      extendedName: undefined,
      hasLogo: false
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(1)
    result.joinLink.should.equal('https://test.be')
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.not.be.called()
    this.getAccountAggregate.calledTwice.should.be.true()
    this.writeAccountAggregate.should.not.be.called()

    this.getTeamAggregate.should.be.called()
    this.getTeam.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeTeamAggregate.should.be.called()

    this.writeTeamAggregate.should.be.calledWithMatch(sot, sub, mode, flowId, expectedTeamAggregateData)
  })

  it('should call stubs (do not create AccountAggregate if already exists)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const groupId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId,
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getAccountAggregate.returns({ groupId, name: 'some-name', rank: 1, totalParticipants: 7 })

    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal('https://test.be')
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.not.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.not.called()
  })

  it('should call stubs (update AccountAggregate if already exists and groupId is not present)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const groupId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId,
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getAccountAggregate.returns({ name: 'some-name', rank: 1, totalParticipants: 7 })

    const expectedAccountAggregate = {
      name: 'some-name',
      rank: 9999999,
      totalParticipants: 9999999,
      groupId,
      groupType: payment.groupType,
      previousLevel: 1,
      acknowledged: false
    }

    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
    result.joinLink.should.equal('https://test.be')
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getAccountProfile.should.not.called()
    this.getAccountAggregate.should.be.called()
    this.writeAccountAggregate.should.be.calledWith(sot, sub, mode, flowId, expectedAccountAggregate)
  })

  it('should fail if all puts fail', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const stubError = new Error('this is the stub error')
    const writeSlotStub = this.sandbox.stub().rejects(stubError)

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)

    // override existing proxy
    this.paymentProxy = proxyquire('../../../../../lib/server/business/payment/processPayment', {
      './get': this.getPayment,
      './put': this.putPayment,
      '../slot/write': writeSlotStub,
      '../slot/getSlotsByPayment': this.getSlots,
      './getEmailTemplate': this.getEmailTemplate,
      '../account/preferences/get': this.getPreferences,
      '../group/slots/get': this.getSlotsForGroup,
      '../account/profile/get': this.getAccountProfile,
      '../aggregates/account/get': this.getAccountAggregate,
      '../aggregates/account/write': this.writeAccountAggregate,
      '../../../server/business/company/put': this.putCompany,
      '../../../server/business/company/get': this.getCompany,
      '../team/put': this.putTeam,
      '../team/get': this.getTeam
    })
    this.paymentProxy.contract.verifyPostconditions = true

    const violation = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejectedWith(ExceptionConditionViolation)
    const err = violation.exception
    console.log(err)
    err.isBoom.should.be.true()
    err.output.statusCode.should.equal(500)
  })

  it('should work if some puts fail', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const stubError = new Error('this is the stub error')
    const writeSlotStub = this.sandbox
      .stub()
      .resolves('succesful put')
      .onThirdCall()
      .rejects(stubError)

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)

    // override existing proxy
    this.paymentProxy = proxyquire('../../../../../lib/server/business/payment/processPayment', {
      './get': this.getPayment,
      './put': this.putPayment,
      '../slot/write': writeSlotStub,
      '../slot/getSlotsByPayment': this.getSlots,
      './getEmailTemplate': this.getEmailTemplate,
      '../account/preferences/get': this.getPreferences,
      '../group/slots/get': this.getSlotsForGroup,
      '../account/profile/get': this.getAccountProfile,
      '../../../server/business/group/profile/get': this.getGroupProfile,
      '../aggregates/account/get': this.getAccountAggregate,
      '../aggregates/account/write': this.writeAccountAggregate,
      '../aggregates/team/get': this.getTeamAggregate,
      '../aggregates/team/write': this.writeTeamAggregate,
      '../account/get': this.getAccount,
      '../team/get': this.getTeam,
      '../team/put': this.putTeam,
      '../group/shortLink/createShortLink': this.createShortLink
    })
    this.paymentProxy.contract.verifyPostconditions = true

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    this.getPayment.should.be.called()
    this.getTeam.should.be.called()
    this.putPayment.should.be.called()
    writeSlotStub.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getAccount.should.be.called()
    result.freeSlots.should.equal(8) // one for the admin, and one failed
    result.joinLink.should.equal('https://test.be')
  })

  it('should work with a few slots', async function () {
    this.timeout(5000)
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    console.log(`Mode: => ${mode}`)

    // override existing proxy
    this.paymentProxy = proxyquire('../../../../../lib/server/business/payment/processPayment', {
      './get': this.getPayment,
      './put': this.putPayment,
      '../slot/write': this.writeSlot,
      '../slot/getSlotsByPayment': this.getSlots,
      './getEmailTemplate': this.getEmailTemplate,
      '../account/preferences/get': this.getPreferences,
      '../group/slots/get': this.getSlotsForGroup,
      '../account/profile/get': this.getAccountProfile,
      '../../../server/business/group/profile/get': this.getGroupProfile,
      '../../../server/business/company/get': this.getCompany,
      '../../../server/business/company/put': this.putCompany,
      '../aggregates/account/get': this.getAccountAggregate,
      '../aggregates/account/write': this.writeAccountAggregate,
      '../aggregates/team/get': this.getTeamAggregate,
      '../aggregates/team/write': this.writeTeamAggregate,
      '../aggregates/company/write': this.writeCompanyAggregate,
      '../aggregates/company/get': this.getCompanyAggregate,
      '../account/get': this.getAccount,
      '../team/get': this.getTeam,
      '../team/put': this.putTeam,
      '../group/shortLink/createShortLink': this.createShortLink
    })
    this.paymentProxy.contract.verifyPostconditions = true

    this.getAccountAggregate.resolves()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'auth0|601bf7978e96f60068464984'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(9)
    result.joinLink.should.equal('https://test.be')

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getAccount.should.be.called()
  })

  it('should work with a lot of slots', async function () {
    this.timeout(5000)
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    console.log(`Mode: => ${mode}`)

    // override existing proxy
    this.paymentProxy = proxyquire('../../../../../lib/server/business/payment/processPayment', {
      './get': this.getPayment,
      './put': this.putPayment,
      '../slot/write': this.writeSlot,
      '../slot/getSlotsByPayment': this.getSlots,
      './getEmailTemplate': this.getEmailTemplate,
      '../account/preferences/get': this.getPreferences,
      '../group/slots/get': this.getSlotsForGroup,
      '../account/profile/get': this.getAccountProfile,
      '../../../server/business/group/profile/get': this.getGroupProfile,
      '../../../server/business/company/get': this.getCompany,
      '../../../server/business/company/put': this.putCompany,
      '../aggregates/account/get': this.getAccountAggregate,
      '../aggregates/account/write': this.writeAccountAggregate,
      '../aggregates/team/get': this.getTeamAggregate,
      '../aggregates/team/write': this.writeTeamAggregate,
      '../aggregates/company/write': this.writeCompanyAggregate,
      '../aggregates/company/get': this.getCompanyAggregate,
      '../account/get': this.getAccount,
      '../team/get': this.getTeam,
      '../team/put': this.putTeam,
      '../group/shortLink/createShortLink': this.createShortLink
    })
    this.paymentProxy.contract.verifyPostconditions = true

    this.getAccountAggregate.resolves()

    this.getSlotsForGroup.returns({
      totalSlots: 10,
      takenSlots: 1,
      availableSlots: 9
    })

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 101,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'auth0|601bf7978e96f60068464984'
    })

    this.getTeam.returns({
      groupType: 'Team',
      linkId: '4d1629fafc6549238151cea7fec5c4e5',
      shortUrl: 'https://team/short-link.be'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(101)
    result.joinLink.should.equal('https://team/short-link.be')

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
    this.getAccount.should.be.called()
  })

  it('should not call Ogone with amount 0', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'FREEVOUCHER',
      numberOfSlots: 10,
      amount: 0,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'google-oauth2|601bf7978e96f60068464984'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(9)

    this.getPaymentStub.should.not.be.called()
  })

  it('should not set paymentstatus if ogone does not return a result', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      numberOfSlots: 10,
      amount: 60,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)

    this.getPaymentStub.resolves(undefined)

    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejectedWith(Boom.forbidden())
    console.log(result)

    this.getPaymentStub.should.be.called()
  })

  it('should call (individual)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'windowslive|601bf7978e96f60068464984'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(0)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.calledWith('confirmation_individual', 'NL')
    this.getPreferences.should.be.called()
  })

  it('should call (individual - FR)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    // override defaults from before test
    this.getPreferences.returns({
      language: 'fr-BE',
      verifiedEmail: { email: 'someone@somewhere.com', verified: false }
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(0)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.calledWith('confirmation_individual', 'FR')
    this.getPreferences.should.be.called()
  })

  it('should call (company)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(10)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.calledWith('confirmation_company', 'NL')
  })

  it('should call (team)', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(9)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.calledWith('confirmation_team', 'NL')
    this.getPreferences.should.be.called()
  })

  it('should return correct shortLink and free slots', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 14,
      amount: 11200,
      paymentStatus: 'Initiated'
    }

    this.getPayment.resolves(payment)
    this.getSlots.resolves([])
    this.getPaymentStub.resolves({ status: '9' })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(13)
    result.joinLink.should.equal('https://test.be')

    const expectedResult = {
      joinLink: 'https://test.be',
      freeSlots: payment.numberOfSlots - 1
    }

    result.should.deepEqual(expectedResult)
  })

  it('should throw forbidden error when invalid orderId', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getPayment.resolves(payment)
    this.getSlots.resolves(['test-slot', 'test-slot-2'])
    this.getPaymentStub.throws()

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejectedWith(Boom.forbidden())
    console.log(result)
  })

  it('should throw forbidden when payment already has slots', async function () {
    this.getPaymentStub.resolves({ status: '9' })
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getPayment.resolves(payment)
    this.getSlots.resolves(['test-slot', 'test-slot-2'])

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejectedWith(Boom.forbidden())
    console.log(result)
  })

  it('should throw forbidden when ogone payment status differs from 9', async function () {
    this.getPaymentStub.resolves({ status: '7' })
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getPayment.resolves(payment)
    this.getSlots.resolves(['test-slot', 'test-slot-2'])

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejectedWith(Boom.forbidden())
    console.log(result)
  })

  it('should replace JOINLINK and TOOLBOXLINK', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    this.getEmailTemplate.resolves({
      subject: '',
      body: '[JOINLINK][TOOLBOXLINK]'
    })
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(10)

    sendGrid.send.should.be.called()
    sendGrid.send
      .getCall(0)
      .args[0].html.includes('[JOINLINK]')
      .should.be.false()
    sendGrid.send
      .getCall(0)
      .args[0].html.includes('[TOOLBOXLINK]')
      .should.be.false()
  })

  it('should replace AUTHMETHOD', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)

    this.getEmailTemplate.resolves({
      subject: '',
      body: '[AUTHMETHOD]'
    })
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(payment.numberOfSlots - 1)

    sendGrid.send.should.be.called()
    sendGrid.send
      .getCall(0)
      .args[0].html.includes('[AUTHMETHOD]')
      .should.be.false()
  })

  it('should replace AUTHMETHOD by Default when something went wrong', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)

    this.getEmailTemplate.resolves({
      subject: '',
      body: '[AUTHMETHOD]'
    })
    console.log(paymentId)

    this.getAccount.returns({
      sub: 'something wrong'
    })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    result.freeSlots.should.equal(payment.numberOfSlots - 1)

    sendGrid.send.should.be.called()
    sendGrid.send
      .getCall(0)
      .args[0].html.includes('Default')
      .should.be.true()
  })

  it('should catch sendgrid error', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    sendGrid.send.throws(new Error('some error'))

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getPreferences.should.be.called()
  })

  it('should throw a bad request', async function () {
    this.getPaymentStub.resolves({ status: '9' })
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }

    this.getPayment.resolves(payment)
    this.getSlots.resolves([])

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      groupId: paymentId,
      accountId: 'test-account'
    }).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })

  it('should not put when paymentStatus is unchanged', async function () {
    this.getPaymentStub.resolves({ status: '9' })
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: '9'
    }

    this.getPayment.resolves(payment)

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    })

    console.log(result)

    this.putPayment.should.not.be.called()
  })

  it('should replace TOOLBOXLINK', async function () {
    const sot = new Date().toISOString()
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 39,
      amount: 312,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    this.getEmailTemplate.resolves({
      subject: '',
      body: '[JOINLINK][TOOLBOXLINK]'
    })
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)
    sendGrid.send.should.be.called()
    sendGrid.send
      .getCall(0)
      .args[0].html.includes('https://bit.ly/DRM-LMR-Toolkit-2021')
      .should.be.true()
  })

  it('should return the the number of slots from payment', async function () {
    const sot = '2020-03-04T11:40:00.007Z'
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    this.getSlotsForGroup.returns({
      totalSlots: 2,
      takenSlots: 1,
      availableSlots: 1
    })

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(10)
  })

  it('should return claimslot when slots were not found', async function () {
    const sot = '2020-03-04T11:40:00.007Z'
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    this.getSlotsForGroup.throws(Boom.notFound())

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(9)
  })

  it('should rethrow badRequest', async function () {
    const sot = '2020-03-04T11:40:00.007Z'
    const sub = 'test-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()
    this.getSlotsForGroup.throws(Boom.badRequest())

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Team',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    const result = await this.paymentProxy(sot, sub, mode, flowId, {
      paymentId: paymentId,
      accountId: 'test-account'
    }).should.be.rejected()
    console.log(result)

    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })

  it('should not create CompanyAggregate if already exists', async function () {
    const sot = '2020-05-24T19:30:01.007Z'
    const sub = 'test-company-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 10,
      amount: 80,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getCompanyAggregate.returns({ name: 'company-name', rank: 5, totalCompanies: 7 })

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(10)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getCompanyAggregate.should.be.called()

    this.getAccountAggregate.should.not.be.called()
    this.getAccountProfile.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()
    this.getTeamAggregate.should.not.be.called()
    this.getGroupProfile.should.not.be.called()
    this.writeTeamAggregate.should.not.be.called()
    this.writeCompanyAggregate.should.not.be.called()
  })

  it('should create CompanyAggregate when multiple slot are payed and no existing slots)', async function () {
    const sot = '2020-04-14T19:30:01.007Z'
    const sub = 'test-company-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 2,
      amount: 16,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getSlotsForGroup.returns({
      totalSlots: 0,
      takenSlots: 0,
      availableSlots: 0
    })

    const expectedCompanyAggregateData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      companyId: payment.groupId,
      totalSteps: 0,
      totalDistance: 0,
      totalCompanies: 9999999,
      name: 'De Stappers',
      rank: 9999999,
      hasLogo: false
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(2)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getCompanyAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeCompanyAggregate.should.be.called()

    this.getAccountProfile.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()

    this.writeCompanyAggregate.should.be.calledWithMatch(sot, sub, mode, flowId, expectedCompanyAggregateData)
  })

  it('should create CompanyAggregate even if the company only buys 1 slot and has no existing slots)', async function () {
    const sot = '2020-04-14T19:30:01.007Z'
    const sub = 'test-company-Id'
    const mode = `automated-test-${uuidv4()}`
    const flowId = uuidv4()
    const paymentId = uuidv4()

    const payment = {
      structureVersion: 1,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: uuidv4(),
      groupType: 'Company',
      voucherCode: 'AMAZINGVOUCHER',
      numberOfSlots: 1,
      amount: 8,
      paymentStatus: 'Initiated'
    }
    this.getPayment.resolves(payment)
    console.log(paymentId)

    this.getSlotsForGroup.returns({
      totalSlots: 0,
      takenSlots: 0,
      availableSlots: 0
    })

    const newCompanyAggregate = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      companyId: payment.groupId,
      totalSteps: 0,
      totalDistance: 0,
      totalCompanies: 9999999,
      name: 'De Stappers',
      rank: 9999999,
      hasLogo: false
    }

    const result = await this.paymentProxy(sot, sub, mode, flowId, { paymentId: paymentId, accountId: 'test-account' })
    console.log(result)

    result.freeSlots.should.equal(1)
    this.getPayment.should.be.called()
    this.putPayment.should.be.called()
    this.writeSlot.should.be.called()
    this.getSlots.should.be.called()
    this.getEmailTemplate.should.be.called()
    this.getSlotsForGroup.should.be.called()
    this.getCompanyAggregate.should.be.called()
    this.getGroupProfile.should.be.called()
    this.writeCompanyAggregate.should.be.called()

    this.getAccountProfile.should.not.be.called()
    this.writeAccountAggregate.should.not.be.called()

    this.writeCompanyAggregate.should.be.calledWithMatch(sot, sub, mode, flowId, newCompanyAggregate)
  })
})
