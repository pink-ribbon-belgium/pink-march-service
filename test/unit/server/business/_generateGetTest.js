/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const DynamodbFixture = require('../../../_DynamodbFixture')
const Boom = require('@hapi/boom')
const { v4: uuidv4 } = require('uuid')

const sot = '2020-01-06T11:23:00.007Z'
const sub = 'id-sub'
const itemFlowId = 'e0c42ffa-9e84-4b98-bd74-9049d6c8c106'
// required in get, but unused
const getFlowId = 'dbc5df57-1398-4c00-968e-355dcac4c539'

function generateGetTest (
  get,
  createItemData,
  createKey,
  pathParamsToIds,
  createPathParams,
  createNotFoundPathParams,
  createBadParams,
  createLinks,
  useActual,
  bodyChecks200,
  createExpectedItemData // optional => useful if any calculated properties are expected in output
) {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.mode = `automated-test-${uuidv4()}`

    this.pathParams = createPathParams()

    this.itemData = createItemData(sot, sub)

    this.expectedItemData = this.itemData
    if (createExpectedItemData) {
      this.expectedItemData = createExpectedItemData(sot, sub)
    }

    this.itemData.unknownProperty = {
      someProperty: 'we will never put an unknown property in dynamo db, but the get should be able to deal with it'
    }
    this.key = createKey(this.mode)

    this.item = {
      mode: this.mode,
      key: this.key,
      submitted: useActual ? 'actual' : this.itemData.createdAt,
      submittedBy: this.itemData.createdBy,
      flowId: itemFlowId,
      data: this.itemData
    }

    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    get.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  describe('with flowId', function () {
    beforeEach(async function () {
      await this.dynamodbFixture.putItem(this.item)
    })

    it('should work', async function () {
      if (createLinks) {
        this.expectedItemData.links = createLinks()
      }

      const result = await get(sot, sub, this.mode, getFlowId, this.pathParams)
      Object.keys(pathParamsToIds).forEach(pathParam => {
        result[pathParamsToIds[pathParam]].should.equal(this.pathParams[pathParam])
      })
      console.log(`expected result => ${JSON.stringify(result)}`)
      if (!bodyChecks200) {
        delete this.itemData.unknownProperty
        result.should.deepEqual(this.expectedItemData)
      } else {
        bodyChecks200(result, sot, sub)
      }
    })

    it('reports not found when the account does not exist', async function () {
      const exc = await get(sot, sub, this.mode, getFlowId, createNotFoundPathParams()).should.be.rejectedWith(
        Boom.notFound()
      )
      console.log(exc)
    })

    it('should throw bad request', async function () {
      const exc = await get(sot, sub, this.mode, getFlowId, createBadParams()).should.be.rejected()
      console.log(exc)
      exc.isBoom.should.be.true()
      exc.output.statusCode.should.equal(400)
    })
  })
  describe('without flowId (legacy)', function () {
    beforeEach(async function () {
      delete this.item.flowId
      await this.dynamodbFixture.putItem(this.item, true)
    })

    it('should work', async function () {
      if (createLinks) {
        this.expectedItemData.links = createLinks()
      }

      const result = await get(sot, sub, this.mode, getFlowId, this.pathParams)
      Object.keys(pathParamsToIds).forEach(pathParam => {
        result[pathParamsToIds[pathParam]].should.equal(this.pathParams[pathParam])
      })
      console.log(`expected result => ${JSON.stringify(result)}`)
      if (!bodyChecks200) {
        delete this.itemData.unknownProperty
        result.should.deepEqual(this.expectedItemData)
      } else {
        bodyChecks200(result, sot, sub)
      }
    })
  })
}

module.exports = generateGetTest
