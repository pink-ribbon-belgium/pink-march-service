/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const PersistentObject = require('../../../../lib/server/business/PersistentObject')
const sinon = require('sinon')

const mode = 'automated-test-68dac7ce-4dbc-4d6b-a4d6-a525079fada6'
const partitionKey = `/${mode}/a-partition-key`
const indexNames = ['A', 'B', 'C', 'D', 'E']

describe(testName, function () {
  beforeEach(function () {
    PersistentObject.contract.verifyPostconditions = true
    this.sandbox = sinon.createSandbox()
    this.getKeyStub = this.sandbox.stub(PersistentObject.prototype, 'getKey')
    this.getLinksStub = this.sandbox.stub(PersistentObject.prototype, 'getLinks')
  })

  afterEach(function () {
    PersistentObject.contract.verifyPostconditions = false
    this.sandbox.restore()
  })

  it('works', function () {
    const persistentObject = new PersistentObject(PersistentObject.contract.KwargsExample)
    persistentObject.key = 'sdfsdf'
    this.getKeyStub.returns(`/${mode}/${persistentObject.key}`)
    this.getLinksStub.returns(undefined)
    persistentObject.should.upholdInvariants()
  })

  describe('indePartitionKeys', function () {
    beforeEach(function () {
      this.getIndexPartitionKeysStub = this.sandbox.stub(PersistentObject.prototype, 'getIndexPartitionKeys')
    })

    function generateIndexXKeysTest (indexName) {
      it(`works with index ${indexName} keys`, function () {
        const persistentObject = new PersistentObject(PersistentObject.contract.KwargsExample)
        persistentObject.key = 'sdfsdf'
        this.getKeyStub.returns(`/${mode}/${persistentObject.key}`)
        const indexPartitionKeys = {}
        indexPartitionKeys[indexName] = partitionKey
        this.getIndexPartitionKeysStub.returns(indexPartitionKeys)
        persistentObject.should.upholdInvariants()
        persistentObject.toItem()[`partition_key_${indexName}`].should.equal(partitionKey)
        persistentObject.toItem()[`sort_key_${indexName}`].should.equal(persistentObject.createdAt)
      })
    }

    indexNames.forEach(generateIndexXKeysTest)
  })
})
