/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/health/get')
const ServiceHealthStatus = require('../../../../../lib/api/health/ServiceHealthStatus')
const sinon = require('sinon')
const util = require('util')
const proxyquire = require('proxyquire')
const getVersionAtOrBefore = require('../../../../../lib/server/aws/dynamodb/getVersionAtOrBefore')
const { v4: uuidv4 } = require('uuid')

describe(testName, function () {
  beforeEach(async function () {
    this.sot = '2020-01-23T11:00:00.007Z'
    this.sub = 'id-of-the-subTest1'
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = 'e0c42ffa-9e84-4b98-bd74-9049d6c8c106'
    this.pathParams = {}

    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    get.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const result = await get(this.sot, this.sub, this.mode, this.flowId, this.pathParams)
    result.dynamodb.should.equal(ServiceHealthStatus.OK)
    result.status.should.equal(ServiceHealthStatus.OK)
    console.log(result)
  })
  describe('problems', function () {
    beforeEach(function () {
      this.sandbox = sinon.createSandbox()
      this.getVersionAtOrBeforeStub = this.sandbox.stub()
      this.getVersionAtOrBeforeStubContractFunction = getVersionAtOrBefore.contract.implementation(
        this.getVersionAtOrBeforeStub
      )
      this.getVersionAtOrBeforeStubContractFunction.contract.verifyPostconditions = true
      this.subject = proxyquire('../../../../../lib/server/business/health/get', {
        '../../aws/dynamodb/getVersionAtOrBefore': this.getVersionAtOrBeforeStubContractFunction
      })
      this.subject.contract.verifyPostconditions = true
    })

    afterEach(function () {
      this.subject.contract.verifyPostconditions = false
      this.sandbox.restore()
    })

    it('should throw error when something goes wrong', async function () {
      this.getVersionAtOrBeforeStub.resolves(undefined) // forces postcondition violation, which is an error to dynamodbHealth
      const err = await this.subject(this.sot, this.sub, this.mode, this.flowId, this.pathParams).should.be.rejected()
      err.output.payload.dynamodb.should.equal(ServiceHealthStatus.ERROR)
      err.output.payload.status.should.equal(ServiceHealthStatus.ERROR)
      console.log(util.inspect(err, { depth: 9 }))
    })
    it('should return warning when something is found unexpectedly', async function () {
      const data = {
        createdAt: get.submittedThatDoesntExist,
        createdBy: 'aSub',
        structureVersion: 987,
        flowId: this.flowId
      }
      this.getVersionAtOrBeforeStub.resolves({
        mode: this.mode,
        key: get.keyThatDoesntExist(this.mode),
        submitted: data.createdAt,
        submittedBy: data.createdBy,
        data
      })
      const result = await this.subject(
        this.sot,
        this.sub,
        this.mode,
        this.flowId,
        this.pathParams
      ).should.be.fulfilled()
      result.dynamodb.should.equal(ServiceHealthStatus.WARNING)
      result.status.should.equal(ServiceHealthStatus.WARNING)
      console.log(util.inspect(result, { depth: 9 }))
    })
  })
})
