/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const Team = require('../../../../../lib/server/business/team/Team')
const TeamDTO = require('../../../../../lib/api/team/TeamDTO')
const Group = require('../../../../../lib/server/business/group/Group')
const Mode = require('../../../../../lib/ppwcode/Mode')

const KwargsExample = {
  mode: Mode.example,
  dto: {
    ...TeamDTO.exampleDynamodb
  }
}

describe(testName, function () {
  beforeEach(function () {
    Team.contract.verifyPostconditions = true
  })

  afterEach(function () {
    Team.contract.verifyPostconditions = false
  })
  it('should work', function () {
    const result = new Team(KwargsExample)
    result.should.upholdInvariants()
  })

  it('should getJoinLink', function () {
    const linkKwargs = {
      mode: Mode.example,
      groupId: KwargsExample.dto.id,
      groupType: KwargsExample.dto.groupType
    }
    const linkUrl = Group.getJoinLink(linkKwargs)
    const expectedResult = `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${KwargsExample.dto.groupType.toLowerCase()}/${
      KwargsExample.dto.id
    }`

    linkUrl.should.equal(expectedResult)
  })
})
