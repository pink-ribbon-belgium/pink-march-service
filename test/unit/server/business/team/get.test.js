/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/team/get')
const generateGetTest = require('../_generateGetTest')
const { v4: uuidv4 } = require('uuid')

const teamId = '87cf58a7-febe-47a8-b039-9e81aec98c20'
const linkId = uuidv4()

describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      id: teamId,
      code: 'TEST-CODE-TEST-CODE',
      groupType: 'Team',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r'
    }),
    mode => `/${mode}/team/${teamId}`,
    { teamId: 'id' },
    () => ({
      teamId
    }),
    () => ({
      teamId: '7bd736c7-44fc-4b31-b262-79bc49053519'
    }),
    () => ({
      teamId: 4524
    }),
    () => ({
      administrators: `/I/team/${teamId}/administrators`,
      slots: `/I/team/${teamId}/slots`,
      payments: `/I/team/${teamId}/payments`,
      publicProfile: `/I/team/${teamId}/publicProfile`
    }),
    false,
    false,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 78,
      id: teamId,
      code: 'TEST-CODE-TEST-CODE',
      groupType: 'Team',
      linkId: linkId,
      shortUrl: 'https://rebrand.ly/mtt6q8r',
      joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/team/${teamId}`
    })
  )
})
