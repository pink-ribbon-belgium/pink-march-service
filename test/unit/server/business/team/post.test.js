/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const post = require('../../../../../lib/server/business/team/post')
const AccountIdExample = require('../../../../../lib/api/account/AccountId').example

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.testUuid = uuidv4()
    this.flowId = '3addd738-118f-462c-a9d0-06fef2191272'
    this.mode = `automated-test-${this.testUuid}`
    this.createdAt = new Date().toISOString()
    this.createdBy = 'id-of-the-company-creator'
    this.teamId = '5cf0c3b6-9284-460f-a46f-7c92a8e1366b'

    this.itemData = {
      createdAt: this.createdAt,
      createdBy: this.createdBy,
      structureVersion: 81,
      id: this.teamId,
      code: 'XXXX-CCCC-BBBB-AAAA',
      groupType: 'Team',
      name: 'This is a company',
      accountId: AccountIdExample
    }

    post.contract.verifyPostconditions = true

    this.teamHasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key',
          '#submitted': 'submitted'
        },
        ExpressionAttributeValues: {
          ':key': this.groupKey,
          ':submitted': this.createdAt
        },
        KeyConditionExpression: '#key = :key and #submitted = :submitted',
        Limit: 1,
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }

    this.groupAdministrationHasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': this.groupAdministrationKey
        },
        KeyConditionExpression: '#key = :key',
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }

    this.groupProfileHasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': this.groupProfileKey
        },
        KeyConditionExpression: '#key = :key',
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    post.contract.verifyPostconditions = false

    if (this.storedTestItemKeys) {
      await Promise.all(
        this.storedTestItemKeys.map(storedKey =>
          this.dynamodbInstance
            .delete({
              TableName: config.dynamodb.test.tableName,
              Key: {
                key: storedKey.key,
                submitted: storedKey.submitted
              }
            })
            .promise()
        )
      )
    }
  })

  it('works', async function () {
    const result = await post(this.createdAt, this.createdBy, this.mode, this.flowId, {}, this.itemData)
    this.groupKey = `/${this.mode}/team/${result.teamId}`
    this.groupAdministrationKey = `/${this.mode}/groupAdministration/account/${AccountIdExample}/group/${result.teamId}`
    this.groupProfileKey = `/${this.mode}/group/${result.teamId}/publicProfile`
    this.storedTestItemKeys = [
      { key: this.groupKey, submitted: this.createdAt },
      {
        key: this.groupAdministrationKey,
        submitted: 'actual'
      },
      {
        key: this.groupAdministrationKey,
        submitted: this.createdAt
      }
    ]
    const teamRetrieved = await this.teamHasArrived()
    teamRetrieved.ScannedCount.should.equal(1)
    const groupProfileRetrieved = await this.groupProfileHasArrived()
    groupProfileRetrieved.ScannedCount.should.equal(1)
    const groupAdministrationRetrieved = await this.groupAdministrationHasArrived()
    groupAdministrationRetrieved.ScannedCount.should.equal(2)
  })
  it('says 404 with a bad body', async function () {
    this.itemData.structureVersion = 'this is not a structure version'
    const exc = await post(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {},
      this.itemData
    ).should.be.rejected()
    exc.should.be.an.Error()
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
