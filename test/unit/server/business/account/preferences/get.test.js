/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/account/preferences/get')
const generateGetTest = require('../../_generateGetTest')

const accountId = 'id-account-preferences'

describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 2,
      accountId: accountId,
      language: 'fr',
      verifiedEmail: {
        email: 'rustigeEric@test.be',
        isVerified: false
      },
      newsletter: false,
      knownFromType: 'OTHER',
      otherText: 'interwebs'
    }),
    mode => `/${mode}/account/${accountId}/preferences`,
    { accountId: 'accountId' },
    () => ({
      accountId
    }),
    () => ({
      accountId: 'does-not-exist'
    }),
    () => ({
      accountId: 4524
    }),
    () => ({
      versions: `/I/account/${accountId}/publicProfile/versions`
    })
  )
})
