/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const AccountPreferences = require('../../../../../../lib/server/business/account/preferences/AccountPreferences')
const put = require('../../../../../../lib/server/business/account/preferences/put')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.flowId = uuidv4()
    this.testUuid = uuidv4()
    this.mode = `automated-test-${this.testUuid}`
    this.createdAt = '2020-01-27T09:00:01.007Z'
    this.createdBy = 'id-preferences-test'

    this.itemData = {
      createdAt: this.createdAt,
      createdBy: this.createdBy,
      structureVersion: 2,
      accountId: 'id-OF-test1',
      language: 'nl-BE',
      verifiedEmail: {
        email: 'rustigeEric@test.be',
        isVerified: false
      },
      newsletter: true,
      knownFromType: 'OTHER',
      otherText: 'internetwebs'
    }

    this.key = `account/${this.itemData.accountId}/preferences`
    this.item = {
      mode: this.mode,
      key: `/${this.mode}/${this.key}`,
      submitted: this.createdAt,
      submittedBy: this.createdBy,
      data: this.itemData
    }
    this.storedTestItemKey = { key: this.item.key, submitted: this.item.submitted }

    this.accountPreferences = new AccountPreferences({
      mode: this.mode,
      dto: { ...this.itemData },
      sot: this.item.submitted,
      sub: this.item.submittedBy
    })

    put.contract.verifyPostconditions = true

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key',
          '#submitted': 'submitted'
        },
        ExpressionAttributeValues: {
          ':key': this.accountPreferences.key,
          ':submitted': this.item.submitted
        },
        KeyConditionExpression: '#key = :key and #submitted = :submitted',
        Limit: 1,
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })
  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    if (this.storedTestItemKey) {
      await this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: { key: this.storedTestItemKey.key, submitted: this.storedTestItemKey.submitted }
        })
        .promise()
      console.log(`deleted item ${util.inspect(this.storedTestItemKey)}`)
      delete this.storedTestItemKey
    }
  })
  it('works', async function () {
    await put(
      this.item.submitted,
      this.item.submittedBy,
      this.mode,
      this.flowId,
      {
        accountId: this.accountPreferences.accountId
      },
      this.itemData
    )
    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(1)
    retrieved.Items[0].key.should.equal(this.accountPreferences.key)
  })
  it('should throw a badRequest', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest4-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()
    const accountId = 'test-accountId'

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1654846,
      accountId: accountId,
      language: accountId,
      verifiedEmail: {
        email: 'rustigeEric@test.be',
        isVerified: false
      },
      newsletter: true,
      knownFromType: 'WALKING',
      deleted: false
    }

    const exception = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        accountId: accountId
      },
      itemData
    ).should.be.rejected()
    console.log(exception)
    exception.isBoom.should.be.true()
    exception.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest5-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()
    const accountId = 'test-accountId'
    const language = 'nl-BE'

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 64654,
      accountId: accountId,
      language: language,
      verifiedEmail: {
        email: 'rustigeEric@test.be',
        isVerified: false
      },
      newsletter: true,
      knownFromType: 'WALKING',
      deleted: false
    }

    const exception = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        accountId: 45
      },
      itemData
    ).should.be.rejected()
    console.log(exception)
    exception.isBoom.should.be.true()
    exception.output.statusCode.should.equal(400)
  })
})
