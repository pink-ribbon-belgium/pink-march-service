/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../../lib/server/business/account/slot/put')
const DynamodbFixture = require('../../../../../_DynamodbFixture')

const flowId = 'c35c6baa-45e2-4664-bb38-62043f1467b8'
const sot = '2020-03-26T15:04:35.047Z'
const sub = 'sub-slot-write-test'
const slotId = '81d58a4a-9b8e-4d5e-8c03-6b557f972c83'
const groupId = '09a57709-79b3-4555-b562-02e5071c9270'
const paymentId = 'ba4e3c42-f558-45bf-a7c3-c22ea73111ab'
const accountId = 'da0bfdab-8a4e-4afc-a023-97aa02f7b6e2'
const subGroupId = '2afaaa05-3e00-4098-aa65-0b020a3bb019'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamoDbFixture = new DynamodbFixture()
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`

    this.key = `/${this.mode}/group/${groupId}/slot/${slotId}`

    this.dto = {
      accountId,
      groupType: 'Company',
      groupId,
      paymentId,
      id: slotId,
      subGroupId,
      structureVersion: 2
    }

    this.dynamoDbFixture.remember(this.key, sot, config.dynamodb.test.tableName)
    this.dynamoDbFixture.remember(this.key, 'actual', config.dynamodb.test.tableName)

    put.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    put.contract.verifyPostconditions = false

    await this.dynamoDbFixture.clean()
  })

  it('should work', async function () {
    await put(sot, sub, this.mode, flowId, { accountId: accountId }, this.dto)
    const retrieved = await this.dynamoDbFixture.hasArrived()
    console.log(retrieved)
    retrieved.every(item => item.key.should.equal(this.key))
  })

  it('should throw a badRequest with bad path parameters', async function () {
    const exc = await put(
      sot,
      sub,
      this.mode,
      this.flowId,
      {
        accountId: ''
      },
      this.dto
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
