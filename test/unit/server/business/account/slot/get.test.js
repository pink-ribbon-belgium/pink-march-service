/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const DynamoDbFixture = require('../../../../../_DynamodbFixture')
const get = require('../../../../../../lib/server/business/account/slot/get')
const Slot = require('../../../../../../lib/server/business/slot/Slot')
const { v4: uuidv4 } = require('uuid')
const boom = require('@hapi/boom')
const { values: groupTypes } = require('../../../../../../lib/api/group/GroupType')

const groupId = 'c100e528-04b9-4102-883c-2003469ff5c3'
const paymentId = 'f7dd1ce5-7c04-4b8a-b673-db31ea0219a7'
const slotId = '5ce3b151-11bb-437b-8cb7-a3e5b8f97e21'
const itemFlowId = '65fa90b0-aaef-46e8-b8ee-8df755140fda'
const itemSot = '2020-03-30T12:28:56.399Z'
const itemSub = 'item-sub-getGroupsForAdministrator-test'
const getSot = '2020-03-30T12:29:27.372Z'
const getSub = 'get-sub-getGroupsForAdministrator-test'
const getFlowId = 'bb4bc7b4-a4c4-45c7-b063-3b71ce279ae5'
const accountId = 'accountId-getGroupsForAdministrator-test'

describe(testName, function () {
  beforeEach(function () {
    this.mode = `automated-test-${uuidv4()}`
    get.contract.verifyPostconditions = true
  })

  groupTypes.forEach(groupType => {
    describe(groupType, function () {
      beforeEach(async function () {
        this.dynamoDbFixture = new DynamoDbFixture()
      })

      afterEach(async function () {
        await this.dynamoDbFixture.clean()
      })

      it('should work with structureVersion 2', async function () {
        const slot = new Slot({
          mode: this.mode,
          dto: {
            accountId,
            groupType,
            groupId,
            paymentId,
            id: slotId,
            createdAt: itemSot,
            createdBy: itemSub,
            structureVersion: 2
          }
        })

        const item = {
          ...slot.toItem(),
          flowId: itemFlowId,
          submitted: 'actual',
          sort_key_A: 'actual',
          sort_key_B: 'actual',
          sort_key_C: 'actual'
        }

        await this.dynamoDbFixture.putItem(item)

        const result = await get(getSot, getSub, this.mode, getFlowId, {
          accountId
        })
        console.log(result)
      })

      it('should work with structureVersion 1', async function () {
        const groupItem = {
          mode: this.mode,
          key: `/${this.mode}/${groupType.toLowerCase()}/${groupId}`,
          submitted: itemSot,
          submittedBy: itemSub,
          flowId: itemFlowId,
          data: {
            createdAt: itemSot,
            createdBy: itemSub,
            structureVersion: 1,
            id: groupId,
            code: 'ABCD-ABCD-ABCD-ABCD',
            groupType,
            // below only for company, but it doesn't hurt for team
            address: 'this is an address 12',
            zip: '2500',
            city: 'Lier',
            vat: 'BE0452123456',
            contactFirstName: 'Jos',
            contactLastName: 'Pros',
            contactEmail: 'email@exampleIn.com',
            contactTelephone: '+32498765432',
            unit: 'Test-Unit'
          }
        }

        const slotItem = {
          mode: this.mode,
          flowId: itemFlowId,
          key: `/${this.mode}/group/${groupId}/slot/${slotId}`,
          submitted: 'actual',
          submittedBy: itemSub,
          data: {
            createdAt: itemSot,
            createdBy: itemSub,
            structureVersion: 1,
            id: slotId,
            accountId,
            groupId,
            paymentId
          },
          partition_key_A: `/${this.mode}/payment/${paymentId}/slots`,
          sort_key_A: 'actual',
          partition_key_B: `/${this.mode}/group/${groupId}/members`,
          sort_key_B: 'actual',
          partition_key_C: `/${this.mode}/account/${accountId}/slot`,
          sort_key_C: 'actual'
        }

        await Promise.all([this.dynamoDbFixture.putItem(groupItem), this.dynamoDbFixture.putItem(slotItem)])

        const result = await get(getSot, getSub, this.mode, getFlowId, {
          accountId
        })
        console.log(result)
      })
    })
  })

  it('should throw not found', async function () {
    await get(getSot, getSub, this.mode, getFlowId, { accountId: 'another_account' }).should.be.rejectedWith(
      boom.notFound()
    )
  })

  it('should throw bad request', async function () {
    const exc = await get(getSot, getSub, this.mode, getFlowId, { accountId: 4568 }).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
