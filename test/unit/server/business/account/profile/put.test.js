/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const AccountProfile = require('../../../../../../lib/server/business/account/profile/AccountProfile')
const put = require('../../../../../../lib/server/business/account/profile/put')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.flowId = uuidv4()
    this.testUuid = uuidv4()
    this.mode = `automated-test-${this.testUuid}`
    this.createdAt = '2020-02-01T11:00:01.007Z'
    this.createdBy = 'id-ProfileTest1'

    this.itemData = {
      createdAt: this.createdAt,
      createdBy: this.createdBy,
      structureVersion: 69,
      accountId: 'id-OF-test1',
      firstName: 'test',
      lastName: 'DoeRustiger',
      gender: 'F',
      dateOfBirth: '1987-01-23T15:22:39.212Z',
      zip: '4500'
    }
    this.key = `account/${this.itemData.accountId}/publicProfile`
    this.item = {
      mode: this.mode,
      key: `/${this.mode}/${this.key}`,
      submitted: this.createdAt,
      submittedBy: this.createdBy,
      data: this.itemData
    }

    this.storedTestItemKeys = [
      {
        key: this.item.key,
        submitted: this.item.submitted
      },
      {
        key: this.item.key,
        submitted: 'actual'
      }
    ]
    this.accountProfile = new AccountProfile({
      mode: this.mode,
      dto: { ...this.itemData },
      sot: this.item.submitted,
      sub: this.item.submittedBy
    })

    put.contract.verifyPostconditions = true

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': this.accountProfile.key
        },
        KeyConditionExpression: '#key = :key',
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    put.contract.verifyPostconditions = false

    if (this.storedTestItemKey) {
      await this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: { key: this.storedTestItemKey.key, submitted: this.storedTestItemKey.submitted }
        })
        .promise()
      console.log(`deleted item ${util.inspect(this.storedTestItemKey)}`)
      delete this.storedTestItemKey
    }
  })

  it('works', async function () {
    await put(
      this.item.submitted,
      this.item.submittedBy,
      this.mode,
      this.flowId,
      {
        accountId: this.accountProfile.accountId
      },
      this.itemData
    )
    const submitted = '2020-01-27T08:23:33.007Z'
    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(2)
    retrieved.Items.every(item => item.key.should.equal(this.accountProfile.key))
    const submittedItems = retrieved.Items.filter(i => i.submitted === submitted)
    submittedItems.every(item => item.sort_key_A.should.equal(submitted))
    const actualItems = retrieved.Items.filter(i => i.submitted === 'actual')
    actualItems.every(item => item.sort_key_A.should.equal('actual'))
  })
  it('should throw a badRequest', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest4-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()
    const accountId = 'test-accountId'

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 69,
      accountId: 'id-OF-test1',
      firstName: 'test',
      lastName: 'DoeRustiger',
      gender: 'FV',
      dateOfBirth: 'wrong date',
      zip: '4500',
      deleted: false
    }
    const exception = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        accountId: accountId
      },
      itemData
    ).should.be.rejected()
    console.log(exception)
    exception.isBoom.should.be.true()
    exception.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sub = 'subtest5-accountId'
    const sot = new Date().toISOString()
    const flowId = uuidv4()
    const accountId = 45

    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 65454,
      accountId: 'id-OF-test5',
      firstName: 'Erica',
      lastName: 'DoeRustiger',
      gender: 'FV',
      dateOfBirth: '1987-01-23T15:22:39.212Z',
      zip: '4500',
      deleted: false
    }
    const exc = await put(
      sot,
      sub,
      mode,
      flowId,
      {
        accountId: accountId
      },
      itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })
})
