/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const AccountProfile = require('../../../../../../lib/server/business/account/profile/AccountProfile')
const AccountProfileDTO = require('../../../../../../lib/api/account/AccountProfileDTO')
const Mode = require('../../../../../../lib/ppwcode/Mode')

const KwargsExample = {
  mode: Mode.example,
  dto: AccountProfileDTO.exampleDynamoDb
}

describe(testName, function () {
  beforeEach(function () {
    AccountProfile.contract.verifyPostconditions = true
  })

  afterEach(function () {
    AccountProfile.contract.verifyPostconditions = false
  })
  it('should work', function () {
    const result = new AccountProfile(KwargsExample)
    result.should.upholdInvariants()
  })
})
