/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const TokenType = require('../../../../../../lib/server/business/account/trackerConnection/TokenType')
const uuidv4 = require('uuid').v4
const config = require('config')
const TrackerConnectionInput = require('../../../../../../lib/api/account/trackerConnection/TrackerConnectionInput')

describe(testName, () => {
  beforeEach(function () {
    this.fetchStub = sinon.stub()
    this.getToken = proxyquire('../../../../../../lib/server/business/account/trackerConnection/getToken', {
      'node-fetch': this.fetchStub
    })
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.getToken.contract.verifyPostconditions = true
  })

  afterEach(function () {
    this.getToken.contract.verifyPostconditions = false
  })

  it('should work for refresh token', async function () {
    this.fetchStub.returns({
      json: () => ({
        refresh_token: 'token'
      })
    })
    const expected = {
      token: 'token',
      tokenType: TokenType.refreshType,
      exp: undefined
    }
    const result = await this.getToken(this.mode, this.flowId, TrackerConnectionInput.example)
    console.log(result)
    result.should.be.ok()
    result.should.deepEqual(expected)
  })

  it('should work for access token', async function () {
    this.fetchStub.returns({
      json: () => ({
        access_token: 'token',
        expires_in: 300
      })
    })
    const result = await this.getToken(this.mode, this.flowId, TrackerConnectionInput.example)
    console.log(result)
    result.should.be.ok()
    result.token.should.equal('token')
    result.tokenType.should.equal(TokenType.accessType)
    result.exp.should.be.ok()
  })
  it('should work for polar', async function () {
    this.fetchStub.returns({
      json: () => ({
        access_token: 'token',
        expires_in: 3000
      })
    })
    const example = {
      trackerType: 'polar',
      code: 'somecode',
      redirectUrl: 'https://localhost:4200/polar.html'
    }
    const result = await this.getToken(this.mode, this.flowId, example)
    console.log(result)
    result.token.should.equal('token')
    result.tokenType.should.equal(TokenType.accessType)
    result.exp.should.be.ok()
  })

  it('should throw 404 when payload contains errors', async function () {
    const error = {
      error_type: 'some_error',
      error_description: 'some description'
    }
    this.fetchStub.returns({
      json: () => ({
        errors: [error]
      })
    })
    const result = await this.getToken(this.mode, this.flowId, TrackerConnectionInput.example).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
    result.output.payload.error.should.equal(error.error_type)
    result.output.payload.message.should.equal(error.error_description)
  })

  it('should use prod config for production', async function () {
    this.fetchStub.returns({
      json: () => ({
        refresh_token: 'token'
      })
    })
    await this.getToken('production', this.flowId, TrackerConnectionInput.example)
    this.fetchStub.should.have.been.called()
    this.fetchStub.getCall(0).args[0].should.equal(config.trackers.fitbit.prod.tokenEndpoint)
  })

  it('should use specific production config for polar', async function () {
    this.fetchStub.returns({
      json: () => ({
        refresh_token: 'token'
      })
    })
    const example = {
      trackerType: 'polar',
      code: 'somecode',
      redirectUrl: 'https://localhost:4200/polar.html'
    }
    await this.getToken('june2020', this.flowId, example)
    this.fetchStub.should.have.been.called()
    this.fetchStub.getCall(0).args[0].should.equal(config.trackers.polar.june2020.tokenEndpoint)
  })
})
