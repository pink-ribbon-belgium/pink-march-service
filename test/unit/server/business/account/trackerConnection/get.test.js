/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const get = require('../../../../../../lib/server/business/account/trackerConnection/get')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const TrackerConnection = require('../../../../../../lib/server/business/account/trackerConnection/TrackerConnection')

describe(testName, () => {
  beforeEach(async function () {
    this.sot = '2020-03-31T12:25:26.918Z'
    this.sub = 'this-is-a-sub'
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = uuidv4()
    this.accountId = uuidv4()
    this.dynamodbFixture = new DynamodbFixture()
    get.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    await this.dynamodbFixture.clean()
    get.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        accountId: this.accountId,
        trackerType: 'fitbit',
        tokenType: 'refresh',
        encryptedToken: 'WA==',
        exp: '2020-03-31T08:30:25.125Z',
        hasError: false,
        createdAt: '2020-03-31T08:30:25.125Z',
        createdBy: this.accountId,
        structureVersion: 1
      }
    })

    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: 'actual',
      sort_key_B: 'actual',
      flowId: this.flowId
    })
    const result = await get(this.sot, this.sub, this.mode, this.flowId, { accountId: this.accountId })
    result.should.be.ok()
  })

  it('should work (deleted)', async function () {
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        createdAt: this.sot,
        createdBy: this.sub,
        structureVersion: 1,
        accountId: this.accountId,
        deleted: true
      }
    })
    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: 'actual',
      sort_key_B: 'actual',
      flowId: this.flowId
    })
    const result = await get(this.sot, this.sub, this.mode, this.flowId, { accountId: this.accountId })
    result.should.be.ok()
  })

  it('should throw 404', async function () {
    const result = await get(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    }).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })

  it('should throw 400', async function () {
    const result = await get(this.sot, this.sub, this.mode, this.flowId, {}).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
