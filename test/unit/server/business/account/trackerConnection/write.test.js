/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const write = require('../../../../../../lib/server/business/account/trackerConnection/write')
const DynamodbFixture = require('../../../../../_DynamodbFixture')

const flowId = '63436f2f-c5cb-4c06-a7f5-a3974f2e3306'
const sot = '2020-04-09T15:04:35.047Z'
const sub = 'sub-trackerConnection-write-test'
const accountId = '4ad14f4a-8593-4d9e-860e-24be034fe064'
const TrackerType = require('../../../../../../lib/api/account/trackerConnection/TrackerType')
const TokenType = require('../../../../../../lib/server/business/account/trackerConnection/TokenType')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.dynamoDbKey = `/${this.mode}/account/${accountId}/trackerConnection`
    this.itemTrackerType = TrackerType.fitbitType

    this.dynamodbFixture.remember(this.dynamoDbKey, sot, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.dynamoDbKey, 'actual', config.dynamodb.test.tableName)

    this.itemData = {
      createdAt: sot,
      createdBy: sub,
      accountId: accountId,
      trackerType: TrackerType.fitbitType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: '2020-04-09T11:05:52.191Z',
      hasError: false,
      structureVersion: 1
    }

    write.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    write.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    await write(sot, sub, this.mode, flowId, this.itemData)

    const storedItems = await this.dynamodbFixture.hasArrived()
    storedItems.length.should.equal(2)
    storedItems.some(slotItem => slotItem.submitted === sot).should.be.true()
    storedItems.some(slotItem => slotItem.submitted === 'actual').should.be.true()

    storedItems.forEach(trackerConnection => {
      if (trackerConnection.submitted === 'actual') {
        trackerConnection.should.have.ownProperty('partition_key_A')
        trackerConnection.partition_key_A.should.equal(`/${this.mode}/trackerConnection`)
        trackerConnection.should.have.ownProperty('sort_key_A')
        trackerConnection.sort_key_A.should.equal(sot)

        trackerConnection.should.have.ownProperty('partition_key_B')
        trackerConnection.partition_key_B.should.equal(`/${this.mode}/trackerConnection/${this.itemTrackerType}`)
        trackerConnection.should.have.ownProperty('sort_key_B')
        trackerConnection.sort_key_B.should.equal(sot)
      } else {
        trackerConnection.should.not.have.ownProperty('partition_key_A')
        trackerConnection.should.not.have.ownProperty('sort_key_A')
        trackerConnection.should.not.have.ownProperty('partition_key_B')
        trackerConnection.should.not.have.ownProperty('sort_key_B')
      }

      trackerConnection.data.should.be.deepEqual(this.itemData)
    })
  })
})
