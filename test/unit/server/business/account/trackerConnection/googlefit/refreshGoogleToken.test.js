/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const uuidv4 = require('uuid').v4
const config = require('config')

const DynamodbFixture = require('../../../../../../_DynamodbFixture')
const testName = require('../../../../../../_testName')(module)
const getActual = require('../../../../../../../lib/server/aws/dynamodb/getActual')
const TokenType = require('../../../../../../../lib/server/business/account/trackerConnection/TokenType')
const TrackerConnection = require('../../../../../../../lib/server/business/account/trackerConnection/TrackerConnection')
const TrackerType = require('../../../../../../../lib/api/account/trackerConnection/TrackerType')

describe(testName, () => {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.fetchStub = sinon.stub()
    this.decryptStub = sinon.stub()
    this.refreshAccessToken = proxyquire(
      '../../../../../../../lib/server/business/account/trackerConnection/googlefit/refreshGoogleToken',
      {
        'node-fetch': this.fetchStub,
        '../../../../aws/crypto/decrypt': this.decryptStub
      }
    )
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2020-03-31T12:25:26.918Z'
    this.sub = 'sub-for-refreshAccessToken-test'

    this.accountId = '4c493368-a7a2-4371-91eb-3925c993cb69'
    this.trackerType = TrackerType.googleType

    this.itemData = {
      accountId: this.accountId,
      trackerType: this.trackerType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: '2020-04-09T18:30:25.125Z',
      hasError: false,
      createdAt: '2020-04-09T08:30:25.125Z',
      createdBy: this.accountId,
      structureVersion: 1
    }
    this.connection = new TrackerConnection({
      mode: this.mode,
      dto: {
        ...this.itemData
      }
    })
    await this.dynamodbFixture.putItem({ ...this.connection.toItem(), submitted: 'actual', flowId: this.flowId })

    this.refreshAccessToken.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.refreshAccessToken.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('should work for refresh token', async function () {
    this.decryptStub.returns('initial_token')

    const newAccessToken = 'new_access_token'

    this.fetchStub.returns({
      json: () => ({
        access_token: newAccessToken,
        expires_in: 300
      })
    })
    const result = await this.refreshAccessToken(this.sot, this.sub, this.mode, this.flowId, this.itemData)
    const key = `/${this.mode}/account/${this.accountId}/trackerConnection`
    const updatedTrackerConnection = await getActual(this.mode, key, true)

    console.log(result)
    result.should.be.ok()
    result.should.deepEqual(newAccessToken)
    console.log(updatedTrackerConnection)

    updatedTrackerConnection.data.encryptedToken.should.be.equal(this.connection.encryptedToken)
  })

  it('should throw 400 when payload contains error', async function () {
    const error = {
      error_type: 'Bad Request',
      error_description: 'some description'
    }
    this.fetchStub.returns({
      json: () => ({
        error: error
      })
    })
    const result = await this.refreshAccessToken(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData
    ).should.be.rejected()

    console.log(result)

    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
    result.output.payload.error.should.equal(error.error_type)

    const key = `/${this.mode}/account/${this.accountId}/trackerConnection`
    const updatedTrackerConnection = await getActual(this.mode, key, true)

    updatedTrackerConnection.data.hasError.should.equal(true)
  })
  it('should use prod config for production', async function () {
    const newAccessToken = 'new_access_token'

    const writeTrackerSpy = sinon.spy()

    this.refreshAccessToken = proxyquire(
      '../../../../../../../lib/server/business/account/trackerConnection/googlefit/refreshGoogleToken',
      {
        'node-fetch': this.fetchStub,
        '../write': writeTrackerSpy,
        '../../../../aws/crypto/decrypt': this.decryptStub
      }
    )
    this.decryptStub.returns('initial_token')
    this.fetchStub.returns({
      json: () => ({
        access_token: newAccessToken,
        expires_in: 300
      })
    })
    await this.refreshAccessToken(this.sot, this.sub, 'production', this.flowId, this.itemData)

    this.fetchStub.should.have.been.called()
    writeTrackerSpy.should.have.be.called()
    this.fetchStub.getCall(0).args[0].should.equal(config.trackers.googlefit.prod.tokenEndpoint)
  })
})
