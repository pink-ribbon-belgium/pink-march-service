/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const sinon = require('sinon')
const proxyquire = require('proxyquire')
const uuidv4 = require('uuid').v4
const moment = require('moment')

const testName = require('../../../../../../_testName')(module)
const TokenType = require('../../../../../../../lib/server/business/account/trackerConnection/TokenType')
const TrackerType = require('../../../../../../../lib/api/account/trackerConnection/TrackerType')

describe(testName, () => {
  beforeEach(async function () {
    this.refreshAccessTokenStub = sinon.stub()
    this.fetchStub = sinon.stub()
    this.getActivityData = proxyquire(
      '../../../../../../../lib/server/business/account/trackerConnection/googlefit/getGoogleFitData',
      {
        'node-fetch': this.fetchStub,
        './refreshGoogleToken': this.refreshAccessTokenStub
      }
    )
    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2020-03-30T12:25:26.918Z'
    this.sub = 'sub-for-getGoogleFitData-test'

    this.accountId = 'e69d7d8a-1124-46f0-b680-9d2458a1e74e'
    this.trackerType = TrackerType.googleType

    this.itemData = {
      accountId: this.accountId,
      trackerType: this.trackerType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: '2020-04-09T18:30:25.125Z',
      hasError: false,
      createdAt: '2020-04-09T08:30:25.125Z',
      createdBy: this.accountId,
      structureVersion: 1
    }
    this.getActivityData.contract.verifyPostconditions = true

    this.timeout(15000)
  })

  afterEach(async function () {
    this.getActivityData.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    this.startDate = new Date('2020-06-02T00:00:00.000Z')
    this.endDate = new Date('2020-06-04T00:00:00.000Z')

    const returnFromGoogleFit = {
      bucket: [
        {
          startTimeMillis: '1591048800000',
          endTimeMillis: '1591085072795',
          dataset: [
            {
              dataSourceId: 'derived:com.google.step_count.delta:com.google.android.gms:aggregated',
              point: [
                {
                  startTimeNanos: '1591066109466000000',
                  endTimeNanos: '1591066169466000000',
                  dataTypeName: 'com.google.step_count.delta',
                  originDataSourceId:
                    'derived:com.google.step_count.cumulative:com.google.android.gms:samsung:SM-A600FN:a5c59fa054e2b9a3:soft_step_counter',
                  value: [{ intVal: 1501, mapVal: [] }]
                }
              ]
            }
          ]
        },
        {
          startTimeMillis: '1591135200000',
          endTimeMillis: '1591085072795',
          dataset: [
            {
              dataSourceId: 'derived:com.google.step_count.delta:com.google.android.gms:aggregated',
              point: []
            }
          ]
        }
      ]
    }

    this.fetchStub.returns({
      json: () => returnFromGoogleFit
    })

    this.refreshAccessTokenStub.returns('new_access_token')

    const result = await this.getActivityData(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )

    const resultDate = new Date(parseInt(returnFromGoogleFit.bucket[0].startTimeMillis, 10))
    const finalDate = new Date(
      moment(resultDate)
        .tz('Europe/Brussels')
        .format('YYYY-MM-DD')
    )

    const expectedResult = [
      {
        accountId: this.accountId,
        trackerType: TrackerType.googleType,
        date: finalDate,
        steps: 1501
      }
    ]

    console.log(result)
    console.log('=========')
    console.log(expectedResult)
    result.should.be.ok()
    result[0].steps.should.be.equal(expectedResult[0].steps)
    result.should.deepEqual(expectedResult)
  })

  it('should throw 400 wen payload contains errors', async function () {
    this.startDate = new Date('2020-04-01')
    this.endDate = new Date('2020-04-04')
    this.initialToken = 'initial-token'
    const error = 'Bad Request'
    this.fetchStub.returns({
      json: () => ({
        error: error
      })
    })
    const result = await this.getActivityData(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    ).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
    result.output.payload.error.should.equal(error)
  })
})
