/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
// const registerPolarUser = require('../../../../../../lib/server/business/account/trackerConnection/registerUserToPolar')
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')

function mockResponse (body, status) {
  const mockResponse = {
    status,
    json: () => body
  }

  return Promise.resolve(mockResponse)
}

describe(testName, () => {
  beforeEach(async function () {
    this.mode = `automated-test-${uuidv4}`
    this.flowId = uuidv4()
    this.token = { token: 'amazing-token' }
    this.fetchStub = sinon.stub()
    this.registerProxy = proxyquire(
      '../../../../../../lib/server/business/account/trackerConnection/registerUserToPolar',
      {
        'node-fetch': this.fetchStub
      }
    )
  })

  it('should return polarId', async function () {
    const accountId = 'Id9999'
    const responseBody = { 'polar-user-id': 456789 }
    this.fetchStub.returns(mockResponse(responseBody, 200))
    const result = await this.registerProxy(this.mode, this.flowId, accountId, this.token)
    result.should.be.equal(456789)
  })

  it('should return undefined', async function () {
    const accountId = 'test-this-amazing-account'
    this.fetchStub.returns(mockResponse({}, 409))
    const result = await this.registerProxy(this.mode, this.flowId, accountId, this.token)
    // eslint-disable-next-line no-undef
    should.not.exist(result)
  })
})
