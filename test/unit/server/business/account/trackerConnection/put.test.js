/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const config = require('config')
const util = require('util')
const dynamodb = require('../../../../../../lib/server/aws/dynamodb/dynamodb')
const { v4: uuidv4 } = require('uuid')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const TrackerType = require('../../../../../../lib/api/account/trackerConnection/TrackerType')
const TokenType = require('../../../../../../lib/server/business/account/trackerConnection/TokenType')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbInstance = await dynamodb()

    this.testUuid = uuidv4()
    this.flowId = '755ede2a-8e9e-4c15-b254-4cdafba28dff'
    this.mode = `automated-test-${this.testUuid}`
    this.createdAt = new Date().toISOString()
    this.createdBy = 'id-of-the-creator'
    this.accountId = uuidv4()

    this.dynamoDbKey = `/${this.mode}/account/${this.accountId}/trackerConnection`

    this.itemData = {
      structureVersion: 1,
      trackerType: TrackerType.fitbitType,
      code: 'this-is-a-code',
      redirectUrl: 'https://localhost:4200/fitbit.html',
      hasError: false
    }

    this.getTokenStub = sinon.stub().returns({ token: 'token', tokenType: TokenType.refreshType })
    this.encryptStub = sinon.stub().returns('WA==')
    this.registerPolarUser = sinon.stub().returns(456789)
    this.put = proxyquire('../../../../../../lib/server/business/account/trackerConnection/put', {
      './getToken': this.getTokenStub,
      '../../../aws/crypto/encrypt': this.encryptStub,
      './registerUserToPolar': this.registerPolarUser
    })

    this.put.contract.verifyPostconditions = true

    this.hasArrived = async function () {
      const params = {
        TableName: config.dynamodb.test.tableName,
        ExpressionAttributeNames: {
          '#key': 'key'
        },
        ExpressionAttributeValues: {
          ':key': this.dynamoDbKey
        },
        KeyConditionExpression: '#key = :key',
        Limit: 2,
        ConsistentRead: true
      }
      const result = await this.dynamodbInstance.query(params).promise()
      console.log(`item ${util.inspect(result)} has arrived`)
      return result
    }
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    this.put.contract.verifyPostconditions = false

    await Promise.all([
      this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: {
            key: this.dynamoDbKey,
            submitted: this.createdAt
          }
        })
        .promise(),
      this.dynamodbInstance
        .delete({
          TableName: config.dynamodb.test.tableName,
          Key: {
            key: this.dynamoDbKey,
            submitted: 'actual'
          }
        })
        .promise()
    ])
    console.log(`deleted item ${this.dynamoDbKey}`)
    delete this.dynamoDbKey
  })

  it('should work', async function () {
    await this.put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        accountId: this.accountId
      },
      this.itemData
    )
    const retrieved = await this.hasArrived()
    console.log(
      'Retrieved',
      retrieved.Items.map(item => item.data)
    )
    retrieved.ScannedCount.should.equal(2)
    retrieved.Items[0].key.should.equal(this.dynamoDbKey)
    this.getTokenStub.should.have.been.called()
    this.encryptStub.should.have.been.called()
  })

  it('should work (Polar)', async function () {
    const itemData = {
      structureVersion: 1,
      trackerType: TrackerType.polarType,
      code: 'this-is-a-code',
      redirectUrl: 'https://localhost:4200/polar.html',
      hasError: false
    }

    await this.put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        accountId: this.accountId
      },
      itemData
    )
    const retrieved = await this.hasArrived()
    console.log(
      'Retrieved',
      retrieved.Items.map(item => item.data)
    )
    retrieved.ScannedCount.should.equal(2)
    retrieved.Items[0].key.should.equal(this.dynamoDbKey)
    this.getTokenStub.should.have.been.called()
    this.encryptStub.should.have.been.called()
    this.registerPolarUser.should.have.been.called()
  })

  it('should work for automatedTest', async function () {
    await this.put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      {
        accountId: this.accountId
      },
      {
        ...this.itemData,
        trackerType: TrackerType.automatedTestType
      }
    )
    const retrieved = await this.hasArrived()
    retrieved.ScannedCount.should.equal(2)
    retrieved.Items[0].key.should.equal(this.dynamoDbKey)
    this.getTokenStub.should.not.have.been.called()
  })

  it('should throw a badRequest', async function () {
    const itemData = {
      ...this.itemData,
      id: 'wrong id'
    }
    const exc = await this.put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      { accountId: this.teamId },
      itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should throw a badRequest when path parameters are invalid', async function () {
    const exc = await this.put(
      this.createdAt,
      this.createdBy,
      this.mode,
      this.flowId,
      { teamId: 'wrong id' },
      this.itemData
    ).should.be.rejected()
    console.log(exc)
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(400)
  })

  it('should set error to true', async function () {
    this.itemData.hasError = true
    await this.put(this.createdAt, this.createdBy, this.mode, this.flowId, { accountId: this.accountId }, this.itemData)
    const retrieved = await this.hasArrived()
    console.log(
      'Retrieved',
      retrieved.Items.map(item => item.data)
    )
    retrieved.Items[0].data.hasError.should.be.true()
  })
})
