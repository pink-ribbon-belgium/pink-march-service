/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const sinon = require('sinon')
const proxyquire = require('proxyquire')
const uuidv4 = require('uuid').v4

const testName = require('../../../../../../_testName')(module)
const TokenType = require('../../../../../../../lib/server/business/account/trackerConnection/TokenType')
const TrackerType = require('../../../../../../../lib/api/account/trackerConnection/TrackerType')

function mockResponse (body, status) {
  const mockResponse = {
    status,
    json: () => body
  }

  return Promise.resolve(mockResponse)
}

describe(testName, () => {
  beforeEach(async function () {
    this.fetchStub = sinon.stub()
    this.decryptStub = sinon.stub()
    this.writeTrackerConnectionStub = sinon.stub()

    this.getPolarDataProxy = proxyquire(
      '../../../../../../../lib/server/business/account/trackerConnection/polar/getPolarData',
      {
        'node-fetch': this.fetchStub,
        '../../../../aws/crypto/decrypt': this.decryptStub,
        '../write': this.writeTrackerConnectionStub
      }
    )

    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2020-06-23T12:25:26.918Z'
    this.sub = 'sub-polar-test'
    this.accountId = 'Idfsqsdf46'
    this.trackerType = TrackerType.polarType
    this.itemData = {
      accountId: this.accountId,
      trackerType: this.trackerType,
      tokenType: TokenType.accessType,
      encryptedToken: 'WA==',
      exp: '2020-07-09T18:30:25.125Z',
      hasError: false,
      createdAt: '2020-07-09T08:30:25.125Z',
      createdBy: this.accountId,
      structureVersion: 1,
      polarId: 4985621
    }

    this.getPolarDataProxy.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.getPolarDataProxy.contract.verifyPostconditions = false
  })

  it('should work', async function () {
    this.startDate = new Date('2020-04-01')
    this.endDate = new Date('2020-04-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }
    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    const activitiesBody = {
      'activity-log': [
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653894'
      ]
    }
    const activityDataBody = {
      id: 1234,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 250
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse(activitiesBody, 200))
    this.fetchStub.onCall(3).returns(mockResponse(activityDataBody, 200))
    this.fetchStub.onCall(4).resolves()
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)

    const expectedResult = [
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-15'),
        steps: 250
      }
    ]
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return empty array when no notifications are found', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {}

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 204))
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    result.length.should.equal(0)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return empty array when no data is available for user', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4485621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4485621/activity-transactions'
        }
      ]
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    result.length.should.equal(0)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return empty array when no data is available for transaction', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse({}, 204))
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    result.length.should.equal(0)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return empty array when no data is found or no transaction was found', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }

    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse({}, 204))
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    result.length.should.equal(0)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return only activities with data', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }

    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    const activitiesBody = {
      'activity-log': [
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653894',
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653909'
      ]
    }

    const activityDataBody = {
      id: 1234,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 250
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse(activitiesBody, 200))
    this.fetchStub.onCall(3).returns(mockResponse(activityDataBody, 200))
    this.fetchStub.onCall(4).returns(mockResponse({}, 204))
    this.fetchStub.onCall(5).resolves()
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    const expectedResult = [
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-15'),
        steps: 250
      }
    ]
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return only latest activity data', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }

    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    const activitiesBody = {
      'activity-log': [
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653894',
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653909'
      ]
    }

    const activityDataBody = {
      id: 1234,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 250
    }

    const activityDataBody2 = {
      id: 1235,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 350
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse(activitiesBody, 200))
    this.fetchStub.onCall(3).returns(mockResponse(activityDataBody, 200))
    this.fetchStub.onCall(4).returns(mockResponse(activityDataBody2, 200))
    this.fetchStub.onCall(5).resolves()
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    const expectedResult = [
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-15'),
        steps: 350
      }
    ]
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should return unique activity data', async function () {
    this.startDate = new Date('2020-05-01')
    this.endDate = new Date('2020-05-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }

    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    const activitiesBody = {
      'activity-log': [
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653894',
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653909',
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653957'
      ]
    }

    const activityDataBody = {
      id: 1234,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 250
    }

    const activityDataBody2 = {
      id: 1235,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 350
    }

    const activityDataBody3 = {
      id: 1235,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-16',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 100
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse(activitiesBody, 200))
    this.fetchStub.onCall(3).returns(mockResponse(activityDataBody, 200))
    this.fetchStub.onCall(4).returns(mockResponse(activityDataBody2, 200))
    this.fetchStub.onCall(5).returns(mockResponse(activityDataBody3, 200))
    this.fetchStub.onCall(6).resolves()
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)
    const expectedResult = [
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-15'),
        steps: 350
      },
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-16'),
        steps: 100
      }
    ]
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })

  it('should work with production mode', async function () {
    this.startDate = new Date('2020-04-01')
    this.endDate = new Date('2020-04-04')

    const notificationsBody = {
      'available-user-data': [
        {
          'user-id': 4985621,
          'data-type': 'ACTIVITY_SUMMARY',
          url: 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions'
        }
      ]
    }
    const createTransactionBody = {
      'transaction-id': 275434334,
      'resource-uri': 'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334'
    }

    const activitiesBody = {
      'activity-log': [
        'https://www.polaraccesslink.com/v3/users/4985621/activity-transactions/275434334/activities/617653894'
      ]
    }
    const activityDataBody = {
      id: 1234,
      'polar-user': 'https://www.polaraccesslink/v3/users/4985621',
      'transaction-id': 275434334,
      date: '2020-05-15',
      created: '2020-04-27T20:11:33.000Z',
      calories: 2329,
      'active-calories': 428,
      duration: 'PT2H44M',
      'active-steps': 250
    }

    this.fetchStub.onCall(0).returns(mockResponse(notificationsBody, 200))
    this.fetchStub.onCall(1).returns(mockResponse(createTransactionBody, 201))
    this.fetchStub.onCall(2).returns(mockResponse(activitiesBody, 200))
    this.fetchStub.onCall(3).returns(mockResponse(activityDataBody, 200))
    this.fetchStub.onCall(4).resolves()
    this.decryptStub.returns('access_token')

    const result = await this.getPolarDataProxy(
      this.sot,
      this.sub,
      'production',
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )
    console.log(result)

    const expectedResult = [
      {
        accountId: 'Idfsqsdf46',
        trackerType: 'polar',
        date: new Date('2020-05-15'),
        steps: 250
      }
    ]
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
    this.writeTrackerConnectionStub.should.be.calledOnce()
  })
})
