/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const getLeastRecentlyUpdated = require('../../../../../../lib/server/business/account/trackerConnection/getLeastRecentlyUpdated')
const { v4: uuidv4 } = require('uuid')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const TrackerConnection = require('../../../../../../lib/server/business/account/trackerConnection/TrackerConnection')

const itemCreatedAt = '2019-04-02T10:07:24.911Z'
const itemCreatedBy = 'sub-updateActivities-test'
const itemAccountId = 'accountId-updateActivities-test'
const itemExp = '2022-04-02T10:07:24.911Z'

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.trackerConnection = {
      createdAt: itemCreatedAt,
      createdBy: itemCreatedBy,
      structureVersion: 1,
      accountId: itemAccountId,
      trackerType: 'fitbit',
      tokenType: 'refresh',
      encryptedToken: 'WA==',
      exp: itemExp
    }
    getLeastRecentlyUpdated.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getLeastRecentlyUpdated.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    const flowId = '47a6600e-44c9-4a89-96cd-faddeb7079fb'
    const sot = '2020-04-02T10:07:24.911Z'
    const itemCreatedAt = '2019-04-02T10:07:24.911Z'
    const itemCreatedAt2 = '2019-05-02T10:07:24.911Z'
    const itemCreatedAt3 = '2022-04-02T10:07:24.911Z'
    const itemCreatedBy = 'sub-updateActivities-test'
    const itemAccountId = 'accountId-updateActivities-test'
    const nrOfTrackerConnectionsToPoll = 2

    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        createdAt: itemCreatedAt,
        createdBy: itemCreatedBy,
        structureVersion: 1,
        accountId: itemAccountId,
        deleted: false
      }
    })
    const trackerConnection2 = new TrackerConnection({
      mode: this.mode,
      dto: {
        createdAt: itemCreatedAt2,
        createdBy: itemCreatedBy,
        structureVersion: 1,
        accountId: 'account-id',
        deleted: false
      }
    })

    const trackerConnection3 = new TrackerConnection({
      mode: this.mode,
      dto: {
        createdAt: itemCreatedAt3,
        createdBy: itemCreatedBy,
        structureVersion: 1,
        accountId: 'test-account-id',
        deleted: false
      }
    })
    await Promise.all([
      this.dynamodbFixture.putItem({
        ...trackerConnection.toItem(),
        submitted: 'actual',
        sort_key_A: itemCreatedAt,
        sort_key_B: itemCreatedAt,
        flowId: flowId
      }),
      this.dynamodbFixture.putItem({
        ...trackerConnection3.toItem(),
        submitted: 'actual',
        sort_key_A: itemCreatedAt3,
        sort_key_B: itemCreatedAt3,
        flowId: flowId
      }),
      this.dynamodbFixture.putItem({
        ...trackerConnection2.toItem(),
        submitted: 'actual',
        sort_key_A: itemCreatedAt2,
        sort_key_B: itemCreatedAt2,
        flowId: flowId
      })
    ])
    const result = await getLeastRecentlyUpdated(flowId, sot, this.mode, nrOfTrackerConnectionsToPoll)
    result.should.be.an.Array()
    result.length.should.be.belowOrEqual(nrOfTrackerConnectionsToPoll)
  })
})
