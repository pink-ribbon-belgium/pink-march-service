/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../../_testName')(module)
const deleteTrackerConnection = require('../../../../../../lib/server/business/account/trackerConnection/delete')
const DynamodbFixture = require('../../../../../_DynamodbFixture')
const uuidv4 = require('uuid').v4
const TrackerType = require('../../../../../../lib/api/account/trackerConnection/TrackerType')
const TokenType = require('../../../../../../lib/server/business/account/trackerConnection/TokenType')
const TrackerConnection = require('../../../../../../lib/server/business/account/trackerConnection/TrackerConnection')
const getVersionAtOrBefore = require('../../../../../../lib/server/aws/dynamodb/getVersionAtOrBefore')
const getActual = require('../../../../../../lib/server/aws/dynamodb/getActual')
const dynamodbTableName = require('../../../../../../lib/server/aws/dynamodb/dynamodbTableName')
const sinon = require('sinon')
const proxyquire = require('proxyquire')

describe(testName, () => {
  beforeEach(async function () {
    this.sot = '2020-03-31T12:25:26.918Z'
    this.sub = 'this-is-a-sub'
    this.mode = `automated-test-${uuidv4()}`
    this.flowId = uuidv4()
    this.accountId = uuidv4()
    this.dynamodbFixture = new DynamodbFixture()
    deleteTrackerConnection.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    await this.dynamodbFixture.clean()
    deleteTrackerConnection.contract.verifyPostconditions = false
  })

  it('should work (Fitbit)', async function () {
    const sot = '2020-03-31T08:30:25.125Z'
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        accountId: this.accountId,
        trackerType: TrackerType.fitbitType,
        tokenType: 'refresh',
        encryptedToken: 'WA==',
        exp: '2020-03-31T08:30:25.125Z',
        hasError: false,
        createdAt: sot,
        createdBy: 'this-is-a-sub',
        structureVersion: 1
      }
    })
    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: sot,
      sort_key_B: sot,
      flowId: this.flowId
    })
    await deleteTrackerConnection(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    })
    const written = await getVersionAtOrBefore(
      this.mode,
      `/${this.mode}/account/${this.accountId}/trackerConnection`,
      new Date().toISOString(),
      true
    )
    written.should.be.ok()
    this.dynamodbFixture.remember(written.key, written.submitted, dynamodbTableName(this.mode))
    this.dynamodbFixture.remember(written.key, 'actual', dynamodbTableName(this.mode))
    written.data.deleted.should.be.true()
    written.data.accountId.should.equal(this.accountId)
  })

  it('should work (Polar)', async function () {
    const fetchStub = sinon.stub()
    this.decryptStub = sinon.stub()
    this.decryptStub.resolves('some-decrypted-token')
    const deleteProxy = proxyquire('../../../../../../lib/server/business/account/trackerConnection/delete', {
      'node-fetch': fetchStub,
      '../../../aws/crypto/decrypt': this.decryptStub
    })

    const sot = '2020-04-13T08:30:25.125Z'
    const sub = 'polar-sub'
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        accountId: this.accountId,
        trackerType: TrackerType.polarType,
        tokenType: TokenType.accessType,
        encryptedToken: 'WA==',
        exp: '2020-06-04T08:30:25.125Z',
        hasError: false,
        createdAt: sot,
        createdBy: sub,
        structureVersion: 1,
        polarId: 4535874
      }
    })

    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: sot,
      sort_key_B: sot,
      flowId: this.flowId
    })

    await deleteProxy(sot, sub, this.mode, this.flowId, {
      accountId: this.accountId
    })

    const written = await getActual(this.mode, `/${this.mode}/account/${this.accountId}/trackerConnection`, true)
    console.log('Latest version ::::::', written)
    written.should.be.ok()
    this.dynamodbFixture.remember(written.key, written.submitted, dynamodbTableName(this.mode))
    this.dynamodbFixture.remember(written.key, 'actual', dynamodbTableName(this.mode))
    written.data.deleted.should.be.true()
    written.data.accountId.should.equal(this.accountId)

    fetchStub.should.be.called()
  })

  it('should work (GoogleFit)', async function () {
    const fechtRevokeGooglePermissionStub = sinon.stub()
    this.decryptStub = sinon.stub()
    this.decryptStub.resolves('some-decrypted-token')
    const deleteProxy = proxyquire('../../../../../../lib/server/business/account/trackerConnection/delete', {
      'node-fetch': fechtRevokeGooglePermissionStub,
      '../../../aws/crypto/decrypt': this.decryptStub
    })

    const sot = '2020-03-31T08:30:25.125Z'
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        accountId: this.accountId,
        trackerType: TrackerType.googleType,
        tokenType: 'refresh',
        encryptedToken: 'WA==',
        exp: '2020-03-31T08:30:25.125Z',
        hasError: false,
        createdAt: sot,
        createdBy: 'this-is-a-sub',
        structureVersion: 1
      }
    })
    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: sot,
      sort_key_B: sot,
      flowId: this.flowId
    })
    await deleteProxy(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    })

    fechtRevokeGooglePermissionStub.should.be.called()
  })

  it('should do nothing when actual is already deleted', async function () {
    const sot = '2020-03-31T08:30:25.125Z'
    const trackerConnection = new TrackerConnection({
      mode: this.mode,
      dto: {
        accountId: this.accountId,
        deleted: true,
        createdAt: sot,
        createdBy: 'this-is-a-sub',
        structureVersion: 1
      }
    })
    await this.dynamodbFixture.putItem({
      ...trackerConnection.toItem(),
      submitted: 'actual',
      sort_key_A: sot,
      sort_key_B: sot,
      flowId: this.flowId
    })
    const putTransactionStub = sinon.stub()

    const deleteProxy = proxyquire('../../../../../../lib/server/business/account/trackerConnection/delete', {
      '../../../aws/dynamodb/putTransaction': putTransactionStub
    })
    await deleteProxy(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    })
    putTransactionStub.should.not.have.been.called()
  })

  it('should throw 404', async function () {
    const result = await deleteTrackerConnection(this.sot, this.sub, this.mode, this.flowId, {
      accountId: this.accountId
    }).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(404)
  })

  it('should throw 400', async function () {
    const result = await deleteTrackerConnection(this.sot, this.sub, this.mode, this.flowId, {}).should.be.rejected()
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
  })
})
