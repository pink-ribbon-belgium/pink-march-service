/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const sinon = require('sinon')
const proxyquire = require('proxyquire')
const uuidv4 = require('uuid').v4

const testName = require('../../../../../../_testName')(module)
const TokenType = require('../../../../../../../lib/server/business/account/trackerConnection/TokenType')
const TrackerType = require('../../../../../../../lib/api/account/trackerConnection/TrackerType')

describe(testName, () => {
  beforeEach(async function () {
    this.refreshAccessTokenStub = sinon.stub()
    this.fetchStub = sinon.stub()
    this.decryptStub = sinon.stub()
    this.getActivityData = proxyquire(
      '../../../../../../../lib/server/business/account/trackerConnection/fitbit/getActivityData',
      {
        'node-fetch': this.fetchStub,
        './refreshAccessToken': this.refreshAccessTokenStub,
        '../../../../aws/crypto/decrypt': this.decryptStub
      }
    )

    this.flowId = uuidv4()
    this.mode = `automated-test-${uuidv4()}`
    this.sot = '2020-03-31T12:25:26.918Z'
    this.sub = 'sub-for-getActivityData-test'

    this.accountId = '4c493368-a7a2-4371-91eb-3925c993cb69'
    this.trackerType = TrackerType.fitbitType

    this.itemData = {
      accountId: this.accountId,
      trackerType: this.trackerType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: '2020-04-09T18:30:25.125Z',
      hasError: false,
      createdAt: '2020-04-09T08:30:25.125Z',
      createdBy: this.accountId,
      structureVersion: 1
    }

    this.getActivityData.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.getActivityData.contract.verifyPostconditions = false
    // await this.dynamodbFixture.clean()
  })

  it('should work', async function () {
    this.startDate = new Date('2020-04-01')
    this.endDate = new Date('2020-04-04')

    this.fetchStub.returns({
      json: () => ({
        'activities-steps': [
          { dateTime: '2020-04-01', value: '10' },
          { dateTime: '2020-04-02', value: '20' },
          { dateTime: '2020-04-03', value: '30' },
          { dateTime: '2020-04-04', value: '40' }
        ]
      })
    })

    this.decryptStub.returns('initial_token')
    this.refreshAccessTokenStub.returns('new_access_token')

    const result = await this.getActivityData(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    )

    const expectedResult = [
      { accountId: this.accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-04-01'), steps: 10 },
      { accountId: this.accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-04-02'), steps: 20 },
      { accountId: this.accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-04-03'), steps: 30 },
      { accountId: this.accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-04-04'), steps: 40 }
    ]

    console.log(result)
    result.should.be.ok()
    result.should.deepEqual(expectedResult)
  })

  it('should throw 400 when payload contains errors', async function () {
    this.initialToken = 'initial-token'
    const error = {
      error_type: 'some_error',
      error_description: 'some description'
    }
    this.fetchStub.returns({
      json: () => ({
        errors: [error]
      })
    })
    const result = await this.getActivityData(
      this.sot,
      this.sub,
      this.mode,
      this.flowId,
      this.itemData,
      this.startDate,
      this.endDate
    ).should.be.rejected()
    console.log(result)
    result.isBoom.should.be.true()
    result.output.statusCode.should.equal(400)
    result.output.payload.error.should.equal(error.error_type)
    result.output.payload.message.should.equal(error.error_description)
  })
})
