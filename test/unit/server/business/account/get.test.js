/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const get = require('../../../../../lib/server/business/account/get')
const generateGetTest = require('../_generateGetTest')

const accountId = 'id-of-the-account'

describe(testName, function () {
  generateGetTest(
    get,
    (sot, sub) => ({
      createdAt: sot,
      createdBy: sub,
      structureVersion: 66,
      id: accountId,
      sub: 'DJI8Dk0DDeW',
      email: 'iets@test.com'
    }),
    mode => `/${mode}/account/${accountId}`,
    { accountId: 'id' },
    () => ({
      accountId
    }),
    () => ({
      accountId: 'does-not-exist'
    }),
    () => ({
      accountId: 4524
    }),
    () => ({
      preferences: `/I/account/${accountId}/preferences`,
      publicProfile: `/I/account/${accountId}/publicProfile`,
      slot: `/I/account/${accountId}/slot`,
      administratorOf: `/I/account/${accountId}/administratorOf`
    })
  )
})
