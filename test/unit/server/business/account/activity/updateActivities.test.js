/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const sinon = require('sinon')
const proxyquire = require('proxyquire')
const { v4: uuidv4 } = require('uuid')

const testName = require('../../../../../_testName')(module)
const TrackerType = require('../../../../../../lib/api/account/trackerConnection/TrackerType')
const TokenType = require('../../../../../../lib/server/business/account/trackerConnection/TokenType')
const updateActivities = require('../../../../../../lib/server/business/account/activity/updateActivities')

const flowId = '47a6600e-44c9-4a89-96cd-faddeb7079fb'
const sot = '2020-06-02T10:07:24.911Z'
const accountId = 'accountId-updateActivities-test'
const trackerCreatedAt = '2019-04-02T10:07:24.911Z'
const trackerCreatedBy = 'sub-updateActivities-test'
const trackerExp = '2022-04-02T10:07:24.911Z'

const lastActivityDate = '2020-06-01T00:00:00.000Z'

describe(testName, function () {
  beforeEach(function () {
    this.mode = `automated-test-${uuidv4()}`

    this.getLastActivityStub = sinon.stub()
    this.getActivityDataFitbitStub = sinon.stub()
    this.getActivityDataGoogleFitStub = sinon.stub()
    this.getActivityDataPolarStub = sinon.stub()
    this.putActivityStub = sinon.stub()
    this.getAccountProfileStub = sinon.stub()
    this.writeTrackerConnection = sinon.stub()

    this.updateActivities = proxyquire('../../../../../../lib/server/business/account/activity/updateActivities', {
      '../../activity/getLastActivity': this.getLastActivityStub,
      '../trackerConnection/fitbit/getActivityData': this.getActivityDataFitbitStub,
      '../trackerConnection/googlefit/getGoogleFitData': this.getActivityDataGoogleFitStub,
      '../trackerConnection/polar/getPolarData': this.getActivityDataPolarStub,
      '../../activity/put': this.putActivityStub,
      '../../account/profile/get': this.getAccountProfileStub,
      '../../../business/account/trackerConnection/write': this.writeTrackerConnection
    })

    this.trackerConnectionData = {
      accountId: accountId,
      trackerType: TrackerType.fitbitType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: trackerExp,
      hasError: false,
      createdAt: trackerCreatedAt,
      createdBy: trackerCreatedBy,
      structureVersion: 1
    }

    this.googleConnectionData = {
      accountId: accountId,
      trackerType: TrackerType.googleType,
      tokenType: TokenType.refreshType,
      encryptedToken: 'WA==',
      exp: trackerExp,
      hasError: false,
      createdAt: trackerCreatedAt,
      createdBy: trackerCreatedBy,
      structureVersion: 1
    }

    this.polarConnectionData = {
      accountId: accountId,
      trackerType: TrackerType.polarType,
      tokenType: TokenType.accessType,
      encryptedToken: 'WA==',
      exp: trackerExp,
      hasError: false,
      createdAt: trackerCreatedAt,
      createdBy: trackerCreatedBy,
      structureVersion: 1,
      polarId: 4685231
    }

    this.getAccountProfileStub.resolves({
      gender: 'M'
    })

    this.updateActivities.contract.verifyPostconditions = true
  })

  afterEach(function () {
    updateActivities.contract.verifyPostconditions = false
  })

  it('works (Fitbit with existing activity)', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: lastActivityDate,
      trackerType: TrackerType.fitbitType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataFitbitStub.returns([
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-01'), steps: 10 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-02'), steps: 20 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-03'), steps: 30 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-04'), steps: 40 }
    ])

    await this.updateActivities(flowId, sot, this.mode, this.trackerConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.trackerConnectionData.accountId)
    this.getActivityDataFitbitStub.should.be.calledWithMatch(
      sot,
      'updateActivities',
      this.mode,
      flowId,
      this.trackerConnectionData,
      new Date(lastActivityDate),
      sinon.match.date
    )
    this.getAccountProfileStub.should.be.called()
    this.putActivityStub.should.be.callCount(4)
  })

  it('works (last activity is manual & startdate < enddate) => do nothing', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: new Date('2020-06-01'),
      trackerType: TrackerType.manualType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataFitbitStub.returns([
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-02'), steps: 10 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-03'), steps: 20 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-04'), steps: 30 },
      { accountId: accountId, trackerType: TrackerType.fitbitType, date: new Date('2020-06-05'), steps: 40 }
    ])

    await this.updateActivities(flowId, sot, this.mode, this.trackerConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.trackerConnectionData.accountId)
    this.getActivityDataFitbitStub.should.be.called()
    this.getAccountProfileStub.should.be.called()
    this.putActivityStub.should.be.called()
  })

  it('works (startdate > enddate) => do nothing', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: new Date('2020-06-30'),
      trackerType: TrackerType.manualType,
      numberOfSteps: 3000,
      deleted: false
    })

    await this.updateActivities(flowId, sot, this.mode, this.trackerConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.trackerConnectionData.accountId)
    this.getActivityDataFitbitStub.should.not.be.called()
    this.getAccountProfileStub.should.not.be.called()
    this.putActivityStub.should.not.be.called()
    this.writeTrackerConnection.should.be.called()
  })

  it('works (GoogleFit with existing activity)', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: lastActivityDate,
      trackerType: TrackerType.googleType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataGoogleFitStub.returns([
      { accountId: accountId, trackerType: TrackerType.googleType, date: new Date('2020-06-01'), steps: 10 },
      { accountId: accountId, trackerType: TrackerType.googleType, date: new Date('2020-06-02'), steps: 20 },
      { accountId: accountId, trackerType: TrackerType.googleType, date: new Date('2020-06-03'), steps: 30 },
      { accountId: accountId, trackerType: TrackerType.googleType, date: new Date('2020-06-04'), steps: 40 }
    ])

    await this.updateActivities(flowId, sot, this.mode, this.googleConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.googleConnectionData.accountId)
    this.getActivityDataGoogleFitStub.should.be.calledWithMatch(
      sot,
      'updateActivities',
      this.mode,
      flowId,
      this.googleConnectionData,
      new Date(lastActivityDate),
      sinon.match.date
    )
    this.getAccountProfileStub.should.be.called()
    this.putActivityStub.should.be.callCount(4)
  })

  // it('works (Polar with existing activity)', async function () {
  //   this.getLastActivityStub.returns({
  //     createdAt: sot,
  //     createdBy: 'some-account',
  //     structureVersion: 1,
  //     accountId: accountId,
  //     activityDate: lastActivityDate,
  //     trackerType: TrackerType.polarType,
  //     numberOfSteps: 3000,
  //     deleted: false
  //   })
  //
  //   this.getActivityDataPolarStub.returns([
  //     { accountId: accountId, trackerType: TrackerType.polarType, date: new Date('2020-07-01'), steps: 100 },
  //     { accountId: accountId, trackerType: TrackerType.polarType, date: new Date('2020-07-02'), steps: 250 }
  //   ])
  //
  //   await this.updateActivities(flowId, sot, this.mode, this.polarConnectionData)
  //
  //   this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.polarConnectionData.accountId)
  //   this.getActivityDataPolarStub.should.be.calledWithMatch(
  //     sot,
  //     'updateActivities',
  //     this.mode,
  //     flowId,
  //     this.polarConnectionData,
  //     new Date(lastActivityDate),
  //     sinon.match.date
  //   )
  //   this.getAccountProfileStub.should.be.called()
  //   this.putActivityStub.should.be.callCount(2)
  // })

  it('works (Fitbit - No new data)', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: lastActivityDate,
      trackerType: TrackerType.fitbitType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataFitbitStub.returns([])

    await this.updateActivities(flowId, sot, this.mode, this.trackerConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.trackerConnectionData.accountId)
    this.getActivityDataFitbitStub.should.be.calledWithMatch(
      sot,
      'updateActivities',
      this.mode,
      flowId,
      this.trackerConnectionData,
      new Date(lastActivityDate),
      sinon.match.date
    )

    this.putActivityStub.should.not.be.called()
  })

  it('works (GoogleFit - No new data)', async function () {
    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: lastActivityDate,
      trackerType: TrackerType.googleType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataGoogleFitStub.returns([])

    await this.updateActivities(flowId, sot, this.mode, this.googleConnectionData)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.googleConnectionData.accountId)
    this.getActivityDataGoogleFitStub.should.be.calledWithMatch(
      sot,
      'updateActivities',
      this.mode,
      flowId,
      this.googleConnectionData,
      new Date(lastActivityDate),
      sinon.match.date
    )

    this.putActivityStub.should.not.be.called()
  })

  // it('works (Polar - No new data)', async function () {
  //   this.getLastActivityStub.returns({
  //     createdAt: sot,
  //     createdBy: 'some-account',
  //     structureVersion: 1,
  //     accountId: accountId,
  //     activityDate: lastActivityDate,
  //     trackerType: TrackerType.polarType,
  //     numberOfSteps: 3000,
  //     deleted: false
  //   })
  //
  //   this.getActivityDataPolarStub.returns([])
  //
  //   await this.updateActivities(flowId, sot, this.mode, this.polarConnectionData)
  //
  //   this.getLastActivityStub.should.be.calledWithMatch(this.mode, this.polarConnectionData.accountId)
  //   this.getActivityDataPolarStub.should.be.calledWithMatch(
  //     sot,
  //     'updateActivities',
  //     this.mode,
  //     flowId,
  //     this.polarConnectionData,
  //     new Date(lastActivityDate),
  //     sinon.match.date
  //   )
  //
  //   this.putActivityStub.should.not.be.called()
  // })

  it('works (unknow tracker)', async function () {
    const unknownTracker = {
      ...this.trackerConnectionData,
      trackerType: TrackerType.automatedTestType
    }

    this.getLastActivityStub.returns({
      createdAt: sot,
      createdBy: 'some-account',
      structureVersion: 1,
      accountId: accountId,
      activityDate: lastActivityDate,
      trackerType: TrackerType.automatedTestType,
      numberOfSteps: 3000,
      deleted: false
    })

    this.getActivityDataFitbitStub.returns([])

    await this.updateActivities(flowId, sot, this.mode, unknownTracker)

    this.getLastActivityStub.should.be.calledWithMatch(this.mode, unknownTracker.accountId)
    this.getActivityDataFitbitStub.should.not.be.called()
    this.putActivityStub.should.not.be.called()
  })
})
