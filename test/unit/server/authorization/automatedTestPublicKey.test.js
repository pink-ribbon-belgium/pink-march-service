/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const automatedTestPublicKey = require('../../../_dummyToken/_getPublicKey')

describe(testName, function () {
  it('finds the key', async function () {
    const result = await automatedTestPublicKey()
    result.should.be.a.String()
    console.log(result)
  })
  it('finds the key, again', async function () {
    const result1 = await automatedTestPublicKey()
    const result2 = await automatedTestPublicKey()
    result2.should.equal(result1)
  })
})
