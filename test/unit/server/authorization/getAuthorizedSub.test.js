/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const getAuthorizedSub = require('../../../../lib/server/authorization/getAuthorizedSub')
const getDummyToken = require('../../../_dummyToken/create')
const { inspect } = require('util')
const publicSigningKeyGetter = require('../../../_publicSigningKeyGetter')
const eventExample = require('../../../../lib/ppwcode/lambda/apigateway/Event').example
const Hoek = require('@hapi/hoek')
const { v4: uuidv4 } = require('uuid')
const customClaims = require('../../../../lib/server/authorization/customClaims')
const config = require('config')

// seconds since epoch
const aLongLongTimeAgo = new Date(1111, 1, 26, 11, 30, 19, 345).getTime() / 1000
const farFuture = new Date(2222, 1, 26, 11, 30, 19, 345).getTime() / 1000

const tokenGenerators = {
  tokenMalformed: async () => 'fkdsghdjdsgjids.hogweoghweoghowg',
  invalidToken: async () => 'fkdsghdjdsgjids.hogweoghweoghowg.oghweogoweheo',
  invalidAlgorithm: async mode => getDummyToken({ algorithm: 'HS256', [customClaims.modeClaim]: mode }),
  invalidSignature: async () => {
    const goodToken = await getDummyToken()
    return goodToken + 'moreCharactersMakeTheSignatureInvalid'
  },
  tooEarly: async mode => getDummyToken({ nbf: farFuture, [customClaims.modeClaim]: mode }),
  expired: async mode => getDummyToken({ exp: aLongLongTimeAgo, [customClaims.modeClaim]: mode }),
  /* NOTE: returning what we expect would, in general, be giving possible attackers insights they could not otherwise
           get */
  invalidAud: async mode =>
    getDummyToken({ aud: 'this is not the audience you are looking for', [customClaims.modeClaim]: mode }),
  invalidIss: async mode =>
    getDummyToken({ iss: 'this is not the issuer you are looking for', [customClaims.modeClaim]: mode }),
  tooOld: async mode => getDummyToken({ iat: aLongLongTimeAgo, [customClaims.modeClaim]: mode }),
  noSub: async mode => getDummyToken({ [customClaims.modeClaim]: mode }, ['sub']),
  modeDoesNotMatch: async mode =>
    getDummyToken({ [customClaims.modeClaim]: 'this is not the mode you are looking for' }),
  raaMalFormed: async mode =>
    getDummyToken({
      [customClaims.modeClaim]: mode,
      [customClaims.raasClaim]: ['a raa', { it: 'is not a raa' }]
    }),
  noMatchingRAA: async mode =>
    getDummyToken({
      [customClaims.modeClaim]: mode,
      [customClaims.raasClaim]: ['a raa', 'another raa']
    })
}

function create4xxTest (messageKey, statusCode, kind) {
  it(`throws \`${messageKey}\` when expected`, async function () {
    const token = await tokenGenerators[messageKey](this.mode)
    const event = Hoek.clone(eventExample)
    event.headers['x-mode'] = this.mode
    event.headers.authorization = `Bearer ${token}`
    const exc = await getAuthorizedSub(event).should.be.rejectedWith(Error)
    console.log(inspect(exc, { depth: 9999 }))
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(statusCode)
    exc.output.payload.attributes.error_description.should.be.equal(
      getAuthorizedSub.contract.messages[kind][messageKey]
    )
    const wwwAuthenticate = exc.output.headers['x-www-authenticate']
    wwwAuthenticate.should.be.a.String()
    wwwAuthenticate.should.containEql(config.authorization.audience)
    if (messageKey !== 'noSub') {
      // make an exception: Boom double-escapes slashes in the header
      wwwAuthenticate.should.containEql(getAuthorizedSub.contract.messages[kind][messageKey])
    }
    if (exc.output.statusCode === 403) {
      wwwAuthenticate.should.match(
        /^Bearer realm=".*", charset="utf-8", error_description=".*", error="insufficient_scope"$/
      )
      const ra = `${event.httpMethod}:${event.path}`
      exc.output.payload.attributes.ra.should.equal(ra)
    } else {
      wwwAuthenticate.should.match(
        /^Bearer realm=".*", charset="utf-8", error_description=".*", error="invalid_token"$/
      )
    }
  })
}

/* NOTE: This is security. Test whether all possible cases where we expect to get blocked by a 401, are indeed blocked,
         never mind coverage. A misconfiguration of the verify call (like we DID encounter during testing) is fatal, and
         creates a security leak.
         -
         However `excluded` are _not tested_, because it is extremely difficult to create a token which (only) violates
         these conditions, and there are other possible errors we cannot enumerate.
         */
const excluded = [
  /* cannot happen because of Joi validation on event in handler */ 'noToken',
  'tokenNotSigned',
  'invalidNbf',
  'invalidExp',
  'invalidIat'
]

describe(testName, function () {
  beforeEach(function () {
    getAuthorizedSub.contract.verifyPostconditions = true
    this.mode = `automated-test-${uuidv4()}`

    /* NOTE: Dirty workaround to be able to communicate data from a test fixture to deep stub code. This is fragile.
             We know. Any better ideas? */
    publicSigningKeyGetter.dummyToken = true
  })

  afterEach(function () {
    publicSigningKeyGetter.dummyToken = false

    getAuthorizedSub.contract.verifyPostconditions = false
  })

  it('returns the sub if there is one', async function () {
    const sub = 'thisIsASub'
    const event = Hoek.clone(eventExample)
    event.headers['x-mode'] = this.mode
    const ra = `${event.httpMethod}:${event.path}`
    const token = await getDummyToken({
      sub,
      [customClaims.modeClaim]: this.mode,
      [customClaims.raasClaim]: ['/something/else/entirely', `${ra}**`]
    })
    event.headers.authorization = `Bearer ${token}`
    const result = await getAuthorizedSub(event)
    result.should.equal(sub)
    console.log(result)
  })
  it('URI encodes the sub', async function () {
    const sub = 'this is not an account id |$?%!#'
    const expected = encodeURIComponent(sub)
    const event = Hoek.clone(eventExample)
    const ra = `${event.httpMethod}:${event.path}`
    const token = await getDummyToken({
      sub,
      [customClaims.modeClaim]: this.mode,
      [customClaims.raasClaim]: ['/something/else/entirely', `${ra}**`]
    })
    event.headers['x-mode'] = this.mode
    event.headers.authorization = `Bearer ${token}`
    const result = await getAuthorizedSub(event)
    result.should.equal(expected)
    console.log(result)
  })
  Object.keys(getAuthorizedSub.contract.messages.unauthorized)
    .filter(m => !excluded.includes(m))
    .forEach(messageKey => {
      create4xxTest(messageKey, 401, 'unauthorized')
    })
  Object.keys(getAuthorizedSub.contract.messages.forbidden)
    .filter(m => !excluded.includes(m))
    .forEach(messageKey => {
      create4xxTest(messageKey, 403, 'forbidden')
    })
  it("throws 'noToken' when there is no authorization header", async function () {
    const event = Hoek.clone(eventExample)
    event.headers['x-mode'] = this.mode
    delete event.headers.authorization
    const exc = await getAuthorizedSub(event).should.be.rejectedWith(Error)
    console.log(inspect(exc, { depth: 9999 }))
    exc.isBoom.should.be.true()
    exc.output.statusCode.should.equal(401)
    exc.output.payload.attributes.error_description.should.be.equal(
      getAuthorizedSub.contract.messages.unauthorized.noToken
    )
    const wwwAuthenticate = exc.output.headers['x-www-authenticate']
    wwwAuthenticate.should.be.a.String()
    wwwAuthenticate.should.containEql(config.authorization.audience)
    wwwAuthenticate.should.containEql(getAuthorizedSub.contract.messages.unauthorized.noToken)
    wwwAuthenticate.should.match(/^Bearer realm=".*", charset="utf-8", error_description=".*", error="invalid_token"$/)
  })
  it('returns the sub if there is one, with the production issuer', async function () {
    const sub = 'thisIsASub'
    const event = Hoek.clone(eventExample)
    event.headers['x-mode'] = 'demo'
    const ra = `${event.httpMethod}:${event.path}`
    const token = await getDummyToken({
      iss: config.sts.production.issuer,
      sub,
      [customClaims.modeClaim]: 'demo',
      [customClaims.raasClaim]: ['/something/else/entirely', `${ra}**`]
    })
    event.headers.authorization = `Bearer ${token}`
    const result = await getAuthorizedSub(event)
    result.should.equal(sub)
    console.log(result)
  })
})
