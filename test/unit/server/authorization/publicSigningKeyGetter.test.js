/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const publicSigningKeyGetter = require('../../../../lib/server/authorization/publicSigningKeyGetter')
const should = require('should')
// NOTE: over time, this kid will no longer exist, as keys are rotated at the STS
const trueKid = 'Mzc0NTA3MjBDQjRCODFEN0FGQ0U2OEY2RkM3RTJCRjI1OTU5MjJENQ'

describe(testName, function () {
  describe('#getPublicSigningKeySTS', function () {
    it('fails without a header', function () {
      const err = publicSigningKeyGetter.sts.bind(undefined, undefined).should.throw()
      console.log(err)
    })
    it('fails with a header without a `kid`', function (done) {
      publicSigningKeyGetter.sts({}, (err, data) => {
        try {
          should(data).be.not.ok()
          err.should.be.an.Error()
          console.log(err)
          done()
        } catch (e) {
          done(e) // when shoulds fail
        }
      })
    })
    it('fails with a header with a wrong `kid`', function (done) {
      publicSigningKeyGetter.sts({ kid: 'wrong' }, (err, data) => {
        try {
          should(data).be.not.ok()
          err.should.be.an.Error()
          console.log(err)
          done()
        } catch (e) {
          done(e) // when shoulds fail
        }
      })
    })
    it('works with a true `kid`', function (done) {
      publicSigningKeyGetter.sts({ kid: trueKid }, (err, data) => {
        try {
          should(err).not.be.ok()
          data.should.be.a.String()
          console.log(data)
          done()
        } catch (err) {
          done(err) // when shoulds fail
        }
      })
    })
  })

  describe('publicSigningKeyGetter', function () {
    beforeEach(function () {
      publicSigningKeyGetter.contract.verifyPostconditions = true
    })

    afterEach(function () {
      publicSigningKeyGetter.contract.verifyPostconditions = false
    })

    it('returns getPublicSigningKeySTS', function () {
      const result = publicSigningKeyGetter()
      result.should.equal(publicSigningKeyGetter.sts)
    })
  })
})
