/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const resourceActionAllowed = require('../../../../lib/server/authorization/resourceActionAllowed')
const eventExample = require('../../../../lib/ppwcode/lambda/apigateway/Event').example
const Hoek = require('@hapi/hoek')
const x = require('cartesian')

const matchingRaas = [
  'GET:/I/account/account_identifier',
  'GET:/I/account/*',
  'GET:/I/account/**',
  'GET:/I/account/account_identifier*',
  'GET:/I/account/account_identifier**',
  'GET:/I/*/account_identifier',
  'GET:/I/*/*',
  'GET:/I/**/account_identifier',
  'GET:/I/**',
  '{GET,PUT}:/I/account/account_identifier',
  '{GET,PUT}:/I/*/account_identifier',
  '{GET,PUT}:/I/{account,other}/account_identifier',
  '{GET,PUT}:/I/account/account_identifier{,/**}',
  'GET:/**/account_identifier',
  '*:/I/account/account_identifier'
]
const nonMatchingRaas = [
  'what else?',
  'GET:/I/account/other_account_identifier',
  'GET:/I/account/other_account_identifier/',
  'GET:/I/account/other_account_identifier/*',
  'GET:/I/account/other_account_identifier/**',
  'GET:/I/else/other_account_identifier',
  'GET:/I/{,else}/other_account_identifier',
  'PUT:/I/account/account_identifier',
  'PUT:/I/account/and/account_identifier',
  'PUT:/I/account/*/account_identifier',
  'PUT:/I/account/**/account_identifier',
  // surprises
  'GET:**/account_identifier'
]

const matchingRaaSet = matchingRaas
  .map(raa => [raa])
  .concat(x([matchingRaas, matchingRaas]))
  .concat(x([nonMatchingRaas, matchingRaas]))
const nonMatchingRaaSet = x([nonMatchingRaas, nonMatchingRaas]).concat(nonMatchingRaas.map(raa => [raa]))

describe(testName, function () {
  beforeEach(function () {
    resourceActionAllowed.contract.verifyPostconditions = true

    this.event = Hoek.clone(eventExample)
    this.ra = `${this.event.httpMethod}:${this.event.path}`
  })
  afterEach(function () {
    resourceActionAllowed.contract.verifyPostconditions = false
  })

  matchingRaaSet.forEach(raas => {
    it(`says yes for raas ${raas.join(', ')}`, function () {
      const result = resourceActionAllowed(raas, this.event)
      console.log(`${this.ra} in [${raas.join(', ')}] ? ${result}`)
      result.should.be.true()
    })
  })
  it('says no for no raas ', function () {
    const result = resourceActionAllowed([], this.event)
    console.log(`${this.ra} in [] ? ${result}`)
    result.should.be.false()
  })
  nonMatchingRaaSet.forEach(raas => {
    it(`says no for raas ${raas.join(', ')}`, function () {
      const result = resourceActionAllowed(raas, this.event)
      console.log(`${this.ra} in [${raas.join(', ')}] ? ${result}`)
      result.should.be.false()
    })
  })
})
