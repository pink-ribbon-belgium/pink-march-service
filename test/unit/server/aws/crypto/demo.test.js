/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* NOTE: Copied from https://github.com/aws/aws-encryption-sdk-javascript.git
         - modules/example-node/src/kms_simple.ts
         - */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const { KmsKeyringNode, encrypt, decrypt, getClient } = require('@aws-crypto/client-node')
const authenticatedTestAWS = require('../../../../_authenticatedTestAWS')
const cmkARN = require('config').kms.cmk

describe(testName, function () {
  it('encrypts and decrypts', async function () {
    const AWS = await authenticatedTestAWS()
    const clientProvider = getClient(AWS.KMS)

    /* The KMS keyring must be configured with the desired CMKs */
    const keyring = new KmsKeyringNode({ clientProvider, generatorKeyId: cmkARN })

    /* Encryption context is a *very* powerful tool for controlling and managing access.
     * It is ***not*** secret!
     * Encrypted data is opaque.
     * You can use an encryption context to assert things about the encrypted data.
     * Just because you can decrypt something does not mean it is what you expect.
     * For example, if you are are only expecting data from 'us-west-2',
     * the origin can identify a malicious actor.
     * See: https://docs.aws.amazon.com/encryption-sdk/latest/developer-guide/concepts.html#encryption-context
     */
    const context = {
      stage: 'test',
      purpose: 'simple demonstration app',
      anId: '46a9771d-057e-47cd-8fb4-adccafdcf370'
    }

    /* Find data to encrypt.  A simple string. */
    const cleartext = 'asdf'
    console.log('cleartext:', cleartext)

    /* Encrypt the data. */
    const encryptionResult = await encrypt(keyring, cleartext, { encryptionContext: context })
    const resultBase64 = encryptionResult.result.toString('base64')
    console.log('resultBase64:', resultBase64)

    /* Decrypt the data. */
    const buffer = Buffer.from(resultBase64, 'base64')
    const { plaintext, messageHeader } = await decrypt(keyring, buffer)

    /* Grab the encryption context so you can verify it. */
    const { encryptionContext } = messageHeader

    /* Verify the encryption context.
     * If you use an algorithm suite with signing,
     * the Encryption SDK adds a name-value pair to the encryption context that contains the public key.
     * Because the encryption context might contain additional key-value pairs,
     * do not add a test that requires that all key-value pairs match.
     * Instead, verify that the key-value pairs you expect match.
     */
    Object.entries(context).forEach(([key, value]) => {
      if (encryptionContext[key] !== value) throw new Error('Encryption Context does not match expected values')
    })

    console.log('messageHeader:', messageHeader)
    console.log('plaintext:', plaintext.toString('utf-8'))
  })
})
