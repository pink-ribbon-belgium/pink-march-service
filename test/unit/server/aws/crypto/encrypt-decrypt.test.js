/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const encrypt = require('../../../../../lib/server/aws/crypto/encrypt')
const decrypt = require('../../../../../lib/server/aws/crypto/decrypt')

const theSecretWordForTonight = 'mud shark'
const encryptionContext = {
  someId: '907a84e9-4cd2-4917-aae0-4f450d4ac112',
  someOtherInfo: 'this is other information'
}

describe(testName, function () {
  beforeEach(function () {
    encrypt.contract.verifyPostconditions = true
    decrypt.contract.verifyPostconditions = true
  })

  afterEach(function () {
    encrypt.contract.verifyPostconditions = false
    decrypt.contract.verifyPostconditions = false
  })

  it('encrypts and decrypts a clear text', async function () {
    console.log('cleartext:', theSecretWordForTonight)
    const cipher = await encrypt(theSecretWordForTonight, encryptionContext)
    console.log('cipher base64:', cipher)
    cipher.should.be.a.String()
    const plaintext = await decrypt(cipher, encryptionContext)
    console.log('plaintext:', plaintext)
    plaintext.should.equal(theSecretWordForTonight)
  })
  it('throws an error with a different context', async function () {
    console.log('cleartext:', theSecretWordForTonight)
    const cipher = await encrypt(theSecretWordForTonight, encryptionContext)
    console.log('cipher base64:', cipher)
    cipher.should.be.a.String()
    const decryptContext = { ...encryptionContext, extraProperty: 'extra' }
    const exc = await decrypt(cipher, decryptContext).should.be.rejected()
    console.log('exc:', exc)
    exc.context.should.deepEqual(decryptContext)
    exc.cipherContext.should.deepEqual(encryptionContext)
  })
})
