/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const EncryptionContext = require('../../../../../lib/server/aws/crypto/EncryptionContext')
const expectSeriousSchema = require('../../../../_expectSeriousSchema')

const invalidStuff = [
  null,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {},
  { aString: 'this is a string', aNumberIsNotAllowed: 5 },
  { aString: 'this is a string', aBooleanIsNotAllowed: false },
  { aString: 'this is a string', anArrayIsNotAllowed: ['even of strings'] },
  { aString: 'this is a string', anObjectIsNotAllowed: { even: 'of string values' } }
]
describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(EncryptionContext.schema, invalidStuff, false)
  })
})
