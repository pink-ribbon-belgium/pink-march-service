/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const should = require('should')
const getActual = require('../../../../../lib/server/aws/dynamodb/getActual')
const DynamodbFixture = require('../../../../_DynamodbFixture')

const boom404 = {
  message: 'Not Found',
  isBoom: true,
  output: {
    statusCode: 404,
    payload: { statusCode: 404, error: 'Not Found', message: 'Not Found' },
    headers: {}
  }
}

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    getActual.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getActual.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  const cases = [{ consistent: undefined }, { consistent: false }, { consistent: true }]

  function createItem (mode, partialKey, flowId) {
    const itemData = {
      createdAt: '2020-02-02T10:00:00.007Z',
      createdBy: 'a-$+user()id',
      aProperty: 'some data',
      structureVersion: 45671
    }

    return {
      mode: mode,
      key: `/${mode}${partialKey}`,
      submitted: 'actual',
      submittedBy: itemData.createdBy,
      flowId,
      data: itemData
    }
  }

  cases.forEach(c => {
    beforeEach(function () {
      this.subjectCall =
        // prettier-ignore
        c.consistent === undefined
          ? function (mode, key) {
            return getActual(mode, key)
          }
          : function (mode, key) {
            return getActual(mode, key, c.consistent)
          }
    })

    it('should work', async function () {
      const mode = `automated-test-${uuidv4()}`
      const key = '/a-niceKey'
      const flowId = '2a990505-ed41-480f-bebb-0dad4b473bf3'

      const item = createItem(mode, key, flowId)
      await this.dynamodbFixture.putItem(item)

      const result = await this.subjectCall(mode, `/${mode}${key}`)
      result.should.deepEqual(item)
    })

    it('reports not found when key does not exist', async function () {
      const mode = `automated-test-${uuidv4()}`

      const exc = await getActual(mode, `/${mode}/shouldNotExist`, c.consistent).should.be.rejectedWith(boom404)
      console.log(exc)
    })

    it('breaks when retrieved data is badly formed', async function () {
      const mode = `automated-test-${uuidv4()}`
      const key = '/a-niceKey-fot-subTest'

      const itemData = {
        createdAt: '2020-02-02T10:00:00.007Z',
        createdBy: 'NOT URL ENCODED',
        aProperty: 'some data'
        // structureVersion: MISSING
      }

      await this.dynamodbFixture.putItem(
        {
          mode: mode,
          key: `/${mode}${key}`,
          submitted: 'actual',
          submittedBy: 'other account id',
          flowId: 'NOT A UUID',
          data: itemData
        },
        true
      )

      const exc = await getActual(mode, `/${mode}${key}`).should.be.rejectedWith(Error)
      console.log(exc)
      should(exc.isBoom).not.be.ok()
    })
  })
})
