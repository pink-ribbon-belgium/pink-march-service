/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const dynamodb = require('../../../../../lib/server/aws/dynamodb/dynamodb')
const AWS = require('aws-sdk')

describe(testName, function () {
  it('generates an AWS dynamodb DocumentClient', async function () {
    const result = await dynamodb()
    console.log(result)
    result.should.be.an.instanceof(AWS.DynamoDB.DocumentClient)
  })
})
