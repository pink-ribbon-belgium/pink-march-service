/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const dynamodbTableName = require('../../../../../lib/server/aws/dynamodb/dynamodbTableName')
const config = require('config')

const nonProductionModes = [
  'automated-test-78181678-8fa9-4476-a092-f348e531e727',
  'qa-12345',
  'acceptance-98765',
  'dev-experiment',
  'and now for something completely different'
]

describe(testName, function () {
  /* NOTE: cannot test this, because with NODE_ENV=mocha, the production table name is NOT loaded in
     config
  const productionModes = ['production', 'demo']
  productionModes.forEach(mode => {
    it(`returns the test dynamodb table with mode ${mode}`, async function () {
      const result = dynamodbTableName()
      result.should.equal(config.dynamodb.production.tableName)
    })
  })
  */
  nonProductionModes.forEach(mode => {
    it(`returns the test dynamodb table with mode ${mode}`, async function () {
      const result = dynamodbTableName()
      result.should.equal(config.dynamodb.test.tableName)
    })
  })
})
