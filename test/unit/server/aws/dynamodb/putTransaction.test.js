/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const putTransaction = require('../../../../../lib/server/aws/dynamodb/putTransaction')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const _ = require('lodash')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.testUUID = uuidv4()
    this.flowId = '5932df48-3cd2-4311-b1b7-3e947b31a9de'
    this.mode = `automated-test-${this.testUUID}`
    this.key = '/a/nice$key'

    this.itemData = {
      createdAt: '2020-01-25T15:00:00.212Z',
      createdBy: 'a-$+user()id',
      structureVersion: 746125,
      aProperty: 'some data'
    }

    this.item = {
      mode: this.mode,
      key: `/${this.mode}${this.key}`,
      submitted: this.itemData.createdAt,
      submittedBy: this.itemData.createdBy,
      flowId: this.flowId,
      data: this.itemData
    }

    this.dynamodbFixture.remember(this.item.key, this.item.submitted, config.dynamodb.test.tableName)
    this.dynamodbFixture.remember(this.item.key, 'actual', config.dynamodb.test.tableName)

    putTransaction.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    putTransaction.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    await putTransaction([
      this.item,
      {
        ...this.item,
        submitted: 'actual'
      }
    ])

    const retrieved = await this.dynamodbFixture.hasArrived()
    console.log(retrieved[1])
    console.log(retrieved[0])
    retrieved.length.should.equal(2)
    _.isEqual(retrieved[1], retrieved[0]).should.be.false()
  })
})
