/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const deleteActual = require('../../../../../lib/server/aws/dynamodb/deleteActual')
const getActual = require('../../../../../lib/server/aws/dynamodb/getActual')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const boom = require('@hapi/boom')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    deleteActual.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    deleteActual.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  function createItem (mode, partialKey, flowId) {
    const itemData = {
      createdAt: '2020-02-02T10:00:00.007Z',
      createdBy: 'a-$+user()id',
      aProperty: 'some data',
      structureVersion: 45671
    }

    return {
      mode: mode,
      key: `/${mode}${partialKey}`,
      submitted: 'actual',
      submittedBy: itemData.createdBy,
      flowId,
      data: itemData
    }
  }

  it('should work', async function () {
    const mode = `automated-test-${uuidv4()}`
    const key = '/a-niceKey'
    const flowId = '2a990505-ed41-480f-bebb-0dad4b473bf3'
    const fullKey = `/${mode}${key}`

    const item = createItem(mode, key, flowId)
    await this.dynamodbFixture.putItem(item)

    const result = await deleteActual(mode, fullKey)
    result.should.deepEqual(item)

    console.log(item)

    await getActual(mode, fullKey).should.be.rejectedWith(boom.notFound())
  })

  it('reports not found when key does not exist', async function () {
    const mode = `automated-test-${uuidv4()}`
    const key = `/${mode}/doesnotExist`

    const exc = await deleteActual(mode, key).should.be.rejectedWith(boom.notFound())
    console.log(exc)
  })
})
