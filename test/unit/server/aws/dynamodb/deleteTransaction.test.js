/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const { v4: uuidv4 } = require('uuid')
const deleteTransaction = require('../../../../../lib/server/aws/dynamodb/deleteTransaction')
const getActual = require('../../../../../lib/server/aws/dynamodb/getActual')
const DynamodbFixture = require('../../../../_DynamodbFixture')
const boom = require('@hapi/boom')

async function createItems (dynamodbFixture, mode, flowId, itemData) {
  await Promise.all(
    itemData.map(data => {
      const item = {
        mode,
        key: data.key,
        submitted: 'actual',
        submittedBy: data.createdBy,
        flowId,
        data: {
          createdAt: data.createdAt,
          createdBy: data.createdBy,
          aProperty: data.aProperty,
          structureVersion: data.structureVersion
        }
      }
      return dynamodbFixture.putItem(item)
    })
  )
}

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    deleteTransaction.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    deleteTransaction.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    const flowId = uuidv4()
    const mode = `automated-test-${uuidv4()}`
    const key1 = `/${mode}/a/nice$key`
    const key2 = `/${mode}/amazingkey`
    const key3 = `/${mode}/notsoamazingkey`

    const baseItemData = {
      createdAt: '2021-02-02T10:00:00.007Z',
      createdBy: 'a-$+user()id',
      aProperty: 'some data',
      structureVersion: 1
    }

    const itemData = [
      { ...baseItemData, key: key1 },
      { ...baseItemData, key: key2 },
      { ...baseItemData, key: key3 }
    ]
    await createItems(this.dynamodbFixture, mode, flowId, itemData)

    const arrivedItems = await this.dynamodbFixture.hasArrived()
    arrivedItems.length.should.be.equal(3)

    await deleteTransaction(mode, [key1, key2])

    await getActual(mode, key1).should.be.rejectedWith(boom.notFound())
    await getActual(mode, key2).should.be.rejectedWith(boom.notFound())
  })
})
