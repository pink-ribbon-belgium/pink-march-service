/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const getByIndex = require('../../../../../lib/server/aws/dynamodb/getByIndex')

const AccountAggregate = require('../../../../../lib/server/business/aggregates/account/AccountAggregate')
const should = require('should')

const boom404 = {
  message: 'Not Found',
  isBoom: true,
  output: {
    statusCode: 404,
    payload: { statusCode: 404, error: 'Not Found', message: 'Not Found' },
    headers: {}
  }
}

const indexA = {
  indexName: 'Index_A',
  partitionKey: 'partition_key_A',
  sortKey: 'sort_key_A'
}

const indexB = {
  indexName: 'Index_B',
  partitionKey: 'partition_key_B',
  sortKey: 'sort_key_B'
}

const index1 = {
  indexName: 'Index_1',
  partitionKey: 'partition_key_1',
  sortKey: 'sort_key_1'
}

const ids = {
  accountId: 'pretty-accountId',
  groupId: '45227494-e0c0-477c-98a7-4d1f4b7aa90a'
}

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    getByIndex.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getByIndex.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  function createItem (sot, sub, mode, flowId, ids, partialKey, submittedValue) {
    const itemData = {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 6548915,
      unknownProperty: {
        extraValue: 'extra value'
      },
      groupId: ids.groupId,
      accountId: ids.accountId,
      deleted: false
    }

    return {
      mode: mode,
      key: `/${mode}${partialKey}`,
      submitted: submittedValue,
      submittedBy: itemData.createdBy,
      flowId,
      data: { ...itemData },
      partition_key_A: `/${mode}/account/${ids.accountId}/administratorOf`,
      sort_key_A: submittedValue,
      partition_key_B: `/${mode}/group/${ids.groupId}/administrators`,
      sort_key_B: submittedValue
    }
  }

  it('should work (return all items)', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-getByIndex-test'
    const flowId = '654a8faf-1b68-4170-b725-0e62e08aa179'

    const key1 = `/groupAdministration/account/${ids.accountId}/group/${ids.groupId}`
    const item1 = createItem(sot, sub, mode, flowId, ids, key1, sot)
    const key2 = `/groupAdministration/account/${ids.accountId}/group/${ids.groupId}`
    const item2 = createItem(sot, sub, mode, flowId, ids, key2, 'actual')

    await Promise.all([this.dynamodbFixture.putItem(item1), this.dynamodbFixture.putItem(item2)])

    const result = await getByIndex(mode, item1.partition_key_A, indexA)
    console.log('testValues', result)

    result.length.should.equal(2)
    result.forEach(itemResult => itemResult.partition_key_A.should.equal(item1.partition_key_A))
  })

  it('should work (return all items - more than 1000)', async function () {
    this.timeout(15000)

    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-getByIndex-test-2'
    const flowId = '654a8faf-1b68-4170-b725-0e62e08aa179'

    this.baseData = []
    const totalnumberOfItems = 1100
    let i
    for (i = 0; i < totalnumberOfItems; i++) {
      this.baseData.push({ accountId: uuidv4(), steps: Math.round(Math.random() * 1000000), rank: 999999 })
    }

    const actualItems = this.baseData.map(d => {
      const accountAggregate = new AccountAggregate({
        mode: mode,
        dto: {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          accountId: d.accountId,
          totalSteps: d.steps,
          totalDistance: Math.round(d.steps * 0.82),
          groupId: ids.groupId,
          groupType: 'Company',
          subGroupId: undefined,
          name: 'some-kind-of-a-name',
          rank: d.rank,
          totalParticipants: 1,
          previousLevel: 1,
          newLevel: false,
          acknowledged: true
        }
      })

      return {
        ...accountAggregate.toItem(),
        submitted: 'actual',
        sort_key_1: accountAggregate.totalSteps,
        sort_key_2: accountAggregate.totalSteps,
        sort_key_3: accountAggregate.totalSteps,
        flowId: flowId
      }
    })

    await Promise.all(actualItems.map(a => this.dynamodbFixture.putItem(a)))

    const partitionKey = `/${mode}/accountAggregate`
    const result = await getByIndex(mode, partitionKey, index1)
    console.log('testValues', result)

    result.length.should.equal(totalnumberOfItems)
    result.forEach(itemResult => itemResult.partition_key_1.should.equal(partitionKey))
  })

  it('should work (only items that match the sort key)', async function () {
    const mode = `automated-test-${uuidv4()}`
    const sot = new Date().toISOString()
    const sub = 'id-getByIndex-test'
    const flowId = 'e79ca7e7-9883-4f36-a55a-f51c47481364'

    const key = `/groupAdministration/account/${ids.accountId}/group/${ids.groupId}`
    const sortKeyValue = 'actual'
    const item = createItem(sot, sub, mode, flowId, ids, key, sortKeyValue)

    await this.dynamodbFixture.putItem(item)

    const result = await getByIndex(mode, item.partition_key_A, indexA, sortKeyValue)
    console.log('testValues', result)

    result.length.should.equal(1)
    result.forEach(itemResult => itemResult.partition_key_A.should.equal(item.partition_key_A))
  })

  it('reports not found when key does not exist', async function () {
    const mode = `automated-test-${uuidv4()}`
    const exc = await getByIndex(
      mode,
      `/${mode}/account/shouldNotExist/administratorOf`,
      indexB
    ).should.be.rejectedWith(boom404)
    console.log(exc)
  })

  it('breaks when retrieved data is badly formed', async function () {
    const mode = `automated-test-${uuidv4()}`

    const itemData = {
      createdAt: '2020-01-01T11:01:12.007Z',
      createdBy: 'NOT URL ENCODED',
      aProperty: 'some data'
      // structureVersion: MISSING
    }

    const item = {
      mode: mode,
      key: `/${mode}/groupadministration/account/${ids.accountId}/group/${ids.groupId}`,
      submitted: itemData.createdAt,
      submittedBy: ids.accountId,
      data: itemData,
      flowId: 'NOT A UUID',
      partition_key_A: `/${mode}/account/${ids.accountId}/administratorOf`,
      sort_key_A: `/${mode}/group/${ids.groupId}`,
      partition_key_B: `/${mode}/group/${ids.groupId}/administrators`,
      sort_key_B: `/${mode}/account/${ids.accountId}`
    }

    await this.dynamodbFixture.putItem(item, true)

    const exc = await getByIndex(mode, item.partition_key_A, indexA).should.be.rejectedWith(Error)
    console.log(exc.toJSON)
    should(exc.isBoom).not.be.ok()
  })
})
