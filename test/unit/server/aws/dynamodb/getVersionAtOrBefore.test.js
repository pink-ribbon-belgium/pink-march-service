/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const DynamodbFixture = require('../../../../_DynamodbFixture')
const { v4: uuidv4 } = require('uuid')
const should = require('should')
const getVersionAtOrBefore = require('../../../../../lib/server/aws/dynamodb/getVersionAtOrBefore')

const boom404 = {
  message: 'Not Found',
  isBoom: true,
  output: {
    statusCode: 404,
    payload: { statusCode: 404, error: 'Not Found', message: 'Not Found' },
    headers: {}
  }
}

const cases = [{ consistent: undefined }, { consistent: false }, { consistent: true }]

function createItem (mode, partialKey, flowId, counter, submitted) {
  const itemData = {
    createdAt: submitted,
    createdBy: 'a-$+user()id',
    aProperty: 'some data',
    counter,
    structureVersion: 42
  }

  return {
    mode: mode,
    key: `/${mode}${partialKey}`,
    submitted: itemData.createdAt,
    submittedBy: itemData.createdBy,
    flowId,
    data: itemData
  }
}

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.testUuid = uuidv4()
    this.mode = `automated-test-${this.testUuid}`
    this.key = '/a/sensible$key'
    this.tooEarly = '2016-01-28T11:23:22.707Z'
    this.earlier = '2018-01-28T11:23:22.707Z'
    this.submitted = '2020-01-28T11:23:22.707Z'
    this.soon = '2021-01-28T11:23:22.707Z'
    this.later = '2022-01-28T11:23:22.707Z'
    this.farFuture = '2222-01-28T11:23:22.707Z'

    getVersionAtOrBefore.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flotsam is deleted
    getVersionAtOrBefore.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  cases.forEach(c => {
    describe(`consistent is ${c.consistent}`, function () {
      beforeEach(function () {
        this.subjectCall =
          // prettier-ignore
          c.consistent === undefined
            ? function (submitted) {
              return getVersionAtOrBefore(this.mode, `/${this.mode}${this.key}`, submitted)
            }
            : function (submitted) {
              return getVersionAtOrBefore(this.mode, `/${this.mode}${this.key}`, submitted, c.consistent)
            }
      })

      it('works with exact sort key', async function () {
        const item = createItem(this.mode, this.key, '5bda54ae-fd2a-49cf-8ba8-157e2dbd1f89', 0, this.submitted)
        await this.dynamodbFixture.putItem(item)

        const result = await this.subjectCall(this.submitted)
        console.log(result)
        result.should.deepEqual(item)
      })

      it('reports not found when key does not exist', async function () {
        const exc = await getVersionAtOrBefore(
          this.mode,
          `/${this.mode}/shouldNotExist`,
          this.submitted,
          c.consistent
        ).should.be.rejectedWith(boom404)
        console.log(exc)
      })

      describe('several matching keys', function () {
        beforeEach(async function () {
          const flowId = 'a7495f3e-3e1e-4da3-8a6c-29c0a2e2fe6f'
          await Promise.all([
            this.dynamodbFixture.putItem(createItem(this.mode, this.key, flowId, 0, this.submitted)),
            this.dynamodbFixture.putItem(createItem(this.mode, this.key, flowId, 1, this.earlier)),
            this.dynamodbFixture.putItem(createItem(this.mode, this.key, flowId, 2, this.later))
          ])
        })

        it('retrieves an exact submitted if most recent', async function () {
          const result = await this.subjectCall(this.later)
          result.data.counter.should.equal(2)
        })
        it('retrieves an exact submitted if intermediate', async function () {
          const result = await this.subjectCall(this.submitted)
          result.data.counter.should.equal(0)
        })
        it('retrieves latest when requested with far future', async function () {
          const result = await this.subjectCall(this.farFuture)
          result.data.counter.should.equal(2)
        })
        it('retrieves intermediate when requested intermediate submitted', async function () {
          const result = await this.subjectCall(this.soon)
          result.data.counter.should.equal(0)
        })
        it('reports non found when requested with too early', async function () {
          const exc = await this.subjectCall(this.tooEarly).should.be.rejectedWith(boom404)
          console.log(exc)
        })
      })

      it('breaks when retrieved data is badly formed', async function () {
        const itemData = {
          createdAt: this.submitted,
          createdBy: 'NOT URL ENCODED',
          aProperty: 'some data'
          // structureVersion: MISSING
        }

        await this.dynamodbFixture.putItem(
          {
            mode: this.mode,
            key: `/${this.mode}${this.key}`,
            submitted: itemData.createdAt,
            submittedBy: 'other account id',
            flowId: 'NOT A UUID',
            data: itemData
          },
          true
        )

        const exc = await getVersionAtOrBefore(
          this.mode,
          `/${this.mode}${this.key}`,
          this.submitted
        ).should.be.rejectedWith(Error)
        console.log(exc.toJSON)
        should(exc.isBoom).not.be.ok()
      })
    })
  })
})
