/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const config = require('config')
const { v4: uuidv4 } = require('uuid')
const put = require('../../../../../lib/server/aws/dynamodb/put')
const DynamodbFixture = require('../../../../_DynamodbFixture')

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()

    this.testUUID = uuidv4()
    this.flowId = '5932df48-3cd2-4311-b1b7-3e947b31a9de'
    this.mode = `automated-test-${this.testUUID}`
    this.key = '/a/sensible$key'

    this.itemData = {
      createdAt: '2020-03-19T08:27:16.572Z',
      createdBy: 'a-$+user()id',
      structureVersion: 99999999999,
      aProperty: 'some data'
    }
    this.item = {
      mode: this.mode,
      key: `/${this.mode}${this.key}`,
      submitted: this.itemData.createdAt,
      submittedBy: this.itemData.createdBy,
      flowId: this.flowId,
      data: this.itemData
    }

    this.dynamodbFixture.remember(this.item.key, this.item.submitted, config.dynamodb.test.tableName)

    put.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(15000) // make sure flutsam is deleted
    put.contract.verifyPostconditions = false
    await this.dynamodbFixture.clean()
  })

  it('works', async function () {
    await put(this.item)
    const retrieved = await this.dynamodbFixture.hasArrived()
    retrieved.length.should.equal(1)
    retrieved[0].should.deepEqual(this.item)
    retrieved[0].flowId.should.equal(this.flowId)
  })
})
