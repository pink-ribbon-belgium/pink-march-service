/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const { v4: uuidv4 } = require('uuid')
const {
  contract: getLeastRecentlyUpdatedContract
} = require('../../lib/server/business/account/trackerConnection/getLeastRecentlyUpdated')
const { contract: updateActivitiesContract } = require('../../lib/server/business/account/activity/updateActivities')
const should = require('should')
const { isEqual } = require('lodash/lang')

const callFlowId = 'ee92575a-254f-4f46-a1e5-ef5da3da3c07'
const context = {
  awsRequestId: callFlowId
}
const nrOfTrackerConnectionsToPoll = 564
const itemCreatedAt = '2020-04-02T11:12:46.383Z'
const itemExps = ['2022-04-02T11:12:46.383Z', '2023-04-02T11:12:46.383Z', '2024-04-02T11:12:46.383Z']

function createTrackerConnection (index) {
  return {
    createdAt: itemCreatedAt,
    createdBy: `createdBy-pollActivities-test-${index}`,
    structureVersion: 1,
    accountId: `accountId-pollActivities-test-${index}`,
    trackerType: 'fitbit',
    tokenType: 'refresh',
    encryptedToken: 'WA==',
    exp: itemExps[index],
    hasError: false
  }
}

describe(testName, function () {
  beforeEach(async function () {
    this.sandbox = sinon.createSandbox()
    this.mode = `automated-test-${uuidv4()}`

    this.getLeastRecentlyUpdated = this.sandbox.stub()
    this.getLeastRecentlyUpdatedContractFunction = getLeastRecentlyUpdatedContract.implementation(
      this.getLeastRecentlyUpdated
    )

    this.updateActivities = this.sandbox.stub()
    this.updateActivitiesContractFunction = updateActivitiesContract.implementation(this.updateActivities)

    this.pollActivities = proxyquire('../../lib/pollActivities', {
      './server/business/account/trackerConnection/getLeastRecentlyUpdated': this
        .getLeastRecentlyUpdatedContractFunction,
      './server/business/account/activity/updateActivities': this.updateActivitiesContractFunction
    })
    this.handler = this.pollActivities.handler
    this.handler.contract.verifyPostconditions = true

    this.event = { mode: this.mode, nrOfTrackerConnectionsToPoll }
  })

  afterEach(function () {
    this.handler.contract.verifyPostconditions = false
    this.getLeastRecentlyUpdatedContractFunction.contract.verifyPostconditions = false
    this.updateActivitiesContractFunction.contract.verifyPostconditions = false
    this.sandbox.restore()
  })

  describe('expected behavior', function () {
    beforeEach(async function () {
      this.getLeastRecentlyUpdatedContractFunction.contract.verifyPostconditions = true
      this.updateActivitiesContractFunction.contract.verifyPostconditions = true
    })

    it('works with 0 trackerConnections found', async function () {
      this.getLeastRecentlyUpdated.resolves([])
      const response = await this.handler(this.event, context)
      should(response).be.undefined()
      this.getLeastRecentlyUpdated.should.be.calledOnce()
      this.getLeastRecentlyUpdated.should.be.calledWithMatch(
        callFlowId,
        sinon.match.string,
        this.mode,
        nrOfTrackerConnectionsToPoll
      )
      this.pollActivities.should.not.be.called()
    })
    it('works with 3 trackerConnections found', async function () {
      const trackerConnections = [createTrackerConnection(0), createTrackerConnection(1), createTrackerConnection(3)]
      this.getLeastRecentlyUpdated.resolves(trackerConnections)
      this.updateActivities.resolves()
      const response = await this.handler(this.event, context)
      should(response).be.undefined()
      this.getLeastRecentlyUpdated.should.be.calledOnce()
      this.getLeastRecentlyUpdated.should.be.calledWithMatch(
        callFlowId,
        sinon.match.string,
        this.mode,
        nrOfTrackerConnectionsToPoll
      )
      const sot = this.getLeastRecentlyUpdated.args[0][1]
      this.updateActivities.should.be.calledThrice()
      this.updateActivities.should.be.alwaysCalledWithMatch(callFlowId, sot, this.mode, sinon.match.object)
      const updateActivitiesCalls = this.updateActivities.getCalls()
      trackerConnections.forEach(tc => {
        updateActivitiesCalls.some(call => isEqual(call.args[3], tc)).should.be.true()
      })
    })
  })

  describe('unexpected behavior', function () {
    it('reports when getLeastRecentlyUpdated throws', async function () {
      const error = new Error()
      this.getLeastRecentlyUpdated.rejects(error)
      const response = await this.handler(this.event, context)
      should(response).be.undefined()
      this.getLeastRecentlyUpdated.should.be.calledOnce()
      this.getLeastRecentlyUpdated.should.be.calledWithMatch(
        callFlowId,
        sinon.match.string,
        this.mode,
        nrOfTrackerConnectionsToPoll
      )
      this.pollActivities.should.not.be.called()
    })
    it('reports when updateActivities throws', async function () {
      const error = new Error()
      const trackerConnections = [createTrackerConnection(0), createTrackerConnection(1), createTrackerConnection(3)]
      this.getLeastRecentlyUpdated.resolves(trackerConnections)
      this.updateActivities.onCall(0).resolves()
      this.updateActivities.onCall(0).rejects(error)
      this.updateActivities.onCall(2).resolves()
      const response = await this.handler(this.event, context)
      should(response).be.undefined()
      this.getLeastRecentlyUpdated.should.be.calledOnce()
      this.getLeastRecentlyUpdated.should.be.calledWithMatch(
        callFlowId,
        sinon.match.string,
        this.mode,
        nrOfTrackerConnectionsToPoll
      )
      const sot = this.getLeastRecentlyUpdated.args[0][1]
      this.updateActivities.should.be.calledThrice()
      this.updateActivities.should.be.alwaysCalledWithMatch(callFlowId, sot, this.mode, sinon.match.object)
      const updateActivitiesCalls = this.updateActivities.getCalls()
      trackerConnections.forEach(tc => {
        updateActivitiesCalls.some(call => isEqual(call.args[3], tc)).should.be.true()
      })
    })
  })
})
