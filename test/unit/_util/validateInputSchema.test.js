/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')
const validateInputSchema = require('../../../lib/_util/validateInputSchema')
const Joi = require('@hapi/joi')

const schema = Joi.object({ foo: Joi.string().required() }).unknown(true)

describe(testName(module), function () {
  it('passes a valid object', function () {
    const result = validateInputSchema(schema, { foo: 'bar', baz: 'bam' })
    result.should.be.true()
  })
  it('fails an invalid object', function () {
    try {
      validateInputSchema(schema, { foo: 7 })
      this.fail()
    } catch (err) {
      err.isBoom.should.be.true()
      err.output.statusCode.should.equal(400)
    }
  })
})
describe(testName(module), function () {})
