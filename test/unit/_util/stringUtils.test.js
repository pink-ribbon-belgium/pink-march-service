/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')
const stringUtils = require('../../../lib/_util/stringUtils')

describe(testName(module), function () {
  describe('leadingZeroes', function () {
    it('should work with string', function () {
      const result = stringUtils.leadingZeroes('156', 6)
      result.should.be.equal('000156')
    })
    it('should work with number', function () {
      const result = stringUtils.leadingZeroes(500, 6)
      result.should.be.equal('000500')
    })
  })
})
