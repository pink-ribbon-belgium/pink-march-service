/*
 * Copyright 2020 PeopleWare n.v.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/* eslint-env mocha */

const testName = require('../../_testName')
const simpleLog = require('../../../lib/_util/simpleLog')
const UUID = require('../../../lib/ppwcode/UUID')

const shouldBeMasked = 'should be masked'

const objWithSecrets = {
  p: 'this is an array',
  secret: shouldBeMasked,
  authorization: shouldBeMasked,
  Authorization: shouldBeMasked
}

const circular = {
  a: 'a',
  b: 'b',
  c: {
    ca: [1, 2, 3, 4, 5],
    cb: 'b',
    cc: {
      cca: 'a',
      ccb: 'b'
    }
  },
  d: 'd'
}
circular.c.ca.push(circular)
circular.c.cc.ccc = circular

const details = [
  undefined,
  [1, 2, 'this is an array', objWithSecrets],
  [1, 2, ['a', objWithSecrets]],
  objWithSecrets,
  { a: 'a', b: 'b', obj: objWithSecrets, s: Symbol('S') },
  3,
  true,
  false,
  'a string',
  Symbol('T'),
  { value: circular, description: 'circular structures' }
]

const errors = details.concat(new Error('this is an error'))

// NOTE: these are just smoke tests, in attendance for a real logging lib
describe(testName(module), function () {
  describe('info', function () {
    details.forEach(d => {
      it(`works with ${(d && d.description) || d}`, function () {
        simpleLog.info(module, 'it', UUID.example, 'PHASE', (d && d.value) || d)
      })
    })
  })
  describe('warn', function () {
    details.forEach(d => {
      it(`works with ${(d && d.description) || d}`, function () {
        simpleLog.warn(module, 'it', UUID.example, 'PHASE', (d && d.value) || d)
      })
    })
  })
  describe('error', function () {
    errors.forEach(e => {
      it(`works with ${(e && e.description) || e}`, function () {
        simpleLog.error(module, 'it', UUID.example, e)
      })
    })
  })
})
