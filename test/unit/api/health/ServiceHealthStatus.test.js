/*
 * Copyright 2020 Jan Dockx
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const ServiceHealthStatus = require('../../../../lib/api/health/ServiceHealthStatus')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const x = require('cartesian')
const should = require('should')

const stuff = [null, '', 8, false, [], {}, Symbol('s'), 'not a status']

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(ServiceHealthStatus.schema, stuff)
  })
  describe('#values', function () {
    it('is an array of values', function () {
      ServiceHealthStatus.values.should.be.an.Array()
    })
    ServiceHealthStatus.values.forEach(v => {
      it(`"${v}" is valid`, function () {
        should(ServiceHealthStatus.schema.validate(v).error).not.be.ok()
      })
    })
  })
  describe('#statusCode', function () {
    it('is an object with numbers', function () {
      function shouldBeIntegerWithin (candidate, min, max) {
        candidate.should.be.a.Number()
        Number.isInteger(candidate).should.be.a.true()
        candidate.should.be.within(min, max)
      }

      ServiceHealthStatus.statusCode.should.be.an.Object()
      shouldBeIntegerWithin(ServiceHealthStatus.statusCode.OK, 200, 299)
      shouldBeIntegerWithin(ServiceHealthStatus.statusCode.WARNING, 200, 299)
      shouldBeIntegerWithin(ServiceHealthStatus.statusCode.ERROR, 400, 499)
    })
  })
  describe('#consolidate', function () {
    beforeEach(function () {
      ServiceHealthStatus.consolidate.contract.verifyPostconditions = true
    })
    afterEach(function () {
      ServiceHealthStatus.consolidate.contract.verifyPostconditions = false
    })

    const cases = x({
      acc: ServiceHealthStatus.values.filter(v => v !== ServiceHealthStatus.UNREACHABLE),
      status: ServiceHealthStatus.values,
      required: [true, false]
    })

    cases.forEach(c => {
      /* prettier-ignore */
      const expected =
        c.status === ServiceHealthStatus.OK
          ? c.acc
          : c.status === ServiceHealthStatus.WARNING
            ? c.acc === ServiceHealthStatus.OK ? ServiceHealthStatus.WARNING : c.acc
            : (c.status === ServiceHealthStatus.ERROR || c.status === ServiceHealthStatus.UNREACHABLE)
              ? c.acc === ServiceHealthStatus.ERROR
                ? ServiceHealthStatus.ERROR
                : c.required ? ServiceHealthStatus.ERROR : ServiceHealthStatus.WARNING
              : undefined
      it(`${c.acc} <- ${c.status}, required ${c.required}, returns ${expected}`, function () {
        const result = ServiceHealthStatus.consolidate(c.acc, c.status, c.required)
        result.should.equal(expected)
      })
    })
  })
})
