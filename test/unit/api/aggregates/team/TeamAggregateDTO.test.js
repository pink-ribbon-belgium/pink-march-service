/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const TeamAggregateDTO = require('../../../../../lib/api/aggregates/team/TeamAggregateDTO')
const expectSeriousSchema = require('../../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  Symbol('a symbol'),
  [],
  {}
]
describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        TeamAggregateDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), teamAggregateId: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), totalSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), totalDistance: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), averageSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), averageDistance: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), companyId: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), groupType: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), totalTeams: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), name: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), extendedName: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== true).map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleIn), rank: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        TeamAggregateDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), teamAggregateId: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), totalSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), totalDistance: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), averageSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), averageDistance: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), companyId: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), groupType: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), totalTeams: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), name: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), extendedName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleDynamoDb), rank: s }))
          ),
        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        TeamAggregateDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), teamAggregateId: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), totalSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), totalDistance: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), averageSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), averageDistance: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), companyId: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), groupType: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), totalTeams: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), name: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), extendedName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(TeamAggregateDTO.exampleOut), rank: s }))
          ),
        false
      )
    })
  })
})
