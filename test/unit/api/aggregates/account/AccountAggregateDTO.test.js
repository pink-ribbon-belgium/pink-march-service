/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../../../_testName')(module)
const AccountaggregateDTO = require('../../../../../lib/api/aggregates/account/AccountAggregateDTO')
const expectSeriousSchema = require('../../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  Symbol('a symbol'),
  [],
  {}
]
describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        AccountaggregateDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), totalDistance: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), groupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), subGroupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), acknowledged: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), previousLevel: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleIn), rank: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        AccountaggregateDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), totalDistance: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), groupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), subGroupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), acknowledged: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), previousLevel: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleDynamoDb), rank: s }))
          )
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        AccountaggregateDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), totalDistance: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), level: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), previousLevel: s })))
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), newLevel: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), stepsToNextLevel: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), groupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), subGroupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), acknowledged: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(AccountaggregateDTO.exampleOut), rank: s }))
          )
      )
    })
  })
})
