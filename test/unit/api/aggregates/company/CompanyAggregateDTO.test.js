/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../../../_testName')(module)
const CompanyAggregateDTO = require('../../../../../lib/api/aggregates/company/CompanyAggregateDTO')
const expectSeriousSchema = require('../../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  Symbol('a symbol'),
  [],
  {}
]
describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        CompanyAggregateDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), companyId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), totalDistance: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), averageDistance: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), averageSteps: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), extendedName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined && typeof is !== 'boolean')
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), unit: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), rank: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        CompanyAggregateDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), companyId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), totalDistance: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), averageDistance: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), averageSteps: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), extendedName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined && typeof is !== 'boolean')
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), unit: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), rank: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleDynamoDb), structureVersion: s }))
          ),
        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        CompanyAggregateDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), companyId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), totalDistance: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), averageSteps: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), averageDistance: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), extendedName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined && typeof is !== 'boolean')
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), hasLogo: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), unit: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), rank: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleOut), structureVersion: s }))
          ),
        false
      )
    })

    /*
    describe('output', function () {
      expectSeriousSchema(
        CompanyAggregateDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), totalSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), totalDistance: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), level: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), stepsToNextLevel: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), groupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), subGroupId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), name: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyAggregateDTO.exampleIn), rank: s }))
          )
      )
    })
*/
  })
})
