/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const AccountPreferencesDTO = require('../../../../lib/api/account/AccountPreferencesDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

const muchStuff = invalidStuff
  .filter(is => is !== undefined)
  .concat(
    invalidStuff
      .filter(is => !Number.isInteger(is) || is !== 2)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), structureVersion: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), createdAt: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), createdBy: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), accountId: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), language: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), verifiedEmail: s }))
  )
  .concat(
    invalidStuff.filter(is => is !== true).map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), newsletter: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), knownFromType: s }))
  )
  .concat(
    invalidStuff
      .filter(is => is !== undefined)
      .filter(is => is !== 'a string')
      .map(s => ({ ...Hoek.clone(AccountPreferencesDTO.example), otherText: s }))
  )

const muchStuffNoCreated = muchStuff.concat([
  { ...Hoek.clone(AccountPreferencesDTO.example), createdAt: undefined },
  { ...Hoek.clone(AccountPreferencesDTO.example), createdBy: undefined }
])

const inputFailures = muchStuff.concat([{ ...Hoek.clone(AccountPreferencesDTO.example), verifiedEmail: undefined }])
const dynamoDbFailures = muchStuffNoCreated.concat([
  { ...Hoek.clone(AccountPreferencesDTO.example), verifiedEmail: undefined }
])
const outputFailures = muchStuffNoCreated

describe(testName, function () {
  describe('#schema', function () {
    describe('base', function () {
      expectSeriousSchema(AccountPreferencesDTO.schema, muchStuff, false)
    })
    describe('input', function () {
      expectSeriousSchema(AccountPreferencesDTO.schema.tailor('input'), inputFailures, true)
    })
    describe('dynamodb', function () {
      expectSeriousSchema(AccountPreferencesDTO.schema.tailor('dynamodb'), dynamoDbFailures, false)
    })
    describe('output', function () {
      expectSeriousSchema(AccountPreferencesDTO.schema.tailor('output'), outputFailures, false)
    })
  })
})
