/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const GroupDTO = require('../../../../lib/api/group/GroupDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        GroupDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), createdAt: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), structureVersion: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), id: s })))
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), code: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), groupType: s })))
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), linkId: s })))
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), shortUrl: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        GroupDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), structureVersion: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), id: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), code: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), groupType: s })))
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), linkId: s })))
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), shortUrl: s }))
          ),

        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        GroupDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), createdAt: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), createdBy: s })))
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleIn), structureVersion: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), id: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), code: s })))
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(GroupDTO.exampleDynamodb), groupType: s })))
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), linkId: s })))
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(GroupDTO.exampleOut), shortUrl: s }))
          ),

        false
      )
    })
  })
})
