/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../../_testName')(module)
const ActivityDTO = require('../../../../lib/api/activity/ActivityDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        ActivityDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), activityDate: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), trackerType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), numberOfSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), distance: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== true).map(s => ({ ...Hoek.clone(ActivityDTO.exampleIn), deleted: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        ActivityDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), activityDate: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), trackerType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), numberOfSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), distance: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== true)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleDynamoDb), deleted: s }))
          ),
        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        ActivityDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), activityDate: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), accountId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), trackerType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), numberOfSteps: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is))
              .map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), distance: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== true).map(s => ({ ...Hoek.clone(ActivityDTO.exampleOut), deleted: s }))
          ),
        false
      )
    })
  })
})
