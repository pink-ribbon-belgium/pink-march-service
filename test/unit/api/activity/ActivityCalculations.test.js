/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const ActivityCalculations = require('../../../../lib/api/activity/ActivityCalculations')

describe(testName, function () {
  it('Calculate distance from step for men', async function () {
    const expectedDistance = 820
    const result = ActivityCalculations.calculateDistanceFromSteps('M', 1000, 0)
    result.should.equal(expectedDistance)
  })
  it('Calculate steps from distance for men', async function () {
    const expectedSteps = 100
    const result = ActivityCalculations.calculateDistanceFromSteps('M', 0, 82)
    result.should.equal(expectedSteps)
  })
  it('Calculate distance from step for women', async function () {
    const expectedDistance = 650
    const result = ActivityCalculations.calculateDistanceFromSteps('F', 1000, 0)
    result.should.equal(expectedDistance)
  })
  it('Calculate steps from distance for women', async function () {
    const expectedSteps = 100
    const result = ActivityCalculations.calculateDistanceFromSteps('F', 0, 65)
    result.should.equal(expectedSteps)
  })
  it('Calculate distance from step for X', async function () {
    const expectedDistance = 735
    const result = ActivityCalculations.calculateDistanceFromSteps('X', 1000, 0)
    result.should.equal(expectedDistance)
  })
  it('Calculate steps from distance for X', async function () {
    const expectedSteps = 136
    const result = ActivityCalculations.calculateDistanceFromSteps('X', 0, 100)
    result.should.equal(expectedSteps)
  })
  it('Calculate total number of steps', async function () {
    const expectedSteps = 3500
    const result = ActivityCalculations.calculateSteps(2000, 3500)
    result.should.equal(expectedSteps)
  })
  it('Calculate steps to next level (level: 1 - steps: 0)', async function () {
    const expectedSteps = 30001
    const result = ActivityCalculations.calculateStepsToNextLevel(0, 1)
    result.should.equal(expectedSteps)
  })
  it('Calculate steps to next level (level: 2 - steps: 19000)', async function () {
    const expectedSteps = 1001
    const result = ActivityCalculations.calculateStepsToNextLevel(79000, 2)
    result.should.equal(expectedSteps)
  })

  it('Calculate steps to next level (level: 11 - steps: 310999)', async function () {
    const expectedSteps = 0
    const result = ActivityCalculations.calculateStepsToNextLevel(310999, 11)
    result.should.equal(expectedSteps)
  })

  it('Calculate level from steps should return 1', async function () {
    const expectedLevel = 1
    const result = ActivityCalculations.calculateLevelFromSteps(30000, 1)
    result.should.equal(expectedLevel)
  })

  it('Calculate level from steps should return 2', async function () {
    const expectedLevel = 2
    const result = ActivityCalculations.calculateLevelFromSteps(80000, 1)
    result.should.equal(expectedLevel)
  })

  it('Calculate level from steps should return 3', async function () {
    const expectedLevel = 3
    const result = ActivityCalculations.calculateLevelFromSteps(130000, 1)
    result.should.equal(expectedLevel)
  })

  it('Calculate level from steps should return 4', async function () {
    const expectedLevel = 4
    const result = ActivityCalculations.calculateLevelFromSteps(180000, 1)
    result.should.equal(expectedLevel)
  })

  it('Calculate level from steps should return 5', async function () {
    const expectedLevel = 5
    const result = ActivityCalculations.calculateLevelFromSteps(230000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 6', async function () {
    const expectedLevel = 6
    const result = ActivityCalculations.calculateLevelFromSteps(280000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 7', async function () {
    const expectedLevel = 7
    const result = ActivityCalculations.calculateLevelFromSteps(330000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 8', async function () {
    const expectedLevel = 8
    const result = ActivityCalculations.calculateLevelFromSteps(390000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 9', async function () {
    const expectedLevel = 9
    const result = ActivityCalculations.calculateLevelFromSteps(460000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 10', async function () {
    const expectedLevel = 10
    const result = ActivityCalculations.calculateLevelFromSteps(540000, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 11', async function () {
    const expectedLevel = 11
    const result = ActivityCalculations.calculateLevelFromSteps(540001, 1)
    result.should.equal(expectedLevel)
  })
  it('Calculate level from steps should return 11+', async function () {
    const expectedLevel = 11
    const result = ActivityCalculations.calculateLevelFromSteps(54000000, 1)
    result.should.equal(expectedLevel)
  })
})
