/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const CompanyDTO = require('../../../../lib/api/company/CompanyDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        CompanyDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), structureVersion: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), id: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), code: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== 'a string').map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), address: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), zip: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== 'a string').map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), city: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), vat: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), contactFirstName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), contactLastName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), contactEmail: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), contactTelephone: s }))
          )
          .concat(
            invalidStuff.concat(['Team', 'cohort']).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), unit: s }))
          )
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), linkId: s })))
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleIn), shortUrl: s }))),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        CompanyDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), structureVersion: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), id: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), code: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), address: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), zip: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), city: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), vat: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), contactFirstName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), contactLastName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), contactEmail: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), contactTelephone: s }))
          )
          .concat(
            invalidStuff
              .concat(['Team', 'cohort'])
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), unit: s }))
          )
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), linkId: s })))
          .concat(
            invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleDynamodb), shortUrl: s }))
          ),

        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        CompanyDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), structureVersion: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), id: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), code: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), address: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), zip: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== 'a string').map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), city: s }))
          )
          .concat(
            invalidStuff.filter(is => is !== undefined).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), vat: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), contactFirstName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), contactLastName: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), contactEmail: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), contactTelephone: s }))
          )
          .concat(
            invalidStuff.concat(['Team', 'cohort']).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), groupType: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !!is)
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), unit: s }))
          )
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), linkId: s })))
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(CompanyDTO.exampleOut), shortUrl: s }))),

        false
      )
    })
  })
})
