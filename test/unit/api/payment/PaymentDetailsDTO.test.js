/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const PaymentDetailsDTO = require('../../../../lib/api/payment/PaymentDetailsDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

describe(testName, function () {
  expectSeriousSchema(
    PaymentDetailsDTO.schema,
    invalidStuff
      .filter(is => is !== undefined)
      .map(s => ({ ...Hoek.clone(PaymentDetailsDTO.example), locale: s }))
      .concat(invalidStuff.map(s => ({ ...Hoek.clone(PaymentDetailsDTO.example), paymentId: s })))
      .concat(
        invalidStuff
          .filter(is => !Number.isInteger(is) || is <= 0)
          .map(s => ({ ...Hoek.clone(PaymentDetailsDTO.example), numberOfSlots: s }))
      )
      .concat(
        invalidStuff
          .filter(is => is !== undefined)
          .filter(is => is !== null)
          .map(s => ({ ...Hoek.clone(PaymentDetailsDTO.example), voucherCode: s }))
      )
      .concat(
        invalidStuff
          .filter(is => typeof is !== 'string')
          .map(s => ({ ...Hoek.clone(PaymentDetailsDTO.example), redirectUrl: s }))
      ),
    true
  )
})
