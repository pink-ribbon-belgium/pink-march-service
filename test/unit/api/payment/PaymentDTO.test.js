/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const PaymentDTO = require('../../../../lib/api/payment/PaymentDTO')
const expectSeriousSchema = require('../../../_expectSeriousSchema')
const Hoek = require('@hapi/hoek')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

describe(testName, function () {
  describe('#schema', function () {
    describe('input', function () {
      expectSeriousSchema(
        PaymentDTO.schema.tailor('input'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), paymentId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), paymentInitiatedAt: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), groupId: s })))
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), numberOfSlots: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !(typeof is === 'number') || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), amount: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleIn), paymentStatus: s }))
          ),
        true
      )
    })
    describe('dynamodb', function () {
      expectSeriousSchema(
        PaymentDTO.schema.tailor('dynamodb'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), paymentId: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), paymentInitiatedAt: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), groupId: s })))
          .concat(
            invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), voucherCode: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), numberOfSlots: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !(typeof is === 'number') || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), amount: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleDynamodb), paymentStatus: s }))
          ),

        false
      )
    })
    describe('output', function () {
      expectSeriousSchema(
        PaymentDTO.schema.tailor('output'),
        invalidStuff
          .filter(is => is !== undefined)
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), createdAt: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), createdBy: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), structureVersion: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), paymentId: s }))
          )
          .concat(invalidStuff.filter(is => !!is).map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), voucherCode: s })))
          .concat(
            invalidStuff
              .filter(is => is !== undefined)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), paymentInitiatedAt: s }))
          )
          .concat(invalidStuff.map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), groupId: s })))
          .concat(
            invalidStuff
              .filter(is => !Number.isInteger(is) || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), numberOfSlots: s }))
          )
          .concat(
            invalidStuff
              .filter(is => !(typeof is === 'number') || is <= 0)
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), amount: s }))
          )
          .concat(
            invalidStuff
              .filter(is => is !== 'a string')
              .map(s => ({ ...Hoek.clone(PaymentDTO.exampleOut), paymentStatus: s }))
          ),

        false
      )
    })
  })
})
