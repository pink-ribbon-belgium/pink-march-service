/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const marshalledDynamodbStreamHandler = require('../../../lib/stream/marshalledDynamodbStreamHandler')
const streamEventExample = require('../../_streamEventExample')
const unmarshalledHandlerContract = require('../../../lib/stream').unmarshalledHandler.contract
const { schema: StreamEvent } = require('../../../lib/stream/StreamEvent')
const Joi = require('@hapi/joi')

const callFlowId = '5ed6c6b5-3cf1-45c3-be4c-66b5686defde'
const context = {
  awsRequestId: callFlowId
}

describe(testName, function () {
  it('works', async function () {
    let unmarshalledEvent
    let passedContext
    const unmarshalledHandler = unmarshalledHandlerContract.implementation(async function unmarshalledHandler (
      event,
      context
    ) {
      unmarshalledEvent = event
      passedContext = context
    })
    unmarshalledHandler.contract.verifyPostconditions = true

    const marshalledHandler = marshalledDynamodbStreamHandler(unmarshalledHandler)

    await marshalledHandler(streamEventExample, context)

    // we tested through the contract that the event of the unmarshalledHandler is as expected
    Joi.attempt(unmarshalledEvent, StreamEvent)
    passedContext.should.equal(context)
  })
})
