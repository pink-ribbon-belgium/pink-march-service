/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const streamRecordHandlers = require('../../../lib/stream/streamRecordHandlers')
const streamRecordHandlerContract = require('../../../lib/stream/StreamRecordHandlerContract')

describe(testName, function () {
  it('is an array of streamHandler contract functions', function () {
    streamRecordHandlers.should.be.an.Array()
    streamRecordHandlers.forEach(streamHandler => {
      streamRecordHandlerContract.isImplementedBy(streamHandler).should.be.true()
    })
  })
})
