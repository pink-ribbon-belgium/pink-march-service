/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const StreamEvent = require('../../../lib/stream/StreamEvent')
const expectSeriousSchema = require('../../_expectSeriousSchema')
const { example: exampleSubmittedItem, exampleActualItem } = require('../../../lib/server/aws/dynamodb/Item')
const streamEventExample = require('../../_streamEventExample')
const processor = require('dynamodb-streams-processor')

const extraExample = {
  Records: processor(streamEventExample.Records)
}

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]
const invalidStuffWithoutUndefined = invalidStuff.filter(is => is !== undefined)

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(
      StreamEvent.schema.example(extraExample),
      invalidStuff
        .concat(invalidStuff.map(is => ({ Records: is })))
        .concat(invalidStuff.map(is => [is]))
        .concat(invalidStuff.map(is => ({ Records: [{ eventName: 'INSERT', dynamodb: is }] })))
        .concat(
          invalidStuffWithoutUndefined.map(is => ({ Records: [{ eventName: 'INSERT', dynamodb: { NewImage: is } }] }))
        )
        .concat(
          invalidStuffWithoutUndefined.map(is => ({ Records: [{ eventName: 'REMOVE', dynamodb: { OldImage: is } }] }))
        )
        .concat(
          invalidStuff.map(is => ({
            Records: [{ eventName: 'MODIFY', dynamodb: { OldImage: is, NewImage: exampleSubmittedItem } }]
          }))
        )
        .concat(
          invalidStuff.map(is => ({
            Records: [{ eventName: 'MODIFY', dynamodb: { OldImage: exampleSubmittedItem, NewImage: is } }]
          }))
        )
        .concat([{ Records: [{ eventName: 'INSERT', dynamodb: { OldImage: exampleActualItem } }] }])
        .concat([{ Records: [{ eventName: 'MODIFY', dynamodb: { NewImage: exampleActualItem } }] }])
        .concat([{ Records: [{ eventName: 'MODIFY', dynamodb: { OldImage: exampleActualItem } }] }])
        .concat([{ Records: [{ eventName: 'REMOVE', dynamodb: { NewImage: exampleActualItem } }] }]),
      true
    )
  })
})
