/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const { schema: ValidStreamRecord, insertExample, modifyExample } = require('../../../lib/stream/ValidStreamRecord')
const expectSeriousSchema = require('../../_expectSeriousSchema')
const { example: exampleSubmittedItem, exampleActualItem } = require('../../../lib/server/aws/dynamodb/Item')

const invalidStuff = [
  null,
  undefined,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {}
]

const anotherKey = '/dev-experiment/another/key'
const anotherSubmitted = '2020-04-04T21:03:43.602Z'

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(
      ValidStreamRecord,
      invalidStuff
        .concat(invalidStuff.map(is => ({ ...insertExample, mode: is })))
        .concat(invalidStuff.map(is => ({ ...insertExample, event: is })))
        .concat(invalidStuff.map(is => ({ ...insertExample, keys: is })))
        .concat(
          invalidStuff.map(is => ({ ...insertExample, keys: { key: is, submitted: insertExample.keys.submitted } }))
        )
        .concat(invalidStuff.map(is => ({ ...insertExample, keys: { key: insertExample.keys.key, submitted: is } })))
        .concat(invalidStuff.map(is => ({ ...insertExample, newItem: is })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, newItem: is })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, oldItem: is })))
        .concat(invalidStuff.map(is => ({ ...insertExample, newItem: { ...exampleActualItem, key: is } })))
        .concat(invalidStuff.map(is => ({ ...insertExample, newItem: { ...exampleActualItem, submitted: is } })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, oldItem: { ...exampleSubmittedItem, key: is } })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, oldItem: { ...exampleSubmittedItem, submitted: is } })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, newItem: { ...exampleSubmittedItem, key: is } })))
        .concat(invalidStuff.map(is => ({ ...modifyExample, newItem: { ...exampleSubmittedItem, submitted: is } })))
        .concat([{ ...insertExample, event: 'MODIFY' }])
        .concat([{ ...modifyExample, event: 'INSERT' }])
        .concat([{ ...insertExample, newItem: { ...insertExample.newItem, key: anotherKey } }])
        .concat([
          {
            ...insertExample,
            newItem: {
              ...insertExample.newItem,
              submitted: anotherSubmitted,
              data: { ...insertExample.newItem.data, createdAt: anotherSubmitted }
            }
          }
        ])
        .concat([{ ...modifyExample, newItem: { ...modifyExample.newItem, key: anotherKey } }])
        .concat([
          {
            ...modifyExample,
            newItem: {
              ...modifyExample.newItem,
              submitted: anotherSubmitted,
              data: { ...modifyExample.newItem.data, createdAt: anotherSubmitted }
            }
          }
        ])
        .concat([{ ...modifyExample, oldItem: { ...modifyExample.oldItem, key: anotherKey } }])
        .concat([
          {
            ...modifyExample,
            oldItem: {
              ...modifyExample.newItem,
              submitted: anotherSubmitted,
              data: { ...modifyExample.oldItem.data, createdAt: anotherSubmitted }
            }
          }
        ]),
      true
    )
  })
})
