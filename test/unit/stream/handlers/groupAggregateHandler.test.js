/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../../_testName')(module)

const util = require('util')
const DynamodbFixture = require('../../../_DynamodbFixture')
const GroupType = require('../../../../lib/api/group/GroupType')
const TeamAggregate = require('../../../../lib/server/business/aggregates/team/TeamAggregate')
const CompanyAggregate = require('../../../../lib/server/business/aggregates/company/CompanyAggregate')
const { v4: uuidv4 } = require('uuid')
const should = require('should')
const getActual = require('../../../../lib/server/aws/dynamodb/getActual')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const { contract: getAccountProfile } = require('../../../../lib/server/business/account/profile/get')
const Boom = require('@hapi/boom')

const sot = '2020-04-15T11:05:52.191Z'
const flowId = '6ca3b82a-840e-4264-b973-b4128913abdd'
const teamAggregateFlowId = '8d8ee951-2dd8-476f-b2cc-3e650cb411a2'
const companyAggregateFlowId = 'f6b3dc55-1c9a-4a7d-aa17-7d3973c104fa'
const itemCreatedAt = '2018-04-15T11:05:52.191Z'
const itemCreatedBy = 'sub-createdBy-teamAggregateHandler'
const accountId = 'account-id-for-aggregateHandler-test'
const companyId = 'bb3f728a-3326-4817-a1ae-bd26b810acd1'
const teamAggregateId = '369e2d55-f7ef-4809-9757-d96474a4b44f'

const accountProfile = {
  accountId: accountId,
  firstName: 'Eric',
  lastName: 'DoeRustig',
  gender: 'M',
  dateOfBirth: '1984-01-23T15:22:39.212Z',
  zip: '3600'
}

const irrelevantStreamRecords = [
  {
    create: mode => ({
      mode,
      event: 'INSERT',
      keys: { key: `/${mode}/some-key`, submitted: itemCreatedAt },
      newItem: {
        mode,
        key: `/${mode}/some-key`,
        submitted: itemCreatedAt,
        submittedBy: itemCreatedBy,
        data: { createdAt: itemCreatedAt, createdBy: itemCreatedBy, structureVersion: 342 }
      }
    }),
    description: 'some-key'
  }
]

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.sleep = function sleep (ms) {
      return new Promise(resolve => {
        setTimeout(resolve, ms)
      })
    }

    this.getProfile = sinon.stub()
    this.getSlotByAccountId = sinon.stub()
    this.getActualStub = sinon.stub()
    this.getByIndexStub = sinon.stub()
    this.getTeam = sinon.stub()
    this.getCompany = sinon.stub()
    this.getVersionAtOrBeforeStub = sinon.stub()

    this.getProfile.resolves(accountProfile)
    const getAccountProfileFunction = getAccountProfile.implementation(this.getProfile)

    this.groupAggregateHandlerProxy = proxyquire('../../../../lib/stream/handlers/groupAggregateHandler', {
      '../../server/business/account/profile/get': getAccountProfileFunction,
      '../../server/business/account/slot/get': this.getSlotByAccountId,
      '../../server/aws/dynamodb/getActual': this.getActualStub,
      '../../server/aws/dynamodb/getByIndex': this.getByIndexStub,
      '../../server/business/team/get': this.getTeam,
      '../../server/business/company/get': this.getCompany,
      '../../server/aws/dynamodb/getVersionAtOrBefore': this.getVersionAtOrBeforeStub
    })

    this.getByIndexStub.resolves([
      {
        name: 'slot1'
      },
      {
        name: 'slot2'
      }
    ])

    this.groupAggregateHandlerProxy.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(5000) // make sure flotsam is deleted
    this.groupAggregateHandlerProxy.contract.verifyPostconditions = true

    await this.dynamodbFixture.clean()
  })

  irrelevantStreamRecords.forEach(sr => {
    /**
     * Handler return false and must to nothing.
     */
    it(`return false, must do nothing for ${sr.description}`, async function () {
      this.timeout(5000)
      const result = await this.groupAggregateHandlerProxy(flowId, sot, sr.create(this.mode))
      result.should.be.false()
    })
  })

  describe('should work for GROUP PROFILE', function () {
    describe('Team', function () {
      beforeEach(async function () {
        this.initialTeamAggregateData = {
          createdAt: '2020-05-24T14:22:39.212Z',
          createdBy: 'sub-createdBy-teamAggregate',
          structureVersion: 1,
          teamAggregateId: teamAggregateId,
          totalSteps: 2500,
          totalDistance: 3000,
          averageSteps: 4167,
          averageDistance: 3846,
          groupType: GroupType.teamType,
          name: 'Roze Mars Dev Team',
          rank: 10,
          totalTeams: 100,
          hasLogo: false
        }

        this.getTeam.resolves({})

        const teamAggregate = new TeamAggregate({
          mode: this.mode,
          dto: {
            ...this.initialTeamAggregateData
          },
          sot: this.initialTeamAggregateData.createdAt,
          sub: this.initialTeamAggregateData.createdBy
        })

        const actualItem = {
          ...teamAggregate.toItem(),
          flowId: teamAggregateFlowId,
          submitted: 'actual',
          sort_key_1: this.initialTeamAggregateData.totalSteps,
          sort_key_2: this.initialTeamAggregateData.totalSteps,
          sort_key_3: undefined
        }

        this.teamAggregateKey = `/${this.mode}/teamAggregate/${teamAggregateId}`

        await this.dynamodbFixture.putItem(actualItem)

        const retrieved = await this.dynamodbFixture.hasArrived()

        console.log('INITIAL TeamAggregate:')
        console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
      })
      it('MODIFY accountprofile => returns FALSE', async function () {
        console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

        const key = `/${this.mode}/group/${teamAggregateId}/publicProfile`
        const keys = { key, submitted: 'actual' }
        const streamRecord = {
          mode: this.mode,
          event: 'MODIFY',
          keys,
          oldItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: teamAggregateId,
              name: 'De fonskes'
            }
          },
          newItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: teamAggregateId,
              name: 'De fonskes'
            }
          }
        }
        const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

        should(result).be.false()

        await this.sleep(1000)

        const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)

        dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
      })

      it('MODIFY accountprofile => returns undefined and updates teamAggregate', async function () {
        console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

        const key = `/${this.mode}/group/${teamAggregateId}/publicProfile`
        const keys = { key, submitted: 'actual' }
        const streamRecord = {
          mode: this.mode,
          event: 'MODIFY',
          keys,
          oldItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: teamAggregateId,
              name: 'De fonskes'
            }
          },
          newItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: teamAggregateId,
              name: 'De fonskes aangepast'
            }
          }
        }
        const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

        should(result).be.undefined()

        await this.sleep(1000)

        const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)

        const expected = {
          ...this.initialTeamAggregateData,
          name: 'De fonskes aangepast'
        }

        dynamodbItem.data.should.deepEqual(expected)
      })
    })

    describe('Company', function () {
      beforeEach(async function () {
        this.initialCompanyAggregateData = {
          createdAt: '2020-05-29T21:15:52.191Z',
          createdBy: 'sub-createdBy-companyAggregate',
          structureVersion: 1,
          companyId: companyId,
          totalSteps: 3000,
          totalDistance: 2250,
          averageSteps: 3000,
          averageDistance: 2250,
          name: 'Lords of the underworld',
          rank: 5,
          totalCompanies: 10,
          hasLogo: false
        }

        this.getCompany.resolves({})

        const companyAggregate = new CompanyAggregate({
          mode: this.mode,
          dto: {
            ...this.initialCompanyAggregateData
          },
          sot: this.initialCompanyAggregateData.createdAt,
          sub: this.initialCompanyAggregateData.createdBy
        })

        const actualItem = {
          ...companyAggregate.toItem(),
          flowId: companyAggregateFlowId,
          submitted: 'actual',
          sort_key_1: this.initialCompanyAggregateData.averageSteps,
          sort_key_2: this.initialCompanyAggregateData.averageSteps,
          sort_key_3: undefined
        }

        this.companyAggregateKey = `/${this.mode}/companyAggregate/${companyId}`

        await this.dynamodbFixture.putItem(actualItem)

        const retrieved = await this.dynamodbFixture.hasArrived()

        console.log('INITIAL CompanyAggregate:')
        console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
      })
      it('MODIFY accountprofile => returns FALSE', async function () {
        console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

        const key = `/${this.mode}/group/${companyId}/publicProfile`
        const keys = { key, submitted: 'actual' }
        const streamRecord = {
          mode: this.mode,
          event: 'MODIFY',
          keys,
          oldItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: companyId,
              name: 'De fonskes'
            }
          },
          newItem: {
            mode: this.mode,
            key,
            submitted: 'actual',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: itemCreatedAt,
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: companyId,
              name: 'De fonskes'
            }
          }
        }
        const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

        should(result).be.false()

        const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)

        dynamodbItem.data.should.deepEqual(this.initialCompanyAggregateData)
      })

      it('should work for INSERT company profile', async function () {
        console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

        const key = `/${this.mode}/group/${companyId}/publicProfile`
        const keys = { key, submitted: '2020-06-02T14:22:39.212Z' }

        const streamRecord = {
          mode: this.mode,
          event: 'INSERT',
          keys,
          newItem: {
            mode: this.mode,
            key,
            submitted: '2020-06-02T14:22:39.212Z',
            submittedBy: itemCreatedBy,
            data: {
              createdAt: '2020-06-02T14:22:39.212Z',
              createdBy: itemCreatedBy,
              structureVersion: 342,
              id: companyId,
              name: 'De fonskes',
              logo: 'amazing logo'
            }
          }
        }

        const expected = {
          ...this.initialCompanyAggregateData,
          name: streamRecord.newItem.data.name,
          hasLogo: true
        }

        this.getTeam.throws(Boom.notFound)
        this.getCompany.resolves({})

        const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)
        console.log(result)

        this.getCompany.should.be.called()
        const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
        dynamodbItem.data.should.deepEqual(expected)
      })
    })

    it('should return false when not found', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${teamAggregateId}/publicProfile`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: teamAggregateId,
            name: 'De fonskes'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: teamAggregateId,
            name: 'De fonskes aangepast'
          }
        }
      }

      this.getTeam.throws(Boom.notFound)
      this.getCompany.throws(Boom.notFound)

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()
    })
  })

  describe('should work for SUBGROUP', function () {
    beforeEach(async function () {
      this.initialTeamAggregateData = {
        createdAt: '2020-05-24T14:22:39.212Z',
        createdBy: 'sub-createdBy-teamAggregate',
        structureVersion: 1,
        teamAggregateId: teamAggregateId,
        totalSteps: 2500,
        totalDistance: 3000,
        averageSteps: 4167,
        averageDistance: 3846,
        companyId: companyId,
        groupType: GroupType.companyType,
        name: 'Roze Mars Dev Team',
        rank: 10,
        totalTeams: 100,
        hasLogo: false
      }

      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          ...this.initialTeamAggregateData
        },
        sot: this.initialTeamAggregateData.createdAt,
        sub: this.initialTeamAggregateData.createdBy
      })

      const actualItem = {
        ...teamAggregate.toItem(),
        flowId: teamAggregateFlowId,
        submitted: 'actual',
        sort_key_1: this.initialTeamAggregateData.totalSteps,
        sort_key_2: this.initialTeamAggregateData.totalSteps,
        sort_key_3: undefined
      }

      this.teamAggregateKey = `/${this.mode}/teamAggregate/${teamAggregateId}`

      await this.dynamodbFixture.putItem(actualItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL TeamAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
    })

    it('INSERT subgroup => returns undefined & update TeamAggregate ', async function () {
      const key = `/${this.mode}/subgroup/${teamAggregateId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: '2020-06-02T14:22:39.212Z',
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: teamAggregateId,
            groupId: companyId,
            name: 'Fonskes',
            logo: 'Amazing logo',
            groupType: 'Company',
            accountId: accountId
          }
        }
      }

      this.getVersionAtOrBeforeStub.returns({
        data: {
          createdAt: '2021-03-01T14:32:07.328Z',
          createdBy: 'automated-test',
          id: companyId,
          name: 'Test-Company',
          structureVersion: 1
        },
        flowId: flowId,
        key: `/dev-experiment/group/${companyId}/publicProfile`,
        mode: 'dev-experiment',
        partition_key_A: '/dev-experiment/groupProfile',
        sort_key_A: '2021-03-01T14:32:07.328Z',
        submitted: '2021-03-01T14:32:07.328Z',
        submittedBy: 'automated-test'
      })
      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)
      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        name: 'Fonskes',
        extendedName: 'Fonskes (Test-Company)',
        hasLogo: true
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY activity (no update of name or logo) => returns false', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/subgroup/${teamAggregateId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: '2020-06-02T14:22:39.212Z',
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: teamAggregateId,
            groupId: companyId,
            name: 'Fonskes',
            logo: 'Amazing logo',
            groupType: 'Company',
            accountId: accountId
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: '2020-06-02T14:22:39.212Z',
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: teamAggregateId,
            groupId: companyId,
            name: 'Fonskes',
            logo: 'Amazing logo',
            groupType: 'Company',
            accountId: accountId
          }
        }
      }
      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })
  })

  describe('Should work for SLOT', function () {
    beforeEach(async function () {
      this.teamAggregateId = '37be5654-5c89-4eb1-944f-15b736c12706'
      this.groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      this.slotId = '3f5ae0c6-eb97-40e7-a8fe-0ca6543a7c27'
      this.accountId = 'account-id-teamAggregateHandler'
      this.flowId = uuidv4()

      this.initialTeamAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-teamAggregate',
        structureVersion: 1,
        teamAggregateId: this.teamAggregateId,
        totalSteps: 2500,
        totalDistance: 2000,
        averageSteps: 2500,
        averageDistance: 2000,
        groupType: GroupType.companyType,
        companyId: this.groupId,
        name: 'Test Team Slot',
        rank: 10,
        totalTeams: 100,
        hasLogo: false
      }

      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          ...this.initialTeamAggregateData
        },
        sot: this.initialTeamAggregateData.createdAt,
        sub: this.initialTeamAggregateData.createdBy
      })

      const actualItem = {
        ...teamAggregate.toItem(),
        flowId: this.flowId,
        submitted: 'actual',
        sort_key_1: this.initialTeamAggregateData.averageSteps,
        sort_key_2: this.initialTeamAggregateData.averageSteps,
        sort_key_3: undefined
      }
      this.teamAggregateKey = `/${this.mode}/teamAggregate/${this.teamAggregateId}`

      await this.dynamodbFixture.putItem(actualItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL TeamAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
    })

    it('MODIFY slot (no update of subgroupId) => returns FALSE)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 344,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        }
      }
      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('INSERT Slot  => returns FALSE', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)
      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 344,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        }
      }
      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY Slot (update of subgroup) => returns undefined & update TeamAggregate)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        }
      }

      this.getActualStub.onCall(0).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          accountId: this.accountId,
          totalSteps: 5400,
          totalDistance: 4560,
          level: 1,
          stepsToNextLevel: 14600,
          groupId: this.groupId,
          groupType: this.groupType,
          subGroupId: this.subGroupId,
          name: this.name,
          rank: this.rank,
          totalParticipants: this.totalParticipants
        }
      })
      this.getActualStub.onCall(1).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          ...this.initialTeamAggregateData
        }
      })

      this.getByIndexStub.resolves([
        {
          data: {
            id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        },
        {
          data: {
            id: '4e9b15af-e66f-4bbf-adfe-e7c06c04e785',
            accountId: 'other-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: 'cf3fc7d6-736b-46bd-b296-1657a0a90568'
          }
        },
        {
          data: {
            id: '063d6cd1-05db-4fc5-8514-f45f78d20edb',
            accountId: 'another-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        }
      ])

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 7900,
        totalDistance: 6560
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY Slot (set averageSteps/Distance to totalSteps when 1 or less memberSlots are found)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        }
      }

      this.getActualStub.onCall(0).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          accountId: this.accountId,
          totalSteps: 5400,
          totalDistance: 4560,
          level: 1,
          stepsToNextLevel: 14600,
          groupId: this.groupId,
          groupType: this.groupType,
          subGroupId: this.subGroupId,
          name: this.name,
          rank: this.rank,
          totalParticipants: this.totalParticipants
        }
      })
      this.getActualStub.onCall(1).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          ...this.initialTeamAggregateData
        }
      })

      this.getByIndexStub.resolves([
        {
          data: {
            id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '17f31e43-6a10-406a-84a7-789406fc7e2d'
          }
        },
        {
          data: {
            id: '4e9b15af-e66f-4bbf-adfe-e7c06c04e785',
            accountId: 'other-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: 'cf3fc7d6-736b-46bd-b296-1657a0a90568'
          }
        },
        {
          data: {
            id: '063d6cd1-05db-4fc5-8514-f45f78d20edb',
            accountId: 'another-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '80dc7486-373d-4269-879c-2c9c414e7ce8'
          }
        }
      ])

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 7900,
        totalDistance: 6560
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY Slot (delete user from subgroup) => returns undefined & update TeamAggregate)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        }
      }

      this.getActualStub.onCall(0).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          accountId: this.accountId,
          totalSteps: 2500,
          totalDistance: 2000,
          level: 1,
          stepsToNextLevel: 14600,
          groupId: this.groupId,
          groupType: this.groupType,
          subGroupId: this.subGroupId,
          name: this.name,
          rank: this.rank,
          totalParticipants: this.totalParticipants
        }
      })
      this.getActualStub.onCall(1).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          ...this.initialTeamAggregateData
        }
      })

      this.getByIndexStub.resolves([
        {
          data: {
            id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        },
        {
          data: {
            id: '4e9b15af-e66f-4bbf-adfe-e7c06c04e785',
            accountId: 'other-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: 'cf3fc7d6-736b-46bd-b296-1657a0a90568'
          }
        },
        {
          data: {
            id: '063d6cd1-05db-4fc5-8514-f45f78d20edb',
            accountId: 'another-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        }
      ])

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 0,
        totalDistance: 0
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY Slot (delete user) => returns FALSE)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/group/${this.groupId}/slot/${this.slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            accountId: this.accountId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.teamAggregateId
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: this.slotId,
            groupType: GroupType.companyType,
            groupId: this.groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        }
      }

      this.getActualStub.onCall(0).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          accountId: this.accountId,
          totalSteps: 2500,
          totalDistance: 2000,
          level: 1,
          stepsToNextLevel: 14600,
          groupId: this.groupId,
          groupType: this.groupType,
          subGroupId: this.subGroupId,
          name: this.name,
          rank: this.rank,
          totalParticipants: this.totalParticipants
        }
      })
      this.getActualStub.onCall(1).resolves({
        mode: this.mode,
        key: this.key,
        submitted: this.createdAt,
        submittedBy: this.createdBy,
        data: {
          ...this.initialTeamAggregateData
        }
      })

      this.getByIndexStub.resolves([
        {
          data: {
            id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        },
        {
          data: {
            id: '4e9b15af-e66f-4bbf-adfe-e7c06c04e785',
            accountId: 'other-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: 'cf3fc7d6-736b-46bd-b296-1657a0a90568'
          }
        },
        {
          data: {
            id: '063d6cd1-05db-4fc5-8514-f45f78d20edb',
            accountId: 'another-accountid',
            groupType: GroupType.companyType,
            groupId: this.initialTeamAggregateData.companyId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: this.initialTeamAggregateData.teamAggregateId
          }
        }
      ])

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })
  })

  describe('Should work for AccountAggregate - teamAggregate', function () {
    beforeEach(async function () {
      this.teamAggregateId = '37be5654-5c89-4eb1-944f-15b736c12706'
      this.groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      this.accountId = 'account-id-teamAggregateHandler'
      this.flowId = uuidv4()

      this.initialTeamAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-teamAggregate',
        structureVersion: 1,
        teamAggregateId: this.teamAggregateId,
        totalSteps: 4500,
        totalDistance: 3500,
        averageSteps: 2500,
        averageDistance: 2000,
        groupType: GroupType.teamType,
        name: 'Test Team Slot',
        rank: 10,
        totalTeams: 100,
        hasLogo: false
      }

      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          ...this.initialTeamAggregateData
        },
        sot: this.initialTeamAggregateData.createdAt,
        sub: this.initialTeamAggregateData.createdBy
      })

      const actualItem = {
        ...teamAggregate.toItem(),
        flowId: this.flowId,
        submitted: 'actual',
        sort_key_1: this.initialTeamAggregateData.averageSteps,
        sort_key_2: this.initialTeamAggregateData.averageSteps,
        sort_key_3: undefined
      }
      this.teamAggregateKey = `/${this.mode}/teamAggregate/${this.teamAggregateId}`

      await this.dynamodbFixture.putItem(actualItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL TeamAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
    })

    it('MODIFY AccountAggregate (delete user) => returns FALSE)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)
      const accountId = 'test-account-delete'

      const key = `/${this.mode}/accountAggregate/${accountId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: this.teamAggregateId,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 1500,
            totalParticipants: 100,
            totalSteps: 2000
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupId: this.teamAggregateId,
            groupType: GroupType.teamType,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 0,
            totalParticipants: 100,
            totalSteps: 0
          }
        }
      }

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY AccountAggregate (delete user) => returns undefined & update TeamAggregate)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)
      const accountId = 'test-account-delete'

      const key = `/${this.mode}/accountAggregate/${accountId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: this.teamAggregateId,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 1500,
            totalParticipants: 100,
            totalSteps: 2000
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupType: GroupType.teamType,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 0,
            totalParticipants: 100,
            totalSteps: 0
          }
        }
      }

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 2500,
        totalDistance: 2000
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })
  })

  describe('Should work for AccountAggregate - companyAggregate', function () {
    beforeEach(async function () {
      this.companyAggregateId = uuidv4()
      this.groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      this.accountId = 'account-id-groupAggregateHandler'
      this.flowId = uuidv4()

      this.initialCompanyAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-companyAggregate',
        structureVersion: 1,
        companyId: this.companyAggregateId,
        totalSteps: 4500,
        totalDistance: 3500,
        averageSteps: 2500,
        averageDistance: 2000,
        name: 'CompanyAggregate',
        rank: 10,
        totalCompanies: 100,
        hasLogo: false
      }

      const companyAggregate = new CompanyAggregate({
        mode: this.mode,
        dto: {
          ...this.initialCompanyAggregateData
        },
        sot: this.initialCompanyAggregateData.createdAt,
        sub: this.initialCompanyAggregateData.createdBy
      })

      const actualItem = {
        ...companyAggregate.toItem(),
        flowId: this.flowId,
        submitted: 'actual',
        sort_key_1: this.initialCompanyAggregateData.averageSteps,
        sort_key_2: this.initialCompanyAggregateData.averageSteps,
        sort_key_3: undefined
      }
      this.companyAggregateKey = `/${this.mode}/companyAggregate/${this.companyAggregateId}`

      await this.dynamodbFixture.putItem(actualItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL CompanyAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)
    })

    it('MODIFY AccountAggregate (delete user) => returns undefined & update CompanyAggregate)', async function () {
      console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)
      const accountId = 'test-account-delete'

      const key = `/${this.mode}/accountAggregate/${accountId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: this.companyAggregateId,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 1500,
            totalParticipants: 100,
            totalSteps: 2000
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            groupType: GroupType.companyType,
            name: 'aggregate',
            previousLevel: 1,
            totalDistance: 0,
            totalParticipants: 100,
            totalSteps: 0
          }
        }
      }

      const result = await this.groupAggregateHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedCompanyAggregate = {
        ...this.initialCompanyAggregateData,
        totalSteps: 2500,
        totalDistance: 2000
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedCompanyAggregate)
    })
  })
})
