/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)

const util = require('util')
const DynamodbFixture = require('../../../_DynamodbFixture')
const UUID = require('../../../../lib/ppwcode/UUID')
const GroupType = require('../../../../lib/api/group/GroupType')
const AccountAggregate = require('../../../../lib/server/business/aggregates/account/AccountAggregate')
const TeamAggregate = require('../../../../lib/server/business/aggregates/team/TeamAggregate')
const CompanyAggregate = require('../../../../lib/server/business/aggregates/company/CompanyAggregate')
const Slot = require('../../../../lib/server/business/slot/Slot')
const { v4: uuidv4 } = require('uuid')
const should = require('should')
const getActual = require('../../../../lib/server/aws/dynamodb/getActual')
const TrackerType = require('../../../../lib/api/account/trackerConnection/TrackerType')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const { contract: getAccountProfile } = require('../../../../lib/server/business/account/profile/get')
const boom = require('@hapi/boom')

const sot = '2020-04-15T11:05:52.191Z'
const flowId = '6ca3b82a-840e-4264-b973-b4128913abdd'
const accountAggregateFlowId = '8d8ee951-2dd8-476f-b2cc-3e650cb411a2'
const itemCreatedAt = '2018-04-15T11:05:52.191Z'
const itemCreatedBy = 'sub-createdBy-accountAggregateHandler'
const activityDate = '2020-01-23T15:22:39.212Z'
const accountId = 'c7c7d869-cebb-4a30-a466-4055a7e1bcce'
const companyId = 'b0137e38-a623-4f0d-ae8d-eb1b992dae58'
const teamAggregateId = '369e2d55-f7ef-4809-9757-d96474a4b44f'
const companyAggregateId = '369e2d55-f7ef-4809-9757-d96474a4b44f'
const teamAggregateFlowId = '125ed98a-97e7-4a0f-a908-94fe5dcab4e6'
const takenSlotId = '0e02fb95-5928-4d37-bbeb-b16dfd38b337'
const freeSlotId = '44181afc-40dc-46ed-baa2-36cafe1de98f'
const paymentId = '29cf0c9c-ac53-482f-a344-d7378de75076'

const accountProfile = {
  accountId: accountId,
  firstName: 'Eric',
  lastName: 'DoeRustig',
  gender: 'M',
  dateOfBirth: '1984-01-23T15:22:39.212Z',
  zip: '3600'
}

describe(testName, function () {
  this.retries(2)

  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.initialAccountAggregateData = {
      createdAt: '2020-04-29T21:15:52.191Z',
      createdBy: 'sub-createdBy-accountAggregate',
      structureVersion: 78,
      accountId: accountId,
      totalSteps: 20361,
      totalDistance: 3000,
      level: 2,
      previousLevel: 2,
      stepsToNextLevel: 1500,
      groupId: UUID.example,
      groupType: GroupType.companyType,
      name: 'some-kind-of-a-name',
      rank: 10,
      totalParticipants: 100,
      acknowledged: true
    }

    const accountAggregate = new AccountAggregate({
      mode: this.mode,
      dto: {
        ...this.initialAccountAggregateData
      },
      sot: this.initialAccountAggregateData.createdAt,
      sub: this.initialAccountAggregateData.createdBy
    })

    const actualItem = {
      ...accountAggregate.toItem(),
      flowId: accountAggregateFlowId,
      submitted: 'actual',
      sort_key_1: this.initialAccountAggregateData.totalSteps,
      sort_key_2: this.initialAccountAggregateData.totalSteps,
      sort_key_3: this.initialAccountAggregateData.totalSteps
    }

    this.accountAggregateKey = `/${this.mode}/accountAggregate/${accountId}`

    await this.dynamodbFixture.putItem(actualItem)

    const retrievedAccountAggregate = await this.dynamodbFixture.hasArrived()

    this.sleep = function sleep (ms) {
      return new Promise(resolve => {
        setTimeout(resolve, ms)
      })
    }

    console.log('INITIAL AcountAggregate:')
    console.log(`${util.inspect(retrievedAccountAggregate, false, null, true /* enable colors */)}`)

    this.getProfile = sinon.stub()
    this.getSlotByAccountId = sinon.stub()
    this.getActualStub = sinon.stub()

    const getAccountProfileFunction = getAccountProfile.implementation(this.getProfile)
    this.activityHandlerProxy = proxyquire('../../../../lib/stream/handlers/activityHandler', {
      '../../server/business/account/profile/get': getAccountProfileFunction,
      '../../server/business/account/slot/get': this.getSlotByAccountId,
      '../../server/aws/dynamodb/getActual': this.getActualStub
    })

    this.getSlotByAccountId.resolves({
      id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
      accountId: accountId,
      groupType: GroupType.teamType,
      groupId: companyId,
      paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
      subgroupId: undefined
    })

    this.getProfile.resolves(accountProfile)

    this.activityHandlerProxy.contract.verifyPostconditions = true
    this.activityHandlerProxy.setVerifyHelperPostconditions(true)

    this.timeout(5000)
  })
  afterEach(async function () {
    this.timeout(5000) // make sure flotsam is deleted
    this.activityHandlerProxy.contract.verifyPostconditions = false
    this.activityHandlerProxy.setVerifyHelperPostconditions(false)

    await this.dynamodbFixture.clean()
  })

  describe('Should work for ACTIVITY', function () {
    it('INSERT activity => returns undefined & update AccountAggregate)', async function () {
      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })
      should(result).be.undefined()

      const expectedAccountAggregate = {
        ...this.initialAccountAggregateData,
        totalSteps: 20461,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
      dynamodbItem.sort_key_1.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_2.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_3.should.be.equal(expectedAccountAggregate.totalSteps)
    })

    it('MODIFY activity (no update of steps or deleted) => returns false', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY activity (was deleted but updated steps) => returns false', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 200,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY activity (was deleted but re-added) => returns undefined & update AccountAggregate', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.undefined()

      const expectedAccountAggregate = {
        ...this.initialAccountAggregateData,
        totalSteps: 20461,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
      dynamodbItem.sort_key_1.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_2.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_3.should.be.equal(expectedAccountAggregate.totalSteps)
    })

    it('MODIFY activity (steps) => returns undefined and update steps & distance in accountAggregate', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 200,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.undefined()

      const expectedAccountAggregate = {
        ...this.initialAccountAggregateData,
        totalSteps: 20461,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
    })

    it('MODIFY activity (steps REAL DATA) => returns undefined and update steps & distance in accountAggregate', async function () {
      this.timeout(5000)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })

      const myAggregate = {
        flowId: '6a3a4ae2-9731-491b-a5e2-5b6396ce36b1',
        submittedBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
        sort_key_3: 72046,
        mode: this.mode,
        sort_key_2: 72046,
        sort_key_1: 72046,
        data: {
          accountId: '6YlChf6unh',
          createdAt: '2020-05-14T07:54:38.153Z',
          groupType: 'Team',
          createdBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
          groupId: '1adb559f-2a2b-486c-a765-48e84367e864',
          name: 'Marien Peter',
          totalSteps: 72046,
          rank: 1,
          previousLevel: 2,
          totalParticipants: 2,
          totalDistance: 59077,
          structureVersion: 1
        },
        partition_key_3: `/${this.mode}/accountAggregate/subgroup/undefined`,
        partition_key_2: `/${this.mode}/accountAggregate/group/1adb559f-2a2b-486c-a765-48e84367e864`,
        submitted: 'actual',
        partition_key_1: `/${this.mode}/accountAggregate`,
        key: `/${this.mode}/accountAggregate/6YlChf6unh`
      }

      await this.dynamodbFixture.putItem(myAggregate)

      const myAccountProfile = {
        accountId: '6YlChf6unh',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      }

      // const key = `/${this.mode}/activity/account/${claimAccountProfile.accountId}/date/${activityDate}`
      // const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys: {
          submitted: 'actual',
          key: `/${this.mode}/activity/account/6YlChf6unh/date/2020-05-13`
        },
        oldItem: {
          mode: this.mode,
          submittedBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
          sort_key_A: '2020-05-13',
          sort_key_B: '2020-05-13',
          submitted: 'actual',
          data: {
            createdAt: '2020-05-14T17:49:00.803Z',
            accountId: '6YlChf6unh',
            activityDate: '2020-05-13',
            deleted: false,
            numberOfSteps: 16000,
            distance: 13120,
            createdBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
            trackerType: 'manual',
            structureVersion: 1
          },
          partition_key_B: `/${this.mode}/activity/account/6YlChf6unh`,
          partition_key_A: `/${this.mode}/activity`,
          flowId: '6975b956-5499-4ff9-ac67-f24a370d82a7',
          key: `/${this.mode}/activity/account/6YlChf6unh/date/2020-05-13`
        },
        newItem: {
          mode: this.mode,
          submittedBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
          sort_key_A: '2020-05-13',
          sort_key_B: '2020-05-13',
          submitted: 'actual',
          data: {
            createdAt: '2020-05-14T19:55:40.261Z',
            accountId: '6YlChf6unh',
            activityDate: '2020-05-13',
            deleted: false,
            numberOfSteps: 1040,
            distance: 853,
            createdBy: 'auth0%7C5ebcf8a2be603d0bcc75a328',
            trackerType: 'manual',
            structureVersion: 1
          },
          partition_key_B: `/${this.mode}/activity/account/6YlChf6unh`,
          partition_key_A: `/${this.mode}/activity`,
          flowId: 'f2e4157f-61d6-4fdc-b394-3e3f0e7bf554',
          key: `/${this.mode}/activity/account/6YlChf6unh/date/2020-05-13`
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: myAccountProfile
      })
      should(result).be.undefined()

      const expectedDistance =
        myAggregate.data.totalDistance + (streamRecord.newItem.data.distance - streamRecord.oldItem.data.distance)

      const expectedAccountAggregate = {
        ...myAggregate.data,
        totalSteps:
          myAggregate.data.totalSteps +
          (streamRecord.newItem.data.numberOfSteps - streamRecord.oldItem.data.numberOfSteps),
        totalDistance: expectedDistance
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, myAggregate.key, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
      dynamodbItem.sort_key_1.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_2.should.be.equal(expectedAccountAggregate.totalSteps)
      dynamodbItem.sort_key_3.should.be.equal(expectedAccountAggregate.totalSteps)
    })

    it('MODIFY activity (set deleted TRUE) => returns undefined and update steps & distance in accountAggregate', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      this.getProfile.resolves({
        accountId: 'id-OF-Eric',
        firstName: 'Eric',
        lastName: 'DoeRustig',
        gender: 'M',
        dateOfBirth: '1984-01-23T15:22:39.212Z',
        zip: '3600'
      })
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 500,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 500,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.undefined()

      const expectedAccountAggregate = {
        ...this.initialAccountAggregateData,
        previousLevel: 2,
        totalSteps: 19861,
        totalDistance: 2590
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
    })
  })

  describe('Should work for ACTIVITY (TeamAggregate => Company)', function () {
    beforeEach(async function () {
      this.initialCompanyAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-createdBy-teamAggregate',
        structureVersion: 1,
        totalSteps: 2500,
        totalDistance: 3000,
        averageSteps: 1,
        averageDistance: 1,
        companyId: companyId,
        name: 'Roze Mars Dev Team',
        extendedName: 'Roze Mars Dev Team (Peopleware)',
        rank: 10,
        totalCompanies: 100,
        hasLogo: false
      }

      const companyAggregate = new CompanyAggregate({
        mode: this.mode,
        dto: {
          ...this.initialCompanyAggregateData
        },
        sot: this.initialCompanyAggregateData.createdAt,
        sub: this.initialCompanyAggregateData.createdBy
      })

      const actualCompanyAggregateItem = {
        ...companyAggregate.toItem(),
        flowId: teamAggregateFlowId,
        submitted: 'actual',
        sort_key_1: this.initialCompanyAggregateData.totalSteps,
        sort_key_2: this.initialCompanyAggregateData.totalSteps,
        sort_key_3: undefined
      }

      this.companyAggregateKey = `/${this.mode}/companyAggregate/${companyId}`

      await this.dynamodbFixture.putItem(actualCompanyAggregateItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL CompanyAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)

      this.getSlotByAccountId.resolves({
        id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
        accountId: accountId,
        groupType: GroupType.companyType,
        groupId: this.initialCompanyAggregateData.companyId,
        paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
      })

      this.getActualStub.resolves({
        ...actualCompanyAggregateItem
      })
    })

    it('INSERT activity => returns undefined & update CompanyAggregate ', async function () {
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)
      should(result).be.undefined()

      const expectedCompanyAggregate = {
        ...this.initialCompanyAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedCompanyAggregate)
      dynamodbItem.should.not.have.ownProperty('sort_key_3')
    })

    it('MODIFY activity (no update of steps or deleted) => returns false', async function () {
      console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialCompanyAggregateData)
    })

    it('MODIFY activity (was deleted but updated steps) => returns false', async function () {
      console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 200,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialCompanyAggregateData)
    })

    it('MODIFY activity (was deleted but re-added) => returns undefined & update CompanyAggregate', async function () {
      console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedCompanyAggregate = {
        ...this.initialCompanyAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedCompanyAggregate)
      dynamodbItem.should.not.have.ownProperty('sort_key_3')
    })

    it('MODIFY activity (steps) => returns undefined and update steps & distance in CompanyAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 200,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialCompanyAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY activity (set deleted TRUE) => returns undefined and update steps & distance in CompanyAggregate', async function () {
      console.log(`CompanyAggregate key => ${this.companyAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedCompanyAggregate = {
        ...this.initialCompanyAggregateData,
        totalSteps: 2400,
        totalDistance: 2918
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.companyAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedCompanyAggregate)
    })
  })

  describe('Should work for ACTIVITY (TeamAggregate => Subgroup)', function () {
    beforeEach(async function () {
      this.initialCompanyAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-createdBy-teamAggregate',
        structureVersion: 1,
        teamAggregateId: teamAggregateId,
        totalSteps: 2500,
        totalDistance: 3000,
        averageSteps: 1,
        averageDistance: 1,
        companyId: companyId,
        groupType: GroupType.companyType,
        name: 'Roze Mars Dev Team',
        extendedName: 'Roze Mars Dev Team (Peopleware)',
        rank: 10,
        totalCompanies: 100,
        hasLogo: false
      }

      this.initialTeamAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-createdBy-teamAggregate',
        structureVersion: 1,
        teamAggregateId: companyAggregateId,
        totalSteps: 5000,
        totalDistance: 4000,
        averageSteps: 1,
        averageDistance: 1,
        groupType: GroupType.teamType,
        companyId: companyId,
        name: 'Roze Mars Dev Team',
        extendedName: 'Roze Mars Dev Team (Peopleware)',
        rank: 10,
        totalTeams: 100,
        hasLogo: false
      }

      const companyAggregate = new CompanyAggregate({
        mode: this.mode,
        dto: {
          ...this.initialCompanyAggregateData
        },
        sot: this.initialCompanyAggregateData.createdAt,
        sub: this.initialCompanyAggregateData.createdBy
      })

      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          ...this.initialTeamAggregateData
        },
        sot: this.initialTeamAggregateData.createdAt,
        sub: this.initialTeamAggregateData.createdBy
      })

      const actualCompanyAggregateItem = {
        ...companyAggregate.toItem(),
        flowId: teamAggregateFlowId,
        submitted: 'actual',
        sort_key_1: this.initialCompanyAggregateData.totalSteps,
        sort_key_2: this.initialCompanyAggregateData.totalSteps,
        sort_key_3: undefined
      }

      const actualTeamAggregateItem = {
        ...teamAggregate.toItem(),
        flowId: teamAggregateFlowId,
        submitted: 'actual',
        sort_key_1: this.initialTeamAggregateData.totalSteps,
        sort_key_2: this.initialTeamAggregateData.totalSteps,
        sort_key_3: undefined
      }

      this.teamAggregateKey = `/${this.mode}/teamAggregate/${teamAggregateId}`
      this.companyAggregateKey = `/${this.mode}/companyAggregate/${companyAggregateId}`

      await this.dynamodbFixture.putItem(actualCompanyAggregateItem)
      await this.dynamodbFixture.putItem(actualTeamAggregateItem)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL TeamAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)

      this.getSlotByAccountId.resolves({
        id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
        accountId: accountId,
        groupType: GroupType.companyType,
        groupId: this.initialTeamAggregateData.companyId,
        paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
        subgroupId: this.initialTeamAggregateData.teamAggregateId
      })

      this.getActualStub.resolves({
        ...actualTeamAggregateItem
      })
    })

    it('INSERT activity => returns undefined & update TeamAggregate ', async function () {
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)
      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 5100,
        totalDistance: 4082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
      dynamodbItem.should.not.have.ownProperty('sort_key_3')
    })

    it('MODIFY activity (no update of steps or deleted) => returns false', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY activity (was deleted but updated steps) => returns false', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 200,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY activity (was deleted but re-added) => returns undefined & update TeamAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 5100,
        totalDistance: 4082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
      dynamodbItem.should.not.have.ownProperty('sort_key_3')
    })

    it('MODIFY activity (steps) => returns undefined and update steps & distance in TeamAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 200,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 5100,
        totalDistance: 4082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY activity (set deleted TRUE) => returns undefined and update steps & distance in TeamAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 4900,
        totalDistance: 3918
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })
  })

  describe('Should work for ACTIVITY (TeamAggregate => Team)', function () {
    beforeEach(async function () {
      this.initialTeamAggregateData = {
        createdAt: '2020-04-29T21:15:52.191Z',
        createdBy: 'sub-createdBy-teamAggregate',
        structureVersion: 1,
        teamAggregateId: teamAggregateId,
        totalSteps: 2500,
        totalDistance: 3000,
        averageSteps: 1,
        averageDistance: 1,
        groupType: GroupType.teamType,
        name: 'Roze Mars Dev Team',
        rank: 10,
        totalTeams: 100,
        hasLogo: false
      }

      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          ...this.initialTeamAggregateData
        },
        sot: this.initialTeamAggregateData.createdAt,
        sub: this.initialTeamAggregateData.createdBy
      })

      const actualItem = {
        ...teamAggregate.toItem(),
        flowId: teamAggregateFlowId,
        submitted: 'actual',
        sort_key_1: this.initialTeamAggregateData.totalSteps,
        sort_key_2: this.initialTeamAggregateData.totalSteps,
        sort_key_3: undefined
      }

      this.teamAggregateKey = `/${this.mode}/teamAggregate/${teamAggregateId}`

      await this.dynamodbFixture.putItem(actualItem)

      const takenSlot = new Slot({
        mode: this.mode,
        dto: {
          id: takenSlotId,
          accountId,
          groupType: GroupType.teamType,
          groupId: this.initialTeamAggregateData.teamAggregateId,
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          paymentId,
          structureVersion: 2
        }
      })

      const actualTakenSlot = {
        ...takenSlot.toItem(),
        flowId,
        submitted: 'actual'
      }

      const freeSlot = new Slot({
        mode: this.mode,
        dto: {
          id: freeSlotId,
          groupType: GroupType.teamType,
          groupId: this.initialTeamAggregateData.teamAggregateId,
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          paymentId,
          structureVersion: 2
        }
      })

      const actualFreeSlot = {
        ...freeSlot.toItem(),
        flowId,
        submitted: 'actual'
      }

      await this.dynamodbFixture.putItem(actualTakenSlot)
      await this.dynamodbFixture.putItem(actualFreeSlot)

      const retrieved = await this.dynamodbFixture.hasArrived()

      console.log('INITIAL TeamAggregate:')
      console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)

      this.getSlotByAccountId.resolves({
        id: 'f0efe714-82ad-414d-9de2-75f171a46a88',
        accountId: accountId,
        groupType: GroupType.teamType,
        groupId: this.initialTeamAggregateData.teamAggregateId,
        paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
      })

      this.getActualStub.resolves({
        ...actualItem
      })
    })

    it('INSERT activity => returns undefined & update TeamAggregate ', async function () {
      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)
      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
      dynamodbItem.should.not.have.ownProperty('sort_key_3')
    })

    it('MODIFY activity (no update of steps or deleted) => returns false', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 100,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
        accountProfile: accountProfile
      })

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY activity (was deleted but updated steps) => returns false', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.fitbitType,
            numberOfSteps: 200,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      dynamodbItem.data.should.deepEqual(this.initialTeamAggregateData)
    })

    it('MODIFY activity (steps) => returns undefined and update steps & distance in TeamAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 200,
            deleted: false
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY activity (update averageSteps/Distance to totals when 1 or less slots are found)', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 200,
            deleted: false
          }
        }
      }

      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 2600,
        totalDistance: 3082
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })

    it('MODIFY activity (set deleted TRUE) => returns undefined and update steps & distance in TeamAggregate', async function () {
      console.log(`TeamAggregate key => ${this.teamAggregateKey}`)

      const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: false
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            activityDate: '2020-01-23T15:22:39.212Z',
            trackerType: TrackerType.polarType,
            numberOfSteps: 100,
            deleted: true
          }
        }
      }
      const result = await this.activityHandlerProxy(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedTeamAggregate = {
        ...this.initialTeamAggregateData,
        totalSteps: 2400,
        totalDistance: 2918
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.teamAggregateKey, true)
      dynamodbItem.should.not.be.undefined()
      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedTeamAggregate)
    })
  })

  it('should return false for anything except ACTIVITY', async function () {
    const key = `/${this.mode}/group/${teamAggregateId}/publicProfile`
    const keys = { key, submitted: 'actual' }
    const streamRecord = {
      mode: this.mode,
      event: 'MODIFY',
      keys,
      oldItem: {
        mode: this.mode,
        key,
        submitted: 'actual',
        submittedBy: itemCreatedBy,
        data: {
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          structureVersion: 342,
          id: teamAggregateId,
          name: 'De fonskes'
        }
      },
      newItem: {
        mode: this.mode,
        key,
        submitted: 'actual',
        submittedBy: itemCreatedBy,
        data: {
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          structureVersion: 342,
          id: teamAggregateId,
          name: 'De fonskes'
        }
      }
    }
    const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
      accountProfile: accountProfile
    })
    should(result).be.false()
  })

  it('should return false when no SLOT is found', async function () {
    const key = `/${this.mode}/activity/account/${accountProfile.accountId}/date/${activityDate}`
    const keys = { key, submitted: 'actual' }
    const streamRecord = {
      mode: this.mode,
      event: 'MODIFY',
      keys,
      oldItem: {
        mode: this.mode,
        key,
        submitted: 'actual',
        submittedBy: itemCreatedBy,
        data: {
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          structureVersion: 342,
          accountId: accountId,
          activityDate: '2020-01-23T15:22:39.212Z',
          trackerType: TrackerType.polarType,
          numberOfSteps: 100,
          deleted: false
        }
      },
      newItem: {
        mode: this.mode,
        key,
        submitted: 'actual',
        submittedBy: itemCreatedBy,
        data: {
          createdAt: itemCreatedAt,
          createdBy: itemCreatedBy,
          structureVersion: 342,
          accountId: accountId,
          activityDate: '2020-01-23T15:22:39.212Z',
          trackerType: TrackerType.polarType,
          numberOfSteps: 200,
          deleted: false
        }
      }
    }

    this.getSlotByAccountId.throws(boom.notFound())

    const result = await this.activityHandlerProxy(flowId, sot, streamRecord, {
      accountProfile: accountProfile
    })
    should(result).be.false()
  })
})
