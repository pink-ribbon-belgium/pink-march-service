/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../../_testName')(module)

const util = require('util')
const DynamodbFixture = require('../../../_DynamodbFixture')
const accountAggregateHandler = require('../../../../lib/stream/handlers/accountAggregateHandler')
const UUID = require('../../../../lib/ppwcode/UUID')
const GroupType = require('../../../../lib/api/group/GroupType')
const AccountAggregate = require('../../../../lib/server/business/aggregates/account/AccountAggregate')
const { v4: uuidv4 } = require('uuid')
const should = require('should')
const getActual = require('../../../../lib/server/aws/dynamodb/getActual')
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const { contract: getAccountProfile } = require('../../../../lib/server/business/account/profile/get')

const sot = '2020-04-15T11:05:52.191Z'
const flowId = '6ca3b82a-840e-4264-b973-b4128913abdd'
const accountAggregateFlowId = '8d8ee951-2dd8-476f-b2cc-3e650cb411a2'
const itemCreatedAt = '2018-04-15T11:05:52.191Z'
const itemCreatedBy = 'sub-createdBy-accountAggregateHandler'
const accountId = 'account-id-for-aggregateHandler-test'

const claimAccountProfile = {
  accountId: accountId,
  firstName: 'Eric',
  lastName: 'DoeRustig',
  gender: 'M',
  dateOfBirth: '1984-01-23T15:22:39.212Z',
  zip: '3600'
}

const irrelevantStreamRecords = [
  {
    create: mode => ({
      mode,
      event: 'INSERT',
      keys: { key: `/${mode}/some-key`, submitted: itemCreatedAt },
      newItem: {
        mode,
        key: `/${mode}/some-key`,
        submitted: itemCreatedAt,
        submittedBy: itemCreatedBy,
        data: { createdAt: itemCreatedAt, createdBy: itemCreatedBy, structureVersion: 342 }
      }
    }),
    description: 'some-key'
  }
]

describe(testName, function () {
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.initialAccountAggregateData = {
      createdAt: '2020-04-29T21:15:52.191Z',
      createdBy: 'sub-createdBy-accountAggregate',
      structureVersion: 78,
      accountId: accountId,
      totalSteps: 20361,
      totalDistance: 3000,
      level: 2,
      previousLevel: 2,
      stepsToNextLevel: 1500,
      groupId: UUID.example,
      groupType: GroupType.companyType,
      name: 'some-kind-of-a-name',
      rank: 10,
      totalParticipants: 100,
      acknowledged: true
    }

    const accountAggregate = new AccountAggregate({
      mode: this.mode,
      dto: {
        ...this.initialAccountAggregateData
      },
      sot: this.initialAccountAggregateData.createdAt,
      sub: this.initialAccountAggregateData.createdBy
    })

    const actualItem = {
      ...accountAggregate.toItem(),
      flowId: accountAggregateFlowId,
      submitted: 'actual',
      sort_key_1: this.initialAccountAggregateData.totalSteps,
      sort_key_2: this.initialAccountAggregateData.totalSteps,
      sort_key_3: this.initialAccountAggregateData.totalSteps
    }

    this.accountAggregateKey = `/${this.mode}/accountAggregate/${accountId}`

    await this.dynamodbFixture.putItem(actualItem)

    const retrieved = await this.dynamodbFixture.hasArrived()

    this.sleep = function sleep (ms) {
      return new Promise(resolve => {
        setTimeout(resolve, ms)
      })
    }

    console.log('INITIAL AccountAggregate:')
    console.log(`${util.inspect(retrieved, false, null, true /* enable colors */)}`)

    this.getProfile = sinon.stub()
    const getAccountProfileFunction = getAccountProfile.implementation(this.getProfile)
    accountAggregateHandler.contract.verifyPostconditions = true
    this.accountAggregateHandlerProxy = proxyquire('../../../../lib/stream/handlers/accountAggregateHandler', {
      '../../server/business/account/profile/get': getAccountProfileFunction
    })
    this.accountAggregateHandlerProxy.contract.verifyPostconditions = true

    this.timeout(5000)
  })
  afterEach(async function () {
    this.timeout(5000) // make sure flotsam is deleted
    this.accountAggregateHandlerProxy.contract.verifyPostconditions = true

    await this.dynamodbFixture.clean()
  })

  irrelevantStreamRecords.forEach(sr => {
    /**
     * Handler return false and must to nothing.
     */
    it(`return false, must do nothing for ${sr.description}`, async function () {
      this.timeout(5000)
      const result = await accountAggregateHandler(flowId, sot, sr.create(this.mode))
      result.should.be.false()
    })
  })

  describe('Should work for ACCOUNT PROFILE', function () {
    it('INSERT accountprofile => returns FALSE', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      const key = `/${this.mode}/account/${claimAccountProfile.accountId}/publicProfile`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            firstName: 'Eddy',
            lastName: 'Hall',
            gender: 'M',
            dateOfBirth: '1935-01-08T15:22:39.212Z',
            zip: '2018'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY accountprofile (no update of firstName/LastName) => returns FALSE)', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      const key = `/${this.mode}/account/${claimAccountProfile.accountId}/publicProfile`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            firstName: 'Eddy',
            lastName: 'Hall',
            gender: 'M',
            dateOfBirth: '1935-01-08T15:22:39.212Z',
            zip: '2000'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            firstName: 'Eddy',
            lastName: 'Hall',
            gender: 'M',
            dateOfBirth: '1935-01-08T15:22:39.212Z',
            zip: '2018'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY accountprofile (update of firstName/LastName) => returns undefined & update AccountAggregate)', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      const key = `/${this.mode}/account/${claimAccountProfile.accountId}/publicProfile`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            firstName: 'Jeroen',
            lastName: 'Vermeulen',
            gender: 'M',
            dateOfBirth: '1935-01-08T15:22:39.212Z',
            zip: '2018'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            accountId: accountId,
            firstName: 'Eddy',
            lastName: 'Hall',
            gender: 'M',
            dateOfBirth: '1935-01-08T15:22:39.212Z',
            zip: '2018'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedAccountProfileAggregate = {
        ...this.initialAccountAggregateData,
        name: 'Eddy Hall'
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedAccountProfileAggregate)
    })
  })

  describe('Should work for SLOT', function () {
    it('INSERT Slot  => returns FALSE', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      const groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      const slotId = '3f5ae0c6-eb97-40e7-a8fe-0ca6543a7c27'

      const key = `/${this.mode}/group/${groupId}/slot/${slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'INSERT',
        keys,
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY slot (no update of subgroupId) => returns FALSE)', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)

      const groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      const slotId = '3f5ae0c6-eb97-40e7-a8fe-0ca6543a7c27'

      const key = `/${this.mode}/group/${groupId}/slot/${slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.false()

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)

      dynamodbItem.data.should.deepEqual(this.initialAccountAggregateData)
    })

    it('MODIFY Slot (update of subgroup) => returns undefined & update AccountAggregate)', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)
      const groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      const slotId = '3f5ae0c6-eb97-40e7-a8fe-0ca6543a7c27'

      const newSubGroupId = '7b28492a-4e64-4449-9ba5-eab823d352e2'

      const key = `/${this.mode}/group/${groupId}/slot/${slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.companyType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: newSubGroupId
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedAccountProfileAggregate = {
        ...this.initialAccountAggregateData,
        subGroupId: newSubGroupId
      }

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      dynamodbItem.data.should.deepEqual(expectedAccountProfileAggregate)
    })

    it('MODIFY Slot (delete account) => returns undefined & update AccountAggregate)', async function () {
      console.log(`AccountAggregate key => ${this.accountAggregateKey}`)
      const groupId = '96ee57d1-8c49-40c3-b742-d8344a55d77b'
      const slotId = '3f5ae0c6-eb97-40e7-a8fe-0ca6543a7c27'

      const key = `/${this.mode}/group/${groupId}/slot/${slotId}`
      const keys = { key, submitted: 'actual' }
      const streamRecord = {
        mode: this.mode,
        event: 'MODIFY',
        keys,
        oldItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            accountId: accountId,
            groupType: GroupType.teamType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
            subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
          }
        },
        newItem: {
          mode: this.mode,
          key,
          submitted: 'actual',
          submittedBy: itemCreatedBy,
          data: {
            createdAt: itemCreatedAt,
            createdBy: itemCreatedBy,
            structureVersion: 342,
            id: slotId,
            groupType: GroupType.companyType,
            groupId: groupId,
            paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c'
          }
        }
      }
      const result = await accountAggregateHandler(flowId, sot, streamRecord)

      should(result).be.undefined()

      const expectedAccountAggregate = {
        ...this.initialAccountAggregateData,
        totalDistance: 0,
        totalSteps: 0
      }
      delete expectedAccountAggregate.subgroupId
      delete expectedAccountAggregate.groupId

      await this.sleep(1000)

      const dynamodbItem = await getActual(this.mode, this.accountAggregateKey, true)
      dynamodbItem.should.not.be.undefined()

      console.log(`UPDATED AGGREGATE => ${util.inspect(dynamodbItem, false, null, true /* enable colors */)}`)

      should(dynamodbItem.sort_key_1).be.undefined()
      should(dynamodbItem.sort_key_2).be.undefined()
      should(dynamodbItem.sort_key_3).be.undefined()
      should(dynamodbItem.partition_key_1).be.undefined()
      should(dynamodbItem.partition_key_2).be.undefined()
      should(dynamodbItem.partition_key_3).be.undefined()
      dynamodbItem.data.should.deepEqual(expectedAccountAggregate)
    })
  })
})
