/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)

describe(testName, function () {
  it('Object is a function', function () {
    console.log('typeof Object:', typeof Object)
    Object.should.be.a.Function()
    console.log('Object instanceof Function:', Object instanceof Function)
    Object.should.be.an.instanceof(Function)
    console.log('Object instanceof Function:', Object instanceof Object)
    Object.should.be.an.instanceof(Object)
  })
  it('Object can be used as a constructor', function () {
    // eslint-disable-next-line
    const instance = new Object()
    console.log(instance)
    instance.should.be.an.Object()
  })
  it('any function can be used as a constructor', function () {
    let self
    function Constructor () {
      console.log('this:', this)
      self = this
      this.myProperty = 'my value'
      this.singASong = function () {
        console.log('lalalalalala')
      }
    }
    const instance = new Constructor()
    console.log('instance:', instance)
    console.log('self:', self)
    instance.should.equal(self)
    if (instance.singASong && typeof instance.singASong === 'function') {
      console.log('It can sing a song')
      instance.singASong()
    }
  })
  it('Object is a constructor, so it has a prototype property', function () {
    // eslint-disable-next-line
    Object.hasOwnProperty('prototype')
    console.log('Object.prototype:', Object.prototype)
  })
  it('An object created with the Object constructor has Object.prototype as prototype', function () {
    // eslint-disable-next-line
    const instance = new Object()
    console.log('instance:', instance)
    console.log('instance.__proto__:', Object.getPrototypeOf(instance))

    console.log('instance === instance.__proto__?:', instance === Object.getPrototypeOf(instance))
    instance.should.not.equal(Object.getPrototypeOf(instance))

    console.log('Object.prototype === instance.__proto__!:', Object.prototype === Object.getPrototypeOf(instance))
    Object.getPrototypeOf(instance).should.equal(Object.prototype)
  })
  it('Constructors can be set up as a "class"', function () {
    function A (firstName, lastName) {
      this.firstName = firstName
      this.lastName = lastName
    }
    A.prototype.getName = function () {
      return `${this.firstName} ${this.lastName}`
    }
    const a = new A('Jeroen', 'Vermeulen')
    console.log('a.firstName:', a.firstName)
    console.log('a.lastName:', a.lastName)
    console.log('a.getName():', a.getName())
  })
  it('Constructors can be set up as an inheritance hierarchy', function () {
    function A (firstName, lastName) {
      this.firstName = firstName
      this.lastName = lastName
    }
    A.prototype.constructor = A
    A.prototype.getName = function () {
      return `${this.firstName} ${this.lastName}`
    }
    const a = new A('Jeroen', 'Vermeulen')
    console.log('a.firstName:', a.firstName)
    console.log('a.lastName:', a.lastName)
    console.log('a.getName():', a.getName())

    function B (firstName, lastName, location) {
      A.call(this, firstName, lastName)
      this.location = location
    }
    B.prototype = Object.create(A.prototype)
    B.prototype.constructor = B
    B.prototype.getName = function () {
      return `${A.prototype.getName.call(this)} who lives in ${this.location}`
    }
    const b = new B('Jeroen', 'Vermeulen', 'Antwerpen')
    console.log('b.firstName:', b.firstName)
    console.log('b.lastName:', b.lastName)
    console.log('b.getName():', b.getName())
    b.getName = function () {
      return 'I am anonymous'
    }
    console.log('b.getName():', b.getName())
    console.log('b is a B', b instanceof B)
    console.log('b is an A', b instanceof A)
  })
})
