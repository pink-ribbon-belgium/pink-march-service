/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */
const testName = require('../../_testName')(module)
const DynamodbFixture = require('../../_DynamodbFixture')
const UUID = require('../../../lib/ppwcode/UUID')
const GroupType = require('../../../lib/api/group/GroupType')
const AccountAggregate = require('../../../lib/server/business/aggregates/account/AccountAggregate')
const updateRanking = require('../../../lib/updateRanking/updateAccountAggregateRanking')
const getByIndex = require('../../../lib/server/aws/dynamodb/getByIndex')

const { v4: uuidv4 } = require('uuid')

const sot = '2020-05-08T16:46:00.000Z'
const flowId = '3ac40618-a2a1-4f3f-85f7-33a9fed53052'

const defaultTimeout = 15000

describe(testName, function () {
  this.timeout(defaultTimeout)
  beforeEach(async function () {
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`

    this.baseData = []

    let i
    for (i = 0; i < 100; i++) {
      this.baseData.push({ accountId: uuidv4(), steps: Math.round(Math.random() * 1000000), rank: 999999 })
    }

    const actualItems = this.baseData.map(d => {
      const accountAggregate = new AccountAggregate({
        mode: this.mode,
        dto: {
          createdAt: '2020-05-08T16:26:00.000Z',
          createdBy: 'sub-update-accountAggregate-ranking',
          structureVersion: 1,
          accountId: d.accountId,
          totalSteps: d.steps,
          totalDistance: Math.round(d.steps * 0.82),
          groupId: UUID.example,
          groupType: GroupType.companyType,
          subGroupId: UUID.example,
          name: 'some-kind-of-a-name',
          rank: d.rank,
          totalParticipants: 1,
          previousLevel: 1,
          acknowledged: true
        }
      })

      return {
        ...accountAggregate.toItem(),
        submitted: 'actual',
        sort_key_1: accountAggregate.totalSteps,
        sort_key_2: accountAggregate.totalSteps,
        sort_key_3: accountAggregate.totalSteps,
        flowId: flowId
      }
    })

    const deletedAccountAggregate = new AccountAggregate({
      mode: this.mode,
      dto: {
        createdAt: '2020-05-08T16:26:00.000Z',
        createdBy: 'sub-update-accountAggregate-ranking',
        structureVersion: 1,
        accountId: 'test-deleted-accountId',
        totalSteps: 0,
        totalDistance: 0,
        groupType: GroupType.companyType,
        name: 'some-kind-of-a-name',
        rank: 1,
        totalParticipants: 1,
        previousLevel: 1,
        acknowledged: true
      }
    })

    const deleteDbItem = {
      ...deletedAccountAggregate.toItem(),
      submitted: 'actual',
      flowId
    }
    delete deleteDbItem.partition_key_1
    delete deleteDbItem.partition_key_2
    delete deleteDbItem.partition_key_3

    actualItems.push(deleteDbItem)

    await Promise.all(actualItems.map(a => this.dynamodbFixture.putItem(a)))
    const retrieved = await this.dynamodbFixture.hasArrived()
    console.log('#saved Items :::::::::::::::::::', retrieved.length)

    updateRanking.contract.verifyPostconditions = true
  })

  afterEach(async function () {
    this.timeout(defaultTimeout)
    await this.dynamodbFixture.clean()
    updateRanking.contract.verifyPostconditions = false
  })

  it('it should update the ranking', async function () {
    await updateRanking(sot, this.mode, flowId)
    const index = {
      indexName: 'Index_1',
      partitionKey: 'partition_key_1',
      sortKey: 'sort_key_1'
    }

    const partitionKey = `/${this.mode}/accountAggregate`
    const result = await getByIndex(this.mode, partitionKey, index)

    let startRank = 1
    let totalsteps = 9999999999999999999999999999999999

    result.length.should.equal(100)
    result.forEach(r => {
      console.log(`${r.data.rank} => ${r.data.totalSteps}`)
      r.data.rank.should.be.equal(startRank++)
      r.data.totalSteps.should.be.lessThanOrEqual(totalsteps)
      r.data.totalParticipants.should.be.equal(this.baseData.length)
      totalsteps = r.data.totalSteps
    })
  })
})
