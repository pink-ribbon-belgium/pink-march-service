/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const DynamodbFixture = require('../../_DynamodbFixture')
const GroupType = require('../../../lib/api/group/GroupType')
const CompanyAggregate = require('../../../lib/server/business/aggregates/company/CompanyAggregate')
const Slot = require('../../../lib/server/business/slot/Slot')
const updateCompanyRanking = require('../../../lib/updateRanking/updateCompanyAggregateRanking')
const getByIndex = require('../../../lib/server/aws/dynamodb/getByIndex')
const { v4: uuidv4 } = require('uuid')

const sot = '2020-06-12T18:33:00.000Z'
const sub = 'sub-update-companyAggregate'
const flowId = '5b93d037-cb5e-4906-8495-0e8b8ea6a062'

describe(testName, function () {
  beforeEach(async function () {
    this.timeout(15000)

    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.baseData = [
      {
        companyId: uuidv4(),
        steps: Math.round(Math.random() * 1000000),
        numberOfSlots: 0,
        rank: 999999
      }
    ]
    let i

    this.numberOfAggregatesForTest = 20

    for (i = 0; i < this.numberOfAggregatesForTest; i++) {
      this.baseData.push({
        companyId: uuidv4(),
        steps: Math.round(Math.random() * 1000000),
        numberOfSlots: Math.round(Math.random() * 10) + 2,
        rank: 999999
      })
    }

    // 1) Create the teamAggregates
    const actualItems = this.baseData.map(d => {
      const companyAggregate = new CompanyAggregate({
        mode: this.mode,
        dto: {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          companyId: d.companyId,
          totalSteps: d.steps,
          totalDistance: Math.round(d.steps * 0.82),
          averageSteps: d.steps,
          averageDistance: Math.round(d.steps * 0.82),
          groupType: GroupType.companyType,
          name: 'Test name',
          totalCompanies: 30,
          rank: d.rank,
          hasLogo: false
        }
      })

      return {
        ...companyAggregate.toItem(),
        submitted: 'actual',
        sort_key_1: companyAggregate.averageSteps,
        sort_key_2: companyAggregate.averageSteps,
        flowId: flowId
      }
    })
    await Promise.all(actualItems.map(a => this.dynamodbFixture.putItem(a)))

    // 2) Create slots
    const slotsToCreate = []

    let accountIdCounter = 1
    this.baseData.forEach(data => {
      const paymentId = uuidv4()
      const accountId = `someAccountId_${accountIdCounter++}`

      for (let i = 0; i < data.numberOfSlots; i++) {
        const slotData = {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 2,
          id: uuidv4(),
          groupType: GroupType.teamType,
          groupId: data.companyId,
          paymentId,
          accountId
        }

        const slot = new Slot({
          mode: this.mode,
          dto: {
            ...slotData
          },
          sot,
          sub
        })

        const dbSlot = slot.toItem()
        dbSlot.flowId = flowId
        dbSlot.submitted = 'actual'
        dbSlot.sort_key_A = 'actual'
        dbSlot.sort_key_B = 'actual'

        slotsToCreate.push(dbSlot)
      }
    })
    await Promise.all(slotsToCreate.map(a => this.dynamodbFixture.putItem(a)))

    updateCompanyRanking.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    await this.dynamodbFixture.clean()
    updateCompanyRanking.contract.verifyPostconditions = false
  })

  it('it should update the ranking', async function () {
    this.timeout(15000)

    await updateCompanyRanking(sot, this.mode, flowId)

    const index = {
      indexName: 'Index_1',
      partitionKey: 'partition_key_1',
      sortKey: 'sort_key_1'
    }

    const partitionKey = `/${this.mode}/companyAggregate`
    const result = await getByIndex(this.mode, partitionKey, index)

    console.log(result)
    console.log('==========================')

    let startRank = 1
    let averageStepsPreviousItem = 9999999999999999999999999999999999

    // Print log the result
    result.forEach(i => console.log(`${i.data.rank} => ${i.data.averageSteps} => ${i.sort_key_1} => ${i.sort_key_2}`))

    result.forEach(r => {
      // check if total is correct
      r.data.totalCompanies.should.be.equal(this.baseData.length)

      // Check if averages are set
      const nbAggregateMembers = this.baseData.filter(x => x.companyId === r.data.companyId)[0].numberOfSlots

      const expectedAverageSteps = Math.round(r.data.totalSteps / nbAggregateMembers)
      r.data.averageSteps.should.be.lessThanOrEqual(expectedAverageSteps)
      r.sort_key_1 = expectedAverageSteps
      r.sort_key_2 = expectedAverageSteps
      r.data.averageDistance.should.be.equal(
        nbAggregateMembers ? Math.round(r.data.totalDistance / nbAggregateMembers) : 0
      )

      // Check if results are sorted by averageSteps
      r.data.rank.should.be.equal(startRank++)
      r.data.averageSteps.should.be.lessThanOrEqual(averageStepsPreviousItem)

      averageStepsPreviousItem = r.data.averageSteps
    })
  })
})
