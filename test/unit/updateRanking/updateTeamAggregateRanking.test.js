/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const DynamodbFixture = require('../../_DynamodbFixture')
const GroupType = require('../../../lib/api/group/GroupType')
const TeamAggregate = require('../../../lib/server/business/aggregates/team/TeamAggregate')
const Slot = require('../../../lib/server/business/slot/Slot')
const updateTeamRanking = require('../../../lib/updateRanking/updateTeamAggregateRanking')
const getByIndex = require('../../../lib/server/aws/dynamodb/getByIndex')
const { v4: uuidv4 } = require('uuid')

const sot = '2020-06-02T14:26:00.000Z'
const sub = 'updateTeamRankingTest'
const flowId = '63ef3b79-0bf8-4990-9e6e-b4ca400fabc7'

describe(testName, function () {
  beforeEach(async function () {
    this.timeout(15000)
    this.dynamodbFixture = new DynamodbFixture()
    this.mode = `automated-test-${uuidv4()}`
    this.baseData = [
      {
        teamAggregateId: uuidv4(),
        steps: Math.round(Math.random() * 1000000),
        rank: 999999,
        numberOfSlots: 0,
        companyId: undefined
      },
      {
        teamAggregateId: uuidv4(),
        steps: Math.round(Math.random() * 1000000),
        rank: 999999,
        numberOfSlots: 0,
        companyId: uuidv4()
      }
    ]
    let i

    this.numberOfAggregatesForTest = 20

    for (i = 0; i < this.numberOfAggregatesForTest; i++) {
      this.baseData.push({
        teamAggregateId: uuidv4(),
        steps: Math.round(Math.random() * 1000000),
        rank: 999999,
        numberOfSlots: Math.round(Math.random() * 10) + 2,
        companyId: i % 2 === 0 ? undefined : uuidv4()
      })
    }

    // 1) Create the teamAggregates
    const actualItems = this.baseData.map(d => {
      const teamAggregate = new TeamAggregate({
        mode: this.mode,
        dto: {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          teamAggregateId: d.teamAggregateId,
          totalSteps: d.steps,
          totalDistance: Math.round(d.steps * 0.82),
          averageSteps: 0,
          averageDistance: 0,
          groupType: d.companyId ? GroupType.companyType : GroupType.teamType,
          companyId: d.companyId,
          name: 'Test name',
          totalTeams: 30,
          rank: d.rank,
          hasLogo: false
        }
      })

      return {
        ...teamAggregate.toItem(),
        submitted: 'actual',
        sort_key_1: teamAggregate.averageSteps,
        sort_key_2: teamAggregate.averageSteps,
        flowId: flowId
      }
    })
    await Promise.all(actualItems.map(a => this.dynamodbFixture.putItem(a)))

    // 2) Create slots
    const slotsToCreate = []

    let accountIdCounter = 1
    this.baseData.forEach(data => {
      const paymentId = uuidv4()
      const accountId = `someAccountId_${accountIdCounter++}`

      for (let i = 0; i < data.numberOfSlots; i++) {
        const slotData = {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 2,
          id: uuidv4(),
          groupType: GroupType.teamType,
          groupId: data.companyId ? data.companyId : data.teamAggregateId,
          subgroupId: data.companyId ? data.teamAggregateId : undefined,
          paymentId,
          accountId
        }

        const slot = new Slot({
          mode: this.mode,
          dto: {
            ...slotData
          },
          sot,
          sub
        })

        const dbSlot = slot.toItem()
        dbSlot.flowId = flowId
        dbSlot.submitted = 'actual'
        dbSlot.sort_key_A = 'actual'
        dbSlot.sort_key_B = 'actual'

        slotsToCreate.push(dbSlot)
      }
    })
    await Promise.all(slotsToCreate.map(a => this.dynamodbFixture.putItem(a)))

    updateTeamRanking.contract.verifyPostconditions = true
  })
  afterEach(async function () {
    this.timeout(15000)
    await this.dynamodbFixture.clean()
    updateTeamRanking.contract.verifyPostconditions = false
  })

  it('it should update the ranking', async function () {
    this.timeout(15000)

    await updateTeamRanking(sot, this.mode, flowId)

    const index = {
      indexName: 'Index_1',
      partitionKey: 'partition_key_1',
      sortKey: 'sort_key_1'
    }

    const partitionKey = `/${this.mode}/teamAggregate`
    const result = await getByIndex(this.mode, partitionKey, index)

    console.log(result)
    console.log('==========================')

    let startRank = 1
    let averageStepsPreviousItem = 9999999999999999999999999999999999

    // Print log the result
    result.forEach(i => console.log(`${i.data.rank} => ${i.data.averageSteps} => ${i.sort_key_1} => ${i.sort_key_2}`))

    result.forEach(r => {
      // check if totalTeams is correct
      r.data.totalTeams.should.be.equal(this.baseData.length)

      // Check if averages are set
      const nbAggregateMembers = this.baseData.filter(x => x.teamAggregateId === r.data.teamAggregateId)[0]
        .numberOfSlots

      const expectedAverageSteps = nbAggregateMembers ? Math.round(r.data.totalSteps / nbAggregateMembers) : 0
      r.data.averageSteps.should.be.equal(expectedAverageSteps)
      r.sort_key_1 = expectedAverageSteps
      r.sort_key_2 = expectedAverageSteps
      r.data.averageDistance.should.be.equal(
        nbAggregateMembers ? Math.round(r.data.totalDistance / nbAggregateMembers) : 0
      )
      // Check if results are sorted by averageSteps
      if (nbAggregateMembers) {
        r.data.rank.should.be.equal(startRank++)
      }
      r.data.averageSteps.should.be.lessThanOrEqual(averageStepsPreviousItem)

      averageStepsPreviousItem = r.data.averageSteps
    })
  })
})
