/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../../_testName')(module)
const Event = require('../../../../../lib/ppwcode/lambda/apigateway/Event')
const expectSeriousSchema = require('../../../../_expectSeriousSchema')

const methodFailures = [null, undefined, 1, true, '', 'HEAD', 'get']
const uriFailures = [null, undefined, 1, true, 'this is not an uri', 'https:://www.example.org/', 'account/identifier']
const stuff = [1, true, 'a string', Symbol('a symbol'), []]
const stuffWithNullAndObject = stuff.concat([null, {}])

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(
      Event.schema,
      stuffWithNullAndObject
        .concat([
          { ...Event.example, headers: undefined },
          { ...Event.example, pathParameters: undefined },
          { ...Event.example, body: undefined }
        ])
        .concat(uriFailures.map(resource => ({ ...Event.example, resource })))
        .concat(uriFailures.map(path => ({ ...Event.example, path })))
        .concat(methodFailures.map(httpMethod => ({ ...Event.example, httpMethod })))
        .concat(stuffWithNullAndObject.map(headers => ({ ...Event.example, headers })))
        .concat(stuff.map(pathParameters => ({ ...Event.example, pathParameters }))),
      true
    )
  })
})
