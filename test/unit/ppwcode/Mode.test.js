/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const Mode = require('../../../lib/ppwcode/Mode')
const expectSeriousSchema = require('../../_expectSeriousSchema')

const invalidStuff = [
  null,
  1,
  -1,
  Math.PI,
  Number.NEGATIVE_INFINITY,
  Number.NaN,
  true,
  'a string',
  Symbol('a symbol'),
  [],
  {},
  'qa34',
  'qa-er',
  'acceptance34',
  'acceptance-as',
  'automated-test-34',
  'productionAndMore',
  'something#demo',
  'something#june2020'
]

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(Mode.schema, invalidStuff, false)
  })
  describe('#regExpString', function () {
    it('is a string', function () {
      Mode.regExpString.should.be.a.String()
      Mode.regExpString.should.not.startWith('^')
      Mode.regExpString.should.not.endWith('$')
    })
  })
})
