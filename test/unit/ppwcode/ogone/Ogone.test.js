/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../../_testName')(module)
const Ogone = require('../../../../lib/ppwcode/ogone/Ogone')
const config = require('config')
const uuidv4 = require('uuid').v4
const sinon = require('sinon')
const proxyquire = require('proxyquire')

describe(testName, function () {
  it('should create payment url', function () {
    const ogoneTestConfig = config.ogone.test
    const orderId = uuidv4()
    const url = Ogone.createPaymentUrl(
      {
        accepturl: 'https://app.pink-march.pink-ribbon-belgium.org/index.html?mode=demo#/register/payment-success',
        amount: 8,
        cancelurl: 'https://app.pink-march.pink-ribbon-belgium.org/index.html?mode=demo#/register/payment-success',
        currency: 'EUR',
        declineurl: 'https://app.pink-march.pink-ribbon-belgium.org/index.html?mode=demo#/register/payment-success',
        exceptionurl: 'https://app.pink-march.pink-ribbon-belgium.org/index.html?mode=demo#/register/payment-success',
        language: 'nl-BE',
        orderId,
        pspid: ogoneTestConfig.pspId,
        userid: ogoneTestConfig.username
      },
      ogoneTestConfig
    )

    url.should.be.ok()
    console.log(url)
  })

  describe('with proxy', function () {
    beforeEach(function () {
      this.fetchStub = sinon.stub()
      this.OgoneProxy = proxyquire('../../../../lib/ppwcode/ogone/Ogone', {
        'node-fetch': this.fetchStub
      })
    })

    it('should return status for a known existing payment', async function () {
      this.fetchStub.returns(
        new Promise(resolve =>
          resolve({
            text: () =>
              '<?xml version="1.0"?>\n' +
              '<ncresponse\n' +
              'orderID="539b4812-5acf-4b7f-b838-421e82ca56d4"\n' +
              'PAYID="3064653649"\n' +
              'PAYIDSUB=""\n' +
              'NCSTATUS="0"\n' +
              'NCERROR="0"\n' +
              'NCERRORPLUS="!"\n' +
              'ACCEPTANCE="test123"\n' +
              'STATUS="9"\n' +
              'IPCTY="BE"\n' +
              'CCCTY="99"\n' +
              'ECI="12"\n' +
              'CVCCheck="NO"\n' +
              'AAVCheck="NO"\n' +
              'VC="NO"\n' +
              'amount="8"\n' +
              'currency="EUR"\n' +
              'PM="CreditCard"\n' +
              'BRAND="VISA"\n' +
              'CARDNO="XXXXXXXXXXXX1111"\n' +
              'IP="213.224.31.90">\n' +
              '</ncresponse>'
          })
        )
      )
      const orderid = '539b4812-5acf-4b7f-b838-421e82ca56d4'

      const ogoneTestConfig = config.ogone.test

      const ogone = new this.OgoneProxy('test', {
        pspid: ogoneTestConfig.pspId,
        userid: ogoneTestConfig.username,
        pswd: ogoneTestConfig.password
      })

      const status = await ogone.getPayment({
        orderid
      })

      status.should.be.ok()
    })

    it('should throw an error with invalid xml', async function () {
      this.fetchStub.returns(
        new Promise(resolve =>
          resolve({
            text: () => 'Eric dit is geen xml'
          })
        )
      )
      const orderid = '539b4812-5acf-4b7f-b838-421e82ca56d4'

      const ogoneTestConfig = config.ogone.test

      const ogone = new this.OgoneProxy('test', {
        pspid: ogoneTestConfig.pspId,
        userid: ogoneTestConfig.username,
        pswd: ogoneTestConfig.password
      })

      await ogone
        .getPayment({
          orderid
        })
        .should.be.rejected()
    })

    it('should throw an error when @ property is not present', async function () {
      this.fetchStub.returns(
        new Promise(resolve =>
          resolve({
            text: () => '<?xml version="1.0"?>\n' + '<ncresponse>\n' + '</ncresponse>'
          })
        )
      )
      const orderid = '539b4812-5acf-4b7f-b838-421e82ca56d4'

      const ogoneTestConfig = config.ogone.test

      const ogone = new this.OgoneProxy('test', {
        pspid: ogoneTestConfig.pspId,
        userid: ogoneTestConfig.username,
        pswd: ogoneTestConfig.password
      })

      await ogone
        .getPayment({
          orderid
        })
        .should.be.rejectedWith(new Error('Unknown response: \n'))
    })

    it('should throw an OgoneError when there is an ncerror', async function () {
      this.fetchStub.returns(
        new Promise(resolve =>
          resolve({
            text: () => '<?xml version="1.0"?>\n' + '<ncresponse\n' + 'NCERROR="5">\n' + '</ncresponse>'
          })
        )
      )
      const orderid = '539b4812-5acf-4b7f-b838-421e82ca56d4'

      const ogoneTestConfig = config.ogone.test

      const ogone = new this.OgoneProxy('test', {
        pspid: ogoneTestConfig.pspId,
        userid: ogoneTestConfig.username,
        pswd: ogoneTestConfig.password
      })

      await ogone
        .getPayment({
          orderid
        })
        .should.be.rejectedWith({ name: 'OgoneError', message: 'NCError: 5', response: { ncerror: '5' } })
    })
  })
})
