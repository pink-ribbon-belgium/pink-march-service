/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../../_testName')(module)
const APIGateway = require('../../../common/APIGateway')
const expectSeriousSchema = require('../../_expectSeriousSchema')
const { clone } = require('@hapi/hoek')
const should = require('should')
const Joi = require('@hapi/joi')

const stuff = [null, 1, true, 'a string with #/: illegal characters', Symbol('a symbol')]

describe(testName, function () {
  describe('#schema', function () {
    expectSeriousSchema(
      APIGateway.schema,
      stuff
        .concat([[]])
        .concat([
          {
            ...clone(APIGateway.example),
            id: undefined
          },
          {
            ...clone(APIGateway.example),
            id: []
          },
          {
            ...clone(APIGateway.example),
            id: '123456789ab'
          },
          {
            ...clone(APIGateway.example),
            id: '123456789'
          },
          {
            ...clone(APIGateway.example),
            id: '123456789A'
          },
          {
            ...clone(APIGateway.example),
            id: '12345678 a'
          }
        ])
        .concat(
          stuff.map(s => ({
            ...clone(APIGateway.example),
            id: s
          }))
        )
        .concat([
          {
            ...clone(APIGateway.example),
            reservedStages: undefined
          }
        ])
        .concat(
          stuff.map(s => ({
            ...clone(APIGateway.example),
            reservedStages: s
          }))
        )
        .concat(
          stuff.map(s => ({
            ...clone(APIGateway.example),
            reservedStages: [s]
          }))
        )
        .concat([
          {
            ...clone(APIGateway.example),
            reservedStages: [undefined]
          },
          {
            ...clone(APIGateway.example),
            reservedStages: [[]]
          },
          {
            ...clone(APIGateway.example),
            reservedStages: ['0000']
          },
          {
            ...clone(APIGateway.example),
            reservedStages: ['00a00']
          },
          {
            ...clone(APIGateway.example),
            reservedStages: ['00002', '00002', '00001']
          },
          {
            ...clone(APIGateway.example),
            reservedStages: ['00009', '00008', '00007', '00006', '00005', '00004', '00003', '00002', '00001']
          }
        ]),
      false
    )
    describe('conversion', function () {
      it('sorts when requested', function () {
        const validation = APIGateway.schema.validate({
          ...clone(APIGateway.example),
          reservedStages: ['00001', '00002']
        })
        should(validation.error).not.be.ok()
        validation.value.reservedStages.forEach((stage, i) => {
          if (i !== 0) {
            validation.value.reservedStages[i].should.be.belowOrEqual(validation.value.reservedStages[i - 1])
          }
        })
      })
      it('passes when sorted, and it is asked not to convert', function () {
        const validation = APIGateway.schema.validate(
          {
            ...clone(APIGateway.example),
            reservedStages: ['00002', '00001']
          },
          { convert: false }
        )
        should(validation.error).not.be.ok()
        validation.value.reservedStages.forEach((stage, i) => {
          if (i !== 0) {
            validation.value.reservedStages[i].should.be.belowOrEqual(validation.value.reservedStages[i - 1])
          }
        })
      })
      it('errors when not sorted, and it is asked not to convert', function () {
        const validation = APIGateway.schema.validate(
          {
            ...clone(APIGateway.example),
            reservedStages: ['00001', '00002']
          },
          { convert: false }
        )
        should(validation.error).be.ok()
      })
    })
  })
  describe('uri', function () {
    const URI = Joi.string()
      .uri()
      .required()

    beforeEach(function () {
      APIGateway.uri.contract.verifyPostconditions = true
    })
    afterEach(function () {
      APIGateway.uri.contract.verifyPostconditions = false
    })
    it('works', function () {
      const result = APIGateway.uri('a-region', 'jfj3j3j39a', 'api-00000')
      result.should.be.a.String()
      should(URI.validate(result).error).not.be.ok()
    })
  })
})
