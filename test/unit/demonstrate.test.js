/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

/* eslint-env mocha */

const should = require('should')

describe('js', function () {
  it('Object is a function', function () {
    console.log(typeof Object)
  })
  it('Object has a prototype', function () {
    const ObjectPrototype = Object.prototype
    ObjectPrototype.constructor.should.equal(Object)
    console.log(ObjectPrototype)
    should(Object.getPrototypeOf(ObjectPrototype)).be.null()
  })
  it('works', function () {
    function PersistentObject (key) {
      console.log('PersistentObject called')
      this.key = key
    }

    PersistentObject.prototype = {}
    PersistentObject.prototype.constructor = PersistentObject

    PersistentObject.prototype.a = 0
    PersistentObject.prototype.key = undefined
    PersistentObject.prototype.toString = function () {
      return `key: ${this.key}`
    }

    PersistentObject.a = 'this is a "static" property'

    function Account (key, id) {
      PersistentObject.call(this, key)
      this.id = id
    }

    Account.prototype = new PersistentObject(undefined)
    Account.prototype.constructor = Account
    Account.prototype.toString = function () {
      const poString = PersistentObject.prototype.toString.call(this)
      return `${poString} - id: ${this.id}`
    }
    Account.prototype.toJSON = function () {
      return [1, 2, 3]
    }

    Account.prototype.b = 1
    Account.prototype.id = undefined

    const p = new Account('thaKey', 'thaId')
    console.log(p.id)
    console.log(p.b)
    console.log(p.key)
    console.log(p.a)
    console.log(p.toString())
    console.log(Object.prototype.toString.call(p))
    console.log(JSON.stringify(p))

    p.toJSON = function () {
      return 'peter'
    }
    console.log(JSON.stringify(p))
    delete p.toJSON
    console.log(JSON.stringify(p))

    Object.defineProperty(p, 'toJSON', {
      writeable: false,
      configurable: false,
      value: function () {
        return 'peter'
      }
    })
    console.log(JSON.stringify(p))
    delete p.toJSON
    console.log(JSON.stringify(p))
  })
})
