/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

/* eslint-env mocha */

const testName = require('../_testName')(module)
const proxyquire = require('proxyquire')
const sinon = require('sinon')
const responseExample = require('../../lib/ppwcode/lambda/apigateway/Response').example
const requestExample = require('../../lib/ppwcode/lambda/apigateway/Event').example
const resourceActionsMap = require('../../lib/server/resourceActionsMap')
const ResourceActionContract = require('../../lib/server/ResourceActionContract')
const RequestHeadersExample = require('../../lib/ppwcode/lambda/apigateway/EventHeaders').exampleWithAuthorization
const getDummyToken = require('../_dummyToken/create')
const Hoek = require('@hapi/hoek')
const publicSigningKeyGetter = require('../_publicSigningKeyGetter')
const { v4: uuidv4 } = require('uuid')
const customClaims = require('../../lib/server/authorization/customClaims')

describe(testName, function () {
  beforeEach(async function () {
    this.mode = `automated-test-${uuidv4()}`
    this.request = Hoek.clone(requestExample)
    const ra = `${this.request.httpMethod}:${this.request.path}`
    const token = await getDummyToken({ [customClaims.modeClaim]: this.mode, [customClaims.raasClaim]: [ra] })
    this.request.headers['x-mode'] = this.mode
    this.request.headers.authorization = `Bearer ${token}`
    this.sandbox = sinon.createSandbox()
    this.ra = `${this.request.httpMethod}:${this.request.resource}`
    this.response = {}

    this.oldRAMResource = resourceActionsMap[this.request.resource]
    this.raStub = sinon.stub()
    this.raStub.resolves(this.response)
    resourceActionsMap[this.request.resource] = {
      [this.request.httpMethod]: ResourceActionContract.implementation(this.raStub)
    }
    this.nominalHttpResultStub = this.sandbox.stub().returns(responseExample) // every call returns 200, and something
    this.service = proxyquire('../../lib/service', {
      './server/NominalHttpResult': this.nominalHttpResultStub
    })
    this.handler = this.service.handler
    this.handler.contract.verifyPostconditions = true

    /* NOTE: Dirty workaround to be able to communicate data from a test fixture to deep stub code. This is fragile.
             We know. Any better ideas? */
    publicSigningKeyGetter.dummyToken = true
  })

  afterEach(function () {
    publicSigningKeyGetter.dummyToken = false

    this.handler.contract.verifyPostconditions = false
    resourceActionsMap[this.request.resource] = this.oldRAMResource
    this.sandbox.restore()
  })

  it('works', async function () {
    const response = await this.handler(this.request, {})
    response.statusCode.should.equal(200)
    this.raStub.should.be.calledOnce()
    this.raStub.should.be.calledWithMatch(
      sinon.match.string,
      sinon.match.string,
      this.request.headers['x-mode'],
      this.request.headers['x-flow-id'],
      this.request.pathParameters,
      this.request.body
    )
    this.nominalHttpResultStub.should.be.calledOnce()
    this.nominalHttpResultStub.should.be.calledWithMatch(this.request.headers['x-flow-id'], this.response)
  })

  it('works for endpoint - statistics (no authorization)', async function () {
    const statisticsResource = '/I/statistics'
    const statistics = {
      totalParticipants: 5,
      daysUntil: 84,
      timesRoundTheWorld: 0.1
    }
    const myGetAuthorizedStub = this.sandbox.stub()
    const getStatisticsStub = this.sandbox.stub().resolves(statistics)
    const resourceActionsMapStub = {
      [statisticsResource]: { GET: getStatisticsStub }
    }
    this.service = proxyquire('../../lib/service', {
      './server/authorization/getAuthorizedSub': myGetAuthorizedStub,
      './server/resourceActionsMap': resourceActionsMapStub
    })
    this.handler = this.service.handler

    this.request.resource = statisticsResource
    this.request.pathParameters = null
    this.request.body = null
    delete this.request.headers.authorization

    const response = await this.handler(this.request, {})
    console.log(response)

    myGetAuthorizedStub.should.not.be.called()
    getStatisticsStub.should.be.called()
    response.should.not.be.null()
    response.statusCode.should.equal(200)
    response.body.should.equal(JSON.stringify(statistics))
  })

  it('works with a null body', async function () {
    const event = { ...this.request, body: null }
    const response = await this.handler(event, {})
    response.statusCode.should.equal(200)
    this.raStub.should.be.calledOnce()
    this.raStub.should.be.calledWithMatch(
      sinon.match.string,
      sinon.match.string,
      event.headers['x-mode'],
      event.headers['x-flow-id'],
      event.pathParameters,
      event.body
    )
    this.nominalHttpResultStub.should.be.calledOnce()
    this.nominalHttpResultStub.should.be.calledWithMatch(this.request.headers['x-flow-id'], this.response)
  })

  it('works with a null pathParameters', async function () {
    const event = { ...this.request, pathParameters: null }
    const response = await this.handler(event, {})
    response.statusCode.should.equal(200)
    this.raStub.should.be.calledOnce()
    this.raStub.should.be.calledWithMatch(
      sinon.match.string,
      sinon.match.string,
      event.headers['x-mode'],
      event.headers['x-flow-id'],
      undefined,
      event.body
    )
    this.nominalHttpResultStub.should.be.calledOnce()
    this.nominalHttpResultStub.should.be.calledWithMatch(this.request.headers['x-flow-id'], this.response)
  })

  it('should return not found for resource', async function () {
    const resource = '/notarealresource'
    const response = await this.handler({ ...this.request, resource }, {})
    console.log(response)
    response.statusCode.should.equal(404)
    JSON.parse(response.body).message.should.containEql(resource)
    this.nominalHttpResultStub.should.not.be.called()
  })

  it('should return method not allowed for action', async function () {
    const httpMethod = 'POST'
    const response = await this.handler({ ...this.request, httpMethod }, {})
    console.log(response)
    response.statusCode.should.equal(405)
    JSON.parse(response.body).message.should.containEql(this.request.path)
    JSON.parse(response.body).message.should.containEql(httpMethod)
    this.nominalHttpResultStub.should.not.be.called()
  })

  it('should parse body if it is provided', async function () {
    this.sandbox.spy(JSON, 'parse')
    await this.handler({ ...this.request, body: "{key: 'value' }" }, {})
    JSON.parse.should.be.called()
    this.nominalHttpResultStub.should.not.be.called()
  })

  it('should return forced error when requested', async function () {
    const forceErrorCode = 403
    const response = await this.handler(
      { ...this.request, headers: { ...RequestHeadersExample, 'x-force-error': forceErrorCode } },
      {}
    )
    response.statusCode.should.equal(forceErrorCode)
    this.nominalHttpResultStub.should.not.be.called()
  })

  describe('should return badRequest without ', function () {
    Object.keys(RequestHeadersExample).forEach(requiredHeader => {
      if (requiredHeader !== 'x-flow-id' && requiredHeader !== 'authorization') {
        it(requiredHeader, async function () {
          const response = await this.handler(
            { ...this.request, headers: { ...RequestHeadersExample, [requiredHeader]: undefined } },
            {}
          )

          response.statusCode.should.equal(400)
          JSON.parse(response.body).message.should.equal(`"headers.${requiredHeader}" is required`)
          this.nominalHttpResultStub.should.not.be.called()
        })
      }
    })
  })

  it('should return internalServerError when an Error is thrown', async function () {
    this.nominalHttpResultStub.throws()
    const response = await this.handler(this.request, {})
    response.statusCode.should.equal(500)
    this.nominalHttpResultStub.should.be.calledOnce()
  })
  it('should return a sensible result if there is no header', async function () {
    const promise = this.handler({ noHeaders: 'no headers in this example' }, {})
    const response = await promise
    response.statusCode.should.equal(400)
    response.headers.should.be.an.Object()
    response.headers['x-flow-id'].should.be.a.String()
    console.log(response)
    this.nominalHttpResultStub.should.not.be.called()
  })
  it('should return a sensible result if there is a header without a flowId', async function () {
    const promise = this.handler({ headers: { noFlowId: 'no flowId here' } }, {})
    const response = await promise
    response.statusCode.should.equal(400)
    response.headers.should.be.an.Object()
    response.headers['x-flow-id'].should.be.a.String()
    console.log(response)
    this.nominalHttpResultStub.should.not.be.called()
  })
  it('should work with capitalized required headers', async function () {
    const event = {
      ...this.request,
      headers: {
        ...this.request.headers,
        Authorization: this.request.headers.authorization
      }
    }
    delete event.headers.authorization
    const response = await this.handler(event, {})
    response.statusCode.should.equal(200)
    this.raStub.should.be.calledOnce()
    this.raStub.should.be.calledWithMatch(
      sinon.match.string,
      sinon.match.string,
      this.request.headers['x-mode'],
      this.request.headers['x-flow-id'],
      this.request.pathParameters,
      this.request.body
    )
    this.nominalHttpResultStub.should.be.calledOnce()
    this.nominalHttpResultStub.should.be.calledWithMatch(this.request.headers['x-flow-id'], this.response)
  })
  it('should return a 401 with a bad token', async function () {
    this.request.headers.authorization = this.request.headers.authorization + 'someBogus'
    const promise = this.handler(this.request, {})
    const response = await promise
    response.statusCode.should.equal(401)
    response.headers.should.be.an.Object()
    response.headers['x-flow-id'].should.be.a.String()
    console.log(response)
    this.nominalHttpResultStub.should.not.be.called()
  })
  it('should return a 403 without a good RAA', async function () {
    this.request.path = '/requesting/something/else'
    const promise = this.handler(this.request, {})
    const response = await promise
    response.statusCode.should.equal(403)
    response.headers.should.be.an.Object()
    response.headers['x-flow-id'].should.be.a.String()
    console.log(response)
    this.nominalHttpResultStub.should.not.be.called()
  })
})
