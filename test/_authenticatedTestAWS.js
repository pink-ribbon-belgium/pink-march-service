/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const authenticatedAWS = require('../common/authenticatedAWS')
const region = require('../claudia/service').lambda.region

// when used as devsecops human
const profile = 'pink-ribbon-belgium-dev'
const automatedTestRoleNameAndPath = 'automated-test/automated-test-pink-march-service'

async function authenticatedTestAWS () {
  const { AWS } = await authenticatedAWS(region, profile, automatedTestRoleNameAndPath)
  return AWS
}

module.exports = authenticatedTestAWS
