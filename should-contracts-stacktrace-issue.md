I saw this while debugging `<promise>.should.be.fulfilled()`, `<promise>.should.be.rejected()`, and
`<promise>.should.be.rejectedWith({property: aValue})`. When my `<promise>` rejects, the entire Mocha process hangs.

On `cjs/should.js`, line 1770, we call `e.message`. It is this call that hangs. This `e` is the error thrown by line
1767, `should(err).match(message)`.

The `message` getter is defined at line 79-88. With a breakpoint at line 80, visualizing `this` in the debugger of IDEA,
I already get a hang.

Debugging quits when I try to evaluate `if (!this._message)`.

line 1767, `should(err).match(message)` eventually reaches `match`, line 2609-2690. Eventually
`should(this.obj).have.property(key).which.match(value)` on line 2663-2665 is called. This leads to 'property', line
2124-2134, and 'properties`, line 2149-2229. By now we have 'missing property'.

Eventually, we get to 252, where `assert` throw a new `LightAssertionError`. In the constructor, line 169-183, the
original rejection is `options.actual`. At 174-180, `get message` is defined, exactly like at 79-88 for
`AssertionError`. There is a comment:

```
// a bit hacky way how to get error to do not have stack
```

`context.fail()` is called at line 356, which calls `assert` (line 233-256) with `false`. Now an `AssertionError` is
thrown at 254. The original rejection is `params.obj`. Now the `message` getter that gets us in trouble is defined at
line 79-88. The original rejection is both `this.obj`, and `this.actual`. There is no `this._message`.

`Error.captureStackTrace` exists, so we call `Error.captureStackTrace(this, this.stackStartFunction)` on line 92.

And now the IDEA debugger shows hanging stuff in the variables pane.

Note that Contracts overrides the stack trace and the message too.

It is unclear why that would affect the stack trace of this Error, however.

But note that `generateMessage()` calls `format(this.actual)`,

    function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

    var sformat = _interopDefault(require('should-format'));

    var config = {
      typeAdaptors: shouldTypeAdaptors.defaultTypeAdaptorStorage,

      getFormatter: function(opts) {
        return new sformat.Formatter(opts || config);
      }
    };

    function format(value, opts) {
      return config.getFormatter(opts).format(value);
    }

`should-format` is a separate package. It features

    function formatError(value) {
      return formatPlainObject.call(this, value, {
        prefix: value.name,
        additionalKeys: [['message', value.message]]
      });
    }

Which calls `value.message`.

`formatPlainObjectValue` does special things for a getter

Note that `formatError` is called at least twice! Is this just an infinite loop?

Eventually we get, after a timeout of Mocha (?) the 'correct' notification. Note that the 'self' in the
ExceptionCondition is the test fixture, which is circular.
