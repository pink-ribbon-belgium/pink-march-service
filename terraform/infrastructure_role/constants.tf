# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

locals {
  region                                         = "eu-west-1"
  profile                                        = "pink-ribbon-belgium-dev"
  repo-basename                                  = "pink-march-service"
  infrastructure_role-name                       = "${local.repo-basename}-infrastructure"
  ci-name                                        = local.repo-basename
  ci-path                                        = "/ci/"
  ci-arn-user                                    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${local.ci-path}${local.ci-name}"
  ci-arn-policy                                  = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.ci-path}${local.ci-name}"
  automated_test-name                            = "automated-test-${local.repo-basename}"
  automated_test-path                            = "/automated-test/"
  automated_test-arn-role                        = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role${local.automated_test-path}${local.automated_test-name}"
  automated_test-arn-policy                      = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.automated_test-path}${local.automated_test-name}"
  dynamodb_table-name_test                       = "${local.repo-basename}-test"
  dynamodb_table-arn_test                        = "arn:aws:dynamodb:${local.region}:${data.aws_caller_identity.current.account_id}:table/${local.dynamodb_table-name_test}"
  dynamodb_table-name_production                 = "${local.repo-basename}-production"
  dynamodb_table-arn_production                  = "arn:aws:dynamodb:${local.region}:${data.aws_caller_identity.current.account_id}:table/${local.dynamodb_table-name_production}"
  bucket_postfix                                 = "pink-ribbon-belgium.org"
  lambda_bucket-name                             = "lambda.${local.bucket_postfix}"
  lambda_bucket-arn                              = "arn:aws:s3:::${local.lambda_bucket-name}"
  lambda_deploy-name                             = "lambda-deploy-${local.repo-basename}"
  lambda_deploy-path                             = "/deploy/"
  lambda_deploy-arn-policy                       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.lambda_deploy-path}${local.lambda_deploy-name}"
  lambda_execution-name                          = "lambda-execution-${local.repo-basename}"
  lambda_execution-path                          = "/execution/"
  lambda_execution-arn-role                      = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role${local.lambda_execution-path}${local.lambda_execution-name}"
  lambda_execution-arn-policy                    = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.lambda_execution-path}${local.lambda_execution-name}"
  lambda-service-name                            = local.repo-basename
  lambda-service-arn                             = "arn:aws:lambda:${local.region}:${data.aws_caller_identity.current.account_id}:function:${local.lambda-service-name}"
  lambda-poll_activities-name                    = "${local.repo-basename}-pollActivities"
  lambda-poll_activities-arn                     = "arn:aws:lambda:${local.region}:${data.aws_caller_identity.current.account_id}:function:${local.lambda-poll_activities-name}"
  lambda-poll_activities-rules-arn               = "arn:aws:events:${local.region}:${data.aws_caller_identity.current.account_id}:rule/${local.lambda-poll_activities-name}-*" // add qualifier per rule
  lambda-update_rankings-name                    = "${local.repo-basename}-updateRankings"
  lambda-update_rankings-arn                     = "arn:aws:lambda:${local.region}:${data.aws_caller_identity.current.account_id}:function:${local.lambda-update_rankings-name}"
  lambda-update_rankings-rules-arn               = "arn:aws:events:${local.region}:${data.aws_caller_identity.current.account_id}:rule/${local.lambda-update_rankings-name}-*" // add qualifier per rule
  lambda-verify_non_processed_payments-name      = "${local.repo-basename}-verifyNonProcessedPayments"
  lambda-verify_non_processed_payments-arn       = "arn:aws:lambda:${local.region}:${data.aws_caller_identity.current.account_id}:function:${local.lambda-verify_non_processed_payments-name}"
  lambda-verify_non_processed_payments-rules-arn = "arn:aws:events:${local.region}:${data.aws_caller_identity.current.account_id}:rule/${local.lambda-verify_non_processed_payments-name}-*" // add qualifier per rule
  lambda-stream-name                             = "${local.repo-basename}-stream"
  lambda-stream-arn                              = "arn:aws:lambda:${local.region}:${data.aws_caller_identity.current.account_id}:function:${local.lambda-stream-name}"
  # Defined here for reasons of consistency, but not used: the ARN of a REST API cannot be determined before creation. It contains the `api-id`, give by AWS on creation.
  api-name              = local.repo-basename
  api_deploy-name       = "api-deploy-${local.repo-basename}"
  api_deploy-path       = "/deploy/"
  api_deploy-arn-policy = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy${local.api_deploy-path}${local.api_deploy-name}"
  cmk-alias-name        = "alias/${local.repo-basename}"
}

data "aws_caller_identity" "current" {}

module "actions" {
  source = "github.com/peopleware/terraform-ppwcode-modules//actions?ref=v%2F6%2F0%2F0"
}
