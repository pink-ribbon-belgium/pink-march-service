# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "role-policy" {
  statement {
    # manage CI user
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreateUser",
      "iam:UpdateUser",
      "iam:DeleteUser",
      "iam:TagUser",
      "iam:UntagUser",
      "iam:AttachUserPolicy",
      "iam:DetachUserPolicy",
      "iam:CreateAccessKey",
      "iam:UpdateAccessKey",
      "iam:DeleteAccessKey"
    ]
    resources = [
      local.ci-arn-user
    ]
  }
  statement {
    # manage policies
    effect = "Allow"
    actions = [
      "iam:Get*",
      "iam:List*",
      "iam:CreatePolicy",
      "iam:DeletePolicy",
      "iam:GetPolicyVersion",
      "iam:CreatePolicyVersion",
      "iam:DeletePolicyVersion",
      "iam:SetDefaultPolicyVersion",
    ]
    resources = [
      local.ci-arn-policy,
      local.automated_test-arn-policy,
      local.lambda_deploy-arn-policy,
      local.lambda_execution-arn-policy,
      local.api_deploy-arn-policy,
    ]
  }
  statement {
    # manage dynamodb table
    effect = "Allow"
    actions = concat(
      module.actions.I-dynamodb-tables-describe,
      module.actions.I-dynamodb-table-define,
      [
        # NOTE: stuff missing in actions at this time (will be added there, and then can ve removed here)
        "dynamodb:DescribeTable",
        "dynamodb:DescribeContinuousBackups",
        "dynamodb:DescribeContributorInsights",
        "dynamodb:DescribeGlobalTable",
        "dynamodb:DescribeGlobalTableSettings",
        "dynamodb:DescribeTableReplicaAutoScaling",
        "dynamodb:DescribeTimeToLive",
        "dynamodb:ListTagsOfResource",
        "dynamodb:TagResource",
        "dynamodb:UntagResource",
        "dynamodb:CreateTableReplica",
        "dynamodb:DeleteTableReplica",
        "dynamodb:CreateGlobalTable",
        "dynamodb:UpdateGlobalTable",
        "dynamodb:UpdateGlobalTableSettings",
        "dynamodb:CreateBackup",
        "dynamodb:RestoreTableFromBackup",
        "dynamodb:RestoreTableToPointInTime",
        "dynamodb:UpdateContinuousBackups",
        "dynamodb:UpdateContributorInsights",
        "dynamodb:UpdateTableReplicaAutoScaling",
        "dynamodb:UpdateTimeToLive"
      ]
    )
    resources = [
      local.dynamodb_table-arn_test,
      local.dynamodb_table-arn_production
    ]
  }
  statement {
    # manage automated test and execution role
    effect = "Allow"
    actions = [
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:DeleteRolePermissionsBoundary",
      "iam:DeleteRolePolicy",
      "iam:DetachRolePolicy",
      "iam:GetRole",
      "iam:GetRolePolicy",
      "iam:ListRolePolicies",
      "iam:ListRoleTags",
      "iam:ListAttachedRolePolicies",
      "iam:PutRolePermissionsBoundary",
      "iam:PutRolePolicy",
      "iam:TagRole",
      "iam:UntagRole",
      "iam:UpdateAssumeRolePolicy",
      "iam:UpdateRole",
      "iam:UpdateRoleDescription",
      "iam:UpdateRole",
    ]
    resources = [
      local.automated_test-arn-role,
      local.lambda_execution-arn-role
    ]
  }
  statement {
    # manage lambda bucket
    effect = "Allow"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:HeadBucket",
    ]

    /* NOTE: arn:aws:s3:::* is not accepted as resource for these actions */
    resources = ["*"]
  }
  statement {
    # manage lambda bucket
    effect = "Allow"
    actions = [
      "s3:Get*",
      "s3:List*",
      "s3:CreateBucket",
      "s3:DeleteBucket",
      "s3:DeleteBucketPolicy",
      "s3:PutBucketAcl",
      "s3:PutBucketPolicy",
      "s3:PutBucketTagging",
      "s3:PutBucketVersioning",
      "s3:PutLifecycleConfiguration",
    ]
    resources = [
      local.lambda_bucket-arn,
    ]
  }
  statement {
    # manage API Gateway
    # https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-create-and-manage-api.html
    # Note that the `api-id` is part of the ARN, but that the `api-id` is defined by AWS on creation. This means we
    # cannot formulate a least privilege policy that contains permissions to this particular API Gateway instance for
    # the infrastructure role. It can manage _all_ REST APIs.
    effect = "Allow"
    actions = [
      "apigateway:*",

    ]
    resources = [
      "*"
    ]
  }

  statement {
    # allow management of lambda event source mappings
    # Cannot create exact permissions, because the eventSourceMapping ARN contains a UUID, which is generated on
    # creation, and cannot be known beforehand.
    effect = "Allow"

    actions = [
      "lambda:CreateEventSourceMapping",
      "lambda:ListEventSourceMappings",
      "lambda:GetEventSourceMapping",
      "lambda:UpdateEventSourceMapping",
      "lambda:DeleteEventSourceMapping"
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"
    # manage lambda permissions
    actions = [
      "lambda:AddPermission",
      "lambda:RemovePermission",
      "lambda:GetPolicy"
    ]
    resources = [
      local.lambda-service-arn,
      local.lambda-poll_activities-arn,
      local.lambda-update_rankings-arn,
      local.lambda-verify_non_processed_payments-arn,
      local.lambda-stream-arn
    ]
  }

  # manage KMS CMK
  statement {
    effect = "Allow"

    actions = [
      "kms:Get*",
      "kms:List*",
    ]

    resources = [
      "*",
    ]
  }

  # Cannot create exact permissions, because the KMS CMK ARN "arn:aws:kms:AWS_region:AWS_account_ID:key/CMK_key_ID"
  # contains the key_id, which is generated by KMS on creation, and cannot be known beforehand.
  statement {
    effect = "Allow"
    actions = [
      "kms:CreateKey",
      "kms:DescribeKey",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:EnableKeyRotation",
      "kms:DisableKeyRotation",
      "kms:UpdateKeyDescription",
    ]

    resources = [
      "*",
    ]
  }

  # This is possible for an alias, because the alias ARN "arn:aws:kms:AWS_region:AWS_account_ID:alias/alias_name"
  # can be predicted.
  statement {
    effect = "Allow"

    actions = [
      "kms:CreateAlias",
      "kms:DeleteAlias",
      "kms:UpdateAlias",
    ]

    resources = [
      "arn:aws:kms:${local.region}:${data.aws_caller_identity.current.account_id}:key/*",
      "arn:aws:kms:${local.region}:${data.aws_caller_identity.current.account_id}:${local.cmk-alias-name}",
    ]
  }

  # allow management of Cloudwatch events
  # https://docs.aws.amazon.com/IAM/latest/UserGuide/list_amazoneventbridge.html#amazoneventbridge-rule
  statement {
    effect = "Allow"

    actions = [
      "events:ListRules",
      "events:ListRuleNamesByTarget",
      "events:DescribeRule",
    ]

    resources = [
      "*"
    ]
  }
  statement {
    # allow management of Cloudwatch events
    # https://docs.aws.amazon.com/IAM/latest/UserGuide/list_amazoneventbridge.html#amazoneventbridge-rule
    effect = "Allow"

    actions = [
      "events:PutRule",
      "events:EnableRule",
      "events:DisableRule",
      "events:DeleteRule",
      "events:ListTargetsByRule",
      "events:PutTargets",
      "events:RemoveTargets",
      "events:ListTagsForResource",
      "events:TagResource",
      "events:UntagResource"
    ]

    resources = [
      local.lambda-poll_activities-rules-arn,
      local.lambda-update_rankings-rules-arn,
      local.lambda-verify_non_processed_payments-rules-arn
    ]
  }

  # manage KMS CMK
  statement {
    effect = "Allow"

    actions = [
      "kms:Get*",
      "kms:List*",
    ]

    resources = [
      "*",
    ]
  }

  # Cannot create exact permissions, because the KMS CMK ARN "arn:aws:kms:AWS_region:AWS_account_ID:key/CMK_key_ID"
  # contains the key_id, which is generated by KMS on creation, and cannot be known beforehand.
  statement {
    effect = "Allow"
    actions = [
      "kms:CreateKey",
      "kms:DescribeKey",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:EnableKeyRotation",
      "kms:DisableKeyRotation",
      "kms:UpdateKeyDescription",
    ]

    resources = [
      "*",
    ]
  }
}

resource "aws_iam_policy" "role-policy" {
  name        = local.infrastructure_role-name
  policy      = data.aws_iam_policy_document.role-policy.json
  path        = "/devsecops/"
  description = "Can manage ${local.repo-basename} CI user and policies"
}
