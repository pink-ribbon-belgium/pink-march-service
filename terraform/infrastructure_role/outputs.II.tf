# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

output "II-infrastructure_role" {
  value = {
    path = aws_iam_role.pink-march-service-infrastructure.path
    name = aws_iam_role.pink-march-service-infrastructure.name
    arn  = aws_iam_role.pink-march-service-infrastructure.arn
  }
}

# both for ci user and policy
output "II-ci" {
  value = {
    name       = local.ci-name
    path       = local.ci-path
    arn-user   = local.ci-arn-user
    arn-policy = local.ci-arn-policy
  }
}

output "II-automated_test" {
  value = {
    name       = local.automated_test-name
    path       = local.automated_test-path
    arn-role   = local.automated_test-arn-role
    arn-policy = local.automated_test-arn-policy
  }
}

output "II-dynamodb_table-test" {
  value = {
    name = local.dynamodb_table-name_test
    arn  = local.dynamodb_table-arn_test
  }
}

output "II-dynamodb_table-production" {
  value = {
    name = local.dynamodb_table-name_production
    arn  = local.dynamodb_table-arn_production
  }
}

output "II-lambda_bucket" {
  value = {
    name = local.lambda_bucket-name
    arn  = local.lambda_bucket-arn
  }
}

output "II-lambda_execution" {
  value = {
    name       = local.lambda_execution-name
    path       = local.lambda_execution-path
    arn-role   = local.lambda_execution-arn-role
    arn-policy = local.lambda_execution-arn-policy
  }
}

output "II-lambda" {
  # obsolete, replaced by II-lambda-service
  value = {
    name = local.lambda-service-name
    arn  = local.lambda-service-arn
  }
}

output "II-lambda-service" {
  value = {
    name = local.lambda-service-name
    arn  = local.lambda-service-arn
  }
}

output "II-lambda-poll_activities" {
  value = {
    name = local.lambda-poll_activities-name
    arn  = local.lambda-poll_activities-arn
  }
}

output "II-lambda-update_rankings" {
  value = {
    name = local.lambda-update_rankings-name
    arn  = local.lambda-update_rankings-arn
  }
}

output "II-lambda-verify_non_processed_payments" {
  value = {
    name = local.lambda-verify_non_processed_payments-name
    arn  = local.lambda-verify_non_processed_payments-arn
  }
}

output "II-lambda-stream" {
  value = {
    name = local.lambda-stream-name
    arn  = local.lambda-stream-arn
  }
}

output "II-lambda_deploy" {
  value = {
    name       = local.lambda_deploy-name
    path       = local.lambda_deploy-path
    arn-policy = local.lambda_deploy-arn-policy
  }
}

output "II-api" {
  value = {
    name = local.api-name
  }
}

output "II-api_deploy" {
  value = {
    name       = local.api_deploy-name
    path       = local.api_deploy-path
    arn-policy = local.api_deploy-arn-policy
  }
}

output "II-cmk-alias-name" {
  value = local.cmk-alias-name
}

