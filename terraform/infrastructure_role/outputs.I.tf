# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

output "I-infrastructure_role" {
  value = {
    path = aws_iam_role.pink-march-service-infrastructure.path
    name = aws_iam_role.pink-march-service-infrastructure.name
    arn  = aws_iam_role.pink-march-service-infrastructure.arn
  }
}

# both for ci user and policy
output "I-ci" {
  value = {
    name       = local.ci-name
    path       = local.ci-path
    arn-user   = local.ci-arn-user
    arn-policy = local.ci-arn-policy
  }
}

output "I-automated_test" {
  value = {
    name       = local.automated_test-name
    path       = local.automated_test-path
    arn-role   = local.automated_test-arn-role
    arn-policy = local.automated_test-arn-policy
  }
}

output "I-dynamodb_table" {
  # TODO legacy - remove
  value = {
    name = "obsolete - deleted"
    arn  = "obsolete - deleted"
  }
}

output "I-dynamodb_table-test" {
  value = {
    name = local.dynamodb_table-name_test
    arn  = local.dynamodb_table-arn_test
  }
}

output "I-dynamodb_table-production" {
  value = {
    name = local.dynamodb_table-name_production
    arn  = local.dynamodb_table-arn_production
  }
}

output "I-lambda_bucket" {
  value = {
    name = local.lambda_bucket-name
    arn  = local.lambda_bucket-arn
  }
}

output "I-lambda_execution" {
  value = {
    name       = local.lambda_execution-name
    path       = local.lambda_execution-path
    arn-role   = local.lambda_execution-arn-role
    arn-policy = local.lambda_execution-arn-policy
  }
}

output "I-lambda" {
  value = {
    name = local.lambda-service-name
    arn  = local.lambda-service-arn
  }
}

output "I-lambda_deploy" {
  value = {
    name       = local.lambda_deploy-name
    path       = local.lambda_deploy-path
    arn-policy = local.lambda_deploy-arn-policy
  }
}

output "I-api" {
  value = {
    name = local.api-name
  }
}

output "I-api_deploy" {
  value = {
    name       = local.api_deploy-name
    path       = local.api_deploy-path
    arn-policy = local.api_deploy-arn-policy
  }
}

