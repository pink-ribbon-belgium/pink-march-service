# pink-march-service Terraform Infrastructure Definition

The infrastructure necessary for the Pink March Service repository.

CI deploys every successful build to AWS Lambda.

Furthermore, roles and privileges are defined here for automated testing.

## Getting started

The infrastructure is defined using [Terraform]. See [Getting started with a Terraform configuration].

## Structure

The infrastructure is defined in [`main/`](main/README.md). It applies the infrastructure, assuming the role defined in
[`infrastructure_role/`](infrastructure_role).

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
