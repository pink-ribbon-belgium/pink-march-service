# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


locals {
  lambdaInvocationArn-service = "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["arn"]}/invocations"
}

resource "aws_api_gateway_rest_api" "api" {
  # keep name and description in sync with openapi
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-api["name"]
  description = "API of the Back-end Service of the Pink Ribbon Belgium Pink March 2020 application."

  endpoint_configuration {
    # we choose regional, because we want to give precedence to speedy interaction with dynamodb, and we use our own
    # Cloudfront, to avoid CORS
    # https://aws.amazon.com/premiumsupport/knowledge-center/api-gateway-cloudfront-distribution/
    types = ["REGIONAL"]
  }

  # https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-gzip-compression-decompression.html
  minimum_compression_size = 50 #bytes

  tags = local.tags
}

// This must be added explicitly to make testing via the console possible
resource "aws_lambda_permission" "api" {
  action        = "lambda:InvokeFunction"
  function_name = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["name"]
  # no qualifier: basic version
  principal = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
}

// we cannot give blanket permissions to all aliases: a * is not allowed as qualifier or in the function name
