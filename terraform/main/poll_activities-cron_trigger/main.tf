# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

locals {
  tags = {
    name = var.tags["name"]
    env  = var.mode
    repo = var.tags["repo"]
  }
  lambda-arn_suffix = var.lambda-build == "$LATEST" ? "" : ":${var.lambda-build}"
}

resource "aws_cloudwatch_event_rule" "poll_activities" {
  name        = "${var.lambda-name}-${var.mode}"
  description = "Trigger ${var.lambda-name} for mode ${var.mode}"

  schedule_expression = "rate(${var.rate})"

  tags = local.tags
}

resource "aws_cloudwatch_event_target" "poll_activities" {
  count = var.mode_start == 0 ? 1 : var.mode_count

  rule      = aws_cloudwatch_event_rule.poll_activities.name
  target_id = "${var.lambda-name}-${var.mode_start == 0 ? var.mode : format("${var.mode}-%05d", var.mode_start + count.index)}"
  arn       = "${var.lambda-arn}${local.lambda-arn_suffix}"
  input = jsonencode({
    mode                         = var.mode_start == 0 ? var.mode : format("${var.mode}-%05d", var.mode_start + count.index)
    nrOfTrackerConnectionsToPoll = var.nrOfTrackerConnectionsToPoll
  })
}

resource "aws_lambda_permission" "poll_activities" {
  action        = "lambda:InvokeFunction"
  function_name = var.lambda-name
  qualifier     = var.lambda-build == "$LATEST" ? null : var.lambda-build
  principal     = "events.amazonaws.com"

  source_arn = aws_cloudwatch_event_rule.poll_activities.arn
}

// we cannot give blanket permissions to all aliases: a * is not allowed as qualifier or in the function name
