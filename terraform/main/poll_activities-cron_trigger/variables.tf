# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

variable "mode" {
  type        = string
  description = "Either an exact mode, or the base name of a qualified mode. In the latter case, a `start` and `stop` is required."
}

variable "mode_start" {
  type        = number
  description = "pollActivities will work for all modes with name `mode-` followed by the numbers in the range `[mode_start, mode_start + mode_count]`, padded to 5 digits."
  default     = 0
}

variable "mode_count" {
  type        = number
  description = "MAX 5! - pollActivities will work for all modes with name `{mode}-` followed by the numbers in the range `[mode_start, mode_start + mode_count]`, padded to 5 digits."
  default     = 0
}

variable "nrOfTrackerConnectionsToPoll" {
  type        = number
  description = "How many tracker connections to poll in 1 execution"
}

variable "rate" {
  type        = string
  description = "rate expression: how often do you want to trigger pollActivities (e.g., '1 hour', '5 minutes', …)"
}

variable "lambda-name" {
  type        = string
  description = "name of the pollActivitities lambda function"
}

variable "lambda-arn" {
  type        = string
  description = "arn of the pollActivitities lambda function"
}

variable "lambda-build" {
  type        = string
  description = "build of the pollActivitities lambda function, as a string (e.g., 'build-12345')"
}

variable "tags" {
  type        = map(string)
  description = "tags for the resources"
}
