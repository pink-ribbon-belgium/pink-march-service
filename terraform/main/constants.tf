# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

locals {
  region  = "eu-west-1"
  profile = "pink-ribbon-belgium-dev"
  repo    = "pink-march-service"
  tags = {
    name = "Pink March Service"
    env  = "production"
    repo = "${local.repo}//terraform/main"
  }
}

data "aws_caller_identity" "current" {}

module "actions" {
  source = "github.com/peopleware/terraform-ppwcode-modules//actions?ref=v%2F6%2F0%2F0"
}
