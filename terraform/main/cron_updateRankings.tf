# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


locals {
  lambdaInvocationArn-update_rankings = "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["arn"]}/invocations"
  update_rankings-lamba_name          = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["name"]
  update_rankings-lamba_arn           = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["arn"]
}

// we cannot give blanket permissions to all aliases: a * is not allowed as qualifier or in the function name

//module "update_rankings-dev_experiment" {
//  source = "./update_rankings-cron_trigger"
//
//  mode         = "dev-experiment"
//  lambda-name  = local.update_rankings-lamba_name
//  lambda-arn   = local.update_rankings-lamba_arn
//  lambda-build = "$LATEST"
//  rate         = "15 minutes"
//  tags         = local.tags
//}

/*
module "update_rankings-acceptance_00057_00062" {
  source = "./update_rankings-cron_trigger"

  mode         = "acceptance"
  mode_start   = 77
  mode_count   = 5 # max 5
  lambda-name  = local.update_rankings-lamba_name
  lambda-arn   = local.update_rankings-lamba_arn
  lambda-build = "build-01467"
  rate         = "1 hour"
  tags         = local.tags
}
*/

/*module "update_rankings-demo" {
  source = "./update_rankings-cron_trigger"

  mode         = "demo"
  lambda-name  = local.update_rankings-lamba_name
  lambda-arn   = local.update_rankings-lamba_arnc
  lambda-build = "build-01467"
  rate         = "15 minutes"
  tags         = local.tags
}*/

/*module "update_rankings-june2020" {
  source = "./update_rankings-cron_trigger"

  mode         = "june2020"
  lambda-name  = local.update_rankings-lamba_name
  lambda-arn   = local.update_rankings-lamba_arn
  lambda-build = "build-01467"
  rate         = "15 minutes"
  tags         = local.tags
}*/


//module "update_rankings-production" {
//  source = "./update_rankings-cron_trigger"
//
//  mode         = "production"
//  lambda-name  = local.update_rankings-lamba_name
//  lambda-arn   = local.update_rankings-lamba_arn
//  lambda-build = "build-01467"
//  rate         = "30 minutes"
//  tags         = local.tags
//}
