# pink-march-service Terraform Infrastructure Definition - main

The infrastructure necessary for the Pink March Service repository.

CI deploys every successful build to AWS Lambda.

Roles and privileges are defined here for deployment.

## Getting started

The infrastructure is defined using [Terraform]. See [Getting started with a Terraform configuration].

You need to have [AWS credentials] set up in `~/.aws/credentials` for profile `pink-ribbon-belgium-dev`, which should be
able to assume the role defined in [`infrastructure_role/`](../infrastructure_role/README.md).

## Defining the λ

The λ is not defined or maintained with [Terraform]. See [`scripts/λ/`](../../scripts/λ/service/README.md).

[terraform]: https://peopleware.atlassian.net/wiki/x/CwAvBg
[getting started with a terraform configuration]: https://peopleware.atlassian.net/wiki/x/p4zhC
[aws credentials]: https://peopleware.atlassian.net/wiki/x/RoAWBg
[claudia `update`]: https://github.com/claudiajs/claudia/blob/master/docs/update.md
