# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "lambda_execution" {
  statement {
    # can test with real DynamoDB Table
    effect = "Allow"
    actions = [
      "dynamodb:BatchGetItem",
      "dynamodb:BatchWriteItem",
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem",
      "dynamodb:Query",
      "dynamodb:Scan",
      "dynamodb:UpdateItem",
    ]
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"]}/index/*",
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"]}/index/*",
    ]
  }

  # the best resource for logging permissions I can find:
  # https://docs.aws.amazon.com/apigateway/latest/developerguide/getting-started-lambda-non-proxy-integration.html
  statement {
    # can create CloudWatch log groups
    effect = "Allow"
    actions = [
      "logs:CreateLogGroup"
    ]
    resources = [
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
  statement {
    # can write to CloudWatch
    effect = "Allow"
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = [
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["name"]}:*",
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["name"]}:*",
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["name"]}:*",
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["name"]}:*",
      "arn:aws:logs:${local.region}:${data.aws_caller_identity.current.account_id}:log-group:/aws/lambda/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["name"]}:*"
    ]
  }

  statement {
    # can generate a data key, and decrypt
    effect = "Allow"

    actions = [
      "kms:GenerateDataKey",
      "kms:Decrypt",
    ]

    resources = [
      aws_kms_key.cmk.arn,
    ]
  }

  # TODO only needed for stream lambda, but too lazy to diversify now
  statement {
    effect = "Allow"

    actions = [
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:DescribeStream",
      "dynamodb:ListStreams"
    ]

    resources = [
      module.dynamodb-test.I-dynamodb_table["stream_arn"],
      module.dynamodb-production.I-dynamodb_table["stream_arn"]
    ]
  }
}

resource "aws_iam_policy" "lambda_execution" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-lambda_execution["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-lambda_execution["path"]
  policy      = data.aws_iam_policy_document.lambda_execution.json
  description = "Allow to work with items in DynamoDB table"
}
