# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


locals {
  lambdaInvocationArn-verify_non_processed_payments = "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["arn"]}/invocations"
  verify_non_processed_payments-lamba_name          = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["name"]
  verify_non_processed_payments-lamba_arn           = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["arn"]
}

// we cannot give blanket permissions to all aliases: a * is not allowed as qualifier or in the function name

//module "verify_non_processed_payments-dev_experiment" {
//  source = "./verify_non_processed_payments-cron_trigger"
//
//  mode         = "dev-experiment"
//  lambda-name  = local.verify_non_processed_payments-lamba_name
//  lambda-arn   = local.verify_non_processed_payments-lamba_arn
//  lambda-build = "$LATEST"
//  rate         = "1 hour"
//  tags         = local.tags
//}

//module "verify_non_processed_payments-demo" {
//  source = "./verify_non_processed_payments-cron_trigger"
//
//  mode         = "demo"
//  lambda-name  = local.verify_non_processed_payments-lamba_name
//  lambda-arn   = local.verify_non_processed_payments-lamba_arn
//  lambda-build = "build-01467"
//  rate         = "12 hours"
//  tags         = local.tags
//}

//module "verify_non_processed_payments-june2020" {
//  source = "./verify_non_processed_payments-cron_trigger"
//
//  mode         = "june2020"
//  lambda-name  = local.verify_non_processed_payments-lamba_name
//  lambda-arn   = local.verify_non_processed_payments-lamba_arn
//  lambda-build = "build-01467"
//  rate         = "12 hours"
//  tags         = local.tags
//}

//module "verify_non_processed_payments-production" {
//  source = "./verify_non_processed_payments-cron_trigger"
//
//  mode         = "production"
//  lambda-name  = local.verify_non_processed_payments-lamba_name
//  lambda-arn   = local.verify_non_processed_payments-lamba_arn
//  lambda-build = "build-01467"
//  rate         = "1 hour"
//  tags         = local.tags
//}

