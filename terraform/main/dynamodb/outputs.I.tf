# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

output "I-dynamodb_table" {
  value = {
    name             = aws_dynamodb_table.main.name
    arn              = aws_dynamodb_table.main.arn
    partition_key    = aws_dynamodb_table.main.hash_key
    sort_key         = aws_dynamodb_table.main.range_key
    encryption       = aws_dynamodb_table.main.server_side_encryption
    stream_arn       = aws_dynamodb_table.main.stream_arn
    stream_view_type = aws_dynamodb_table.main.stream_view_type
    stream_label     = aws_dynamodb_table.main.stream_label
  }
}
