# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# The DynamoDB partition key (`key`) is the key-part of the resource URI. The DynamoDB sort key is the `submittedAt`
# (sot) (`submitted`) of each resource, to the millisecond, as ISO-date string.
#
# This implies that a resource cannot be updated more than once each millisecond.
# A sequence number or UUID as unique identification would not have this limitation, but is far more difficult to
# set up. For a sequence number, we first have to lookup the existing registrations, and then calculate the successor.
# This is not an atomic action, so we would have to deal with optimistic locking too. A UUID does not have the order,
# and requires more work afterwards.
#
# The data of each item is schema-less in DynamoDB, but follows a schema defined by the `structureVersion` attribute.
#
# Data is not intended to be deleted. Ever. If we want to remove data from the table, first, an export of legacy
# data to S3 should be aranged, and then a TTL should be set up for all tables.
#
# We will only add new versions of records, never change an existing record. See
# https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/bp-sort-keys.html "Using Sort Keys for Version Control"

resource "aws_dynamodb_table" "main" {
  name      = var.name
  hash_key  = "key"
  range_key = "submitted"

  attribute {
    name = "key"
    type = "S"
  }

  attribute {
    name = "submitted"
    type = "S"
  }

  attribute {
    name = "partition_key_A"
    type = "S"
  }

  attribute {
    name = "sort_key_A"
    type = "S"
  }

  attribute {
    name = "partition_key_B"
    type = "S"
  }

  attribute {
    name = "sort_key_B"
    type = "S"
  }

  attribute {
    name = "partition_key_C"
    type = "S"
  }

  attribute {
    name = "sort_key_C"
    type = "S"
  }

  attribute {
    name = "partition_key_D"
    type = "S"
  }

  attribute {
    name = "sort_key_D"
    type = "S"
  }

  attribute {
    name = "partition_key_E"
    type = "S"
  }

  attribute {
    name = "sort_key_E"
    type = "S"
  }

  attribute {
    name = "partition_key_1"
    type = "S"
  }

  attribute {
    name = "sort_key_1"
    type = "N"
  }

  attribute {
    name = "partition_key_2"
    type = "S"
  }

  attribute {
    name = "sort_key_2"
    type = "N"
  }

  attribute {
    name = "partition_key_3"
    type = "S"
  }

  attribute {
    name = "sort_key_3"
    type = "N"
  }

  global_secondary_index {
    name            = "Index_A"
    hash_key        = "partition_key_A"
    range_key       = "sort_key_A"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_B"
    hash_key        = "partition_key_B"
    range_key       = "sort_key_B"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_C"
    hash_key        = "partition_key_C"
    range_key       = "sort_key_C"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_D"
    hash_key        = "partition_key_D"
    range_key       = "sort_key_D"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_E"
    hash_key        = "partition_key_E"
    range_key       = "sort_key_E"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_1"
    hash_key        = "partition_key_1"
    range_key       = "sort_key_1"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_2"
    hash_key        = "partition_key_2"
    range_key       = "sort_key_2"
    projection_type = "ALL"
  }

  global_secondary_index {
    name            = "Index_3"
    hash_key        = "partition_key_3"
    range_key       = "sort_key_3"
    projection_type = "ALL"
  }

  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  // encryption at rest using an AWS managed Customer Master Key by default

  billing_mode = "PAY_PER_REQUEST"

  tags = var.tags

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}
