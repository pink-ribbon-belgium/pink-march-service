# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "deploy_api" {
  statement {
    # manage API Gateway
    # https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-create-and-manage-api.html
    effect = "Allow"
    actions = [
      "apigateway:*",

    ]
    resources = [
      "arn:aws:apigateway:${local.region}::/restapis/${aws_api_gateway_rest_api.api.id}",
      "arn:aws:apigateway:${local.region}::/restapis/${aws_api_gateway_rest_api.api.id}/*",
      "arn:aws:apigateway:${local.region}::/tags/arn%3Aaws%3Aapigateway%3A${local.region}%3A%3A%2Frestapis%2F${aws_api_gateway_rest_api.api.id}%2F*"
    ]
  }
  statement {
    # allow reading CI user data (for CI itself)
    effect = "Allow"
    actions = [
      "iam:GetUser"
    ]
    resources = [
      aws_iam_user.ci.arn
    ]
  }
  statement {
    effect = "Allow"
    # manage lambda permissions
    actions = [
      "lambda:AddPermission"
    ]
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["arn"]
    ]
  }
}

resource "aws_iam_policy" "deploy_api" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-api_deploy["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-api_deploy["path"]
  policy      = data.aws_iam_policy_document.deploy_api.json
  description = "Allow to deploy api gateway"
}
