# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


locals {
  lambda-stream-build        = "build-01467"
  lambdaInvocationArn-stream = "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["arn"]}/invocations"
  stream-lamba_name          = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["name"]
  stream-lamba_arn           = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["arn"]

}

# The lambda function should never fail, but shit happens.
#
# If it fails, it fails for the entire collection of stream records. The AWS mechanism retries (at least once, because
# if we enter 0, it uses the default of 10000 retries). On retry, the collection is bisected, so when a collection
# fails, each half of the collection is retried. When a collection fails, it is probably because of some particular
# records in the collection. Other records might work. To be sure the ok-records _are_ processed, we need to be ensure
# that, by iteratively bisecting, the failing records are eventually offered as a singleton.
#
# Therefor we must specify
#   batch_size < 2^maximum_retry_attempts
# or
#   2ln batch_size < maximum_retry_attempts

resource "aws_lambda_event_source_mapping" "stream-test" {
  event_source_arn                   = module.dynamodb-test.I-dynamodb_table["stream_arn"]
  function_name                      = local.stream-lamba_name # latest
  batch_size                         = 2040                    # 2000 < 2^11 = 2048
  maximum_batching_window_in_seconds = 10                      # 1 minutes
  maximum_record_age_in_seconds      = 3600                    # if record isn't processed within maximum_record_age_in_seconds, record is skipped
  parallelization_factor             = 1
  maximum_retry_attempts             = 11 # 0 defaults to 10000! # 2040 < 2^11 = 2048
  bisect_batch_on_function_error     = true
  starting_position                  = "LATEST"
}

resource "aws_lambda_event_source_mapping" "stream-production" {
  event_source_arn = module.dynamodb-production.I-dynamodb_table["stream_arn"]
  function_name    = "${local.stream-lamba_name}:${local.lambda-stream-build}"

  batch_size                         = 2040 # 2000 < 2^11 = 2048
  maximum_batching_window_in_seconds = 30   # tweak as necessary
  maximum_record_age_in_seconds      = 3600 # if record isn't processed within maximum_record_age_in_seconds, record is skipped
  parallelization_factor             = 1
  maximum_retry_attempts             = 11 # 0 defaults to 10000! # 2040 < 2^11 = 2048
  bisect_batch_on_function_error     = true
  starting_position                  = "LATEST"
}
