# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "automated-test-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    condition {
      test = "StringEquals"
      values = [
        "devsecops"
      ]
      variable = "aws:PrincipalTag/canAssumeRole"
    }
  }
}

resource "aws_iam_role" "automated_test" {
  name               = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["name"]
  path               = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["path"]
  assume_role_policy = data.aws_iam_policy_document.automated-test-assume-role-policy.json
  tags               = local.tags
}

resource "aws_iam_role_policy_attachment" "automated_test" {
  policy_arn = aws_iam_policy.automated_test.arn
  role       = aws_iam_role.automated_test.name
}
