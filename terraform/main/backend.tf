# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

terraform {
  backend "s3" {
    bucket         = "tfstate.pink-ribbon-belgium.org"
    key            = "pink-march-service-main.tfstate"
    region         = "eu-west-1"
    profile        = "pink-ribbon-belgium-dev"
    encrypt        = true
    dynamodb_table = "tfstate-lock.pink-ribbon-belgium.org"
  }
}

data "terraform_remote_state" "infrastructure_role" {
  backend = "s3"
  config = {
    bucket  = "tfstate.pink-ribbon-belgium.org"
    key     = "pink-march-service-infrastructure_role.tfstate"
    region  = local.region
    profile = local.profile
  }
}
