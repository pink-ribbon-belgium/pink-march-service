# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


locals {
  lambdaInvocationArn-poll_activities = "arn:aws:apigateway:${local.region}:lambda:path/2015-03-31/functions/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["arn"]}/invocations"
  poll_activities-lamba_name          = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["name"]
  poll_activities-lamba_arn           = data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["arn"]
}

// we cannot give blanket permissions to all aliases: a * is not allowed as qualifier or in the function name

//module "poll_activities-dev_experiment" {
//  source = "./poll_activities-cron_trigger"
//
//  mode                         = "dev-experiment"
//  lambda-name                  = local.poll_activities-lamba_name
//  lambda-arn                   = local.poll_activities-lamba_arn
//  lambda-build                 = "$LATEST"
//  rate                         = "1 hour"
//  nrOfTrackerConnectionsToPoll = 10
//  tags                         = local.tags
//}

/*
module "poll_activities-acceptance_00057_00062" {
  source = "./poll_activities-cron_trigger"

  mode                         = "acceptance"
  mode_start                   = 77
  mode_count                   = 5 # max 5
  lambda-name                  = local.poll_activities-lamba_name
  lambda-arn                   = local.poll_activities-lamba_arn
  lambda-build                 = "build-01467"
  rate                         = "15 minutes"
  nrOfTrackerConnectionsToPoll = 10
  tags                         = local.tags
}
*/

/*module "poll_activities-demo" {
  source = "./poll_activities-cron_trigger"

  mode                         = "demo"
  lambda-name                  = local.poll_activities-lamba_name
  lambda-arn                   = local.poll_activities-lamba_arn
  lambda-build                 = "build-01467"
  rate                         = "10 minutes"
  nrOfTrackerConnectionsToPoll = 10
  tags                         = local.tags
}*/

/*module "poll_activities-june2020" {
  source = "./poll_activities-cron_trigger"

  mode                         = "june2020"
  lambda-name                  = local.poll_activities-lamba_name
  lambda-arn                   = local.poll_activities-lamba_arn
  lambda-build                 = "build-01467"
  rate                         = "15 minutes"
  nrOfTrackerConnectionsToPoll = 10
  tags                         = local.tags
}*/

//module "poll_activities-production" {
//  source = "./poll_activities-cron_trigger"
//
//  mode                         = "production"
//  lambda-name                  = local.poll_activities-lamba_name
//  lambda-arn                   = local.poll_activities-lamba_arn
//  lambda-build                 = "build-01467"
//  rate                         = "2 minutes"
//  nrOfTrackerConnectionsToPoll = 200
//  tags                         = local.tags
//}

