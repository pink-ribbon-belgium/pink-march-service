# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "automated_test" {
  statement {
    # can test with real DynamoDB Table
    effect  = "Allow"
    actions = module.actions.I-dynamodb-items-readwrite
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["arn"]}/index/*",
      data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"],
      "${data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["arn"]}/index/*",
    ]
  }
  statement {
    # can invoke the lambda
    effect = "Allow"
    actions = [
      "lambda:InvokeFunction"
    ]
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["arn"]
    ]
  }

  statement {
    # can generate a data key, and decrypt
    effect = "Allow"

    actions = [
      "kms:GenerateDataKey",
      "kms:Decrypt",
    ]

    resources = [
      aws_kms_key.cmk.arn,
    ]
  }
}

resource "aws_iam_policy" "automated_test" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-automated_test["path"]
  policy      = data.aws_iam_policy_document.automated_test.json
  description = "Allow to test with DynamoDB table"
}
