# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# There is one table for the entire service. The DynamoDB partition key (`key`) is the key-part of the resource URI. The
# DynamoDB sort key is the `submittedAt` (sot) (`submitted`) of each resource, to the millisecond, as ISO-date string.
#
# This implies that a resource cannot be updated more than once each millisecond.
# A sequence number or UUID as unique identification would not have this limitation, but is far more difficult to
# set up. For a sequence number, we first have to lookup the existing registrations, and then calculate the successor.
# This is not an atomic action, so we would have to deal with optimistic locking too. A UUID does not have the order,
# and requires more work afterwards.
#
# Each item also holds common data:
# - user (sub - for auditing - `createdby`)
# - flowId
# - structure version
#
# The data of each item is schema-less in DynamoDB, but follows a schema defined by the `version` attribute.
#
# Data is not intended to be deleted. Ever. If we want to remove data from the table, first, an export of legacy
# data to S3 should be aranged, and then a TTL should be set up for all tables.
#
# We will only add new versions of records, never change an existing record. See
# https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/bp-sort-keys.html "Using Sort Keys for Version Control"

module "dynamodb-test" {
  source = "./dynamodb"
  name   = data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-test["name"]
  tags   = merge(local.tags, { env = "test" })
}

module "dynamodb-production" {
  source = "./dynamodb"
  name   = data.terraform_remote_state.infrastructure_role.outputs.II-dynamodb_table-production["name"]
  tags   = merge(local.tags, { env = "production" })
}
