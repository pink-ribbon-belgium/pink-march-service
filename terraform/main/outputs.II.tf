# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

output "II-ci" {
  value = {
    name              = aws_iam_user.ci.name
    path              = aws_iam_user.ci.path
    arn               = aws_iam_user.ci.arn
    aws_access_key_id = aws_iam_access_key.ci.id
  }
}

output "II-ui_ci-aws_secret_access_key" {
  value     = aws_iam_access_key.ci.secret
  sensitive = true
}

output "II-dynamodb_table-test" {
  value = module.dynamodb-test.I-dynamodb_table
}

output "II-dynamodb_table-production" {
  value = module.dynamodb-production.I-dynamodb_table
}

output "II-automated_test" {
  value = {
    name = aws_iam_role.automated_test.name
    path = aws_iam_role.automated_test.path
    arn  = aws_iam_role.automated_test.arn
  }
}

output "II-lambda_bucket" {
  value = {
    id                          = aws_s3_bucket.lambda.id
    name                        = aws_s3_bucket.lambda.bucket
    arn                         = aws_s3_bucket.lambda.arn
    bucket_regional_domain_name = aws_s3_bucket.lambda.bucket_regional_domain_name
  }
}

output "II-lambdaInvocationArn" {
  # obsolete, replaced by II-lambdaInvocationArn-service
  value = local.lambdaInvocationArn-service
}

output "II-lambdaInvocationArn-service" {
  value = local.lambdaInvocationArn-service
}

output "II-lambdaInvocationArn-poll_activities" {
  value = local.lambdaInvocationArn-poll_activities
}

output "II-lambdaInvocationArn-update_rankings" {
  value = local.lambdaInvocationArn-update_rankings
}

output "II-lambdaInvocationArn-verify_non_processed_payments" {
  value = local.lambdaInvocationArn-verify_non_processed_payments
}

output "II-lambdaInvocationArn-stream" {
  value = local.lambdaInvocationArn-stream
}

output "II-api" {
  value = {
    name          = aws_api_gateway_rest_api.api.name,
    id            = aws_api_gateway_rest_api.api.id,
    arn           = "arn:aws:execute-api:${local.region}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}"
    execution_arn = aws_api_gateway_rest_api.api.execution_arn
    source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
  }
}

output "II-cmk" {
  value = {
    id    = aws_kms_key.cmk.key_id
    arn   = aws_kms_key.cmk.arn
    alias = aws_kms_alias.cmk.name
  }
}

