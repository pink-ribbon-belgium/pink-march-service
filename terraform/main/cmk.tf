resource "aws_kms_key" "cmk" {
  description         = "Customer Master Key for pink-march-service"
  enable_key_rotation = true
  tags                = local.tags
}

resource "aws_kms_alias" "cmk" {
  name          = data.terraform_remote_state.infrastructure_role.outputs.II-cmk-alias-name
  target_key_id = aws_kms_key.cmk.key_id
}
