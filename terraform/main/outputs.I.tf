# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

output "I-ci" {
  value = {
    name              = aws_iam_user.ci.name
    path              = aws_iam_user.ci.path
    arn               = aws_iam_user.ci.arn
    aws_access_key_id = aws_iam_access_key.ci.id
  }
}

output "I-ui_ci-aws_secret_access_key" {
  value     = aws_iam_access_key.ci.secret
  sensitive = true
}

output "I-dynamodb_table" {
  # TODO legacy - remove
  value = {
    name          = "obsolete - deleted"
    arn           = "obsolete - deleted"
    partition_key = "obsolete - deleted"
    sort_key      = "obsolete - deleted"
    encryption    = "obsolete - deleted"
  }
}

output "I-dynamodb_table-test" {
  value = module.dynamodb-test.I-dynamodb_table
}

output "I-dynamodb_table-production" {
  value = module.dynamodb-production.I-dynamodb_table
}

output "I-automated_test" {
  value = {
    name = aws_iam_role.automated_test.name
    path = aws_iam_role.automated_test.path
    arn  = aws_iam_role.automated_test.arn
  }
}

output "I-lambda_bucket" {
  value = {
    id                          = aws_s3_bucket.lambda.id
    name                        = aws_s3_bucket.lambda.bucket
    arn                         = aws_s3_bucket.lambda.arn
    bucket_regional_domain_name = aws_s3_bucket.lambda.bucket_regional_domain_name
  }
}

output "I-lambdaInvocationArn" {
  value = local.lambdaInvocationArn-service
}

output "I-api" {
  value = {
    name          = aws_api_gateway_rest_api.api.name,
    id            = aws_api_gateway_rest_api.api.id,
    arn           = "arn:aws:execute-api:${local.region}:${data.aws_caller_identity.current.account_id}:${aws_api_gateway_rest_api.api.id}"
    execution_arn = aws_api_gateway_rest_api.api.execution_arn
    source_arn    = "${aws_api_gateway_rest_api.api.execution_arn}/*/*/*"
  }
}
