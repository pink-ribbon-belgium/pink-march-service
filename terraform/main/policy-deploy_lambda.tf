# Copyright (C) 2020 PeopleWare NV
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

data "aws_iam_policy_document" "deploy_lambda" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      aws_s3_bucket.lambda.arn
    ]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
      "s3:PutObject"
    ]
    resources = [
      "${aws_s3_bucket.lambda.arn}/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["name"]}/*",
      "${aws_s3_bucket.lambda.arn}/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["name"]}/*",
      "${aws_s3_bucket.lambda.arn}/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["name"]}/*",
      "${aws_s3_bucket.lambda.arn}/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["name"]}/*",
      "${aws_s3_bucket.lambda.arn}/${data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["name"]}/*"
    ]
  }
  # Claudia doc says we need full access:
  # https://claudiajs.com/tutorials/installing.html
  # Configuring access credentials, Detailed info about credentials
  # We limit access however
  statement {
    effect = "Allow"
    actions = [
      "lambda:GetAccountSettings",
      "lambda:ListEventSourceMappings",
      "lambda:ListFunctions",
      "lambda:ListLayerVersions",
      "lambda:ListLayers",
    ]
    resources = ["*"]
  }
  statement {
    effect = "Allow"
    # no layer version or event source mapping privileges
    # no invoke function privilege
    # replication only needed for @Edge (https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-edge-permissions.html)
    # other unused: "lambda:AddPermission", "lambda:RemovePermission", "lambda:CreateFunction", "lambda:DeleteFunction", …
    actions = [
      "lambda:GetPolicy",
      "lambda:GetFunction",
      "lambda:UpdateFunctionCode",
      "lambda:GetFunctionConfiguration",
      "lambda:UpdateFunctionConfiguration",
      # function concurrency not yet supported by Claudia
      # "lambda:PutFunctionConcurrency",
      # "lambda:GetFunctionConcurrency", !! not in https://docs.aws.amazon.com/IAM/latest/UserGuide/list_awslambda.html, but in console!
      # "lambda:DeleteFunctionConcurrency",
      "lambda:PublishVersion",
      "lambda:ListVersionsByFunction",
      "lambda:CreateAlias",
      "lambda:ListAliases",
      "lambda:GetAlias",
      "lambda:UpdateAlias",
      "lambda:TagResource",
      "lambda:ListTags",
      "lambda:UntagResource",
    ]
    resources = [
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-service["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-poll_activities["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-update_rankings["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-verify_non_processed_payments["arn"],
      data.terraform_remote_state.infrastructure_role.outputs.II-lambda-stream["arn"]
    ]
  }
}

resource "aws_iam_policy" "deploy_lambda" {
  name        = data.terraform_remote_state.infrastructure_role.outputs.II-lambda_deploy["name"]
  path        = data.terraform_remote_state.infrastructure_role.outputs.II-lambda_deploy["path"]
  policy      = data.aws_iam_policy_document.deploy_lambda.json
  description = "Allow to deploy lambda"
}
