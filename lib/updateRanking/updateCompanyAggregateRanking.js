/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const { Promise: PromiseContract } = require('@toryt/contracts-iv')
const validateSchema = require('../_util/validateSchema')
const getByIndex = require('../server/aws/dynamodb/getByIndex')

const aggregateCalculations = require('../api/aggregates/calculation/AggregateCalculations')

const dynamodb = require('../../lib/server/aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')

const simpleLog = require('../_util/simpleLog')

const { schema: UUID } = require('./../ppwcode/UUID')
const { schema: PointInTime } = require('./../ppwcode/PointInTime')
const { schema: Mode } = require('./../ppwcode/Mode')

const requiredUUID = UUID.required().label('flowId')
const requiredPointInTime = PointInTime.required().label('sot')
const requiredMode = Mode.required().label('mode')

const updateCompanyAggregateRanking = new PromiseContract({
  pre: [
    sot => validateSchema(requiredPointInTime, sot),
    (sot, mode) => validateSchema(requiredMode, mode),
    (sot, mode, flowId) => validateSchema(requiredUUID, flowId)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function updateAccountAggregateRanking (sot, mode, flowId) {
  simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'START')
  try {
    const dynamodbInstance = await dynamodb()

    // 1) get all companyAggregates => index 1 - sorted by SortKey1

    const companyAggregateIndex = {
      indexName: 'Index_1',
      partitionKey: 'partition_key_1',
      sortKey: 'sort_key_1'
    }

    const companyAggregatePartitionKey = `/${mode}/companyAggregate`
    const companyAggregates = await getByIndex(mode, companyAggregatePartitionKey, companyAggregateIndex)

    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'AGGREGATES RETRIEVED', {
      mode: mode,
      aggregateCount: companyAggregates.length
    })

    // 2) Get members for team
    const memberIndex = {
      indexName: 'Index_B',
      partitionKey: 'partition_key_B',
      sortKey: 'sort_key_B'
    }

    const companyAggregateMembers = await Promise.all(
      companyAggregates.map(async agg => {
        let numberOfMembers

        try {
          const memberSlots = await getByIndex(
            mode,
            `/${mode}/group/${agg.data.companyId}/members`,
            memberIndex,
            'actual'
          )
          numberOfMembers = memberSlots.length
        } catch (err) {
          numberOfMembers = 0
        }

        return { key: agg.key, numberOfMembers: numberOfMembers }
      })
    )

    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'MEMBER INFO RETRIEVED', {
      mode: mode
    })

    // 3) Update averages
    companyAggregates.forEach(agg => {
      const numberOfMembers = companyAggregateMembers.filter(x => x.key === agg.key)[0].numberOfMembers
      const averages = aggregateCalculations.calculateAverage(
        numberOfMembers,
        agg.data.totalSteps,
        agg.data.totalDistance
      )

      agg.data.averageSteps = averages.averageSteps
      agg.data.averageDistance = averages.averageDistance
      agg.sort_key_1 = averages.averageSteps
      agg.sort_key_2 = averages.averageSteps
    })

    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'AGGREGATES AVERAGES UPDATED', {
      mode: mode
    })

    // 4) sort teamAggregates by averageSteps
    companyAggregates.sort((a, b) => (a.data.averageSteps < b.data.averageSteps ? 1 : -1))

    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'AGGREGATES SORTED', {
      mode: mode
    })

    // 5) set Rank & update only changed properties
    let rankNumber = 1
    await Promise.all(
      companyAggregates.map(a => {
        const newRank = rankNumber++

        const params = {
          TableName: dynamodbTableName(mode),
          Key: {
            key: a.key,
            submitted: 'actual'
          },
          UpdateExpression:
            'SET #data.#totalCompanies = :totalCompanies, #data.#rank = :rank, #data.#averageSteps = :averageSteps, #data.#averageDistance = :averageDistance, #sort_key_1 = :averageSteps, #sort_key_2 = :averageSteps',
          ExpressionAttributeNames: {
            '#data': 'data',
            '#totalCompanies': 'totalCompanies',
            '#rank': 'rank',
            '#averageSteps': 'averageSteps',
            '#averageDistance': 'averageDistance',
            '#sort_key_1': 'sort_key_1',
            '#sort_key_2': 'sort_key_2'
          },
          ExpressionAttributeValues: {
            ':totalCompanies': companyAggregates.length,
            ':rank': newRank,
            ':averageSteps': a.data.averageSteps,
            ':averageDistance': a.data.averageDistance
          },
          ReturnValues: 'UPDATED_NEW'
        }

        return dynamodbInstance.update(params).promise()
      })
    )
    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'NOMINAL END', { mode: mode })
  } catch (e) /* istanbul ignore next */ {
    simpleLog.info(module, 'updateCompanyAggregateRanking', flowId, 'FAILED', { mode: mode, error: e })
  }
})

module.exports = updateCompanyAggregateRanking
