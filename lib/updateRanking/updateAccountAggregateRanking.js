/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const { Promise: PromiseContract } = require('@toryt/contracts-iv')
const validateSchema = require('../_util/validateSchema')
const getByIndex = require('../server/aws/dynamodb/getByIndex')

const dynamodb = require('../../lib/server/aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../lib/server/aws/dynamodb/dynamodbTableName')

const simpleLog = require('../_util/simpleLog')

const { schema: UUID } = require('./../ppwcode/UUID')
const { schema: PointInTime } = require('./../ppwcode/PointInTime')
const { schema: Mode } = require('./../ppwcode/Mode')

const requiredUUID = UUID.required().label('flowId')
const requiredPointInTime = PointInTime.required().label('sot')
const requiredMode = Mode.required().label('mode')

const updateAccountAggregateRanking = new PromiseContract({
  pre: [
    sot => validateSchema(requiredPointInTime, sot),
    (sot, mode) => validateSchema(requiredMode, mode),
    (sot, mode, flowId) => validateSchema(requiredUUID, flowId)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function updateAccountAggregateRanking (sot, mode, flowId) {
  simpleLog.info(module, 'updateAccountAggregateRanking', flowId, 'START', {
    mode: mode
  })

  const dynamodbInstance = await dynamodb()

  try {
    // 1) get all accountAggregates => index A - sorted by SortKeyA

    const index = {
      indexName: 'Index_1',
      partitionKey: 'partition_key_1',
      sortKey: 'sort_key_1'
    }

    const partitionKey = `/${mode}/accountAggregate`
    const result = await getByIndex(mode, partitionKey, index)

    // 2) Update ranking & save again
    let rankNumber = 1

    await Promise.all(
      result.map(a => {
        const newRank = rankNumber++

        const params = {
          TableName: dynamodbTableName(mode),
          Key: {
            key: a.key,
            submitted: 'actual'
          },
          UpdateExpression: 'SET #data.#totalParticipants = :totalParticipants, #data.#rank = :rank',
          ExpressionAttributeNames: {
            '#data': 'data',
            '#totalParticipants': 'totalParticipants',
            '#rank': 'rank'
          },
          ExpressionAttributeValues: {
            ':totalParticipants': result.length,
            ':rank': newRank
          },
          ReturnValues: 'UPDATED_NEW'
        }

        return dynamodbInstance.update(params).promise()
      })
    )
    simpleLog.info(module, 'updateAccountAggregateRanking', flowId, 'NOMINAL END', { mode: mode })
  } catch (e) /* istanbul ignore next */ {
    simpleLog.error(module, 'updateAccountAggregateRanking', flowId, e, {
      mode: mode
    })
  }
})

module.exports = updateAccountAggregateRanking
