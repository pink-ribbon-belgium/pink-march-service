/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const util = require('util')

/* IDEA: this is a quick fix, while we wait for a better solution. No contracts, no tests. */

const propertyToMask = /[aA]uthorization|secret/

function masked (data, visited) {
  if (data && (typeof data === 'object' || Array.isArray(data)) && visited.includes(data)) {
    return '[Circular]'
  }
  visited.push(data)
  if (!data) {
    return data
  }
  if (Array.isArray(data)) {
    return data.map(e => masked(e, visited))
  }
  if (typeof data === 'object') {
    return Object.keys(data).reduce((acc, k) => {
      if (propertyToMask.test(k)) {
        acc[k] = '*****'
      } else {
        acc[k] = masked(data[k], visited)
      }
      return acc
    }, {})
  }
  return data
}

/* istanbul ignore next */
function errorLogging (callingModule, functionName, flowId, phase) {
  const message = {
    flowId,
    level: 'WARN',
    module: callingModule.filename,
    function: functionName,
    phase,
    errorDuringLogging: true
  }
  console.log(JSON.stringify(message))
}

/**
 * @param {NodeModule} callingModule
 * @param {string} functionName
 * @param {string} flowId
 * @param {string} phase
 * @param {object} details
 */
function info (callingModule, functionName, flowId, phase, details) {
  try {
    const message = {
      flowId,
      level: 'INFO',
      module: callingModule.filename,
      function: functionName,
      phase,
      details: masked(details, [])
    }
    console.log(JSON.stringify(message))
  } catch (ignore) /* istanbul ignore next */ {
    errorLogging(callingModule, functionName, flowId, phase)
  }
}

/**
 * @param {Module} callingModule
 * @param {string} functionName
 * @param {string} flowId
 * @param {string} phase
 * @param {object} details
 */
function warn (callingModule, functionName, flowId, phase, details) {
  try {
    const message = {
      flowId,
      level: 'WARN',
      module: callingModule.filename,
      function: functionName,
      phase,
      details: masked(details, [])
    }
    console.log(JSON.stringify(message))
  } catch (ignore) /* istanbul ignore next */ {
    errorLogging(callingModule, functionName, flowId, phase)
  }
}

/**
 * @param {Module} callingModule
 * @param {string} functionName
 * @param {string} flowId
 * @param {Error} err
 */
function error (callingModule, functionName, flowId, err) {
  try {
    const message = {
      flowId,
      level: 'ERROR',
      module: callingModule.filename,
      function: functionName,
      message: err && err.message,
      stack: err && err.stack,
      details: util.inspect(masked(err, []), { depth: 9, maxArrayLength: 5 })
    }
    console.log(JSON.stringify(message))
  } catch (ignore) /* istanbul ignore next */ {
    errorLogging(callingModule, functionName, flowId, 'ERROR')
  }
}

module.exports = { info, warn, error }
