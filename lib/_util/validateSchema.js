/*
 * Copyright 2020 PeopleWare n.v.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @typedef {object} Joi
 * @property {function} alternatives
 * @property {function} any
 * @property {function} array
 * @property {function} attempt
 * @property {function} date
 * @property {function} func
 * @property {function} number
 * @property {function} object
 * @property {function} reach
 * @property {function} ref
 * @property {function} string
 * @property {function} valid
 * @property {function} validate
 */

/**
 * @typedef {function} ContractFunction
 * @property {Contract} contract
 * @property {function} implementation
 * @property {object} [context]
 */

function validateSchema (schema, value, context) {
  const error = schema.validate(value, { convert: false, context }).error
  if (!error) {
    return true
  }
  console.error(error.annotate())
  return false
}

module.exports = validateSchema
