/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const AccountId = require('../../../lib/api/account/AccountId')
const UUID = require('../../ppwcode/UUID')
const Joi = require('@hapi/joi')
const Link = require('./../../ppwcode/lambda/Link')
const GroupType = require('./../../api/group/GroupType')
const StructureVersion = require('../../ppwcode/StructureVersion')

const exampleIn = {
  ...DTO.exampleIn,
  structureVersion: 2,
  id: UUID.example,
  groupId: '96ee57d1-8c49-40c3-b742-d8344a55d77b',
  paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
  subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
}

/** @type {SlotDTO} */
const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  structureVersion: 2,
  id: UUID.example,
  accountId: 'id-of-Eric',
  groupType: GroupType.teamType,
  groupId: '96ee57d1-8c49-40c3-b742-d8344a55d77b',
  paymentId: '8cf2795d-efdf-43ca-9d1c-346e458bca8c',
  subgroupId: '7b28492a-4e64-4449-9ba5-eab823d352e2'
}
const exampleDynamodbTeam = exampleDynamodb
const exampleDynamodbCompany = {
  ...exampleDynamodbTeam,
  groupType: GroupType.companyType
}

/** @type {SlotDTO} */
const exampleOut = {
  ...exampleDynamodb,
  links: {
    account: '/account/id-of-Eric',
    accountProfile: '/account/29486cc8-a728-4925-9bdb-e488337a93d6/publicProfile',
    payment: '/payment/0f0a3cd3-9e52-439e-b628-2cd8549024df',
    group: '/company/b924b8af-09c6-47e8-89d0-86b06cc9421a'
  }
}
const exampleOutTeam = exampleOut
const exampleOutCompany = { ...exampleOutTeam, groupType: GroupType.companyType }

const SlotDTO = DTO.baseSchema
  .keys({
    structureVersion: StructureVersion.schema.required().alter({
      input: schema => schema.min(1),
      dynamodb: schema => schema.min(2),
      output: schema => schema.min(2)
    }),
    id: UUID.schema.required(),
    accountId: AccountId.schema.optional(),
    groupType: GroupType.schema.alter({
      input: schema => schema.optional(),
      dynamodb: schema => schema.required(),
      output: schema => schema.required()
    }),
    groupId: UUID.schema.required(),
    paymentId: UUID.schema.required(),
    subgroupId: UUID.schema.optional(),
    links: Link.schema
      .keys({
        account: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        accountProfile: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        payment: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        group: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb,
  exampleDynamodbTeam,
  exampleDynamodbCompany,
  exampleOut,
  exampleOutTeam,
  exampleOutCompany,
  schema: SlotDTO
}
