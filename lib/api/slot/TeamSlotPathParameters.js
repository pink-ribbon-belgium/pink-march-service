/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../ppwcode/UUID')

const example = {
  teamId: '5c7b0213-922d-4d29-b8c3-a32a3e5baa27',
  slotId: '6191ef2c-cfe9-49f9-86e9-1f3a39f5cadd'
}

const TeamSlotPathParameters = Joi.object()
  .keys({
    teamId: UUID.schema.required(),
    slotId: UUID.schema.required()
  })
  .unknown(false)
  .example(example)

module.exports = {
  example: example,
  schema: TeamSlotPathParameters
}
