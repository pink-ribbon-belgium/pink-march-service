/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const UUID = require('../../ppwcode/UUID')
const PointInTime = require('../../ppwcode/PointInTime')
const Joi = require('@hapi/joi')
const VoucherCode = require('../voucher/VoucherCode')
const GroupType = require('../group/GroupType')
const PaymentStatus = require('../payment/PaymentStatus')
const Link = require('./../../ppwcode/lambda/Link')

const exampleIn = {
  ...DTO.exampleIn,
  paymentId: UUID.example,
  paymentInitiatedAt: '2020-02-02T15:00:39.212Z',
  groupId: 'e88a6e1a-87c5-42fc-a804-2211d6bed387',
  groupType: GroupType.companyType,
  numberOfSlots: 15,
  amount: 46.25,
  paymentStatus: PaymentStatus.AUTHORISED
}

/** @type {PaymentDTO} */
const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  paymentId: UUID.example,
  paymentInitiatedAt: '2020-02-02T15:00:39.212Z',
  groupId: 'e88a6e1a-87c5-42fc-a804-2211d6bed387',
  groupType: GroupType.teamType,
  voucherCode: 'PV2020',
  numberOfSlots: 15,
  amount: 46.25,
  paymentStatus: PaymentStatus.INITIALIZED
}

/** @type {PaymentDTO} */
const exampleOut = {
  ...exampleDynamodb,
  links: {
    versions: '/payment/572e7911-7c89-4726-aee8-84d2968b3f72/versions',
    group: '/group/572e7911-7c89-4726-aee8-84d2968b3f72',
    voucher: '/voucher/33dacfc9-afcc-4a51-ba4b-cb41e51f1a7f'
  }
}

const PaymentDTO = DTO.baseSchema
  .keys({
    paymentId: UUID.schema.required(),
    paymentInitiatedAt: PointInTime.schema.required(),
    groupId: UUID.schema.required(),
    groupType: Joi.string()
      .valid('Company', 'Team')
      .required(),
    voucherCode: VoucherCode.schema.optional().allow(null),
    numberOfSlots: Joi.number()
      .integer()
      .positive()
      .required(),
    amount: Joi.number()
      .positive()
      .allow(0)
      .required(),
    paymentStatus: PaymentStatus.schema.required(),
    links: Link.schema
      .keys({
        versions: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        group: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        voucher: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: PaymentDTO
}
