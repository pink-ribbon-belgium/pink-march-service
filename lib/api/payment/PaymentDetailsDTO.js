/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const DTO = require('../../ppwcode/lambda/DTO')
const UUID = require('../../ppwcode/UUID')
const VoucherCode = require('../voucher/VoucherCode')

const example = {
  ...DTO.exampleIn,
  paymentId: 'ec82e5fc-2619-4a83-a747-32d82093b0c1',
  groupType: 'Team',
  locale: 'nl-BE',
  numberOfSlots: 20,
  voucherCode: VoucherCode.example,
  redirectUrl: 'https://localhost:4200/index.html#/register/payment-success'
}

const PaymentDetailsDTO = DTO.baseSchema
  .keys({
    paymentId: UUID.schema.required(),
    groupType: Joi.string()
      .required()
      .valid('Team', 'Company'),
    locale: Joi.string()
      .pattern(/^[a-z]{2}-[A-Z]{2}?$/)
      .required(),
    numberOfSlots: Joi.number()
      .integer()
      .positive()
      .required(),
    voucherCode: VoucherCode.schema.allow(null),
    redirectUrl: Joi.string()
      .uri()
      .required()
  })
  .unknown(true)
  .example(example)

module.exports = {
  example,
  schema: PaymentDetailsDTO
}
