/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

/* MUDO payment status is not sufficiently designed. We know for sure that would the current code,
  'Initialized' is used in `initializePayment`, and that in `processPayment` the code received from Ogone
  (a string representation of a number) is used.
   'success' was used in a test. This is the subject of ROZEMARS19-164. */

// https://epayments-support.ingenico.com/en/get-started/payment-platform-user-guides/transaction-statuses/guide

const PaymentStatus = {
  INITIALIZED: 'Initialized',
  INVALID_OR_INCOMPLETE: '0',
  CANCELLED_BY_CUSTOMER: '1',
  AUTHORIZATION_REFUSED: '2',
  ORDER_STORED: '4',
  STORED_WAITING_EXTERNAL_RESULT: '40',
  WAITING_FOR_CLIENT_PAYMENT: '41',
  WAITING_AUTHENTICATION: '46',
  AUTHORISED: '5',
  AUTHORIZED_WAITING_EXTERNAL_RESULT: '50',
  AUTHORISATION_WAITING: '51',
  AUTHORISATION_NOT_KNOWN: '52',
  STANDBY: '55',
  OK_WITH_SCHEDULED_PAYMENTS: '56',
  NOT_OK_WITH_SCHEDULED_PAYMENTS: '57',
  AUTHORIZATION_TO_BE_REQUESTED_MANUALLY: '59',
  AUTHORISED_AND_CANCELLED_GENERAL: '6',
  AUTHOR_DELETION_WAITING: '61',
  AUTHOR_DELETION_UNCERTAIN: '62',
  AUTHOR_DELETION_REFUSED: '63',
  AUTHORISED_AND_CANCELLED: '64',
  PAYMENT_DELETED_GENERAL: '7',
  PAYMENT_DELETION_PENDING: '71',
  PAYMENT_DELETION_UNCERTAIN: '72',
  PAYMENT_DELETION_REFUSED: '73',
  PAYMENT_DELETED: '74',
  REFUND_GENERAL: '8',
  REFUND_PENDING: '81',
  REFUND_UNCERTAIN: '82',
  REFUND_REFUSED: '83',
  REFUND: '84',
  REFUND_HANDLED_BY_MERCHANT: '85',
  PAYMENT_REQUESTED: '9',
  PAYMENT_PROCESSING: '91',
  PAYMENT_UNCERTAIN: '92',
  PAYMENT_REFUSED: '93',
  REFUND_DECLINED_BY_THE_ACQUIRER: '94',
  PAYMENT_HANDLED_BY_MERCHANT: '95',
  REFUND_REVERSED: '96',
  BEING_PROCESSED: '99'
}

const values = Object.values(PaymentStatus)
PaymentStatus.schema = values.reduce(
  (acc, v) => acc.example(v),
  Joi.string()
    .valid(...values)
    .description('INITIALIZED: we sent a url to navigate to Ogone to the client; ')
)

PaymentStatus.values = values

module.exports = PaymentStatus
