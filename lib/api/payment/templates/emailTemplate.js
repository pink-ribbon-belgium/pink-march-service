/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Joi = require('@hapi/joi')

const example = {
  flow: 'some flow',
  fromname: 'Some Name',
  language: 'NL',
  subject: 'a subject for an email',
  body: '<!DOCTYPE html><html><body><p>This is the email body</p></body></html>'
}

const EmailTemplate = Joi.object()
  .keys({
    flow: Joi.string().required(),
    fromname: Joi.string().required(),
    language: Joi.string()
      .valid('NL', 'FR')
      .required(),
    subject: Joi.string().required(),
    body: Joi.string().required()
  })
  .unknown(true)
  .example(example)

module.exports = {
  example: example,
  schema: EmailTemplate
}
