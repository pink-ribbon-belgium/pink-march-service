/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

const example = {
  unitPrice: 8,
  slotsToAdd: 10,
  numberOfParticipants: 10,
  amountPaid: 0,
  subTotal: 80,
  discount: 8,
  total: 72,
  voucherIsValid: true
}

const CalculationResult = Joi.object()
  .keys({
    unitPrice: Joi.number()
      .precision(2)
      .positive()
      .required(),
    numberOfParticipants: Joi.number()
      .integer()
      .positive()
      .required(),
    slotsToAdd: Joi.number()
      .integer()
      .positive()
      .required(),
    amountPaid: Joi.number()
      .precision(2)
      .min(0)
      .required(),
    subTotal: Joi.number()
      .precision(2)
      .min(0)
      .required(),
    discount: Joi.number()
      .precision(2)
      .min(0)
      .required(),
    total: Joi.number()
      .precision(2)
      .min(0)
      .required(),
    voucherIsValid: Joi.bool().required()
  })
  .example(example)

module.exports = { schema: CalculationResult, example }
