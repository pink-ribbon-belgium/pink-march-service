/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const GroupDTO = require('../../../lib/api/group/GroupDTO')
const UUID = require('../../ppwcode/UUID')
const Joi = require('@hapi/joi')
const GroupType = require('../group/GroupType')

// noinspection SpellCheckingInspection
const exampleIn = {
  ...GroupDTO.exampleIn,
  groupType: GroupType.teamType
}

const exampleDynamodb = {
  ...GroupDTO.exampleDynamodb,
  groupType: GroupType.teamType
}

const exampleOut = {
  ...GroupDTO.exampleOut,
  groupType: GroupType.teamType,
  joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.teamType.toLowerCase()}/${
    GroupDTO.exampleIn.id
  }`,
  links: {
    publicProfile: `/${exampleDynamodb.groupType.toLowerCase()}/${exampleDynamodb.id}/publicProfile`,
    administrators: `/${this.groupType}/${exampleDynamodb.id}/administrators`,
    slots: `/${this.groupType}/${exampleDynamodb.id}/slots`,
    payments: `/${this.groupType}/${exampleDynamodb.id}/payments`
  }
}

/**
 * Represents a Team in the real world
 *
 * @typedef TeamDTO
 * @type {GroupDTO}
 * @property {string} code - unique code of this team.
 */

/** @type {ObjectSchema} */
const TeamDTO = GroupDTO.schema
  .keys({
    id: UUID.schema
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.required(),
        output: schema => schema.required()
      })
      .required(),
    groupType: Joi.string()
      .valid('Team')
      .required()
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: TeamDTO
}
