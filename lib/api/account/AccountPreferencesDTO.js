/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const DTO = require('../../ppwcode/lambda/DTO')
const AccountId = require('./AccountId')
const VerifiedEmail = require('./VerifiedEmail')
const StructureVersion = require('../../ppwcode/StructureVersion')
const KnownFromType = require('./KnownFromType')

const exampleIn = {
  ...DTO.exampleIn,
  language: 'nl',
  verifiedEmail: {
    email: 'rustigeEric@test.be',
    isVerified: false
  },
  structureVersion: 2,
  newsletter: false,
  knownFromType: KnownFromType.previousEdtionType
}

const exampleDynamoDBOut = {
  ...DTO.exampleDynamodb,
  accountId: 'preference_account-id',
  language: 'nl-BE',
  verifiedEmail: {
    email: 'rustigeEric@test.be',
    isVerified: false
  },
  structureVersion: 2,
  newsletter: false,
  knownFromType: KnownFromType.otherType,
  otherText: 'interwebs'
}

/**
 * No links for this DTO.
 *
 * `verifiedEmail` will _always_ have a value when this is saved via the API.
 * `verifiedEmail` will _always_ have a value when this is stored in DynamodDB, or when this is retrieved from DynamoDB.
 * `verifiedEmail` will _always_ have a value when this is returned over the network.
 *
 * `verifiedEmail` was added in structure version 2. `ensureVerifiedEmailOnAccountPreferences.js` is a script that
 * updates all existing items in DynamoDB to have a `verifiedEmail`. The email is copied from `Account` if none is
 * found. The structure version is updated to 2.
 */
const AccountPreferencesDTO = DTO.baseSchema
  .keys({
    structureVersion: StructureVersion.schema.valid(2).required(),
    accountId: AccountId.schema.alter({
      input: schema => schema.optional(),
      output: schema => schema.required(),
      dynamodb: schema => schema.required()
    }),
    language: Joi.string()
      .pattern(/^[a-z]{2}(-[A-Z]{2})?$/)
      .required(),
    verifiedEmail: VerifiedEmail.schema.required(),
    newsletter: Joi.bool().required(),
    knownFromType: KnownFromType.schema.required(),
    otherText: Joi.string().optional()
  })
  .example(exampleDynamoDBOut)
  .alter({ input: schema => schema.example(exampleIn) })

module.exports = {
  exampleIn,
  exampleDynamoDB: exampleDynamoDBOut,
  example: exampleDynamoDBOut,
  schema: AccountPreferencesDTO
}
