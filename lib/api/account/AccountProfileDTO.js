/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const DTO = require('../../ppwcode/lambda/DTO')
const AccountId = require('./AccountId')

const example = {
  accountId: 'id-OF-Eric',
  firstName: 'Eric',
  lastName: 'DoeRustig',
  gender: 'M',
  dateOfBirth: '1984-01-23T15:22:39.212Z',
  zip: '3600'
}

const exampleIn = {
  ...DTO.exampleIn,
  ...example
}

const exampleDynamoDb = {
  ...DTO.exampleDynamodb,
  ...example
}

const exampleOut = {
  ...DTO.example,
  ...example
}

const Gender = Joi.string()
  .length(1)
  .pattern(/^([MFX])$/)

const AccountProfileDTO = DTO.baseSchema
  .keys({
    accountId: AccountId.schema.required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    gender: Gender.required(),
    dateOfBirth: Joi.string()
      .isoDate()
      .required(),
    zip: Joi.string().required()
  })
  .unknown(true)
  .example(example)

module.exports = { exampleIn, exampleDynamoDb, example: exampleOut, schema: AccountProfileDTO, Gender }
