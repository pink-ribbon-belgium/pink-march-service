/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

/** @type {TrackerType} */
const fitbitType = 'fitbit'

/** @type {TrackerType} */
const polarType = 'polar'

/** @type {TrackerType} */
const garminType = 'garmin'

/** @type {TrackerType} */
const googleType = 'googlefit'

/** @type {TrackerType} */
const automatedTestType = 'automatedTest'

/** @type {TrackerType} */
const manualType = 'manual'

/**
 * Represents the type of Tracker connected
 *
 * @typedef TrackerType
 * @type {string}
 */

/** @type {ObjectSchema} */
const schema = Joi.string()
  .valid(fitbitType, polarType, garminType, googleType, manualType, automatedTestType)
  .example(fitbitType)
  .example(polarType)
  .example(garminType)
  .example(googleType)
  .example(manualType)
  .example(automatedTestType)

module.exports = {
  fitbitType,
  polarType,
  garminType,
  googleType,
  manualType,
  automatedTestType,
  values: [fitbitType, polarType, garminType, googleType, manualType, automatedTestType],
  schema
}
