/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const AccountId = require('./AccountId')
const Joi = require('@hapi/joi')
const Link = require('./../../ppwcode/lambda/Link')

/** @type {AccountDTO} */
const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  id: 'id-OF-THIS-account',
  sub: 'DHak45sEKK',
  email: 'email@example.com'
}

/** @type {AccountDTO} */
const exampleOut = {
  ...exampleDynamodb,
  links: {
    preferences: '/account/id-OF-THIS-account/preferences',
    publicProfile: '/account/id-OF-THIS-account/profile',
    slot: '/account/id-OF-THIS-account/slot',
    administratorOf: '/account/id-OF-THIS-account/administratorOf'
  }
}

/**
 * Represents an Account in the real world, which is the closest we can get to a user.
 *
 * A user can have multiple Accounts, but we should try to keep users from creating multiple accounts.
 * An account could be used by multiple users, but that is a big anti-pattern. We advise users not to share accounts,
 * and take no responsibility if they do.
 *
 * @typedef AccountDTO
 * @type {DTO}
 * @property {string} id - Opaque id of this account; generated in IdP. This id is anonymized.
 *                        input not needed, because put doesn't exist
 * @property {string} sub - Opaque id of this account; generated in IdP. This id may contain privacy sensitive
 *                          information (e.g., the social account used to create the account in the IdP). Do not use
 *                          where other accounts can see it.
 * @property {string} email - Email address of the account.
 */

/** @type {ObjectSchema} */
const AccountDTO = DTO.baseSchema
  .keys({
    id: AccountId.schema.required(),
    sub: Joi.string().required(),
    email: Joi.string()
      .email()
      .optional(),
    links: Link.schema
      .keys({
        preferences: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        publicProfile: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        slot: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        administratorOf: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = { exampleDynamodb, exampleOut, schema: AccountDTO }
