/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

/** @type {KnownFromType} */
const previousEdtionType = 'PREVIOUSEDITION'

/** @type {KnownFromType} */
const friendsType = 'FRIENDS'

/** @type {KnownFromType} */
const workType = 'WORK'

/** @type {KnownFromType} */
const socialType = 'SOCIAL'

/** @type {KnownFromType} */
const mailType = 'MAIL'

/** @type {KnownFromType} */
const magazineType = 'MAGAZINE'

/** @type {KnownFromType} */
const televisionType = 'TELEVISION'

/** @type {KnownFromType} */
const webType = 'WEB'

/** @type {KnownFromType} */
const walkingType = 'WALKING'

/** @type {KnownFromType} */
const walkingWebType = 'WALKINGWEB'

/** @type {KnownFromType} */
const otherType = 'OTHER'

/**
 * Represents the type of location where user learned about DRM
 *
 * @typedef KnownFromType
 * @type {string}
 */

/** @type {ObjectSchema} */
const schema = Joi.string()
  .valid(
    previousEdtionType,
    friendsType,
    workType,
    socialType,
    mailType,
    magazineType,
    televisionType,
    webType,
    walkingType,
    walkingWebType,
    otherType
  )
  .example(previousEdtionType)
  .example(friendsType)
  .example(workType)
  .example(socialType)
  .example(mailType)
  .example(magazineType)
  .example(televisionType)
  .example(webType)
  .example(walkingType)
  .example(walkingWebType)
  .example(otherType)

module.exports = {
  previousEdtionType,
  friendsType,
  workType,
  socialType,
  mailType,
  magazineType,
  televisionType,
  webType,
  walkingType,
  walkingWebType,
  otherType,
  values: [
    previousEdtionType,
    friendsType,
    workType,
    socialType,
    mailType,
    magazineType,
    televisionType,
    webType,
    walkingType,
    walkingWebType,
    otherType
  ],
  schema
}
