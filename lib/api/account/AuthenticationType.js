/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

/** @type {AuthenticationType} */
const auth0Type = {
  nl: 'e-mail + wachtwoord',
  fr: 'e-mail - mot de passe'
}

/** @type {AuthenticationType} */
const facebookType = {
  nl: 'Facebook',
  fr: 'Facebook'
}

/** @type {AuthenticationType} */
const googleType = {
  nl: 'Google',
  fr: 'Google'
}

/** @type {AuthenticationType} */
const windowsType = {
  nl: 'Microsoft',
  fr: 'Microsoft'
}

/**
 * Represents a Group in the real world
 *
 * @typedef AuthenticationType
 * @type {Object}
 */

/** @type {ObjectSchema} */
const AuthenticationType = Joi.object()
  .keys({
    nl: Joi.string().required(),
    fr: Joi.string().required()
  })
  .valid(auth0Type, facebookType, googleType, windowsType)
  .example(auth0Type)
  .example(facebookType)
  .example(googleType)
  .example(windowsType)

module.exports = {
  auth0Type,
  facebookType,
  googleType,
  windowsType,
  values: [auth0Type, facebookType, googleType, windowsType],
  schema: AuthenticationType
}
