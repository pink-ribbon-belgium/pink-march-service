/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const DTO = require('../../ppwcode/lambda/DTO')
const UUID = require('../../ppwcode/UUID')
const GroupType = require('./GroupType')
const Link = require('./../../ppwcode/lambda/Link')
const ShortLinkResult = require('../group/shortLink/shortLinkResult')

// noinspection SpellCheckingInspection
/** @type {GroupDTO} */
const exampleIn = {
  ...DTO.exampleIn,
  id: UUID.example,
  groupType: GroupType.teamType,
  linkId: ShortLinkResult.example.linkId,
  shortUrl: ShortLinkResult.example.shortLink
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  id: UUID.example,
  code: 'FKFG-KDKK-KSKK-LWSK',
  groupType: GroupType.companyType,
  linkId: ShortLinkResult.example.linkId,
  shortUrl: ShortLinkResult.example.shortLink
}

/** @type {GroupDTO} */
const exampleOut = {
  ...exampleDynamodb,
  joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.companyType.toLowerCase()}/${
    UUID.example
  }`,
  links: {
    publicProfile: `/${exampleDynamodb.groupType.toLowerCase()}/${exampleDynamodb.id}/publicProfile`,
    administrators: `/${this.groupType}/${exampleDynamodb.id}/administrators`,
    slots: `/${this.groupType}/${exampleDynamodb.id}/slots`,
    payments: `/${this.groupType}/${exampleDynamodb.id}/payments`
  }
}

/**
 * Represents a Group in the real world
 *
 * @typedef GroupDTO
 * @type {DTO}
 * @property {string} id - id of this group.
 * @property {string} code - code that allows {@link Account}s to become a member of this `Group`
 */

/** @type {ObjectSchema} */
const GroupDTO = DTO.baseSchema
  .keys({
    id: UUID.schema.required(),
    code: Joi.string()
      .pattern(/^[A-Z]{4}(-[A-Z]{4}){3}$/)
      .alter({
        dynamodb: schema => schema.required(),
        output: schema => schema.required(),
        input: schema => schema.optional()
      })
      .required(),
    groupType: Joi.string()
      .required()
      .valid('Team', 'Company'),
    joinLink: Joi.string()
      .uri()
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      }),
    linkId: UUID.schema.optional().allow(null),
    shortUrl: Joi.string()
      .uri()
      .optional()
      .allow(null),
    links: Link.schema
      .keys({
        publicProfile: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        slots: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        payments: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        administrators: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn),
    dynamodb: schema => schema.example(exampleDynamodb),
    output: schema => schema.example(exampleOut)
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: GroupDTO
}
