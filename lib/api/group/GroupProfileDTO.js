/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const DTO = require('../../ppwcode/lambda/DTO')
const UUID = require('../../ppwcode/UUID')
const Link = require('./../../ppwcode/lambda/Link')
const Logo = require('./../../ppwcode/Logo')

const exampleIn = {
  ...DTO.exampleIn,
  name: 'test-groupProfile',
  logo: Logo.example
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  id: UUID.example,
  name: 'test-groupProfile',
  logo: Logo.example
}

/** @type {GroupDTO} */
const exampleOut = {
  ...exampleDynamodb,
  links: {
    group: '/group/ac8ff1ec-8085-43ac-9494-f8d3a26f6da9'
  }
}

const GroupProfileDTO = DTO.baseSchema
  .keys({
    id: UUID.schema.alter({
      input: schema => schema.optional(),
      dynamodb: schema => schema.required(),
      output: schema => schema.required()
    }),
    name: Joi.string().required(),
    logo: Logo.schema,
    links: Link.schema
      .keys({
        group: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn),
    dynamodb: schema => schema.example(exampleDynamodb),
    output: schema => schema.example(exampleOut)
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: GroupProfileDTO
}
