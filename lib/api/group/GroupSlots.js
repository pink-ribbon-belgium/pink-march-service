/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

const example = {
  totalSlots: 8,
  takenSlots: 4,
  availableSlots: 4
}

const schema = Joi.object()
  .keys({
    totalSlots: Joi.number()
      .integer()
      .positive()
      .required(),
    takenSlots: Joi.number()
      .integer()
      .min(0)
      .required(),
    availableSlots: Joi.number()
      .integer()
      .min(0)
      .required()
  })
  .example(example)

module.exports = { schema, example }
