/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const Joi = require('@hapi/joi')
const AccountId = require('../account/AccountId')
const UUID = require('../../ppwcode/UUID')
const Link = require('../../ppwcode/lambda/Link')
const GroupType = require('./../../api/group/GroupType')
const StructureVersion = require('../../ppwcode/StructureVersion')

const exampleIn = {
  ...DTO.exampleIn,
  structureVersion: 2,
  accountId: 'account-id',
  groupId: '012d73d9-b3eb-4012-98fc-a3e0a691f97c',
  deleted: false
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  structureVersion: 2,
  accountId: 'account-id',
  groupType: GroupType.teamType,
  groupId: '012d73d9-b3eb-4012-98fc-a3e0a691f97c',
  deleted: false
}
const exampleDynamodbTeam = exampleDynamodb
const exampleDynamodbCompany = {
  ...exampleDynamodbTeam,
  groupType: GroupType.companyType
}

const exampleOut = {
  ...exampleDynamodb,
  links: {
    account: `/account/${exampleDynamodb.accountId}`,
    group: `/team/${exampleDynamodb.groupId}`
  }
}
const exampleOutTeam = exampleOut
const exampleOutCompany = { ...exampleOutTeam, groupType: GroupType.companyType }

const GroupAdminDTO = DTO.schema
  .keys({
    structureVersion: StructureVersion.schema.required().alter({
      input: schema => schema.min(1),
      dynamodb: schema => schema.min(2),
      output: schema => schema.min(2)
    }),
    accountId: AccountId.schema.required().alter({ input: schema => schema.optional() }),
    groupType: GroupType.schema.alter({
      input: schema => schema.optional(),
      dynamodb: schema => schema.required(),
      output: schema => schema.required()
    }),
    groupId: UUID.schema.required().alter({ input: schema => schema.optional() }),
    deleted: Joi.bool().required(),
    links: Link.schema
      .keys({
        account: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        group: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodbTeam, { override: true }).example(exampleDynamodbCompany),
    output: schema => schema.example(exampleOutTeam, { override: true }).example(exampleOutCompany)
  })

module.exports = {
  exampleIn,
  exampleDynamodb,
  exampleDynamodbTeam,
  exampleDynamodbCompany,
  exampleOut,
  exampleOutTeam,
  exampleOutCompany,
  schema: GroupAdminDTO
}
