/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
/**
 * Calculate the total steps of a user, a group or a company.
 * @param oldSteps {integer}
 * @param newSteps {integer}
 * @returns {number}
 */
function calculateSteps (oldSteps, newSteps) {
  return oldSteps + (newSteps - oldSteps)
}

/**
 * Calculate the total distant walked by a person, group or Comp
 * @param gender {string}
 * @param totalSteps {integer}
 * @param distance {integer}
 * @returns {number}
 */
function calculateDistanceFromSteps (gender, totalSteps, distance) {
  if (distance === 0) {
    if (gender === 'M') {
      return Math.round(totalSteps * 0.82)
    } else if (gender === 'F') {
      return Math.round(totalSteps * 0.65)
    } else {
      return Math.round(totalSteps * ((0.82 + 0.65) / 2))
    }
  } else {
    if (gender === 'M') {
      return Math.round(distance / 0.82)
    } else if (gender === 'F') {
      return Math.round(distance / 0.65)
    } else {
      return Math.round(distance / ((0.82 + 0.65) / 2))
    }
  }
}

/**
 * calculate the number of steps to walk until next level is reached.
 * @param totalSteps {integer}
 * @param currentLevel {integer}
 * @returns {number}
 */
function calculateStepsToNextLevel (totalSteps, currentLevel) {
  const levelInfo = getLevelInfo()

  const nextLevelInfo = levelInfo.filter(l => l.level === currentLevel)

  if (nextLevelInfo.length > 0) {
    const myResult = nextLevelInfo[0].maxSteps + 1 - totalSteps
    return myResult
  } else {
    return 0
  }
}

/**
 * calculate the level from the number of steps. Say a person walks 100000 steps
 * which is (8,2 km) the formula ( ROUND(LevelX *(10000 + LevelX * 90);-3)) can not
 * be used in that form.
 * @param totalSteps {number}
 * @param currentLevel {number}
 * @returns {number}
 */
function calculateLevelFromSteps (totalSteps, currentLevel) {
  const levelInfo = getLevelInfo()

  for (const item of levelInfo) {
    // console.log(`Level ${item.level} => max. ${item.maxSteps} steps`)
    if (totalSteps <= item.maxSteps) {
      return item.level
    }
  }

  return 11
}

function getLevelInfo () {
  // For the calculation of the levels we've used the this formula : Bxg^x
  // B= 10000
  // g (growth-factor) => 1.409730739
  // x (level)

  return [
    { level: 1, maxSteps: 30000 },
    { level: 2, maxSteps: 80000 },
    { level: 3, maxSteps: 130000 },
    { level: 4, maxSteps: 180000 },
    { level: 5, maxSteps: 230000 },
    { level: 6, maxSteps: 280000 },
    { level: 7, maxSteps: 330000 },
    { level: 8, maxSteps: 390000 },
    { level: 9, maxSteps: 460000 },
    { level: 10, maxSteps: 540000 }
  ]
}

module.exports = {
  calculateSteps: calculateSteps,
  calculateDistanceFromSteps: calculateDistanceFromSteps,
  calculateStepsToNextLevel: calculateStepsToNextLevel,
  calculateLevelFromSteps: calculateLevelFromSteps
}
