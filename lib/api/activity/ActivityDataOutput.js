/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Joi = require('@hapi/joi')
const AccountId = require('../account/AccountId')
const TrackerType = require('../account/trackerConnection/TrackerType')

const example = {
  accountId: AccountId.example,
  trackerType: TrackerType.fitbitType,
  date: new Date('2020-04-15'),
  steps: 100
}

const schema = Joi.object()
  .keys({
    accountId: AccountId.schema.required(),
    trackerType: TrackerType.schema.required(),
    date: Joi.date()
      .iso()
      .required(),
    steps: Joi.number()
      .positive()
      .min(0)
      .required()
  })
  .tailor('output')
  .example(example, { override: true })

module.exports = { schema, example }
