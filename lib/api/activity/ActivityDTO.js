/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const AccountId = require('../account/AccountId')
const Joi = require('@hapi/joi')
const Link = require('./../../ppwcode/lambda/Link')
const PointInTime = require('./../../ppwcode/PointInTime')
const TrackerType = require('./../account/trackerConnection/TrackerType')

const exampleIn = {
  ...DTO.exampleIn,
  accountId: 'id-OF-THIS-account',
  activityDate: '2020-01-23T15:22:39.212Z',
  trackerType: TrackerType.polarType,
  numberOfSteps: 100,
  distance: 10000,
  deleted: false
}

const exampleDynamoDb = {
  ...DTO.exampleDynamodb,
  accountId: 'id-OF-THIS-account',
  activityDate: '2020-01-23T15:22:39.212Z',
  trackerType: TrackerType.polarType,
  numberOfSteps: 100,
  distance: 10000,
  deleted: false
}

const exampleOut = {
  ...exampleDynamoDb,
  links: {
    account: '/account/id-of-Eric',
    accountProfile: '/account/30000cc8-a728-4925-9bdb-e488337a93d6/publicProfile'
  }
}

/**
 * Represents an Activity object in the real world
 * It holds the activity log of a user for a certain date and
 * on a certain trackerType.
 * @typedef ActivityDTO
 * @type {DTO}
 *
 */
const ActivityDTO = DTO.baseSchema
  .keys({
    accountId: AccountId.schema.required(),
    activityDate: PointInTime.schema.required(),
    trackerType: Joi.string()
      .valid(
        TrackerType.polarType,
        TrackerType.fitbitType,
        TrackerType.garminType,
        TrackerType.googleType,
        TrackerType.manualType
      )
      .required(),
    numberOfSteps: Joi.number()
      .precision(0)
      .required(),
    distance: Joi.number()
      .precision(0)
      .required(),
    deleted: Joi.bool().required(),
    links: Link.schema
      .keys({
        account: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        accountProfile: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    dynamodb: schema => schema.example(exampleDynamoDb, { override: true }),
    input: schema => schema.example(exampleIn, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })
module.exports = {
  exampleIn: exampleIn,
  exampleDynamoDb: exampleDynamoDb,
  exampleOut: exampleOut,
  schema: ActivityDTO
}
