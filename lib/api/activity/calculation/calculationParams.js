/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

const example = {
  gender: 'F',
  steps: 4560,
  distance: 0
}

const CalculationParams = Joi.object()
  .keys({
    gender: Joi.string()
      .length(1)
      .pattern(/^([MFX])$/)
      .required(),
    steps: Joi.number()
      .min(0)
      .integer()
      .required(),
    distance: Joi.number()
      .integer()
      .min(0)
      .required()
  })
  .unknown(true)
  .example(example)

module.exports = {
  example,
  schema: CalculationParams
}
