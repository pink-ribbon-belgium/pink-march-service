/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const GroupDTO = require('../group/GroupDTO')
const Joi = require('@hapi/joi')
const SingleLineStringSchema = require('../../ppwcode/SingleLineString').schema
const GroupType = require('../group/GroupType')

/** @type {CompanyDTO} */
const exampleIn = {
  ...GroupDTO.exampleIn,
  groupType: GroupType.companyType,
  address: 'this is an address 12',
  zip: '2500',
  city: 'Lier',
  vat: 'BE0452123456',
  contactFirstName: 'Jos',
  contactLastName: 'Pros',
  contactEmail: 'email@exampleIn.com',
  contactTelephone: '+32498765432',
  unit: 'Test-Unit'
}

const exampleDynamodb = {
  ...GroupDTO.exampleDynamodb,
  groupType: GroupType.companyType,
  address: 'this is an address 12',
  zip: '2500',
  city: 'Lier',
  vat: 'BE0452123456',
  reference: 'REF2021-1567468',
  contactFirstName: 'Jos',
  contactLastName: 'Pros',
  contactEmail: 'email@exampleIn.com',
  contactTelephone: '+32498765432',
  unit: 'Test-Unit'
}

const exampleOut = {
  ...exampleDynamodb,
  joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/${GroupType.companyType.toLowerCase()}/${
    GroupDTO.exampleIn.id
  }`,
  links: {
    publicProfile: `/${exampleDynamodb.groupType.toLowerCase()}/${exampleDynamodb.id}/publicProfile`,
    administrators: `/${this.groupType}/${exampleDynamodb.id}/administrators`,
    slots: `/${this.groupType}/${exampleDynamodb.id}/slots`,
    payments: `/${this.groupType}/${exampleDynamodb.id}/payments`
  }
}
/**
 * Represents a Company in the real world, which is a subclass of a Group
 *
 * {INSERT MORE DESCRIPTION HERE}
 *
 * @typedef CompanyDTO
 * @type {DTO}
 * @property {number} id - opaque id of this company.
 */

/** @type {ObjectSchema} */
const CompanyDTO = GroupDTO.schema
  .keys({
    groupType: Joi.string()
      .valid('Company')
      .required(),
    address: SingleLineStringSchema.required(),
    zip: Joi.string()
      .pattern(/^\d{4}$/)
      .required(),
    city: SingleLineStringSchema.required(),
    vat: Joi.string()
      .pattern(/^BE\d{10}$/)
      .required(),
    reference: SingleLineStringSchema.optional(),
    contactFirstName: SingleLineStringSchema.required(),
    contactLastName: SingleLineStringSchema.required(),
    contactEmail: Joi.string()
      .email()
      .required(),
    contactTelephone: Joi.string()
      .pattern(/^\+\d{10,20}$/)
      .required(),
    unit: Joi.string()
      .optional()
      .allow(null)
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: CompanyDTO
}
