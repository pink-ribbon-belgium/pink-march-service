/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const CompanyDTO = require('./CompanyDTO')
const GroupProfileDTO = require('../group/GroupProfileDTO')
const UUID = require('../../ppwcode/UUID')

// noinspection SpellCheckingInspection
const example = {
  ...CompanyDTO.exampleIn,
  ...GroupProfileDTO.exampleIn,
  accountId: UUID.example
}

/**
 * Represents the creation of a Company
 *
 * @typedef NewCompanyDTO
 * @type {CompanyDTO}
 * @property {string} code - unique code of this company.
 */

/** @type {ObjectSchema} */
const NewCompanyDTO = CompanyDTO.schema
  .tailor('input')
  .concat(GroupProfileDTO.schema.tailor('input'))
  .keys({
    accountId: UUID.schema.required()
  })
  .unknown(true)
  .example(example, { override: true })

module.exports = {
  example: example,
  schema: NewCompanyDTO
}
