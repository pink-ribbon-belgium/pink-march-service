/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const GroupDTO = require('../../../lib/api/group/GroupDTO')
const UUID = require('../../ppwcode/UUID')
const Joi = require('@hapi/joi')
const GroupType = require('../group/GroupType')
const Link = require('../../ppwcode/lambda/Link')
const Logo = require('./../../ppwcode/Logo')
const AccountId = require('../../api/account/AccountId')

const exampleIn = {
  ...GroupDTO.exampleIn,
  groupId: '0dc839a9-50ea-4948-bb39-7e50b8b75e62',
  groupType: GroupType.companyType,
  name: 'test-subGroup',
  logo: Logo.example,
  accountId: 'test-account-id'
}

const exampleDynamoDB = {
  ...GroupDTO.exampleDynamodb,
  groupId: '0dc839a9-50ea-4948-bb39-7e50b8b75e62',
  groupType: GroupType.companyType,
  name: 'test-subGroup',
  logo: Logo.example,
  accountId: 'test-account-id'
}

const exampleOut = {
  ...exampleDynamoDB,
  joinLink: `https://app.pink-march.pink-ribbon-belgium.org/index.html#/join/subgroup/${GroupDTO.exampleIn.id}`,
  links: {
    slots: `I/${this.groupType}/${exampleDynamoDB.id}/slots`
  }
}

/**
 * Represents a SubGroup in the real world
 *
 * @typedef SubGroupDTO
 * @type {GroupDTO}
 * @property {string} code - unique code of this subgroup
 */

/** @type {ObjectSchema} */
const SubGroupDTO = GroupDTO.schema
  .keys({
    id: UUID.schema.alter({
      input: schema => schema.optional(),
      dynamodb: schema => schema.required(),
      output: schema => schema.required()
    }),
    groupId: UUID.schema.required(),
    groupType: Joi.string()
      .valid('Company')
      .required(),
    name: Joi.string().required(),
    logo: Logo.schema,
    accountId: AccountId.schema.required(),
    links: Link.schema
      .keys({
        slots: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamoDB, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamodb: exampleDynamoDB,
  exampleOut: exampleOut,
  schema: SubGroupDTO
}
