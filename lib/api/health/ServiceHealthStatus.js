const Joi = require('@hapi/joi')
const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../_util/validateSchema')

const ServiceHealthStatus = {
  OK: 'OK',
  WARNING: 'WARNING',
  ERROR: 'ERROR',
  UNREACHABLE: 'UNREACHABLE'
}

const values = Object.values(ServiceHealthStatus)
ServiceHealthStatus.schema = values.reduce(
  (acc, v) => acc.example(v),
  Joi.string()
    .valid(...values)
    .description(
      'OK: 200 - the service is running within specifications; ' +
        'WARNING: 270 - the service is running, but some requirements are not fulfilled; ' +
        'ERROR: 470 - the service is running, but some crucial specifications are not fulfilled, and operation is ' +
        'not guaranteed; ' +
        'UNREACHABLE: 500 - the service is not running or not available'
    )
)

ServiceHealthStatus.values = values
ServiceHealthStatus.statusCode = {
  OK: 200,
  WARNING: 270,
  ERROR: 470
  // we can never answer UNREACHABLE, since we are reachable to answer
}

const requiredServiceHealthStatus = ServiceHealthStatus.schema.required()

/**
 * @param {string} acc - the consolidated {@link ServiceHealthStatus} so far
 * @param {string} status - the {@link ServiceHealthStatus} to consolidate into `acc`
 * @param {boolean} required - whether `status` is to be considered required in the result
 */
ServiceHealthStatus.consolidate = new Contract({
  pre: [
    acc => validateSchema(requiredServiceHealthStatus, acc),
    acc => acc !== ServiceHealthStatus.UNREACHABLE,
    (acc, status) => validateSchema(requiredServiceHealthStatus, status)
    // required is truthy or falsy
  ],
  post: [
    function (acc, status, required) {
      return (
        Contract.outcome(arguments) !== ServiceHealthStatus.OK ||
        (acc === ServiceHealthStatus.OK && status === ServiceHealthStatus.OK)
      )
    },
    function (acc, status, required) {
      return (
        Contract.outcome(arguments) !== ServiceHealthStatus.WARNING ||
        (acc === ServiceHealthStatus.OK &&
          (status === ServiceHealthStatus.WARNING ||
            ((status === ServiceHealthStatus.ERROR || status === ServiceHealthStatus.UNREACHABLE) && !required))) ||
        (acc === ServiceHealthStatus.WARNING &&
          (!required || (status !== ServiceHealthStatus.ERROR && status !== ServiceHealthStatus.UNREACHABLE)))
      )
    },
    function (acc, status, required) {
      return (
        Contract.outcome(arguments) !== ServiceHealthStatus.ERROR ||
        acc === ServiceHealthStatus.ERROR ||
        ((status === ServiceHealthStatus.ERROR || status === ServiceHealthStatus.UNREACHABLE) && required)
      )
    },
    function () {
      return Contract.outcome(arguments) !== ServiceHealthStatus.UNREACHABLE
    }
  ]
}).implementation(function consolidate (acc, status, required) {
  // we can never answer UNREACHABLE, since we are reachable to answer
  switch (acc) {
    case ServiceHealthStatus.OK:
      switch (status) {
        case ServiceHealthStatus.OK:
          return acc
        case ServiceHealthStatus.WARNING:
          return status
        default:
          // ERROR, UNREACHABLE
          return required ? ServiceHealthStatus.ERROR : ServiceHealthStatus.WARNING
      }
    case ServiceHealthStatus.WARNING:
      switch (status) {
        case ServiceHealthStatus.OK:
        case ServiceHealthStatus.WARNING:
          return acc
        default:
          // ERROR, UNREACHABLE
          return required ? ServiceHealthStatus.ERROR : ServiceHealthStatus.WARNING
      }
    default:
      // ERROR, UNREACHABLE
      return ServiceHealthStatus.ERROR
  }
})

module.exports = ServiceHealthStatus
