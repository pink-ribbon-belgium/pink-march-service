/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const AccountId = require('../../account/AccountId')

const example = {
  accountId: AccountId.example
}
const AccountAggregatePathParameters = Joi.object()
  .keys({
    accountId: AccountId.schema.required()
  })
  .unknown(false)
  .example(example)
module.exports = {
  example: example,
  schema: AccountAggregatePathParameters
}
