/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const AccountId = require('../../account/AccountId')
const DTO = require('../../../ppwcode/lambda/DTO')
const Joi = require('@hapi/joi')
const UUID = require('../../../ppwcode/UUID')
const Link = require('../../../ppwcode/lambda/Link')
const GroupType = require('../../group/GroupType')

const example = {
  accountId: 'some-accountId-for-accountAggregate',
  totalSteps: 2500,
  totalDistance: 3000,
  groupId: UUID.example,
  groupType: GroupType.companyType,
  subGroupId: UUID.example,
  name: 'some-kind-of-a-name',
  rank: 10,
  totalParticipants: 100,
  previousLevel: 2,
  acknowledged: false
}

const exampleIn = {
  ...DTO.exampleIn,
  ...example
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  ...example
}

const exampleOut = {
  ...exampleDynamodb,
  level: 2,
  newLevel: false,
  previousLevel: 2,
  stepsToNextLevel: 1500,
  links: {
    account: '/account/id-of-Eric',
    accountProfile: '/account/30000cc8-a728-4925-9bdb-e488337a93d6/publicProfile'
  }
}

const AccountAggregateDTO = DTO.baseSchema
  .keys({
    accountId: AccountId.schema.required(),
    totalSteps: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    totalDistance: Joi.number()
      .min(0)
      .allow(0)
      .positive()
      .integer()
      .required(),
    level: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .alter({
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required(),
        input: schema => schema.optional()
      }),
    newLevel: Joi.boolean().alter({
      dynamodb: schema => schema.forbidden(),
      output: schema => schema.required(),
      input: schema => schema.optional()
    }),
    previousLevel: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    stepsToNextLevel: Joi.number()
      .positive()
      .allow(0)
      .integer()
      .alter({
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required(),
        input: schema => schema.optional()
      }),
    groupId: UUID.schema.optional(),
    groupType: GroupType.schema.required(),
    subGroupId: UUID.schema.optional(),
    name: Joi.string().required(),
    rank: Joi.number()
      .positive()
      .integer(),
    totalParticipants: Joi.number()
      .positive()
      .integer()
      .required(),
    acknowledged: Joi.boolean().required(),
    links: Link.schema
      .keys({
        account: Joi.string()
          .uri({ relativeOnly: true })
          .required(),
        accountProfile: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    input: schema => schema.example(exampleIn, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })
module.exports = {
  exampleIn: exampleIn,
  exampleDynamoDb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: AccountAggregateDTO
}
