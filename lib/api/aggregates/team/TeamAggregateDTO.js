/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../../ppwcode/UUID')
const DTO = require('../../../ppwcode/lambda/DTO')
const GroupType = require('../../group/GroupType')
const Link = require('../../../ppwcode/lambda/Link')

const example = {
  teamAggregateId: 'f5b372e4-2ed4-4403-9a73-ff79a8ad4105',
  totalSteps: 4500,
  totalDistance: 3750,
  averageSteps: 4125,
  averageDistance: 3468,
  companyId: UUID.example,
  groupType: GroupType.companyType,
  name: 'some-kind-of-monster',
  extendedName: 'Parent(some-kind-of-monster)',
  rank: 2,
  totalTeams: 10,
  hasLogo: false
}

const exampleIn = {
  ...DTO.exampleIn,
  ...example
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  ...example
}

const exampleOut = {
  ...exampleDynamodb,
  links: {
    team: '/team/f5b372e4-2ed4-4403-9a73-ff79a8ad4105'
  }
}

const TeamAggregateDTO = DTO.baseSchema
  .keys({
    teamAggregateId: UUID.schema.required(),
    totalSteps: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    totalDistance: Joi.number()
      .min(0)
      .allow(0)
      .positive()
      .integer()
      .required(),
    averageSteps: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    averageDistance: Joi.number()
      .min(0)
      .allow(0)
      .positive()
      .integer()
      .required(),
    totalTeams: Joi.number()
      .positive()
      .integer()
      .required(),
    companyId: UUID.schema.optional(),
    name: Joi.string().required(),
    rank: Joi.number()
      .positive()
      .integer(),
    groupType: GroupType.schema.required(),
    extendedName: Joi.string().optional(),
    hasLogo: Joi.bool().required(),
    links: Link.schema
      .keys({
        team: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    input: schema => schema.example(exampleIn, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamoDb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: TeamAggregateDTO
}
