/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../../ppwcode/lambda/DTO')
const Joi = require('@hapi/joi')
const UUID = require('../../../ppwcode/UUID')
const Link = require('../../../ppwcode/lambda/Link')

const example = {
  companyId: UUID.example,
  totalSteps: 3000,
  totalDistance: 2500,
  averageSteps: 3000,
  averageDistance: 2500,
  unit: 'P&V',
  hasLogo: true,
  name: 'company-aggregate-name',
  extendedName: 'companyX (Unit1)',
  rank: 10,
  totalCompanies: 100
}

const exampleIn = {
  ...DTO.exampleIn,
  ...example
}

const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  ...example
}

const exampleOut = {
  ...exampleDynamodb,
  links: {
    company: '/company/ac8ff1ec-8085-43ac-9494-f8d3a26f6da9'
  }
}

const CompanyAggregateDTO = DTO.baseSchema
  .keys({
    companyId: UUID.schema.required(),
    totalSteps: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    totalDistance: Joi.number()
      .min(0)
      .allow(0)
      .positive()
      .integer()
      .required(),
    averageSteps: Joi.number()
      .positive()
      .allow(0)
      .min(0)
      .integer()
      .required(),
    averageDistance: Joi.number()
      .min(0)
      .allow(0)
      .positive()
      .integer()
      .required(),
    unit: Joi.string().optional(),
    name: Joi.string().required(),
    extendedName: Joi.string().optional(),
    hasLogo: Joi.bool().required(),
    rank: Joi.number()
      .positive()
      .integer(),
    totalCompanies: Joi.number()
      .positive()
      .integer()
      .required(),
    links: Link.schema
      .keys({
        company: Joi.string()
          .uri({ relativeOnly: true })
          .required()
      })
      .alter({
        input: schema => schema.optional(),
        dynamodb: schema => schema.forbidden(),
        output: schema => schema.required()
      })
  })
  .unknown(true)
  .alter({
    dynamodb: schema => schema.example(exampleDynamodb, { override: true }),
    input: schema => schema.example(exampleIn, { override: true }),
    output: schema => schema.example(exampleOut, { override: true })
  })

module.exports = {
  exampleIn: exampleIn,
  exampleDynamoDb: exampleDynamodb,
  exampleOut: exampleOut,
  schema: CompanyAggregateDTO
}
