/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const DTO = require('../../ppwcode/lambda/DTO')
const Joi = require('@hapi/joi')
const PointInTime = require('../../ppwcode/PointInTime')
const requiredVoucherCode = require('./VoucherCode').schema.required()

/** @type {VoucherDTO} */
const exampleIn = {
  ...DTO.exampleIn,
  voucherCode: 'PV2020',
  reusable: false,
  percentage: 0.1,
  expiryDate: PointInTime.example
}

/** @type {VoucherDTO} */
const exampleDynamodb = {
  ...DTO.exampleDynamodb,
  voucherCode: 'PV2020',
  reusable: false,
  percentage: 0.1,
  expiryDate: PointInTime.example
}

const VoucherDTO = DTO.baseSchema
  .keys({
    voucherCode: requiredVoucherCode,
    reusable: Joi.boolean().required(),
    amount: Joi.number()
      .positive()
      .precision(2)
      .optional(),
    percentage: Joi.number()
      .positive()
      .max(1)
      .precision(2)
      .optional(),
    expiryDate: PointInTime.schema.required()
  })
  .or('amount', 'percentage')
  .oxor('amount', 'percentage')
  .unknown(true)
  .alter({
    input: schema => schema.example(exampleIn, { override: true }),
    dynamodb: schema => schema.example(exampleDynamodb, { override: true })
  })

module.exports = {
  schema: VoucherDTO,
  exampleIn,
  exampleDynamodb
}
