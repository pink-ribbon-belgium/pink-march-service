/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const log = require('./_util/simpleLog')
const validateSchema = require('./_util/validateSchema')
const { schema: StreamEvent } = require('./stream/StreamEvent')
const { schema: LambdaContext } = require('./LambdaContext')
const streamRecordHandlers = require('./stream/streamRecordHandlers')
const marshalledDynamodbStreamHandler = require('./stream/marshalledDynamodbStreamHandler')
const { schema: Item } = require('./server/aws/dynamodb/Item')

/**
 * Lambda consuming DynamoDB stream. The event contains the old and the new DynamoDB Item. All stream handlers are
 * called in parallel, async. Stream handlers should quickly determine whether the change is relevant to them, and
 * return early if not. If a stream handler determines that the change is relevant, it should start its work, and
 * resolve asap. Stream handlers should never throw any errors, but instead log at INFO or WARN level.
 *
 * The mode is determined from the always present `newItem`.
 *
 * When stream handlers write to storage, they should use a `sub` of the form `stream-HANDLER_NAME`.
 *
 * `REMOVE` stream records are not expected in production, since we never delete information. We do however clean up
 * after automated tests. Therefor, this function accepts `REMOVE` stream records, but discards them early (and reports
 * the count).
 *
 * This function _should_ never throw (but shit happens). When this function throws, AWS retries the call with the same
 * collection of records. The event source mapping determines how often. It is a good idea to enable _bisection_.
 * In this case, the failing record collection is split in half, and each half is retried separately. By repeating this
 * enough times (> 2ln record collection size), eventually a failing record is retried separately. This ensures that
 * the records of a record collection that do not fail, are handled eventually.
 *
 * Note that the preconditions are determined by AWS, and not by us. This implies that there could be items in stream
 * records that are valid for AWS, but not for us. Because this function should never throw, we need to handle these
 * cases in nominal code. `NewImage` and `OldImage` objects that are not acceptable {@link Item}s are logged as `ERROR`,
 * and removed from the record collection to be processed.
 *
 * This means that any `ERROR` in the logs should be considered a programming error in production. We do write faulty
 * items in automated tests. As a result, some `ERROR` logs from the test dynamodb table are expected.
 */
const handlerContract = new PromiseContract({
  pre: [event => validateSchema(StreamEvent, event), (event, context) => validateSchema(LambdaContext, context)],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
})

async function handlerImpl (event, context) {
  const { awsRequestId: flowId } = context // awsRequestId happens to be a UUID, used as flowId
  const sot = new Date().toISOString()
  const { insertModifyRecords, deleteCount, invalidCount } = event.Records.reduce(
    (acc, record) => {
      if (record.eventName === 'REMOVE') {
        // NOTE: delete records are not processed. We never delete in production. They only appear in automated tests.
        acc.deleteCount++
        return acc
      }

      // MUDO: don't process records from automated test?

      let ok = true
      const oldItemValidation = Item.validate(record.dynamodb.OldImage) // might not exist
      if (oldItemValidation.error) {
        log.error(module, 'handler', flowId, oldItemValidation.error)
        ok = false
      }
      const newItemValidation = Item.validate(record.dynamodb.NewImage)
      if (newItemValidation.error) {
        log.error(module, 'handler', flowId, newItemValidation.error)
        ok = false
      }
      if (!ok) {
        acc.invalidCount++
        return acc
      }

      acc.insertModifyRecords.push({
        mode: record.dynamodb.NewImage.mode,
        event: record.eventName,
        keys: record.dynamodb.Keys,
        oldItem: oldItemValidation.value,
        newItem: newItemValidation.value
      })
      return acc
    },
    { insertModifyRecords: [], deleteCount: 0, invalidCount: 0 }
  )
  const nrOfRecordHandlings = insertModifyRecords.length * streamRecordHandlers.length
  log.info(module, 'handler', flowId, 'CALLED', {
    /* We do not log the event here: it is large, contains irrelevant information, and might contain **!sensitive**
       information. */
    context,
    sot,
    nrOfRecords: event.Records.length,
    nrOfDeleteRecords: deleteCount,
    nrOfInvalidRecords: invalidCount,
    nrOfInsertModifyRecords: insertModifyRecords.length,
    nrOfStreamHandlers: streamRecordHandlers.length,
    nrOfRecordHandlings
  })

  const recordHandlings = { total: 0, errors: 0, unhandled: 0, handled: 0 }
  await Promise.allSettled(
    insertModifyRecords
      .map(record =>
        streamRecordHandlers.map(async streamRecordHandler => {
          recordHandlings.total++
          try {
            const handled = await streamRecordHandler(flowId, sot, record)
            recordHandlings[handled === false ? 'unhandled' : 'handled']++
            // log.info(
            //   module,
            //   'handler',
            //   flowId,
            //   handled === false ? 'STREAM_RECORD_HANDLER_UNHANDLED' : 'STREAM_RECORD_HANDLER_HANDLED',
            //   {
            //     mode: record.mode,
            //     streamRecordHandler: streamRecordHandler.name,
            //     keys: record.keys
            //   }
            // )
          } catch (err) {
            // eat, but log, error
            recordHandlings.errors++
            log.error(module, 'handler', flowId, err)
          }
        })
      )
      .flat()
  )

  log[recordHandlings.errors <= 0 ? 'info' : 'warn'](module, 'handler', flowId, 'NOMINAL_END', {
    sot,
    recordHandlings
  })
}

const handler = handlerContract.implementation(handlerImpl)
handler.streamHandlers = streamRecordHandlers

module.exports = { handler: marshalledDynamodbStreamHandler(handler), unmarshalledHandler: handler }
