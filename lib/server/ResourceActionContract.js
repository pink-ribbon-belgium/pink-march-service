/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../_util/validateSchema')
const AccountId = require('../api/account/AccountId')
const Mode = require('../ppwcode/Mode')
const Uuid = require('../ppwcode/UUID')
const PointInTime = require('../ppwcode/PointInTime')
const PathParameters = require('../ppwcode/lambda/apigateway/PathParameters')

const requiredSot = PointInTime.schema
  .required()
  .label('sot')
  // TODO review; for the put methods at least, this should be internal
  .allow('actual')
const requiredSub = AccountId.schema.required().label('sub')
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = Uuid.schema.required().label('flowId')

/**
 * Resource Actions are called from the {@link service#handler}, with a `sot`, `sub`, `mode`, `flowId`,
 * `pathParameters`, and `body`. `sot`, `sub`, `mode`, and `flowId`, are required. `pathParameters` and `body` are
 * optional. If there are `pathParameters` or a `body`, they are never `null`. If there is a `pathParameters`, it is an
 * object. A `body` can be anything that can be represented in JSON.
 *
 * @type {PromiseContract}
 */
/*  NOTE: Particular implementation will test `pathParameters` and `body` if necessary, and boom if nok, and have
          stronger post and exception conditions. */
const ResourceActionContract = new PromiseContract({
  pre: [
    sot => validateSchema(requiredSot, sot),
    (sot, sub) => validateSchema(requiredSub, sub),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, pathParam) => validateSchema(PathParameters.schema.label('pathParams'), pathParam)
  ],
  post: [],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
})

module.exports = ResourceActionContract
