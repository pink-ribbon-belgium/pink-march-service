/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const getAccountAggregates = require('../aggregates/account/getAllAccountAggregates')
const getTakenSlots = require('../slot/getAllTakenSlots')
const calculateStatistics = require('../../../api/statistics/calculation/calculateStatistics')
const config = require('config')
const putTransactionDynamodb = require('../../aws/dynamodb/putTransaction')

const log = require('./../../../_util/simpleLog')

const write = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice()
}).implementation(async function writeStatistics (sot, sub, mode, flowId) {
  log.info(module, 'writeStatistics', flowId, 'CALLED', { sot, mode })
  const accountAggregates = await getAccountAggregates(sot, sub, mode, flowId)
  const totalDistanceParticipants = accountAggregates.reduce((acc, item) => {
    return acc + item.totalDistance
  }, 0)
  const timesRoundTheWorld = calculateStatistics.calculateDistanceAroundTheWorld(totalDistanceParticipants)
  const takenSlots = await getTakenSlots(sot, sub, mode, flowId)

  const today = new Date()
  let activityPeriodStartDate
  let activityPeriodEndDate
  /* istanbul ignore next : we can't test different modes */
  switch (mode) {
    case 'dev-experiment':
    case 'demo':
    case 'june2020':
      activityPeriodStartDate = new Date(config.activityPeriod[mode].startdate)
      activityPeriodEndDate = new Date(config.activityPeriod[mode].enddate)
      break
    case 'production':
      activityPeriodStartDate = new Date('2021-05-01')
      activityPeriodEndDate = new Date(config.activityPeriod[mode].enddate)
      break
    default:
      activityPeriodStartDate = new Date(config.activityPeriod.default.startdate)
      activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
      break
  }
  let calculateDays
  let daysUntil

  /* istanbul ignore next : we can't test different modes */
  if (today < activityPeriodStartDate) {
    calculateDays = activityPeriodStartDate.getTime() - today.getTime()
    daysUntil = Math.floor(calculateDays / (1000 * 3600 * 24)) + 1 // TODO add calculation to calculateStatistics
  } else if (today > activityPeriodStartDate && today < activityPeriodEndDate) {
    calculateDays = activityPeriodEndDate.getTime() - today.getTime()
    daysUntil = Math.floor(calculateDays / (1000 * 3600 * 24)) + 2
  } else if (today > activityPeriodStartDate && today === activityPeriodEndDate) {
    daysUntil = 1
  } else {
    daysUntil = 0
  }

  const submittedItem = {
    mode: mode,
    key: `/${mode}/statistics`,
    submitted: sot,
    submittedBy: sub,
    flowId,
    data: {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      totalParticipants: takenSlots.length,
      daysUntil,
      timesRoundTheWorld
    }
  }

  const actualItem = {
    ...submittedItem,
    submitted: 'actual'
  }
  const result = putTransactionDynamodb([submittedItem, actualItem])

  log.info(module, 'writeStatistics', flowId, 'NOMINAL_END', { sot, mode })

  return result
})

module.exports = write
