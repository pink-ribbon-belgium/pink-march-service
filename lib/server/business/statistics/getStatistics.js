/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')

const getActual = require('../../aws/dynamodb/getActual')

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ])
}).implementation(async function getStatistics (sot, sub, mode, flowId) {
  const key = `/${mode}/statistics`
  const item = await getActual(mode, key, true)

  return {
    totalParticipants: item.data.totalParticipants,
    daysUntil: item.data.daysUntil,
    timesRoundTheWorld: item.data.timesRoundTheWorld
  }
})

module.exports = get
