/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const UUID = require('../../../../ppwcode/UUID')
const GroupProfileDTO = require('../../../../api/group/GroupProfileDTO')

const requiredGroupId = UUID.schema.required()

const dynamodbDTO = GroupProfileDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const GroupProfileContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.id === kwargs.dto.id
    },
    function (kwargs) {
      return this.name === kwargs.dto.name
    },
    function (kwargs) {
      return this.logo === kwargs.dto.logo
    }
  ])
})

GroupProfileContract.Kwargs = Kwargs

GroupProfileContract.methods = { ...PersistentObject.contract.methods }

GroupProfileContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredGroupId, this.id)
  },
  function () {
    return this.toJSON().id === this.id
  },
  function () {
    return this.toJSON().name === this.name
  },
  function () {
    return this.toJSON().logo === this.logo
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  }
])

function GroupProfileImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.id = kwargs.dto.id
  this.name = kwargs.dto.name
  this.logo = kwargs.dto.logo
}

GroupProfileImpl.prototype = Object.create(PersistentObject.prototype)
GroupProfileImpl.prototype.constructor = GroupProfileImpl

GroupProfileImpl.prototype.id = undefined

GroupProfileImpl.prototype.structureVersion = 1

GroupProfileImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    id: this.id,
    name: this.name,
    logo: this.logo
  }
}

GroupProfileImpl.prototype.getKey = function () {
  return GroupProfile.getKey({ id: this.id, mode: this.mode })
}

GroupProfileImpl.prototype.getLinks = function () {
  return {
    group: `/I/group/${this.id}`
  }
}

GroupProfileImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/groupProfile`
  }
}

GroupProfileImpl.prototype.invariants = GroupProfileContract.invariants

const GroupProfile = GroupProfileContract.implementation(GroupProfileImpl)

GroupProfile.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/group/${kwargs.id}/publicProfile`
})

module.exports = GroupProfile
