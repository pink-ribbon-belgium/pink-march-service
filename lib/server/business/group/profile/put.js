/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const putDynamodb = require('../../../aws/dynamodb/put')
const GroupProfile = require('./GroupProfile')
const GroupProfileDTO = require('../../../../api/group/GroupProfileDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateInputStream = require('../../../../_util/validateInputSchema')
const TeamParams = require('../../../../api/team/TeamPathParameters')
const CompanyParams = require('../../../../api/company/CompanyPathParameters')

const requiredPathParametersSchema = Joi.alternatives(TeamParams.schema, CompanyParams.schema)

const requiredInputSchema = GroupProfileDTO.schema.tailor('input').required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputStream(requiredPathParametersSchema, pathParameters)
  validateInputStream(requiredInputSchema, body)
  const groupProfile = new GroupProfile({
    mode,
    dto: { ...body, createdAt: sot, createdBy: sub, id: pathParameters.teamId || pathParameters.companyId },
    sot,
    sub
  })

  const item = {
    ...groupProfile.toItem(),
    flowId
  }

  return putDynamodb(item)
})

module.exports = put
