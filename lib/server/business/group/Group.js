/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const UUID = require('../../../ppwcode/UUID')
const PersistentObject = require('../PersistentObject')
const GroupDTO = require('../../../api/group/GroupDTO')
const Joi = require('@hapi/joi')
const config = require('config')
const Mode = require('../../../ppwcode/Mode')

const requiredUUID = UUID.schema.required()
const requiredMode = Mode.schema.required().label('mode')

const dynamodbDTO = GroupDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

/**
 * @typedef Kwargs
 * @extends {PersistentObject.Kwargs}
 *
 * @property {GroupDTO} dto
 */

/** @type {ObjectSchema} */
const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const GetLinkKwargs = Joi.object()
  .keys({
    mode: requiredMode,
    groupId: requiredUUID,
    groupType: Joi.string()
      .required()
      .valid('Team', 'Company')
  })
  .required()

/**
 * Represents an Group in the real world
 *
 * @param {Kwargs} kwargs
 * @constructor
 */
const GroupContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.id === kwargs.dto.id
    },
    function (kwargs) {
      return this.code === kwargs.dto.code
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.linkId === kwargs.dto.linkId
    },
    function (kwargs) {
      return this.shortUrl === kwargs.dto.shortUrl
    }
  ])
})

GroupContract.Kwargs = Kwargs

GroupContract.methods = { ...PersistentObject.contract.methods }

GroupContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredUUID, this.id)
  },
  function () {
    return this.toJSON().id === this.id
  },
  function () {
    return this.toJSON().code === this.code
  },
  function () {
    return !!this.groupType
  },
  function () {
    return typeof this.groupType === 'string'
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    const links = this.getLinks()
    const linkPrefix = `/I/${this.groupType.toLowerCase()}/${this.id}`
    return Object.values(links).every(link => link.startsWith(linkPrefix))
  },
  function () {
    return this.getLinks().administrators === `/I/${this.groupType.toLowerCase()}/${this.id}/administrators`
  },
  function () {
    return this.getLinks().publicProfile === `/I/${this.groupType.toLowerCase()}/${this.id}/publicProfile`
  },
  function () {
    return this.getLinks().slots === `/I/${this.groupType.toLowerCase()}/${this.id}/slots`
  },
  function () {
    return this.getLinks().payments === `/I/${this.groupType.toLowerCase()}/${this.id}/payments`
  },
  function () {
    return this.toJSON().linkId === this.linkId
  },
  function () {
    return this.toJSON().shortUrl === this.shortUrl
  }
])

function GroupImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.id = kwargs.dto.id
  this.code = kwargs.dto.code
  this.groupType = kwargs.dto.groupType
  this.linkId = kwargs.dto.linkId
  this.shortUrl = kwargs.dto.shortUrl
}

GroupImpl.prototype = Object.create(PersistentObject.prototype)
GroupImpl.prototype.constructor = GroupImpl

/**
 * Opaque id of this group.
 *
 * @type {string}
 */
GroupImpl.prototype.id = undefined

/**
 * Version of the structure written out by `toJSON` (the constructor might be able to understand multiple, earlier or
 * later versions).
 *
 * @type {number}
 */
GroupImpl.prototype.structureVersion = 1

/**
 * @return {DTO}
 */
GroupImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    id: this.id,
    code: this.code,
    groupType: this.groupType,
    linkId: this.linkId,
    shortUrl: this.shortUrl
  }
}

GroupImpl.prototype.getKey = new Contract({}).abstract

GroupImpl.prototype.getLinks = function () {
  return {
    administrators: `/I/${this.groupType.toLowerCase()}/${this.id}/administrators`, // TODO not implemented
    slots: `/I/${this.groupType.toLowerCase()}/${this.id}/slots`,
    payments: `/I/${this.groupType.toLowerCase()}/${this.id}/payments`,
    publicProfile: `/I/${this.groupType.toLowerCase()}/${this.id}/publicProfile`
  }
}

GroupImpl.prototype.getJoinLink = function () {
  return Group.getJoinLink({ mode: this.mode, groupId: this.id, groupType: this.groupType })
}

GroupImpl.prototype.invariants = GroupContract.invariants

const Group = GroupContract.implementation(GroupImpl)

Group.getJoinLink = new Contract({
  pre: [kwargs => validateSchema(GetLinkKwargs, kwargs)]
}).implementation(function getJoinLink (kwargs) {
  let baseUrl
  if (kwargs.mode === 'demo') {
    baseUrl = config.baseurl.demo.url
  } else if (kwargs.mode === 'june2020') {
    baseUrl = config.baseurl.june2020.url
  } else {
    baseUrl = config.baseurl.url
  }
  return `${baseUrl}index.html#/join/${kwargs.groupType.toLowerCase()}/${kwargs.groupId}`
})

module.exports = Group
