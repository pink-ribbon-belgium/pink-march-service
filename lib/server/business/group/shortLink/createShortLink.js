/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const config = require('config')
const fetch = require('node-fetch')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../../_util/validateSchema')
const requiredShortLinkParams = require('../../../../api/group/shortLink/shortLinkParams').schema
const requiredShortLinkResult = require('../../../../api/group/shortLink/shortLinkResult').schema
const PointInTime = require('../../../../ppwcode/PointInTime').schema
const Mode = require('../../../../ppwcode/Mode')
const Uuid = require('../../../../ppwcode/UUID')

const requiredPointInTime = PointInTime.required()
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = Uuid.schema.required().label('flowId')

const createJoinLink = new PromiseContract({
  pre: [
    sot => validateSchema(requiredPointInTime, sot),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, params) => validateSchema(requiredShortLinkParams, params)
  ],
  post: [
    function (sot, sub, mode, flowId, params, response) {
      return validateSchema(requiredShortLinkResult, response)
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    }
  ]
}).implementation(async function createJoinLink (sot, sub, mode, flowId, params) {
  const apiKey = ['production', 'demo', 'june2020'].includes(mode)
    ? config.rebrandly.prod.apikey
    : config.rebrandly.dev.apikey

  const headers = {
    'Content-Type': 'application/json',
    apikey: apiKey
  }
  const url = 'https://api.rebrandly.com/v1/links'
  const body = {
    destination: params.joinLink,
    title: params.groupId
  }

  const response = await fetch(url, {
    method: 'POST',
    headers: headers,
    body: JSON.stringify(body)
  })

  let result
  if (response.status === 200) {
    result = await response.json()
  } else {
    throw Error(`${response.status} - ${response.statusText}`)
  }

  return {
    linkId: result.id,
    shortLink: `https://${result.shortUrl}`,
    destination: params.joinLink
  }
})

module.exports = createJoinLink
