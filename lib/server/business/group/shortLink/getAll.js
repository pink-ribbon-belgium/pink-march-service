/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const config = require('config')
const fetch = require('node-fetch')
const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../../_util/validateSchema')
const requiredShortLinkResult = require('../../../../api/group/shortLink/shortLinkResult').schema

const getShortLinks = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: [
    function (sot, sub, mode, flowId, result) {
      return result.length ? result.every(x => validateSchema(requiredShortLinkResult.required(), x)) : true
    }
  ]
}).implementation(async function getShortLinks (sot, sub, mode, flowId) {
  const apiKey = ['production', 'demo', 'june2020'].includes(mode)
    ? config.rebrandly.prod.apikey
    : config.rebrandly.dev.apikey

  const headers = {
    'Content-Type': 'application/json',
    apikey: apiKey
  }
  // limit of 25!!
  const url = 'https://api.rebrandly.com/v1/links'
  const response = await fetch(url, {
    method: 'GET',
    headers: headers
  })
  let result
  if (response.status === 200) {
    result = await response.json()
  } else {
    return []
  }
  return result.map(item => {
    return {
      linkId: item.id,
      shortLink: `https://${item.shortUrl}`,
      destination: item.destination
    }
  })
})

module.exports = getShortLinks
