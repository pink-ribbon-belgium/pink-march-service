/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const config = require('config')
const fetch = require('node-fetch')
const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../../_util/validateSchema')
const DeleteResult = require('../../../../api/group/shortLink/deleteResult').schema
const PointInTime = require('../../../../ppwcode/PointInTime').schema
const Mode = require('../../../../ppwcode/Mode')
const Uuid = require('../../../../ppwcode/UUID')

const requiredPointInTime = PointInTime.required()
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = Uuid.schema.required().label('flowId')
const requiredPathParams = Joi.object().keys({ linkId: Uuid.schema.required() })

const deleteShortLink = new PromiseContract({
  pre: [
    sot => validateSchema(requiredPointInTime, sot),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, pathParams) => validateSchema(requiredPathParams, pathParams)
  ],
  post: [
    function (sot, sub, mode, flowId, pathParams, response) {
      return validateSchema(DeleteResult, response)
    }
  ]
}).implementation(async function deleteShortLink (sot, sub, mode, flowId, pathParams) {
  const apiKey = ['production', 'demo', 'june2020'].includes(mode)
    ? config.rebrandly.prod.apikey
    : config.rebrandly.dev.apikey

  const headers = {
    'Content-Type': 'application/json',
    apikey: apiKey
  }
  const linkId = pathParams.linkId
  const url = `https://api.rebrandly.com/v1/links/${linkId}`
  const response = await fetch(url, {
    method: 'DELETE',
    headers: headers
  })

  let result
  if (response.status === 200) {
    result = await response.json()
  } else {
    result = {
      status: `${response.status} - ${response.statusText}`
    }
  }

  return {
    linkId,
    deleted: result.status
  }
})

module.exports = deleteShortLink
