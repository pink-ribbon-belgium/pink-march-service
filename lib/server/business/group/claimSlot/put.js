/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const config = require('config')
const Joi = require('@hapi/joi')
const sendGridMail = require('@sendgrid/mail')
const UUID = require('../../../../ppwcode/UUID')
const AccountId = require('../../../../api/account/AccountId')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const writeSlot = require('../../slot/write')
const getEmailTemplate = require('../../payment/getEmailTemplate')
const getPreferences = require('../../account/preferences/get')
const getAccountProfile = require('../../account/profile/get')
const getAccount = require('../../account/get')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const simpleLog = require('../../../../_util/simpleLog')
const writeAccountAggregate = require('../../aggregates/account/write')
const assert = require('assert')
const AuthenticationType = require('../../../../api/account/AuthenticationType')

const requiredPathParams = Joi.alternatives(
  Joi.object().keys({ teamId: UUID.schema.required(), accountId: AccountId.schema.required() }),
  Joi.object().keys({ companyId: UUID.schema.required(), accountId: AccountId.schema.required() })
)
const slotKeys = {
  indexName: 'Index_B',
  partitionKey: 'partition_key_B',
  sortKey: 'sort_key_B'
}

// TODO pull out the helper function, and test separately
async function sendMail (flowId, email, locale, authMethod) {
  simpleLog.info(module, 'claimSlot', flowId, 'SEND_MAIL', { email, locale })
  sendGridMail.setApiKey(config.email.sendgridApiKey)

  const mailTemplate = await getEmailTemplate(
    'confirmation_individual',
    locale.substring(0, 2).toLowerCase() === 'fr' ? 'FR' : 'NL'
  )
  const htmlBody = mailTemplate.body.replace(/\[AUTHMETHOD]/g, authMethod)

  const msg = {
    to: email,
    from: {
      name: mailTemplate.fromname,
      email: config.email.from
    },
    subject: mailTemplate.subject,
    html: htmlBody
  }

  try {
    await sendGridMail.send(msg)
    simpleLog.info(module, 'claimSlot', flowId, 'SENDING_MAIL_SUCCESS', { email, locale, msg })
  } catch (err) /* istanbul ignore next */ {
    /* keep silent on error, but log */
    simpleLog.info(module, 'claimSlot', flowId, 'SENDING_MAIL_ERROR', { email, locale, msg, err })
  }
}

// functions separated out to do easy logging
async function getFreeSlots (mode, flowId, groupId) {
  simpleLog.info(module, 'claimSlot', flowId, 'GET_FREE_SLOTS', { mode, groupId })
  const result = await getByIndex(mode, `/${mode}/group/${groupId}/free`, slotKeys, 'actual') // throws 404
  assert.ok(result)
  assert(result.length > 0)
  simpleLog.info(module, 'claimSlot', flowId, 'RECEIVED_FREE_SLOTS', { mode, groupId, freeSlots: result.length })
  return result
}

async function getAccountPreferences (sot, sub, mode, flowId, accountId) {
  simpleLog.info(module, 'claimSlot', flowId, 'GET_ACCOUNT_PREFERENCES_FOUND', { sot, sub, mode, accountId })
  const accountPreferences = await getPreferences(sot, sub, mode, flowId, { accountId })
  simpleLog.info(module, 'claimSlot', flowId, 'ACCOUNT_PREFERENCES_FOUND', {
    sot,
    sub,
    mode,
    accountId,
    accountPreferences
  })
  return accountPreferences
}

async function getAccountInfo (sot, sub, mode, flowId, accountId) {
  simpleLog.info(module, 'claimSlot', flowId, 'GET_ACCOUNT_FOUND', { sot, sub, mode, accountId })
  const accountInfo = await getAccount(sot, sub, mode, flowId, { accountId })
  simpleLog.info(module, 'claimSlot', flowId, 'ACCOUNT_FOUND', {
    sot,
    sub,
    mode,
    accountId,
    accountInfo
  })
  return accountInfo
}

function getAuthMethod (account, locale) {
  const split = account.sub.split('|')
  const localeSubstring = locale.substring(0, 2).toLowerCase()

  let authMethod
  switch (split[0]) {
    case 'auth0':
      authMethod = AuthenticationType.auth0Type[localeSubstring]
      break
    case 'facebook':
      authMethod = AuthenticationType.facebookType[localeSubstring]
      break
    case 'google-oauth2':
      authMethod = AuthenticationType.googleType[localeSubstring]
      break
    case 'windowslive':
      authMethod = AuthenticationType.windowsType[localeSubstring]
      break
    default:
      authMethod = 'Default'
      break
  }

  return authMethod
}

async function claim (sot, sub, mode, flowId, slotToClaim, accountId) {
  simpleLog.info(module, 'claimSlot', flowId, 'SLOT_PUT', { sot, sub, mode, slotId: slotToClaim.id, accountId })
  await writeSlot(sot, sub, mode, flowId, { ...slotToClaim, accountId }) // only programming errors can occur
  simpleLog.info(module, 'claimSlot', flowId, 'SLOT_PUT_SUCCESS', { sot, sub, mode, slotId: slotToClaim.id, accountId })
}

async function createAccountAggregate (sot, sub, mode, flowId, accountId, groupId, groupType, accountProfile) {
  const accountAggregateData = {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 1,
    accountId: accountId,
    totalSteps: 0,
    totalDistance: 0,
    previousLevel: 1,
    groupId: groupId,
    groupType: groupType,
    name: `${accountProfile.firstName} ${accountProfile.lastName}`,
    rank: 9999999,
    totalParticipants: 9999999,
    acknowledged: false
  }

  await writeAccountAggregate(sot, sub, mode, flowId, accountAggregateData)
}

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 400 ||
        PromiseContract.outcome(arguments).output.statusCode === 404
      )
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters) {
  simpleLog.info(module, 'claimSlot', flowId, 'CALLED', { sot, sub, mode, pathParameters })

  validateInputSchema(requiredPathParams, pathParameters)

  const groupId = pathParameters.teamId || pathParameters.companyId

  const [freeSlots, accountPreferences, accountProfile, account] = await Promise.all([
    getFreeSlots(mode, flowId, groupId), // throw 404
    getAccountPreferences(sot, sub, mode, flowId, pathParameters.accountId),
    getAccountProfile(sot, sub, mode, flowId, { accountId: pathParameters.accountId }),
    getAccountInfo(sot, sub, mode, flowId, pathParameters.accountId)
  ])

  const authMethod = getAuthMethod(account, accountPreferences.language)

  await Promise.all([
    claim(
      sot,
      sub,
      mode,
      flowId,
      freeSlots[0].data, // any free slot, why not the first
      pathParameters.accountId
    ),
    createAccountAggregate(
      sot,
      sub,
      mode,
      flowId,
      pathParameters.accountId,
      groupId,
      freeSlots[0].data.groupType,
      accountProfile
    ),
    sendMail(flowId, accountPreferences.verifiedEmail.email, accountPreferences.language, authMethod)
  ])
  simpleLog.info(module, 'claimSlot', flowId, 'SUCCESS', { sot, sub, mode, pathParameters })
})

module.exports = put
