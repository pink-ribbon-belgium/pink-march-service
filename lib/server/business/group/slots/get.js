/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const requiredGroupSlots = require('../../../../api/group/GroupSlots').schema.required()
const ResourceActionContract = require('../../../ResourceActionContract')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const boom = require('@hapi/boom')
const Joi = require('@hapi/joi')
const UUID = require('../../../../ppwcode/UUID')
const GroupTypePathParameter = require('../../../../api/group/GroupTypePathParameter')
const requiredPathParametersSchema = Joi.object()
  .keys({
    groupId: UUID.schema.required(),
    groupType: GroupTypePathParameter.schema.optional()
  })
  .required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredGroupSlots, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function getGroupSlots (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }
  let [takenSlots, availableSlots] = await Promise.allSettled([
    // IDEA consider doing a count on dynamodb, instead of getting the actual items
    getByIndex(mode, `/${mode}/group/${pathParameters.groupId}/members`, index, 'actual'),
    getByIndex(mode, `/${mode}/group/${pathParameters.groupId}/free`, index, 'actual')
  ])

  if (!takenSlots.value && !availableSlots.value) {
    throw boom.notFound()
  }

  takenSlots = takenSlots.value ? takenSlots.value.length : 0
  availableSlots = availableSlots.value ? availableSlots.value.length : 0

  return {
    totalSlots: takenSlots + availableSlots,
    takenSlots: takenSlots,
    availableSlots: availableSlots
  }
})

module.exports = get
