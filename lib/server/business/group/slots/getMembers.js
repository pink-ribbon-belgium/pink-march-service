/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const getActual = require('../../../aws/dynamodb/getActual')
const Joi = require('@hapi/joi')
const UUID = require('../../../../ppwcode/UUID')
const AccountIdSchema = require('../../../../api/account/AccountId')

const GroupPathParams = Joi.object().keys({ groupId: UUID.schema.required() })
const requiredPathParametersSchema = GroupPathParams.required()
const outputSchema = Joi.array().items(
  Joi.object().keys({
    accountId: AccountIdSchema.schema.required(),
    name: Joi.string(),
    subgroupId: UUID.schema.optional().allow(null)
  })
)

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameter, response) {
      return validateSchema(outputSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function getMembers (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const memberIndexPartitionKey = `/${mode}/group/${pathParameters.groupId}/members`
  const memberSlots = await getByIndex(mode, memberIndexPartitionKey, index, 'actual')

  const membersInfo = await Promise.all(
    memberSlots.map(slot => getActual(mode, `/${mode}/account/${slot.data.accountId}/publicProfile`))
  )

  const mappedMemberInfo = membersInfo.map((profile, index) => ({
    ...profile.data,
    subgroupId: memberSlots[index].data.subgroupId
  }))

  return mappedMemberInfo.map(i => ({
    accountId: i.accountId,
    name: `${i.lastName} ${i.firstName}`,
    subgroupId: i.subgroupId || null
  }))
})

module.exports = get
