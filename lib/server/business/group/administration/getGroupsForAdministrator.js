/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const GroupAdministration = require('./GroupAdministration')
const GroupAdministrationDTO = require('../../../../api/group/GroupAdministrationDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const upgrade = require('../_upgradeWithGroupType')

const requiredGroupAdministrationSchema = GroupAdministrationDTO.schema.tailor('output').required()
const requiredPathParams = AccountPathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(ga => validateSchema(requiredGroupAdministrationSchema, ga))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)

  const key = `/${mode}/account/${pathParameters.accountId}/administratorOf`
  const indexA = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }
  const results = await getByIndex(mode, key, indexA, 'actual')
  const upgraded = await upgrade(sot, sub, mode, flowId, results)

  const groupAdministrations = []

  upgraded.forEach(item => {
    const groupAdmin = new GroupAdministration({
      mode,
      dto: {
        ...item.data
      }
    })
    const groupAdministrationJson = groupAdmin.toJSON()
    groupAdministrationJson.links = groupAdmin.getLinks()
    groupAdministrations.push(groupAdministrationJson)
  })

  return groupAdministrations
})

module.exports = get
