/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const GroupAdministration = require('./GroupAdministration')
const GroupAdministrationDTO = require('../../../../api/group/GroupAdministrationDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const TeamParams = require('../../../../api/team/TeamPathParameters')
const CompanyParams = require('../../../../api/company/CompanyPathParameters')

const requiredPathParametersSchema = Joi.alternatives(TeamParams.schema, CompanyParams.schema)

const requiredGroupAdministrationSchema = GroupAdministrationDTO.schema.tailor('output').required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(ga => validateSchema(requiredGroupAdministrationSchema, ga))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  // TODO replace key with ...{groupType}/{...id}
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  const key = `/${mode}/group/${pathParameters.teamId || pathParameters.companyId}/administrators`
  const indexB = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }
  const dtoPromise = getByIndex(mode, key, indexB)
  const result = await dtoPromise

  /* MUDO Requires backward compatible measure like getGroupsForAdministrator;
          Note done yet, because this function IS NOT USED currently. */

  const groupAdministrations = []

  result.forEach(item => {
    const groupAdmin = new GroupAdministration({
      mode,
      dto: {
        ...item.data
      }
    })
    const groupAdministrationJson = groupAdmin.toJSON()
    groupAdministrationJson.links = groupAdmin.getLinks()
    groupAdministrations.push(groupAdministrationJson)
  })

  return groupAdministrations
})

module.exports = get
