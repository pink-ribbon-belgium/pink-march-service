/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const putTransactionDynamodb = require('../../../aws/dynamodb/putTransaction')
const GroupAdministration = require('./GroupAdministration')
const GroupAdministrationDTO = require('../../../../api/group/GroupAdministrationDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const GroupAdminParams = require('../../../../api/group/GroupAdministrationPathParameters')
const { startCase } = require('lodash/string')

const requiredPathParametersSchema = GroupAdminParams.schema
const requiredInputSchema = GroupAdministrationDTO.schema.tailor('input').required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  validateInputSchema(requiredInputSchema, body)

  const groupAdministration = new GroupAdministration({
    mode,
    /* NOTE: SECURITY: createdAt and createdBy are audit data. External users can add unknown
     *                  properties to the PUT body. We must take care that an external user's
     *                  `createdAt` and 'createdBy` does not overwrite the true values we put in. */
    dto: {
      ...body,
      createdAt: sot,
      createdBy: sub,
      structureVersion: 2,
      accountId: pathParameters.accountId,
      groupType: startCase(pathParameters.groupType),
      groupId: pathParameters.groupId
    },
    sot,
    sub
  })

  const originalItem = {
    ...groupAdministration.toItem(),
    flowId
  }

  return putTransactionDynamodb([
    originalItem,
    {
      ...originalItem,
      submitted: 'actual',
      sort_key_A: 'actual',
      sort_key_B: 'actual',
      sort_key_C: 'actual'
    }
  ])
})

module.exports = put
