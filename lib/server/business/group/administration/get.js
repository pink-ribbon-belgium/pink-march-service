/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getActual = require('../../../aws/dynamodb/getActual')
const GroupAdministration = require('./GroupAdministration')
const validateSchema = require('../../../../_util/validateSchema')
const GroupAdministrationDTO = require('../../../../api/group/GroupAdministrationDTO')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const GroupAdminPathParams = require('../../../../api/group/GroupAdministrationPathParameters')
const Boom = require('@hapi/boom')
const { startCase } = require('lodash/string')

const requiredPathParametersSchema = GroupAdminPathParams.schema.required()
const requiredGroupAdministrationSchema = GroupAdministrationDTO.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.groupType.toLowerCase() === pathParameters.groupType
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.groupId === pathParameters.groupId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredGroupAdministrationSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function (sot, sub, mode, flowId, pathParameters) {
  // TODO replace key with ...{groupType}/{...id}
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  const key = `/${mode}/groupAdministration/account/${pathParameters.accountId}/group/${pathParameters.groupId}`
  const result = await getActual(mode, key)

  // be backward compatible: in structure version 1, there was no groupType
  if (result.data.structureVersion < 2) {
    result.data.structureVersion = 2
    result.data.groupType = startCase(pathParameters.groupType)
  }

  if (result.data.groupType.toLowerCase() !== pathParameters.groupType) {
    throw Boom.notFound()
  }

  const groupAdministration = new GroupAdministration({
    mode,
    dto: {
      ...result.data
    }
  })

  const groupAdministrationJson = groupAdministration.toJSON()

  groupAdministrationJson.links = groupAdministration.getLinks()
  return groupAdministrationJson
})

module.exports = get
