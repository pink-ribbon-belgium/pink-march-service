/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const GroupAdminDTO = require('../../../../api/group/GroupAdministrationDTO')
const AccountId = require('../../../../api/account/AccountId')
const UUID = require('../../../../ppwcode/UUID')
const GroupType = require('../../../../api/group/GroupType')

const requiredAccountId = AccountId.schema.required()
const requiredGroupId = UUID.schema.required()
const requiredGroupType = GroupType.schema.required()

const dynamodbDTO = GroupAdminDTO.schema.tailor('dynamodb').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const GroupAdminContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.groupId === kwargs.dto.groupId
    },
    function (kwargs) {
      return this.deleted === kwargs.dto.deleted
    }
  ])
})

GroupAdminContract.Kwargs = Kwargs

GroupAdminContract.methods = { ...PersistentObject.contract.methods }

GroupAdminContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return this.structureVersion >= 2
  },
  function () {
    return validateSchema(requiredAccountId, this.accountId)
  },
  function () {
    return validateSchema(requiredGroupType, this.groupType)
  },
  function () {
    return validateSchema(requiredGroupId, this.groupId)
  },
  function () {
    return typeof this.deleted === 'boolean'
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return this.toJSON().groupId === this.groupId
  },
  function () {
    return this.toJSON().deleted === this.deleted
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return Object.keys(this.getLinks()).includes('account')
  },
  function () {
    return Object.keys(this.getLinks()).includes('group')
  }
])

function GroupAdminImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.groupType = kwargs.dto.groupType
  this.groupId = kwargs.dto.groupId
  this.deleted = kwargs.dto.deleted
}

GroupAdminImpl.prototype = Object.create(PersistentObject.prototype)
GroupAdminImpl.prototype.constructor = GroupAdminImpl

GroupAdminImpl.prototype.accountId = undefined

GroupAdminImpl.prototype.groupId = undefined

GroupAdminImpl.prototype.deleted = undefined

GroupAdminImpl.prototype.groupType = undefined
GroupAdminImpl.prototype.structureVersion = 2

GroupAdminImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    accountId: this.accountId,
    groupType: this.groupType,
    groupId: this.groupId,
    deleted: this.deleted
  }
}

GroupAdminImpl.prototype.getKey = function () {
  return GroupAdmin.getKey({ id: this.groupId, accountId: this.accountId, mode: this.mode })
}

GroupAdminImpl.prototype.getLinks = function () {
  return {
    account: `/I/account/${this.accountId}`,
    group: `/I/${this.groupType.toLowerCase()}/${this.groupId}`
  }
}

GroupAdminImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/account/${this.accountId}/administratorOf`,
    B: `/${this.mode}/group/${this.groupId}/administrators`,
    C: `/${this.mode}/groupAdministration`
  }
}

GroupAdminImpl.prototype.invariants = GroupAdminContract.invariants

const GroupAdmin = GroupAdminContract.implementation(GroupAdminImpl)

GroupAdmin.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs.keys({ accountId: requiredAccountId }), kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/groupAdministration/account/${kwargs.accountId}/group/${kwargs.id}`
})

module.exports = GroupAdmin
