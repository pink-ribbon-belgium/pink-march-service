/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const getTeam = require('../team/get')
const getCompany = require('../company/get')

/**
 * Add `groupType` to all `results`, and set `structureVersion` to 2.
 *
 * This is a quick fix function, used to make get backward compatible with previous versions stored in persistent
 * storage. Whatever the `item.data` type, we add the `groupType` to it, and set `structureVersion` to 2.
 * The group type to add is determined by verifying for which group type the `item.data.groupId` returns an Item.
 *
 * Items for which no group could be found for `item.data.groupId` are removed from `results`.
 *
 * Since this a quick-fix function, it is only indirectly tested.
 */
async function upgrade (sot, sub, mode, flowId, results) {
  async function exists (getter, pathParams, groupType) {
    try {
      await getter(sot, sub, mode, flowId, pathParams)
      return groupType
    } catch (exc) {
      /* istanbul ignore else : this is a quick fix, we are not heroic on coverage */
      if (exc.isBoom && exc.output.statusCode === 404) {
        return false
      }
      /* istanbul ignore next : this is a quick fix, we are not heroic on coverage */
      throw exc
    }
  }

  const upgraded = await Promise.all(
    results.map(async item => {
      // be backward compatible: in structure version 1, there was no groupType
      if (item.data.structureVersion < 2) {
        item.data.structureVersion = 2
        const [team, company] = await Promise.all([
          exists(getTeam, { teamId: item.data.groupId }, 'Team'),
          exists(getCompany, { companyId: item.data.groupId }, 'Company')
        ])
        item.data.groupType = team || company
        if (!item.data.groupType) {
          // skip items for which no group could be found
          return undefined
        }
      }
      return item
    })
  )
  return upgraded.filter(item => !!item)
}

module.exports = upgrade
