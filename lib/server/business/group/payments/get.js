/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const Payment = require('../../payment/Payment')
const PaymentDTO = require('../../../../api/payment/PaymentDTO')
const validateSchema = require('../../../../_util/validateSchema')
const Joi = require('@hapi/joi')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const TeamParams = require('../../../../api/team/TeamPathParameters')
const CompanyParams = require('../../../../api/company/CompanyPathParameters')

const requiredPathParameters = Joi.alternatives(TeamParams.schema, CompanyParams.schema)
const requiredPaymentSchema = PaymentDTO.schema.tailor('output').required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(payment => payment.groupId === (pathParameters.teamId || pathParameters.companyId))
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(Joi.array().items(requiredPaymentSchema), response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParameters, pathParameters)
  const groupId = pathParameters.teamId || pathParameters.companyId
  const key = `/${mode}/${pathParameters.teamId ? 'team' : 'company'}/${groupId}/payments`
  const index = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }
  const dtoPromise = getByIndex(mode, key, index)
  const result = await dtoPromise

  const payments = result.map(
    item =>
      new Payment({
        mode,
        dto: item.data
      })
  )

  return payments.map(payment => ({
    ...payment.toJSON(),
    links: payment.getLinks()
  }))
})

module.exports = get
