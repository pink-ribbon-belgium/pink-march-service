/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const AccountAggregate = require('../../../../server/business/aggregates/account/AccountAggregate')

const getAll = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId) {
  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }
  const key = `/${mode}/accountAggregate`
  const result = await getByIndex(mode, key, index)

  const aggregates = []

  result.map(item => {
    // Quick & dirty fix to avoid errors when getting all account aggregates for writing statistics
    // This issue can occure when the accountAggregate is updated by the streamhandler
    /* istanbul ignore next */
    if (item.data.totalDistance < 0) {
      item.data.totalDistance = 0
    }

    const aggregate = new AccountAggregate({
      mode,
      dto: {
        ...item.data
      }
    })

    const aggregateJson = aggregate.toJSON()
    aggregateJson.links = aggregate.getLinks()
    aggregates.push(aggregateJson)
  })

  return aggregates
})

module.exports = getAll
