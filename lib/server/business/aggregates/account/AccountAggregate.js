/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const AccountId = require('../../../../api/account/AccountId')
const AccountAggregateDTO = require('../../../../api/aggregates/account/AccountAggregateDTO')

const requiredAccountId = AccountId.schema.required()

const dynamodbDTO = AccountAggregateDTO.schema.tailor('input').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const AccountAggregateContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.totalSteps === kwargs.dto.totalSteps
    },
    function (kwargs) {
      return this.totalDistance === kwargs.dto.totalDistance
    },
    function (kwargs) {
      return this.level === kwargs.dto.level
    },
    function (kwargs) {
      return this.previousLevel === kwargs.dto.previousLevel
    },
    function (kwargs) {
      return this.newLevel === kwargs.dto.newLevel
    },
    function (kwargs) {
      return this.stepsToNextLevel === kwargs.dto.stepsToNextLevel
    },
    function (kwargs) {
      return this.groupId === kwargs.dto.groupId
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.subGroupId === kwargs.dto.subGroupId
    },
    function (kwargs) {
      return this.name === kwargs.dto.name
    },
    function (kwargs) {
      return this.rank === kwargs.dto.rank
    },
    function (kwargs) {
      return this.acknowledged === kwargs.dto.acknowledged
    },
    function (kwargs) {
      return this.totalParticipants === kwargs.dto.totalParticipants
    }
  ])
})

AccountAggregateContract.Kwargs = Kwargs
AccountAggregateContract.methods = { ...PersistentObject.contract.methods }

AccountAggregateContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredAccountId, this.accountId)
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().totalSteps === this.totalSteps
  },
  function () {
    return this.toJSON().totalDistance === this.totalDistance
  },
  function () {
    return this.toJSON().level === this.level
  },
  function () {
    return this.toJSON().previousLevel === this.previousLevel
  },
  function () {
    return this.toJSON().newLevel === this.newLevel
  },
  function () {
    return this.toJSON().stepsToNextLevel === this.stepsToNextLevel
  },
  function () {
    return this.toJSON().groupId === this.groupId
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return this.toJSON().subGroupId === this.subGroupId
  },
  function () {
    return this.toJSON().name === this.name
  },
  function () {
    return this.toJSON().rank === this.rank
  },
  function () {
    return this.toJSON().totalParticipants === this.totalParticipants
  },
  function () {
    return this.toJSON().acknowledged === this.acknowledged
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return this.getKey() === `/${this.mode}/accountAggregate/${this.accountId}`
  },
  function () {
    return Object.keys(this.getLinks()).includes('account')
  },
  function () {
    return this.getLinks().account === `/I/account/${this.accountId}`
  },
  function () {
    return Object.keys(this.getLinks()).includes('accountProfile')
  },
  function () {
    return this.getLinks().accountProfile === `/I/account/${this.accountId}/publicProfile`
  }
])

function AccountAggregateImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.totalSteps = kwargs.dto.totalSteps
  this.totalDistance = kwargs.dto.totalDistance
  this.level = kwargs.dto.level
  this.previousLevel = kwargs.dto.previousLevel
  this.newLevel = kwargs.dto.newLevel
  this.stepsToNextLevel = kwargs.dto.stepsToNextLevel
  this.groupId = kwargs.dto.groupId
  this.groupType = kwargs.dto.groupType
  this.subGroupId = kwargs.dto.subGroupId
  this.name = kwargs.dto.name
  this.rank = kwargs.dto.rank
  this.totalParticipants = kwargs.dto.totalParticipants
  this.acknowledged = kwargs.dto.acknowledged
}

AccountAggregateImpl.prototype = Object.create(PersistentObject.prototype)
AccountAggregateImpl.prototype.constructor = AccountAggregateImpl

AccountAggregateImpl.prototype.structureVersion = 1

AccountAggregateImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    accountId: this.accountId,
    totalSteps: this.totalSteps,
    totalDistance: this.totalDistance,
    level: this.level,
    previousLevel: this.previousLevel,
    newLevel: this.newLevel,
    stepsToNextLevel: this.stepsToNextLevel,
    groupId: this.groupId,
    groupType: this.groupType,
    subGroupId: this.subGroupId,
    name: this.name,
    rank: this.rank,
    totalParticipants: this.totalParticipants,
    acknowledged: this.acknowledged
  }
}

AccountAggregateImpl.prototype.getKey = function () {
  return AccountAggregate.getKey({ id: this.accountId, mode: this.mode })
}

AccountAggregateImpl.prototype.getIndexPartitionKeys = function () {
  return {
    N1: `/${this.mode}/accountAggregate`,
    N2: `/${this.mode}/accountAggregate/group/${this.groupId}`,
    N3: this.subGroupId ? `/${this.mode}/accountAggregate/subgroup/${this.subGroupId}` : undefined
  }
}

AccountAggregateImpl.prototype.getLinks = function () {
  return {
    account: `/I/account/${this.accountId}`,
    accountProfile: `/I/account/${this.accountId}/publicProfile`
  }
}
AccountAggregateImpl.prototype.invariants = AccountAggregateContract.invariants

const AccountAggregate = AccountAggregateContract.implementation(AccountAggregateImpl)

AccountAggregate.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/accountAggregate/${kwargs.id}`
})

module.exports = AccountAggregate
