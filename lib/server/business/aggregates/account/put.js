/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const accountAggregateDTO = require('../../../../api/aggregates/account/AccountAggregateDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const accountAggregatePathParameters = require('../../../../api/aggregates/account/AccountAggregatePathParameters')

const simpleLog = require('../../../../_util/simpleLog')

const dynamodb = require('../../../aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../../aws/dynamodb/dynamodbTableName')

const requiredPathParameters = accountAggregatePathParameters.schema.required()
const requiredInputSchema = accountAggregateDTO.schema.tailor('input')

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParameters, pathParameters)
  validateInputSchema(requiredInputSchema, body)

  // MUDO: Refactor this in the future
  // This is a quick workaround to fix the race-condition issues when acknowledging the new level
  // only previousLevel & acknowledged will be updated instead of the whole accountAggregate

  const accountAggregateKey = `/${mode}/accountAggregate/${pathParameters.accountId}`

  simpleLog.info(module, 'putAccountAggregate', flowId, 'START', {
    mode: mode,
    accountId: pathParameters.accountId,
    key: accountAggregateKey,
    input: body
  })

  const dynamodbInstance = await dynamodb()

  const params = {
    TableName: dynamodbTableName(mode),
    Key: {
      key: accountAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#previousLevel = :previousLevel, #data.#acknowledged = :acknowledged',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#previousLevel': 'previousLevel',
      '#acknowledged': 'acknowledged'
    },
    ExpressionAttributeValues: {
      ':previousLevel': body.previousLevel,
      ':acknowledged': body.acknowledged
    },
    ReturnValues: 'UPDATED_NEW'
  }

  try {
    const result = await dynamodbInstance.update(params).promise()

    simpleLog.info(module, 'putAccountAggregate', flowId, 'END', {
      mode: mode,
      accountId: pathParameters.accountId,
      key: accountAggregateKey,
      result: result.Attributes.data
    })
  } catch (e) {
    simpleLog.info(module, 'putAccountAggregate', flowId, 'FAILED', {
      mode: mode,
      accountId: pathParameters.accountId,
      key: accountAggregateKey,
      error: e
    })

    throw e
  }
})
module.exports = put
