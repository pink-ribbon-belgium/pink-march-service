/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const AccountAggregateDTO = require('../../../../api/aggregates/account/AccountAggregateDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountAggregate = require('./AccountAggregate')
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const Joi = require('@hapi/joi')
const UUID = require('../../../../ppwcode/UUID')

const requiredPathParams = Joi.object()
  .keys({
    subgroupId: UUID.schema.required()
  })
  .unknown(false)

const requiredOutputSchema = AccountAggregateDTO.schema.tailor('output')

const getAggregates = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(aggregate => validateSchema(requiredOutputSchema, aggregate))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  const index = {
    indexName: 'Index_3',
    partitionKey: 'partition_key_3',
    sortKey: 'sort_key_3'
  }
  const key = `/${mode}/accountAggregate/subgroup/${pathParameters.subgroupId}`
  const result = await getByIndex(mode, key, index)

  const aggregates = []

  result.map(item => {
    const aggregate = new AccountAggregate({
      mode,
      dto: {
        ...item.data,
        level: 1,
        stepsToNextLevel: 1
      }
    })

    const aggregateJson = aggregate.toJSON()
    aggregateJson.links = aggregate.getLinks()
    aggregates.push(aggregateJson)
  })

  return aggregates
})

module.exports = getAggregates
