/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const AccountAggregateDTO = require('../../../../api/aggregates/account/AccountAggregateDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountAggregatePathParameters = require('../../../../api/aggregates/account/AccountAggregatePathParameters')
const AccountAggregate = require('./AccountAggregate')
const getActual = require('../../../aws/dynamodb/getActual')
const activityCalculations = require('../../../../api/activity/ActivityCalculations')

const requiredAccountAggregateSchema = AccountAggregateDTO.schema.tailor('output').required()
const requiredPathParams = AccountAggregatePathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredAccountAggregateSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)

  const key = `/${mode}/accountAggregate/${pathParameters.accountId}`
  const result = await getActual(mode, key, true)

  const currentLevel = activityCalculations.calculateLevelFromSteps(result.data.totalSteps, 1)

  // Enrich the result from database with level & stepsToNextLevel
  let enrichedDto = {
    ...result.data,
    newLevel: false,
    level: currentLevel,
    stepsToNextLevel: activityCalculations.calculateStepsToNextLevel(result.data.totalSteps, currentLevel)
  }

  /* istanbul ignore else */
  if (currentLevel > result.data.previousLevel) {
    enrichedDto = {
      ...enrichedDto,
      newLevel: true,
      acknowledged: false
    }
  }

  const accountAggregate = new AccountAggregate({
    mode,
    dto: enrichedDto
  })
  const accountAggregateJson = accountAggregate.toJSON()
  accountAggregateJson.links = accountAggregate.getLinks()
  return accountAggregateJson
})
module.exports = get
