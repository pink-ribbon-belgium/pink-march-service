/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const TeamAggregateDTO = require('../../../../api/aggregates/team/TeamAggregateDTO')
const UUID = require('../../../../ppwcode/UUID')

const requiredUUID = UUID.schema.required()
const dynamodbDTO = TeamAggregateDTO.schema.tailor('dynamodb').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const TeamAggregateContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.teamAggregateId === kwargs.dto.teamAggregateId
    },
    function (kwargs) {
      return this.totalSteps === kwargs.dto.totalSteps
    },
    function (kwargs) {
      return this.totalDistance === kwargs.dto.totalDistance
    },
    function (kwargs) {
      return this.averageSteps === kwargs.dto.averageSteps
    },
    function (kwargs) {
      return this.averageDistance === kwargs.dto.averageDistance
    },
    function (kwargs) {
      return this.totalTeams === kwargs.dto.totalTeams
    },
    function (kwargs) {
      return this.companyId === kwargs.dto.companyId
    },
    function (kwargs) {
      return this.name === kwargs.dto.name
    },
    function (kwargs) {
      return this.rank === kwargs.dto.rank
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.extendedName === kwargs.dto.extendedName
    },
    function (kwargs) {
      return this.hasLogo === kwargs.dto.hasLogo
    }
  ])
})

TeamAggregateContract.Kwargs = Kwargs
TeamAggregateContract.methods = { ...PersistentObject.contract.methods }

TeamAggregateContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredUUID, this.teamAggregateId)
  },
  function () {
    return this.toJSON().teamAggregateId === this.teamAggregateId
  },
  function () {
    return this.toJSON().totalSteps === this.totalSteps
  },
  function () {
    return this.toJSON().totalDistance === this.totalDistance
  },
  function () {
    return this.toJSON().averageSteps === this.averageSteps
  },
  function () {
    return this.toJSON().averageDistance === this.averageDistance
  },
  function () {
    return this.toJSON().totalTeams === this.totalTeams
  },
  function () {
    return this.toJSON().companyId === this.companyId
  },
  function () {
    return this.toJSON().name === this.name
  },
  function () {
    return this.toJSON().rank === this.rank
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return this.toJSON().extendedName === this.extendedName
  },
  function () {
    return this.toJSON().hasLogo === this.hasLogo
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return this.getKey() === `/${this.mode}/teamAggregate/${this.teamAggregateId}`
  },
  function () {
    return this.getLinks().team === `/I/team/${this.teamAggregateId}`
  }
])

function TeamAggregateImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.teamAggregateId = kwargs.dto.teamAggregateId
  this.totalSteps = kwargs.dto.totalSteps
  this.totalDistance = kwargs.dto.totalDistance
  this.averageSteps = kwargs.dto.averageSteps
  this.averageDistance = kwargs.dto.averageDistance
  this.totalTeams = kwargs.dto.totalTeams
  this.companyId = kwargs.dto.companyId
  this.name = kwargs.dto.name
  this.rank = kwargs.dto.rank
  this.groupType = kwargs.dto.groupType
  this.extendedName = kwargs.dto.extendedName
  this.hasLogo = kwargs.dto.hasLogo
}

TeamAggregateImpl.prototype = Object.create(PersistentObject.prototype)
TeamAggregateImpl.prototype.constructor = TeamAggregateImpl

TeamAggregateImpl.prototype.structureVersion = 1

TeamAggregateImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    teamAggregateId: this.teamAggregateId,
    totalSteps: this.totalSteps,
    totalDistance: this.totalDistance,
    averageSteps: this.averageSteps,
    averageDistance: this.averageDistance,
    totalTeams: this.totalTeams,
    companyId: this.companyId,
    name: this.name,
    rank: this.rank,
    groupType: this.groupType,
    extendedName: this.extendedName,
    hasLogo: this.hasLogo
  }
}

TeamAggregateImpl.prototype.getKey = function () {
  return TeamAggregate.getKey({ id: this.teamAggregateId, mode: this.mode })
}

TeamAggregateImpl.prototype.getIndexPartitionKeys = function () {
  return {
    N1: `/${this.mode}/teamAggregate`,
    N2: this.companyId ? `/${this.mode}/teamAggregate/company/${this.companyId}` : undefined
  }
}

TeamAggregateImpl.prototype.getLinks = function () {
  return {
    team: `/I/team/${this.teamAggregateId}`
  }
}

TeamAggregateImpl.prototype.invariants = TeamAggregateContract.invariants

const TeamAggregate = TeamAggregateContract.implementation(TeamAggregateImpl)

TeamAggregate.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/teamAggregate/${kwargs.id}`
})

module.exports = TeamAggregate
