/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const TeamAggregateDTO = require('../../../../api/aggregates/team/TeamAggregateDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const TeamAggregate = require('./TeamAggregate')
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const Joi = require('@hapi/joi')
const UUID = require('../../../../ppwcode/UUID')

const requiredPathParams = Joi.object()
  .keys({
    companyId: UUID.schema.required()
  })
  .unknown(false)
const requiredOutputSchema = TeamAggregateDTO.schema.tailor('output')

const getSubgroupAggregates = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(aggregate => validateSchema(requiredOutputSchema, aggregate))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  const index = {
    indexName: 'Index_2',
    partitionKey: 'partition_key_2',
    sortKey: 'sort_key_2'
  }
  const key = `/${mode}/teamAggregate/company/${pathParameters.companyId}`
  const result = await getByIndex(mode, key, index)

  const aggregates = []

  result.map(item => {
    const aggregate = new TeamAggregate({
      mode,
      dto: {
        ...item.data
      }
    })
    const aggregateJson = aggregate.toJSON()
    aggregateJson.links = aggregate.getLinks()
    aggregates.push(aggregateJson)
  })

  return aggregates
})

module.exports = getSubgroupAggregates
