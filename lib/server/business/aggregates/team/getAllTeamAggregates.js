/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const TeamAggregateDTO = require('../../../../api/aggregates/team/TeamAggregateDTO')
const validateSchema = require('../../../../_util/validateSchema')
const TeamAggregate = require('./TeamAggregate')
const getByIndex = require('../../../aws/dynamodb/getByIndex')

const requiredOutputSchema = TeamAggregateDTO.schema.tailor('output')

const getAll = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, response) {
      return response.every(aggregate => validateSchema(requiredOutputSchema, aggregate))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId) {
  const index = {
    indexName: 'Index_1',
    partitionKey: 'partition_key_1',
    sortKey: 'sort_key_1'
  }
  const key = `/${mode}/teamAggregate`
  const result = await getByIndex(mode, key, index)

  const aggregates = []

  result.map(item => {
    const aggregate = new TeamAggregate({
      mode,
      dto: {
        ...item.data
      }
    })
    const aggregateJson = aggregate.toJSON()
    aggregateJson.links = aggregate.getLinks()
    aggregates.push(aggregateJson)
  })

  return aggregates
})

module.exports = getAll
