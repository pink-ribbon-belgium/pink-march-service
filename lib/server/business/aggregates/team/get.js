/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const TeamAggregateDTO = require('../../../../api/aggregates/team/TeamAggregateDTO')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const TeamAggregatePathParameters = require('../../../../api/aggregates/team/TeamAggregatePathParameters')
const TeamAggregate = require('./TeamAggregate')
const getActual = require('../../../aws/dynamodb/getActual')

const requiredTeamAggregateSchema = TeamAggregateDTO.schema.tailor('output').required()
const requiredPathParams = TeamAggregatePathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.teamAggregateId === pathParameters.teamAggregateId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredTeamAggregateSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)

  const key = `/${mode}/teamAggregate/${pathParameters.teamAggregateId}`
  const result = await getActual(mode, key, true)

  const teamAggregate = new TeamAggregate({
    mode,
    dto: result.data
  })
  const teamAggregateJson = teamAggregate.toJSON()
  teamAggregateJson.links = teamAggregate.getLinks()
  return teamAggregateJson
})
module.exports = get
