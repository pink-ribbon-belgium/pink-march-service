/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const UUID = require('../../../../ppwcode/UUID')
const CompanyAggregateDTO = require('../../../../api/aggregates/company/CompanyAggregateDTO')

const requiredUUIDSchema = UUID.schema.required()

const dynamodbDTO = CompanyAggregateDTO.schema.tailor('input').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const CompanyAggregateContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.companyId === kwargs.dto.companyId
    },
    function (kwargs) {
      return this.totalSteps === kwargs.dto.totalSteps
    },
    function (kwargs) {
      return this.totalDistance === kwargs.dto.totalDistance
    },
    function (kwargs) {
      return this.averageSteps === kwargs.dto.averageSteps
    },
    function (kwargs) {
      return this.averageDistance === kwargs.dto.averageDistance
    },
    function (kwargs) {
      return this.unit === kwargs.dto.unit
    },
    function (kwargs) {
      return this.name === kwargs.dto.name
    },
    function (kwargs) {
      return this.extendedName === kwargs.dto.extendedName
    },
    function (kwargs) {
      return this.hasLogo === kwargs.dto.hasLogo
    },
    function (kwargs) {
      return this.rank === kwargs.dto.rank
    },
    function (kwargs) {
      return this.totalParticipants === kwargs.dto.totalParticipants
    }
  ])
})

CompanyAggregateContract.Kwargs = Kwargs
CompanyAggregateContract.methods = { ...PersistentObject.contract.methods }

CompanyAggregateContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredUUIDSchema, this.companyId)
  },
  function () {
    return this.toJSON().companyId === this.companyId
  },
  function () {
    return this.toJSON().totalSteps === this.totalSteps
  },
  function () {
    return this.toJSON().totalDistance === this.totalDistance
  },
  function () {
    return this.toJSON().averageSteps === this.averageSteps
  },
  function () {
    return this.toJSON().averageDistance === this.averageDistance
  },
  function () {
    return this.toJSON().unit === this.unit
  },
  function () {
    return this.toJSON().name === this.name
  },
  function () {
    return this.toJSON().extendedName === this.extendedName
  },
  function () {
    return this.toJSON().hasLogo === this.hasLogo
  },
  function () {
    return this.toJSON().rank === this.rank
  },
  function () {
    return this.toJSON().totalParticipants === this.totalParticipants
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return this.getKey() === `/${this.mode}/companyAggregate/${this.companyId}`
  },
  function () {
    return Object.keys(this.getLinks()).includes('company')
  },
  function () {
    return this.getLinks().company === `/I/company/${this.companyId}`
  }
])

function CompanyAggregateImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.companyId = kwargs.dto.companyId
  this.totalSteps = kwargs.dto.totalSteps
  this.totalDistance = kwargs.dto.totalDistance
  this.averageSteps = kwargs.dto.averageSteps
  this.averageDistance = kwargs.dto.averageDistance
  this.unit = kwargs.dto.unit
  this.name = kwargs.dto.name
  this.extendedName = kwargs.dto.extendedName
  this.hasLogo = kwargs.dto.hasLogo
  this.rank = kwargs.dto.rank
  this.totalCompanies = kwargs.dto.totalCompanies
}

CompanyAggregateImpl.prototype = Object.create(PersistentObject.prototype)
CompanyAggregateImpl.prototype.constructor = CompanyAggregateImpl

CompanyAggregateImpl.prototype.structureVersion = 1

CompanyAggregateImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    companyId: this.companyId,
    totalSteps: this.totalSteps,
    totalDistance: this.totalDistance,
    averageSteps: this.averageSteps,
    averageDistance: this.averageDistance,
    unit: this.unit,
    name: this.name,
    extendedName: this.extendedName,
    hasLogo: this.hasLogo,
    rank: this.rank,
    totalCompanies: this.totalCompanies
  }
}

CompanyAggregateImpl.prototype.getKey = function () {
  return CompanyAggregate.getKey({ id: this.companyId, mode: this.mode })
}

CompanyAggregateImpl.prototype.getIndexPartitionKeys = function () {
  return {
    N1: `/${this.mode}/companyAggregate`,
    N2: this.unit ? `/${this.mode}/companyAggregate/unit/${this.unit}` : undefined
  }
}

CompanyAggregateImpl.prototype.getLinks = function () {
  return {
    company: `/I/company/${this.companyId}`
  }
}
CompanyAggregateImpl.prototype.invariants = CompanyAggregateContract.invariants

const CompanyAggregate = CompanyAggregateContract.implementation(CompanyAggregateImpl)

CompanyAggregate.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/companyAggregate/${kwargs.id}`
})

module.exports = CompanyAggregate
