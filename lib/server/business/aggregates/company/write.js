/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const putTransaction = require('../../../aws/dynamodb/putTransaction')
const CompanyAggregate = require('./CompanyAggregate')
const companyAggregateDTO = require('../../../../api/aggregates/company/CompanyAggregateDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise

const AccountId = require('../../../../api/account/AccountId')
const Mode = require('../../../../ppwcode/Mode')
const Uuid = require('../../../../ppwcode/UUID')
const PointInTime = require('../../../../ppwcode/PointInTime')

const validateSchema = require('../../../../_util/validateSchema')

const requiredInputSchema = companyAggregateDTO.schema.tailor('input')

const requiredSot = PointInTime.schema.required().label('sot')
const requiredSub = AccountId.schema.required().label('sub')
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = Uuid.schema.required().label('flowId')

const write = new PromiseContract({
  pre: [
    sot => validateSchema(requiredSot, sot),
    (sot, sub) => validateSchema(requiredSub, sub),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, accountAggregateDataDTO) => validateSchema(requiredInputSchema, accountAggregateDataDTO)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function write (sot, sub, mode, flowId, companyAggregateDataDTO) {
  // Ensure that level & stepsToNextLevel are not saved in to the database

  const itemData = {
    ...companyAggregateDataDTO
  }

  const accountAggregate = new CompanyAggregate({
    mode,
    /* NOTE: SECURITY: createdAt and createdBy are audit data. External users can add unknown
     *                  properties to the PUT body. We must take care that an external user's
     *                  `createdAt` and 'createdBy` does not overwrite the true values we put in. */
    dto: {
      ...itemData,
      createAt: sot,
      createdBy: sub
    },
    sot,
    sub
  })
  const item = {
    ...accountAggregate.toItem(),
    flowId
  }
  const submittedItem = {
    ...item,
    partition_key_1: undefined,
    sort_key_1: undefined,
    partition_key_2: undefined,
    sort_key_2: undefined,
    partition_key_3: undefined,
    sort_key_3: undefined
  }

  const actualItem = {
    ...item,
    submitted: 'actual',
    sort_key_1: companyAggregateDataDTO.averageSteps,
    sort_key_2: companyAggregateDataDTO.averageSteps,
    sort_key_3: undefined
  }
  return putTransaction([submittedItem, actualItem])
})

module.exports = write
