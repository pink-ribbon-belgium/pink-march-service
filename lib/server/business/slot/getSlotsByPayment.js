/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../../ppwcode/UUID')
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../aws/dynamodb/getByIndex')
const SlotDTO = require('../../../api/slot/SlotDTO')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const upgrade = require('../group/_upgradeWithGroupType')

const requiredSlotSchema = SlotDTO.schema.tailor('dynamodb').required()
const requiredPathParams = Joi.object().keys({ paymentId: UUID.schema.required() })

/* TODO Only used internally at this time; should be a more simple internal method. Only used to ensure that there are
        no slots yet for a payment, before processing it. The internal method should do just that. Furthermore,
        these semantics are debatable. Should the determination not be that the payment should not be processed yet? */
const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(item => validateSchema(requiredSlotSchema, item))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  const key = `/${mode}/payment/${pathParameters.paymentId}/slots`
  const indexA = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  let result
  try {
    result = await getByIndex(mode, key, indexA, 'actual')
  } catch (e) {
    return []
  }

  const upgraded = await upgrade(sot, sub, mode, flowId, result)

  return upgraded.map(item => item.data)
})

module.exports = get
