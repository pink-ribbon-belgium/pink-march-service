/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const UUID = require('../../../ppwcode/UUID')
const PersistentObject = require('../../../server/business/PersistentObject')
const SlotDTO = require('../../../api/slot/SlotDTO')
const GroupType = require('../../../api/group/GroupType')

const requiredUUID = UUID.schema.required()
const requiredGroupType = GroupType.schema.required()

const dynamodbDTO = SlotDTO.schema.tailor('dynamodb').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const SlotContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.id === kwargs.dto.id
    },
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.groupId === kwargs.dto.groupId
    },
    function (kwargs) {
      return this.paymentId === kwargs.dto.paymentId
    },
    function (kwargs) {
      return this.subgroupId === kwargs.dto.subgroupId
    }
  ])
})

SlotContract.Kwargs = Kwargs

SlotContract.methods = { ...PersistentObject.contract.methods }

SlotContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return this.structureVersion >= 2
  },
  function () {
    return validateSchema(requiredUUID, this.id)
  },
  function () {
    return validateSchema(requiredGroupType, this.groupType)
  },
  function () {
    return validateSchema(requiredUUID, this.groupId)
  },
  function () {
    return validateSchema(requiredUUID, this.paymentId)
  },
  function () {
    return validateSchema(requiredUUID, this.subgroupId)
  },
  function () {
    return this.toJSON().id === this.id
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return this.toJSON().groupId === this.groupId
  },
  function () {
    return this.toJSON().paymentId === this.paymentId
  },
  function () {
    return this.toJSON().subgroupId === this.subgroupId
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return this.getIndexPartitionKeys().A === `/${this.mode}/payment/${this.paymentId}/slots`
  },
  function () {
    // explicitly uses 'group', and not a concrete type; see server/business/group/slots/get.js
    const pattern = new RegExp(`^\\/${this.mode}\\/group\\/${this.groupId}\\/(members|free)$`)
    return pattern.test(this.getIndexPartitionKeys().B)
  },
  function () {
    const ending = this.accountId ? 'members' : 'free'
    return this.getIndexPartitionKeys().B.endsWith(ending)
  },
  function () {
    return this.accountId || !this.getIndexPartitionKeys().C
  },
  function () {
    return !this.accountId || this.getIndexPartitionKeys().C === `/${this.mode}/account/${this.accountId}/slot`
  },
  function () {
    return this.getIndexPartitionKeys().D === `/${this.mode}/slots`
  },
  function () {
    if (this.accountId === undefined) {
      return this.getIndexPartitionKeys().E === `/${this.mode}/slots/free`
    } else {
      return this.getIndexPartitionKeys().E === `/${this.mode}/slots/members`
    }
  },

  function () {
    if (this.accountId === undefined) {
      return true
    } else {
      return Object.keys(this.getLinks()).includes('account')
    }
  },
  function () {
    if (this.accountId === undefined) {
      return true
    } else {
      return Object.keys(this.getLinks()).includes('accountProfile')
    }
  },
  function () {
    return Object.keys(this.getLinks()).includes('payment')
  },
  function () {
    return Object.keys(this.getLinks()).includes('group')
  }
])

function SlotImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.id = kwargs.dto.id
  this.accountId = kwargs.dto.accountId
  this.groupType = kwargs.dto.groupType
  this.groupId = kwargs.dto.groupId
  this.paymentId = kwargs.dto.paymentId
  this.subgroupId = kwargs.dto.subgroupId
}

SlotImpl.prototype = Object.create(PersistentObject.prototype)
SlotImpl.prototype.constructor = SlotImpl

SlotImpl.prototype.structureVersion = 2
SlotImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    id: this.id,
    accountId: this.accountId,
    groupId: this.groupId,
    groupType: this.groupType,
    paymentId: this.paymentId,
    subgroupId: this.subgroupId
  }
}

SlotImpl.prototype.getKey = function () {
  return Slot.getKey({ id: this.groupId, slotId: this.id, mode: this.mode })
}

/* TODO It would be more logical to only add these indices to the `actual` item, and to use the sort key for semantic
        relevant sorting and paging when getting a list. */
SlotImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/payment/${this.paymentId}/slots`,
    B: `/${this.mode}/group/${this.groupId}/${this.accountId ? 'members' : 'free'}`,
    C: this.accountId && `/${this.mode}/account/${this.accountId}/slot`,
    D: `/${this.mode}/slots`,
    E: `/${this.mode}/slots/${this.accountId ? 'members' : 'free'}`
  }
}

SlotImpl.prototype.getLinks = function () {
  if (this.accountId === undefined) {
    return {
      payment: `/I/payment/${this.paymentId}`,
      group: `/I/${this.groupType.toLowerCase()}/${this.groupId}`
    }
  } else {
    return {
      account: `/I/account/${this.accountId}`,
      accountProfile: `/I/account/${this.accountId}/publicProfile`,
      payment: `/I/payment/${this.paymentId}`,
      group: `/I/${this.groupType.toLowerCase()}/${this.groupId}`
    }
  }
}

SlotImpl.prototype.invariants = SlotContract.invariants

const Slot = SlotContract.implementation(SlotImpl)

Slot.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs.keys({ slotId: requiredUUID }), kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/group/${kwargs.id}/slot/${kwargs.slotId}`
})

module.exports = Slot
