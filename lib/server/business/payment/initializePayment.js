/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const config = require('config')
const { v4: uuidv4 } = require('uuid')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateSchema = require('../../../_util/validateSchema')
const Payment = require('./Payment')
const putPayment = require('./put')
const calculation = require('./calculation/calculation')
const getVoucher = require('../voucher/get')
const Joi = require('@hapi/joi')
const Ogone = require('../../../ppwcode/ogone/Ogone')
const UUID = require('../../../ppwcode/UUID')
const validateInputSchema = require('../../../_util/validateInputSchema')
const TeamParams = require('../../../api/team/TeamPathParameters')
const CompanyParams = require('../../../api/company/CompanyPathParameters')
const PaymentStatus = require('../../../api/payment/PaymentStatus')

const requiredPathParams = Joi.alternatives(TeamParams.schema, CompanyParams.schema)

const Response = Joi.object().keys({
  postUrl: Joi.string().uri(),
  paymentId: UUID.schema.required()
})

/**
 * @param {string} merchantId
 * @param {string} paymentDetails
 */
const initializePaymentContract = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, body, result) {
      return validateSchema(Response, result)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function initializePayment (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParams, pathParameters)
  const isProduction = mode === 'production'
  /* prettier-ignore */
  const ogoneConfig = isProduction
    ? /* istanbul ignore next : we do not test the production payment url currently; see TODO in test */ config.ogone.production
    : config.ogone.test
  let voucher
  // 1. Instantiate FinalPayment
  if (body.voucherCode) {
    try {
      voucher = await getVoucher(sot, sub, mode, flowId, { voucherCode: body.voucherCode })
    } catch (err) {
      // Voucher is not found so no discount
    }
  }

  const groupId = pathParameters.companyId || pathParameters.teamId
  const calculationParams = {
    numberOfParticipants: body.numberOfSlots,
    groupId: groupId,
    voucherCode: body.voucherCode
  }

  const amount = await calculation(sot, sub, mode, flowId, calculationParams, voucher)

  console.log(amount)

  const paymentId = uuidv4()
  // 2. Change status payment for customer
  // putPayment
  const initializePayment = new Payment({
    mode,
    dto: {
      createdAt: sot,
      createdBy: sub,
      structureVersion: body.structureVersion,
      paymentId: paymentId,
      paymentInitiatedAt: sot,
      groupId: groupId,
      groupType: body.groupType,
      voucherCode: body.voucherCode,
      numberOfSlots: body.numberOfSlots,
      amount: amount.total,
      paymentStatus: PaymentStatus.INITIALIZED
    }
  })

  await putPayment(sot, sub, mode, flowId, { paymentId }, initializePayment)

  const postUrl = Ogone.createPaymentUrl(
    {
      accepturl: body.redirectUrl,
      amount: amount.total,
      cancelurl: body.redirectUrl,
      currency: 'EUR',
      declineurl: body.redirectUrl,
      exceptionurl: body.redirectUrl,
      language: body.locale,
      orderId: paymentId,
      pspid: ogoneConfig.pspId
    },
    ogoneConfig
  )

  return {
    postUrl,
    paymentId
  }
})

module.exports = initializePaymentContract
