/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../../ppwcode/UUID')
const putDynamodb = require('../../aws/dynamodb/put')
const Payment = require('./Payment')
const PaymentDTO = require('../../../api/payment/PaymentDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateInputSchema = require('../../../_util/validateInputSchema')

const requiredPathParams = Joi.object().keys({ paymentId: UUID.schema.required() })
const requiredInputSchema = PaymentDTO.schema.tailor('input').required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParams, pathParameters)
  validateInputSchema(requiredInputSchema, body)
  const payment = new Payment({
    mode,

    /* NOTE: SECURITY: createdAt and createdBy are audit data. External users can add unknown
     *                  properties to the PUT body. We must take care that an external user's
     *                  `createdAt` and 'createdBy` does not overwrite the true values we put in. */
    dto: {
      ...body,
      createdAt: sot,
      createdBy: sub,
      paymentId: pathParameters.paymentId
    }
  })

  return putDynamodb({
    mode: payment.mode,
    key: payment.key,
    submitted: payment.createdAt,
    submittedBy: payment.createdBy,
    flowId,
    data: payment.toJSON(),
    partition_key_A: `/${mode}/voucher/${payment.voucherCode}/payments`,
    sort_key_A: payment.createdAt,
    partition_key_B: `/${mode}/status/${payment.paymentStatus}/payments`,
    sort_key_B: payment.createdAt,
    partition_key_C: `/${mode}/${body.groupType.toLowerCase()}/${body.groupId}/payments`,
    sort_key_C: payment.createdAt
  })
})

module.exports = put
