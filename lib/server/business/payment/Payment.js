/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const UUID = require('../../../ppwcode/UUID')
const PointInTime = require('../../../ppwcode/PointInTime')
const Mode = require('../../../ppwcode/Mode')
const PersistentObject = require('../../../server/business/PersistentObject')
const PaymentDTO = require('../../../api/payment/PaymentDTO')
const GroupType = require('../../../api/group/GroupType')
const Voucher = require('../../../api/voucher/VoucherCode')
const PaymentStatus = require('../../../api/payment/PaymentStatus')
const Joi = require('@hapi/joi')

const requiredUUID = UUID.schema.required()
const requiredPointInTime = PointInTime.schema.required()
const requiredMode = Mode.schema.required()
const requiredKeyKwargs = Joi.object()
  .keys({
    paymentId: requiredUUID,
    mode: requiredMode
  })
  .required()
const requiredGroupType = GroupType.schema.required()
const requiredPositiveNatural = Joi.number()
  .integer()
  .positive()
  .required()
const requiredPositiveOr0 = Joi.number()
  .positive()
  .allow(0)
  .required()

const dynamodbDTO = PaymentDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const PaymentContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.paymentId === kwargs.dto.paymentId
    },
    function (kwargs) {
      return this.paymentInitiatedAt === kwargs.dto.paymentInitiatedAt
    },
    function (kwargs) {
      return this.groupId === kwargs.dto.groupId
    },
    function (kwargs) {
      return this.groupType === kwargs.dto.groupType
    },
    function (kwargs) {
      return this.voucherCode === kwargs.dto.voucherCode
    },
    function (kwargs) {
      return this.numberOfSlots === kwargs.dto.numberOfSlots
    },
    function (kwargs) {
      return this.amount === kwargs.dto.amount
    },
    function (kwargs) {
      return this.paymentStatus === kwargs.dto.paymentStatus
    }
  ])
})

PaymentContract.Kwargs = Kwargs

PaymentContract.methods = { ...PersistentObject.contract.methods }

PaymentContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredUUID, this.paymentId)
  },
  function () {
    return validateSchema(requiredPointInTime, this.paymentInitiatedAt)
  },
  function () {
    return validateSchema(requiredUUID, this.groupId)
  },
  function () {
    return validateSchema(requiredGroupType, this.groupType)
  },
  function () {
    return validateSchema(Voucher.schema, this.voucherCode)
  },
  function () {
    return validateSchema(requiredPositiveNatural, this.numberOfSlots)
  },
  function () {
    return validateSchema(requiredPositiveOr0, this.amount)
  },
  function () {
    return validateSchema(PaymentStatus.schema, this.paymentStatus)
  },
  function () {
    return this.toJSON().paymentId === this.paymentId
  },
  function () {
    return this.toJSON().paymentInitiatedAt === this.paymentInitiatedAt
  },
  function () {
    return this.toJSON().groupId === this.groupId
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return this.toJSON().voucherCode === this.voucherCode
  },
  function () {
    return this.toJSON().numberOfSlots === this.numberOfSlots
  },
  function () {
    return this.toJSON().amount === this.amount
  },
  function () {
    return this.toJSON().paymentStatus === this.paymentStatus
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  }
])

function PaymentImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.paymentId = kwargs.dto.paymentId
  this.paymentInitiatedAt = kwargs.dto.paymentInitiatedAt
  this.groupId = kwargs.dto.groupId
  this.groupType = kwargs.dto.groupType
  this.voucherCode = kwargs.dto.voucherCode
  this.numberOfSlots = kwargs.dto.numberOfSlots
  this.amount = kwargs.dto.amount
  this.paymentStatus = kwargs.dto.paymentStatus
}

PaymentImpl.prototype = Object.create(PersistentObject.prototype)
PaymentImpl.prototype.constructor = PaymentImpl

PaymentImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    paymentId: this.paymentId,
    paymentInitiatedAt: this.paymentInitiatedAt,
    groupId: this.groupId,
    groupType: this.groupType,
    voucherCode: this.voucherCode,
    numberOfSlots: this.numberOfSlots,
    amount: this.amount,
    paymentStatus: this.paymentStatus
  }
}

PaymentImpl.prototype.getKey = function () {
  return Payment.getKey({ paymentId: this.paymentId, mode: this.mode })
}

PaymentImpl.prototype.getLinks = function () {
  return {
    versions: `/I/payment/${this.paymentId}/versions`,
    group: `/I/${this.groupType.toLowerCase()}/${this.groupId}`,
    voucher: `/I/voucher/${this.voucherCode}`
  }
}

PaymentImpl.prototype.invariants = PaymentContract.invariants

const Payment = PaymentContract.implementation(PaymentImpl)

Payment.getKey = new Contract({
  pre: [kwargs => validateSchema(requiredKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/payment/${kwargs.paymentId}`
})

module.exports = Payment
