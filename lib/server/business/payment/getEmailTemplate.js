/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv/').Promise
const yaml = require('js-yaml')
const path = require('path')
const fs = require('fs').promises
const Joi = require('@hapi/joi')
const validateSchema = require('../../../_util/validateSchema')
const EmailTemplate = require('../../../api/payment/templates/emailTemplate')

const regexp = /[0-9a-zA-Z_]+$/

const templatenNameSchema = Joi.string()
  .pattern(regexp)
  .required()
const languageSchema = Joi.string()
  .valid('NL', 'FR')
  .required()

const GetEmailTemplate = new PromiseContract({
  pre: [
    function (templateName) {
      return validateSchema(templatenNameSchema, templateName)
    },
    function (templateName, language) {
      return validateSchema(languageSchema, language)
    }
  ],
  post: [
    function (templateName, language, result) {
      return validateSchema(EmailTemplate.schema, result)
    }
  ]
}).implementation(async function getEmailTemplate (templateName, language) {
  const templateDir = path.join(__dirname, 'templates', language.toUpperCase())
  const absPath = path.format({ dir: templateDir, name: templateName, ext: '.yaml' })
  const contents = await fs.readFile(absPath)
  const result = yaml.safeLoad(contents, { filename: absPath })

  return result
})

module.exports = GetEmailTemplate
