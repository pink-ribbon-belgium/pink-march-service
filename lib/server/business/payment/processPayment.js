/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const sendGridMail = require('@sendgrid/mail')
const Joi = require('@hapi/joi')
const Boom = require('@hapi/boom')
const UUID = require('../../../ppwcode/UUID')
const AccountId = require('../../../api/account/AccountId')
const GroupType = require('../../../api/group/GroupType')
const AuthenticationType = require('../../../api/account/AuthenticationType')
const config = require('config')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const uuidv4 = require('uuid').v4
const getPayment = require('./get')
const putPayment = require('./put')
const ProcessPaymentResultDTO = require('../../../api/payment/ProcessPaymentResultDTO')
const validateSchema = require('../../../_util/validateSchema')
const Ogone = require('../../../ppwcode/ogone/Ogone')
const getSlotsByPaymentId = require('../slot/getSlotsByPayment')
const getPreferences = require('../account/preferences/get')
const getEmailTemplate = require('./getEmailTemplate')
const validateInputSchema = require('../../../_util/validateInputSchema')
const simpleLog = require('../../../_util/simpleLog')
const PaymentStatus = require('../../../api/payment/PaymentStatus')
const writeSlot = require('../slot/write')
const getSlotsForGroup = require('../group/slots/get')
const getAccountProfile = require('../account/profile/get')
const getGroupProfile = require('../../../server/business/group/profile/get')
const getCompany = require('../../../server/business/company/get')
const putCompany = require('../../../server/business/company/put')
const writeAccountAggregate = require('../aggregates/account/write')
const getAccountAggregate = require('../aggregates/account/get')
const writeTeamAggregate = require('../aggregates/team/write')
const getTeamAggregate = require('../aggregates/team/get')
const writeCompanyAggregate = require('../aggregates/company/write')
const getCompanyAggregate = require('../aggregates/company/get')
const getAccount = require('../account/get')
const getTeam = require('../team/get')
const putTeam = require('../team/put')
const createShortLink = require('../group/shortLink/createShortLink')

let existingSlotsForGroup

const requiredPathParams = Joi.object().keys({
  paymentId: UUID.schema.required(),
  accountId: AccountId.schema.required(),
  teamId: UUID.schema.optional(), //  Allow teamId/companyId => needed for authorization
  companyId: UUID.schema.optional()
})

// TODO seperate out these functions as internal functions, and test independently

/**
 * Check if slots already have been generated, but don't throw until after the payment has been processed.
 *
 * IDEA: These semantics are debatable. Should the determination not be that the payment should not be processed yet?
 */
async function getNoSlotsYet (sot, sub, mode, flowId, paymentId) {
  const slotsForPayment = await getSlotsByPaymentId(sot, sub, mode, flowId, { paymentId })
  const totalSlots = slotsForPayment.length

  return totalSlots === 0
}

async function getTotalSlotsForGroup (sot, sub, mode, flowId, payment) {
  try {
    const slotsForGroup = await getSlotsForGroup(sot, sub, mode, flowId, {
      groupId: payment.groupId
    })
    return slotsForGroup.totalSlots
  } catch (err) {
    if (!err.isBoom || err.output.statusCode !== 404) {
      throw err
    }
    return 0
  }
}

/*
async function shouldClaimSlotsForAdministrator (sot, sub, mode, flowId, payment) {
  if (payment.groupType.toLowerCase() === 'team') {
    try {
      const slotsForTeam = await getSlotsForGroup(sot, sub, mode, flowId, {
        groupId: payment.groupId
      })
      return slotsForTeam.totalSlots === 0
    } catch (err) {
      if (!err.isBoom || err.output.statusCode !== 404) {
        throw err
      }
      return true
    }
  }
  return false
}
*/

/*
 if claimSlotForAdministrator is true, accountProfile MUST be given otherwise it maybe undefined
 */
async function createSlots (
  sot,
  sub,
  mode,
  flowId,
  numberOfSlots,
  groupType,
  groupId,
  paymentId,
  accountId,
  claimSlotForAdministrator
) {
  let freeSlots = numberOfSlots
  // Generate slots for payment
  const slots = new Array(freeSlots).fill(undefined).map(() => ({
    structureVersion: 2,
    id: uuidv4(),
    groupType,
    groupId: groupId,
    paymentId: paymentId
    // free
  }))
  if (claimSlotForAdministrator) {
    slots[0].accountId = accountId
    freeSlots = numberOfSlots - 1
  }

  /* Do not use a transaction here for all at once. This can easily get too large.
     There does not seem to be a need for an overall transaction here. With a correct setup, there is little
     or no reason this would fail. If some of the separate writes would fail, we can still continue
     with the other slots. In a good implementation, we can retry the failed putTransactions again.
     For now (we are aiming for production), we implement separate putTransactions of the slot couples. If none fail,
     everything is ok. If all fail, this code will fail with a 500. If some fail, we log the failure, and report the
     correct number of free slots.
     This actually needs to be done async, via a stream / event queue, and be handled there. */
  const outcomes = await Promise.allSettled(slots.map(slot => writeSlot(sot, sub, mode, flowId, slot)))
  const errors = outcomes.filter(o => o.status === 'rejected')
  const successes = outcomes.filter(o => o.status === 'fulfilled')
  if (successes.length <= 0) {
    outcomes.forEach(o => {
      simpleLog.error(module, 'processPayment', flowId, o.reason)
    })
    throw Boom.badImplementation('failed to create any slot')
  }
  if (errors.length > 0) {
    simpleLog.info(module, 'processPayment', flowId, 'WRITING_SLOTS', {
      paymentId: paymentId,
      groupId: groupId,
      groupType: groupType,
      message: 'writing some slots failed',
      nrOfFailedSlotWrites: errors.length,
      nrOfSucceededSlotWrites: successes.length,
      errors
    }) // IDEA should be WARN, or a functional solution
    freeSlots -= errors.length
  }

  simpleLog.info(module, 'processPayment', flowId, 'WRITING_SLOTS_END', {
    paymentId: paymentId,
    groupId: groupId,
    groupType: groupType,
    message: 'writing some slots failed',
    nrOfFailedSlotWrites: errors.length,
    nrOfSucceededSlotWrites: successes.length,
    errors
  })
  return freeSlots
}

function getAuthMethod (account) {
  const split = account.sub.split('|')
  return split[0]
}

async function sendEmail (flowId, accountEmail, groupType, locale, shortLink, toolboxLink, auth) {
  let authMethod
  const localeSubstring = locale.substring(0, 2).toLowerCase()
  sendGridMail.setApiKey(config.email.sendgridApiKey)
  switch (auth) {
    case 'auth0':
      authMethod = AuthenticationType.auth0Type[localeSubstring]
      break
    case 'facebook':
      authMethod = AuthenticationType.facebookType[localeSubstring]
      break
    case 'google-oauth2':
      authMethod = AuthenticationType.googleType[localeSubstring]
      break
    case 'windowslive':
      authMethod = AuthenticationType.windowsType[localeSubstring]
      break
    default:
      authMethod = 'Default'
      break
  }

  let templateName
  if (!groupType) {
    templateName = 'confirmation_individual'
  } else {
    templateName = `confirmation_${groupType.toLowerCase()}`
  }

  const mailTemplate = await getEmailTemplate(templateName, locale.substring(0, 2).toLowerCase() === 'fr' ? 'FR' : 'NL')

  const htmlBody = mailTemplate.body
    .replace(/\[JOINLINK]/g, shortLink)
    .replace(/\[TOOLBOXLINK]/g, toolboxLink)
    .replace(/\[AUTHMETHOD]/g, authMethod)

  const mailMessage = {
    to: accountEmail,
    from: {
      name: mailTemplate.fromname,
      email: config.email.from
    },
    subject: mailTemplate.subject,
    html: htmlBody
  }

  try {
    await sendGridMail.send(mailMessage)
  } catch (error) {
    // fail silently if mail fails, but log
    /* istanbul ignore next */
    simpleLog.info(module, 'processPayment', flowId, 'SENDING_MAIL', { mailMessage, error })
    // IDEA should be WARN
  }
}

async function ensureAccountAggregate (sot, sub, mode, flowId, payment, accountId) {
  if (payment.groupType.toLowerCase() === 'team') {
    let existingAccountAggregate
    try {
      existingAccountAggregate = await getAccountAggregate(sot, sub, mode, flowId, { accountId })
    } catch (e) {
      existingAccountAggregate = undefined
    }
    let accountAggregateData
    if (!existingAccountAggregate) {
      const accountProfile = await getAccountProfile(sot, sub, mode, flowId, { accountId })
      accountAggregateData = {
        createdAt: sot,
        createdBy: sub,
        structureVersion: 1,
        accountId: accountId,
        totalSteps: 0,
        totalDistance: 0,
        groupId: payment.groupId,
        groupType: payment.groupType,
        name: `${accountProfile.firstName} ${accountProfile.lastName}`,
        rank: 9999999,
        totalParticipants: 9999999,
        previousLevel: 1,
        acknowledged: false
      }
      await writeAccountAggregate(sot, sub, mode, flowId, accountAggregateData)
    } else if (existingAccountAggregate && !existingAccountAggregate.groupId) {
      accountAggregateData = {
        ...existingAccountAggregate,
        groupId: payment.groupId,
        groupType: payment.groupType,
        rank: 9999999,
        totalParticipants: 9999999,
        previousLevel: 1,
        acknowledged: false
      }
      await writeAccountAggregate(sot, sub, mode, flowId, accountAggregateData)
    }
  }
}

async function ensureTeamAggregate (sot, sub, mode, flowId, payment, existingSlotsForTeam, teamAdminAccountId) {
  /* istanbul ignore else : code will never be reached */
  if (payment.groupType.toLowerCase() === 'team') {
    const totalMembers = existingSlotsForTeam + payment.numberOfSlots
    if (totalMembers > 1) {
      let existingTeamAggregate
      try {
        existingTeamAggregate = await getTeamAggregate(sot, sub, mode, flowId, { teamAggregateId: payment.groupId })
      } catch (e) {
        existingTeamAggregate = undefined
      }

      // create aggregate if not already exists
      if (!existingTeamAggregate) {
        const groupProfile = await getGroupProfile(sot, sub, mode, flowId, { teamId: payment.groupId })

        let teamAdminAccountAggregate
        // Only a teamAdmin can buy additional slots, so we assume that this is the only existing member => take over the steps & distance from it's accountAggregate
        if (existingSlotsForTeam > 0) {
          try {
            teamAdminAccountAggregate = await getAccountAggregate(sot, sub, mode, flowId, {
              accountId: teamAdminAccountId
            })
          } catch (e) {
            teamAdminAccountAggregate = undefined
          }
        }

        // Just create the basic aggregate, setting the number of steps & distance should be done in the teamAggregateHandler (handle insert)
        const teamAggregateData = {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          teamAggregateId: payment.groupId,
          totalSteps: teamAdminAccountAggregate ? teamAdminAccountAggregate.totalSteps : 0,
          totalDistance: teamAdminAccountAggregate ? teamAdminAccountAggregate.totalDistance : 0,
          averageSteps: 1,
          averageDistance: 1,
          totalTeams: 9999999,
          companyId: undefined,
          name: groupProfile.name,
          rank: 9999999,
          groupType: payment.groupType,
          extendedName: undefined,
          hasLogo: !!groupProfile.logo
        }

        await writeTeamAggregate(sot, sub, mode, flowId, teamAggregateData)
      }
    }
  }
}

async function ensureCompanyAggregate (sot, sub, mode, flowId, payment, existingSlotsForCompany, unit) {
  /* istanbul ignore else : code will never be reached */
  if (payment.groupType.toLowerCase() === 'company') {
    const totalMembers = existingSlotsForCompany + payment.numberOfSlots
    /* istanbul ignore else */
    if (totalMembers >= 1) {
      let existingCompanyAggregate
      try {
        existingCompanyAggregate = await getCompanyAggregate(sot, sub, mode, flowId, { companyId: payment.groupId })
      } catch (e) {
        existingCompanyAggregate = undefined
      }

      // create aggregate if not already exists
      if (!existingCompanyAggregate) {
        const groupProfile = await getGroupProfile(sot, sub, mode, flowId, { companyId: payment.groupId })

        // Just create the basic aggregate, setting the number of steps & distance should be done in the companyAggregateHandler (handle insert)
        const companyAggregateData = {
          createdAt: sot,
          createdBy: sub,
          structureVersion: 1,
          companyId: payment.groupId,
          totalSteps: 0,
          totalDistance: 0,
          totalCompanies: 9999999,
          name: groupProfile.name,
          rank: 9999999,
          unit: unit,
          hasLogo: !!groupProfile.logo,
          averageSteps: 1,
          averageDistance: 1
        }

        await writeCompanyAggregate(sot, sub, mode, flowId, companyAggregateData)
      }
    }
  }
}

async function getShortLink (sot, sub, mode, flowId, payment, group, existingSlots) {
  simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_ENTER', {
    message: 'Initial values when entering getShortLink function',
    paymentId: payment.paymentId,
    groupId: group.groupId,
    groupType: group.groupType,
    payment,
    group,
    existingSlots
  })
  switch (payment.groupType) {
    case GroupType.teamType:
      simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK', {
        message: 'Start get short link for Team',
        paymentId: payment.paymentId,
        groupId: group.groupId,
        groupType: group.groupType,
        payment,
        group,
        existingSlots
      })
      if ((existingSlots === 0 && payment.numberOfSlots > 1) || existingSlots >= 1) {
        if (!group.linkId) {
          let shortLinkResult
          let itemData
          try {
            shortLinkResult = await createShortLink(sot, sub, mode, flowId, {
              joinLink: group.joinLink,
              groupId: payment.groupId
            })
            itemData = {
              createdAt: sot,
              createdBy: sub,
              structureVersion: 1,
              id: group.id,
              code: group.code,
              groupType: group.groupType,
              linkId: shortLinkResult.linkId,
              shortUrl: shortLinkResult.shortLink
            }
          } catch (err) {
            simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_FAILED', {
              message: 'Create shortlink failed',
              error: err,
              paymentId: payment.paymentId,
              groupId: group.groupId,
              groupType: group.groupType,
              shortLink: group.joinLink
            })
            itemData = {
              createdAt: sot,
              createdBy: sub,
              structureVersion: 1,
              id: group.id,
              code: group.code,
              groupType: group.groupType,
              shortUrl: group.joinLink
            }
          }
          await putTeam(sot, sub, mode, flowId, { teamId: payment.groupId }, itemData)
          simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_SUCCESS', {
            message: 'Create shortlink successful',
            paymentId: payment.paymentId,
            groupId: group.groupId,
            groupType: group.groupType,
            shortLink: itemData.shortUrl
          })
          return itemData.shortUrl
        }
        simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_EXISTING', {
          message: 'Return existing shortlink',
          paymentId: payment.paymentId,
          groupId: group.groupId,
          groupType: group.groupType,
          shortLink: group.shortUrl
        })
        return group.shortUrl
      } else {
        simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_INDIVIDUAL', {
          message: 'Return without shortlink -> individual not a team ',
          paymentId: payment.paymentId,
          groupId: group.groupId,
          groupType: group.groupType,
          existingSlots,
          paymentSlots: payment.numberOfSlots
        })
        // Shouldn't return a link because in this case it's an individual
        return
      }
    case GroupType.companyType:
      simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK', {
        message: 'Start get short link for Company',
        paymentId: payment.paymentId,
        groupId: group.groupId,
        groupType: group.groupType,
        payment,
        group,
        existingSlots
      })
      if (!group.linkId) {
        let shortLinkResult
        let itemData
        try {
          shortLinkResult = await createShortLink(sot, sub, mode, flowId, {
            joinLink: group.joinLink,
            groupId: payment.groupId
          })
          itemData = {
            ...group,
            linkId: shortLinkResult.linkId,
            shortUrl: shortLinkResult.shortLink
          }
        } catch (err) {
          itemData = {
            ...group,
            shortUrl: group.joinLink
          }
        }
        delete itemData.joinLink
        delete itemData.links

        await putCompany(sot, sub, mode, flowId, { companyId: payment.groupId }, itemData)
        simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_SUCCESS', {
          message: 'Create shortlink successful',
          paymentId: payment.paymentId,
          groupId: group.groupId,
          groupType: group.groupType,
          shortLink: itemData.shortUrl
        })
        return itemData.shortUrl
      }
      simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_EXISTING', {
        message: 'Return existing shortlink',
        paymentId: payment.paymentId,
        groupId: group.groupId,
        groupType: group.groupType,
        shortLink: group.shortUrl
      })
      return group.shortUrl
    /* istanbul ignore next */
    default:
      simpleLog.info(module, 'processPayment', flowId, 'GET_SHORTLINK_DEFAULT_SUCCESS', {
        message: 'Get shortlink default value',
        paymentId: payment.paymentId,
        groupId: group.groupId,
        groupType: group.groupType,
        accountId: payment.accountId,
        shortLink: group.joinLink
      })
      return group.joinLink
  }
}

/**
 * @param {string} merchantId
 * @param {string} paymentDetails
 */
const processPaymentContract = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, result) {
      return validateSchema(ProcessPaymentResultDTO.schema, result)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 400 ||
        PromiseContract.outcome(arguments).output.statusCode === 403 ||
        PromiseContract.outcome(arguments).output.statusCode === 404
      )
    }
  ])
}).implementation(async function processPayment (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  /* istanbul ignore next */
  const ogoneConfig = mode === 'production' ? config.ogone.production : config.ogone.test
  /* istanbul ignore next */
  const ogoneMode = mode === 'production' ? 'prod' : 'test'
  // start async calls early, await when needed
  const noSlotsYetPromise = getNoSlotsYet(sot, sub, mode, flowId, pathParameters.paymentId)
  const accountPromise = getAccount(sot, sub, mode, flowId, { accountId: pathParameters.accountId })

  const payment = await getPayment(sot, sub, mode, flowId, { paymentId: pathParameters.paymentId }) // possible 404
  // const claimSlotForAdministratorPromise = shouldClaimSlotsForAdministrator(sot, sub, mode, flowId, payment) // start question early
  const getExistingTotalSlotsForGroupPromise = getTotalSlotsForGroup(sot, sub, mode, flowId, payment) // start question early
  const groupPromise =
    payment.groupType === GroupType.companyType
      ? getCompany(sot, sub, mode, flowId, { companyId: payment.groupId })
      : getTeam(sot, sub, mode, flowId, { teamId: payment.groupId })
  const accountPreferencesPromise =
    payment.groupType === GroupType.teamType
      ? getPreferences(sot, sub, mode, flowId, { accountId: pathParameters.accountId })
      : undefined

  let ogoneResult

  let newPaymentStatus

  if (payment.amount > 0) {
    const ogone = new Ogone(ogoneMode, {
      pspId: ogoneConfig.pspId,
      userId: ogoneConfig.username,
      pswd: ogoneConfig.password
    })
    try {
      ogoneResult = await ogone.getPayment({
        orderId: payment.paymentId
      })
      newPaymentStatus = ogoneResult ? ogoneResult.status : PaymentStatus.INITIALIZED
    } catch (err) {
      // MUDO don't make unexpected errors seem defined; what is happening here?
      throw Boom.forbidden()
    }
  } else {
    newPaymentStatus = PaymentStatus.PAYMENT_REQUESTED
  }

  const updatedPayment = {
    ...payment,
    paymentStatus: newPaymentStatus
  }
  delete updatedPayment.links

  if (updatedPayment.paymentStatus !== payment.paymentStatus) {
    await putPayment(sot, sub, mode, flowId, { paymentId: payment.paymentId }, updatedPayment)
  }

  // we have registered the attempt, now check whether the payment is ok
  if (updatedPayment.paymentStatus !== PaymentStatus.PAYMENT_REQUESTED) {
    throw Boom.forbidden()
  }

  if (!(await noSlotsYetPromise)) {
    throw Boom.forbidden()
  }

  const ensureAccountAggregatePromise = ensureAccountAggregate(
    sot,
    sub,
    mode,
    flowId,
    payment,
    pathParameters.accountId
  )

  existingSlotsForGroup = await getExistingTotalSlotsForGroupPromise
  const group = await groupPromise

  let ensureTeamOrCompanyAggregatePromise
  switch (payment.groupType) {
    case GroupType.teamType:
      ensureTeamOrCompanyAggregatePromise = ensureTeamAggregate(
        sot,
        sub,
        mode,
        flowId,
        payment,
        existingSlotsForGroup,
        pathParameters.accountId
      )
      break
    case GroupType.companyType:
      ensureTeamOrCompanyAggregatePromise = ensureCompanyAggregate(
        sot,
        sub,
        mode,
        flowId,
        payment,
        existingSlotsForGroup,
        group.unit
      )
      break
    /* istanbul ignore next */
    default:
      break
  }

  const freeSlots = await createSlots(
    sot,
    sub,
    mode,
    flowId,
    payment.numberOfSlots,
    payment.groupType,
    payment.groupId,
    payment.paymentId,
    pathParameters.accountId,
    payment.groupType === GroupType.teamType && existingSlotsForGroup === 0
  )
  /*
   * Create shortLink when needed after creating slots, because createSlots might fail
   * */
  const shortUrl = await getShortLink(sot, sub, mode, flowId, payment, group, existingSlotsForGroup)

  // do not send email in parallel, because createSlots might fail
  /* IDEA It shouldn't; error handling in createSlots is shaky; this should be done async (queue), and errors should be
          handled there. */
  let email
  let language
  if (payment.groupType === GroupType.companyType) {
    email = group.contactEmail
    language = 'nl'
  } else {
    const accountPreferences = await accountPreferencesPromise
    email = accountPreferences.verifiedEmail.email
    language = accountPreferences.language
  }
  const account = await accountPromise
  const authMethod = getAuthMethod(account)

  const sendMailPromise = sendEmail(
    flowId,
    email,
    payment.numberOfSlots !== 1 && payment.groupType,
    language,
    shortUrl,
    'https://bit.ly/DRM-LMR-Toolkit-2021',
    authMethod
  )

  await Promise.all([ensureAccountAggregatePromise, ensureTeamOrCompanyAggregatePromise, sendMailPromise])
  // Return free slots and joinLink
  return { joinLink: shortUrl || null, freeSlots }
})

module.exports = processPaymentContract
