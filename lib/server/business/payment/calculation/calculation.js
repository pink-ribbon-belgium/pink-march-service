/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../../_util/validateSchema')
const CalculationParams = require('../../../../api/payment/calculation/calculationParams')
const CalculationResult = require('../../../../api/payment/calculation/calculationResult')
const Voucher = require('../../../../api/voucher/VoucherDTO').schema.tailor('dynamodb')
const PointInTime = require('../../../../ppwcode/PointInTime').schema
const Mode = require('../../../../ppwcode/Mode')
const requiredMode = Mode.schema.required().label('mode')
const PaymentStatus = require('../../../../api/payment/PaymentStatus')
const getSlotsForGroup = require('../../group/slots/get')

const getByIndex = require('./../../../../server/aws/dynamodb/getByIndex')

const requiredPointInTime = PointInTime.required()

async function getTotalSlotsForGroup (sot, sub, mode, flowId, pathParam) {
  try {
    const slotsForGroup = await getSlotsForGroup(sot, sub, mode, flowId, pathParam)
    return slotsForGroup.totalSlots
  } catch (err) {
    return 0
  }
}

function calculatePrice (numberOfSlots) {
  switch (true) {
    case numberOfSlots <= 4: {
      return numberOfSlots * 10
    }
    case numberOfSlots === 5: {
      return 40
    }
    case numberOfSlots > 5: {
      return numberOfSlots * 8
    }
  }
}

const calculation = new PromiseContract({
  pre: [
    function (sot) {
      return validateSchema(requiredPointInTime, sot)
    },
    function (sot, sub, mode) {
      return validateSchema(requiredMode, mode)
    },
    function (sot, sub, mode, flowId, params) {
      return validateSchema(CalculationParams.schema, params)
    },
    function (sot, sub, mode, flowId, params, voucher) {
      return validateSchema(Voucher, voucher)
    },
    function (sot, sub, mode, flowId, params, voucher) {
      return !voucher || params.voucherCode === voucher.voucherCode
    }
  ],
  post: [
    function (sot, sub, mode, flowId, params, voucher, result) {
      return validateSchema(CalculationResult.schema, result)
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return result.slotsToAdd === params.numberOfParticipants
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return result.unitPrice === (result.numberOfParticipants > 4 ? 8 : 10)
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return result.subTotal === result.total + result.discount
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return result.discount <= result.subTotal
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      /* is not expired */
      return !voucher || sot <= voucher.expiryDate || result.discount === 0
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return (
        !voucher ||
        voucher.expiryDate < sot ||
        !voucher.amount ||
        result.discount === Math.min(voucher.amount, result.subTotal)
      )
    },
    function (sot, sub, mode, flowId, params, voucher, result) {
      return (
        !voucher ||
        voucher.expiryDate < sot ||
        !voucher.percentage ||
        result.discount === voucher.percentage * result.subTotal
      )
    }
  ]
}).implementation(async function calculationImpl (sot, sub, mode, flowId, params, voucher) {
  const existingSlotsForGroup = await getTotalSlotsForGroup(sot, sub, mode, flowId, { groupId: params.groupId })
  const slotsToAdd = params.numberOfParticipants
  const amountPaid = calculatePrice(existingSlotsForGroup)

  const numberOfParticipants = existingSlotsForGroup + slotsToAdd
  const unitPrice = numberOfParticipants < 5 ? 10 : 8
  const totalCost = numberOfParticipants * unitPrice

  const subTotal = totalCost - amountPaid

  let voucherIsValid = true
  if (voucher && !voucher.reusable) {
    const alreadyUsed = await isVoucherAlreadyUsed(mode, voucher.voucherCode)
    voucherIsValid = !alreadyUsed
  }

  const discount = Math.min(
    voucher && voucherIsValid && voucher.expiryDate >= sot
      ? voucher.amount
        ? voucher.amount
        : voucher.percentage * subTotal
      : 0,
    subTotal
  )
  const total = subTotal - discount

  return {
    numberOfParticipants,
    slotsToAdd,
    unitPrice,
    amountPaid,
    subTotal,
    discount,
    total,
    voucherIsValid: voucherIsValid
  }
})

async function isVoucherAlreadyUsed (mode, voucherCode) {
  const index = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const partitionKey = `/${mode}/voucher/${voucherCode}/payments`

  let result = []
  try {
    result = await getByIndex(mode, partitionKey, index)
  } catch (e) {
    /* istanbul ignore else */
    if (e.isBoom && e.output.statusCode === 404) {
      return false
    } else {
      throw e
    }
  }

  const resultsToHandle = result.filter(i => i.data.paymentStatus === PaymentStatus.PAYMENT_REQUESTED)

  return resultsToHandle.length > 0
}

module.exports = calculation
