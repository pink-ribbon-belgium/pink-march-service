/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const calculation = require('./calculation')
const getVoucher = require('../../voucher/get')
const CalculationResultDTO = require('../../../../api/payment/calculation/calculationResult')
const validateSchema = require('../../../../_util/validateSchema')

const requiredCalculationResultSchema = CalculationResultDTO.schema.required()

const post = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, body, response) {
      return validateSchema(requiredCalculationResultSchema, response)
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  let voucher
  if (body.voucherCode) {
    try {
      voucher = await getVoucher(sot, sub, mode, flowId, { voucherCode: body.voucherCode })
    } catch (err) {
      // Voucher is not found but we need to move on, no discount it is
      // MUDO do not eat errors! only move one if this is an expected error! (boom 404) - empty catch clauses are ALWAYS wrong
    }
  }
  return await calculation(sot, sub, mode, flowId, body, voucher)
})

module.exports = post
