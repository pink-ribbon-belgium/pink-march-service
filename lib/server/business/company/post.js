/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const CompanyDTO = require('../../../api/company/CompanyDTO')
const putCompany = require('./put')
const putGroupProfile = require('../group/profile/put')
const putGroupAdministration = require('../group/administration/put')
const uuidv4 = require('uuid').v4
const UUID = require('../../../ppwcode/UUID')
const Joi = require('@hapi/joi')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const requiredValidationInputSchema = CompanyDTO.schema.tailor('input').required()

const post = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function () {
      return validateSchema(
        Joi.object().keys({ companyId: UUID.schema.required() }),
        PromiseContract.outcome(arguments)
      )
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  const groupId = uuidv4()
  validateInputSchema(requiredValidationInputSchema, { ...body, id: groupId })

  await Promise.all([
    putCompany(sot, sub, mode, flowId, { companyId: groupId }, body),
    putGroupProfile(sot, sub, mode, flowId, { companyId: groupId }, body),
    putGroupAdministration(
      sot,
      sub,
      mode,
      flowId,
      { accountId: body.accountId, groupType: 'company', groupId },
      { structureVersion: 1, deleted: false }
    )
  ])
  return {
    companyId: groupId
  }
})

module.exports = post
