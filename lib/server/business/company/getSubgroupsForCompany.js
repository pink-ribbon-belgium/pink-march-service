/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../aws/dynamodb/getByIndex')
const validateSchema = require('../../../_util/validateSchema')
const SubgroupDto = require('../../../api/subgroup/SubGroupDTO')
const validateInputSchema = require('../../../_util/validateInputSchema')
const CompanyPathParameters = require('../../../api/company/CompanyPathParameters')

const requiredSubgroupSchema = SubgroupDto.schema.tailor('dynamodb').required()

const getSubgroups = new PromiseContract({
  pre: ResourceActionContract.pre.concat([
    function (sot, sub, mode, flowId, pathParameters) {
      return validateInputSchema(CompanyPathParameters.schema.required(), pathParameters)
    }
  ]),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(item => validateSchema(requiredSubgroupSchema, item))
    }
  ]),
  exception: ResourceActionContract.exception.slice()
}).implementation(async function getSubgroups (sot, sub, mode, flowId, pathParameters) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }
  const indexPartitionKey = `/${mode}/subgroup/company/${pathParameters.companyId}`
  let result
  try {
    result = await getByIndex(mode, indexPartitionKey, index)
  } catch (e) {
    return []
  }

  return result.map(item => item.data)
})

module.exports = getSubgroups
