/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const putDynamodb = require('../../aws/dynamodb/put')
const Company = require('./Company')
const CompanyDTO = require('../../../api/company/CompanyDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateInputSchema = require('../../../_util/validateInputSchema')
const CompanyPathParameters = require('../../../api/company/CompanyPathParameters')

const requiredValidationInputSchema = CompanyDTO.schema.tailor('input').required()
const requiredCompanyPathParameters = CompanyPathParameters.schema.required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredCompanyPathParameters, pathParameters)
  validateInputSchema(requiredValidationInputSchema, body)
  const company = new Company({
    mode,

    /* NOTE: SECURITY: createdAt and createdBy are audit data. External users can add unknown
     *                  properties to the PUT body. We must take care that an external user's
     *                  `createdAt` and 'createdBy` does not overwrite the true values we put in. */
    dto: {
      ...body,
      groupType: 'Company',
      createdAt: sot,
      createdBy: sub,
      id: pathParameters.companyId
    }
  })

  const item = {
    ...company.toItem(),
    flowId
  }

  return putDynamodb(item)
})

module.exports = put
