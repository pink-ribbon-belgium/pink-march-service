/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const Group = require('../../../server/business/group/Group')
const PersistentObject = require('../../../server/business/PersistentObject')
const CompanyDTO = require('../../../api/company/CompanyDTO')
const Mode = require('../../../ppwcode/Mode')

const dynamodbDTO = CompanyDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const KwargsExample = {
  mode: Mode.example,
  dto: CompanyDTO.exampleDynamodb
}

/**
 * @typedef Kwargs
 * @extends {PersistentObject.Kwargs}
 *
 * @property {CompanyDTO} dto
 */

/** @type {ObjectSchema} */
const Kwargs = Group.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() }).example(KwargsExample)

/**
 * Represents an Account in the real world, which is the closest we can get to a user.
 *
 * A user can have multiple Accounts, but we should try to keep users from creating multiple accounts.
 * An account could be used by multiple users, but that is a big anti-pattern. We advise users not to share accounts,
 * and take no responsibility if they do.
 *
 * @param {Kwargs} kwargs
 * @constructor
 */
const CompanyContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: Group.contract.post.concat([
    function (kwargs) {
      return this.address === kwargs.dto.address
    },
    function (kwargs) {
      return this.zip === kwargs.dto.zip
    },
    function (kwargs) {
      return this.city === kwargs.dto.city
    },
    function (kwargs) {
      return this.vat === kwargs.dto.vat
    },
    function (kwargs) {
      return this.reference === kwargs.dto.reference
    },
    function (kwargs) {
      return this.contactFirstName === kwargs.dto.contactFirstName
    },
    function (kwargs) {
      return this.contactLastName === kwargs.dto.contactLastName
    },
    function (kwargs) {
      return this.contactEmail === kwargs.dto.contactEmail
    },
    function (kwargs) {
      return this.contactTelephone === kwargs.dto.contactTelephone
    },
    function (kwargs) {
      return this.code === kwargs.dto.code
    },
    function (kwargs) {
      return this.unit === kwargs.dto.unit
    },
    function (kwargs) {
      return this.links === kwargs.dto.links
    },
    function (kwargs) {
      return this.joinLink === kwargs.dto.joinLink
    }
  ])
})

CompanyContract.Kwargs = Kwargs

CompanyContract.KwargsExample = KwargsExample

CompanyContract.methods = { ...Group.contract.methods }

CompanyContract.invariants = Group.contract.invariants.concat([
  function () {
    return this.toJSON().address === this.address
  },
  function () {
    return this.toJSON().zip === this.zip
  },
  function () {
    return this.toJSON().city === this.city
  },
  function () {
    return this.toJSON().vat === this.vat
  },
  function () {
    return this.toJSON().reference === this.reference
  },
  function () {
    return this.toJSON().contactFirstName === this.contactFirstName
  },
  function () {
    return this.toJSON().contactLastName === this.contactLastName
  },
  function () {
    return this.toJSON().contactEmail === this.contactEmail
  },
  function () {
    return this.toJSON().contactTelephone === this.contactTelephone
  },
  function () {
    return this.toJSON().code === this.code
  },
  function () {
    return this.toJSON().unit === this.unit
  },
  function () {
    return this.toJSON().links === this.links
  },
  function () {
    return this.toJSON().joinLink === this.joinLink
  },
  function () {
    return this.groupType === 'Company'
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    const links = this.getLinks()
    const linkPrefix = `/I/company/${this.id}`
    return Object.values(links).every(link => link.startsWith(linkPrefix))
  }
])

function CompanyImpl (kwargs) {
  Group.call(this, kwargs)
  this.address = kwargs.dto.address
  this.zip = kwargs.dto.zip
  this.city = kwargs.dto.city
  this.vat = kwargs.dto.vat
  this.reference = kwargs.dto.reference
  this.contactFirstName = kwargs.dto.contactFirstName
  this.contactLastName = kwargs.dto.contactLastName
  this.contactEmail = kwargs.dto.contactEmail
  this.contactTelephone = kwargs.dto.contactTelephone
  this.code = kwargs.dto.code
  this.unit = kwargs.dto.unit
}

CompanyImpl.prototype = Object.create(Group.prototype)

CompanyImpl.prototype.constructor = CompanyImpl

CompanyImpl.prototype.toJSON = function () {
  return {
    ...Group.prototype.toJSON.call(this),
    address: this.address,
    zip: this.zip,
    city: this.city,
    vat: this.vat,
    reference: this.reference,
    contactFirstName: this.contactFirstName,
    contactLastName: this.contactLastName,
    contactEmail: this.contactEmail,
    contactTelephone: this.contactTelephone,
    code: this.code,
    unit: this.unit
  }
}

CompanyImpl.prototype.invariants = CompanyContract.invariants

/**
 * The type plus the id.
 *
 * @return {string}
 */
CompanyImpl.prototype.getKey = function () {
  return Company.getKey({ id: this.id, mode: this.mode })
}

CompanyImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/company`
  }
}

/*
 * getLinks is inherited from group
 * In order to add additional links uncomment next code
 *
 * CompanyImpl.prototype.getLinks = function () {
 *   return {
 *     ...Group.prototype.getLinks.call(this),
 *     someOtherLink: ''
 *   }
 * }
 */

const Company = CompanyContract.implementation(CompanyImpl)

Company.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/company/${kwargs.id}`
})

module.exports = Company
