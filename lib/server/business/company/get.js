/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../aws/dynamodb/getVersionAtOrBefore')
const Company = require('./Company')
const validateSchema = require('../../../_util/validateSchema')
const CompanyDTO = require('../../../api/company/CompanyDTO')
const validateInputSchema = require('../../../_util/validateInputSchema')
const CompanyPathParameters = require('../../../api/company/CompanyPathParameters')

const requiredCompanySchema = CompanyDTO.schema.tailor('output').required()
const requiredCompanyPathParameters = CompanyPathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.id === pathParameters.companyId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredCompanySchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredCompanyPathParameters, pathParameters)

  const key = `/${mode}/company/${pathParameters.companyId}`
  const dtoPromise = getVersionAtOrBefore(mode, key, sot)
  const result = await dtoPromise

  result.data.groupType = 'Company'
  const company = new Company({
    mode,
    dto: {
      ...result.data
    }
  })

  const companyJson = company.toJSON()
  companyJson.links = company.getLinks()
  companyJson.joinLink = company.getJoinLink()

  return companyJson
})

module.exports = get
