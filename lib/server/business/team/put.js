/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const putDynamodb = require('../../aws/dynamodb/put')
const Team = require('./../team/Team')
const TeamDTO = require('../../../api/team/TeamDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateInputSchema = require('../../../_util/validateInputSchema')
const TeamPathParams = require('../../../api/team/TeamPathParameters')

const requiredPathParameters = TeamPathParams.schema.required()
const requiredInputSchema = TeamDTO.schema.tailor('input').required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParameters, pathParameters)
  validateInputSchema(requiredInputSchema, body)
  const team = new Team({
    mode,
    dto: {
      ...body,
      groupType: 'Team',
      createdAt: sot,
      createdBy: sub,
      id: pathParameters.teamId
    },
    sot,
    sub
  })
  const item = {
    ...team.toItem(),
    flowId
  }

  return putDynamodb(item)
})

module.exports = put
