/*
 * Copyright 2020 PeopleWare n.v.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const Group = require('../../../server/business/group/Group')
const PersistentObject = require('../PersistentObject')
const TeamDTO = require('../../../api/team/TeamDTO')

const dynamodbDTO = TeamDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const Kwargs = Group.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const TeamContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: Group.contract.post.slice()
})

TeamContract.Kwargs = Kwargs

TeamContract.methods = { ...Group.contract.methods }

TeamContract.invariants = Group.contract.invariants.concat([
  function () {
    return this.groupType === 'Team'
  },
  function () {
    const links = this.getLinks()
    const linkPrefix = `/I/team/${this.id}`
    return Object.values(links).every(link => link.startsWith(linkPrefix))
  }
])

function TeamImpl (kwargs) {
  Group.call(this, kwargs)
}

TeamImpl.prototype = Object.create(Group.prototype)
TeamImpl.prototype.constructor = TeamImpl

TeamImpl.prototype.id = undefined
TeamImpl.prototype.structureVersion = 1

TeamImpl.prototype.getKey = function () {
  return Team.getKey({ id: this.id, mode: this.mode })
}

TeamImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/team`
  }
}

/**
 * getLinks is inherited from group
 * In order to add additional links uncomment next code
 *
 * TeamImpl.prototype.getLinks = function () {
 *   return {
 *     ...Group.prototype.getLinks.call(this),
 *     someOtherLink: ''
 *   }
 * }
 */

TeamImpl.prototype.invariants = TeamContract.invariants

const Team = TeamContract.implementation(TeamImpl)

Team.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/team/${kwargs.id}`
})

module.exports = Team
