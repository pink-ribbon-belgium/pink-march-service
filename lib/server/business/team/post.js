/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const TeamDTO = require('../../../api/team/TeamDTO')
const putTeam = require('./put')
const putGroupProfile = require('../group/profile/put')
const putGroupAdministration = require('../group/administration/put')
const uuidv4 = require('uuid').v4
const UUID = require('../../../ppwcode/UUID')
const Joi = require('@hapi/joi')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const requiredValidationInputSchema = TeamDTO.schema.tailor('input').required()

const post = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function () {
      return validateSchema(Joi.object().keys({ teamId: UUID.schema.required() }), PromiseContract.outcome(arguments))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  const groupId = uuidv4()
  validateInputSchema(requiredValidationInputSchema, { ...body, id: groupId })

  await Promise.all([
    putTeam(sot, sub, mode, flowId, { teamId: groupId }, body),
    putGroupProfile(sot, sub, mode, flowId, { teamId: groupId }, body),
    putGroupAdministration(
      sot,
      sub,
      mode,
      flowId,
      { accountId: body.accountId, groupType: 'team', groupId },
      { structureVersion: 1, deleted: false }
    )
  ])
  return {
    teamId: groupId
  }
})

module.exports = post
