/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../aws/dynamodb/getVersionAtOrBefore')
const Account = require('./Account')
const validateSchema = require('../../../_util/validateSchema')
const AccountDTO = require('../../../api/account/AccountDTO')
const validateInputSchema = require('../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../api/account/AccountPathParameters')

const requiredAccountSchema = AccountDTO.schema.tailor('output').required()
const requiredAccountPathParameters = AccountPathParameters.schema.required()

/**
 * @param {PointInTime} sot
 * @param {AccountId} sub
 * @param {Mode} mode
 * @param {FlowId} flowId
 * @param {object} pathParameters
 */
const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.id === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredAccountSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredAccountPathParameters, pathParameters)
  const key = `/${mode}/account/${pathParameters.accountId}` // TODO move to key calculation to Account static?
  const dtoPromise = getVersionAtOrBefore(mode, key, sot)
  const result = await dtoPromise

  const account = new Account({
    mode,
    dto: {
      ...result.data
    }
  })

  const accountJson = account.toJSON()
  accountJson.links = account.getLinks()
  return accountJson
})

module.exports = get
