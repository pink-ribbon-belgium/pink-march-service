/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const AccountId = require('../../../api/account/AccountId')
const PersistentObject = require('../../../server/business/PersistentObject')
const AccountDTO = require('../../../api/account/AccountDTO')

const requiredAccountId = AccountId.schema.required()

const dynamodbDTO = AccountDTO.schema.tailor('dynamodb').required()

/**
 * @typedef Kwargs
 * @extends {PersistentObject.Kwargs}
 *
 * @property {AccountDTO} dto
 */

/** @type {ObjectSchema} */
const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

/**
 * Represents an Account in the real world, which is the closest we can get to a user.
 *
 * A user can have multiple Accounts, but we should try to keep users from creating multiple accounts.
 * An account could be used by multiple users, but that is a big anti-pattern. We advise users not to share accounts,
 * and take no responsibility if they do.
 *
 * @param {Kwargs} kwargs
 * @constructor
 */
const AccountContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.id === kwargs.dto.id
    },
    function (kwargs) {
      return this.email === kwargs.dto.email
    }
  ])
})

AccountContract.Kwargs = Kwargs

AccountContract.methods = { ...PersistentObject.contract.methods }

AccountContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredAccountId, this.id)
  },
  function () {
    return !!this.sub && typeof this.sub === 'string'
  },
  function () {
    return this.toJSON().id === this.id
  },
  function () {
    return this.toJSON().sub === this.sub
  },
  function () {
    return this.toJSON().email === this.email
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  }
])

function AccountImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.id = kwargs.dto.id
  this.sub = kwargs.dto.sub
  this.email = kwargs.dto.email
}

AccountImpl.prototype = Object.create(PersistentObject.prototype)
AccountImpl.prototype.constructor = AccountImpl

/**
 * Opaque id of this account.
 *
 * @type {string}
 */
AccountImpl.prototype.id = undefined

/**
 * Version of the structure written out by `toJSON` (the constructor might be able to understand multiple, earlier or
 * later versions).
 *
 * @type {number}
 */
AccountImpl.prototype.structureVersion = 1

/**
 * @return {DTO}
 */
AccountImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    id: this.id,
    sub: this.sub,
    email: this.email
  }
}

/**
 * The type plus the id.
 *
 * @return {string}
 */
AccountImpl.prototype.getKey = function () {
  return Account.getKey({ id: this.id, mode: this.mode })
}

AccountImpl.prototype.getLinks = function () {
  return {
    preferences: `/I/account/${this.id}/preferences`,
    publicProfile: `/I/account/${this.id}/publicProfile`,
    slot: `/I/account/${this.id}/slot`,
    administratorOf: `/I/account/${this.id}/administratorOf`
  }
}

AccountImpl.prototype.invariants = AccountContract.invariants

const Account = AccountContract.implementation(AccountImpl)

Account.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/account/${kwargs.id}`
})

module.exports = Account
