/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const AccountId = require('../../../../api/account/AccountId')
const AccountProfileDTO = require('../../../../api/account/AccountProfileDTO')

const requiredAccountId = AccountId.schema.required()

const dynamodbDTO = AccountProfileDTO.schema.tailor('dynamodb').required()
const outputDTO = AccountProfileDTO.schema.tailor('output').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const AccountProfileContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.firstName === kwargs.dto.firstName
    },
    function (kwargs) {
      return this.lastName === kwargs.dto.lastName
    },
    function (kwargs) {
      return this.gender === kwargs.dto.gender
    },
    function (kwargs) {
      return this.dateOfBirth === kwargs.dto.dateOfBirth
    },
    function (kwargs) {
      return this.zip === kwargs.dto.zip
    }
  ])
})

AccountProfileContract.Kwargs = Kwargs

AccountProfileContract.methods = { ...PersistentObject.contract.methods }

AccountProfileContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredAccountId, this.accountId)
  },
  function () {
    return this.getKey() === `/${this.mode}/account/${this.accountId}/publicProfile`
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().firstName === this.firstName
  },
  function () {
    return this.toJSON().lastName === this.lastName
  },
  function () {
    return this.toJSON().gender === this.gender
  },
  function () {
    return this.toJSON().dateOfBirth === this.dateOfBirth
  },
  function () {
    return this.toJSON().zip === this.zip
  },
  function () {
    return validateSchema(outputDTO, this.toJSON())
  },
  function () {
    return this.getIndexPartitionKeys().A === `/${this.mode}/AccountProfile`
  }
])

function AccountProfileImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.firstName = kwargs.dto.firstName
  this.lastName = kwargs.dto.lastName
  this.gender = kwargs.dto.gender
  this.dateOfBirth = kwargs.dto.dateOfBirth
  this.zip = kwargs.dto.zip
}

AccountProfileImpl.prototype = Object.create(PersistentObject.prototype)
AccountProfileImpl.prototype.constructor = AccountProfileImpl

AccountProfileImpl.prototype.accountId = undefined

AccountProfileImpl.prototype.structureVersion = 1

AccountProfileImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    accountId: this.accountId,
    firstName: this.firstName,
    lastName: this.lastName,
    gender: this.gender,
    dateOfBirth: this.dateOfBirth,
    zip: this.zip
  }
}

AccountProfileImpl.prototype.getKey = function () {
  return AccountProfile.getKey({ id: this.accountId, mode: this.mode })
}

AccountProfileImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/AccountProfile`
  }
}

AccountProfileImpl.prototype.getLinks = function () {
  return {
    versions: `/I/account/${this.accountId}/publicProfile/versions`
  }
}

AccountProfileImpl.prototype.invariants = AccountProfileContract.invariants

const AccountProfile = AccountProfileContract.implementation(AccountProfileImpl)

AccountProfile.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/account/${kwargs.id}/publicProfile`
})

module.exports = AccountProfile
