/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const putTransactionDynamodb = require('../../../aws/dynamodb/putTransaction')
const AccountProfile = require('./AccountProfile')
const AccountProfileDTO = require('../../../../api/account/AccountProfileDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')

const requiredValidateInputSchema = AccountProfileDTO.schema.required()
const requiredAccountPathParameters = AccountPathParameters.schema.required()

/**
 * @param {PointInTime} sot
 * @param {AccountId} sub
 * @param {Mode} mode
 * @param {Uuid} Uuid
 * @param {object} pathParameters
 * @param {any} body
 */
const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredAccountPathParameters, pathParameters)
  validateInputSchema(requiredValidateInputSchema, body)

  const accountProfile = new AccountProfile({
    mode,
    dto: { ...body, createdAt: sot, createdBy: sub, accountId: pathParameters.accountId }
  })

  const originalItem = accountProfile.toItem()
  originalItem.flowId = flowId

  return putTransactionDynamodb([
    originalItem,
    {
      ...originalItem,
      submitted: 'actual',
      sort_key_A: 'actual'
    }
  ])
})

module.exports = put
