/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const config = require('config')
const fetch = require('node-fetch')
const validateSchema = require('../../../../_util/validateSchema')
const TrackerConnectionInput = require('../../../../api/account/trackerConnection/TrackerConnectionInput')
const Mode = require('../../../../ppwcode/Mode')
const TokenType = require('./TokenType')
const Token = require('./Token')
const boom = require('@hapi/boom')
const Uuid = require('../../../../ppwcode/UUID')
const simpleLog = require('../../../../_util/simpleLog')
const requiredFlowId = Uuid.schema.required().label('flowId')

const getToken = new PromiseContract({
  pre: [
    function (mode) {
      return validateSchema(Mode.schema.required(), mode)
    },
    function (mode, flowId) {
      return validateSchema(requiredFlowId, flowId)
    },
    function (mode, flowId, input) {
      return validateSchema(TrackerConnectionInput.schema.required(), input)
    }
  ],
  post: [
    function () {
      return validateSchema(Token.schema.required(), PromiseContract.outcome(arguments))
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
}).implementation(async function getToken (mode, flowId, input) {
  const trackerConfig = ['production', 'demo', 'june2020'].includes(mode)
    ? input.trackerType !== 'polar'
      ? config.trackers[input.trackerType].prod
      : config.trackers[input.trackerType][mode]
    : config.trackers[input.trackerType].dev
  const url = trackerConfig.tokenEndpoint
  const clientId = trackerConfig.clientId
  const clientSecret = trackerConfig.clientSecret
  const params = new URLSearchParams()
  const headerBuf = Buffer.from(`${clientId}:${clientSecret}`, 'utf8')
  if (input.trackerType !== 'polar') {
    params.append('client_id', clientId)
    params.append('code_verifier', input.code_verifier)
  }
  params.append('grant_type', 'authorization_code')
  params.append('code', input.code)
  params.append('redirect_uri', input.redirectUrl)
  const headers = new fetch.Headers({ Authorization: `Basic ${headerBuf.toString('base64')}` })
  simpleLog.info(module, 'getToken', flowId, 'Fetching headers', {})
  const response = await fetch(url, { method: 'POST', body: params, headers })
  simpleLog.info(module, 'getToken', flowId, 'Fetching response', { response })
  const payload = await response.json()
  simpleLog.info(module, 'getToken', flowId, 'creating payload', { payload })
  const date = new Date()
  if (payload.errors) {
    const badRequest = boom.badRequest()
    const error = payload.errors[0]
    simpleLog.info(module, 'getToken', flowId, 'GET_TOKEN_ERROR', { error })
    badRequest.output.payload.error = error.error_type
    badRequest.output.payload.message = error.error_description
    throw badRequest
  }
  if (payload.expires_in) {
    date.setSeconds(date.getSeconds() + payload.expires_in)
  }
  simpleLog.info(module, 'getToken', flowId, 'GET_TOKEN_SUCCESS', {})
  return {
    token: payload.refresh_token || payload.access_token,
    tokenType: payload.refresh_token ? TokenType.refreshType : TokenType.accessType,
    exp: payload.refresh_token ? undefined : date.toISOString()
  }
})

module.exports = getToken
