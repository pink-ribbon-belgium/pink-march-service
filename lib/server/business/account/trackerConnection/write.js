/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise

const putTransaction = require('../../../aws/dynamodb/putTransaction')
const TrackerConnection = require('./TrackerConnection')

const validateSchema = require('../../../../_util/validateSchema')
const AccountId = require('../../../../api/account/AccountId')
const Mode = require('../../../../ppwcode/Mode')
const Uuid = require('../../../../ppwcode/UUID')
const PointInTime = require('../../../../ppwcode/PointInTime')
// const simpleLog = require('../../../../_util/simpleLog')
const TrackerConnectionSchema = require('../../../../../lib/server/business/account/trackerConnection/TrackerConnectionSchema')
const requiredSot = PointInTime.schema.required().label('sot')
const requiredSub = AccountId.schema.required().label('sub')
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = Uuid.schema.required().label('flowId')

const write = new PromiseContract({
  pre: [
    sot => validateSchema(requiredSot, sot),
    (sot, sub) => validateSchema(requiredSub, sub),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, trackerConnectionDto) =>
      validateSchema(TrackerConnectionSchema.connectedSchema, trackerConnectionDto)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function put (sot, sub, mode, flowId, trackerConnectionDto) {
  // simpleLog.info(module, 'write', flowId, 'WRITE TOKEN CALLED', {
  //   mode: mode,
  //   accountId: trackerConnectionDto.accountId
  // })
  const trackerConnection = new TrackerConnection({
    mode,
    /* NOTE: SECURITY: createdAt and createdBy are audit data. External users can add unknown
     *                  properties to the PUT body. We must take care that an external user's
     *                  `createdAt` and 'createdBy` does not overwrite the true values we put in. */
    dto: {
      ...trackerConnectionDto,
      createdAt: sot,
      createdBy: sub
    },
    sot,
    sub
  })

  // simpleLog.info(module, 'write', flowId, 'TRACKERCONNECTION CREATED', {
  //   mode: mode,
  //   accountId: trackerConnectionDto.accountId
  // })

  const item = {
    ...trackerConnection.toItem(),
    flowId
  }

  const submittedItem = {
    ...item,
    partition_key_A: undefined,
    sort_key_A: undefined,
    partition_key_B: undefined,
    sort_key_B: undefined,
    partition_key_C: undefined,
    sort_key_C: undefined
  }

  const actualItem = {
    ...item,
    submitted: 'actual',
    sort_key_A: sot,
    sort_key_B: sot
  }
  const result = putTransaction([submittedItem, actualItem])

  // simpleLog.info(module, 'write', flowId, 'NOMINAL END', {
  //   mode: mode,
  //   accountId: trackerConnectionDto.accountId
  // })

  return result
})

module.exports = write
