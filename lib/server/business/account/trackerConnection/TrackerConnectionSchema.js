/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const AccountId = require('../../../../api/account/AccountId')
const DTO = require('../../../../ppwcode/lambda/DTO')
const TrackerType = require('../../../../api/account/trackerConnection/TrackerType')
const TokenType = require('./TokenType')

const exampleConnected = {
  ...DTO.exampleDynamodb,
  accountId: AccountId.example,
  trackerType: 'fitbit',
  tokenType: 'refresh',
  encryptedToken: 'WA==',
  exp: '2020-03-31T11:05:52.191Z',
  hasError: false
}

const exampleDisconnected = {
  ...DTO.exampleDynamodb,
  accountId: AccountId.example,
  deleted: true
}

const connectedSchema = DTO.schema
  .keys({
    accountId: AccountId.schema.required(),
    trackerType: TrackerType.schema.required(),
    tokenType: TokenType.schema.required(),
    encryptedToken: Joi.string()
      .base64()
      .required(),
    exp: Joi.string()
      .isoDate()
      .allow(null),
    polarId: Joi.number().optional(),
    hasError: Joi.boolean().required()
  })
  .tailor('dynamodb')
  .example(exampleConnected, { override: true })

const disconnectedSchema = DTO.schema
  .keys({
    accountId: AccountId.schema.required(),
    deleted: Joi.bool().required()
  })
  .tailor('dynamodb')
  .example(exampleDisconnected, { override: true })

const schema = Joi.alternatives(connectedSchema.required(), disconnectedSchema.required())

module.exports = { schema, connectedSchema, disconnectedSchema, exampleConnected, exampleDisconnected }
