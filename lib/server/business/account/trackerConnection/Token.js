/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const TokenType = require('./TokenType')

const exampleAccess = {
  token: 'token',
  tokenType: TokenType.accessType,
  exp: '2011-10-05T14:48:00.000Z'
}

const exampleRefresh = {
  token: 'token',
  tokenType: TokenType.refreshType
}

const schema = Joi.object()
  .keys({
    token: Joi.string().required(),
    tokenType: TokenType.schema.required(),
    exp: Joi.string().isoDate()
  })
  .example(exampleAccess)
  .example(exampleRefresh)

module.exports = { exampleAccess, exampleRefresh, schema }
