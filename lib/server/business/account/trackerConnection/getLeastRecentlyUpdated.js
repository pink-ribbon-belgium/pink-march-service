/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const { Promise: PromiseContract } = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const { schema: UUID } = require('../../../../ppwcode/UUID')
const { schema: PointInTime } = require('../../../../ppwcode/PointInTime')
const { schema: Mode } = require('../../../../ppwcode/Mode')
const TrackerConnection = require('./TrackerConnectionSchema')
const Joi = require('@hapi/joi')
const log = require('../../../../_util/simpleLog')

const requiredUUID = UUID.required().label('flowId')
const requiredPointInTime = PointInTime.required().label('sot')
const requiredMode = Mode.required().label('mode')
const requiredNatural = Joi.number()
  .integer()
  .positive()
  .required()
  .label('nrOfTrackerConnectionsToPoll')
const requiredTrackerConnections = Joi.array()
  .items(TrackerConnection.schema)
  .min(0) // may be empty
  // max is dynamic
  .required()
  .label('result')
const dynamodb = require('../../../aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../../aws/dynamodb/dynamodbTableName')

const getLeastRecentlyUpdated = new PromiseContract({
  pre: [
    flowId => validateSchema(requiredUUID, flowId),
    (flowId, sot) => validateSchema(requiredPointInTime, sot),
    (flowId, sot, mode) => validateSchema(requiredMode, mode),
    (flowId, sot, mode, nrOfTrackerConnectionsToPoll) => validateSchema(requiredNatural, nrOfTrackerConnectionsToPoll)
  ],
  post: [
    (flowId, sot, mode, nrOfTrackerConnectionsToPoll, result) =>
      validateSchema(requiredTrackerConnections.max(nrOfTrackerConnectionsToPoll), result),
    (flowId, sot, mode, nrOfTrackerConnectionsToPoll, result) =>
      result.every(trackerConnection => trackerConnection.createdAt < sot)
  ]
  // no semantic exceptions; all exceptions are programming errors
}).implementation(async function getLeastRecentlyUpdated (flowId, sot, mode, nrOfTrackerConnectionsToPoll) {
  log.info(module, 'getLeastRecentlyUpdated', flowId, 'CALLED', {
    sot,
    mode,
    nrOfTrackerConnectionsToPoll
  })

  const indexA = {
    indexName: 'Index_A',
    partitionKey: 'partition_key_A',
    sortKey: 'sort_key_A'
  }

  const key = `/${mode}/trackerConnection`

  // MUDO: Refactor => Handle multiple results (see getByIndex)

  const dynamodbInstance = await dynamodb()
  const result = await dynamodbInstance
    .query({
      TableName: dynamodbTableName(mode),
      IndexName: indexA.indexName,
      ExpressionAttributeNames: {
        '#partitionKey': indexA.partitionKey,
        '#sortKey': indexA.sortKey
      },
      ExpressionAttributeValues: {
        ':partitionKey': key,
        ':sortKey': sot
      },
      KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey <= :sortKey',
      ScanIndexForward: true,
      Limit: nrOfTrackerConnectionsToPoll
    })
    .promise()

  const connections = result.Items.map(item => item.data)

  log.info(module, 'getLeastRecentlyUpdated', flowId, 'NOMINAL_END', {
    nrFound: connections.length,
    /* Do not log the entire trackerConnection. This is a) too much information for a log, and b) it contains sensitive
       data. Do make sure we can identify what was found. Keep this minimal here (more detail in `updateActivities`). */
    trackerConnections: result.Items.map(trackerConnection => ({
      trackerType: trackerConnection.trackerType,
      accountId: trackerConnection.accountId
    }))
  })
  return connections
})

module.exports = getLeastRecentlyUpdated
