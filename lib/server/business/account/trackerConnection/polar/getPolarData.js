/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const config = require('config')
const PromiseContract = require('@toryt/contracts-iv').Promise
const fetch = require('node-fetch')
const validateSchema = require('../../../../../_util/validateInputSchema')
const PointInTime = require('../../../../../ppwcode/PointInTime')
const Uuid = require('../../../../../ppwcode/UUID')
const Mode = require('../../../../../ppwcode/Mode')
const AccountId = require('../../../../../api/account/AccountId')
const writeTrackerConnection = require('../write')
const ActivityDataOutput = require('../../../../../api/activity/ActivityDataOutput')
const TrackerType = require('../../../../../api/account/trackerConnection/TrackerType')
const TrackerConnectionSchema = require('../TrackerConnectionSchema')
const decrypt = require('../../../../aws/crypto/decrypt')
const simpleLog = require('../../../../../_util/simpleLog')
const moment = require('moment-timezone')

const getPolarData = new PromiseContract({
  pre: [
    sot => validateSchema(PointInTime.schema.required().label('sot'), sot),
    (sot, sub) => validateSchema(AccountId.schema.required().label('sub'), sub),
    (sot, sub, mode) => validateSchema(Mode.schema.required().label('mode'), mode),
    (sot, sub, mode, flowId) => validateSchema(Uuid.schema.required().label('flowId'), flowId),
    (sot, sub, mode, flowId, trackerConnectionData) =>
      validateSchema(TrackerConnectionSchema.schema.required().label('trackerConnectionData'), trackerConnectionData)
  ],
  post: [
    function (sot, sub, mode, flowId, activityTrackerData, startdate, enddate, result) {
      return result.length ? result.every(x => validateSchema(ActivityDataOutput.schema.required(), x)) : true
    }
  ]
}).implementation(async function getPolarData (sot, sub, mode, flowId, trackerConnectionData, startDate, endDate) {
  const trackerType = TrackerType.polarType
  const accountId = trackerConnectionData.accountId
  const polarId = trackerConnectionData.polarId
  const trackerConfig = ['production', 'demo', 'june2020'].includes(mode)
    ? config.trackers[trackerType][mode]
    : config.trackers[trackerType].dev

  // Ensure that the tracker connection is updated ( for getting the least updated tracker connections)
  await writeTrackerConnection(sot, sub, mode, flowId, trackerConnectionData)

  const headerBuf = Buffer.from(`${trackerConfig.clientId}:${trackerConfig.clientSecret}`, 'utf8')

  const accessToken = await decrypt(trackerConnectionData.encryptedToken, {
    accountId: accountId,
    trackerType: trackerType
  })

  // Current polar flow client (polar accesslink-api) only available for development

  const headers = {
    Accept: 'application/json',
    Authorization: `Basic ${headerBuf.toString('base64')}`
  }

  const notificationResponse = await fetch('https://www.polaraccesslink.com/v3/notifications', {
    method: 'GET',
    headers: headers
  })

  let payload
  if (notificationResponse.status === 200) {
    payload = await notificationResponse.json()
    simpleLog.info(module, 'getPolarData', flowId, 'GET_NOTIFICATIONS', {
      mode: mode,
      payload: payload
    })
  } else {
    simpleLog.info(module, 'getPolarData', flowId, 'GET_NOTIFICATIONS_NO_DATA', {
      mode,
      accountId
    })
    return []
  }

  // Check available data for user
  const users = payload['available-user-data']
  simpleLog.info(module, 'getPolarData', flowId, 'GET_AVAILABLE_DATA', {
    mode,
    accountId,
    response: users
  })

  const userData = users.filter(user => user['user-id'] === polarId)

  let transactionUrl
  if (userData.length > 0) {
    transactionUrl = userData[0].url
    simpleLog.info(module, 'getPolarData', flowId, 'GET_AVAILABLE_USER_DATA_URL', {
      mode,
      accountId,
      transactionUrl: transactionUrl
    })
  } else {
    simpleLog.info(module, 'getPolarData', flowId, 'GET_AVAILABLE_USER_DATA_FAIL', {
      mode,
      accountId
    })
    return []
  }

  // getActivities from transaction
  const transactionHeader = {
    Authorization: `Bearer ${accessToken}`
  }

  const transactionResponse = await fetch(transactionUrl, { method: 'POST', headers: transactionHeader })

  simpleLog.info(module, 'getPolarData', flowId, 'GET_TRANSACTION_RESPONSE', {
    mode,
    accountId,
    response: transactionResponse
  })

  let transactionPayload
  if (transactionResponse.status === 201) {
    transactionPayload = await transactionResponse.json()
  } else {
    simpleLog.info(module, 'getPolarData', flowId, 'GET_TRANSACTION_RESPONSE_NO_DATA', {
      mode,
      accountId,
      response: transactionResponse
    })
    return []
  }

  // Get transactionId and resourceUri
  const transactionId = transactionPayload['transaction-id']
  const resourceUrl = transactionPayload['resource-uri']

  // Retrieve list activities
  const newActivitiesResponse = await fetch(resourceUrl, { method: 'GET', headers: transactionHeader })
  simpleLog.info(module, 'getPolarData', flowId, 'GET_ACTIVITY_SUMMARY_RESPONSE', {
    mode,
    accountId,
    response: newActivitiesResponse
  })

  let newActivitiesPayload
  if (newActivitiesResponse.status === 200) {
    newActivitiesPayload = await newActivitiesResponse.json()
  } else {
    return []
  }

  const activityUriList = newActivitiesPayload['activity-log']
  simpleLog.info(module, 'getPolarData', flowId, 'GET_ACTIVITY_SUMMARY_RESPONSE', {
    mode,
    accountId,
    activityList: activityUriList
  })

  // Get Activity Summary-
  const activityData = await Promise.allSettled(
    activityUriList.map(uri =>
      fetch(uri, {
        method: 'GET',
        headers: { Accept: 'application/json', Authorization: `Bearer ${accessToken}` }
      }).then(r => {
        if (r.status === 200) {
          return r.json()
        } else {
          return undefined
        }
      })
    )
  )

  simpleLog.info(module, 'getPolarData', flowId, 'GET_ACTIVITY_DATA', {
    mode,
    accountId,
    result: activityData
  })

  // Commit transaction
  const commitTransactionUrl = `https://www.polaraccesslink.com/v3/users/${polarId}/activity-transactions/${transactionId}`
  const commitTransactionResponse = await fetch(commitTransactionUrl, { method: 'PUT', headers: transactionHeader })
  simpleLog.info(module, 'getPolarData', flowId, 'COMMIT_TRANSACTION', {
    mode,
    accountId,
    response: commitTransactionResponse
  })

  const toUpdate = []
  const filtered = activityData.filter(item => item.value !== undefined)
  filtered.forEach(item => {
    const activityDate = item.value.date
    const index = toUpdate.length ? toUpdate.findIndex(activity => activity.date === activityDate) : -1
    if (index === -1) {
      toUpdate.push({
        accountId: accountId,
        trackerType: trackerType,
        date: item.value.date,
        steps: item.value['active-steps']
      })
    } else {
      /* istanbul ignore else */
      if (toUpdate[index].steps < item.value['active-steps']) {
        toUpdate[index] = {
          accountId: accountId,
          trackerType: trackerType,
          date: item.value.date,
          steps: item.value['active-steps']
        }
      }
    }
  })

  simpleLog.info(module, 'getPolarData', flowId, 'GET_DATA_NOMINAL_END', {
    mode,
    accountId,
    result: toUpdate
  })

  const result = toUpdate.map(activity => {
    return {
      ...activity,
      date: new Date(activity.date)
    }
  })

  let activityPeriodEndDate
  /* istanbul ignore next : we can't test different modes */
  switch (mode) {
    case 'dev-experiment':
    case 'demo':
    case 'june2020':
    case 'production':
      activityPeriodEndDate = new Date(config.activityPeriod[mode].enddate)
      break
    default:
      activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
      break
  }

  return result.filter(
    x =>
      x.date <
      moment
        .tz(activityPeriodEndDate, 'Europe/Brussels')
        .add(1, 'day')
        .toDate()
  )
})

module.exports = getPolarData
