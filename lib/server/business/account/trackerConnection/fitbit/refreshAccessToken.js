/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const config = require('config')
const fetch = require('node-fetch')
const validateSchema = require('../../../../../_util/validateSchema')

const writeTrackerConnection = require('../write')
const PointInTime = require('../../../../../ppwcode/PointInTime')
const Uuid = require('../../../../../ppwcode/UUID')
const Mode = require('../../../../../ppwcode/Mode')
const AccountId = require('../../../../../api/account/AccountId')
const TrackerConnectionSchema = require('../TrackerConnectionSchema')
const encrypt = require('../../../../aws/crypto/encrypt')
const decrypt = require('../../../../aws/crypto/decrypt')
const boom = require('@hapi/boom')
const simpleLog = require('../../../../../_util/simpleLog')

const refreshAccessToken = new PromiseContract({
  pre: [
    sot => validateSchema(PointInTime.schema.required().label('sot'), sot),
    (sot, sub) => validateSchema(AccountId.schema.required().label('sub'), sub),
    (sot, sub, mode) => validateSchema(Mode.schema.required().label('mode'), mode),
    (sot, sub, mode, flowId) => validateSchema(Uuid.schema.required().label('flowId'), flowId),
    (sot, sub, mode, flowId, trackerConnection) =>
      validateSchema(TrackerConnectionSchema.schema.required().label('trackerConnection'), trackerConnection)
  ],
  post: [
    function () {
      return validateSchema(Joi.string().required(), PromiseContract.outcome(arguments))
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
}).implementation(async function refreshAccessToken (sot, sub, mode, flowId, trackerConnectionData) {
  const trackerType = trackerConnectionData.trackerType
  const accountId = trackerConnectionData.accountId

  const refreshToken = await decrypt(trackerConnectionData.encryptedToken, {
    accountId: accountId,
    trackerType: trackerType
  })

  const trackerConfig = ['production', 'demo', 'june2020'].includes(mode)
    ? config.trackers[trackerType].prod
    : config.trackers[trackerType].dev
  const url = trackerConfig.tokenEndpoint
  const clientId = trackerConfig.clientId
  const clientSecret = trackerConfig.clientSecret
  const headerBuf = Buffer.from(`${clientId}:${clientSecret}`, 'utf8')

  const params = new URLSearchParams()
  params.append('grant_type', 'refresh_token')
  params.append('refresh_token', refreshToken)

  const headers = new fetch.Headers({ Authorization: `Basic ${headerBuf.toString('base64')}` })
  const response = await fetch(url, { method: 'POST', body: params, headers })
  const payload = await response.json()

  if (payload.errors) {
    const badRequest = boom.badRequest()
    const error = payload.errors[0]

    /* istanbul ignore next */
    if (error.statusCode === 401) {
      simpleLog.info(module, 'refreshAccessToken', flowId, 'REFRESH_ACCESSTOKEN_ERROR', { error })
    }
    // ToDo: error logging 500
    simpleLog.info(module, 'refreshAccessToken', flowId, 'REFRESH_ACCESSTOKEN_ERROR', { error })
    badRequest.output.payload.error = error.error_type
    badRequest.output.payload.message = error.error_description

    await writeTrackerConnection(sot, sub, mode, flowId, {
      ...trackerConnectionData,
      hasError: true
    })

    throw badRequest
  }

  // simpleLog.info(module, 'refreshAccessToken', flowId, 'REFRESH_ACCESSTOKEN_SUCCESS', {})

  const encryptedToken = await encrypt(payload.refresh_token, {
    accountId: accountId,
    trackerType: trackerType
  })

  const trackerData = {
    ...trackerConnectionData,
    encryptedToken: encryptedToken
  }

  await writeTrackerConnection(sot, sub, mode, flowId, trackerData)

  return payload.access_token
})

module.exports = refreshAccessToken
