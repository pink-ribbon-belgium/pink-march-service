/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const boom = require('@hapi/boom')
const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
// const config = require('config')
const fetch = require('node-fetch')
const moment = require('moment')
const validateSchema = require('../../../../../_util/validateSchema')

const TrackerType = require('../../../../../api/account/trackerConnection/TrackerType')
const PointInTime = require('../../../../../ppwcode/PointInTime')
const Uuid = require('../../../../../ppwcode/UUID')
const Mode = require('../../../../../ppwcode/Mode')
const AccountId = require('../../../../../api/account/AccountId')
const ActivityDataOutput = require('./../../../../../api/activity/ActivityDataOutput')
const TrackerConnectionSchema = require('../TrackerConnectionSchema')

const simpleLog = require('../../../../../_util/simpleLog')
const refreshAccessToken = require('./refreshAccessToken')

const dateSchema = Joi.date()

const getActivityData = new PromiseContract({
  pre: [
    sot => validateSchema(PointInTime.schema.required().label('sot'), sot),
    (sot, sub) => validateSchema(AccountId.schema.required().label('sub'), sub),
    (sot, sub, mode) => validateSchema(Mode.schema.required().label('mode'), mode),
    (sot, sub, mode, flowId) => validateSchema(Uuid.schema.required().label('flowId'), flowId),
    (sot, sub, mode, flowId, trackerConnectionData) =>
      validateSchema(TrackerConnectionSchema.schema.required().label('trackerConnectionData'), trackerConnectionData),
    (sot, sub, mode, flowId, trackerConnectionData, startDate) =>
      validateSchema(dateSchema.required().label('startDate'), startDate),
    (sot, sub, mode, flowId, trackerConnectionData, startdate, enddate) =>
      validateSchema(dateSchema.required().label('endDate'), enddate)
  ],
  post: [
    function (sot, sub, mode, flowId, activityTrackerData, startdate, enddate, result) {
      return result.every(x => validateSchema(ActivityDataOutput.schema.required(), x))
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
  // ToDo: replace accountID by trackerconnection
}).implementation(async function getActivityData (sot, sub, mode, flowId, trackerConnectionData, startDate, endDate) {
  const trackerType = TrackerType.fitbitType
  const accountId = trackerConnectionData.accountId

  const newAccessToken = await refreshAccessToken(sot, sub, mode, flowId, trackerConnectionData)

  const formattedEndDate = moment(endDate).format('YYYY-MM-DD')
  const formattedStartDate = moment(startDate).format('YYYY-MM-DD')

  // const trackerConfig = mode === 'production' ? config.trackers[trackerType].prod : config.trackers[trackerType].dev
  const url = `https://api.fitbit.com/1/user/-/activities/steps/date/${formattedEndDate}/${formattedStartDate}.json`

  const headers = new fetch.Headers({ Authorization: `Bearer  ${newAccessToken}` })
  const response = await fetch(url, { method: 'GET', headers })
  const payload = await response.json()

  if (payload.errors) {
    const badRequest = boom.badRequest()
    const error = payload.errors[0]

    simpleLog.error(module, 'getActivityData', flowId, 'FITBIT_GET_ACTIVITY_DATA_ERROR', { error })
    badRequest.output.payload.error = error.error_type
    badRequest.output.payload.message = error.error_description
    throw badRequest
  }

  const result = payload['activities-steps'].map(item => ({
    accountId: accountId,
    trackerType: trackerType,
    date: new Date(item.dateTime),
    steps: Number(item.value)
  }))

  return result
})

module.exports = getActivityData
