/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const getActual = require('../../../aws/dynamodb/getActual')
const TrackerConnection = require('./TrackerConnection')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const TrackerConnectionOutput = require('../../../../api/account/trackerConnection/TrackerConnectionOutput')

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, result) {
      return result.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, result) {
      return validateSchema(TrackerConnectionOutput.schema.required(), result)
    }
  ]),
  exception: ResourceActionContract.exception.concat(function () {
    return (
      PromiseContract.outcome(arguments).output.statusCode === 404 ||
      PromiseContract.outcome(arguments).output.statusCode === 400
    )
  })
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(AccountPathParameters.schema.required(), pathParameters)
  const key = `/${mode}/account/${pathParameters.accountId}/trackerConnection`
  const item = await getActual(mode, key, true)
  const connection = new TrackerConnection({ mode, dto: item.data })
  const result = {
    createdAt: connection.createdAt,
    createdBy: connection.createdBy,
    structureVersion: connection.structureVersion,
    accountId: connection.accountId,
    trackerType: connection.trackerType,
    exp: connection.exp,
    hasError: connection.deleted === true ? undefined : connection.hasError
  }

  return result
})
module.exports = get
