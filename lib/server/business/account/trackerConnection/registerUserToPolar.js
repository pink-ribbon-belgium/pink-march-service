/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const fetch = require('node-fetch')
const log = require('../../../../_util/simpleLog')

async function registerUserToPolar (mode, flowId, accountId, token) {
  log.info(module, 'registerPolarUser', flowId, 'ENTERING REGISTER_USER', {
    mode: mode,
    accountId: accountId
  })
  let id
  const url = 'https://www.polaraccesslink.com/v3/users'
  const inputBody = {
    'member-id': accountId
  }

  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `Bearer ${token}`
  }

  const response = await fetch(url, { method: 'POST', body: JSON.stringify(inputBody), headers: headers })
  if (response.status === 200) {
    const result = await response.json()
    id = result['polar-user-id']
    log.info(module, 'registerPolarUser', flowId, 'REGISTER_USER_SUCCESS', {
      mode: mode,
      accountId: accountId,
      polarId: id
    })
    return id
  } else {
    log.info(module, 'registerPolarUser', flowId, 'REGISTER_USER_FAIL', {
      mode: mode,
      accountId: accountId,
      error: response.status
    })
    return undefined
  }
}

module.exports = registerUserToPolar
