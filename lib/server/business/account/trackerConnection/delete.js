/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const fetch = require('node-fetch')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const decrypt = require('../../../aws/crypto/decrypt')
const getActual = require('../../../aws/dynamodb/getActual')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const TrackerConnection = require('./TrackerConnection')
const putTransaction = require('../../../aws/dynamodb/putTransaction')
const TrackerType = require('../../../../api/account/trackerConnection/TrackerType')
const simpleLog = require('../../../../_util/simpleLog')

const deleteTrackerConnection = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  exception: ResourceActionContract.exception.concat(function () {
    return (
      PromiseContract.outcome(arguments).output.statusCode === 404 ||
      PromiseContract.outcome(arguments).output.statusCode === 400
    )
  })
}).implementation(async function deleteTrackerConnection (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(AccountPathParameters.schema.required(), pathParameters)

  simpleLog.info(module, 'deleteTrackerConnection', flowId, 'CALLED', {
    mode: mode,
    accountId: pathParameters.accountId
  })

  const key = `/${mode}/account/${pathParameters.accountId}/trackerConnection`
  const existing = await getActual(mode, key, true)

  simpleLog.info(module, 'deleteTrackerConnection', flowId, 'TRACKERCONNECTION_FOUND', {
    mode: mode,
    accountId: pathParameters.accountId,
    trackerConnection: {
      ...existing.data,
      encryptedToken: undefined
    }
  })

  if (existing.data.deleted) {
    return undefined
  }

  if (existing.data.trackerType === TrackerType.googleType) {
    /*
    The refresh_token is only provided on the first authorization from the user.
    Subsequent authorizations, will not return the refresh_token again.
    Google will return an access_token instead of the refresh_token
    Therefore we need to revoke the permission from the google account
    */
    try {
      const decryptedToken = await decrypt(existing.data.encryptedToken, {
        accountId: existing.data.accountId,
        trackerType: existing.data.trackerType
      })
      const revokeUrl = `https://accounts.google.com/o/oauth2/revoke?token=${encodeURIComponent(decryptedToken)}`
      await fetch(revokeUrl)

      simpleLog.info(module, 'deleteTrackerConnection', flowId, 'REVOKE_GOOGLE_PERMISSION_SUCCESS', {
        mode: mode,
        accountId: existing.data.accountId
      })
    } catch (e) /* istanbul ignore next */ {
      simpleLog.info(module, 'deleteTrackerConnection', flowId, 'REVOKE_GOOGLE_PERMISSION_FAILED', {
        mode: mode,
        accountId: existing.data.accountId,
        error: e
      })
    }
  }

  /**
   * When partner wishes no longer to receive user data, user can be de-registered.
   * This will revoke the access token authorized by user.
   */
  if (existing.data.trackerType === TrackerType.polarType) {
    try {
      const decryptedToken = await decrypt(existing.data.encryptedToken, {
        accountId: existing.data.accountId,
        trackerType: existing.data.trackerType
      })
      const headers = {
        Authorization: `Bearer ${decryptedToken}`
      }
      const deregisterURL = `https://www.polaraccesslink.com/v3/users/${existing.data.polarId}`
      await fetch(deregisterURL, { method: 'DELETE', headers: headers })
      simpleLog.info(module, 'deleteTrackerConnection', flowId, 'REVOKE_POLAR_PERMISSION_SUCCESS', {
        mode: mode,
        accountId: existing.data.accountId,
        polarId: existing.data.polarId
      })
    } catch (e) {
      /* istanbul ignore next */
      simpleLog.info(module, 'deleteTrackerConnection', flowId, 'REVOKE_POLAR_PERMISSION_FAILED', {
        mode: mode,
        accountId: existing.data.accountId,
        polarId: existing.data.polarId,
        error: e
      })
    }
  }

  const trackerConnection = new TrackerConnection({
    mode,
    dto: {
      createdAt: sot,
      createdBy: sub,
      structureVersion: 1,
      accountId: pathParameters.accountId,
      deleted: true
    }
  })

  const originalItem = { ...trackerConnection.toItem(), flowId }

  await putTransaction([
    {
      ...originalItem,
      sort_key_A: undefined,
      sort_key_B: undefined
    },
    { ...originalItem, submitted: 'actual', sort_key_A: sot, sort_key_B: sot }
  ])

  simpleLog.info(module, 'deleteTrackerConnection', flowId, 'NOMINAL_END', {
    mode: mode,
    accountId: pathParameters.accountId,
    trackerConnection: {
      ...originalItem.data
    }
  })
})

module.exports = deleteTrackerConnection
