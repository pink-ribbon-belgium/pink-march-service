/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const config = require('config')
const fetch = require('node-fetch')
const validateSchema = require('../../../../../_util/validateInputSchema')

const writeTrackerConnection = require('../write')
const PointInTime = require('../../../../../ppwcode/PointInTime')
const Uuid = require('../../../../../ppwcode/UUID')
const Mode = require('../../../../../ppwcode/Mode')
const AccountId = require('../../../../../api/account/AccountId')
const TrackerConnectionSchema = require('../TrackerConnectionSchema')
// const encrypt = require('../../../../aws/crypto/encrypt')
const decrypt = require('../../../../aws/crypto/decrypt')
const boom = require('@hapi/boom')
const simpleLog = require('../../../../../_util/simpleLog')

const refreshGoogleToken = new PromiseContract({
  pre: [
    sot => validateSchema(PointInTime.schema.required().label('sot'), sot),
    (sot, sub) => validateSchema(AccountId.schema.required().label('sub'), sub),
    (sot, sub, mode) => validateSchema(Mode.schema.required().label('mode'), mode),
    (sot, sub, mode, flowId) => validateSchema(Uuid.schema.required().label('flowId'), flowId),
    (sot, sub, mode, flowId, trackerConnection) =>
      validateSchema(TrackerConnectionSchema.schema.required().label('trackerConnection'), trackerConnection)
  ],
  post: [
    function () {
      return validateSchema(Joi.string().required(), PromiseContract.outcome(arguments))
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
}).implementation(async function refreshGoogleToken (sot, sub, mode, flowId, trackerConnectionData) {
  const trackerType = trackerConnectionData.trackerType
  const accountId = trackerConnectionData.accountId

  // simpleLog.info(module, 'refreshGoogleToken', flowId, 'REFRESH_GOOGLE_ACCESSTOKEN_CALLED', {
  //   mode: mode,
  //   accountId: accountId
  // })

  const refreshToken = await decrypt(trackerConnectionData.encryptedToken, {
    accountId: accountId,
    trackerType: trackerType
  })
  const trackerConfig = ['production', 'demo', 'june2020'].includes(mode)
    ? config.trackers[trackerType].prod
    : config.trackers[trackerType].dev
  const url = trackerConfig.tokenEndpoint
  const clientId = trackerConfig.clientId
  const clientSecret = trackerConfig.clientSecret
  const grandType = 'refresh_token'

  // simpleLog.info(module, 'refreshGoogleToken', flowId, 'REFRESH_GOOGLE_ACCESSTOKEN_CONFIG', {
  //   mode: mode,
  //   accountId: accountId,
  //   clientId: clientId,
  //   tokenUrl: url
  // })

  const params = new URLSearchParams()
  params.append('client_id', clientId)
  params.append('client_secret', clientSecret)
  params.append('grant_type', grandType)
  params.append('refresh_token', refreshToken)

  const response = await fetch(url, { method: 'POST', body: params })
  const payload = await response.json()

  if (payload.error) {
    const badRequest = boom.badRequest()
    const error = payload.error

    // Note: usefull information: https://blog.timekit.io/google-oauth-invalid-grant-nightmare-and-how-to-fix-it-9f4efaf1da35
    simpleLog.info(module, 'refreshGoogleToken', flowId, 'REFRESH_GOOGLE_ACCESSTOKEN_ERROR', {
      mode: mode,
      accountId: accountId,
      error: error,
      errorDescription: payload.error_description
    })

    await writeTrackerConnection(sot, sub, mode, flowId, {
      ...trackerConnectionData,
      hasError: true
    })
    throw badRequest
  }

  // Just write the trackerConnection without updating the encrypted token
  // Google returns a refresh token at initial connection
  // This refresh token is used for getting an access token that's needed to make the call
  // for more information seet: https://developers.google.com/identity/protocols/oauth2
  await writeTrackerConnection(sot, sub, mode, flowId, trackerConnectionData)

  // simpleLog.info(module, 'refreshGoogleToken', flowId, 'REFRESH_GOOGLE_ACCESSTOKEN_SUCCESS', {
  //   mode: mode,
  //   accountId: accountId
  // })

  return payload.access_token
})

module.exports = refreshGoogleToken
