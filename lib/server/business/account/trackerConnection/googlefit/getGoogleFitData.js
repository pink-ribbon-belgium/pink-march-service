/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const boom = require('@hapi/boom')
const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const fetch = require('node-fetch')
const validateSchema = require('../../../../../_util/validateInputSchema')
const PointInTime = require('../../../../../ppwcode/PointInTime')
const Uuid = require('../../../../../ppwcode/UUID')
const Mode = require('../../../../../ppwcode/Mode')
const AccountId = require('../../../../../api/account/AccountId')
const ActivityDataOutput = require('../../../../../api/activity/ActivityDataOutput')
const TrackerType = require('../../../../../api/account/trackerConnection/TrackerType')
const TrackerConnectionSchema = require('../TrackerConnectionSchema')
const moment = require('moment-timezone')
// const util = require('util')

const simpleLog = require('../../../../../_util/simpleLog')
const refreshGoogleToken = require('./refreshGoogleToken')
const dateSchema = Joi.date()

const getGoogleFitData = new PromiseContract({
  pre: [
    sot => validateSchema(PointInTime.schema.required().label('sot'), sot),
    (sot, sub) => validateSchema(AccountId.schema.required().label('sub'), sub),
    (sot, sub, mode) => validateSchema(Mode.schema.required().label('mode'), mode),
    (sot, sub, mode, flowId) => validateSchema(Uuid.schema.required().label('flowId'), flowId),
    (sot, sub, mode, flowId, trackerConnectionData) =>
      validateSchema(TrackerConnectionSchema.schema.required().label('trackerConnectionData'), trackerConnectionData),
    (sot, sub, mode, flowId, trackerConnectionData, startDate) =>
      validateSchema(dateSchema.required().label('startDate'), startDate),
    (sot, sub, mode, flowId, trackerConnectionData, startdate, enddate) =>
      validateSchema(dateSchema.required().label('endDate'), enddate)
  ],
  post: [
    function (sot, sub, mode, flowId, activityTrackerData, startdate, enddate, result) {
      return result.every(x => validateSchema(ActivityDataOutput.schema.required(), x))
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    }
  ]
}).implementation(async function getGoogleFitData (sot, sub, mode, flowId, trackerConnectionData, startDate, endDate) {
  const trackerType = TrackerType.googleType
  const accountId = trackerConnectionData.accountId
  const newAccessToken = await refreshGoogleToken(sot, sub, mode, flowId, trackerConnectionData)

  // simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOGLE_FIT_DATA_CALLED', {
  //   mode: mode,
  //   accountId: accountId,
  //   trackterType: trackerType,
  //   startDate: startDate.toString(),
  //   endDate: endDate.toString()
  // })

  const timezone = 'Europe/Brussels'

  const realStartDate = moment
    .tz(startDate, timezone)
    .startOf('day')
    .toDate()

  const realEndDate = moment
    .tz(endDate, timezone)
    .endOf('day')
    .toDate()

  // simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOGLE_FIT_DATA_DATES_MANIPULATED', {
  //   mode: mode,
  //   accountId: accountId,
  //   trackterType: trackerType,
  //   startDate: realStartDate.toString(),
  //   endDate: realEndDate.toString()
  // })

  const startTimeMillis = realStartDate.getTime()
  const endTimeMillis = realEndDate.getTime()

  const url = 'https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate'
  const bodyString = {
    aggregateBy: [
      {
        dataTypeName: 'com.google.step_count.delta',
        dataSourceId: 'derived:com.google.step_count.delta:com.google.android.gms:estimated_steps'
      }
    ],
    bucketByTime: { durationMillis: 86400000 },
    startTimeMillis: startTimeMillis,
    endTimeMillis: endTimeMillis
  }

  // simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOGLE_FIT_DATA_PREPARED', {
  //   mode: mode,
  //   accountId: accountId,
  //   trackterType: trackerType,
  //   body: bodyString
  // })

  const response = await fetch(url, {
    method: 'POST',
    body: JSON.stringify(bodyString),
    headers: { 'Content-type': 'application/json', Authorization: `Bearer ${newAccessToken}` }
  })

  const payload = await response.json()

  if (payload.error) {
    const badRequest = boom.badRequest()
    const error = payload.error
    simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOLGE_FIT_DATA_ERROR', {
      mode,
      accountId,
      error
    })
    throw badRequest
  }

  // simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOLGE_FIT_DATA_RESPONSE', {
  //   mode,
  //   accountId,
  //   response: payload
  // })

  // console.log(`payload : ${util.inspect(payload, false, null, true /* enable colors */)}`)

  const result = payload.bucket.map(item => {
    const resultDate = new Date(parseInt(item.startTimeMillis, 10))

    const finalDate = new Date(moment.tz(resultDate, timezone).format('YYYY-MM-DD'))

    if (item.dataset[0].point.length > 0) {
      return {
        accountId: accountId,
        trackerType: trackerType,
        date: finalDate,
        steps: item.dataset[0].point[0].value[0].intVal
      }
    }
  })

  // const resultForLogging = result
  //   .filter(item => item !== undefined)
  //   .map(item => {
  //     const tmp = { ...item, date: item.date.toString() }
  //     return tmp
  //   })

  // simpleLog.info(module, 'getGoogleFitData', flowId, 'GET_GOOLGE_FIT_DATA_NOMINAL_END', {
  //   mode,
  //   accountId,
  //   result: resultForLogging
  // })

  return result.filter(item => item !== undefined)
})

module.exports = getGoogleFitData
