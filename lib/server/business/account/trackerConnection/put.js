/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const TrackerConnectionInputDTO = require('../../../../api/account/trackerConnection/TrackerConnectionInput')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const TrackerType = require('../../../../api/account/trackerConnection/TrackerType')
const TokenType = require('./TokenType')
const getToken = require('./getToken')
const encrypt = require('../../../aws/crypto/encrypt')
const log = require('../../../../_util/simpleLog')
const registerUserToPolar = require('./registerUserToPolar')
const writeTrackerConnection = require('../../account/trackerConnection/write')

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.slice()
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(AccountPathParameters.schema.required(), pathParameters)
  validateInputSchema(TrackerConnectionInputDTO.schema.required(), body)
  log.info(module, 'put', flowId, 'ENTERING PUT', {
    createdAt: sot,
    createdBy: sub,
    mode: mode,
    accountId: pathParameters.accountId,
    trackerType: body.trackerType,
    body: body
  })

  const token =
    body.trackerType === TrackerType.automatedTestType
      ? { token: 'token', tokenType: TokenType.refreshType }
      : await getToken(mode, flowId, body)

  log.info(module, 'put', flowId, 'TOKEN RECIEVED', {
    mode: mode,
    accountId: pathParameters.accountId
  })

  const encryptedToken = await encrypt(token.token, {
    accountId: pathParameters.accountId,
    trackerType: body.trackerType
  })

  log.info(module, 'put', flowId, 'TOKEN ENCRYPTED', {
    mode: mode,
    accountId: pathParameters.accountId
  })
  let polarIdUser
  if (body.trackerType === 'polar') {
    polarIdUser = await registerUserToPolar(mode, flowId, pathParameters.accountId, token.token, flowId)

    log.info(module, 'put', flowId, 'GET POLAR_ID', {
      mode: mode,
      accountId: pathParameters.accountId,
      polarId: polarIdUser
    })
  }
  await writeTrackerConnection(sot, sub, mode, flowId, {
    createdAt: sot,
    createdBy: sub,
    accountId: pathParameters.accountId,
    polarId: polarIdUser,
    trackerType: body.trackerType,
    tokenType: token.tokenType,
    encryptedToken,
    exp: token.exp,
    hasError: body.hasError ? body.hasError : false,
    structureVersion: 1
  })

  log.info(module, 'put', flowId, 'NOMINAL END', {
    mode: mode,
    accountId: pathParameters.accountId
  })
})

module.exports = put
