/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const TrackerConnectionDto = require('./TrackerConnectionSchema')

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: TrackerConnectionDto.schema.required() })

const TrackerConnectionContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.trackerType === kwargs.dto.trackerType
    },
    function (kwargs) {
      return this.tokenType === kwargs.dto.tokenType
    },
    function (kwargs) {
      return this.encryptedToken === kwargs.dto.encryptedToken
    },
    function (kwargs) {
      return this.exp === kwargs.dto.exp
    },
    function (kwargs) {
      return this.polarId === kwargs.dto.polarId
    },
    function (kwargs) {
      return this.hasError === kwargs.dto.hasError
    },
    function (kwargs) {
      return this.deleted === kwargs.dto.deleted
    }
  ])
})

TrackerConnectionContract.Kwargs = Kwargs

TrackerConnectionContract.methods = { ...PersistentObject.contract.methods }

TrackerConnectionContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().trackerType === this.trackerType
  },
  function () {
    return this.toJSON().tokenType === this.tokenType
  },
  function () {
    return this.toJSON().encryptedToken === this.encryptedToken
  },
  function () {
    return this.toJSON().exp === this.exp
  },
  function () {
    return this.toJSON().polarId === this.polarId
  },
  function () {
    return this.toJSON().hasError === this.hasError
  },
  function () {
    return this.toJSON().deleted === this.deleted
  },
  function () {
    return validateSchema(TrackerConnectionDto.schema.required(), this.toJSON())
  }
])

function TrackerConnectionImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.trackerType = kwargs.dto.trackerType
  this.tokenType = kwargs.dto.tokenType
  this.encryptedToken = kwargs.dto.encryptedToken
  this.exp = kwargs.dto.exp
  this.deleted = kwargs.dto.deleted
  this.hasError = kwargs.dto.hasError
  this.polarId = kwargs.dto.polarId
}

TrackerConnectionImpl.prototype = Object.create(PersistentObject.prototype)
TrackerConnectionImpl.prototype.constructor = TrackerConnectionImpl

TrackerConnectionImpl.prototype.toJSON = function () {
  return JSON.parse(
    JSON.stringify({
      ...PersistentObject.prototype.toJSON.call(this),
      accountId: this.accountId,
      trackerType: this.trackerType,
      tokenType: this.tokenType,
      encryptedToken: this.encryptedToken,
      exp: this.exp,
      hasError: this.hasError,
      deleted: this.deleted,
      polarId: this.polarId
    })
  )
}

TrackerConnectionImpl.prototype.getKey = function () {
  return TrackerConnection.getKey({ id: this.accountId, mode: this.mode })
}

TrackerConnectionImpl.prototype.getLinks = function () {
  return undefined
}

TrackerConnectionImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: !this.deleted ? `/${this.mode}/trackerConnection` : undefined,
    B: !this.deleted ? `/${this.mode}/trackerConnection/${this.trackerType}` : undefined
  }
}

TrackerConnectionImpl.prototype.invariants = TrackerConnectionContract.invariants

const TrackerConnection = TrackerConnectionContract.implementation(TrackerConnectionImpl)

TrackerConnection.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/account/${kwargs.id}/trackerConnection`
})

module.exports = TrackerConnection
