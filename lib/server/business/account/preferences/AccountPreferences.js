/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../../_util/validateSchema')
const PersistentObject = require('../../PersistentObject')
const AccountId = require('../../../../api/account/AccountId')
const AccountPreferencesDTO = require('../../../../api/account/AccountPreferencesDTO')
const VerifiedEmail = require('../../../../api/account/VerifiedEmail')

const requiredAccountId = AccountId.schema.required()
const requiredVerifiedEmail = VerifiedEmail.schema.required()

const dynamodbDTO = AccountPreferencesDTO.schema.tailor('dynamodb').required()
const outputDTO = AccountPreferencesDTO.schema.tailor('output').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const AccountPreferencesContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.language === kwargs.dto.language
    },
    function (kwargs) {
      return this.newsletter === kwargs.dto.newsletter
    },
    function (kwargs) {
      return this.knownFromType === kwargs.dto.knownFromType
    },
    function (kwargs) {
      return this.otherText === kwargs.dto.otherText
    }
  ])
})

AccountPreferencesContract.Kwargs = Kwargs

AccountPreferencesContract.methods = { ...PersistentObject.contract.methods }

AccountPreferencesContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return this.structureVersion === 2
  },
  function () {
    return validateSchema(requiredAccountId, this.accountId)
  },
  function () {
    return validateSchema(requiredVerifiedEmail, this.verifiedEmail)
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().language === this.language
  },
  function () {
    return this.toJSON().newsletter === this.newsletter
  },
  function () {
    return this.toJSON().knownFromType === this.knownFromType
  },
  function () {
    return this.toJSON().otherText === this.otherText
  },
  function () {
    return validateSchema(outputDTO, this.toJSON())
  }
])

function AccountPreferencesImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.language = kwargs.dto.language
  this.verifiedEmail = kwargs.dto.verifiedEmail
  this.newsletter = kwargs.dto.newsletter
  this.knownFromType = kwargs.dto.knownFromType
  this.otherText = kwargs.dto.otherText
}

AccountPreferencesImpl.prototype = Object.create(PersistentObject.prototype)
AccountPreferencesImpl.prototype.constructor = AccountPreferencesImpl

AccountPreferencesImpl.prototype.accountId = undefined

AccountPreferencesImpl.prototype.structureVersion = 2

AccountPreferencesImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    accountId: this.accountId,
    language: this.language,
    verifiedEmail: this.verifiedEmail,
    newsletter: this.newsletter,
    knownFromType: this.knownFromType,
    otherText: this.otherText
  }
}

AccountPreferencesImpl.prototype.getKey = function () {
  return AccountPreferences.getKey({ id: this.accountId, mode: this.mode })
}

AccountPreferencesImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: `/${this.mode}/accountPreference`
  }
}

AccountPreferencesImpl.prototype.getLinks = function () {
  return {
    versions: `/I/account/${this.accountId}/publicProfile/versions`
  }
}

AccountPreferencesImpl.prototype.invariants = AccountPreferencesContract.invariants

const AccountPreferences = AccountPreferencesContract.implementation(AccountPreferencesImpl)

AccountPreferences.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/account/${kwargs.id}/preferences`
})

module.exports = AccountPreferences
