/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const validateSchema = require('../../../../_util/validateSchema')
const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../../aws/dynamodb/getVersionAtOrBefore')
const AccountPreferences = require('./AccountPreferences')
const AccountPreferencesDTO = require('../../../../api/account/AccountPreferencesDTO')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')

const requiredAccountPreferences = AccountPreferencesDTO.schema.tailor('output').required()
const requiredAccountPathParameters = AccountPathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredAccountPreferences, response)
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredAccountPathParameters, pathParameters)
  const key = `/${mode}/account/${pathParameters.accountId}/preferences` // TODO move to key calculation to AccountPreferences static?

  const result = await getVersionAtOrBefore(mode, key, sot)

  const accountPreferences = new AccountPreferences({
    mode,
    dto: {
      ...result.data
    }
  })
  const accountPreferencesJson = accountPreferences.toJSON()
  accountPreferencesJson.links = accountPreferences.getLinks()
  return accountPreferencesJson
})

module.exports = get
