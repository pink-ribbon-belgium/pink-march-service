/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const { Promise: PromiseContract } = require('@toryt/contracts-iv')
const moment = require('moment')
const validateSchema = require('../../../../_util/validateSchema')
const { schema: UUID } = require('../../../../ppwcode/UUID')
const { schema: PointInTime } = require('../../../../ppwcode/PointInTime')
const { schema: Mode } = require('../../../../ppwcode/Mode')
const log = require('../../../../_util/simpleLog')
const config = require('config')

const getActivityDataFitbit = require('../trackerConnection/fitbit/getActivityData')
const getActivityDataGoogleFit = require('../trackerConnection/googlefit/getGoogleFitData')
// const getActivityDataPolar = require('../trackerConnection/polar/getPolarData')
const getLastActivity = require('../../activity/getLastActivity')
const putActivity = require('../../activity/put')
const writeTrackerConnection = require('../../../business/account/trackerConnection/write')
const TrackerType = require('../../../../api/account/trackerConnection/TrackerType')
const TrackerConnectionSchema = require('../trackerConnection/TrackerConnectionSchema')
const calculateDistance = require('../../../../api/activity/ActivityCalculations')
const getAccountProfile = require('../../account/profile/get')

const requiredUUID = UUID.required().label('flowId')
const requiredPointInTime = PointInTime.required().label('sot')
const requiredMode = Mode.required().label('mode')
const requiredTrackerConnection = TrackerConnectionSchema.schema.required().label('trackerConnection')

const sub = 'updateActivities' // sub that represents this process

function logTrackerConnection (mode, sot, flowId, phase, trackerConnection, ex) {
  log.info(module, 'updateActivities', flowId, phase, {
    sot,
    mode,
    accountId: trackerConnection.accountId,
    exception: ex === undefined ? 'N/A' : ex,
    trackerConnection: {
      createdAt: trackerConnection.createdAt,
      createdBy: trackerConnection.createdBy,
      structureVersion: trackerConnection.structureVersion,
      id: trackerConnection.id,
      accountId: trackerConnection.accountId,
      exp: trackerConnection.exp
    } // do not log the sensitive information in the trackerConnection
  })
}

const updateActivities = new PromiseContract({
  pre: [
    flowId => validateSchema(requiredUUID, flowId),
    (flowId, sot) => validateSchema(requiredPointInTime, sot),
    (flowId, sot, mode) => validateSchema(requiredMode, mode),
    (flowId, sot, mode, trackerConnection) => validateSchema(requiredTrackerConnection, trackerConnection)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function updateActivities (flowId, sot, mode, trackerConnection) {
  /* istanbul ignore next */
  logTrackerConnection(mode, sot, flowId, 'CALLED', trackerConnection)

  try {
    // 1) Get Activity Data from tracker
    let startDate
    let endDate = new Date()

    let activityPeriodStartDate
    let activityPeriodEndDate

    /* istanbul ignore next : we can't test different modes */
    switch (mode) {
      case 'dev-experiment':
      case 'demo':
      case 'june2020':
      case 'production':
        activityPeriodStartDate = new Date(config.activityPeriod[mode].startdate)
        activityPeriodEndDate = new Date(config.activityPeriod[mode].enddate)
        break
      default:
        activityPeriodStartDate = new Date(config.activityPeriod.default.startdate)
        activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
        break
    }

    try {
      const lastActivity = await getLastActivity(mode, trackerConnection.accountId)
      // Don't replace manual activities
      /* prettier-ignore */
      startDate =
        lastActivity.trackerType !== TrackerType.manualType
          ? moment(lastActivity.activityDate).toDate()
          : moment(lastActivity.activityDate)
            .add(1, 'days')
            .toDate()

      startDate = new Date(Math.max.apply(null, [startDate, activityPeriodStartDate]))
      endDate = new Date(Math.min.apply(null, [endDate, activityPeriodEndDate]))

      if (startDate > endDate) {
        log.info(module, 'updateActivities', flowId, 'NOTHING TO UPDATE', {
          mode,
          lastActivityDate: lastActivity.activityDate,
          lastActivityType: lastActivity.trackerType,
          accountId: trackerConnection.accountId,
          startDate: startDate.toISOString(),
          endDate: endDate.toISOString()
        })

        await writeTrackerConnection(sot, sub, mode, flowId, trackerConnection)

        return
      }
    } catch (ignore) /* istanbul ignore next */ {
      startDate = activityPeriodStartDate
    }

    const profile = await getAccountProfile(sot, sub, mode, flowId, { accountId: trackerConnection.accountId })
    let newActivityData

    // log.info(module, 'updateActivities', flowId, 'UPDATE_ACTIVITIES_GET_ACTIVITY_DATA', {
    //   mode,
    //   accountId: trackerConnection.accountId,
    //   trackerType: trackerConnection.trackerType,
    //   startDate: startDate.toISOString(),
    //   endDate: endDate.toISOString()
    // })

    switch (trackerConnection.trackerType) {
      case TrackerType.fitbitType:
        newActivityData = await getActivityDataFitbit(sot, sub, mode, flowId, trackerConnection, startDate, endDate)
        break
      case TrackerType.googleType:
        newActivityData = await getActivityDataGoogleFit(sot, sub, mode, flowId, trackerConnection, startDate, endDate)
        break
      // case TrackerType.polarType:
      //   newActivityData = await getActivityDataPolar(sot, sub, mode, flowId, trackerConnection, startDate, endDate)
      //   break
      default:
      // Do nothing
    }

    // 2) Process Activity Data from tracker
    const outcomes = await Promise.allSettled(
      newActivityData.map(activityData =>
        putActivity(
          sot,
          sub,
          mode,
          flowId,
          { accountId: trackerConnection.accountId, activityDate: activityData.date.toISOString() },
          {
            createdAt: sot,
            createdBy: sub,
            structureVersion: 1,
            accountId: activityData.accountId,
            activityDate: activityData.date.toISOString(),
            trackerType: activityData.trackerType,
            numberOfSteps: activityData.steps,
            distance: Math.round(calculateDistance.calculateDistanceFromSteps(profile.gender, activityData.steps, 0)),
            deleted: false
          }
        )
      )
    )
    const errors = outcomes.filter(o => o.status === 'rejected')

    /* istanbul ignore next */
    if (errors.length > 0) {
      outcomes.forEach(o => {
        const errorReason = o.reason
        log.info(module, 'updateActivities', flowId, 'PUT_ACTIVITY_FAILED', {
          mode,
          errorReason,
          startDate: startDate.toISOString(),
          endDate: endDate.toISOString()
        })
      })
    }

    /* istanbul ignore next */
    logTrackerConnection(mode, sot, flowId, 'NOMINAL_END', trackerConnection)
  } catch (e) {
    /* istanbul ignore next */
    logTrackerConnection(mode, sot, flowId, 'NOMINAL_END_CATCHED_ERROR', trackerConnection, e)
  }
})

module.exports = updateActivities
