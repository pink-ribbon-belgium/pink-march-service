/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../../_util/validateSchema')
const ActivityDTO = require('../../../../api/activity/ActivityDTO')
const Boom = require('@hapi/boom')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const validateInputSchema = require('../../../../_util/validateInputSchema')

const requiredPathParams = AccountPathParameters.schema.required()
const requiredActivity = ActivityDTO.schema.required().label('activity')
const dynamodb = require('../../../aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../../aws/dynamodb/dynamodbTableName')
// const log = require('../../../../_util/simpleLog')

const getActivities = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(item => item.accountId === pathParameters.accountId)
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.every(item => validateSchema(requiredActivity, item))
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ])
}).implementation(async function getActivities (sot, sub, mode, flowId, pathParameters) {
  // log.info(module, 'getActivitiesByAccountId', flowId, 'CALLED', {
  //   mode: mode,
  //   accountId: pathParameters.accountId
  // })

  validateInputSchema(requiredPathParams, pathParameters)
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const sortKeyValue = new Date().toISOString()
  const key = `/${mode}/activity/account/${pathParameters.accountId}`

  // MUDO: Refactor => Handle multiple results (see getByIndex)
  const dynamodbInstance = await dynamodb()
  const result = await dynamodbInstance
    .query({
      TableName: dynamodbTableName(mode),
      IndexName: index.indexName,
      ExpressionAttributeNames: {
        '#partitionKey': index.partitionKey,
        '#sortKey': index.sortKey
      },
      ExpressionAttributeValues: {
        ':partitionKey': key,
        ':sortKey': sortKeyValue
      },
      KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey <= :sortKey',
      ScanIndexForward: false // sort items descending
    })
    .promise()

  if (result.Items.length <= 0) {
    throw Boom.notFound()
  }

  const activities = result.Items.map(item => item.data)
  // log.info(module, 'getActivitiesByAccountId', flowId, 'NOMINAL_END', {
  //   nrFound: activities.length
  // })
  return activities
})

module.exports = getActivities
