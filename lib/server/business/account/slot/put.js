/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const writeSlot = require('../../slot/write')
const SlotDTO = require('../../../../api/slot/SlotDTO')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const validateSchema = require('../../../../_util/validateSchema')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')

const requiredSlotSchema = SlotDTO.schema.tailor('input').required()
const requiredAccountPathParameters = AccountPathParameters.schema.required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredSlotSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 400
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredAccountPathParameters, pathParameters)
  validateInputSchema(requiredSlotSchema, body)

  await writeSlot(sot, sub, mode, flowId, { ...body, accountId: pathParameters.accountId })
})

module.exports = put
