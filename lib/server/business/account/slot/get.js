/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getByIndex = require('../../../aws/dynamodb/getByIndex')
const Slot = require('../../slot/Slot')
const SlotDTO = require('../../../../api/slot/SlotDTO')
const AccountPathParameters = require('../../../../api/account/AccountPathParameters')
const validateSchema = require('../../../../_util/validateSchema')
const validateInputSchema = require('../../../../_util/validateInputSchema')
const upgrade = require('../../group/_upgradeWithGroupType')

const requiredSlotSchema = SlotDTO.schema.tailor('output').required()
const requiredAccountPathParameters = AccountPathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredSlotSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredAccountPathParameters, pathParameters)

  const index = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }

  const partitionKey = `/${mode}/account/${pathParameters.accountId}/slot`
  const result = await getByIndex(mode, partitionKey, index, 'actual')

  const [upgraded] = await upgrade(sot, sub, mode, flowId, result)

  const slot = new Slot({
    mode,
    dto: upgraded.data
  })

  const slotJson = slot.toJSON()
  slotJson.links = slot.getLinks()
  return slotJson
})

module.exports = get
