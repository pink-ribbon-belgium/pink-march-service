/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../_util/validateSchema')
const AccountId = require('../../api/account/AccountId')
const PointInTime = require('../../ppwcode/PointInTime')
const StructureVersion = require('../../ppwcode/StructureVersion')
const Mode = require('../../ppwcode/Mode')
const DTO = require('../../ppwcode/lambda/DTO')
const Joi = require('@hapi/joi')
const Item = require('../aws/dynamodb/Item').schema
const _ = require('lodash')
const Key = require('../../ppwcode/dynamodb/Key')
const Link = require('../../ppwcode/lambda/Link')

const requiredItemWrite = Item.tailor('read').required() // read tailoring, to allow get of old Items

const requiredMode = Mode.schema.required()
const requiredPointInTime = PointInTime.schema.required()
const requiredAccountId = AccountId.schema.required()
const requiredStructureVersion = StructureVersion.schema.required()
const optionalLinks = Link.schema.optional()

const IndexPartitionKeys = Joi.object()
  .keys({
    A: Key.schema.optional(),
    B: Key.schema.optional(),
    C: Key.schema.optional(),
    D: Key.schema.optional(),
    E: Key.schema.optional(),
    N1: Key.schema.optional(),
    N2: Key.schema.optional(),
    N3: Key.schema.optional()
  })
  .required()

const fullDTO = DTO.schema
  .tailor('output')
  .unknown(true)
  .required()
const basicDTO = DTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

/**
 * @typedef Kwargs
 *
 * `sot` and `sub` are either both present or not. If they are, they are used as `createdAt` and `createdBy` of the
 * {@link PersistentObject}, and the respective properties of `dto` are ignored.
 *
 * @type {object}
 * @property {string} mode
 * @property {DTO} dto
 * @property {PointInTime} [sot] - Start of Transaction
 * @property {string} [sub] - account id found in access token
 */

const KwargsExample = {
  mode: Mode.example,
  dto: DTO.exampleDynamodb
}

/** @type {ObjectSchema} */
const Kwargs = Joi.object()
  .keys({
    mode: requiredMode,
    dto: basicDTO,
    /* MUDO This is a design mistake. The intention was that the constructor can be used for both input DTOs as objects
            from DynamoDB, as dto. createdAt, createdBy, and mode, are never part of an input body. They are added from
            headers. They are required in objects from DynamoDB though. The idea was to pass in these 3 arguments
            separately when PersistentObjects are created from input (in `put` functions), and deal with the variance
            here once and for all. However, in subclasses, there are more of such properties, for path parameters.
            These also are not part of the input DTO (body), but they are there in objects from DynamoDB.
            Instead of coding this variance in each subclass, it is easier to just require a DynamoDB object structure
            as argument for these classes, and have the `put` functions prepare the argument.
            The original intention was not well understood, and has not been applied. Having the `put` functions tweak
            the argument object is what was implemented most of the time.
            Therefor, this should be refactor to only take a DynamodDB object as argument always, and simplified by
            removing the other arguments.
    */
    sot: PointInTime.schema,
    sub: AccountId.schema
  })
  .with('sot', 'sub')
  .or('dto.createdAt', 'sot')
  .or('dto.createdBy', 'sub')
  .required()
  .label('Kwargs')
  .example(KwargsExample)

const GetKeyKwargs = Joi.object()
  .keys({
    id: requiredAccountId,
    mode: requiredMode
  })
  .required()

/**
 * Object that can be persisted.
 *
 * By keeping these instances immutable, development becomes easier (functional code).
 * If possible, only introduce derived properties, i.e., getters without arguments. That way, everything can be
 * expressed in invariants, and no separate contracts or tests are necessary, apart from the constructor tests
 * (that check invariants).
 *
 * @param {Kwargs} kwargs
 * @constructor
 */
const PersistentObject = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: [
    function (kwargs) {
      return this.mode === kwargs.mode
    },
    function (kwargs) {
      return this.createdAt === kwargs.sot || kwargs.dto.createdAt
    },
    function (kwargs) {
      return this.createdBy === kwargs.sub || kwargs.dto.createdBy
    },
    function (kwargs) {
      return this.structureVersion === kwargs.dto.structureVersion
    }
  ]
})

PersistentObject.Kwargs = Kwargs

PersistentObject.GetKeyKwargs = GetKeyKwargs

PersistentObject.KwargsExample = KwargsExample

PersistentObject.Key = Joi.string()
  .pattern(/^\/.*/) // should start with a slash
  .uri({ relativeOnly: true })
  .required()

PersistentObject.methods = {}

PersistentObject.invariants = [
  function () {
    return validateSchema(requiredMode, this.mode)
  },
  function () {
    return validateSchema(requiredPointInTime, this.createdAt)
  },
  function () {
    return validateSchema(requiredAccountId, this.createdBy)
  },
  function () {
    return validateSchema(requiredStructureVersion, this.structureVersion)
  },
  function () {
    return validateSchema(fullDTO, this.toJSON())
  },
  function () {
    return this.toJSON().createdAt === this.createdAt
  },
  function () {
    return this.toJSON().createdBy === this.createdBy
  },
  function () {
    return this.toJSON().structureVersion === this.structureVersion
  },
  function () {
    // stringifiable
    try {
      JSON.stringify(this.toJSON())
      return true
    } catch (err) /* istanbul ignore next */ {
      console.error('not stringifiable', err)
      return false
    }
  },
  function () {
    const json = this.toJSON()
    const stringified = JSON.stringify(json)
    const parsed = JSON.parse(stringified)
    const instance = new this.constructor({ mode: this.mode, dto: parsed })
    return _.isEqual(instance, this)
  },
  function () {
    return validateSchema(PersistentObject.Key, this.getKey())
  },
  function () {
    return this.key === this.getKey()
  },
  function () {
    return validateSchema(IndexPartitionKeys, this.getIndexPartitionKeys())
  },
  function () {
    return validateSchema(requiredItemWrite, this.toItem())
  },
  function () {
    return this.toItem().mode === this.mode
  },
  function () {
    return this.toItem().key === this.key
  },
  function () {
    return this.toItem().submitted === this.createdAt
  },
  function () {
    return this.toItem().submittedBy === this.createdBy
  },
  function () {
    return _.isEqual(this.toItem().data, this.toJSON())
  },
  function () {
    return (
      !this.getIndexPartitionKeys().A ||
      (this.toItem().partition_key_A === this.getIndexPartitionKeys().A && this.toItem().sort_key_A === this.createdAt)
    )
  },
  function () {
    return (
      !this.getIndexPartitionKeys().B ||
      (this.toItem().partition_key_B === this.getIndexPartitionKeys().B && this.toItem().sort_key_B === this.createdAt)
    )
  },
  function () {
    return (
      !this.getIndexPartitionKeys().C ||
      (this.toItem().partition_key_C === this.getIndexPartitionKeys().C && this.toItem().sort_key_C === this.createdAt)
    )
  },
  function () {
    return (
      !this.getIndexPartitionKeys().D ||
      (this.toItem().partition_key_D === this.getIndexPartitionKeys().D && this.toItem().sort_key_D === this.createdAt)
    )
  },
  function () {
    return (
      !this.getIndexPartitionKeys().E ||
      (this.toItem().partition_key_E === this.getIndexPartitionKeys().E && this.toItem().sort_key_E === this.createdAt)
    )
  },
  function () {
    return validateSchema(optionalLinks, this.getLinks())
  },
  function () {
    const links = this.getLinks()
    if (links !== undefined) {
      return Object.values(links).every(link => !link.endsWith('/'))
    } else {
      return true
    }
  }
]

function PersistentObjectImpl (kwargs) {
  this.mode = kwargs.mode
  this.createdAt = kwargs.sot || kwargs.dto.createdAt
  this.createdBy = kwargs.sub || kwargs.dto.createdBy
  this.structureVersion = kwargs.dto.structureVersion
}

PersistentObjectImpl.prototype.constructor = PersistentObjectImpl

/**
 * Type of data. The string 'production' is used for production data. Automated tests use `automated-test-<uuid/v4>`,
 * where `<uuid/v4>` is a UUID/v4 that uniquely identifies a test. Quality assurance uses `qa-<version>`. Acceptance
 * tests use `acceptance-<version>`. Demo items are identified by `demo`.
 *
 * @type {string}
 */
PersistentObjectImpl.prototype.mode = undefined

/**
 * SoT of the resource action that `put` this item version, as ISO-8601 string.
 *
 * @type {string}
 */
PersistentObjectImpl.prototype.createdAt = undefined

/**
 * Opaque id of the account that executed the resource action that `put` this item version
 *
 * @type {string}
 */
PersistentObjectImpl.prototype.createdBy = undefined

/**
 * Version of the structure written out by `toJSON` (the constructor might be able to understand multiple, earlier or
 * later versions).
 *
 * @type {number}
 */
PersistentObjectImpl.prototype.structureVersion = 1

/**
 * @return {DTO}
 */
PersistentObjectImpl.prototype.toJSON = function () {
  return {
    createdAt: this.createdAt,
    createdBy: this.createdBy,
    structureVersion: this.structureVersion
  }
}

/**
 * Should be implemented to return the base key of the instance. Will be prepended with `mode`, when used to store data.
 *
 * @return {string}
 */
PersistentObjectImpl.prototype.getKey = new Contract({}).abstract
PersistentObjectImpl.prototype.getLinks = new Contract({}).abstract

PersistentObjectImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: undefined,
    B: undefined,
    C: undefined,
    D: undefined,
    E: undefined,
    N1: undefined,
    N2: undefined,
    N3: undefined
  }
}

/**
 * Returns the {@link Item} to store for this object in DynamoDB.
 *
 * @type {Item}
 */
PersistentObjectImpl.prototype.toItem = function () {
  const indexPartitionKeys = this.getIndexPartitionKeys()

  return {
    mode: this.mode,
    key: this.key,
    submitted: this.createdAt,
    submittedBy: this.createdBy,
    data: this.toJSON(),
    /* TODO It would be more logical to only add these indices to the `actual` item, when there is one, and to use the
            sort key for semantic relevant sorting and paging when getting a list. */
    partition_key_A: indexPartitionKeys.A,
    sort_key_A: indexPartitionKeys.A === undefined ? undefined : this.createdAt,
    partition_key_B: indexPartitionKeys.B,
    sort_key_B: indexPartitionKeys.B === undefined ? undefined : this.createdAt,
    partition_key_C: indexPartitionKeys.C,
    sort_key_C: indexPartitionKeys.C === undefined ? undefined : this.createdAt,
    partition_key_D: indexPartitionKeys.D,
    sort_key_D: indexPartitionKeys.D === undefined ? undefined : this.createdAt,
    partition_key_E: indexPartitionKeys.E,
    sort_key_E: indexPartitionKeys.E === undefined ? undefined : this.createdAt,
    partition_key_1: indexPartitionKeys.N1,
    sort_key_1: indexPartitionKeys.N1 === undefined ? undefined : new Date(this.createdAt).getTime(),
    partition_key_2: indexPartitionKeys.N2,
    sort_key_2: indexPartitionKeys.N2 === undefined ? undefined : new Date(this.createdAt).getTime(),
    partition_key_3: indexPartitionKeys.N3,
    sort_key_3: indexPartitionKeys.N3 === undefined ? undefined : new Date(this.createdAt).getTime()
  }
}

PersistentObjectImpl.prototype.invariants = PersistentObject.invariants

Object.defineProperty(PersistentObjectImpl.prototype, 'key', {
  get: function key () {
    return this.getKey()
  }
})

module.exports = PersistentObject.implementation(PersistentObjectImpl)
