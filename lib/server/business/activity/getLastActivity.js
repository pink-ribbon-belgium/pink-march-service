/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const { Promise: PromiseContract } = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const { schema: Mode } = require('../../../ppwcode/Mode')
const ActivityDTO = require('../../../api/activity/ActivityDTO')
const { schema: AccountId } = require('../../../api/account/AccountId')
const Boom = require('@hapi/boom')

const requiredMode = Mode.required().label('mode')
const requiredAccountId = AccountId.required().label('accountId')
const requiredActivity = ActivityDTO.schema.required().label('activity')
const dynamodb = require('../../aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../aws/dynamodb/dynamodbTableName')

const index = {
  indexName: 'Index_B',
  partitionKey: 'partition_key_B',
  sortKey: 'sort_key_B'
}

const getLastActivity = new PromiseContract({
  pre: [mode => validateSchema(requiredMode, mode), (mode, accountId) => validateSchema(requiredAccountId, accountId)],
  post: [(mode, accountId, result) => validateSchema(requiredActivity, result)],
  exception: [
    function () {
      return PromiseContract.outcome(arguments).isBoom
    },
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ]
}).implementation(async function getLastActivity (mode, accountId) {
  const sortKeyValue = new Date().toISOString()
  const key = `/${mode}/activity/account/${accountId}`

  const dynamodbInstance = await dynamodb()

  // MUDO: Refactor => Handle multiple results (see getByIndex)

  const result = await dynamodbInstance
    .query({
      TableName: dynamodbTableName(mode),
      IndexName: index.indexName,
      ExpressionAttributeNames: {
        '#partitionKey': index.partitionKey,
        '#sortKey': index.sortKey
      },
      ExpressionAttributeValues: {
        ':partitionKey': key,
        ':sortKey': sortKeyValue
      },
      KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey <= :sortKey',
      ScanIndexForward: false, // sort items descending
      Limit: 5
    })
    .promise()

  const activities = result.Items.map(item => item.data)

  if (activities.length <= 0) {
    throw Boom.notFound()
  }

  return activities[0]
})

module.exports = getLastActivity
