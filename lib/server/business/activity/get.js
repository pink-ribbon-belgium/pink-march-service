/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ActivityDTO = require('../../../api/activity/ActivityDTO')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const ActivityPathParameters = require('../../../api/activity/ActivityPathParameters')
const Activity = require('./Activity')
const getActual = require('../../aws/dynamodb/getActual')

const requiredActivitySchema = ActivityDTO.schema.tailor('output').required()
const requiredPathParams = ActivityPathParameters.schema.required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.accountId === pathParameters.accountId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.activityDate === pathParameters.activityDate
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredActivitySchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  /* prettier-ignore */
  const key = `/${mode}/activity/account/${pathParameters.accountId}/date/${pathParameters.activityDate.slice(0, 10)}`
  const result = await getActual(mode, key, true)

  const activity = new Activity({
    mode,
    dto: {
      ...result.data
    }
  })
  const activityJson = activity.toJSON()
  activityJson.links = activity.getLinks()
  return activityJson
})
module.exports = get
