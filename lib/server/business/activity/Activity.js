/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const PersistentObject = require('../PersistentObject')
const PointInTime = require('../../../ppwcode/PointInTime')
const ActivityDTO = require('../../../api/activity/ActivityDTO')

const requiredActivityDate = PointInTime.schema.required()
const dynamodbDTO = ActivityDTO.schema.tailor('dynamodb').required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO.unknown(true).required() })

const ActivityContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.activityDate === kwargs.dto.activityDate
    },
    function (kwargs) {
      return this.trackerType === kwargs.dto.trackerType
    },
    function (kwargs) {
      return this.numberOfSteps === kwargs.dto.numberOfSteps
    },
    function (kwargs) {
      return this.distance === kwargs.dto.distance
    },
    function (kwargs) {
      return this.deleted === kwargs.dto.deleted
    }
  ])
})

ActivityContract.Kwargs = Kwargs
ActivityContract.methods = { ...PersistentObject.contract.methods }

ActivityContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().activityDate === this.activityDate
  },
  function () {
    return this.toJSON().trackerType === this.trackerType
  },
  function () {
    return this.toJSON().numberOfSteps === this.numberOfSteps
  },
  function () {
    return this.toJSON().deleted === this.deleted
  },
  function () {
    return this.toJSON().distance === this.distance
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  },
  function () {
    return this.getIndexPartitionKeys().A === `/${this.mode}/activity`
  },
  function () {
    return this.getIndexPartitionKeys().B === `/${this.mode}/activity/account/${this.accountId}`
  },
  function () {
    return Object.keys(this.getLinks()).includes('account')
  },
  function () {
    return Object.keys(this.getLinks()).includes('accountProfile')
  }
])

function ActivityImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.accountId = kwargs.dto.accountId
  this.activityDate = kwargs.dto.activityDate
  this.trackerType = kwargs.dto.trackerType
  this.numberOfSteps = kwargs.dto.numberOfSteps
  this.distance = kwargs.dto.distance
  this.deleted = kwargs.dto.deleted
}

ActivityImpl.prototype = Object.create(PersistentObject.prototype)
ActivityImpl.prototype.constructor = ActivityImpl

ActivityImpl.prototype.structureVersion = 1
ActivityImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    accountId: this.accountId,
    activityDate: this.activityDate,
    trackerType: this.trackerType,
    numberOfSteps: this.numberOfSteps,
    distance: this.distance,
    deleted: this.deleted
  }
}

ActivityImpl.prototype.getKey = function () {
  return Activity.getKey({ id: this.accountId, activityDate: this.activityDate, mode: this.mode })
}

ActivityImpl.prototype.getIndexPartitionKeys = function () {
  return {
    A: !this.deleted ? `/${this.mode}/activity` : undefined,
    B: !this.deleted ? `/${this.mode}/activity/account/${this.accountId}` : undefined
  }
}

ActivityImpl.prototype.getLinks = function () {
  return {
    account: `/I/account/${this.accountId}`,
    accountProfile: `/I/account/${this.accountId}/publicProfile`
  }
}
ActivityImpl.prototype.invariants = ActivityContract.invariants
const Activity = ActivityContract.implementation(ActivityImpl)

Activity.getKey = new Contract({
  pre: [
    kwargs =>
      validateSchema(PersistentObject.contract.GetKeyKwargs.keys({ activityDate: requiredActivityDate }), kwargs)
  ]
}).implementation(function getKey (kwargs) {
  const dateForKey = kwargs.activityDate.slice(0, 10)
  return `/${kwargs.mode}/activity/account/${kwargs.id}/date/${dateForKey}`
})

module.exports = Activity
