/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../../ResourceActionContract')
const validateSchema = require('../../../../_util/validateSchema')
const calculation = require('../../../../api/activity/ActivityCalculations')
const calculationResult = require('../../../../api/activity/calculation/calculationResult')

const calculateDistance = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: [
    function (sot, sub, mode, flowId, pathParameters, body, result) {
      return validateSchema(calculationResult.schema, result)
    }
  ]
}).implementation(async function calculate (sot, sub, mode, flowId, pathParameters, body) {
  const steps = body.steps ? body.steps : 0
  const distance = body.distance ? body.distance : 0

  const result = calculation.calculateDistanceFromSteps(body.gender, steps, distance)
  const roundedResult = Math.round(result)
  return {
    steps: steps || roundedResult,
    distance: distance || roundedResult
  }
})

module.exports = calculateDistance
