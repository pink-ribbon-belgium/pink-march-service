/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Boom = require('@hapi/boom')
const config = require('config')
const putTransaction = require('../../aws/dynamodb/putTransaction')
const Activity = require('./Activity')
const ActivityDTO = require('../../../api/activity/ActivityDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateInputSchema = require('../../../_util/validateInputSchema')
const ActivityPathParams = require('../../../api/activity/ActivityPathParameters')

const requiredPathParameters = ActivityPathParams.schema.required()
const requiredInputSchema = ActivityDTO.schema.tailor('input').required()

const put = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 400 ||
        PromiseContract.outcome(arguments).output.statusCode === 403
      )
    }
  ])
}).implementation(async function put (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredPathParameters, pathParameters)
  validateInputSchema(requiredInputSchema, body)

  // Ensure that the activitydate only contains the date-part of the date
  const activityDatePart = body.activityDate.slice(0, 10)

  let activityPeriodStartDate
  let activityPeriodEndDate
  /* istanbul ignore next : we can't test different modes */
  switch (mode) {
    case 'dev-experiment':
    case 'demo':
    case 'june2020':
    case 'production':
      activityPeriodStartDate = new Date(config.activityPeriod[mode].startdate)
      activityPeriodEndDate = new Date(config.activityPeriod[mode].enddate)
      break
    default:
      activityPeriodStartDate = new Date(config.activityPeriod.default.startdate)
      activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
      break
  }

  const activityDate = new Date(activityDatePart)

  if (activityDate < activityPeriodStartDate || activityDate > activityPeriodEndDate) {
    throw Boom.forbidden('activity date not within period', {
      mode,
      accountId: body.accountId,
      trackerType: body.trackerType,
      activityDate,
      startDate: activityPeriodStartDate.toISOString().slice(0, 10),
      endDate: activityPeriodEndDate.toISOString().slice(0, 10)
    })
  }

  const activity = new Activity({
    mode,
    dto: {
      ...body,
      activityDate: activityDatePart,
      createdAt: sot,
      createdBy: sub
    },
    sot,
    sub
  })

  const item = {
    ...activity.toItem(),
    flowId
  }

  const submittedItem = {
    ...item,
    partition_key_A: undefined,
    sort_key_A: undefined,
    partition_key_B: undefined,
    sort_key_B: undefined,
    partition_key_C: undefined,
    sort_key_C: undefined
  }

  const actualItem = {
    ...item,
    submitted: 'actual',
    sort_key_A: activityDatePart,
    sort_key_B: activityDatePart
  }

  return putTransaction([submittedItem, actualItem])
})

module.exports = put
