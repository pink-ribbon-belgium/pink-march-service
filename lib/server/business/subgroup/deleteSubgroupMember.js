/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const UUID = require('../../../ppwcode/UUID')
const AccountId = require('../../../api/account/AccountId')
const validateInputSchema = require('../../../_util/validateInputSchema')
const simpleLog = require('../../../_util/simpleLog')
const writeSlot = require('../slot/write')
const getByIndex = require('../../aws/dynamodb/getByIndex')
const boom = require('@hapi/boom')
const config = require('config')

const requiredPathParametersSchema = Joi.object()
  .keys({
    subgroupId: UUID.schema.required(),
    companyId: UUID.schema.required(),
    accountId: AccountId.schema.required()
  })
  .required()

async function updateSlot (sot, sub, mode, flowId, slotToUpdate) {
  simpleLog.info(module, 'updateSlot', flowId, 'SLOT_PUT', { sot, sub, mode, slotId: slotToUpdate.id })
  await writeSlot(sot, sub, mode, flowId, { ...slotToUpdate }) // only programming errors can occur
  simpleLog.info(module, 'updateSlot', flowId, 'SLOT_PUT_SUCCESS', {
    sot,
    sub,
    mode,
    slotId: slotToUpdate.id
  })
}

const deleteSubgroupMember = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 403 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function deleteSubgroupMember (sot, sub, mode, flowId, pathParameters) {
  simpleLog.info(module, 'deleteSubgroupMember', flowId, 'DELETE_SUBGROUP_MEMBER_START', {
    sot,
    sub,
    mode,
    pathParameters
  })
  validateInputSchema(requiredPathParametersSchema, pathParameters)

  let activityPeriodEndDate
  /* istanbul ignore next : we can't test different modes */
  switch (mode) {
    case 'dev-experiment':
    case 'demo':
    case 'june2020':
    case 'production':
      activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
      break
    default:
      activityPeriodEndDate = new Date(config.activityPeriod.default.enddate)
      break
  }
  const memberDeleteEndDate = new Date(activityPeriodEndDate.setDate(activityPeriodEndDate.getDate() - 7))
  const now = new Date()

  if (now >= memberDeleteEndDate) {
    throw boom.forbidden('member delete period has passed', {
      mode,
      accountId: pathParameters.accountId,
      memberDeleteEndDate
    })
  }

  const slotIndex = {
    indexName: 'Index_C',
    partitionKey: 'partition_key_C',
    sortKey: 'sort_key_C'
  }
  // Get slot to update
  const slotPartitionKey = `/${mode}/account/${pathParameters.accountId}/slot`
  const slot = await getByIndex(mode, slotPartitionKey, slotIndex, 'actual')

  // Check if account belongs to company
  if (slot[0].data.groupId !== pathParameters.companyId) {
    throw boom.badRequest()
  }

  // Check if member is still in member of the subgroup
  if (slot[0].data.subgroupId !== pathParameters.subgroupId) {
    throw boom.badRequest()
  }
  delete slot[0].data.subgroupId

  // Update existing slot by removing subgroupId
  await updateSlot(sot, sub, mode, flowId, slot[0].data)

  simpleLog.info(module, 'deleteSubgroupMember', flowId, 'DELETE_SUBGROUP_MEMBER_SUCCESS', {
    sot,
    sub,
    mode,
    pathParameters
  })
})

module.exports = deleteSubgroupMember
