/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../aws/dynamodb/getVersionAtOrBefore')
const SubGroup = require('./SubGroup')
const SubGroupDTO = require('../../../api/subgroup/SubGroupDTO')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const SubGroupPathParams = require('../../../api/subgroup/SubGroupPathParameters')

const requiredPathParams = SubGroupPathParams.schema.required()
const requiredSubGroupSchema = SubGroupDTO.schema.tailor('output').required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.id === pathParameters.subgroupId
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredSubGroupSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  const key = `/${mode}/subgroup/${pathParameters.subgroupId}`
  const dtoPromise = getVersionAtOrBefore(mode, key, sot)
  const result = await dtoPromise

  result.data.groupType = 'Company'
  const subgroup = new SubGroup({
    mode,
    dto: {
      ...result.data
    }
  })

  const subGroupJson = subgroup.toJSON()
  subGroupJson.links = subgroup.getLinks()
  subGroupJson.joinLink = subgroup.getJoinLink()
  return subGroupJson
})

module.exports = get
