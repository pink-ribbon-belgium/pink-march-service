/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const PromiseContract = require('@toryt/contracts-iv').Promise
const SubGroupPathParams = require('../../../api/subgroup/SubGroupPathParameters')
const AccountIdSchema = require('../../../api/account/AccountId').schema

const requiredPathParametersSchema = SubGroupPathParams.schema.required()
const outputschema = Joi.array().items(Joi.object().keys({ accountId: AccountIdSchema.required(), name: Joi.string() }))

const ResourceActionContract = require('../../ResourceActionContract')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const getByIndex = require('../../aws/dynamodb/getByIndex')
const getActual = require('../../aws/dynamodb/getActual')
const boom = require('@hapi/boom')

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(outputschema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function getGroupSlots (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParametersSchema, pathParameters)
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const memberIndexPartitionKey = `/${mode}/group/${pathParameters.companyId}/members`
  const groupMemberslots = await getByIndex(mode, memberIndexPartitionKey, index, 'actual')

  const memberslots = groupMemberslots.filter(s => s.data.subgroupId === pathParameters.subgroupId)

  if (memberslots.length === 0) {
    throw boom.notFound()
  }

  const membersInfo = await Promise.all(
    memberslots.map(slot => getActual(mode, `/${mode}/account/${slot.data.accountId}/publicProfile`))
  )

  return membersInfo.map(i => ({ accountId: i.data.accountId, name: `${i.data.lastName} ${i.data.firstName}` }))
})

module.exports = get
