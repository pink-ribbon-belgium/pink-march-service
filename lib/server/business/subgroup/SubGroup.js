/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const Group = require('../../../server/business/group/Group')
const PersistentObject = require('../PersistentObject')
const SubGroupDTO = require('../../../api/subgroup/SubGroupDTO')
const Mode = require('../../../ppwcode/Mode')
const UUID = require('../../../ppwcode/UUID')
const AccountId = require('../../../api/account/AccountId')
const config = require('config')
const Joi = require('@hapi/joi')

const requiredUUID = UUID.schema.required()
const requiredAccountId = AccountId.schema.required()
const requiredMode = Mode.schema.required().label('mode')

const dynamoDBDTO = SubGroupDTO.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const KwargsExample = {
  mode: Mode.example,
  dto: SubGroupDTO.exampleDynamodb
}

const Kwargs = Group.contract.Kwargs.keys({ dto: dynamoDBDTO.unknown(true).required() }).example(KwargsExample)

const GetLinkKwargs = Joi.object()
  .keys({
    mode: requiredMode,
    groupId: requiredUUID
  })
  .required()

const SubGroupContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: Group.contract.post.concat([
    function (kwargs) {
      return this.id === kwargs.dto.id
    },
    function (kwargs) {
      return this.groupId === kwargs.dto.groupId
    },
    function (kwargs) {
      return this.name === kwargs.dto.name
    },
    function (kwargs) {
      return this.logo === kwargs.dto.logo
    },
    function (kwargs) {
      return this.accountId === kwargs.dto.accountId
    },
    function (kwargs) {
      return this.joinLink === kwargs.dto.joinLink
    }
  ])
})

SubGroupContract.Kwargs = Kwargs
SubGroupContract.KwargsExample = KwargsExample
SubGroupContract.methods = { ...Group.contract.methods }

SubGroupContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredUUID, this.id)
  },
  function () {
    return validateSchema(requiredUUID, this.groupId)
  },
  function () {
    return validateSchema(requiredAccountId, this.accountId)
  },
  function () {
    return this.toJSON().id === this.id
  },
  function () {
    return this.toJSON().groupId === this.groupId
  },
  function () {
    return this.toJSON().code === this.code
  },
  function () {
    return this.toJSON().name === this.name
  },
  function () {
    return this.toJSON().logo === this.logo
  },
  function () {
    return this.toJSON().accountId === this.accountId
  },
  function () {
    return this.toJSON().joinLink === this.joinLink
  },
  function () {
    return !!this.groupType
  },
  function () {
    return typeof this.groupType === 'string'
  },
  function () {
    return this.toJSON().groupType === this.groupType
  },
  function () {
    return validateSchema(dynamoDBDTO, this.toJSON())
  },
  function () {
    const links = this.getLinks()
    const linkPrefix = `/I/${this.groupType.toLowerCase()}/${this.id}`
    return Object.values(links).every(link => link.startsWith(linkPrefix))
  },
  function () {
    return this.getLinks().slots === `/I/${this.groupType.toLowerCase()}/${this.id}/slots`
  }
])

function SubGroupImpl (kwargs) {
  Group.call(this, kwargs)
  this.groupId = kwargs.dto.groupId
  this.name = kwargs.dto.name
  this.logo = kwargs.dto.logo
  this.accountId = kwargs.dto.accountId
}

SubGroupImpl.prototype = Object.create(Group.prototype)
SubGroupImpl.prototype.constructor = SubGroupImpl

SubGroupImpl.prototype.id = undefined
SubGroupImpl.prototype.structureVersion = 1
SubGroupImpl.prototype.name = 'test-subgroup'
SubGroupImpl.prototype.logo = 'VE9PTUFOWVNFQ1JFVFM'
SubGroupImpl.prototype.accountId = 'test-account-id'

SubGroupImpl.prototype.getKey = function () {
  return SubGroup.getKey({ id: this.id, mode: this.mode })
}

SubGroupImpl.prototype.getLinks = function () {
  return {
    slots: `/I/${this.groupType.toLowerCase()}/${this.id}/slots`
  }
}

SubGroupImpl.prototype.getJoinLink = function () {
  return SubGroup.getJoinLink({ mode: this.mode, groupId: this.id })
}

SubGroupImpl.prototype.toJSON = function () {
  return {
    ...Group.prototype.toJSON.call(this),
    groupId: this.groupId,
    name: this.name,
    logo: this.logo,
    accountId: this.accountId
  }
}

SubGroupImpl.prototype.invariants = SubGroupContract.invariants

const SubGroup = SubGroupContract.implementation(SubGroupImpl)

SubGroup.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs, kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/subgroup/${kwargs.id}`
})

SubGroup.getJoinLink = new Contract({
  pre: [kwargs => validateSchema(GetLinkKwargs, kwargs)]
}).implementation(function getJoinLink (kwargs) {
  let baseUrl
  if (kwargs.mode === 'demo') {
    baseUrl = config.baseurl.demo.url
  } else if (kwargs.mode === 'june2020') {
    baseUrl = config.baseurl.june2020.url
  } else {
    baseUrl = config.baseurl.url
  }
  return `${baseUrl}index.html#/join/subgroup/${kwargs.groupId}`
})

module.exports = SubGroup
