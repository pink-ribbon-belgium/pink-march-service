/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const SubgroupDTO = require('../../../api/subgroup/SubGroupDTO')
const getSlot = require('../account/slot/get')
const getGroupProfile = require('../group/profile/get')
const writeSlot = require('../slot/write')
const writeTeamAggregate = require('../aggregates/team/write')
const writeSubgroup = require('./write')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')
const UUID = require('../../../ppwcode/UUID')
const uuidv4 = require('uuid').v4
const Joi = require('@hapi/joi')

const requiredSubgroupSchema = SubgroupDTO.schema.required()

const post = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function () {
      return validateSchema(
        Joi.object().keys({ subgroupId: UUID.schema.required() }),
        PromiseContract.outcome(arguments)
      )
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function post (sot, sub, mode, flowId, pathParameters, body) {
  validateInputSchema(requiredSubgroupSchema, body)
  const subgroupId = uuidv4()

  const slot = await getSlot(sot, sub, mode, flowId, { accountId: body.accountId })
  delete slot.links

  const groupProfile = await getGroupProfile(sot, sub, mode, flowId, { teamId: body.groupId })

  const teamAggregateData = {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 1,
    teamAggregateId: subgroupId,
    totalSteps: 0,
    totalDistance: 0,
    totalTeams: 9999999,
    averageSteps: 1,
    averageDistance: 1,
    companyId: body.groupId,
    name: body.name,
    rank: 9999999,
    groupType: body.groupType,
    extendedName: `${body.name} (${groupProfile.name})`,
    hasLogo: !!body.logo
  }

  await Promise.all([
    writeSubgroup(sot, sub, mode, flowId, { ...body, id: subgroupId }),
    writeSlot(sot, sub, mode, flowId, { ...slot, subgroupId }),
    writeTeamAggregate(sot, sub, mode, flowId, teamAggregateData)
  ])
  return {
    subgroupId: subgroupId
  }
})

module.exports = post
