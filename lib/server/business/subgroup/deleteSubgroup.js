/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const ResourceActionContract = require('../../ResourceActionContract')
const validateInputSchema = require('../../../_util/validateInputSchema')
const requiredPathParametersSchema = require('../../../api/subgroup/SubGroupPathParameters').schema
const getMembers = require('./getMembers')
const getActual = require('../../aws/dynamodb/getActual')
const deleteTransaction = require('../../aws/dynamodb/deleteTransaction')
const boom = require('@hapi/boom')

const deleteSubgroup = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.slice(),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function deleteSubgroup (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParametersSchema, pathParameters)

  // Check if subgroup still has members
  let members
  try {
    members = await getMembers(sot, sub, mode, flowId, pathParameters)
  } catch (err) {
    /* istanbul ignore else */
    if (err.output.statusCode === 404) {
      members = []
    }
  }

  if (members.length > 0) {
    throw boom.badRequest()
  }

  // Check if subgroup exists
  const subgroupKey = `/${mode}/subgroup/${pathParameters.subgroupId}`
  await getActual(mode, subgroupKey, true)

  // Check if teamAggregate exists
  const teamAggregateKey = `/${mode}/teamAggregate/${pathParameters.subgroupId}`
  await getActual(mode, teamAggregateKey, true)

  // Delete subgroup and teamAggregate in 1 transaction
  await deleteTransaction(mode, [subgroupKey, teamAggregateKey])
})

module.exports = deleteSubgroup
