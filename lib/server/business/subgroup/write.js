/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const putTransactionDynamodb = require('../../aws/dynamodb/putTransaction')
const Subgroup = require('./SubGroup')
const SubgroupDTO = require('../../../api/subgroup/SubGroupDTO')
const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const AccountId = require('../../../api/account/AccountId')
const Mode = require('../../../ppwcode/Mode')
const UUID = require('../../../ppwcode/UUID')
const PointInTime = require('../../../ppwcode/PointInTime')

const requiredSot = PointInTime.schema.required().label('sot')
const requiredSub = AccountId.schema.required().label('sub')
const requiredMode = Mode.schema.required().label('mode')
const requiredFlowId = UUID.schema.required().label('flowId')

const requiredSubgroupSchema = SubgroupDTO.schema
  .tailor('input')
  .required()
  .label('subgroupArgs')

const write = new PromiseContract({
  pre: [
    sot => validateSchema(requiredSot, sot),
    (sot, sub) => validateSchema(requiredSub, sub),
    (sot, sub, mode) => validateSchema(requiredMode, mode),
    (sot, sub, mode, flowId) => validateSchema(requiredFlowId, flowId),
    (sot, sub, mode, flowId, slotArgs) => validateSchema(requiredSubgroupSchema, slotArgs)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
}).implementation(async function put (sot, sub, mode, flowId, subgroupArgs) {
  const subgroup = new Subgroup({
    mode,
    dto: {
      ...subgroupArgs,
      createdAt: sot,
      createdBy: sub
    },
    sot,
    sub
  })

  const originalItem = {
    mode: subgroup.mode,
    key: subgroup.key,
    submitted: subgroup.createdAt,
    submittedBy: subgroup.createdBy,
    flowId,
    data: subgroup.toJSON()
  }

  return putTransactionDynamodb([
    originalItem,
    {
      ...originalItem,
      submitted: 'actual',
      partition_key_A: `/${mode}/subgroup`,
      sort_key_A: subgroup.name,
      partition_key_B: `/${mode}/subgroup/${subgroup.groupType.toLowerCase()}/${subgroup.groupId}`,
      sort_key_B: subgroup.name,
      partition_key_C: `/${mode}/subgroup/${subgroup.groupType.toLowerCase()}`,
      sort_key_C: subgroup.name
    }
  ])
})

module.exports = write
