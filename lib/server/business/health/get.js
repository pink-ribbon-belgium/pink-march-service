/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../aws/dynamodb/getVersionAtOrBefore')
const ServiceHealthStatus = require('../../../api/health/ServiceHealthStatus')
const Boom = require('@hapi/boom')
const validateSchema = require('../../../_util/validateSchema')
const ServiceHealthDTO = require('../../../api/health/ServiceHealthDTO')

/**
 * Return a key that doesn't exist, that follows our requirements.
 *
 * @param {Mode} mode
 */
function keyThatDoesntExist (mode) {
  return `/${mode}/health/does_not_exist`
}

/**
 * Beginning of the Era of Nabonassar.
 *
 * https://www.quora.com/What-is-the-first-recorded-date-in-human-history
 *
 * Well, not really surely the first, but funny and very early. Not relevant, since the key will be unique
 */
const firstRecordedHistoricEvent = new Date('-000747-02-26T12:00+0300').toISOString()

/**
 * Try to get an item from DynamoDB that doesn't exist. Expect a clean response for it not being found. That 'proves'
 * that our connection with DynamoDB is healthy.
 *
 * @param {Mode} mode
 */
async function dynamodbHealth (mode) {
  try {
    await getVersionAtOrBefore(mode, keyThatDoesntExist(mode), firstRecordedHistoricEvent)
    // dynamodb answered with an item, but was not expected
    return ServiceHealthStatus.WARNING
  } catch (err) {
    if (err.isBoom && err.output.statusCode === 404) {
      // expected healthy response
      return ServiceHealthStatus.OK
    }
    return ServiceHealthStatus.ERROR
    // IDEA diversify between ERROR and UNREACHABLE
  }
}

/**
 * Return the health of the service.
 *
 * Tries to get a non-existing item from dynamodb, and expects a zero result.
 *
 * @param {PointInTime} sot
 * @param {AccountId} sub
 * @param {Mode} mode
 * @param {FlowId} flowId
 */
const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function () {
      return validateSchema(ServiceHealthDTO.schema, PromiseContract.outcome(arguments))
    },
    function () {
      return [ServiceHealthStatus.OK, ServiceHealthStatus.WARNING].includes(PromiseContract.outcome(arguments).status)
    },
    function () {
      return [ServiceHealthStatus.OK, ServiceHealthStatus.WARNING].includes(PromiseContract.outcome(arguments).dynamodb)
    },
    function (sot) {
      return PromiseContract.outcome(arguments).createdAt === sot
    },
    function (sot, sub) {
      return PromiseContract.outcome(arguments).createdBy === sub
    },
    function () {
      return PromiseContract.outcome(arguments).structureVersion === 1
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 470
    },
    function () {
      return validateSchema(ServiceHealthDTO.schema, PromiseContract.outcome(arguments).output.health)
    },
    function () {
      return PromiseContract.outcome(arguments).output.payload.status === ServiceHealthStatus.ERROR
    },
    function () {
      return PromiseContract.outcome(arguments).output.payload.dynamodb === ServiceHealthStatus.ERROR
    },
    function (sot) {
      return PromiseContract.outcome(arguments).output.payload.createdAt === sot
    },
    function (sot, sub) {
      return PromiseContract.outcome(arguments).output.payload.createdBy === sub
    },
    function () {
      return PromiseContract.outcome(arguments).output.payload.structureVersion === 1
    }
  ])
}).implementation(async function get (sot, sub, mode) {
  const dynamodb = await dynamodbHealth(mode)
  const result = {
    createdAt: sot,
    createdBy: sub,
    structureVersion: 1,
    dynamodb,
    status: dynamodb // no need to ServiceHealthStatus.consolidate, because just one underlying service at this time
  }
  if (result.status === ServiceHealthStatus.OK || result.status === ServiceHealthStatus.WARNING) {
    return result
  }
  const error = new Boom.Boom('Service is not healthy', { statusCode: 470 })
  error.output.payload = result
  delete error.output.payload.error
  throw error
})

get.keyThatDoesntExist = keyThatDoesntExist
get.submittedThatDoesntExist = firstRecordedHistoricEvent

module.exports = get
