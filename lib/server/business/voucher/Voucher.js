/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const validateSchema = require('../../../_util/validateSchema')
const PersistentObject = require('../../../server/business/PersistentObject')
const VoucherDto = require('../../../api/voucher/VoucherDTO')
const requiredVoucherCode = require('../../../api/voucher/VoucherCode').schema.required()
const requiredPointInTime = require('../../../ppwcode/PointInTime').schema.required()

const dynamodbDTO = VoucherDto.schema
  .tailor('dynamodb')
  .unknown(true)
  .required()

const Kwargs = PersistentObject.contract.Kwargs.keys({ dto: dynamodbDTO })

const VoucherContract = new Contract({
  pre: [kwargs => validateSchema(Kwargs, kwargs)],
  post: PersistentObject.contract.post.concat([
    function (kwargs) {
      return this.voucherCode === kwargs.dto.voucherCode
    },
    function (kwargs) {
      return this.reusable === kwargs.dto.reusable
    },
    function (kwargs) {
      return this.amount === kwargs.dto.amount
    },
    function (kwargs) {
      return this.percentage === kwargs.dto.percentage
    },
    function (kwargs) {
      return this.expiryDate === kwargs.dto.expiryDate
    }
  ])
})

VoucherContract.Kwargs = Kwargs

VoucherContract.methods = { ...PersistentObject.contract.methods }

VoucherContract.invariants = PersistentObject.contract.invariants.concat([
  function () {
    return validateSchema(requiredVoucherCode, this.voucherCode)
  },
  function () {
    return validateSchema(requiredPointInTime, this.expiryDate)
  },
  function () {
    return this.toJSON().voucherCode === this.voucherCode
  },
  function () {
    return this.toJSON().reusable === this.reusable
  },
  function () {
    return this.toJSON().amount === this.amount
  },
  function () {
    return this.toJSON().percentage === this.percentage
  },
  function () {
    return this.toJSON().expiryDate === this.expiryDate
  },
  function () {
    return validateSchema(dynamodbDTO, this.toJSON())
  }
])

function VoucherImpl (kwargs) {
  PersistentObject.call(this, kwargs)
  this.voucherCode = kwargs.dto.voucherCode
  this.reusable = kwargs.dto.reusable
  this.amount = kwargs.dto.amount
  this.percentage = kwargs.dto.percentage
  this.expiryDate = kwargs.dto.expiryDate
}

VoucherImpl.prototype = Object.create(PersistentObject.prototype)
VoucherImpl.prototype.constructor = VoucherImpl

VoucherImpl.prototype.toJSON = function () {
  return {
    ...PersistentObject.prototype.toJSON.call(this),
    voucherCode: this.voucherCode,
    reusable: this.reusable,
    amount: this.amount,
    percentage: this.percentage,
    expiryDate: this.expiryDate
  }
}

VoucherImpl.prototype.getKey = function () {
  return Voucher.getKey({ id: this.voucherCode, mode: this.mode })
}

VoucherImpl.prototype.invariants = VoucherContract.invariants

const Voucher = VoucherContract.implementation(VoucherImpl)

Voucher.getKey = new Contract({
  pre: [kwargs => validateSchema(PersistentObject.contract.GetKeyKwargs.keys({ id: requiredVoucherCode }), kwargs)]
}).implementation(function getKey (kwargs) {
  return `/${kwargs.mode}/voucher/${kwargs.id}`
})

module.exports = Voucher
