/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const ResourceActionContract = require('../../ResourceActionContract')
const PromiseContract = require('@toryt/contracts-iv').Promise
const getVersionAtOrBefore = require('../../aws/dynamodb/getVersionAtOrBefore')
const Voucher = require('./Voucher')
const VoucherDTO = require('../../../api/voucher/VoucherDTO')
const VoucherCode = require('../../../api/voucher/VoucherCode')
const validateSchema = require('../../../_util/validateSchema')
const validateInputSchema = require('../../../_util/validateInputSchema')

const requiredPathParams = Joi.object().keys({ voucherCode: VoucherCode.schema.required() })
const requiredVoucherSchema = VoucherDTO.schema.tailor('output').required()

const get = new PromiseContract({
  pre: ResourceActionContract.pre.slice(),
  post: ResourceActionContract.post.concat([
    function (sot, sub, mode, flowId, pathParameters, response) {
      return response.voucherCode === pathParameters.voucherCode
    },
    function (sot, sub, mode, flowId, pathParameters, response) {
      return validateSchema(requiredVoucherSchema, response)
    }
  ]),
  exception: ResourceActionContract.exception.concat([
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode === 404 ||
        PromiseContract.outcome(arguments).output.statusCode === 400
      )
    }
  ])
}).implementation(async function get (sot, sub, mode, flowId, pathParameters) {
  validateInputSchema(requiredPathParams, pathParameters)
  const key = `/${mode}/voucher/${pathParameters.voucherCode}`
  const dtoPromise = getVersionAtOrBefore(mode, key, sot)
  const result = await dtoPromise

  const voucher = new Voucher({
    mode,
    dto: {
      ...result.data
    }
  })

  const voucherJson = voucher.toJSON()
  return voucherJson
})

module.exports = get
