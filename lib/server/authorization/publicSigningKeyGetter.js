/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const jwksClient = require('jwks-rsa')
const Contract = require('@toryt/contracts-iv')
const config = require('config')
const util = require('util')
// use shim for Promise.any until it becomes part of Node
require('promise.any').shim()

/* No contract for getPublicSigningKey…: it is not currently efficient to use contracts with callbacks. Since this will
   be called by ('jsonwebtoken#verify')[https://github.com/auth0/node-jsonwebtoken#readme], specifying preconditions
   makes no sense either. Methods _are_ tested. */

/* We use 2 clients, one for the production STS, and one for the test STS. getPublicSigningKeySTS combines both
   results. */
const testJWKSSigningKeys = util.promisify(
  jwksClient({
    jwksUri: config.sts.test.wellKnownJWKSUri
  }).getSigningKey
)
const productionJWKSSigningKeys = util.promisify(
  jwksClient({
    jwksUri: config.sts.production.wellKnownJWKSUri
  }).getSigningKey
)

function getPublicSigningKeySTS (header, callback) {
  Promise.any([testJWKSSigningKeys(header.kid), productionJWKSSigningKeys(header.kid)])
    .then(key => {
      callback(null, key.rsaPublicKey || key.publicKey)
    })
    .catch(err => {
      callback(err)
    })
}

/**
 * Returns a function that returns the public signing key for the `kid` of the given `header`.
 *
 * The returned function is a wrapper around ('jwks-rsa#getSigningKey')[https://github.com/auth0/node-jwks-rsa#readme],
 * that makes automated tests possible . Because it is a wrapper, we follow the signature and contract of
 * ('jwks-rsa#getSigningKey')[https://github.com/auth0/node-jwks-rsa#readme]. This is a (possibly) asynchronous
 * function that uses a traditional callback, and not Promises or `async` / `await`. This has to be
 * used that way, because that is how it is used in
 * ('jsonwebtoken#verify')[https://github.com/auth0/node-jsonwebtoken#readme].
 *
 * This is a function that returns the key getter, because that way we can vary the function used in automated tests.
 */
const publicSigningKeyGetter = new Contract({
  post: [result => result === getPublicSigningKeySTS]
}).implementation(function publicSigningKeyGetter () {
  return getPublicSigningKeySTS
})

publicSigningKeyGetter.sts = getPublicSigningKeySTS

module.exports = publicSigningKeyGetter
