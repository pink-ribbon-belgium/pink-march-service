/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const micromatch = require('micromatch')
const validateSchema = require('../../_util/validateSchema')
const requiredEvent = require('../../ppwcode/lambda/apigateway/Event').schema.required()

const resourceActionAllowedContract = new Contract({
  pre: [
    raas => Array.isArray(raas),
    raas => raas.every(raa => !!raa && typeof raa === 'string'),
    (raas, event) => validateSchema(requiredEvent, event)
  ],
  post: [
    (raas, event, result) => typeof result === 'boolean',
    (raas, event, result) => result === micromatch.isMatch(`${event.httpMethod}:${event.path}`, raas)
  ]
})

function resourceActionAllowedImpl (raas, event) {
  const resourceAction = `${event.httpMethod}:${event.path}`
  return micromatch.isMatch(resourceAction, raas)
}

module.exports = resourceActionAllowedContract.implementation(resourceActionAllowedImpl)
