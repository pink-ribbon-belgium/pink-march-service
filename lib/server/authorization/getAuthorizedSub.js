/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const AccountId = require('../../api/account/AccountId')
const validateSchema = require('../../_util/validateSchema')
const JWT = require('jsonwebtoken')
const Boom = require('@hapi/boom')
const { promisify } = require('util')
const config = require('config')
const assert = require('assert')
const requiredEvent = require('../../ppwcode/lambda/apigateway/Event').schema.required()
const customClaims = require('./customClaims')
const path = require('path')
const resourceActionAllowed = require('./resourceActionAllowed')
const { productionModesPattern } = require('../../ppwcode/Mode')

/* Get the publicSigningKeyGetterInstance (so we can splice in another one in automated tests). */
const projectRoot = path.join(__dirname, '..', '..', '..')
const publicSigningKeyGetterModuleAbsolutePath = path.join(
  projectRoot,
  config.authorization.publicSigningKeyGetter.modulePath
)
const publicSigningKeyGetterRelPath = path.relative(__dirname, publicSigningKeyGetterModuleAbsolutePath)
const publicSigningKeyGetter = require(`./${publicSigningKeyGetterRelPath}`)

const verify = promisify(JWT.verify)

const requiredAccountId = AccountId.schema.required()

/* Extracted from https://github.com/auth0/node-jsonwebtoken/blob/master/verify.js

   The following messages signal programming errors, and are not acceptable:

   - 'clockTimestamp must be a number'
   - 'jwt must be a string',
   - 'nonce must be a non-empty string',
   - 'verify must be called asynchronous if secret or public key is provided as a callback',
   - 'error in secret or public key callback: ',
   - 'secret or public key must be provided',
   - '"maxAge" should be a number of seconds or string representing a timespan eg: "1d", "20h", 60',
   - 'jwt subject invalid. expected: ' // we are determining the sub, not checking it
   - 'jwt nonce invalid. expected: ' // we are not using a nonce
   - 'jwt jwtid invalid', // we are not using a jwtid
   // NOTE: returning the nonce in an error message would be a definite leak
 */
const verifyMessages = {
  noToken: 'jwt must be provided',
  tokenMalformed: 'jwt malformed', // untested
  invalidToken: 'invalid token',
  tokenNotSigned: 'jwt signature is required', // untested
  invalidAlgorithm: 'invalid algorithm',
  invalidSignature: 'invalid signature',
  invalidNbf: 'invalid nbf value', // untested
  tooEarly: 'jwt not active',
  invalidExp: 'invalid exp value', // untested
  expired: 'jwt expired',
  /* NOTE: returning what we expect would, in general, be giving possible attackers insights they could not otherwise
           get */
  invalidAud: 'jwt audience invalid', // 'jwt audience invalid. expected: '
  invalidIss: 'jwt issuer invalid', // 'jwt issuer invalid. expected: '
  invalidIat: 'iat required when maxAge is specified', // untested
  tooOld: 'maxAge exceeded'
}

const noSub = `sub that matches ${AccountId.regexp} is required in authorization Bearer token payload`
const modeDoesNotMatch = `claim '${customClaims.modeClaim} in token does not match mode in request`
const raaMalFormed = `claim '${customClaims.raasClaim}' must be an array of glob patterns`
const noMatchingRAA = `no matching Resource Action Authorization in '${customClaims.raasClaim}' for requested Resource Action`

/**
 * Returns the sub in `jwt`, URI encoded.
 *
 * Throws 'Unauthorized' when
 *
 * - there is no `authorization` header
 * - there is no `jwt`,
 * - the `jwt`'s `iss` is not a trusted STS,
 * - the `jwt` is not signed by the `iss`,
 * - the `jwt`'s `iat` and `exp` are not before and after `now`, respectively,
 * - the `jwt`'s `audience` is not this service,
 * - the `jwt`'s `scopes` are not as configured,
 * - the `jwt` does not contain a non-empty `sub`
 * - the `jwr` does not contain a `raa`
 * - the `jwt`'s `raa` does not match the resource-action this request represents
 *
 * NOTE: jsonwebtoken.verify does a lot of work, but calls jws.decode unprotected by a catch. An extremely malformed
 *       token makes decode throw, e.g., when the payload is not JSON-parseable. All the possible errors are
 *       nigh impossible to enumerate. On the other hand, such unexpected errors are client-issues (4xx).
 *       Given that this code is broadely tested for configuration errors of `verify`, and that `verify` is
 *       extremely widely used globally, we assume that any unexpected error when calling `verify` is not a
 *       programming error, but an issue with the token.
 *       Therefor, the contract excepts _any_ message in an exception, and not only the expected ones, if the exception
 *       has the boolean property 'verifyFailed'.
 *
 * @param {Event} event
 * @return {AccountId}
 */
const getSubFromJWTContract = new PromiseContract({
  pre: [event => validateSchema(requiredEvent, event)], // authorization header is optional
  post: [(event, result) => validateSchema(requiredAccountId, result)],
  exception: [
    function () {
      return Boom.isBoom(PromiseContract.outcome(arguments))
    },
    function () {
      return PromiseContract.outcome(arguments).message === PromiseContract.outcome(arguments).output.payload.message
    },
    function () {
      return [401, 403].includes(PromiseContract.outcome(arguments).output.statusCode)
    },
    function () {
      return !!PromiseContract.outcome(arguments).output.headers['x-www-authenticate']
    },
    function () {
      return (
        (PromiseContract.outcome(arguments).output.payload.attributes.error === 'invalid_token') ===
        (PromiseContract.outcome(arguments).output.statusCode === 401)
      )
    },
    function () {
      return (
        (PromiseContract.outcome(arguments).output.payload.attributes.error === 'insufficient_scope') ===
        (PromiseContract.outcome(arguments).output.statusCode === 403)
      )
    },
    function () {
      return (
        !PromiseContract.outcome(arguments).output.payload.attributes.verifyFailed ||
        PromiseContract.outcome(arguments).output.statusCode === 401
      )
    },
    function () {
      return (
        PromiseContract.outcome(arguments).output.statusCode !== 401 ||
        PromiseContract.outcome(arguments).output.payload.attributes.verifyFailed ||
        Object.values(getSubFromJWTContract.messages.unauthorized).includes(
          PromiseContract.outcome(arguments).output.payload.attributes.error_description
        )
      )
    },
    function () {
      return (
        (PromiseContract.outcome(arguments).output.statusCode === 403) ===
        (!PromiseContract.outcome(arguments).output.payload.attributes.verifyFailed &&
          PromiseContract.outcome(arguments).output.payload.attributes.error_description === noMatchingRAA)
      )
    }
  ]
})
getSubFromJWTContract.messages = {
  unauthorized: { ...verifyMessages, noSub, modeDoesNotMatch, raaMalFormed },
  forbidden: { noMatchingRAA }
}

/**
 * `verify` mentions expected values in its error message. We don't want to communicate this to the outside.
 */
function stripTooMuchInformation (verifyError) {
  const interestingPart = /^(.*?)(\. expected:.*)?$/.exec(verifyError.message)
  return interestingPart[1]
}

/* https://tools.ietf.org/html/rfc6750, chapter 3 defines OAuth2 error responses. They are implemented in `create401`
   and `create403`.

   According to RFC6750, both status codes require a 'WWW-Authenticate'. However, API Gateway 'remaps'
   'WWW-Authenticate' headers (see
   https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-known-issues.html#api-gateway-known-issues-rest-apis).
   The header name is changed from 'WWW-Authenticate' to 'x-amzn-remapped-www-authenticate'. It is unclear why, but not
   debatable. As a workaround, we also export 'x-www-authenticate'
 */
function create401 (errorDescription, verifyFailed) {
  const exc = Boom.unauthorized('invalid_token', 'Bearer', {
    realm: config.authorization.audience,
    charset: 'utf-8',
    error_description: errorDescription,
    verifyFailed
  })
  exc.output.headers['x-www-authenticate'] = exc.output.headers['WWW-Authenticate']
  return exc
}

// https://tools.ietf.org/html/rfc6750 chapter 3
function create403 (errorDescription, ra) {
  const exc = Boom.forbidden('insufficient_scope')
  const wwwAuthenticate = `Bearer realm="${config.authorization.audience}", charset="utf-8", error_description="${noMatchingRAA}", error="insufficient_scope"`
  exc.output.headers = {
    'x-www-authenticate': wwwAuthenticate,
    'WWW-Authenticate': wwwAuthenticate
  }
  // WWW-Authenticate
  exc.output.payload.attributes = {
    realm: config.authorization.audience,
    charset: 'utf-8',
    error: 'insufficient_scope',
    error_description: noMatchingRAA,
    ra
  }
  return exc
}

const getAuthorizedSub = getSubFromJWTContract.implementation(async function getSubFromJWT (event) {
  const keyGetter = publicSigningKeyGetter()
  const mode = event.headers['x-mode']
  // For matching modes we use `config.sts.production`. For all others we use `config.sts.test`.
  const issuer = config.sts[productionModesPattern.test(mode) ? 'production' : 'test'].issuer
  const jwt = event.headers.authorization && event.headers.authorization.slice('Bearer '.length)
  let decoded
  // TODO check required scope -> 403
  try {
    /* NOTE: jsonwebtoken.verify does a lot of work, but calls jws.decode unprotected by a catch. An extremely malformed
             token makes decode throw, e.g., when the payload is not JSON-parseable. All the possible errors are
             nigh impossible to enumerate. On the other hand, such unexpected errors are client-issues (4xx).
             Given that this code is broadely tested for configuration errors of `verify`, and that `verify` is
             extremely widely used globally, we assume that any unexpected error when calling `verify` is not a
             programming error, but an issue with the token. */
    decoded = await verify(jwt, keyGetter, {
      algorithms: ['RS256'],
      issuer,
      audience: config.authorization.audience,
      clockTolerance: 10,
      maxAge: config.sts.tokenMaxAge
    })
  } catch (err) {
    throw create401(stripTooMuchInformation(err), true)
  }
  // NOTE: we are not using scopes, so no verification of scopes
  /* Code inspection of verify makes clear that, if it does not throw, it always returns an object.
     Assert this, because this is security related code. */
  assert.ok(decoded)
  assert(typeof decoded === 'object')
  if (decoded[customClaims.modeClaim] !== mode) {
    throw create401(modeDoesNotMatch)
  }
  const validation = requiredAccountId.validate(decoded.sub && encodeURIComponent(decoded.sub))
  if (validation.error) {
    throw create401(noSub)
  }
  const raas = decoded[customClaims.raasClaim]
  if (!raas || !Array.isArray(raas) || !raas.every(raa => !!raa && typeof raa === 'string')) {
    throw create401(raaMalFormed)
  }
  if (!resourceActionAllowed(raas, event)) {
    throw create403(noMatchingRAA, `${event.httpMethod}:${event.path}`)
  }
  return validation.value
})

module.exports = getAuthorizedSub
