/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const Item = require('./Item').schema
const ModeSchema = require('../../../ppwcode/Mode').schema
const KeySchema = require('../../../ppwcode/dynamodb/Key').schema
const IndexSchema = require('../../../ppwcode/dynamodb/Index').schema
const Boom = require('@hapi/boom')
const dynamodb = require('./dynamodb')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')
const Joi = require('@hapi/joi')

const requiredMode = ModeSchema.required()
const requiredKey = KeySchema.required()
const requiredIndex = IndexSchema.required()
const requiredItemRead = Item.tailor('read').required()

const Items = Joi.array().items(requiredItemRead)

const getByIndex = new PromiseContract({
  pre: [
    mode => validateSchema(requiredMode, mode),
    (mode, partitionKey) => validateSchema(requiredKey, partitionKey),
    (mode, partitionKey, index) => validateSchema(requiredIndex, index)
  ],
  post: [
    function () {
      return validateSchema(Items, PromiseContract.outcome(arguments))
    },
    function (mode) {
      return PromiseContract.outcome(arguments).every(item => item.mode === mode)
    }
  ],
  exception: [
    // use outcome(), because consistent is optional
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    },
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ]
}).implementation(async function getByIndex (mode, partitionKey, index, sortKeyValue) {
  const dynamodbInstance = await dynamodb()

  let done = false
  let params

  if (sortKeyValue) {
    params = {
      TableName: dynamodbTableName(mode),
      IndexName: index.indexName,
      ExpressionAttributeNames: {
        '#partitionKey': index.partitionKey,
        '#sortKey': index.sortKey
      },
      ExpressionAttributeValues: {
        ':partitionKey': partitionKey,
        ':sortKey': sortKeyValue
      },
      KeyConditionExpression: '#partitionKey = :partitionKey and #sortKey = :sortKey',
      ScanIndexForward: false,
      Limit: 1000,
      ExclusiveStartKey: undefined
    }
  } else {
    params = {
      TableName: dynamodbTableName(mode),
      IndexName: index.indexName,
      ExpressionAttributeNames: {
        '#partitionKey': index.partitionKey
      },
      ExpressionAttributeValues: {
        ':partitionKey': partitionKey
      },
      KeyConditionExpression: '#partitionKey = :partitionKey',
      ScanIndexForward: false,
      Limit: 1000,
      ExclusiveStartKey: undefined
    }
  }

  let finalResult = []

  while (!done) {
    const result = await dynamodbInstance.query(params).promise()

    finalResult = finalResult.concat(result.Items.map(item => item))

    if (!result.LastEvaluatedKey) {
      // console.log('👌 all done')
      done = true
    } else {
      params.ExclusiveStartKey = result.LastEvaluatedKey
      // console.log('👁 there was more')
    }
  }

  if (finalResult.length <= 0) {
    throw Boom.notFound()
  }

  // run the results through Joi to get coercion, if any
  // we then have to deal with the errors too!
  const validationValues = []
  finalResult.forEach(item => {
    const validation = requiredItemRead.validate(item)
    if (validation.error) {
      throw validation.error
    } else {
      validationValues.push(validation.value)
    }
  })
  return validationValues
})

module.exports = getByIndex
