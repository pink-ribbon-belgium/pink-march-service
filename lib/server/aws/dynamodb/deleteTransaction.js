/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const dynamodb = require('./dynamodb')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')

async function executeTransactWrite (dynamodbInstance, params) {
  const transactionRequest = dynamodbInstance.transactWrite(params)
  let cancellationReasons
  /* istanbul ignore next */
  transactionRequest.on('extractError', response => {
    try {
      cancellationReasons = JSON.parse(response.httpResponse.body.toString()).CancellationReasons
    } catch (err) /* istanbul ignore next */ {
      // suppress this just in case some types of errors aren't JSON parseable
      console.error('Error extracting cancellation error', err)
    }
  })
  return new Promise((resolve, reject) => {
    transactionRequest.send((err, response) => {
      /* istanbul ignore next */
      if (err) {
        console.error('Error performing transactWrite', { cancellationReasons, err })
        return reject(err)
      }
      return resolve(response)
    })
  })
}

/**
 * @param {Item} item
 */
const deleteTransaction = new PromiseContract({
  pre: [(mode, keys) => keys.every(key => key.startsWith(`/${mode}/`))],
  post: [(mode, keys, result) => result === undefined]
}).implementation(async function (mode, keys) {
  const dynamodbInstance = await dynamodb()

  // Could be more generic by introducing sortKey instead of hardcoded 'actual' as sortKey
  const params = {
    TransactItems: keys.map(key => ({
      Delete: {
        TableName: dynamodbTableName(mode),
        Key: {
          key: key,
          submitted: 'actual'
        }
      }
    }))
  }
  await executeTransactWrite(dynamodbInstance, params)
})

module.exports = deleteTransaction
