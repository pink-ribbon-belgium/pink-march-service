/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const Item = require('./Item').schema
const dynamodb = require('./dynamodb')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')

const requiredItemWrite = Item.tailor('write').required()

/**
 * @param {Item} item
 */
const put = new PromiseContract({
  pre: [
    items => items.every(item => validateSchema(requiredItemWrite, item)),
    items => items.every(item => item.key.startsWith(`/${item.mode}/`)), // NOTE: cannot, as yet, be expressed in Item schema
    items => items.every(item => item.data.constructor === Object)
  ],
  post: [(items, result) => result === undefined]
}).implementation(async function (items) {
  const dynamodbInstance = await dynamodb()

  async function executeTransactWrite (params) {
    const transactionRequest = dynamodbInstance.transactWrite(params)
    let cancellationReasons
    /* istanbul ignore next */
    transactionRequest.on('extractError', response => {
      try {
        cancellationReasons = JSON.parse(response.httpResponse.body.toString()).CancellationReasons
      } catch (err) /* istanbul ignore next */ {
        // suppress this just in case some types of errors aren't JSON parseable
        console.error('Error extracting cancellation error', err)
      }
    })
    return new Promise((resolve, reject) => {
      transactionRequest.send((err, response) => {
        /* istanbul ignore next */
        if (err) {
          console.error('Error performing transactWrite', { cancellationReasons, err })
          return reject(err)
        }
        return resolve(response)
      })
    })
  }

  const params = {
    TransactItems: items.map(item => ({
      Put: {
        TableName: dynamodbTableName(item.mode),
        Item: item
      }
    }))
  }
  await executeTransactWrite(params)
})

module.exports = put
