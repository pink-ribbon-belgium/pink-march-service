/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const Item = require('./Item').schema
const ModeSchema = require('../../../ppwcode/Mode').schema
const KeySchema = require('../../../ppwcode/dynamodb/Key').schema
const Boom = require('@hapi/boom')
const dynamodb = require('./dynamodb')
const assert = require('assert')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')
const Joi = require('@hapi/joi')

const requiredMode = ModeSchema.required()
const requiredKey = KeySchema.required()
const requiredSubmitted = Joi.string()
  .isoDate()
  .required()
const requiredItemRead = Item.tailor('read').required()

/**
 * Retrieves the item with exact `key`, and most recent `submitted` equal to or before the given `submitted`.
 * Fails with a 401 if no such item is found.
 *
 * @param {string} mode
 * @param {string} key
 * @param {string} submitted - ISO-8601 string; `submitted` of the result must be equal or before this time
 * @param {boolean} [consistent]
 */
const getVersionAtOrBefore = new PromiseContract({
  pre: [
    mode => validateSchema(requiredMode, mode),
    (mode, key) => validateSchema(requiredKey, key),
    (mode, key) => key.startsWith(`/${mode}/`), // TODO cannot, as yet, be expressed in Item schema
    (mode, key, submitted) => validateSchema(requiredSubmitted, submitted),
    (mode, key, submitted, consistent) => !consistent || consistent === true
  ],
  post: [
    // use outcome(), because consistent is optional
    function () {
      return validateSchema(requiredItemRead, PromiseContract.outcome(arguments))
    },
    function (mode) {
      return PromiseContract.outcome(arguments).mode === mode
    },
    function (mode, key) {
      return PromiseContract.outcome(arguments).key === key
    },
    function (mode, key, submitted) {
      return PromiseContract.outcome(arguments).submitted <= submitted
    }
  ],
  exception: [
    // use outcome(), because consistent is optional
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    },
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ]
}).implementation(async function getVersionAtOrBefore (mode, key, submitted, consistent) {
  const dynamodbInstance = await dynamodb()

  const params = {
    TableName: dynamodbTableName(mode),
    ExpressionAttributeNames: {
      '#key': 'key',
      '#submitted': 'submitted'
    },
    ExpressionAttributeValues: {
      ':key': key,
      ':submitted': submitted
    },
    KeyConditionExpression: '#key = :key and #submitted <= :submitted',
    Limit: 1,
    ScanIndexForward: false, // true = ascending, false = descending
    ConsistentRead: consistent
  }
  const result = await dynamodbInstance.query(params).promise()

  if (result.Items.length <= 0) {
    throw Boom.notFound()
  }
  assert(result.Items.length === 1)

  const item = result.Items[0]
  const validation = requiredItemRead.validate(item)
  if (validation.error) {
    throw validation.error
  }

  return validation.value
})

module.exports = getVersionAtOrBefore
