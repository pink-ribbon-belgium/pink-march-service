/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const Item = require('./Item').schema
const dynamodb = require('./dynamodb')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')

const requiredItemWrite = Item.tailor('write').required()

/**
 * @param {Item} item
 */
const put = new PromiseContract({
  pre: [
    item => validateSchema(requiredItemWrite, item),
    item => item.key.startsWith(`/${item.mode}/`), // NOTE: cannot, as yet, be expressed in Item schema
    item => item.data.constructor === Object
  ],
  post: [(item, result) => result === undefined]
}).implementation(async function (item) {
  const dynamodbInstance = await dynamodb()

  const params = {
    TableName: dynamodbTableName(item.mode),
    Item: item
  }
  await dynamodbInstance.put(params).promise()
})

module.exports = put
