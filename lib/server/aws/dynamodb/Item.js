/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Joi = require('@hapi/joi')
const Mode = require('../../../ppwcode/Mode')
const Key = require('../../../ppwcode/dynamodb/Key')
const DTO = require('../../../ppwcode/lambda/DTO')
const UUID = require('../../../ppwcode/UUID').schema

/**
 * Wrapper for instances of business resources of the service stored in DynamoDB. All data is versioned, i.e., no
 * updates are allowed. New versions of the same object are added with a different `submitted`. The current version is
 * the version with the most recent `submitted`.
 *
 * @typedef Item
 * @type {object}
 * @property {string} mode - Type of data. The string 'production' is used for production data. Automated tests use
 *                           `automated-test-<uuid/v4>`, where `<uuid/v4>` is a UUID/v4 that uniquely identifies a test.
 *                           Quality assurance uses `qa-<version>`. Acceptance tests use `acceptance-<version>`. Demo
 *                           items are identified by `demo`.
 * @property {string} key - DynamoDB partition key. `key` MUST START WITH `/<mode>/…`.
 * @property {string} submitted - DynamoDB sort key. This is the ISO-8601 representation of SoT. Different versions of
 *                                the same item are identified by this key. This does not support more than 1 version
 *                                per ms.
 * @property {object} data - the business contents of the item
 */

/** @type {Item} */
const example = {
  mode: Mode.example,
  key: Key.example,
  submitted: DTO.exampleDynamodb.createdAt,
  submittedBy: DTO.exampleDynamodb.createdBy,
  flowId: '0873a7cb-5f2d-417a-aea7-2d543a8d14ca',
  data: DTO.exampleDynamodb
}

/** @type {Item} */
const example2 = {
  mode: Mode.example,
  key: Key.example,
  submitted: 'actual',
  submittedBy: DTO.exampleDynamodb.createdBy,
  flowId: '0873a7cb-5f2d-417a-aea7-2d543a8d14ca',
  data: DTO.exampleDynamodb
}

const Submitted = Joi.equal(Joi.ref('data.createdAt'))
  .allow('actual')
  .required()

/** @type {ObjectSchema} */
const Item = Joi.object()
  .keys({
    mode: Mode.schema.required(),
    flowId: UUID.alter({
      /* NOTE: flowId is in principle required.
               However, it is required in DynamoDB solely for reference during debug.
               It is not semantically relevant.
               This property was introduced after we went to production, so there are records in the database without
               a flowId. Since this is not semantically relevant, this is not an issue.
               Therefor, the flowId is optional on read (but it will be there for records written from now on). */
      write: schema => schema.required(),
      read: schema => schema.optional()
    }),
    key: Key.schema.required(),
    /* IDEA: key MUST START WITH mode
             We cannot express that with Joi currently, without an extension.
             https://github.com/jmac105/joi-starts-with-ref is an old example of that (that is not compatible anymore)
             Work to make our own extension for this particular case. */
    submitted: Submitted,
    submittedBy: Joi.equal(Joi.ref('data.createdBy')).required(),
    data: DTO.schema
      .tailor('dynamodb')
      .unknown(true)
      .required(),
    partition_key_A: Key.schema.optional(),
    sort_key_A: Joi.any().optional(),
    partition_key_B: Key.schema.optional(),
    sort_key_B: Joi.any().optional(),
    partition_key_C: Key.schema.optional(),
    sort_key_C: Joi.any().optional(),
    partition_key_D: Key.schema.optional(),
    sort_key_D: Joi.any().optional(),
    partition_key_E: Key.schema.optional(),
    sort_key_E: Joi.any().optional(),
    partition_key_1: Key.schema.optional(),
    sort_key_1: Joi.number().optional(),
    partition_key_2: Key.schema.optional(),
    sort_key_2: Joi.number().optional(),
    partition_key_3: Key.schema.optional(),
    sort_key_3: Joi.number().optional()
  })
  .unknown(false)
  .example(example)
  .example(example2)

module.exports = { example, exampleActual: example2, schema: Item, submittedSchema: Submitted }
