/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const config = require('config')
const path = require('path')

let instance

/**
 * This is an async function because that is required in tests.
 * Lazily get an authenticatedAWS, and create a DynamoDB DocumentClient for it.
 *
 * @returns {Promise<DocumentClient>}
 */
async function dynamodb () {
  if (!instance) {
    const projectRoot = path.join(__dirname, '..', '..', '..', '..')
    const authenticatedAWSModuleAbsolutePath = path.join(projectRoot, config.authenticatedAWS.modulePath)
    const authenticatedAWSModuleRelPath = path.relative(__dirname, authenticatedAWSModuleAbsolutePath)
    const authenticatedAWS = require(authenticatedAWSModuleRelPath)
    const AWS = await authenticatedAWS()
    instance = new AWS.DynamoDB.DocumentClient()
  }
  return instance
}

module.exports = dynamodb
