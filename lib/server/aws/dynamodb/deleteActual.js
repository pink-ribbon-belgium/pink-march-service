/*
 * Copyright (C) 2021 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../../../_util/validateSchema')
const ModeSchema = require('../../../ppwcode/Mode').schema
const KeySchema = require('../../../ppwcode/dynamodb/Key').schema
const Boom = require('@hapi/boom')
const dynamodb = require('./dynamodb')
const dynamodbTableName = require('../dynamodb/dynamodbTableName')

const requiredMode = ModeSchema.required()
const requiredKey = KeySchema.required()

const deleteActual = new PromiseContract({
  pre: [
    mode => validateSchema(requiredMode, mode),
    (mode, key) => validateSchema(requiredKey, key),
    (mode, key) => key.startsWith(`/${mode}/`)
  ],
  post: [
    function (mode) {
      return PromiseContract.outcome(arguments).mode === mode
    },
    function (mode, key) {
      return PromiseContract.outcome(arguments).key === key
    }
  ],
  exception: [
    function () {
      return PromiseContract.outcome(arguments) instanceof Error
    },
    function () {
      return PromiseContract.outcome(arguments).isBoom
    },
    function () {
      return PromiseContract.outcome(arguments).output.statusCode === 404
    }
  ]
}).implementation(async function deleteActual (mode, key) {
  const dynamodbInstance = await dynamodb()

  const params = {
    TableName: dynamodbTableName(mode),
    Key: {
      key: key,
      submitted: 'actual'
    },
    ReturnValues: 'ALL_OLD'
  }

  const result = await dynamodbInstance.delete(params).promise()
  console.log('Delete result', result)

  if (!result.Attributes) {
    throw Boom.notFound()
  }

  return result.Attributes
})

module.exports = deleteActual
