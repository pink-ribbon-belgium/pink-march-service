const Joi = require('@hapi/joi')

const example = {
  someId: '907a84e9-4cd2-4917-aae0-4f450d4ac112',
  someOtherInfo: 'this is other information',
  onlyStringsAreAllowed: 'so no numbers, booleans, …'
}

const EncryptionContext = Joi.object()
  .pattern(/\w+/, Joi.string().required())
  .min(1)
  .example(example)

module.exports = { schema: EncryptionContext, example }
