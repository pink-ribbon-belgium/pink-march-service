/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const { KmsKeyringNode, getClient } = require('@aws-crypto/client-node')
const cmkARN = require('config').kms.cmk
const path = require('path')
const config = require('config')

let keyringCache

/**
 * Lazily return a key ring (one per execution).
 */
const keyring = new PromiseContract({ post: [] }).implementation(async function keyring () {
  if (!keyringCache) {
    const projectRoot = path.join(__dirname, '..', '..', '..', '..')
    const authenticatedAWSModuleAbsolutePath = path.join(projectRoot, config.authenticatedAWS.modulePath)
    const authenticatedAWSModuleRelPath = path.relative(__dirname, authenticatedAWSModuleAbsolutePath)
    const authenticatedAWS = require(authenticatedAWSModuleRelPath)
    const AWS = await authenticatedAWS()
    const clientProvider = getClient(AWS.KMS)

    keyringCache = new KmsKeyringNode({ clientProvider, generatorKeyId: cmkARN })
  }
  return keyringCache
})

module.exports = keyring
