/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const getKeyring = require('./keyring')
const { isEqual } = require('lodash/lang')
const { decrypt: kmsDecrypt } = require('@aws-crypto/client-node')
const validateSchema = require('../../../_util/validateSchema')
const { schema: EncryptionContext } = require('./EncryptionContext')

const excMessage = 'Encryption context does not match given context'

const requiredEncryptionContext = EncryptionContext.required().label('context')

/**
 * Given a `cipher` and `context` object, decrypt the `cipher` with symmetric envelope encryption, using the
 * embedded KMS CMK as secondary encryption key as encryption key.
 *
 * `cipher` must be a base64 string, resulting from {@link ./encrypt}.
 * The result of the decryption also returns a context. The given `context` must be a deep subset of the
 * context in the `cipher`. If not, this function throws.
 *
 * @param {string} cipher
 * @param {object} context
 * @returns {string}
 */
const decrypt = new PromiseContract({
  pre: [
    cipher => !!cipher && typeof cipher === 'string',
    (cipher, context) => validateSchema(requiredEncryptionContext, context)
  ],
  post: [(cipher, context, result) => !!result && typeof result === 'string'],
  exception: [
    (cipher, context, exc) => exc instanceof Error,
    (cipher, context, exc) => exc.message === excMessage,
    (cipher, context, exc) => isEqual(exc.context, context),
    // do not enforce the schema on cipherContext
    (cipher, context, exc) => !isEqual(exc.cipherContext, context)
  ]
}).implementation(async function decrypt (cipher, context) {
  const keyring = await getKeyring()

  const buffer = Buffer.from(cipher, 'base64')
  const { plaintext, messageHeader } = await kmsDecrypt(keyring, buffer)

  const relevantCipherContext = Object.keys(context).reduce((acc, key) => {
    if (messageHeader.encryptionContext[key]) {
      acc[key] = messageHeader.encryptionContext[key]
    }
    return acc
  }, {})

  if (!isEqual(relevantCipherContext, context)) {
    const exc = new Error(excMessage)
    exc.context = context
    exc.cipherContext = relevantCipherContext
    throw exc
  }

  return plaintext.toString('utf-8')
})

module.exports = decrypt
