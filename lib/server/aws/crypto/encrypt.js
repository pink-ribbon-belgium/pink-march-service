/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const getKeyring = require('./keyring')
const { encrypt: kmsEncrypt } = require('@aws-crypto/client-node')
const validateSchema = require('../../../_util/validateSchema')
const { schema: EncryptionContext } = require('./EncryptionContext')

const requiredEncryptionContext = EncryptionContext.required().label('encryptionContext')

/**
 * Given a `cleartext` and a `encryptionContext` object, encrypt the `cleartext` with symmetric envelope encryption,
 * using a KMS CMK as secondary encryption key for the encryption key.
 *
 * The result is a base64 string, which is a self-contained structure that contains the encrypted `cleartext`, the
 * non-encrypted `encryptionContext`, the encrypted encryption key, and other information. The encrypted encryption key
 * is a self contained AWS KMS CMK data key, that contains the ID in AWS KMS of it's (secondary) encryption key.
 *
 * @param {string} cleartext
 * @param {object} context
 * @returns {string}
 */
const encrypt = new PromiseContract({
  pre: [
    cleartext => !!cleartext && typeof cleartext === 'string',
    (cleartext, encryptionContext) => validateSchema(requiredEncryptionContext, encryptionContext)
  ],
  post: [(cleartext, encryptionContext, result) => !!result && typeof result === 'string']
}).implementation(async function encrypt (cleartext, encryptionContext) {
  const keyring = await getKeyring()
  const encryptionResult = await kmsEncrypt(keyring, cleartext, { encryptionContext })
  return encryptionResult.result.toString('base64')
})

module.exports = encrypt
