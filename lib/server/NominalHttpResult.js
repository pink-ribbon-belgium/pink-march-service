/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Contract = require('@toryt/contracts-iv')
const requiredResponseSchema = require('../ppwcode/lambda/apigateway/Response').schema.required()
const validateSchema = require('../_util/validateSchema')
const requiredUUID = require('../ppwcode/UUID').schema.required()

const NominalHttpResult = new Contract({
  pre: [
    function (flowId) {
      return validateSchema(requiredUUID, flowId)
    }
  ],
  post: [
    function (flowId, result, response) {
      return response.headers['x-flow-id'] === flowId
    },
    function (flowId, result, response) {
      return response.statusCode === 200 || response.statusCode === 204
    },
    function (flowId, result, response) {
      return response.statusCode !== 200 || result !== undefined
    },
    function (flowId, result, response) {
      return response.statusCode !== 204 || result === undefined
    },
    function (flowId, result, response) {
      return response.body === JSON.stringify(result)
    },
    function (flowId, result, response) {
      return validateSchema(requiredResponseSchema, response)
    }
  ]
})

function nominalHttpResult (flowId, result) {
  return {
    statusCode: result !== undefined ? 200 : 204,
    headers: {
      'x-flow-id': flowId,
      'cache-control': 'public, max-age=5' // TODO: do not cache PUT and POST
    },
    body: JSON.stringify(result)
  }
}

module.exports = NominalHttpResult.implementation(nominalHttpResult)
