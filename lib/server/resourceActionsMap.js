/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Health = { GET: require('./business/health/get') }
const Account = { GET: require('./business/account/get') }
const Team = {
  GET: require('./business/team/get'),
  PUT: require('./business/team/put')
}
const Company = {
  GET: require('./business/company/get'),
  PUT: require('./business/company/put')
}
const AccountProfile = {
  GET: require('./business/account/profile/get'),
  PUT: require('./business/account/profile/put')
}
const AccountPreferences = {
  GET: require('./business/account/preferences/get'),
  PUT: require('./business/account/preferences/put')
}
const GroupProfile = {
  GET: require('./business/group/profile/get'),
  PUT: require('./business/group/profile/put')
}
const GroupAdministration = {
  GET: require('./business/group/administration/get'),
  PUT: require('./business/group/administration/put')
}
const TrackerConnection = {
  GET: require('./business/account/trackerConnection/get'),
  PUT: require('./business/account/trackerConnection/put'),
  DELETE: require('./business/account/trackerConnection/delete')
}
const Subgroup = {
  GET: require('./business/subgroup/get'),
  PUT: require('./business/subgroup/put'),
  DELETE: require('./business/subgroup/deleteSubgroup')
}

const AccountSlot = {
  GET: require('./business/account/slot/get'),
  PUT: require('./business/account/slot/put')
}

const AccountAggregate = {
  GET: require('./business/aggregates/account/get'),
  PUT: require('./business/aggregates/account/put')
}

const ActivitiesForAccount = require('../server/business/account/activity/getActivitiesByAccountId')
const getGroupsForAdministrator = require('../server/business/group/administration/getGroupsForAdministrator')
const postCalculation = require('../server/business/payment/calculation/post')
const getGroupVerification = require('../server/business/group/verification/get')
const postInitializePayment = require('../server/business/payment/initializePayment')
const putProcessPayment = require('../server/business/payment/processPayment')
const putClaimSlot = require('../server/business/group/claimSlot/put')
const getGroupPayments = require('../server/business/group/payments/get')
const postCompany = require('../server/business/company/post')
const postTeam = require('./business/team/post')
const getGroupSlots = require('./business/group/slots/get')
const postSubgroup = require('./business/subgroup/post')
const getMembersSubgroup = require('./business/subgroup/getMembers')
const ActivityForAccount = require('./business/activity/put')
const postActivityCalculation = require('../server/business/activity/calculation/calculateDistance')
const getAccountAggregatesForGroup = require('./business/aggregates/account/getAggregatesForGroup')
const getTeamAggregate = require('./business/aggregates/team/get')
const getAllTeamAggregates = require('./business/aggregates/team/getAllTeamAggregates')
const getAllTeamAggregatesForSubgroups = require('./business/aggregates/team/getTeamAggregatesForCompany')
const getAccountAggregatesForSubgroup = require('./business/aggregates/account/getAggregatesForSubgroup')
const getCompanyAggregate = require('./business/aggregates/company/get')
const getAllCompanyAggregates = require('./business/aggregates/company/getAllCompanyAggregates')
const getCompanyAggregatesForUnit = require('./business/aggregates/company/getCompanyAggregatesForUnit')
const getStatistics = require('./business/statistics/getStatistics')
const getMembers = require('./business/group/slots/getMembers')
const deleteTeamMember = require('./business/team/deleteMember')
const deleteCompanyMember = require('./business/company/deleteCompanyMember')
const deleteSubgroupMember = require('./business/subgroup/deleteSubgroupMember')
const getCompanySubgroups = require('./business/company/getSubgroupsForCompany')

const resourceActionsMap = {
  '/I/health': Health, // NOTE: integration test ok
  '/I/account/{accountId}': Account, // NOTE: integration test ok
  '/I/account/{accountId}/publicProfile': AccountProfile, // NOTE: integration test ok
  '/I/account/{accountId}/preferences': AccountPreferences, // NOTE: integration test ok
  '/I/account/{accountId}/administratorOf': { GET: getGroupsForAdministrator }, // NOTE: integration test ok
  '/I/account/{accountId}/slot': AccountSlot, // NOTE: integration test ok
  '/I/account/{accountId}/trackerConnection': TrackerConnection, // NOTE: integration test ok
  '/I/account/{accountId}/activities': { GET: ActivitiesForAccount },
  '/I/account/{accountId}/activity/{activityDate}': { PUT: ActivityForAccount },
  '/I/activity/calculation': { POST: postActivityCalculation },
  '/I/accountaggregate/{accountId}': AccountAggregate,
  '/I/accountaggregate/group/{groupId}': { GET: getAccountAggregatesForGroup },
  '/I/accountaggregate/subgroup/{subgroupId}': { GET: getAccountAggregatesForSubgroup },
  '/I/{groupType}/{groupId}/administrators/{accountId}': GroupAdministration, // NOTE: integration test ok
  '/I/{groupType}/{groupId}/slots': { GET: getGroupSlots }, // NOTE: integration test ok
  '/I/group/{groupId}/members': { GET: getMembers },
  '/I/team': { POST: postTeam },
  '/I/team/{teamId}': Team, // NOTE: integration test ok
  '/I/team/{teamId}/publicProfile': GroupProfile, // NOTE: integration test ok
  '/I/team/{teamId}/verification': { GET: getGroupVerification },
  '/I/team/{teamId}/payments': { GET: getGroupPayments },
  '/I/team/{teamId}/payment': { POST: postInitializePayment },
  '/I/team/{teamId}/payment/{paymentId}/process/{accountId}': { PUT: putProcessPayment },
  '/I/team/{teamId}/claimSlot/{accountId}': { PUT: putClaimSlot },
  '/I/team/{teamId}/slot/{accountId}': { PUT: deleteTeamMember },
  '/I/teamaggregate/{teamAggregateId}': { GET: getTeamAggregate },
  '/I/teamaggregate/company/{companyId}': { GET: getAllTeamAggregatesForSubgroups },
  '/I/teamaggregate/all': { GET: getAllTeamAggregates },
  '/I/companyaggregate/{companyId}': { GET: getCompanyAggregate },
  '/I/companyaggregate/unit/{unit}': { GET: getCompanyAggregatesForUnit },
  '/I/companyaggregate/all': { GET: getAllCompanyAggregates },
  '/I/company': { POST: postCompany },
  '/I/company/{companyId}': Company, // NOTE: integration test ok
  '/I/company/{companyId}/publicProfile': GroupProfile, // NOTE: integration test ok
  '/I/company/{companyId}/subgroups': { GET: getCompanySubgroups },
  '/I/company/{companyId}/verification': { GET: getGroupVerification },
  '/I/company/{companyId}/payments': { GET: getGroupPayments },
  '/I/company/{companyId}/payment': { POST: postInitializePayment },
  '/I/company/{companyId}/payment/{paymentId}/process/{accountId}': { PUT: putProcessPayment },
  '/I/company/{companyId}/claimSlot/{accountId}': { PUT: putClaimSlot },
  '/I/company/{companyId}/slot/{accountId}': { PUT: deleteCompanyMember },
  '/I/payment/calculation': { POST: postCalculation }, // NOTE: integration test ok
  '/I/subgroup': { POST: postSubgroup },
  '/I/subgroup/{subgroupId}/company/{companyId}': Subgroup,
  '/I/subgroup/{subgroupId}/company/{companyId}/members': { GET: getMembersSubgroup },
  '/I/subgroup/{subgroupId}/company/{companyId}/slot/{accountId}': { PUT: deleteSubgroupMember },
  '/I/statistics': { GET: getStatistics }
}

module.exports = resourceActionsMap
