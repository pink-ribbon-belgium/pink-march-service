/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const PromiseContract = require('@toryt/contracts-iv').Promise
const log = require('./_util/simpleLog')
const Mode = require('./ppwcode/Mode').schema
const validateSchema = require('./_util/validateSchema')
const Joi = require('@hapi/joi')
const { schema: LambdaContext } = require('./LambdaContext')
const GroupType = require('./api/group/GroupType')
const PaymentStatus = require('./api/payment/PaymentStatus')
const getByIndex = require('./server/aws/dynamodb/getByIndex')
const getVersionAtOrBefore = require('./server/aws/dynamodb/getVersionAtOrBefore')
const getAdministratorsForGroup = require('./server/business/group/administration/getAdministratorsForGroup')
const processPayment = require('./server/business/payment/processPayment')
const moment = require('moment')

const Event = Joi.object()
  .keys({
    mode: Mode.required()
  })
  .unknown(true)
  .required()
  .label('event')

const handler = new PromiseContract({
  pre: [event => validateSchema(Event, event), (event, context) => validateSchema(LambdaContext, context)],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
})

async function getPaymentsToVerify (mode, sot, flowId) {
  const index = {
    indexName: 'Index_B',
    partitionKey: 'partition_key_B',
    sortKey: 'sort_key_B'
  }

  const partitionKey = `/${mode}/status/Initialized/payments`
  const result = await getByIndex(mode, partitionKey, index)

  const resultsToHandle = result.filter(i => i.data.amount > 0)

  const paymentKeys = resultsToHandle.map(p => p.key)

  const uniquePaymentKeys = paymentKeys.reduce(
    (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
    []
  )

  const latestVersionsOfPayments = await Promise.all(
    uniquePaymentKeys.map(item => getVersionAtOrBefore(mode, item, sot))
  )

  const paymentsToVerify = latestVersionsOfPayments.filter(
    i => i.data.paymentStatus !== PaymentStatus.PAYMENT_REQUESTED
  )

  const limitDate = moment(new Date())
    .subtract(5, 'days')
    .toDate()

  const filterDate = limitDate.toISOString()

  const endDate = moment(new Date())
    .subtract(2, 'minutes')
    .toDate()
  const filterEndDate = endDate.toISOString()

  // Only verify payments of last 5 days until (2 minutes ago - don't process pending payments from UI) & payments that are not invalid or incomplete
  const filteredPayments = paymentsToVerify.filter(
    p =>
      p.data.paymentStatus !== PaymentStatus.INVALID_OR_INCOMPLETE &&
      p.data.paymentInitiatedAt > filterDate &&
      p.data.paymentInitiatedAt < filterEndDate
  )

  log.info(module, 'getPaymentsToVerify', flowId, 'NON_PROCESSED_PAYMENTS_RECEIVED', {
    sot,
    mode,
    nrOfPaymentsToVerify: filteredPayments.length,
    payments: filteredPayments.map(p => p.data.paymentId)
  })

  return filteredPayments
}

async function handlerImpl (event, context) {
  const { awsRequestId: flowId } = context // awsRequestId happens to be a UUID, used as flowId
  const { mode } = event
  const sot = new Date().toISOString()
  const sub = 'verifyNonProcessedPayments'

  try {
    log.info(module, 'handler', flowId, 'CALLED', {
      event,
      context,
      sot,
      mode
    })

    const paymentsToVerify = await getPaymentsToVerify(mode, sot, flowId)

    let processedAsPayed = 0

    const outcomes = await Promise.allSettled(
      paymentsToVerify.map(async p => {
        const groupAdministrations = await getAdministratorsForGroup(
          sot,
          sub,
          mode,
          flowId,
          p.data.groupType === GroupType.teamType ? { teamId: p.data.groupId } : { companyId: p.data.groupId }
        )
        try {
          log.info(module, 'handler', flowId, 'START_PROCESS_PAYMENT', {
            accountId: groupAdministrations[0].accountId,
            paymentId: p.data.paymentId,
            mode
          })
          await processPayment(sot, sub, mode, flowId, {
            accountId: groupAdministrations[0].accountId,
            paymentId: p.data.paymentId
          })

          log.info(module, 'handler', flowId, 'PROCESS_PAYMENT_SUCCESS', {
            accountId: groupAdministrations[0].accountId,
            paymentId: p.data.paymentId,
            mode
          })
          processedAsPayed++
        } catch (err) {
          if (err.isBoom && err.output.statusCode !== 403) {
            log.info(module, 'handler', flowId, 'PROCESS_PAYMENT_FAILED', {
              accountId: groupAdministrations[0].accountId,
              paymentId: p.data.paymentId,
              mode,
              error: err
            })
            throw err
          }
          log.info(module, 'handler', flowId, 'PROCESS_PAYMENT_FORBIDDEN', {
            accountId: groupAdministrations[0].accountId,
            paymentId: p.data.paymentId,
            mode
          })
        }
      })
    )

    log.info(module, 'handler', flowId, 'ALL_PAYMENTS_PROCESSED', {
      mode
    })

    const errors = outcomes.filter(o => o.status === 'rejected')
    const successes = outcomes.filter(o => o.status === 'fulfilled')

    log.info(module, 'handler', flowId, 'NOMINAL_END', {
      sot,
      mode,
      paymentVerificationResult: {
        processedAsPayed,
        successes: successes.length,
        errors: errors.length
      }
    })
  } catch (err) {
    log.error(module, 'handler', flowId, err)
    // do not rethrow: nobody is listening
  }
}

module.exports = { handler: handler.implementation(handlerImpl) }
