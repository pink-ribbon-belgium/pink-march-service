/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const { schema: Item, example: exampleSubmitted, exampleActual } = require('../server/aws/dynamodb/Item')

// https://docs.aws.amazon.com/lambda/latest/dg/with-ddb.html
// https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html

const insertExample = {
  mode: exampleSubmitted.mode,
  event: 'INSERT',
  keys: { key: exampleSubmitted.key, submitted: exampleSubmitted.submitted },
  newItem: exampleSubmitted
}
const modifyExample = {
  mode: exampleActual.mode,
  event: 'MODIFY',
  keys: { key: exampleActual.key, submitted: exampleActual.submitted },
  oldItem: {
    ...exampleActual,
    flowId: '91621fc0-1cae-4bdb-9a52-153d6ddbdebd'
  },
  newItem: {
    ...exampleActual,
    flowId: 'e3105ab5-fcfe-4be8-b1ce-485d30b04653',
    data: { ...exampleActual.data, createdAt: '2020-04-02T20:39:45.601Z' }
  }
}

/**
 * @typedef RequiredStreamRecord
 * @property mode {Mode}
 * @property event {'INSERT'|'MODIFY'}
 * @property keys.key {String}
 * @property keys.submitted {String}
 * @property [oldItem] {Item}
 * @property newItem {Item}
 */

const ValidStreamRecord = Joi.object()
  .keys({
    mode: Joi.equal(Joi.ref('newItem.mode')).required(),
    event: Joi.valid('INSERT', 'MODIFY').required(),
    keys: Joi.object()
      .keys({
        key: Joi.equal(Joi.ref('...newItem.key')).required(),
        submitted: Joi.equal(Joi.ref('...newItem.submitted')).required()
      })
      .unknown(false)
      .required(),
    oldItem: Item.when('event', {
      switch: [
        { is: 'INSERT', then: Joi.forbidden() },
        {
          is: 'MODIFY',
          then: Joi.required()
        }
      ]
    })
      .assert('.mode', Joi.ref('...mode'), '=== mode')
      .assert('.key', Joi.ref('...keys.key'), '=== keys.key')
      .assert('.submitted', Joi.ref('...keys.submitted'), '=== keys.submitted'),
    newItem: Item.required()
  })
  .unknown(true)
  .required()
  .example(insertExample)
  .example(modifyExample)
  .label('streamRecord')

module.exports = { schema: ValidStreamRecord, insertExample, modifyExample }
