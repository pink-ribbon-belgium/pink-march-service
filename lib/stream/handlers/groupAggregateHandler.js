/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const streamRecordHandlerContract = require('../StreamRecordHandlerContract')
const dynamodb = require('../../server/aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../server/aws/dynamodb/dynamodbTableName')
const simpleLog = require('../../_util/simpleLog')
const getActual = require('../../server/aws/dynamodb/getActual')
const getVersionAtOrBefore = require('../../server/aws/dynamodb/getVersionAtOrBefore')
const getTeam = require('../../server/business/team/get')
const getCompany = require('../../server/business/company/get')

async function groupAggregateStreamRecordHandler (flowId, sot, streamRecord) {
  const isGroupProfile =
    streamRecord.keys.key === `/${streamRecord.mode}/group/${streamRecord.newItem.data.id}/publicProfile`

  const isSlotWithSubgroupId =
    streamRecord.keys.key ===
      `/${streamRecord.mode}/group/${streamRecord.newItem.data.groupId}/slot/${streamRecord.newItem.data.id}` &&
    streamRecord.newItem.data.subgroupId &&
    streamRecord.keys.submitted === 'actual'

  const wasSlotWithSubgroupId =
    streamRecord.keys.key ===
      `/${streamRecord.mode}/group/${streamRecord.newItem.data.groupId}/slot/${streamRecord.newItem.data.id}` &&
    streamRecord.oldItem &&
    streamRecord.oldItem.data.subgroupId &&
    !streamRecord.newItem.data.subgroupId &&
    streamRecord.keys.submitted === 'actual'

  const isSubgroupProfile =
    streamRecord.keys.key === `/${streamRecord.mode}/subgroup/${streamRecord.newItem.data.id}` &&
    streamRecord.keys.submitted === 'actual'

  const isAccountAggregate =
    streamRecord.keys.key === `/${streamRecord.mode}/accountAggregate/${streamRecord.newItem.data.accountId}` &&
    streamRecord.keys.submitted === 'actual'

  if (!isGroupProfile && !isSlotWithSubgroupId && !isSubgroupProfile && !wasSlotWithSubgroupId && !isAccountAggregate) {
    // Extend logging when adding additional checks
    // simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'NOT HANDLED', {
    //   mode: streamRecord.mode,
    //   key: streamRecord.keys.key,
    //   streamRecord: streamRecord,
    //   isGroupProfile: isGroupProfile,
    //   isSlotWithSubgroupId: isSlotWithSubgroupId,
    //   wasSlotWithSubgroupId: wasSlotWithSubgroupId,
    //   isAccountAggregate: isAccountAggregate
    // })
    return false
  }

  /* istanbul ignore else */
  if (isGroupProfile) {
    simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'CALL processGroupProfile', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processGroupProfile(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (isSlotWithSubgroupId) {
    simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'CALL processSlot', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processSlot(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (isSubgroupProfile) {
    simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'CALL processSubgroupProfile', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processSubgroupProfile(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (wasSlotWithSubgroupId) {
    simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'CALL processDeleteAccountFromSubgroup', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processDeleteAccountFromSubgroup(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (isAccountAggregate) {
    simpleLog.info(module, 'groupAggregateStreamRecordHandler', flowId, 'CALL processAccountAggregate', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processDeleteAccountAggregate(streamRecord, sot, flowId)
  }
}

/**
 * Updates the GroupAggregate based on the GroupProfile streamrecord
 *  => update name of groupAggregate
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processGroupProfile (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processGroupProfile', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  // Do nothing if no relevant data is changed
  if (
    streamRecord.event === 'MODIFY' &&
    streamRecord.newItem.data.name === streamRecord.oldItem.data.name &&
    streamRecord.newItem.data.logo === streamRecord.oldItem.data.logo
  ) {
    simpleLog.info(module, 'processGroupProfile', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update'
    })
    return false
  }

  let aggregateType

  try {
    await getTeam(sot, 'groupAggregateStreamRecordHandler', streamRecord.mode, flowId, {
      teamId: streamRecord.newItem.data.id
    })
    aggregateType = 'teamAggregate'
  } catch (err) {
    try {
      await getCompany(sot, 'groupAggregateStreamRecordHandler', streamRecord.mode, flowId, {
        companyId: streamRecord.newItem.data.id
      })
      aggregateType = 'companyAggregate'
    } catch (err) {
      simpleLog.error(module, 'teamAggregateStreamRecordHandler', flowId, err)
      return false
    }
  }

  const groupAggregateKey = `/${streamRecord.mode}/${aggregateType}/${streamRecord.newItem.data.id}`

  simpleLog.info(module, 'processGroupProfile', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord,
    aggregateKey: groupAggregateKey
  })

  const newName = `${streamRecord.newItem.data.name}`
  const hasLogo = !!streamRecord.newItem.data.logo

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: groupAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#name = :name, #data.#hasLogo = :hasLogo',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#name': 'name',
      '#hasLogo': 'hasLogo'
    },
    ExpressionAttributeValues: {
      ':name': newName,
      ':hasLogo': hasLogo
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processGroupProfile', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await upgateGroupAggregate(params, flowId, 'GroupProfile')
}

/**
 * Updates the accountAggregate => set subgroupId of accountAggregate with the value from the slot
 * add existing activities to total of subgroup TeamAggregate
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processSlot (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processSlot', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  if (
    streamRecord.event === 'INSERT' ||
    (streamRecord.event === 'MODIFY' && streamRecord.oldItem.data.subgroupId === streamRecord.newItem.data.subgroupId)
  ) {
    simpleLog.info(module, 'processSlot', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update'
    })
    return false
  }

  simpleLog.info(module, 'processSlot', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  const accountAggregateKey = `/${streamRecord.mode}/accountAggregate/${streamRecord.newItem.data.accountId}`

  const accountAggregate = await getActual(streamRecord.mode, accountAggregateKey, true)

  simpleLog.info(module, 'processSlot', flowId, 'GET AccountAggregate', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    accountAggregate: accountAggregate
  })

  const teamAggregateKey = `/${streamRecord.mode}/teamAggregate/${streamRecord.newItem.data.subgroupId}`

  const teamAggregate = await getActual(streamRecord.mode, teamAggregateKey, true)
  simpleLog.info(module, 'processSlot', flowId, 'AGGREGATE TO UPDATE ', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    teamAggregate: teamAggregate
  })

  const stepsToAdd = accountAggregate.data.totalSteps
  const distanceToAdd = accountAggregate.data.totalDistance

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: teamAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = #data.#totalSteps + :stepsToAdd, #data.#totalDistance = #data.#totalDistance + :distanceToAdd',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance'
    },
    ExpressionAttributeValues: {
      ':stepsToAdd': stepsToAdd,
      ':distanceToAdd': distanceToAdd
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processSlot', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await upgateGroupAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'Slot')
}

/**
 * Updates the teamAggregate => update totalSteps and totalDistance
 * subtract existing activities from total of subgroup TeamAggregate when removed from subgroup
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processDeleteAccountFromSubgroup (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  if (streamRecord.event === 'INSERT' || (streamRecord.event === 'MODIFY' && !streamRecord.newItem.data.accountId)) {
    simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update - account was deleted'
    })
    return false
  }

  simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  const accountAggregateKey = `/${streamRecord.mode}/accountAggregate/${streamRecord.newItem.data.accountId}`

  const accountAggregate = await getActual(streamRecord.mode, accountAggregateKey, true)

  simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'GET AccountAggregate', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    accountAggregate: accountAggregate
  })

  const teamAggregateKey = `/${streamRecord.mode}/teamAggregate/${streamRecord.oldItem.data.subgroupId}`

  const teamAggregate = await getActual(streamRecord.mode, teamAggregateKey, true)
  simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'AGGREGATE TO UPDATE ', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    teamAggregate: teamAggregate
  })

  const stepsToDelete = accountAggregate.data.totalSteps
  const distanceToDelete = accountAggregate.data.totalDistance

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: teamAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = #data.#totalSteps - :stepsToDelete, #data.#totalDistance = #data.#totalDistance - :distanceToDelete',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance'
    },
    ExpressionAttributeValues: {
      ':stepsToDelete': stepsToDelete,
      ':distanceToDelete': distanceToDelete
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processDeleteAccountFromSubgroup', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await upgateGroupAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'Slot')
}

/**
 * Updates the TeamAggregate based on the GroupProfile streamrecord
 *  => update name and/or logo of teamAggregate
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processSubgroupProfile (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processSubgroupProfile', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  // Do nothing if no relevant data is changed
  if (
    streamRecord.event === 'MODIFY' &&
    streamRecord.oldItem.data.name === streamRecord.newItem.data.name &&
    streamRecord.oldItem.data.logo === streamRecord.newItem.data.logo
  ) {
    simpleLog.info(module, 'processSubgroupProfile', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update'
    })
    return false
  }

  const teamAggregateKey = `/${streamRecord.mode}/teamAggregate/${streamRecord.newItem.data.id}`
  const groupProfileKey = `/${streamRecord.mode}/group/${streamRecord.newItem.data.groupId}/publicProfile`

  simpleLog.info(module, 'processSubgroupProfile', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord,
    aggregateKey: teamAggregateKey
  })

  const groupProfile = await getVersionAtOrBefore(streamRecord.mode, groupProfileKey, sot)

  const newName = `${streamRecord.newItem.data.name}`
  const newExtendedName = `${streamRecord.newItem.data.name} (${groupProfile.data.name})`
  const hasLogo = !!streamRecord.newItem.data.logo

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: teamAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#name = :name, #data.#hasLogo = :hasLogo, #data.#extendedName = :extendedName',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#name': 'name',
      '#extendedName': 'extendedName',
      '#hasLogo': 'hasLogo'
    },
    ExpressionAttributeValues: {
      ':name': newName,
      ':extendedName': newExtendedName,
      ':hasLogo': hasLogo
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processSubgroupProfile', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await upgateGroupAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'Subgroup')
}

/**
 * Updates the GroupAggregate based on the AccountAggregate streamrecord
 *  => update totalSteps and totalDistance groupAggregate
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processDeleteAccountAggregate (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processDeleteAccountAggregate', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  // Do nothing if no relevant data is changed
  if (streamRecord.event === 'MODIFY' && streamRecord.newItem.data.groupId) {
    simpleLog.info(module, 'processDeleteAccountAggregate', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update - Account is not removed from group'
    })
    return false
  }

  let aggregateKey
  if (streamRecord.oldItem.data.groupType === 'Company') {
    aggregateKey = `/${streamRecord.mode}/companyAggregate/${streamRecord.oldItem.data.groupId}`
  } else {
    /* istanbul ignore else */
    if (streamRecord.oldItem.data.groupType === 'Team') {
      aggregateKey = `/${streamRecord.mode}/teamAggregate/${streamRecord.oldItem.data.groupId}`
    }
  }

  const stepsToDelete = streamRecord.oldItem.data.totalSteps
  const distanceToDelete = streamRecord.oldItem.data.totalDistance

  simpleLog.info(module, 'processDeleteAccountAggregate', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord,
    aggregateKey,
    stepsToDelete,
    distanceToDelete
  })

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: aggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = #data.#totalSteps - :stepsToDelete, #data.#totalDistance = #data.#totalDistance - :distanceToDelete',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance'
    },
    ExpressionAttributeValues: {
      ':stepsToDelete': stepsToDelete,
      ':distanceToDelete': distanceToDelete
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processSubgroupProfile', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await upgateGroupAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'AccountAggregate')
}

/**
 * Execute dynamoDb update statement based on the provided params
 * @param params params for update statement
 * @param flowId flowId
 * @param mode mode
 * @param key key of the entity
 * @param entityName Name of the entity which triggered the update
 * @returns {Promise<void>}
 */
async function upgateGroupAggregate (params, flowId, mode, key, entityName) {
  simpleLog.info(module, 'updateGroupAggregate', flowId, 'CALLED', {
    mode: mode,
    key: key,
    params: params
  })
  const dynamodbInstance = await dynamodb()

  simpleLog.info(module, 'updateGroupAggregate', flowId, 'DYNAMODB OK', { mode: mode, key: key })

  const result = await dynamodbInstance.update(params).promise()

  simpleLog.info(module, 'updateGroupAggregate', flowId, 'END', { mode: mode, key: key, result: result })
}

module.exports = streamRecordHandlerContract.implementation(groupAggregateStreamRecordHandler)
