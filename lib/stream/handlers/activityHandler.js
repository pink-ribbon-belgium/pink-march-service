/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const streamRecordHandlerContract = require('../StreamRecordHandlerContract')
const GroupType = require('../../api/group/GroupType')
const dynamodb = require('../../server/aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../server/aws/dynamodb/dynamodbTableName')
const getSlotByAccountId = require('../../server/business/account/slot/get')
const getAccountProfile = require('../../server/business/account/profile/get')
const activityCalculations = require('../../api/activity/ActivityCalculations')
const simpleLog = require('../../_util/simpleLog')
const getByIndex = require('../../server/aws/dynamodb/getByIndex')
const Contract = require('@toryt/contracts-iv')
const { schema: RequiredStreamRecord } = require('../ValidStreamRecord')
const validateSchema = require('../../_util/validateSchema')
const { ok } = require('assert')
const { schema: Mode } = require('../../ppwcode/Mode')
const { schema: PointInTime } = require('../../ppwcode/PointInTime')
const { schema: AccountId } = require('../../api/account/AccountId')
const { schema: UUID } = require('../../ppwcode/UUID')
const { schema: Key } = require('../../ppwcode/dynamodb/Key')
const { Gender } = require('../../api/account/AccountProfileDTO')

const RequiredMode = Mode.required()
const RequiredPointInTime = PointInTime.required()
const RequiredAccountId = AccountId.required()
const RequiredUUID = UUID.required()
const RequiredKey = Key.required()
const RequiredGender = Gender.required()

/**
 * Sub in persistence for this stream handler.
 *
 * @type {String}
 */
const sub = 'activityHandler'

function keyPattern (mode) {
  return new RegExp(`\\/${mode}\\/activity\\/account\\/[^/]+\\/date/[^/]+`)
}

/**
 * `streamRecord` is an `actual` {@link Activity} change stream record.
 *
 * @param streamRecord {RequiredStreamRecord}
 * @result {boolean}
 */
const isAnActualActivityStreamRecord = new Contract({
  pre: [streamRecord => validateSchema(RequiredStreamRecord, streamRecord)],
  post: [
    (streamRecord, result) => typeof result === 'boolean',
    (streamRecord, result) => !result || keyPattern(streamRecord.mode).test(streamRecord.keys.key),
    (streamRecord, result) => !result || streamRecord.keys.submitted === 'actual'
  ]
}).implementation(function isAnActualActivityStreamRecord (streamRecord) {
  return (
    streamRecord.keys.key ===
      `/${streamRecord.mode}/activity/account/${streamRecord.newItem.data.accountId}/date/${streamRecord.newItem.data.activityDate}` &&
    streamRecord.keys.submitted === 'actual'
  )
})

/**
 * `streamRecord` is an `INSERT`, or a deletion or re-creation of an existing {@link Activity}, or a change an
 * {@link Activity} that wasn't deleted earlier.
 *
 * @param streamRecord {RequiredStreamRecord}
 * @result {boolean}
 */
const isRelevantChange = new Contract({
  pre: [
    streamRecord => validateSchema(RequiredStreamRecord, streamRecord),
    streamRecord => isAnActualActivityStreamRecord(streamRecord)
  ],
  post: [
    (streamRecord, result) => typeof result === 'boolean',
    // INSERT is always relevant
    (streamRecord, result) => streamRecord.event !== 'INSERT' || result === true,
    // items that are deleted or re-created in this change are relevant
    (streamRecord, result) =>
      !(streamRecord.event === 'MODIFY' && streamRecord.oldItem.data.deleted !== streamRecord.newItem.data.deleted) ||
      result === true,
    // old deleted items that are not re-created are always irrelevant
    (streamRecord, result) =>
      !(
        streamRecord.event === 'MODIFY' &&
        streamRecord.oldItem.data.deleted === streamRecord.newItem.data.deleted &&
        streamRecord.oldItem.data.deleted === true
      ) || result === false,
    // if the change is for a non-deleted record, it is relevant
    (streamRecord, result) =>
      !(
        streamRecord.event === 'MODIFY' &&
        streamRecord.oldItem.data.deleted === streamRecord.newItem.data.deleted &&
        streamRecord.oldItem.data.deleted === false
      ) || result === true
  ]
}).implementation(function isRelevantChange (streamRecord) {
  return (
    streamRecord.event !== 'MODIFY' || // insert
    // either delete, or re-create, or MODIFY of an existing record; not a MODIFY of a record that was and stays deleted
    !streamRecord.oldItem.data.deleted ||
    !streamRecord.newItem.data.deleted
  )
})

/**
 * Get the steps to add from the stream record
 *
 * @param streamRecord {RequiredStreamRecord}
 * @returns {number}
 */
const getStepsToAdd = new Contract({
  pre: [
    streamRecord => validateSchema(RequiredStreamRecord, streamRecord),
    streamRecord =>
      streamRecord.event !== 'MODIFY' ||
      !(streamRecord.oldItem.data.deleted === true && streamRecord.newItem.data.deleted === true)
  ],
  post: [
    (streamRecord, result) => typeof result === 'number',
    // if activity is inserted, or a deleted record is re-added in this change, add the number of steps
    (streamRecord, result) =>
      !(
        streamRecord.event === 'INSERT' ||
        /* MODIFY */ (streamRecord.oldItem.data.deleted === true && streamRecord.newItem.data.deleted === false)
      ) || result === streamRecord.newItem.data.numberOfSteps,
    // if activity is 'deleted' in this change, subtract the number of steps
    (streamRecord, result) =>
      !(
        streamRecord.event === 'MODIFY' &&
        streamRecord.oldItem.data.deleted === false &&
        streamRecord.newItem.data.deleted === true
      ) || result === -streamRecord.oldItem.data.numberOfSteps,
    // if a non-deleted record changes, add the delta
    (streamRecord, result) =>
      !(
        streamRecord.event === 'MODIFY' &&
        streamRecord.oldItem.data.deleted === false &&
        streamRecord.newItem.data.deleted === false
      ) || result === streamRecord.newItem.data.numberOfSteps - streamRecord.oldItem.data.numberOfSteps
  ]
}).implementation(function getStepsToAdd (streamRecord) {
  if (streamRecord.event === 'INSERT') {
    return streamRecord.newItem.data.numberOfSteps // can be 0
  }

  ok(streamRecord.event === 'MODIFY')
  ok(streamRecord.oldItem)

  if (streamRecord.oldItem.data.deleted === false && streamRecord.newItem.data.deleted === true) {
    return streamRecord.oldItem.data.numberOfSteps * -1 // can be 0
  }

  if (streamRecord.oldItem.data.deleted === true && streamRecord.newItem.data.deleted === false) {
    return streamRecord.newItem.data.numberOfSteps // can be 0
  }

  ok(streamRecord.oldItem.data.deleted === false && streamRecord.newItem.data.deleted === false)
  // otherwise add the delta, can be negative; can be 0
  return streamRecord.newItem.data.numberOfSteps - streamRecord.oldItem.data.numberOfSteps
})

/**
 * Get the gender from the profile of `accountId`.
 *
 * @param sot {String}
 * @param mode  {String}
 * @param accountId  {String}
 * @param flowId  {String}
 * @param streamRecordKey  {String}
 *
 * @returns {Promise<String>}
 */
const getGender = new Contract.Promise({
  pre: [
    sot => validateSchema(RequiredPointInTime, sot),
    (sot, mode) => validateSchema(RequiredMode, mode),
    (sot, mode, accountId) => validateSchema(RequiredAccountId, accountId),
    (sot, mode, accountId, flowId) => validateSchema(RequiredUUID, flowId),
    (sot, mode, accountId, flowId, streamRecordKey) => validateSchema(RequiredKey, streamRecordKey)
  ],
  post: [(sot, mode, accountId, flowId, streamRecordKey, result) => validateSchema(RequiredGender, result)]
}).implementation(async function getGender (sot, mode, accountId, flowId, streamRecordKey) {
  // simpleLog.info(module, 'getGender', flowId, 'CALLED', {
  //   mode,
  //   key: streamRecordKey,
  //   accountId
  // })

  const accountProfile = await getAccountProfile(sot, sub, mode, flowId, {
    accountId: accountId
  })

  const gender = accountProfile.gender
  // simpleLog.info(module, 'getGender', flowId, 'RECEIVED AccountProfile', {
  //   mode: mode,
  //   key: streamRecordKey,
  //   accountId,
  //   accountProfile,
  //   gender
  // })

  return gender
})

const groupAggregateUpdateExpression =
  'SET #data.#totalSteps = #data.#totalSteps + :stepsToAdd, #data.#totalDistance = #data.#totalDistance + :distanceToAdd'
const accountAggregateUpdateExpression = `${groupAggregateUpdateExpression}, #sort_key_1 = #sort_key_1 + :stepsToAdd, #sort_key_2 = #sort_key_2 + :stepsToAdd, #sort_key_3 = #sort_key_3 + :stepsToAdd`
const groupAggregationExpressionAttributeNames = {
  '#data': 'data',
  '#totalSteps': 'totalSteps',
  '#totalDistance': 'totalDistance'
}
const accountAggregateExpressionAttributeNames = {
  ...groupAggregationExpressionAttributeNames,
  '#sort_key_1': 'sort_key_1',
  '#sort_key_2': 'sort_key_2',
  '#sort_key_3': 'sort_key_3'
}

// TODO we do not keep history of the changes. Should we?
/**
 * Execute dynamoDb update statement based on the provided params
 *
 * @param mode {Mode}
 * @param flowId {UUID}
 * @param updateExpression {String}
 * @param expressionAttributeNames {Object}
 * @param aggregateKey {String}
 * @param stepsToAdd {Number}
 * @param distanceToAdd {Number}
 * @returns {Promise<void>}
 */
async function updateAggregate (
  mode,
  flowId,
  updateExpression,
  expressionAttributeNames,
  aggregateKey,
  stepsToAdd,
  distanceToAdd
) {
  // simpleLog.info(module, 'updateAggregate', flowId, 'CALLED', {
  //   mode: mode,
  //   params: { aggregateKey, stepsToAdd, distanceToAdd }
  // })

  const dynamodbInstance = await dynamodb()
  const result = await dynamodbInstance
    .update({
      TableName: dynamodbTableName(mode),
      Key: {
        key: aggregateKey,
        submitted: 'actual'
      },
      UpdateExpression: updateExpression,
      ExpressionAttributeNames: expressionAttributeNames,
      ExpressionAttributeValues: {
        ':stepsToAdd': stepsToAdd,
        ':distanceToAdd': distanceToAdd
      },
      ReturnValues: 'UPDATED_NEW'
    })
    .promise()
  /* TODO With ALL_NEW, we get back the item in the new state. We should also save that as a history item, with the correct sot and sub,
          to see history in the DB. */

  simpleLog.info(module, 'updateAggregate', flowId, 'END', {
    mode: mode,
    result: result
  })
}

const indexB = {
  indexName: 'Index_B',
  partitionKey: 'partition_key_B',
  sortKey: 'sort_key_B'
}

// TODO should really be a separate module; this is useful functionality
// TODO getting everything by index, and then counting, is extremely expensive; can't we count directly?
async function getNumberOfGroupSlots (mode, groupId) {
  const memberKey = `/${mode}/group/${groupId}/members`
  const freeKey = `/${mode}/group/${groupId}/free`

  return (await Promise.allSettled([getByIndex(mode, memberKey, indexB), getByIndex(mode, freeKey, indexB)]))
    .filter(r => r.status === 'fulfilled')
    .map(r => r.value)
    .flat().length
}

/**
 * Execute dynamoDb update statement based on the provided params
 *
 * @param mode {Mode}
 * @param flowId {UUID}
 * @param aggregateKey {String}
 * @param stepsToAdd {Number}
 * @param distanceToAdd {Number}
 * @returns {Promise<void>}
 */
async function updateGroupAggregate (mode, flowId, aggregateKey, stepsToAdd, distanceToAdd) {
  return updateAggregate(
    mode,
    flowId,
    groupAggregateUpdateExpression,
    groupAggregationExpressionAttributeNames,
    aggregateKey,
    stepsToAdd,
    distanceToAdd
  )
}

/**
 * Update the company aggregate, and the subgroup aggregate if `subgroupId` is given.
 *
 * @param mode {Mode}
 * @param flowId {UUID}
 * @param companyId {GroupId}
 * @param subgroupId {GroupId}
 * @param stepsToAdd {number}
 * @param distanceToAdd {number}
 * @returns {Promise<void>}
 */
async function updateCompanyAndSubgroupAggregate (mode, flowId, companyId, subgroupId, stepsToAdd, distanceToAdd) {
  const companyAggregateUpdate = updateGroupAggregate(
    mode,
    flowId,
    `/${mode}/companyAggregate/${companyId}`,
    stepsToAdd,
    distanceToAdd
  )
  /* prettier-ignore */
  return subgroupId
    ? Promise.all([
      companyAggregateUpdate,
      updateGroupAggregate(mode, flowId, `/${mode}/teamAggregate/${subgroupId}`, stepsToAdd, distanceToAdd)
    ])
    : companyAggregateUpdate
}

/**
 * Update teamAggregate if account is member of a team with more than 1 member.
 *
 * @param mode {Mode}
 * @param flowId {UUID}
 * @param teamId {GroupId}
 * @param stepsToAdd {number}
 * @param distanceToAdd {number}
 * @returns {Promise<void>}
 */
async function updateTeamAggregateIfNecessary (mode, flowId, teamId, stepsToAdd, distanceToAdd) {
  const numberOfSlots = await getNumberOfGroupSlots(mode, teamId)
  if (numberOfSlots > 1) {
    return updateGroupAggregate(mode, flowId, `/${mode}/teamAggregate/${teamId}`, stepsToAdd, distanceToAdd)
  }
}

/**
 * Updates the AccountAggregate and group aggregate, i.e., add number of steps & distance, based on the Activity stream
 * record.
 *
 * @param streamRecord {RequiredStreamRecord}
 * @param sot {String}
 * @param flowId {String}
 * @returns {Promise<void>|false}
 */
async function activityHandler (flowId, sot, streamRecord) {
  if (!isAnActualActivityStreamRecord(streamRecord) || !isRelevantChange(streamRecord)) {
    // it would generate far too many logs if we logged this
    return false
  }
  const stepsToAdd = getStepsToAdd(streamRecord)
  if (stepsToAdd === 0) {
    // it would generate far too many logs if we logged this
    return false
  }

  const mode = streamRecord.mode

  simpleLog.info(module, 'activityHandler', flowId, 'CALLED and deemed relevant change', {
    mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  /* We need
     - the gender of the account (to calculate the distance)
     - the slot (to determine the type of group we are in, because updates are different)

     First we will try to get slot for the account.
     When a member is removed from a team or company, the occupied slot will be updated before this handler will be reached.
     The slot linked to the account will not be found.
   */
  let accountSlot
  try {
    accountSlot = await getSlotByAccountId(sot, 'groupAggregateHandler', mode, flowId, {
      accountId: streamRecord.newItem.data.accountId
    })
  } catch (err) {
    return false
  }

  const gender = await getGender(sot, mode, streamRecord.newItem.data.accountId, flowId, streamRecord.keys.key)

  const distanceToAdd = activityCalculations.calculateDistanceFromSteps(gender, stepsToAdd, 0)

  simpleLog.info(module, 'activityHandler', flowId, 'DETERMINED, starting aggregate updates …', {
    mode,
    key: streamRecord.keys.key,
    gender,
    accountSlot,
    stepsToAdd,
    distanceToAdd
  })

  await Promise.all([
    updateAggregate(
      mode,
      flowId,
      accountAggregateUpdateExpression,
      accountAggregateExpressionAttributeNames,
      `/${mode}/accountAggregate/${streamRecord.newItem.data.accountId}`,
      stepsToAdd,
      distanceToAdd
    ),
    /* prettier-ignore */
    accountSlot.groupType === GroupType.companyType
      ? updateCompanyAndSubgroupAggregate(
        mode,
        flowId,
        accountSlot.groupId,
        accountSlot.subgroupId,
        stepsToAdd,
        distanceToAdd
      )
      : updateTeamAggregateIfNecessary(mode, flowId, accountSlot.groupId, stepsToAdd, distanceToAdd)
  ])

  simpleLog.info(module, 'activityHandler', flowId, 'NOMINAL_END', {
    mode,
    key: streamRecord.keys.key
  })
}

/**
 * Quick fix function, to be able to set verifyPostconditions for helper functions to true in tests (coverage).
 * Normally, these functions should be tested separately, but now is not the time.
 *
 * @param verifyPostconditions {boolean}
 * @return {void}
 */
function setVerifyHelperPostconditions (verifyPostconditions) {
  // noinspection JSUnresolvedVariable
  isAnActualActivityStreamRecord.contract.verifyPostconditions = verifyPostconditions
  // noinspection JSUnresolvedVariable
  isRelevantChange.contract.verifyPostconditions = verifyPostconditions
  // noinspection JSUnresolvedVariable
  getGender.contract.verifyPostconditions = verifyPostconditions
  // noinspection JSUnresolvedVariable
  getStepsToAdd.contract.verifyPostconditions = verifyPostconditions
}

module.exports = streamRecordHandlerContract.implementation(activityHandler)
module.exports.setVerifyHelperPostconditions = setVerifyHelperPostconditions
