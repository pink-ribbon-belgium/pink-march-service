/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const streamRecordHandlerContract = require('../StreamRecordHandlerContract')
const dynamodb = require('../../server/aws/dynamodb/dynamodb')
const dynamodbTableName = require('../../server/aws/dynamodb/dynamodbTableName')
const simpleLog = require('../../_util/simpleLog')

async function accountAggregateStreamRecordHandler (flowId, sot, streamRecord) {
  const isAccountProfile =
    streamRecord.keys.key === `/${streamRecord.mode}/account/${streamRecord.newItem.data.accountId}/publicProfile` &&
    streamRecord.keys.submitted === 'actual'

  const isAccountSlot =
    streamRecord.keys.key ===
      `/${streamRecord.mode}/group/${streamRecord.newItem.data.groupId}/slot/${streamRecord.newItem.data.id}` &&
    !!streamRecord.newItem.data.accountId &&
    streamRecord.keys.submitted === 'actual'

  const wasAccountSlot =
    streamRecord.keys.key ===
      `/${streamRecord.mode}/group/${streamRecord.newItem.data.groupId}/slot/${streamRecord.newItem.data.id}` &&
    !streamRecord.newItem.data.accountId &&
    streamRecord.keys.submitted === 'actual'

  if (!isAccountProfile && !isAccountSlot && !wasAccountSlot) {
    // Extend logging when adding additional checks
    // simpleLog.info(module, 'accountAggregateStreamRecordHandler', flowId, 'NOT HANDLED', {
    //   mode: streamRecord.mode,
    //   key: streamRecord.keys.key,
    //   streamRecord: streamRecord,
    //   isAccountProfile: isAccountProfile,
    //   isAccountSlot: isAccountSlot,
    //   wasAccountSlot: wasAccountSlot
    // })
    return false
  }

  if (isAccountProfile) {
    simpleLog.info(module, 'accountAggregateStreamRecordHandler', flowId, 'CALL processAccountProfile', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processAccountProfile(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (isAccountSlot) {
    simpleLog.info(module, 'accountAggregateStreamRecordHandler', flowId, 'CALL processSlot', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processSlot(streamRecord, sot, flowId)
  }

  /* istanbul ignore else */
  if (wasAccountSlot) {
    simpleLog.info(module, 'accountAggregateStreamRecordHandler', flowId, 'CALL processDeleteSlot', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      streamRecord: streamRecord
    })
    return processDeleteSlot(streamRecord, sot, flowId)
  }
}

/**
 * Updates the AccountAggregate based on the AccountProfile-streamrecord
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processAccountProfile (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processAccountProfile', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  if (
    streamRecord.event === 'INSERT' ||
    (streamRecord.event === 'MODIFY' &&
      streamRecord.oldItem.data.firstName === streamRecord.newItem.data.firstName &&
      streamRecord.oldItem.data.lastName === streamRecord.newItem.data.lastName)
  ) {
    simpleLog.info(module, 'processAccountProfile', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update'
    })
    return false
  }

  simpleLog.info(module, 'processAccountProfile', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  const accountAggregateKey = `/${streamRecord.mode}/accountAggregate/${streamRecord.newItem.data.accountId}`

  const newName = `${streamRecord.newItem.data.firstName} ${streamRecord.newItem.data.lastName}`

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: accountAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#name = :name',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#name': 'name'
    },
    ExpressionAttributeValues: {
      ':name': newName
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processAccountProfile', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await updateAccountAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'AccountProfile')
}

/**
 * Updates the accountAggregate => set subgroupId of accountAggregate with the value from the slot
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processSlot (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processSlot', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  if (
    streamRecord.event === 'INSERT' ||
    (streamRecord.event === 'MODIFY' && streamRecord.oldItem.data.subgroupId === streamRecord.newItem.data.subgroupId)
  ) {
    simpleLog.info(module, 'processSlot', flowId, 'INTITIAL CHECKS', {
      mode: streamRecord.mode,
      key: streamRecord.keys.key,
      message: 'Nothing to update'
    })
    return false
  }

  simpleLog.info(module, 'processSlot', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  const accountAggregateKey = `/${streamRecord.mode}/accountAggregate/${streamRecord.newItem.data.accountId}`
  // eslint-disable-next-line camelcase
  const partition_key_3 = `/${streamRecord.mode}/accountAggregate/subgroup/${streamRecord.newItem.data.subgroupId}`

  const newSubgroupId = streamRecord.newItem.data.subgroupId

  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: accountAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression: 'SET #data.#subGroupId = :subGroupId, #partition_key_3= :partition_key_3',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#subGroupId': 'subGroupId',
      '#partition_key_3': 'partition_key_3'
    },
    ExpressionAttributeValues: {
      ':subGroupId': newSubgroupId,
      ':partition_key_3': partition_key_3
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processSlot', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await updateAccountAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'Slot')
}

/**
 * Updates the accountAggregate => delete subgroupId of accountAggregate with the value from the slot
 * @param streamRecord
 * @param sot {string}
 * @param flowId {string}
 * @returns {Promise<void>}
 */
async function processDeleteSlot (streamRecord, sot, flowId) {
  simpleLog.info(module, 'processDeleteSlot', flowId, 'CALLED', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key
  })

  simpleLog.info(module, 'processDeleteSlot', flowId, 'START PROCESSING', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    streamRecord: streamRecord
  })

  const accountAggregateKey = `/${streamRecord.mode}/accountAggregate/${streamRecord.oldItem.data.accountId}`

  // Set totalSteps, totalDistance and sortKeys to 0 to prevent possible errors when totals go below 0
  const params = {
    TableName: dynamodbTableName(streamRecord.mode),
    Key: {
      key: accountAggregateKey,
      submitted: 'actual'
    },
    UpdateExpression:
      'SET #data.#totalSteps = :key_value, #data.#totalDistance = :key_value REMOVE #data.#subGroupId, #data.#groupId, #partition_key_1, #partition_key_2, #partition_key_3, #sort_key_1, #sort_key_2, #sort_key_3',
    ExpressionAttributeNames: {
      '#data': 'data',
      '#subGroupId': 'subGroupId',
      '#groupId': 'groupId',
      '#totalSteps': 'totalSteps',
      '#totalDistance': 'totalDistance',
      '#partition_key_1': 'partition_key_1',
      '#partition_key_2': 'partition_key_2',
      '#partition_key_3': 'partition_key_3',
      '#sort_key_1': 'sort_key_1',
      '#sort_key_2': 'sort_key_2',
      '#sort_key_3': 'sort_key_3'
    },
    ExpressionAttributeValues: {
      ':key_value': 0
    },
    ReturnValues: 'UPDATED_NEW'
  }

  simpleLog.info(module, 'processDeleteSlot', flowId, 'PARAMETERS FOR UPDATE', {
    mode: streamRecord.mode,
    key: streamRecord.keys.key,
    params: params
  })

  await updateAccountAggregate(params, flowId, streamRecord.mode, streamRecord.keys.key, 'Slot')
}

/**
 * Execute dynamoDb update statement based on the provided params
 * @param params params for update statement
 * @param flowId flowId
 * @param mode mode
 * @param key key of the entity
 * @param entityName Name of the entity which triggered the update
 * @returns {Promise<void>}
 */
async function updateAccountAggregate (params, flowId, mode, key, entityName) {
  simpleLog.info(module, 'updateAccountAggregate', flowId, 'CALLED', {
    mode: mode,
    key: key,
    params: params
  })
  const dynamodbInstance = await dynamodb()

  simpleLog.info(module, 'updateAccountAggregate', flowId, 'DYNAMODB OK', {
    mode: mode,
    key: key
  })

  const result = await dynamodbInstance.update(params).promise()

  simpleLog.info(module, 'updateAccountAggregate', flowId, 'END', {
    mode: mode,
    key: key,
    result: result
  })
}

module.exports = streamRecordHandlerContract.implementation(accountAggregateStreamRecordHandler)
