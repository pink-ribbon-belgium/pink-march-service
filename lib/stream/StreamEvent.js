/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const { example: exampleSubmitted, exampleActual } = require('../server/aws/dynamodb/Item')

// https://docs.aws.amazon.com/lambda/latest/dg/with-ddb.html
// https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html

const example = {
  Records: [
    {
      eventName: 'INSERT',
      dynamodb: { Keys: { key: exampleActual.key, submitted: exampleActual.submitted }, NewImage: exampleActual }
    },
    {
      eventName: 'MODIFY',
      dynamodb: {
        Keys: { key: exampleSubmitted.key, submitted: exampleSubmitted.submitted },
        OldImage: { ...exampleSubmitted, flowId: '91621fc0-1cae-4bdb-9a52-153d6ddbdebd' },
        NewImage: {
          ...exampleSubmitted,
          flowId: 'e3105ab5-fcfe-4be8-b1ce-485d30b04653',
          data: { ...exampleSubmitted.data, createdAt: '2020-04-02T20:39:45.601Z', newData: 'new value' }
        },
        otherStuff: true
      }
    },
    {
      eventName: 'REMOVE',
      dynamodb: { Keys: { key: exampleActual.key, submitted: exampleActual.submitted }, OldImage: exampleActual }
    }
  ],
  otherStuff: true
}

/**
 * We cannot make the schema stricter than the rules of AWS. If our rules are validated, we must deal with that.
 */
const Record = Joi.object()
  .keys({
    eventName: Joi.allow('INSERT', 'MODIFY', 'REMOVE').required(),
    dynamodb: Joi.object()
      .keys({
        Keys: Joi.object()
          .unknown(true)
          .required(),
        // we do delete in automated tests
        OldImage: Joi.object().when('...eventName', {
          switch: [
            { is: 'INSERT', then: Joi.forbidden() },
            { is: 'MODIFY', then: Joi.required() },
            { is: 'REMOVE', then: Joi.required() }
          ]
        }),
        NewImage: Joi.object().when('...eventName', {
          switch: [
            { is: 'INSERT', then: Joi.required() },
            { is: 'MODIFY', then: Joi.required() },
            { is: 'REMOVE', then: Joi.forbidden() }
          ]
        })
      })
      .unknown(true)
      .required()
  })
  .unknown(true)
  .required()
  .label('Record')

const StreamEvent = Joi.object()
  .keys({
    Records: Joi.array()
      .items(Record)
      .required()
  })
  .unknown(true)
  .required()
  .example(example)
  .label('event')

module.exports = { schema: StreamEvent, example }
