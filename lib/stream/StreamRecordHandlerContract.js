/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const validateSchema = require('../_util/validateSchema')
const { schema: UUID } = require('../ppwcode/UUID')
const { schema: PointInTime } = require('../ppwcode/PointInTime')
const { schema: RequiredStreamRecord } = require('./ValidStreamRecord')

const RequiredSot = PointInTime.required().label('sot')
const RequiredFlowId = UUID.required().label('flowId')

/**
 * StreamRecordHandlers are called from {@link stream}.
 *
 * Stream record handlers should quickly determine whether the change is relevant to them, and
 * return early if not with `false`. If a stream record handler determines that the change is relevant, it should start
 * its work, and resolve asap with `undefined`.
 *
 * Stream handlers should never throw any errors, but instead log at INFO or WARN level (and still end nominally and
 * return `undefined`).
 *
 * All stream handlers should log their behavior (at the very least the call, and what they did on successful
 * completion).
 *
 * @type {PromiseContract}
 */
const StreamRecordHandlerContract = new PromiseContract({
  pre: [
    flowId => validateSchema(RequiredFlowId, flowId),
    (flowId, sot) => validateSchema(RequiredSot, sot),
    (flowId, sot, streamRecord) => validateSchema(RequiredStreamRecord, streamRecord)
  ],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === false || PromiseContract.outcome(arguments) === undefined
    }
  ]
  // no exceptions
})

module.exports = StreamRecordHandlerContract
