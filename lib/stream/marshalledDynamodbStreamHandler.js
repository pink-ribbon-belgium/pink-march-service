/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const processor = require('dynamodb-streams-processor')

/**
 * DynamoDB stream events arrive at Lambdas in an annoying format (_marshalled_). This returns a function that
 * _unmarshalls_ such an event, and calls the wrapped function with the unmarshalled {@link StreamEvent} and `context`.
 * It returns an 'marshalled Dynamodb stream handler' from an 'unmarshalled Dynamodb stream handler'.
 *
 * See {@link https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/Converter.html}.
 */
function marshalledDynamodbStreamHandler (handler) {
  return async function marshalledHandler (rawEvent, context) {
    const marshalledEvent = {
      Records: processor(rawEvent.Records)
    }
    return handler(marshalledEvent, context)
  }
}

module.exports = marshalledDynamodbStreamHandler
