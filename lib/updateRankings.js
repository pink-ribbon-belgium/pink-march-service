/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const log = require('./_util/simpleLog')
const Mode = require('./ppwcode/Mode').schema
const validateSchema = require('./_util/validateSchema')
const Joi = require('@hapi/joi')
const { schema: LambdaContext } = require('./LambdaContext')
const updateAccountAggregateRanking = require('../lib/updateRanking/updateAccountAggregateRanking')
const updateTeamAggregateRanking = require('../lib/updateRanking/updateTeamAggregateRanking')
const updateCompanyAggregateRanking = require('../lib/updateRanking/updateCompanyAggregateRanking')
const writeStatistics = require('../lib/server/business/statistics/writeStatistics')

const Event = Joi.object()
  .keys({
    mode: Mode.required()
  })
  .unknown(true)
  .required()
  .label('event')

const handler = new PromiseContract({
  pre: [event => validateSchema(Event, event), (event, context) => validateSchema(LambdaContext, context)],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
})

async function handlerImpl (event, context) {
  const { awsRequestId: flowId } = context // awsRequestId happens to be a UUID, used as flowId
  const { mode } = event
  const sot = new Date().toISOString()
  try {
    log.info(module, 'handler', flowId, 'CALLED', {
      event,
      context,
      sot,
      mode
    })
    await Promise.all([
      updateAccountAggregateRanking(sot, mode, flowId),
      updateTeamAggregateRanking(sot, mode, flowId),
      updateCompanyAggregateRanking(sot, mode, flowId),
      writeStatistics(sot, 'cron-job', mode, flowId)
    ])
    log.info(module, 'handler', flowId, 'NOMINAL_END', { sot, mode })
  } catch (err) {
    log.error(module, 'handler', flowId, err)
    // do not rethrow: nobody is listening
  }
}

module.exports = { handler: handler.implementation(handlerImpl) }
