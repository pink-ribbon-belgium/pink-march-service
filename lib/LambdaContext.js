/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('./ppwcode/UUID').schema

const example = {
  awsRequestId: 'f013ce9b-cb86-4e8d-b93b-18469663f1ed',
  otherStuff: true
}

const LambdaContext = Joi.object()
  .keys({ awsRequestId: UUID.required() })
  .unknown(true)
  .required()
  .example(example)
  .label('context')

module.exports = { schema: LambdaContext, example }
