/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const PromiseContract = require('@toryt/contracts-iv').Promise
const log = require('./_util/simpleLog')
const Mode = require('./ppwcode/Mode').schema
const validateSchema = require('./_util/validateSchema')
const Joi = require('@hapi/joi')
const { schema: LambdaContext } = require('./LambdaContext')
const getLeastRecentlyUpdated = require('./server/business/account/trackerConnection/getLeastRecentlyUpdated')
const updateActivities = require('./server/business/account/activity/updateActivities')

const Event = Joi.object()
  .keys({
    mode: Mode.required(),
    nrOfTrackerConnectionsToPoll: Joi.number()
      .integer()
      .positive()
      .required()
  })
  .unknown(true)
  .required()
  .label('event')

const handler = new PromiseContract({
  pre: [event => validateSchema(Event, event), (event, context) => validateSchema(LambdaContext, context)],
  post: [
    function () {
      return PromiseContract.outcome(arguments) === undefined
    }
  ]
})

async function handlerImpl (event, context) {
  const { awsRequestId: flowId } = context // awsRequestId happens to be a UUID, used as flowId
  const { mode, nrOfTrackerConnectionsToPoll } = event
  const sot = new Date().toISOString()
  try {
    log.info(module, 'handler', flowId, 'CALLED', {
      event,
      context,
      sot,
      mode,
      nrOfTrackerConnectionsToPoll
    })
    const trackerConnections = await getLeastRecentlyUpdated(flowId, sot, mode, nrOfTrackerConnectionsToPoll)
    await Promise.all(
      trackerConnections.map(trackerConnection => updateActivities(flowId, sot, mode, trackerConnection))
    )
    log.info(module, 'handler', flowId, 'NOMINAL_END', { sot, mode, nrOfTrackerConnectionsToPoll })
  } catch (err) {
    log.error(module, 'handler', flowId, err)
    // do not rethrow: nobody is listening
  }
}

module.exports = { handler: handler.implementation(handlerImpl) }
