/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Joi = require('@hapi/joi')

const uuidPattern = '[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}'
const regExpString = `(production|automated-test-${uuidPattern}|qa-\\d+|acceptance-\\d+|demo|dev-experiment|june2020)`
const regExp = new RegExp(`^${regExpString}$`)
const automatedTestPattern = new RegExp(`^automated-test-${uuidPattern}$`)

/**
 * For matching modes we use production infrastructure (STS, DynamoDB table). For all others we use test infrastructure.
 *
 * @type {RegExp}
 */
const productionModesPattern = /^(production|demo|june2020)$/

/** @type {Mode} */ const exampleAutomatedTest = 'automated-test-cbf23c05-2ed0-4fe2-8247-241aa8f0aa75'
/** @type {Mode} */ const example2 = 'production' // DO NOT USE AS MAIN EXAMPLE
/** @type {Mode} */ const example3 = 'qa-00234'
/** @type {Mode} */ const example4 = 'acceptance-00023'
/** @type {Mode} */ const example5 = 'demo'
/** @type {Mode} */ const example6 = 'june2020'
/** @type {Mode} */ const exampleDevExperiment = 'dev-experiment'

/**
 * Type of data.
 *
 * - the string `production` is used for production data
 * - automated tests use `automated-test-<uuid/v4>`, where `<uuid/v4>` is a UUID/v4 that uniquely identifies a test
 * - quality assurance uses `qa-<version>`
 * - acceptance tests use `acceptance-<version>`
 * - demo items (in the production environment) are identified by `demo`
 * - `dev-experiment` can be used by developers to … experiment
 *
 * @typedef Mode
 * @type {string}
 */

/** @type {StringSchema} */
const Mode = Joi.string()
  .pattern(regExp)
  .example(exampleAutomatedTest)
  .example(example2)
  .example(example3)
  .example(example4)
  .example(example5)
  .example(example6)
  .example(exampleDevExperiment)

module.exports = {
  example: exampleAutomatedTest,
  exampleDemo: example5,
  exampleJune2020: example6,
  exampleAutomatedTest,
  exampleDevExperiment,
  schema: Mode,
  regExpString,
  automatedTestPattern,
  productionModesPattern
}
