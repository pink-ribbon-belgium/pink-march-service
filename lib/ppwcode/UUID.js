/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Joi = require('@hapi/joi')

/** @type {UUID} */
const example = '72bd9328-3e3e-46da-93da-bff6a66d275d'

/**
 * @typedef UUID
 * @type {string}
 */

/** @type {StringSchema} */
const UUID = Joi.string()
  .uuid({
    version: ['uuidv4']
  })
  .example(example)

module.exports = { example, schema: UUID }
