/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Joi = require('@hapi/joi')

/** @type {StructureVersion} */
const example = 24

/**
 * Version of the data structure of a data structure.
 *
 * Each change of the data structure of item data of a given type should be signalled by a higher `structureVersion`
 * number. Read-code can then vary on interpretation of the data returned from persistent storage by `structureVersion`,
 * to make backward compatibility possible with zero-downtime.
 *
 * @typedef StructureVersion
 * @type {number}
 */

/** @type {NumberSchema} */
const StructureVersion = Joi.number()
  .integer()
  .positive()
  .example(example)

module.exports = { example, schema: StructureVersion }
