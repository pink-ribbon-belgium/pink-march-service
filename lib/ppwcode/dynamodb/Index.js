/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')

const example = {
  indexName: 'Index_A',
  partitionKey: 'partition_key_A',
  sortKey: 'sort_key_A'
}

// only allow existing indexes on dynamodb taitemible
const Index = Joi.object()
  .keys({
    indexName: Joi.string()
      .required()
      .valid('Index_A', 'Index_B', 'Index_C', 'Index_D', 'Index_E', 'Index_1', 'Index_2', 'Index_3'),
    partitionKey: Joi.string()
      .required()
      .valid(
        'partition_key_A',
        'partition_key_B',
        'partition_key_C',
        'partition_key_D',
        'partition_key_E',
        'partition_key_1',
        'partition_key_2',
        'partition_key_3'
      ),
    sortKey: Joi.any()
      .optional()
      .valid(
        'sort_key_A',
        'sort_key_B',
        'sort_key_C',
        'sort_key_D',
        'sort_key_E',
        'sort_key_1',
        'sort_key_2',
        'sort_key_3'
      )
  })
  .example(example)
  .unknown(false)

module.exports = { example, schema: Index }
