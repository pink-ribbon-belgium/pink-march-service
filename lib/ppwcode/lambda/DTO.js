/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const AccountId = require('../../api/account/AccountId')
const PointInTime = require('../PointInTime')
const StructureVersion = require('../StructureVersion')
const Link = require('./Link')

/** @type {DTO} */
const exampleIn = {
  structureVersion: StructureVersion.example,
  unknownProperty: {
    extraValue: 'extra value'
  }
}

/** @type {DTO} */
const exampleDynamodb = {
  createdAt: PointInTime.example,
  createdBy: AccountId.example,
  structureVersion: StructureVersion.example
}

/** @type {DTO} */
const exampleOut = {
  ...exampleDynamodb,
  links: Link.example
}

/**
 * DTO objects are stringifiable, are received and sent over HTTP, and stored as data in persistent storage.
 *
 * DTO objects have a number of common, required properties, for auditing and evolution support. Some properties are
 * required or forbidden, depending on their origin.
 *
 * Each change of the data structure of item data of a given type should be signalled by a higher `structureVersion`
 * number. Read-code can then vary on interpretation of the data returned from persistent storage by `structureVersion`,
 * to make backward compatibility possible with zero-downtime.
 *
 * @typedef DTO
 * @type {object}
 * @property {string} [createdAt] - SoT of the resource action that `put` this item version, as ISO-8601 string;
 *                                  optional as PUT DTO (ignored when present), required in all other cases
 * @property {string} [createdBy] - opaque id of the account that executed the resource action that `put` this item
 *                                  version; optional as PUT DTO (ignored when present), required in all other cases
 * @property {number} structureVersion - version of the data structure of this item data instance
 * @property {object} [links] - HateOAS references. Only relevant in output, where they are optional.
 */

const alterations = {
  /**
   * Make some properties optional properties. Use to validate data received from external users through the API.
   * These properties are then ignored.
   */
  input: schema => schema.optional(),

  /**
   * Require these properties. Use to and from persistent storage.
   */
  dynamodb: schema => schema.required(),

  /**
   * Require these properties. Use when returning data to external users  through the API.
   */
  output: schema => schema.required()
}

/** @type {ObjectSchema} */
const DTOBase = Joi.object()
  .keys({
    createdAt: PointInTime.schema.alter(alterations),
    createdBy: AccountId.schema.alter(alterations),
    structureVersion: StructureVersion.schema.required(),
    links: Link.schema.alter({
      dynamodb: schema => schema.forbidden(),
      input: schema => schema.optional(),
      output: schema => schema.optional()
    })
  })
  .alter({
    input: schema => schema.unknown(true),
    dynamodb: schema => schema.unknown(false),
    output: schema => schema.unknown(false)
  })

/** @type {ObjectSchema} */
const DTO = DTOBase.example(exampleDynamodb).alter({
  input: schema => schema.example(exampleIn),
  output: schema => schema.example(exampleOut)
})

module.exports = { example: exampleOut, exampleIn, exampleDynamodb, schema: DTO, baseSchema: DTOBase }
