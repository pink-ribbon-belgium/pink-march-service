/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
const Joi = require('@hapi/joi')
const AccountId = require('../../api/account/AccountId')

const example = {
  sub: AccountId.example
}

/**
 * `sub` must be a `segment-nz` according to [RFC 3986](https://tools.ietf.org/html/rfc3986),
 * i.e., a non-zero-length sequence of `pchar`:
 *
 *     segment-nz    = 1*pchar
 *     pchar         = unreserved / pct-encoded / sub-delims / ":" / "@"
 *     pct-encoded   = "%" HEXDIG HEXDIG
 *     unreserved    = ALPHA / DIGIT / "-" / "." / "_" / "~"
 *     sub-delims    = "!" / "$" / "&" / "'" / "(" / ")" / "*" / "+" / "," / ";" / "="
 *
 * Forbidden characters are, e.g., `:/?#[]@|`. If necessary, such characters can be URL-encoded (e.g., `%20` represents
 * a space).
 */
const jwtPayload = Joi.object()
  .keys({
    sub: AccountId.schema.required()
  })
  .unknown(true)
  .example(example)
module.exports = {
  example,
  schema: jwtPayload
}
