/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const ResponseHeaders = require('./ResponseHeaders')

const example = {
  statusCode: 200,
  headers: ResponseHeaders.example
}

const Response = Joi.object()
  .keys({
    statusCode: Joi.number()
      .integer()
      .positive()
      .required(),
    headers: ResponseHeaders.schema.required(),
    body: Joi.string()
      .allow(null)
      .optional() // IDEA if there is a body, it must be a JSON string
  })
  .unknown(false)
  .label('Response')
  .example(example)

module.exports = {
  schema: Response,
  example
}
