/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../UUID')
const Mode = require('../../Mode')

// noinspection SpellCheckingInspection
const jwtExample =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'
const authorizationExample = `Bearer ${jwtExample}`

/**
 * @typedef EventHeaders
 *
 * Headers of API Gateway Lambda Proxy integration event, ppwcode style.
 *
 * @property {UUID} x-flow-id
 * @property {Mode} x-mode
 * @property {number} [x-force-error]
 * @property {string} [authorization] - OAuth2 JWT Bearer access token
 */

/** @type {EventHeaders} */
const requiredExample = {
  'x-flow-id': UUID.example,
  'x-mode': Mode.example
}
const exampleWithAuthorization = { ...requiredExample, authorization: authorizationExample }
const exampleWithForceError = { ...requiredExample, 'x-force-error': 555 }

const Authorization = Joi.string()
  .pattern(/^Bearer [A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/) // allows padding
  .example(authorizationExample)

const EventHeaders = Joi.object()
  .keys({
    'x-flow-id': UUID.schema.required(),
    'x-mode': Mode.schema.required(),
    authorization: Authorization.optional(),
    'x-force-error': Joi.number()
      .integer()
      .min(400)
      .max(599)
      .optional()
  })
  .pattern(
    Joi.string()
      .lowercase()
      .pattern(/[a-z0-9-]+/),
    Joi.string().required()
  )
  .unknown(true)
  .example(requiredExample)
  .example(exampleWithAuthorization)
  .example(exampleWithForceError)

module.exports = { schema: EventHeaders, example: requiredExample, exampleWithAuthorization }
