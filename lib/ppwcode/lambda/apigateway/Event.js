/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const Headers = require('./EventHeaders')
const PathParameters = require('./PathParameters')

/**
 * @typedef Event
 *
 * Event according to API Gateway Lambda proxy integration.
 *
 * @property {string} resource
 * @property {string} path
 * @property {string} httpMethod
 * @property {EventHeaders} headers
 * @property {object} pathParameters
 * @property {*} [body]
 */

/** @type {Event} */
const example1 = {
  resource: '/I/account/{accountId}',
  path: '/I/account/account_identifier',
  httpMethod: 'GET',
  headers: Headers.exampleWithAuthorization,
  pathParameters: PathParameters.example,
  body: null
}

/** @type {Event} */
const example2 = {
  resource: '/I/account/{accountId}',
  path: '/I/health',
  httpMethod: 'GET',
  headers: Headers.exampleWithAuthorization,
  pathParameters: null,
  body: "{ aProperty: ' this is a body' }"
}

/** @type {Event} */
const example3 = {
  resource: '/I/statistics',
  path: '/I/statistics',
  httpMethod: 'GET',
  headers: Headers.example,
  pathParameters: null,
  body: null
}

const Event = Joi.object()
  .keys({
    resource: Joi.string()
      .pattern(/^\/.*/)
      .required(),
    path: Joi.string()
      .pattern(/^\/.*/)
      .uri({ relativeOnly: true })
      .required(),
    httpMethod: Joi.string()
      .valid('GET', 'PUT', 'POST', 'DELETE')
      .required(),
    headers: Headers.schema.required(),
    // IDEA Implement multiValueHeaders when needed
    // IDEA Implement queryStringParameters when needed
    // IDEA Implement multiValueQueryStringParameters when needed
    pathParameters: PathParameters.schema.allow(null).required(),
    // IDEA implement stageVariables when needed
    // IDEA Implement requestContext when needed
    body: Joi.any()
      .allow(null)
      .required()
    // IDEA implement isBase64Encoded when needed
  })
  .unknown(true)
  .example(example1)
  .example(example2)
  .example(example3)

module.exports = {
  example: example1,
  schema: Event
}
