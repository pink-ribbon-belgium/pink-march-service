/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const Joi = require('@hapi/joi')
const UUID = require('../../UUID')

const example = {
  'x-flow-id': UUID.example
}

const ResponseHeaders = Joi.object()
  .keys({
    'x-flow-id': UUID.schema.required(),
    'WWW-Authenticate': Joi.string() // IDEA: limit to 401 and 403
      .pattern(/^Bearer/)
      .optional(),
    'x-www-authenticate': Joi.string() // IDEA: limit to 401 and 403
      .pattern(/^Bearer/)
      .optional(),
    'cache-control': Joi.string()
      .pattern(/^public, max-age=/)
      .optional()
  })
  .and('WWW-Authenticate', 'x-www-authenticate')
  .example(example)

module.exports = {
  example,
  schema: ResponseHeaders
}
