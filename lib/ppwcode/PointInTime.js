/*
 * Copyright (C) 2020 PeopleWare NV
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/lic
 */

const Joi = require('@hapi/joi')

/** @type {PointInTime} */
const example = '2020-01-23T15:22:39.212Z'

/**
 * Usually "start of transaction" (SoT). A point in time, represented as ISO-8601 string.
 *
 * @typedef PointInTime
 * @type {string}
 */

/** @type {PointInTime} */
const PointInTime = Joi.string()
  .isoDate()
  .example(example)

module.exports = { example, schema: PointInTime }
