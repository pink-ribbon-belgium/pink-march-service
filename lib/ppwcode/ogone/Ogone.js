/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const crypto = require('crypto')
const fetch = require('node-fetch')
const qs = require('querystring')
const xml2js = require('xml2js')

function amountFormator (value) {
  return Math.round(value * 100)
}

function OgoneError (response) {
  this.name = 'OgoneError'
  this.message = 'NCError: ' + response.ncerror
  this.response = response
}

function buildSha (body, shaIn) {
  const hashable = Object.keys(body)
    .map(k => `${k}=${body[k]}${shaIn}`)
    .join('')
  return crypto
    .createHash('sha512')
    .update(hashable)
    .digest('hex')
    .toUpperCase()
}

function createPaymentUrl (payment, ogoneConfig) {
  payment.amount = amountFormator(payment.amount)
  const body = Request.upper(payment)
  const sha = buildSha(body, ogoneConfig.shaIn)
  const queryString = Request.stringify({ ...body, SHASIGN: sha })
  return `${ogoneConfig.postUrl}?${queryString}`
}

OgoneError.prototype = new Error()
OgoneError.prototype.constructor = OgoneError

function Ogone (mode, defaults) {
  this.defaults = defaults
  this.mode = mode
}

Ogone.prototype.getPayment = async function (json) {
  const result = await new QueryRequest(this.mode, { ...json, ...this.defaults }).status()

  return result
}

Ogone.createPaymentUrl = createPaymentUrl

function Request (url, json, schema) {
  this.query = { ...json }
  this.url = url
  this.parser = new xml2js.Parser(xml2js.defaults['0.1'])
}

Request.prototype._prepare = function (query) {
  return Request.upper(query)
}

Request.prototype._send = async function () {
  var prepared = this._prepare(this.query)
  var requestBody = Request.stringify(prepared)
  const body = await fetch(this.url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: requestBody
  }).then(body => body.text())
  return new Promise(resolve => {
    this.parser.parseString(body, function (err, result) {
      if (err) throw err
      if (!result['@']) throw new Error('Unknown response: ' + result)
      var normalize = Request.lower(result['@'])
      if (normalize.ncerror && normalize.ncerror !== '0') {
        throw new OgoneError(normalize)
      }
      resolve(normalize)
    })
  })
}

Request.stringify = function (obj) {
  var prepared = {}
  Object.keys(obj)
    .filter(k => obj[k])
    .forEach(function (key) {
      prepared[key] = obj[key].toString()
    })
  return qs.stringify(prepared)
}

Request.normalize = function (method, obj) {
  var result = {}
  Object.keys(obj).map(function (key) {
    var normalized = key[method]()
    result[normalized] = obj[key]
  })
  return result
}

Request.lower = Request.normalize.bind(this, 'toLowerCase')
Request.upper = Request.normalize.bind(this, 'toUpperCase')

function QueryRequest (mode, json) {
  Request.call(this, 'https://secure.ogone.com/ncol/' + mode + '/querydirect.asp', json)
}

QueryRequest.prototype = Request.prototype
QueryRequest.constructor = QueryRequest

QueryRequest.prototype.status = Request.prototype._send

module.exports = Ogone
