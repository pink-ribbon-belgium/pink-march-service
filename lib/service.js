/*
 * Copyright (C) 2020 PeopleWare NV
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

const validateSchema = require('./_util/validateSchema')
const Boom = require('@hapi/boom')
const getAuthorizedSub = require('./server/authorization/getAuthorizedSub')
const PromiseContract = require('@toryt/contracts-iv').Promise
const requiredResponseSchema = require('./ppwcode/lambda/apigateway/Response').schema.required()
const nominalHttpResult = require('./server/NominalHttpResult')
const requiredEvent = require('./ppwcode/lambda/apigateway/Event').schema.required()
const resourceActionsMap = require('./server/resourceActionsMap')
const { v4: uuidv4 } = require('uuid')
const log = require('./_util/simpleLog')

const handler = new PromiseContract({
  pre: [
    event => !!event,
    event => typeof event === 'object',
    (event, context) => !!context,
    (event, context) => typeof context === 'object'
  ],
  post: [
    function (event, context, response) {
      return validateSchema(requiredResponseSchema, response)
    }
  ]
})

/**
 * Although it is standard in node to lowercase headers always, API Gateway does not, and it is a pending issue in Joi
 * to do it for us (https://github.com/hapijs/joi/pull/1771).
 *
 * As a workaround, we lowercase all headers here, if they exist, before we do anything else.
 */
function lowercaseHeaders (event) {
  if (event.headers && typeof event.headers === 'object') {
    event.headers = Object.keys(event.headers).reduce((acc, mixedCaseHeaderName) => {
      acc[mixedCaseHeaderName.toLowerCase()] = event.headers[mixedCaseHeaderName]
      return acc
    }, {})
  }
}

async function handlerImpl (event, context) {
  try {
    lowercaseHeaders(event)

    const shouldLog = event.httpMethod !== 'GET'

    /* istanbul ignore next */
    if (shouldLog) {
      log.info(module, 'handler', (event && event.headers && event.headers['x-flow-id']) || 'no flow id', 'CALLED', {
        event,
        context
      })
    }

    const sot = new Date().toISOString()
    const eventValidation = requiredEvent.validate(event, {
      abortEarly: false
    })
    if (eventValidation.error) {
      // noinspection ExceptionCaughtLocallyJS
      throw Boom.badRequest(eventValidation.error)
    }
    if (eventValidation.value.headers['x-force-error']) {
      // noinspection ExceptionCaughtLocallyJS
      throw new Boom.Boom('Forced error', { statusCode: eventValidation.value.headers['x-force-error'] })
    }

    const mode = eventValidation.value.headers['x-mode']
    const flowId = eventValidation.value.headers['x-flow-id']

    const resource = resourceActionsMap[event.resource]
    if (!resource) {
      // noinspection ExceptionCaughtLocallyJS
      throw Boom.notFound(`${event.resource} does not exist`)
    }
    const resourceAction = resource[event.httpMethod]
    if (!resourceAction) {
      // noinspection ExceptionCaughtLocallyJS
      throw Boom.methodNotAllowed(`${event.httpMethod} is not allowed on resource ${event.path}`)
    }

    const shouldAuthorize = event.resource !== '/I/statistics'

    // throws 401 or 403
    const sub = shouldAuthorize ? await getAuthorizedSub(eventValidation.value) : 'public'

    const response = nominalHttpResult(
      flowId,
      await resourceAction(
        sot,
        sub,
        mode,
        flowId,
        event.pathParameters === null ? undefined : event.pathParameters,
        event.body ? JSON.parse(event.body) : event.body
      )
    )

    /* istanbul ignore next */
    if (shouldLog) {
      log.info(module, 'handler', flowId, 'NOMINAL_END', response)
    }

    return response
  } catch (err) {
    // We get an error from event validation if there are no errors, or no flow id: we need to be able to report
    // those to.
    const responseFlowId = (event && event.headers && event.headers['x-flow-id']) || uuidv4()
    if (err.isBoom) {
      const response = {
        statusCode: err.output.statusCode,
        headers: {
          ...err.output.headers,
          'x-flow-id': responseFlowId,
          'cache-control': 'public, max-age=5'
        },
        body: JSON.stringify(err.output.payload)
      }
      log.info(module, 'handler', responseFlowId, 'EXCEPTIONAL_END', response)
      return response
    }
    log.error(module, 'handler', responseFlowId, err)
    return {
      statusCode: 500,
      headers: {
        'x-flow-id': responseFlowId,
        'cache-control': 'public, max-age=5'
      },
      body: JSON.stringify(
        'We messed up. Sorry. The issue has been logged. Alarms have sounded. Teams are scrambling. We are on it.'
      )
    }
  }
}

module.exports = { handler: handler.implementation(handlerImpl) }
