# Pink Ribbon Belgium - Pink March 2020 Service

Back-end Service of the Pink Ribbon Belgium Pink March 2020 application. Realized on AWS with Lambda and API Gateway.

## Target environment

This application is developed for Node 12 LTS or later.

## Development

### IDE

JetBrains WebStorm settings are included and kept up-to-date by all developers.

## License

Released under the [GNU GENERAL PUBLIC LICENSE, Version 3](https://www.gnu.org/licenses/gpl-3.0.nl.html).

## Deployment
